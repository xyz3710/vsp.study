﻿namespace ManifestResourceTest
{
	partial class FrmTestDriver
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.picTest = new System.Windows.Forms.PictureBox();
			this.btnLoad = new System.Windows.Forms.Button();
			this.txtMsg = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.picTest)).BeginInit();
			this.SuspendLayout();
			// 
			// picTest
			// 
			this.picTest.Location = new System.Drawing.Point(12, 23);
			this.picTest.Name = "picTest";
			this.picTest.Size = new System.Drawing.Size(268, 159);
			this.picTest.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.picTest.TabIndex = 0;
			this.picTest.TabStop = false;
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(84, 198);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(124, 23);
			this.btnLoad.TabIndex = 1;
			this.btnLoad.Text = "Resource Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// txtMsg
			// 
			this.txtMsg.Location = new System.Drawing.Point(13, 233);
			this.txtMsg.Multiline = true;
			this.txtMsg.Name = "txtMsg";
			this.txtMsg.Size = new System.Drawing.Size(267, 303);
			this.txtMsg.TabIndex = 2;
			// 
			// FrmTestDriver
			// 
			this.ClientSize = new System.Drawing.Size(292, 548);
			this.Controls.Add(this.txtMsg);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.picTest);
			this.Name = "FrmTestDriver";
			this.Text = "FrmTestDriver";
			((System.ComponentModel.ISupportInitialize)(this.picTest)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox picTest;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.TextBox txtMsg;
	}
}

