using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Resources;

namespace ManifestResourceTest
{
	public partial class FrmTestDriver : Form
	{
		public FrmTestDriver()
		{
			InitializeComponent();

			ResourceManager rm = new ResourceManager(GetType());
			
			Text = rm.GetString("Test");
			picTest.Image = (Bitmap)rm.GetObject("excel");
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			Assembly assembly = GetType().Assembly;

			// 어셈블리 매니페스트 리소스를 나열한다
			foreach (string resourceName in assembly.GetManifestResourceNames())
				txtMsg.Text += string.Format("{0}{1}", resourceName, Environment.NewLine);
			
			// Load excel.gif resource
			// "Resources.excel.gif" 리소스를 바이트 형태로 저장하여 스트림을 얻는다.
			// NOTICE: 1. 이 스트림이 닫히지 않도록 주의를 해야 하는데, 만약 닫히면 비트맵 개체가 스트림에 액세스 하지 못하게 된다.
			// NOTICE: 2. 또한 리소스 이름을 정확한 포맷으로 작성하도록 주의해야 한다.
			Stream stream = assembly.GetManifestResourceStream(string.Format("{0}.{1}", GetType().Namespace, "Resources.excel.gif"));

			if (stream != null)
				picTest.Image = new Bitmap(stream);
		}
	}
}