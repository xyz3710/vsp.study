using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;

namespace Serialize
{
	public class Program
	{
		public static void Main(string[] args)
		{
			StreamTestDriver();

			DoSerialize();
		}

		private static void DoSerialize()
		{
			MyData myData = new MyData();

			string tempPath = Environment.GetEnvironmentVariable("Temp");

			using (Stream stream = new FileStream(string.Format(@"{0}\MyData.xml", tempPath), FileMode.Create))
            {
            	// Write stream
				IFormatter formatter = new SoapFormatter();

				formatter.Serialize(stream, myData);

				// Seek to BOF
				stream.Seek(0L, SeekOrigin.Begin);

				// Read stream
				MyData readData = formatter.Deserialize(stream) as MyData;

				if (readData != null)
					Console.WriteLine("XML read result : {0}\t{1}", readData.StringValue, readData.Length);
            }
		}

		private static void StreamTestDriver()
		{
			string s = "Wahoo!";
			int n = 452;

			using (Stream stream = new MemoryStream())
			{
				#region using Byte to stream
				//				// Write stream
				//				byte[] byte1 = UnicodeEncoding.Unicode.GetBytes(s);
				//				byte[] byte2 = BitConverter.GetBytes(n);
				//
				//				stream.Write(byte1, 0, byte1.Length);
				//				stream.Write(byte2, 0, byte2.Length);
				//
				//				// Seek to BOF
				//				stream.Seek(0L, SeekOrigin.Begin);
				//
				//				// Read stream
				//				byte[] byte3 = new byte[stream.Length - 4];
				//				byte[] byte4 = new byte[4];
				//
				//				stream.Read(byte3, 0, byte3.Length);
				//				stream.Read(byte4, 0, byte4.Length);
				//
				//				Console.WriteLine("{0}\t{1}", UnicodeEncoding.Unicode.GetString(byte3), BitConverter.ToInt32(byte4, 0));
				#endregion

				#region using StreamReader, StreamWriter
				//				// Write stream
				//				StreamWriter sWriter = new StreamWriter(stream);
				//
				//				sWriter.WriteLine(s);
				//				sWriter.WriteLine(n);
				//				sWriter.Flush();
				//
				//				// Seek to BOF
				//				stream.Seek(0L, SeekOrigin.Begin);
				//
				//				// Read stream
				//				StreamReader sReader = new StreamReader(stream);
				//
				//				string s2 = sReader.ReadLine();
				//				int n2 = int.Parse(sReader.ReadLine());
				//
				//				Console.WriteLine("{0}\t{1}", s2, n2);
				#endregion

				#region using BinaryWriter, BinaryReader
				// Write stream
				BinaryWriter bWriter = new BinaryWriter(stream);

				bWriter.Write(s);
				bWriter.Write(n);
				bWriter.Flush();

				// Seek to BOF
				bWriter.Seek(0, SeekOrigin.Begin);

				// Read stream
				BinaryReader bReader = new BinaryReader(stream);

				string s3 = bReader.ReadString();
				int n3 = bReader.ReadInt32();

				Console.WriteLine("{0}\t{1}", s3, n3);
				#endregion
			}
		}
	}
}
