using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;

namespace Serialize
{
	[Serializable]
	public class MyData : ISerializable, IDeserializationCallback
	{
		private string _stringValue;
		[NonSerialized]
		private int _length;
		private ArrayList _oldString;
		private static string _currentVersion;

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the MyData class.
		/// </summary>
		public MyData()
		{
			_stringValue = "Yaho!!";
			_length = _stringValue.Length;
			_oldString = new ArrayList();
			_currentVersion = "1.0";
		}

		/// <summary>
		/// Initializes a new instance of the MyData class.
		/// </summary>
		public MyData(SerializationInfo info, StreamingContext context)
		{
//			// 이름/값 쌍으로부터 값을 얻는다.
//			_stringValue = info.GetString("MyString");
//			_length = _stringValue.Length;

			// 해당 버전에 기초해서 데이터를 읽어들인다.
			string streamVersion = info.GetString("Version");

			switch (streamVersion)
			{
				case "1.0":
					_stringValue = info.GetString("MyString");
					_length = _stringValue.Length;

					break;
				case "2.0":
					_stringValue = info.GetString("MyString");
					_length = _stringValue.Length;
					_oldString = (ArrayList)info.GetValue("OldString", typeof(ArrayList));
					
					break;
			}
		}
        
		#endregion

		#region Property
		public string StringValue
		{
			get
			{
				return _stringValue;
			}
			set
			{
				_stringValue = value;
				_length = _stringValue.Length;
			}
		}

		public int Length
		{
			get
			{
				return _length;
			}
		}
		#endregion

		#region ISerializable 멤버

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
//			// 이름/값 쌍을 값에 추가한다.
//			info.AddValue("MyString", _stringValue);

			// 데이터에 버전을 기록해 둔다.
			info.AddValue("Version", _currentVersion);
			info.AddValue("MyString", _stringValue);
			info.AddValue("OldString", _oldString);
		}

		#endregion

		#region IDeserializationCallback 멤버

		public void OnDeserialization(object sender)
		{
			// 문자열의 크기(_length)를 _stringValue 필드에 저장(cache)한다
			_length = _stringValue.Length;           
		}

		#endregion
	}
}
