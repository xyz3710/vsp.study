using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate09
{
	public class Boss
	{
		public int WorkCompleted()
		{
			System.Threading.Thread.Sleep(3000);
			Console.WriteLine("Better...");

			return 6;
		}
	}
}
