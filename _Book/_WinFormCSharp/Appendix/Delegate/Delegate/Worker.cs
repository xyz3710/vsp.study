using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate09
{
	public class Worker
	{
		public event WorkStarted Started;
		public event WorkProcessing Processing;
		public event WorkCompleted Completed;

		public void DoWork()
		{
			Console.WriteLine("Worker : work started.");

			if (Started != null)
				Started();

			if (Processing != null)
				Processing();

			Console.WriteLine("Worker : work completed");

			if (Completed != null)
			{
				#region 비동기 통지 : Fire and forget
//				foreach (WorkCompleted wc in Completed.GetInvocationList())
//				{
//					int grade = wc();
//
//					Console.WriteLine("Worker grade = {0}", grade);
//				}
				#endregion

				#region 비동기 통지 : Polling
				foreach (WorkCompleted wc in Completed.GetInvocationList())
				{
					IAsyncResult res = wc.BeginInvoke(null, null);

					while (res.IsCompleted == false)
						System.Threading.Thread.Sleep(1);

					int grade = wc.EndInvoke(res);

					Console.WriteLine("Worker grade = {0}", grade);
				}
				#endregion

				#region 비동기 통지 : 대리자
//				foreach (WorkCompleted wc in Completed.GetInvocationList())
//					wc.BeginInvoke(new AsyncCallback(WorkGraded), wc);
				#endregion
			}
		}

		private void WorkGraded(IAsyncResult res)
		{
			WorkCompleted wc = res.AsyncState as WorkCompleted;

			if (wc != null)
			{
				int grade = wc.EndInvoke(res);

				Console.WriteLine("Worker grade = {0}", grade);
			}			
		}
	}
}
