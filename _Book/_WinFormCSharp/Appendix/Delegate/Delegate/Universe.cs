using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate09
{
	public delegate void WorkStarted();
	public delegate void WorkProcessing();
	public delegate int WorkCompleted();

	public class Universe
	{
		static void Main(string[] args)
		{
			Worker peter = new Worker();
			Boss boss = new Boss();

			peter.Completed += new WorkCompleted(boss.WorkCompleted);
			peter.Started += new WorkStarted(Universe.WorkerStartedWork);
			peter.Completed += new WorkCompleted(Universe.WorkerCompletedWork);

			peter.DoWork();
			
			Console.WriteLine("Main : worker completed work");
		}

		public static void WorkerStartedWork()
		{
			Console.WriteLine("Universe notices worker starting work");
		}

		public static int WorkerCompletedWork()
		{
			System.Threading.Thread.Sleep(4000);
			Console.WriteLine("Universe pleased with workers's work");

			return 7;
		}
	}
}