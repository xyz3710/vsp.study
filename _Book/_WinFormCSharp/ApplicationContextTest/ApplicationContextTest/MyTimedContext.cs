using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ApplicationContextTest
{
	public partial class MyTimedContext : ApplicationContext
	{
		private Timer _timer;

		/// <summary>
		/// Initializes a new instance of the MyTimedContext class.
		/// </summary>
		public MyTimedContext(Form form)
			: base(form)
		{
			if (_timer == null)
				_timer = new Timer();
            
			_timer.Tick += new EventHandler(TimesUp);
			_timer.Interval = 3000;
			_timer.Start();
		}

		void TimesUp(object sender, EventArgs e)
		{
			_timer.Stop();
			_timer.Dispose();

			DialogResult res = MessageBox.Show("시간이 지났습니다.\r\n계속하시겠습니까?", "Times Up!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

			if (res == DialogResult.Yes)
				_timer.Start();
			else
				base.MainForm.Close();
		}

		protected override void OnMainFormClosed(object sender, EventArgs e)
		{
			// 기본 클래스가 어플리케이션을 종료하지 않도록 한다.
			// 주의: 리모팅 서비스가 자체 클라이언트 지원을 마치고 났을 때 Application.Exit를 호출한다는 사실에 유의..
			if (ServiceRemotingClient() == true)
				return;
			
			base.OnMainFormClosed(sender, e);
		}
		
		protected  bool ServiceRemotingClient()
		{
			return false;
		}
	}
}
