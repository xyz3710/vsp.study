using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TestDriver
{
	public partial class TestDriver : Form
	{
		private bool _mouseDown;

		public TestDriver()
		{
			InitializeComponent();
		}

		private void TestDriver_Load(object sender, EventArgs e)
		{
			
		}
				
		private void button1_Click(object sender, EventArgs e)
		{
			ellipseLabel1.Prefix = "Test : ";
		}

		private void ellipseLabel1_PrefixChanged(object sender, System.Windows.CustomControls.PrefixEventArgs e)
		{
			MessageBox.Show(string.Format("{0}로 변경되었다", e.Prefix));
		}

		private void ellipseLabel1_MouseDown(object sender, MouseEventArgs e)
		{
			_mouseDown = true;

			SetMouseForeColor(e);
		}

		private void ellipseLabel1_MouseMove(object sender, MouseEventArgs e)
		{
			if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
			//if (_mouseDown == true)
            	SetMouseForeColor(e);
		}

		private void ellipseLabel1_MouseUp(object sender, MouseEventArgs e)
		{
			SetMouseForeColor(e);

			_mouseDown = false;
		}

		private void SetMouseForeColor(MouseEventArgs e)
		{
			int red = (e.X * 255 / (ClientSize.Width - e.X)) % 256;

			if (red < 0)
				red -= red;
			
			if (red > 256)
				red = 0;

			int green = 128;
			int blue = (e.Y * 255 / ClientSize.Height - e.Y) % 256;

			if (blue < 0)
				blue -= blue;

			if (blue > 256)
				blue = 0;

			BackColor = Color.FromArgb(red, green, blue);
		}
	}
}