﻿namespace TestDriver
{
	partial class TestDriver
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.fileTextBox1 = new FileTextBox.FileTextBox();
			this.ellipseLabel2 = new System.Windows.CustomControls.EllipseLabel();
			this.ellipseLabel1 = new System.Windows.CustomControls.EllipseLabel();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(37, 14);
			this.button1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(123, 31);
			this.button1.TabIndex = 1;
			this.button1.Text = "PrefixChage";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// fileTextBox1
			// 
			this.fileTextBox1.ForeColor = System.Drawing.Color.Red;
			this.fileTextBox1.Location = new System.Drawing.Point(278, 140);
			this.fileTextBox1.Name = "fileTextBox1";
			this.fileTextBox1.Size = new System.Drawing.Size(692, 26);
			this.fileTextBox1.TabIndex = 2;
			// 
			// ellipseLabel2
			// 
			this.ellipseLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
			this.ellipseLabel2.Location = new System.Drawing.Point(32, 140);
			this.ellipseLabel2.Name = "ellipseLabel2";
			this.ellipseLabel2.Prefix = null;
			this.ellipseLabel2.Size = new System.Drawing.Size(240, 26);
			this.ellipseLabel2.TabIndex = 3;
			this.ellipseLabel2.Text = "File 경로를 입력하세요";
			// 
			// ellipseLabel1
			// 
			this.ellipseLabel1.ForeColor = System.Drawing.Color.BlanchedAlmond;
			this.ellipseLabel1.Location = new System.Drawing.Point(37, 55);
			this.ellipseLabel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.ellipseLabel1.Name = "ellipseLabel1";
			this.ellipseLabel1.Prefix = "Foo : ";
			this.ellipseLabel1.Size = new System.Drawing.Size(943, 57);
			this.ellipseLabel1.TabIndex = 0;
			this.ellipseLabel1.Text = "ellipseLabel1";
			this.ellipseLabel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ellipseLabel1_MouseDown);
			this.ellipseLabel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ellipseLabel1_MouseMove);
			this.ellipseLabel1.PrefixChanged += new System.Windows.CustomControls.EllipseLabel.PrefixChangedEventHandler(this.ellipseLabel1_PrefixChanged);
			this.ellipseLabel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ellipseLabel1_MouseUp);
			// 
			// TestDriver
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 19F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.ClientSize = new System.Drawing.Size(1016, 713);
			this.Controls.Add(this.ellipseLabel2);
			this.Controls.Add(this.fileTextBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.ellipseLabel1);
			this.Font = new System.Drawing.Font("Impact", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
			this.Name = "TestDriver";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "TestDriver";
			this.Load += new System.EventHandler(this.TestDriver_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.CustomControls.EllipseLabel ellipseLabel1;
		private System.Windows.Forms.Button button1;
		private FileTextBox.FileTextBox fileTextBox1;
		private System.Windows.CustomControls.EllipseLabel ellipseLabel2;
	}
}