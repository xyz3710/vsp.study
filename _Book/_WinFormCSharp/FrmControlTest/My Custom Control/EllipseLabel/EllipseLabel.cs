﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace System.Windows.CustomControls
{
	[ToolboxBitmap(@"D:\VSP\FrmControlTest\My Custom Control\EllipseLabel\icon.bmp")]
	public partial class EllipseLabel : Control
	{
		private string _prefix;
		private Color _lineColor;
		private int _lineWidth;

		public delegate void PrefixChangedEventHandler(object sender, PrefixEventArgs e);

		public event PrefixChangedEventHandler PrefixChanged;

		public EllipseLabel()
		{
			InitializeComponent();

			TextChanged += new EventHandler(EllipseLabel_TextChanged);

			_lineColor = Color.Black;
			_lineWidth = 1;
		}

		/// <summary>
		/// Text 속성의 Prefix을 설정합니다.
		/// </summary>
		[Description("Text의 Prefix을 지정합니다.")]
		public string Prefix
		{
			get
			{
				return _prefix;
			}
			set
			{
				_prefix = value;

				// PrefixChanged event를 발생시킨다.
				if (PrefixChanged != null)
					PrefixChanged(this, new PrefixEventArgs(_prefix));

				Invalidate();
			}
		}

		/// <summary>
		/// Reset~ 으로 method를 작성하면 속성창에서 오른쪽 버튼을 누르면 다시 설정이란 메뉴가 나온다
		/// </summary>
		private void ResetPrefix()
		{
			_prefix = string.Empty;
		}

		void EllipseLabel_TextChanged(object sender, EventArgs e)
		{
			Invalidate();
		}

		[Category("모양")]
		[Description("테두리 선 색을 변경합니다.(기본 : Black)")]
		[DefaultValue(typeof(Color), "Black")]
		public Color LineColor
		{
			get
			{
				return _lineColor;
			}
			set
			{
				_lineColor = value;

				Invalidate();
			}
		}

		[Category("모양")]
		[Description("테두리 선 두께를 변경합니다.(기본 : 1)")]
		[DefaultValue(1)]
		public int LineWidth
		{
			get
			{
				return _lineWidth;
			}
			set
			{
				_lineWidth = value;

				Invalidate();
			}
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			Graphics graphics = pe.Graphics;

			using (Brush foreBrush = new SolidBrush(ForeColor))
			{
				using (Brush backBrush = new SolidBrush(BackColor))
				{
					using (Brush lineBrush = new SolidBrush(_lineColor))
					{
						using (Pen borderPen = new Pen(lineBrush, _lineWidth))
						{
							borderPen.Alignment = PenAlignment.Inset;

							graphics.FillEllipse(foreBrush, pe.ClipRectangle);
							graphics.DrawEllipse(borderPen, pe.ClipRectangle);

							StringFormat format = new StringFormat();

							format.Alignment = StringAlignment.Center;
							format.LineAlignment = StringAlignment.Center;

							graphics.DrawString(string.Format("{0}{1}", _prefix, Text), Font, backBrush, pe.ClipRectangle, format);
						}
					}
				}
			}

			// 기본 클래스 OnPaint를 호출하고 있습니다.
			base.OnPaint(pe);
		}
	}
}
