﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FileTextBox
{
	public partial class FileTextBox : TextBox
	{
		public FileTextBox()
		{
			InitializeComponent();
		}

		protected override void OnTextChanged(EventArgs e)
		{
			// 존재하지 않는 파일의 정보가 입력되면, Text 색상을 빨강색으로 입력한다.
			if (File.Exists(Text) == false)
				ForeColor = Color.Red;
			else
				ForeColor =  Color.Black;


			base.OnTextChanged(e);
		}
	}
}
