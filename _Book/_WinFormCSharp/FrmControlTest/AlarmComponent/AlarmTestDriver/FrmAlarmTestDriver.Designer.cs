﻿namespace AlarmTestDriver
{
	partial class FrmAlarmTestDriver
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.alarmComponent1 = new Alarm.AlarmComponent(this.components);
			this.cboAlarmTime = new System.Windows.Forms.ComboBox();
			this.lblWhen = new System.Windows.Forms.Label();
			this.btnSetAlarm = new System.Windows.Forms.Button();
			this.lblNow = new System.Windows.Forms.Label();
			this.clkNow = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// alarmComponent1
			// 
			this.alarmComponent1.Alarm = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
			this.alarmComponent1.AlarmSounded += new System.EventHandler(this.alarmComponent1_AlarmSounded);
			// 
			// cboAlarmTime
			// 
			this.cboAlarmTime.FormatString = "T";
			this.cboAlarmTime.FormattingEnabled = true;
			this.cboAlarmTime.Location = new System.Drawing.Point(76, 15);
			this.cboAlarmTime.Name = "cboAlarmTime";
			this.cboAlarmTime.Size = new System.Drawing.Size(121, 20);
			this.cboAlarmTime.TabIndex = 0;
			// 
			// lblWhen
			// 
			this.lblWhen.AutoSize = true;
			this.lblWhen.Location = new System.Drawing.Point(26, 18);
			this.lblWhen.Name = "lblWhen";
			this.lblWhen.Size = new System.Drawing.Size(44, 12);
			this.lblWhen.TabIndex = 1;
			this.lblWhen.Text = "When :";
			// 
			// btnSetAlarm
			// 
			this.btnSetAlarm.Location = new System.Drawing.Point(76, 53);
			this.btnSetAlarm.Name = "btnSetAlarm";
			this.btnSetAlarm.Size = new System.Drawing.Size(121, 23);
			this.btnSetAlarm.TabIndex = 2;
			this.btnSetAlarm.Text = "SetAlarm";
			this.btnSetAlarm.UseVisualStyleBackColor = true;
			this.btnSetAlarm.Click += new System.EventHandler(this.btnSetAlarm_Click);
			// 
			// lblNow
			// 
			this.lblNow.AutoSize = true;
			this.lblNow.Location = new System.Drawing.Point(26, 88);
			this.lblNow.Name = "lblNow";
			this.lblNow.Size = new System.Drawing.Size(31, 12);
			this.lblNow.TabIndex = 1;
			this.lblNow.Text = "Now";
			// 
			// clkNow
			// 
			this.clkNow.Enabled = true;
			this.clkNow.Interval = 1000;
			this.clkNow.Tick += new System.EventHandler(this.clkNow_Tick);
			// 
			// FrmAlarmTestDriver
			// 
			this.AcceptButton = this.btnSetAlarm;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(222, 114);
			this.Controls.Add(this.btnSetAlarm);
			this.Controls.Add(this.lblNow);
			this.Controls.Add(this.lblWhen);
			this.Controls.Add(this.cboAlarmTime);
			this.Name = "FrmAlarmTestDriver";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "AlarmTestDriver";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Alarm.AlarmComponent alarmComponent1;
		private System.Windows.Forms.ComboBox cboAlarmTime;
		private System.Windows.Forms.Label lblWhen;
		private System.Windows.Forms.Button btnSetAlarm;
		private System.Windows.Forms.Label lblNow;
		private System.Windows.Forms.Timer clkNow;
	}
}

