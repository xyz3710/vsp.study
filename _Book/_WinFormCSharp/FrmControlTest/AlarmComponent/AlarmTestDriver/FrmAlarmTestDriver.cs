using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AlarmTestDriver
{
	public partial class FrmAlarmTestDriver : Form
	{
		private bool _isSet;

		public FrmAlarmTestDriver()
		{
			InitializeComponent();
		}

		private void clkNow_Tick(object sender, EventArgs e)
		{
			if (_isSet == false)
			{
				if (cboAlarmTime.Items.Count == 10)
					cboAlarmTime.Items.Clear();

				cboAlarmTime.Items.Add(DateTime.Now.AddSeconds(3));
				cboAlarmTime.SelectedIndex = cboAlarmTime.Items.Count - 1;
			}

			lblNow.Text = DateTime.Now.ToString();
		}

		private void alarmComponent1_AlarmSounded(object sender, EventArgs e)
		{
			MessageBox.Show("Wake Up!!!");
			_isSet = false;
		}

		private void btnSetAlarm_Click(object sender, EventArgs e)
		{
			alarmComponent1.Alarm = (DateTime)cboAlarmTime.SelectedItem;
			_isSet = true;
		}
	}
}