using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Alarm
{
	public partial class AlarmComponent : Component
	{
		private DateTime _alarm;

		public event EventHandler AlarmSounded;

		public AlarmComponent()
		{
			InitializeComponent();
		}

		public AlarmComponent(IContainer container)
		{
			container.Add(this);

			InitializeComponent();
		}

		[Description("알람 시간을 설정합니다.")]
		public DateTime Alarm
		{
			get
			{
				return _alarm;
			}
			set
			{
				_alarm = value;
			}
		}

		private void clkAlarm_Tick(object sender, EventArgs e)
		{
			// 알람 시간과 3초 이하의 차이가 나는지 검사한다.
			int seconds = ((TimeSpan)(DateTime.Now - _alarm)).Seconds;

			if ((seconds >= 0) && (seconds <= 3))
			{
				_alarm = DateTime.MaxValue;	// 알람이 단 한번 표시 되도록 설정한다.

				if (AlarmSounded != null)
					AlarmSounded(this, EventArgs.Empty);
			}            	
		}
	}
}
