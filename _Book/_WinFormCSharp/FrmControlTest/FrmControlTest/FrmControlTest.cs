using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace ControlTest
{
	public partial class FrmControlTest : Form
	{
		public FrmControlTest()
		{
			InitializeComponent();

			InitListBox();
		}

		private void InitListBox()
		{
			Type type = this.GetType();

			foreach (PropertyInfo pi in type.GetProperties())
				lstTest.Items.Add(pi.Name);
		}

		private void lstTest_DrawItem(object sender, DrawItemEventArgs e)
		{
			// 배경을 그린다
			e.DrawBackground();

			// 기본 폰트를 얻는다.

			Font drawFont = e.Font;
			bool outFont = false;

			// 선택된 항목을 Italic으로 한다.
			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
			{
				outFont = true;
				drawFont = new Font(drawFont, FontStyle.Italic);
			}

			using (Brush brush = new SolidBrush(e.ForeColor))
            {
				ListBox listBox = sender as ListBox;
                
                if (listBox != null)
                {
                	  // 목록 상자의 항목을 그린다.
					e.Graphics.DrawString(listBox.Items[e.Index].ToString(), drawFont, new SolidBrush(e.ForeColor), e.Bounds);

					if (outFont == true)
						drawFont.Dispose();
                }
            }

			// 포커스 사각형(Focus Rectangle)을 그린다.
			e.DrawFocusRectangle();

			StringFormat format = new StringFormat();

			format.LineAlignment = StringAlignment.Center;
			format.Alignment = StringAlignment.Center;
			ControlPaint.DrawStringDisabled(e.Graphics, "--- StringDisabledEffect ---", Font, ForeColor, e.Bounds, format);

			// 크기 조절 할경우 사라진다.
			//Invalidate();
		}

		private void lstTest_MeasureItem(object sender, MeasureItemEventArgs e)
		{
			// 짝수번째에 위치한 모든 항목의 높이를 두배로 만든다.
			// 참고로 항목의 인텍스는 0부터 시작한다.
			if (e.Index % 2 == 0)
				e.ItemHeight *= 2;
		}

		private void FrmControlTest_Load(object sender, EventArgs e)
		{

			StringFormat format = new StringFormat();

			format.LineAlignment = StringAlignment.Center;
			format.Alignment = StringAlignment.Center;
			//ControlPaint.DrawStringDisabled(e);
		}
	}
}