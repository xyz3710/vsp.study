﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ControlTest
{
	public partial class EllipseLabel : Control
	{
		public EllipseLabel()
		{
			InitializeComponent();

			TextChanged += new EventHandler(EllipseLabel_TextChanged);
		}

		void EllipseLabel_TextChanged(object sender, EventArgs e)
		{
			Invalidate();
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			Graphics graphics = pe.Graphics;

			using (Brush foreBrush = new SolidBrush(ForeColor))
            {
            	using (Brush backBrush = new SolidBrush(BackColor))
                {
					graphics.FillEllipse(foreBrush, pe.ClipRectangle);

					StringFormat format = new StringFormat();

					format.Alignment = StringAlignment.Center;
					format.LineAlignment = StringAlignment.Center;

					graphics.DrawString(Text, Font, backBrush, pe.ClipRectangle, format);
                }
            }             
			
			// 기본 클래스 OnPaint를 호출하고 있습니다.
			base.OnPaint(pe);
		}
	}
}
