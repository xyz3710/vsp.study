﻿namespace ControlTest
{
	partial class FrmControlTest
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.lstTest = new System.Windows.Forms.ListBox();
			this.lblTest = new System.Windows.Forms.Label();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.AutoScroll = true;
			this.splitContainer1.Panel1.Controls.Add(this.tlpMain);
			this.splitContainer1.Panel1MinSize = 100;
			this.splitContainer1.Panel2MinSize = 100;
			this.splitContainer1.Size = new System.Drawing.Size(1003, 594);
			this.splitContainer1.SplitterDistance = 565;
			this.splitContainer1.TabIndex = 2;
			// 
			// tlpMain
			// 
			this.tlpMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tlpMain.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.3992F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.6008F));
			this.tlpMain.Controls.Add(this.lstTest, 0, 0);
			this.tlpMain.Controls.Add(this.lblTest, 0, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Size = new System.Drawing.Size(565, 594);
			this.tlpMain.TabIndex = 3;
			// 
			// lstTest
			// 
			this.lstTest.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstTest.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
			this.lstTest.FormattingEnabled = true;
			this.lstTest.Location = new System.Drawing.Point(5, 5);
			this.lstTest.Name = "lstTest";
			this.lstTest.Size = new System.Drawing.Size(275, 288);
			this.lstTest.TabIndex = 0;
			// 
			// lblTest
			// 
			this.lblTest.AutoSize = true;
			this.lblTest.Location = new System.Drawing.Point(5, 298);
			this.lblTest.Name = "lblTest";
			this.lblTest.Size = new System.Drawing.Size(130, 12);
			this.lblTest.TabIndex = 1;
			this.lblTest.Text = "OnwerDraw test Label";
			// 
			// FrmControlTest
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1003, 594);
			this.Controls.Add(this.splitContainer1);
			this.Name = "FrmControlTest";
			this.Text = "ControlTestForm";
			this.Load += new System.EventHandler(this.FrmControlTest_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.ListBox lstTest;
		private System.Windows.Forms.Label lblTest;

	}
}

