﻿namespace ClockTestDriver
{
	partial class ClockTestDriver
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.clockControl1 = new ClockControl.ClockControl();
			this.lblTimeZone1Hour = new System.Windows.Forms.Label();
			this.lblTimeZone3Hour = new System.Windows.Forms.Label();
			this.lblTimeZone5Hour = new System.Windows.Forms.Label();
			this.lblTimeZone7Hour = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.clockControl1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::ClockTestDriver.Properties.Resources._1;
			this.pictureBox1.Location = new System.Drawing.Point(22, 142);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(100, 200);
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = global::ClockTestDriver.Properties.Resources._2;
			this.pictureBox2.Location = new System.Drawing.Point(122, 142);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(100, 200);
			this.pictureBox2.TabIndex = 3;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = global::ClockTestDriver.Properties.Resources._3;
			this.pictureBox3.Location = new System.Drawing.Point(222, 142);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(100, 200);
			this.pictureBox3.TabIndex = 3;
			this.pictureBox3.TabStop = false;
			// 
			// pictureBox4
			// 
			this.pictureBox4.Image = global::ClockTestDriver.Properties.Resources._4;
			this.pictureBox4.Location = new System.Drawing.Point(322, 142);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(100, 200);
			this.pictureBox4.TabIndex = 3;
			this.pictureBox4.TabStop = false;
			// 
			// clockControl1
			// 
			this.clockControl1.Alarm = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
			this.clockControl1.BackupAlarm = new System.DateTime(2007, 9, 28, 9, 13, 10, 31);
			this.clockControl1.BorderHand = new ClockControl.Hand(System.Drawing.Color.Yellow, 2);
			this.clockControl1.ClockFace = ClockControl.ClockFace.Analog;
			this.clockControl1.DigitalTimeFormat = "mm-dd-yyyy mm:ss:hh";
			this.clockControl1.HostingForm = this;
			this.clockControl1.Location = new System.Drawing.Point(7, 12);
			this.clockControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.clockControl1.Name = "clockControl1";
			this.clockControl1.PrimaryAlarm = new System.DateTime(2007, 9, 28, 9, 13, 9, 31);
			this.clockControl1.Size = new System.Drawing.Size(430, 124);
			this.clockControl1.TabIndex = 4;
			// 
			// lblTimeZone1Hour
			// 
			this.lblTimeZone1Hour.AutoSize = true;
			this.lblTimeZone1Hour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTimeZone1Hour.Location = new System.Drawing.Point(22, 360);
			this.lblTimeZone1Hour.Name = "lblTimeZone1Hour";
			this.lblTimeZone1Hour.Size = new System.Drawing.Size(143, 14);
			this.lblTimeZone1Hour.TabIndex = 5;
			this.lblTimeZone1Hour.Text = "2007-09-28 오전 10:13:09";
			this.clockControl1.SetTimeZoneModifier(this.lblTimeZone1Hour, 1);
			// 
			// lblTimeZone3Hour
			// 
			this.lblTimeZone3Hour.AutoSize = true;
			this.lblTimeZone3Hour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTimeZone3Hour.Location = new System.Drawing.Point(22, 381);
			this.lblTimeZone3Hour.Name = "lblTimeZone3Hour";
			this.lblTimeZone3Hour.Size = new System.Drawing.Size(143, 14);
			this.lblTimeZone3Hour.TabIndex = 5;
			this.lblTimeZone3Hour.Text = "2007-09-28 오후 12:13:09";
			this.clockControl1.SetTimeZoneModifier(this.lblTimeZone3Hour, 3);
			// 
			// lblTimeZone5Hour
			// 
			this.lblTimeZone5Hour.AutoSize = true;
			this.lblTimeZone5Hour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTimeZone5Hour.Location = new System.Drawing.Point(22, 402);
			this.lblTimeZone5Hour.Name = "lblTimeZone5Hour";
			this.lblTimeZone5Hour.Size = new System.Drawing.Size(137, 14);
			this.lblTimeZone5Hour.TabIndex = 5;
			this.lblTimeZone5Hour.Text = "2007-09-28 오후 2:13:09";
			this.clockControl1.SetTimeZoneModifier(this.lblTimeZone5Hour, 5);
			// 
			// lblTimeZone7Hour
			// 
			this.lblTimeZone7Hour.AutoSize = true;
			this.lblTimeZone7Hour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTimeZone7Hour.Location = new System.Drawing.Point(22, 423);
			this.lblTimeZone7Hour.Name = "lblTimeZone7Hour";
			this.lblTimeZone7Hour.Size = new System.Drawing.Size(137, 14);
			this.lblTimeZone7Hour.TabIndex = 5;
			this.lblTimeZone7Hour.Text = "2007-09-28 오후 4:13:09";
			this.clockControl1.SetTimeZoneModifier(this.lblTimeZone7Hour, 7);
			// 
			// ClockTestDriver
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(445, 470);
			this.Controls.Add(this.lblTimeZone7Hour);
			this.Controls.Add(this.lblTimeZone5Hour);
			this.Controls.Add(this.lblTimeZone3Hour);
			this.Controls.Add(this.lblTimeZone1Hour);
			this.Controls.Add(this.clockControl1);
			this.Controls.Add(this.pictureBox4);
			this.Controls.Add(this.pictureBox3);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Name = "ClockTestDriver";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "오전 9:52:08";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.clockControl1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.PictureBox pictureBox2;
		private ClockControl.ClockControl clockControl1;
		private System.Windows.Forms.Label lblTimeZone1Hour;
		private System.Windows.Forms.Label lblTimeZone7Hour;
		private System.Windows.Forms.Label lblTimeZone5Hour;
		private System.Windows.Forms.Label lblTimeZone3Hour;











	}
}

