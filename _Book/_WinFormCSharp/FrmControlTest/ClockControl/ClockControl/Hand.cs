using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Collections;
using System.Text.RegularExpressions;

namespace ClockControl
{
	[TypeConverter(typeof(HandConverter))]
	public class Hand
	{
		private Color _color;
		private int _width;

		public Hand()
		{
			_color = Color.Black;
			_width = 1;
		}

		/// <summary>
		/// Initializes a new instance of the Hand class.
		/// </summary>
		/// <param name="color"></param>
		/// <param name="width"></param>
		public Hand(Color color, int width)
		{
			_color = color;
			_width = width;
		}

		#region Properties
		/// <summary>
		/// Get or set Color.
		/// </summary>
		public Color Color
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
			}
		}

		/// <summary>
		/// Get or set Width.
		/// </summary>
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		#endregion

	}
}
