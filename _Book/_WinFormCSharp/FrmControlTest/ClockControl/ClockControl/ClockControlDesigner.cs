using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.ComponentModel.Design;

namespace ClockControl
{
	public class ClockControlDesigner : ControlDesigner
	{
		private ClockControl _clockControl;
		private bool _showBorder;
		private DesignerVerb _showBorderVerb;

		/// <summary>
		/// Initializes a new instance of the ClockControlDesigner class.
		/// </summary>
		public ClockControlDesigner()
		{
			_showBorder = true;
		}

		/// <summary>
		/// Get or set ShowBorder.
		/// </summary>
		public bool ShowBorder
		{
			get
			{
				return _showBorder;
			}
			set
			{
				// 속성 값을 변경한다.
				PropertyDescriptor property = TypeDescriptor.GetProperties(typeof(ClockControl))["ShowBorder"];

				RaiseComponentChanging(property);

				_showBorder = value;

				RaiseComponentChanged(property, !_showBorder, _showBorder);

				// 컨텍스트 메뉴에 있는 [Show/Hide Border] 명령 항목(verb item)을 토글한다.
				IMenuCommandService menuService = GetService(typeof(IMenuCommandService)) as IMenuCommandService;

				if (menuService != null)
				{
					// [Show/Hide Border] 명령 항목(verb item)을 다시 생성한다.
					if (menuService.Verbs.IndexOf(_showBorderVerb) >= 0)
					{
						menuService.Verbs.Remove(_showBorderVerb);

						_showBorderVerb = new DesignerVerb(GetVerbText(), new EventHandler(ShowBorderClicked));

						menuService.Verbs.Add(_showBorderVerb);
					}
				}
                
				_clockControl.Invalidate();
			}
		}

		// 디자인 타임에서만 테두리를 그려준다.
		public override void Initialize(System.ComponentModel.IComponent component)
		{
			base.Initialize(component);
			
			// 시계 컨트롤에 대한 참조를 얻는다
			_clockControl = component as ClockControl;
		}

		// 디자인 타임에서만 테두리를 그려준다.
		protected override void OnPaintAdornments(System.Windows.Forms.PaintEventArgs pe)
		{
			base.OnPaintAdornments(pe);

			// 아날로그 문자판이 아니면 테두리를 표시하지 않는다.
			if (_showBorder == false || (_clockControl.ClockFace == ClockFace.Digital))
				return;

			// 테두리를 그린다.
			Graphics graphics = pe.Graphics;

			using (Pen pen = new Pen(Color.Gray, 1))
            {
				pen.DashStyle = DashStyle.Dash;

				graphics.DrawRectangle(pen, 0, 0, _clockControl.Width - 1, _clockControl.Height - 1);
            }
		}

		// 속성창에서 디자인 타임에만 사용할 수 있도록 한다.
		protected override void PreFilterProperties(System.Collections.IDictionary properties)
		{
			base.PreFilterProperties(properties);

			// 디자인 타임에서만 허용되는 속성 항목을 생성하고
			// 이것을 속성 창의 디자인 카테고리에 추가한다.
			properties["ShowBorder"] = TypeDescriptor.CreateProperty(
				typeof(ClockControlDesigner),
				"ShowBorder",
				typeof(bool),
				CategoryAttribute.Design,
				DesignOnlyAttribute.Yes);
		}

		public override DesignerVerbCollection Verbs
		{
			get
			{
				// 컨텍스트 메뉴 항목의 새 목록을 반환한다
				DesignerVerbCollection verbs = new DesignerVerbCollection();

				_showBorderVerb = new DesignerVerb(GetVerbText(), new EventHandler(ShowBorderClicked));

				verbs.Add(_showBorderVerb);
                
				return verbs;
			}
		}

		public void ShowBorderClicked(object sender, EventArgs e)
		{
			// 속성 값을 토글한다.
			_showBorder = !_showBorder;
		}

		private string GetVerbText()
		{
			return "[Show/Hide Border]";
		}
	}
}
