/**********************************************************************************************************************/
/*	Domain		:	
/*	Creator		:	SUPERMOBILE\xyz37
/*	Create		:	2007년 9월 7일 금요일 오전 9:43
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	컴포넌트를 디버깅 할때는 
 *					프로젝트 옵션->디버그	
 *						시작 작업->시작 외부 프로그램 : C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe
 *						시작 옵션->명령줄 인수 : 테스트 드라이버 솔루션을 지정한다.
 *					컴포넌트 프로젝트를 시작프로젝트 로 설정한다
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Collections;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Drawing.Design;

namespace ClockControl
{
	[ToolboxBitmap(@"D:\VSP\FrmControlTest\ClockControl\ClockControl\clock_control.bmp")]
	[DefaultEvent("AlarmSounded")]
	[DefaultProperty("ShowAnalogTime")]
	[ProvideProperty("TimeZoneModifier", typeof(Control))]
	[Designer(typeof(ClockControlDesigner))]
	public partial class ClockControl : UserControl, ISupportInitialize, IExtenderProvider
	{
		private Timer _clkTimer;
		private Timer _clkAlarm;
		private DateTime _alarm;
		private bool _isItTimerForABreak;
		private bool _showAnalogTime;
		private Form _hostingForm;
		private DateTime _primaryAlarm;
		private DateTime _backupAlarm;
		private bool _initializing;
		private Hashtable _timeZoneModifiers;
		private ClockFace _clockFace;
		private Hand _borderHand;
		private string _digitalTimeFormat;

		public event EventHandler AlarmSounded;

		public ClockControl()
		{
			InitializeComponent();

			InitTimers();

			// 필요시 instance를 생성한다.
			_timeZoneModifiers = null;
			_clockFace = ClockFace.Digital;
			_digitalTimeFormat = "yyyy-mm-dd hh:mm:ss";
		}

		private void InitTimers()
		{
			_clkTimer = new Timer();

			_clkTimer.Interval = 1000;
			_clkTimer.Tick += new EventHandler(_clkTimer_Tick);
			_clkTimer.Start();

			_clkAlarm = new Timer();

			_clkAlarm.Interval = 1000;
			_clkAlarm.Tick += new EventHandler(_clkAlarm_Tick);
			_clkAlarm.Start();

			_primaryAlarm = DateTime.Now;
			_backupAlarm = DateTime.Now.AddSeconds(1);
			
			_borderHand = new Hand();
		}

		[Category("동작"), Description(""), Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public bool IsItTimerForABreak
		{
			get
			{
				return _isItTimerForABreak;
			}
			set
			{
				_isItTimerForABreak = value;
			}
		}

		[Category("동작"), Description("늦게 일어나는 사람들을 위한 알람")]
		[DefaultValue(null)]
		public DateTime Alarm
		{
			get
			{
				return _alarm;
			}
			set
			{
				_alarm = value;
			}
		}

		[Category("모양")]
		[Description("아나로그 시간으로 보일지 여부(아직 안된다 ㅡ.ㅡ;)")]
		[DefaultValue(false)]
		public bool ShowAnalogTime
		{
			get
			{
				return _showAnalogTime;
			}
			set
			{
				_showAnalogTime = value;
			}
		}

		private void ResetShowAnalogTime()
		{
			_showAnalogTime = false;
		}

		[Browsable(false)]
		public Form HostingForm
		{
			// 디자인타임에 InitializeComponent 메서드를 채우기 위해 사용한다.
			get
			{
				if (_hostingForm == null && DesignMode == true)
				{
					// 디자이너 호스트에 엑세스 하여 최상위 컴포넌트에 대한 참조를 얻는다.
					IDesignerHost designer = GetService(typeof(IDesignerHost)) as IDesignerHost;

					if (designer != null)
						_hostingForm = designer.RootComponent as Form;
				}

				return _hostingForm;
			}
			// 런타임에 InitializeComponent 메서드에 의해 설정된다.
			set
			{
				if (DesignMode == false)
				{
					// 런타임에 호스트 폼을 변경해선 안된다.
					if ((_hostingForm != null) && (_hostingForm != value))
						throw new InvalidOperationException("런타임에는 HostingForm을 지정할 수 없습니다.");
					else
						_hostingForm = value;
				}
			}
		}

		[Description("기본 알람")]
		public DateTime PrimaryAlarm
		{
			get
			{
				return _primaryAlarm;
			}
			set
			{
				//				if (value >= _backupAlarm)
				//                	throw new ArgumentOutOfRangeException("기본 알람은 백업 알람 전으로 설정되어 있어야 합니다.");

				if (_initializing == false)
					_primaryAlarm = value;
			}
		}

		[Description("백업 알람 - 잠에서 안깨는 사람을 위해 두었다.")]
		public DateTime BackupAlarm
		{
			get
			{
				return _backupAlarm;
			}
			set
			{
				//				if (value < _primaryAlarm)
				//                	throw new ArgumentOutOfRangeException("백업 알람은 기본 알람 뒤로 설정되어야 합니다..");
				if (_initializing == false)
					_backupAlarm = value;
			}
		}

		[Category("동작")]
		[Description("현재 시간과 다른 TimeZone을 세팅합니다.")]
		[DefaultValue(0)]
		public int GetTimeZoneModifier(Control extendee)
		{
			if (_timeZoneModifiers == null)
				_timeZoneModifiers = new Hashtable();
			
			// 해당 컴포넌트의 시간대 옵셋을 반환한다.
			return int.Parse(_timeZoneModifiers[extendee] == null ? "0" : _timeZoneModifiers[extendee].ToString());
		}

		public void SetTimeZoneModifier(Control extendee, object value)
		{
			if (_timeZoneModifiers == null)
				_timeZoneModifiers = new Hashtable();

			// 확장 속성을 사용하지 않는 경우
			if (value == null)
			{
				// 확장 속성을 제거 한다.
				_timeZoneModifiers.Remove(extendee);

				if (DesignMode == false)
					extendee.Click -= new EventHandler(extendee_Click);
			}
			else
			{
				// 정수 단위로 옵셋을 추가한다.
				_timeZoneModifiers[extendee] = int.Parse(value.ToString());

				// 확장 속성의 TimeZoneModifier에 따라서 시간을 더해준다.
				extendee.Text = DateTime.Now.AddHours(double.Parse(value.ToString())).ToString();

				if (DesignMode == false)
					extendee.Click += new EventHandler(extendee_Click);
			}            
		}

		[Category("모양")]
		[Description("시계의 모양을 결정합니다.")]
		[DefaultValue(typeof(ClockFace), "Digital")]
		[Editor(typeof(FaceEditor), typeof(UITypeEditor))]
		public ClockFace ClockFace
		{
			get
			{
				return _clockFace;
			}
			set
			{
				_clockFace = value;
			}
		}

		[Description("테두리 색과 두께를 조정할 수 있습니다.")]
		[TypeConverter(typeof(HandConverter))]		// class나 Property에 설정할 수 있다.
		public Hand BorderHand
		{
			get
			{
				elblNow.LineColor = _borderHand.Color;
				elblNow.LineWidth = _borderHand.Width;

				return _borderHand;
			}
			set
			{
				_borderHand = value;

				elblNow.LineColor = _borderHand.Color;
				elblNow.LineWidth = _borderHand.Width;
			}
		}

		private bool ShouldSerializeBorderHand()
		{
			// 기본 값이 아닌 값만 직렬화 한다.
			bool result = (_borderHand.Color != Color.Black) || (_borderHand.Width != 1);

			return result;
		}

		[Description("디지털 시계의 포맷입니다.")]
		[DefaultValue("yyyy-mm-dd hh:mm:ss")]
		[Editor(typeof(DigitalTimeFormatEditor), typeof(UITypeEditor))]
		public string DigitalTimeFormat
		{
			get
			{
				return _digitalTimeFormat;
			}
			set
			{
				_digitalTimeFormat = value;

				Invalidate();
			}
		}

		void extendee_Click(object sender, EventArgs e)
		{
			
		}

		void _clkTimer_Tick(object sender, EventArgs e)
		{
			// 아래 코드를 추가할 경우 DesignMode 에서는 시간이 보이지 않는다.
			#region For Test
			/*
				if (DesignMode == true)
					return;
			*/
			#endregion

			elblNow.Text = string.Format("{0}{1}{2}", DateTime.Now.ToShortDateString(), Environment.NewLine, DateTime.Now.ToLongTimeString());
			
			if (_hostingForm != null)
				_hostingForm.Text = DateTime.Now.ToLongTimeString();
		}

		private void _clkAlarm_Tick(object sender, EventArgs e)
		{
			// 알람 시간과 3초 이하의 차이가 나는지 검사한다.
			int seconds = ((TimeSpan)(DateTime.Now - _alarm)).Seconds;

			if ((seconds >= 0) && (seconds <= 3))
			{
				_alarm = DateTime.MaxValue;	// 알람이 단 한번 표시 되도록 설정한다.

				if (AlarmSounded != null)
					AlarmSounded(this, EventArgs.Empty);
			}
		}

		#region ISupportInitialize 멤버
		// InitializeComponent에서는 알파벳 순으로 속성이 정의 되므로 
		// Backup이 먼저 설정되고 Primary가 나중에 설정될 경우 동시 유효성 때문에 선, 후가 바뀌어서 항상 exception이 발생한다.
		// 이경우 유효성 중단을 하려면 ISupportInitialize를 구현해야 한다.
		public void BeginInit()
		{
			_initializing = true;
		} 

		public void EndInit()
		{
			if (_primaryAlarm >= _backupAlarm)
				throw new ArgumentOutOfRangeException("기본 알람은 백업 알람 전으로 설정되어야 합니다.");
		}

		#endregion

		#region IExtenderProvider 멤버

		public bool CanExtend(object extendee)
		{
			// 자기 자신을 확장할 수 없다.
			if (extendee == this)
				return false;

			// 적합한 컨트롤을 확장한다.
			bool canExtend = (extendee is PictureBox) || 
				(extendee is Panel) ||
				(extendee is TextBox) ||
				(extendee is Label);

			return canExtend;
		}

		#endregion

		
	}
}