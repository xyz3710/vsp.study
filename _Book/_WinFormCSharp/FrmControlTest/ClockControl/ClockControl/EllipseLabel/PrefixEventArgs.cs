using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace System.Windows.CustomControls
{
	public class PrefixEventArgs : EventArgs
	{
		private string _prefix;

		#region Properties
		/// <summary>
		/// Get or set Prefix.
		/// </summary>
		public string Prefix
		{
			get
			{
				return _prefix;
			}
			set
			{
				_prefix = value;
			}
		}

		#endregion
        
		public PrefixEventArgs(string prefix)
		{
			_prefix = prefix;
		}
	}
}
