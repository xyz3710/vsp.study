using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Collections;
using System.Text.RegularExpressions;

namespace ClockControl
{
	[Flags]
	public enum ClockFace
	{
		/// <summary>
		/// 아날로그
		/// </summary>
		Analog=0,
		/// <summary>
		/// 디지털
		/// </summary>
		Digital=1,
		/// <summary>
		/// 아날로그 + 디지털
		/// </summary>
		Both,
	}
}
