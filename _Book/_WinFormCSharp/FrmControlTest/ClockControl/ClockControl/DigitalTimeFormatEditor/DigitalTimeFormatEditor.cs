using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;

namespace ClockControl
{
	public class DigitalTimeFormatEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
		{
			if (context != null)
				return UITypeEditorEditStyle.Modal;
            
			return base.GetEditStyle(context);
		}

		public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if ((context != null) && (provider != null))
			{
				// 속성 창의 UI 출력 서비스에 엑세스 한다.
				IWindowsFormsEditorService editorService = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;

				if (editorService != null)
				{
					// UI 편집기 폼의 인스턴스를 생성한다.
					DigitalTimeFormatEditorForm modalEditor = new DigitalTimeFormatEditorForm();

					// 현재 속성 값을 UI 편집기 대화 상자에 넘긴다.
					modalEditor.DigitalTimeFormat = (string)value;

					// UI 편집기 대화 상자를 출력한다.
					if (editorService.ShowDialog(modalEditor) == DialogResult.OK)
                    	// UI 편집기 폼으로부터 새 속성 값을 반환한다.
						return modalEditor.DigitalTimeFormat;
				}                
			}				
            
			return base.EditValue(context, provider, value);
		}
	}
}
