﻿namespace ClockControl
{
	partial class DigitalTimeFormatEditorForm
	{
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 구성 요소 디자이너에서 생성한 코드

		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblFormatCaption = new System.Windows.Forms.Label();
			this.txtFormat = new System.Windows.Forms.TextBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.cboFormat = new System.Windows.Forms.ComboBox();
			this.lblExample = new System.Windows.Forms.Label();
			this.lblFormatExample = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(354, 74);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 3;
			this.btnOK.Text = "&OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(435, 74);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// lblFormatCaption
			// 
			this.lblFormatCaption.AutoSize = true;
			this.lblFormatCaption.Location = new System.Drawing.Point(22, 11);
			this.lblFormatCaption.Name = "lblFormatCaption";
			this.lblFormatCaption.Size = new System.Drawing.Size(52, 12);
			this.lblFormatCaption.TabIndex = 1;
			this.lblFormatCaption.Text = "Format :";
			// 
			// txtFormat
			// 
			this.txtFormat.Location = new System.Drawing.Point(80, 8);
			this.txtFormat.Name = "txtFormat";
			this.txtFormat.Size = new System.Drawing.Size(187, 21);
			this.txtFormat.TabIndex = 0;
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(273, 8);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(44, 23);
			this.btnAdd.TabIndex = 1;
			this.btnAdd.Text = "<<";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// cboFormat
			// 
			this.cboFormat.FormattingEnabled = true;
			this.cboFormat.Location = new System.Drawing.Point(323, 8);
			this.cboFormat.Name = "cboFormat";
			this.cboFormat.Size = new System.Drawing.Size(187, 20);
			this.cboFormat.TabIndex = 2;
			this.cboFormat.SelectedIndexChanged += new System.EventHandler(this.cboFormat_SelectedIndexChanged);
			// 
			// lblExample
			// 
			this.lblExample.AutoSize = true;
			this.lblExample.Location = new System.Drawing.Point(22, 50);
			this.lblExample.Name = "lblExample";
			this.lblExample.Size = new System.Drawing.Size(67, 12);
			this.lblExample.TabIndex = 1;
			this.lblExample.Text = "Example : ";
			// 
			// lblFormatExample
			// 
			this.lblFormatExample.AutoSize = true;
			this.lblFormatExample.Location = new System.Drawing.Point(95, 50);
			this.lblFormatExample.Name = "lblFormatExample";
			this.lblFormatExample.Size = new System.Drawing.Size(125, 12);
			this.lblFormatExample.TabIndex = 1;
			this.lblFormatExample.Text = "2007-09-01 오전 09:33";
			// 
			// DigitalTimeFormatEditorForm
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(520, 109);
			this.Controls.Add(this.cboFormat);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.txtFormat);
			this.Controls.Add(this.lblFormatExample);
			this.Controls.Add(this.lblExample);
			this.Controls.Add(this.lblFormatCaption);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DigitalTimeFormatEditorForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Digital Time Format Editor";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblFormatCaption;
		private System.Windows.Forms.TextBox txtFormat;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.ComboBox cboFormat;
		private System.Windows.Forms.Label lblExample;
		private System.Windows.Forms.Label lblFormatExample;
	}
}
