using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ClockControl
{
	public partial class DigitalTimeFormatEditorForm : Form
	{
		private string _digitalTimeFormat;
				
		public DigitalTimeFormatEditorForm()
		{
			InitializeComponent();

			_digitalTimeFormat = "yyyy-mm-dd hh:mm:ss";

			cboFormat.Items.Add(_digitalTimeFormat);
			cboFormat.Items.Add("yyyy-mm-dd hh24:mm:ss");
		}

		public string DigitalTimeFormat
		{
			get
			{
				return _digitalTimeFormat;
			}
			set
			{
				_digitalTimeFormat = value;
			}
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			_digitalTimeFormat = txtFormat.Text;
			DialogResult = DialogResult.OK;
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			if (cboFormat.SelectedIndex == 0)
			{
				txtFormat.Text = "yyyy-mm-dd hh:mm:ss";
				lblFormatExample.Text = "2007-09-01 ���� 09:33:10";

			}
			else if (cboFormat.SelectedIndex == 1)
			{
				txtFormat.Text = "yyyy-mm-dd hh24:mm:ss";
				lblFormatExample.Text = "2007-09-01 09:33:10";
			}

			btnOK.Select();
		}

		private void cboFormat_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cboFormat.SelectedIndex == 0)
				lblFormatExample.Text = "2007-09-01 ���� 09:33:10";
			else if (cboFormat.SelectedIndex == 1)
				lblFormatExample.Text = "2007-09-01 09:33:10";
		}
	}
}
