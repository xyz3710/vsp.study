using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.ComponentModel.Design.Serialization;
using System.Collections;
using System.Reflection;

namespace ClockControl
{
	/// <summary>
	/// Font와 같은 중첩 속성 창을 구현하려면 ExpandableObjectConverter를 상속 받아야 한다.
	/// </summary>
	public class HandConverter : ExpandableObjectConverter//TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			// string type을 Hand type으로 변환할 수 있다.
			if (sourceType == typeof(string))
				return true;

			return base.CanConvertFrom(context, sourceType);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			// InstanceDescriptor 개체로 변환될 수 있어야 한다.
			if (destinationType == typeof(InstanceDescriptor))
				return true;
            	
			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			// 문자열을 변환할 수 있는지 검사한다.
			if (value is string)
			{
				// Hand type을 만든다
				try
				{
					// Hand 속성을 얻는다
					string propertyList = (string)value;
					string[] properties = propertyList.Split(';');

					return new Hand(Color.FromName(properties[0].Trim()), 
						int.Parse(properties[1]));					
				}
				catch
				{
					throw new ArgumentException("인자값이 유효하지 않습니다.");
				}
			}

			return base.ConvertFrom(context, culture, value);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			// 원래 값이 Hand 타입 개체인지 검사한다.
			if (value is Hand)
			{
				// InstanceDescriptor 개체로 변환한다.
				if (destinationType == typeof(InstanceDescriptor))
				{
					Hand hand = value as Hand;
					object[] properties = new object[2];
					Type[] types = new Type[2];

					// Color
					types[0] = typeof(Color);
					properties[0] = hand.Color;

					// Width
					types[1] = typeof(int);
					properties[1] = hand.Width;

					// 생성자를 만든다
					ConstructorInfo ci = typeof(Hand).GetConstructor(types);

					return new InstanceDescriptor(ci, properties);
				}

				// 문자열로 변환한다
				if (destinationType == typeof(string))
				{
					Hand hand = (Hand)value;
                    string color = hand.Color.IsNamedColor ?
						hand.Color.Name :
						string.Format("{0}, {1}, {2}", hand.Color.R, hand.Color.G, hand.Color.B);

					return string.Format("{0}; {1}", color, hand.Width.ToString());
				}
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
		{
			// 항상 새 인스턴스를 생성하도록 한다.
			return true;
			//return base.GetCreateInstanceSupported(context);
		}

		public override object CreateInstance(ITypeDescriptorContext context, System.Collections.IDictionary propertyValues)
		{
			// 새 인스턴스를 생성하기 위해 IDictionary 타입의 인스턴스를 사용한다.
			return new Hand((Color)propertyValues["Color"], (int)propertyValues["Width"]);
			//return base.CreateInstance(context, propertyValues);
		}
	}
}
