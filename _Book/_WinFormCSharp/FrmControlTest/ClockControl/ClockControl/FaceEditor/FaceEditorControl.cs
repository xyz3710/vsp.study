using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace ClockControl
{
	public partial class FaceEditorControl : UserControl
	{
		private ClockFace _clockFace;
		private IWindowsFormsEditorService _editorService;

		public FaceEditorControl(IWindowsFormsEditorService editorService)
		{
			InitializeComponent();

			_clockFace = ClockFace.Both;
			_editorService = editorService;
		}

		public ClockFace ClockFace
		{
			get
			{
				return _clockFace;
			}
			set
			{
				_clockFace = value;
			}
		}

		private void picBoth_Click(object sender, EventArgs e)
		{
			_clockFace = ClockFace.Both;

			// 선택된 값에 따라 UI 편집기를 닫는다.
			_editorService.CloseDropDown();
		}

		private void picDigital_Click(object sender, EventArgs e)
		{
			_clockFace = ClockFace.Digital;

			// 선택된 값에 따라 UI 편집기를 닫는다.
			_editorService.CloseDropDown();
		}

		private void picAnalog_Click(object sender, EventArgs e)
		{
			_clockFace = ClockFace.Analog;

			// 선택된 값에 따라 UI 편집기를 닫는다.
			_editorService.CloseDropDown();
		}
	}
}
