using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace ClockControl
{
	public class FaceEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
		{
			if (context != null)
				return UITypeEditorEditStyle.DropDown;
                        	
			return base.GetEditStyle(context);
		}

		public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if ((context != null) && (provider != null))
			{
				// 속성 창의 UI 출력 서비스에 엑세스 한다.
				IWindowsFormsEditorService editorService = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;

				if (editorService != null)
				{
					// UI 편집기 컨트롤의 인스턴스를 생성한다.
					FaceEditorControl dropDownEditor = new FaceEditorControl(editorService);

					// 현재 속성 값을 UI 편집기 컨트롤에 넘긴다.
					dropDownEditor.ClockFace = (ClockFace)value;

					// UI 편집기 컨트롤을 표시한다
					editorService.DropDownControl(dropDownEditor);

					// UI 편집기 컨트롤로부터 새 속성 값을 반환한다.

					return dropDownEditor.ClockFace;
				}
			}				
            
			return base.EditValue(context, provider, value);
		}
	}
}
