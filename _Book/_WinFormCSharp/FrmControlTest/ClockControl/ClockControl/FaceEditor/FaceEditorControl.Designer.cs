﻿namespace ClockControl
{
	partial class FaceEditorControl
	{
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 구성 요소 디자이너에서 생성한 코드

		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.picAnalog = new System.Windows.Forms.PictureBox();
			this.picDigital = new System.Windows.Forms.PictureBox();
			this.picBoth = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.picAnalog)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picDigital)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picBoth)).BeginInit();
			this.SuspendLayout();
			// 
			// picAnalog
			// 
			this.picAnalog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.picAnalog.Image = global::ClockControl.Properties.Resources.analog;
			this.picAnalog.Location = new System.Drawing.Point(200, 4);
			this.picAnalog.Margin = new System.Windows.Forms.Padding(0);
			this.picAnalog.Name = "picAnalog";
			this.picAnalog.Size = new System.Drawing.Size(100, 92);
			this.picAnalog.TabIndex = 0;
			this.picAnalog.TabStop = false;
			this.picAnalog.Click += new System.EventHandler(this.picAnalog_Click);
			// 
			// picDigital
			// 
			this.picDigital.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.picDigital.Image = global::ClockControl.Properties.Resources.digital;
			this.picDigital.Location = new System.Drawing.Point(100, 4);
			this.picDigital.Margin = new System.Windows.Forms.Padding(0);
			this.picDigital.Name = "picDigital";
			this.picDigital.Size = new System.Drawing.Size(100, 92);
			this.picDigital.TabIndex = 0;
			this.picDigital.TabStop = false;
			this.picDigital.Click += new System.EventHandler(this.picDigital_Click);
			// 
			// picBoth
			// 
			this.picBoth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.picBoth.Image = global::ClockControl.Properties.Resources.both;
			this.picBoth.Location = new System.Drawing.Point(0, 4);
			this.picBoth.Margin = new System.Windows.Forms.Padding(0);
			this.picBoth.Name = "picBoth";
			this.picBoth.Size = new System.Drawing.Size(100, 92);
			this.picBoth.TabIndex = 0;
			this.picBoth.TabStop = false;
			this.picBoth.Click += new System.EventHandler(this.picBoth_Click);
			// 
			// FaceEditorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.picAnalog);
			this.Controls.Add(this.picDigital);
			this.Controls.Add(this.picBoth);
			this.Name = "FaceEditorControl";
			this.Size = new System.Drawing.Size(300, 100);
			((System.ComponentModel.ISupportInitialize)(this.picAnalog)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picDigital)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picBoth)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox picBoth;
		private System.Windows.Forms.PictureBox picDigital;
		private System.Windows.Forms.PictureBox picAnalog;
	}
}
