﻿namespace Modal_Modaless
{
	partial class FrmTestDriver
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTestDriver));
			this.btnPropertiesDialog = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.txtAppName = new System.Windows.Forms.TextBox();
			this.lblAppName = new System.Windows.Forms.Label();
			this.lblAppPhone = new System.Windows.Forms.Label();
			this.lblLoanAmount = new System.Windows.Forms.Label();
			this.txtAppPhone = new System.Windows.Forms.TextBox();
			this.txtLoanAmount = new System.Windows.Forms.TextBox();
			this._infoProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this._toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.btnAccept = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this._infoProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// btnPropertiesDialog
			// 
			this.btnPropertiesDialog.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnPropertiesDialog.Location = new System.Drawing.Point(19, 141);
			this.btnPropertiesDialog.Name = "btnPropertiesDialog";
			this.btnPropertiesDialog.Size = new System.Drawing.Size(182, 23);
			this.btnPropertiesDialog.TabIndex = 0;
			this.btnPropertiesDialog.Text = "PropertiesDialog";
			this.btnPropertiesDialog.UseVisualStyleBackColor = true;
			this.btnPropertiesDialog.Click += new System.EventHandler(this.btnPropertiesDialog_Click);
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Location = new System.Drawing.Point(302, 141);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "&Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// txtAppName
			// 
			this.txtAppName.Location = new System.Drawing.Point(143, 19);
			this.txtAppName.Name = "txtAppName";
			this.txtAppName.Size = new System.Drawing.Size(234, 21);
			this.txtAppName.TabIndex = 1;
			this.txtAppName.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxValidating);
			// 
			// lblAppName
			// 
			this.lblAppName.Location = new System.Drawing.Point(17, 23);
			this.lblAppName.Name = "lblAppName";
			this.lblAppName.Size = new System.Drawing.Size(117, 12);
			this.lblAppName.TabIndex = 2;
			this.lblAppName.Text = "Application Name : ";
			this.lblAppName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAppPhone
			// 
			this.lblAppPhone.Location = new System.Drawing.Point(15, 61);
			this.lblAppPhone.Name = "lblAppPhone";
			this.lblAppPhone.Size = new System.Drawing.Size(117, 12);
			this.lblAppPhone.TabIndex = 2;
			this.lblAppPhone.Text = "Application Phone : ";
			this.lblAppPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblLoanAmount
			// 
			this.lblLoanAmount.Location = new System.Drawing.Point(17, 99);
			this.lblLoanAmount.Name = "lblLoanAmount";
			this.lblLoanAmount.Size = new System.Drawing.Size(117, 12);
			this.lblLoanAmount.TabIndex = 2;
			this.lblLoanAmount.Text = "Loan Amount : ";
			this.lblLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAppPhone
			// 
			this.txtAppPhone.Location = new System.Drawing.Point(143, 57);
			this.txtAppPhone.Name = "txtAppPhone";
			this.txtAppPhone.Size = new System.Drawing.Size(234, 21);
			this.txtAppPhone.TabIndex = 1;
			this.txtAppPhone.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxValidating);
			// 
			// txtLoanAmount
			// 
			this.txtLoanAmount.Location = new System.Drawing.Point(143, 95);
			this.txtLoanAmount.Name = "txtLoanAmount";
			this.txtLoanAmount.Size = new System.Drawing.Size(234, 21);
			this.txtLoanAmount.TabIndex = 1;
			this.txtLoanAmount.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxValidating);
			// 
			// _infoProvider
			// 
			this._infoProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this._infoProvider.ContainerControl = this;
			this._infoProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("_infoProvider.Icon")));
			// 
			// _errorProvider
			// 
			this._errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
			this._errorProvider.ContainerControl = this;
			// 
			// btnAccept
			// 
			this.btnAccept.Location = new System.Drawing.Point(214, 141);
			this.btnAccept.Name = "btnAccept";
			this.btnAccept.Size = new System.Drawing.Size(75, 23);
			this.btnAccept.TabIndex = 0;
			this.btnAccept.Text = "&Accept";
			this.btnAccept.UseVisualStyleBackColor = true;
			this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
			// 
			// FrmTestDriver
			// 
			this.AcceptButton = this.btnAccept;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(398, 184);
			this.Controls.Add(this.btnPropertiesDialog);
			this.Controls.Add(this.btnAccept);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.lblAppName);
			this.Controls.Add(this.txtLoanAmount);
			this.Controls.Add(this.txtAppPhone);
			this.Controls.Add(this.txtAppName);
			this.Controls.Add(this.lblAppPhone);
			this.Controls.Add(this.lblLoanAmount);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.HelpButton = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmTestDriver";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FrmTestDriver";
			this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.FrmTestDriver_HelpRequested);
			((System.ComponentModel.ISupportInitialize)(this._infoProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnPropertiesDialog;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TextBox txtAppName;
		private System.Windows.Forms.Label lblAppName;
		private System.Windows.Forms.Label lblAppPhone;
		private System.Windows.Forms.Label lblLoanAmount;
		private System.Windows.Forms.TextBox txtAppPhone;
		private System.Windows.Forms.TextBox txtLoanAmount;
		private System.Windows.Forms.ErrorProvider _infoProvider;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.ToolTip _toolTip;
		private System.Windows.Forms.Button btnAccept;
	}
}

