using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Modal_Modaless
{
	public partial class PropertiesDialog : Form
	{
		public event EventHandler Accept;

		public PropertiesDialog()
		{
			InitializeComponent();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			if (Accept != null)
				Accept(this, EventArgs.Empty);            
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}