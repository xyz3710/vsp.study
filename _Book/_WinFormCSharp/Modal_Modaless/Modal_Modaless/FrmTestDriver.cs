using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Modal_Modaless
{
	public partial class FrmTestDriver : Form
	{
		private const string TP_INSERT_APP_NAME = "이름을 넣어주세요";
		private const string TP_INSERT_APP_PHONE = "전화번호를 넣어주세요";
		private const string TP_LOAN_AMOUNT = "대출금을 입력합니다.";

		public FrmTestDriver()
		{
			InitializeComponent();

			_toolTip.SetToolTip(txtAppName, TP_INSERT_APP_NAME);
			_toolTip.SetToolTip(txtAppPhone, TP_INSERT_APP_PHONE);
			_toolTip.SetToolTip(txtLoanAmount, TP_LOAN_AMOUNT);

			InitControlInfo();
		}

		private void InitControlInfo()
		{
			foreach (Control control in GetAllControls())
			{
				string toolTip = _toolTip.GetToolTip(control);

				if (toolTip.Length == 0)
					continue;

				_infoProvider.SetError(control, toolTip);
			}
		}

		private Control[] GetAllControls()
		{
			ArrayList allControls = new ArrayList();
			Queue<Control.ControlCollection> queue = new Queue<Control.ControlCollection>();

			queue.Enqueue(Controls);

			while (queue.Count > 0)
			{
				Control.ControlCollection controls = queue.Dequeue();

				if (controls == null || controls.Count == 0)
					continue;

				foreach (Control control in controls)
				{
					allControls.Add(control);

					queue.Enqueue(control.Controls);
				}
			}

			return (Control[])allControls.ToArray(typeof(Control));
		}

		private void btnPropertiesDialog_Click(object sender, EventArgs e)
		{
			PropertiesDialog dlg = new PropertiesDialog();

			dlg.Accept += new EventHandler(dlg_Accept);

			dlg.Show();
		}

		void dlg_Accept(object sender, EventArgs e)
		{
			PropertiesDialog dlg = sender as PropertiesDialog;

			if (dlg != null)
				MessageBox.Show(string.Format("{0} Form 의 Accept button이 눌렸습니다.", dlg.Name));
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void txtAppName_Validating(object sender, CancelEventArgs e)
		{
			TextBox textBox = sender as TextBox;

			#region ErrorProvider만 이용
			/*
				if (appName != null)
				{
					#region For Test
					
						//if (appName.Text.Length == 0)
						//{
						//    MessageBox.Show("이름을 입력하세요", "Error");

						//    e.Cancel = true;
						//}
					
					#endregion

					string errorString = string.Empty;

					if (appName.Text.Length == 0)
					{
						errorString = TP_INSERT_APP_NAME;

						e.Cancel = true;
					}
					
					_infoProvider.SetError(appName, errorString);
				}            	
			*/
			#endregion

			TextBoxValidating(textBox, e);
		}

		private void btnAccept_Click(object sender, EventArgs e)
		{
			foreach (Control control in GetAllControls())
			{
				control.Select();

				if (Validate() == false)
				{
					DialogResult = DialogResult.None;

					break;
				}
			}

			DialogResult = DialogResult.OK;
			Close();
		}

		private void TextBoxValidating(object sender, CancelEventArgs e)
		{
			TextBox textBox = sender as TextBox;            
			
			if (textBox != null)
			{
				string toolTip = _toolTip.GetToolTip(textBox);

				if (textBox.Text.Length == 0)
				{
					// TextBox에 아무런 문자가 들어있지 않으면 오류를 표시한다.
					_errorProvider.SetError(textBox, toolTip);
					_infoProvider.SetError(textBox, null);

					e.Cancel = true;
				}
				else
				{
					// 텍스트 상자에 문자가 들어있으면 정보를 보여준다.
					_errorProvider.SetError(textBox, null);
					_infoProvider.SetError(textBox, toolTip);
				}
			}
		}

		private void FrmTestDriver_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			// 화면 좌표를 클라이언트 좌표로 변환한다
			Point point = PointToClient(hlpevent.MousePos);

			// 사용자가 어느 컨트롤을 클릭했는지 찾는다
			// 주의 : GetChildAtPoint 메서드는(그룹 상자 안에 있는 컨트롤처럼)
			// 다른 컨트롤에 포함된 컨트롤을 제대로 처리하지 못한다.
			Control controNeedingHelp = null;

			foreach (Control control in GetAllControls())
			{
				if (control.Bounds.Contains(point) == true)
				{
					controNeedingHelp = control;

					break;
				}
			}
			
			// 도움말을 보여준다
			string help = _toolTip.GetToolTip(controNeedingHelp);

			if (help.Length == 0)
				return;

			MessageBox.Show(help, "Help");

			hlpevent.Handled = true;
		}
	}
}