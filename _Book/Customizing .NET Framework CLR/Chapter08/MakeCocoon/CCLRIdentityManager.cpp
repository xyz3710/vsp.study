//
//
//
#include "stdafx.h"
#include "CCLRIdentityManager.h"

CCLRIdentityManager::CCLRIdentityManager()
{
	// use GetRealProcAddress to get a pointer to GetCLRIdentityManager
	HRESULT hr = GetRealProcAddress("GetCLRIdentityManager", (void **)&m_pIdentityManagerProc);
	assert(m_pIdentityManagerProc);

	// call GetCLRIdentityManager to get a pointer to ICLRAssemblyIdentityManager
	hr = (m_pIdentityManagerProc)(IID_ICLRAssemblyIdentityManager, (IUnknown **)&m_pIdentityManager);
	assert(m_pIdentityManager);
}

CCLRIdentityManager::~CCLRIdentityManager()
{
	assert(m_pIdentityManager);
	m_pIdentityManager->Release();
}


wchar_t *CCLRIdentityManager::GetBindingIdentityForFile(wchar_t *pszFileName)
{

	// Call first to get the required buffer size
	DWORD cbBuffer = 0;
	HRESULT hr = m_pIdentityManager->GetBindingIdentityFromFile(pszFileName,
		                                                      0,
															  NULL,
															  &cbBuffer);

	wchar_t *pBindingIdentity = (wchar_t *)malloc(cbBuffer*sizeof(wchar_t));

	// Call again to actually get the identity
	hr = m_pIdentityManager->GetBindingIdentityFromFile(pszFileName,
		                                                      0,
															  pBindingIdentity,
															  &cbBuffer);
	return pBindingIdentity;
}