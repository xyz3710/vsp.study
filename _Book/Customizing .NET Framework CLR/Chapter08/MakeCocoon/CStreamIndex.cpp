

#include "stdafx.h"
#include "CStreamIndex.h"

//
// CStreamIndexEntry
//
CStreamIndexEntry::CStreamIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity)
{
	// save copies of the strings
	DWORD cbStreamName = wcslen(pStreamName);
	DWORD cbIdentity  = wcslen(pBindingIdentity);

	m_pStreamName = (wchar_t *)malloc((cbStreamName+1)*sizeof(wchar_t));
	m_pBindingIdentity = (wchar_t *)malloc((cbIdentity+1)*sizeof(wchar_t));

	ZeroMemory(m_pStreamName,(cbStreamName+1)*sizeof(wchar_t));
	ZeroMemory(m_pBindingIdentity,(cbIdentity+1)*sizeof(wchar_t));

	wcsncpy(m_pStreamName, pStreamName, cbStreamName);
	wcsncpy(m_pBindingIdentity, pBindingIdentity, cbIdentity);

}

CStreamIndexEntry::~CStreamIndexEntry()
{
	// free the strings
	free(m_pStreamName);
	free(m_pBindingIdentity);
}


//
// CStreamIndex
//
CStreamIndex::CStreamIndex(IStorage *pRootStorage)
{
	assert(pRootStorage);
	m_pRootStorage = pRootStorage;
	m_pRootStorage->AddRef();
}

CStreamIndex::~CStreamIndex()
{
	// Free all the objects that represent stream entries
	vector<CStreamIndexEntry *>::iterator iter;

	for (iter = m_index.begin(); iter != m_index.end(); iter ++)
	{
		CStreamIndexEntry *pCurrentEntry = (CStreamIndexEntry *)*iter;
		delete pCurrentEntry;
	}

	m_pRootStorage->Release();
}


HRESULT CStreamIndex::AddIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity)
{
	// add a new instance of CStreamIndexEntry to the vector
	CStreamIndexEntry *pNewEntry = new CStreamIndexEntry(pStreamName, pBindingIdentity);

	m_index.push_back(pNewEntry);
	return S_OK;
}

HRESULT CStreamIndex::WriteStream()
{
	// Create the index stream
	IStream *pIndexStream = NULL;
	HRESULT hr = m_pRootStorage->CreateStream(L"_index", STGM_DIRECT | STGM_CREATE | STGM_WRITE | STGM_SHARE_EXCLUSIVE, 0, 0, &pIndexStream);
	assert(SUCCEEDED(hr));

	// Write the number of index entries
	ULONG ulSizeWritten = 0;
	DWORD dwSize = sizeof(DWORD);
	DWORD *pSize = (DWORD *)malloc(dwSize);

	*pSize = m_index.size();
	hr = pIndexStream->Write((void *)pSize, dwSize, &ulSizeWritten);
	assert(SUCCEEDED(hr));
	assert(ulSizeWritten == dwSize);

	vector<CStreamIndexEntry *>::iterator iter;

	// Iterate through the vector writing to the index for each entry
	for (iter = m_index.begin(); iter != m_index.end(); iter ++)
	{
		CStreamIndexEntry *pCurrentEntry = (CStreamIndexEntry *)*iter;

		// write the length of the identity name
		*pSize = wcslen(pCurrentEntry->GetBindingIdentity());
	
		hr = pIndexStream->Write((void *)pSize, dwSize, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwSize);

		// write the identity name itself
		DWORD dwStringBytes = (*pSize) * sizeof(wchar_t);
		hr = pIndexStream->Write(pCurrentEntry->GetBindingIdentity(), dwStringBytes, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwStringBytes);

		// write the length of the stream name
		*pSize = wcslen(pCurrentEntry->GetStreamName());
	
		hr = pIndexStream->Write((void *)pSize, dwSize, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwSize);

		// write the stream name itself
		dwStringBytes = (*pSize) * sizeof(wchar_t);
		hr = pIndexStream->Write(pCurrentEntry->GetStreamName(), dwStringBytes, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwStringBytes);

	}

	free(pSize);
	pIndexStream->Release();
	return S_OK;
}

