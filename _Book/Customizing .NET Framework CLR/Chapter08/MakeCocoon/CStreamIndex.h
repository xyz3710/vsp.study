//
//
//

class CStreamIndexEntry
{
public:
	CStreamIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity);
	virtual ~CStreamIndexEntry();

	const wchar_t *GetStreamName() { return m_pStreamName; }
	const wchar_t *GetBindingIdentity() { return m_pBindingIdentity; }

private:
	wchar_t *m_pStreamName;
	wchar_t *m_pBindingIdentity;
};

class CStreamIndex
{
public:
	CStreamIndex(IStorage *pRootStorage);
	virtual ~CStreamIndex();

	HRESULT AddIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity);
	HRESULT WriteStream();

private:
	IStorage *m_pRootStorage;
	vector<CStreamIndexEntry *> m_index;

};
