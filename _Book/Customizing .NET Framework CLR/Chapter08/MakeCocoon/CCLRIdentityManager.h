//
//
//

typedef HRESULT (__stdcall *CLRIdentityManagerProc)(REFIID, IUnknown **);

class CCLRIdentityManager
{
public:
	CCLRIdentityManager();
	virtual ~CCLRIdentityManager();

	wchar_t *GetBindingIdentityForFile(wchar_t *pszFileName);

private:
	CLRIdentityManagerProc       m_pIdentityManagerProc;
	ICLRAssemblyIdentityManager *m_pIdentityManager;
};
