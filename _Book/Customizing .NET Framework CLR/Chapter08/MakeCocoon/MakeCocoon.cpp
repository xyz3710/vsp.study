// 
// MakeCocoon.cpp
//
// Takes a directory of files and makes a "cocoon".  MakeCocoon.exe takes as input
// the main executable to wrap in the cocoon.  It streams that executable, plus
// all dlls in the same directory into an OLE Structured Storage file.
//

#include "stdafx.h"
#include "CStreamIndex.h"
#include "CCLRIdentityManager.h"


// Given an assembly file on disk, this function creates a stream under pRootStorage
// and writes the bytes of the assembly to that stream.  It also creates an entry
// in the index that maps the name of the new stream to the binding identity of
// the file it contains.
HRESULT CreateStreamForAssembly(IStorage *pRootStorage, CStreamIndex *pStreamIndex, LPWSTR pAssemblyFileName)
{
	// Make sure we can open the file
	HANDLE hFile = CreateFile(pAssemblyFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("Error opening file: %s\n", pAssemblyFileName);
		return E_FAIL;
	}

	wprintf(L"Creating Stream for Assembly in file: %s\n", pAssemblyFileName);

	// Get the file size so we know how many bytes to write to the OLE Structured Storage File
	DWORD dwSize = GetFileSize(hFile, NULL);

	// Map the file into memory
	HANDLE hFileMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, dwSize, NULL);
	PVOID pFile = MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);

	// Pull the file extension off the name so we're left with just the simple assembly name
	wchar_t wszSimpleAsmName[MAX_PATH];
	ZeroMemory(wszSimpleAsmName, MAX_PATH*2);
	wcsncpy(wszSimpleAsmName, pAssemblyFileName, wcslen(pAssemblyFileName)-4);

	// Create a stream in which to store our assembly
	IStream *pMainStream = NULL;
	HRESULT hr = pRootStorage->CreateStream(wszSimpleAsmName, STGM_DIRECT | STGM_CREATE | STGM_WRITE | STGM_SHARE_EXCLUSIVE, 0, 0, &pMainStream);
	assert(SUCCEEDED(hr));

	// Write the assembly into the stream
	ULONG ulSizeWritten = 0;
	hr = pMainStream->Write(pFile, dwSize, &ulSizeWritten);
	assert(SUCCEEDED(hr));
	assert(ulSizeWritten == dwSize);

	// clean up - release the Stream, Unmap the file and close handles.
	pMainStream->Release();
	UnmapViewOfFile(pFile);
	CloseHandle(hFileMapping);
	CloseHandle(hFile);

	// add an entry to the index for this stream
	CCLRIdentityManager *pIdentityManager = new CCLRIdentityManager();

	wchar_t *pBindingIdentity = pIdentityManager->GetBindingIdentityForFile(pAssemblyFileName);
	assert(pBindingIdentity);

	hr = pStreamIndex->AddIndexEntry(wszSimpleAsmName, pBindingIdentity);
	assert(SUCCEEDED(hr));

	free(pBindingIdentity);
	delete pIdentityManager;

	return hr;
}

// Create a Stream that holds a string.  We use this to write entry point data
// into the storage and to record the binding identity of the assembly containing
// the application's executable.
HRESULT CreateStreamForString(IStorage *pRootStorage, wchar_t *pszStreamName, wchar_t *pszString)
{
	wprintf(L"Creating String Stream containing: %s\n", pszString);

	// Create a stream in which to store the string
	IStream *pStringStream = NULL;
	HRESULT hr = pRootStorage->CreateStream(pszStreamName, STGM_DIRECT | STGM_CREATE | STGM_WRITE | STGM_SHARE_EXCLUSIVE, 0, 0, &pStringStream);
	assert(SUCCEEDED(hr));

	// Write the string to the stream
	ULONG ulSizeWritten = 0;
	DWORD dwSize = wcslen(pszString)*sizeof(wchar_t);
	hr = pStringStream->Write(pszString, dwSize, &ulSizeWritten);
	assert(SUCCEEDED(hr));
	assert(ulSizeWritten == dwSize);

	pStringStream->Release();

	return S_OK;
}


int wmain(int argc, wchar_t* argv[])
{
	// Make sure the correct number of arguments were passed.  
	if (argc != 3)
	{
		wprintf(L"Usage: MakeCocoon <exe file name> <name of type containing Main()>\n");
		return 0;
	}

	// Construct the file name for the cocoon.  I use the name of the exe 
	// minus ".exe" + the ".cocoon" extension
	wchar_t wszCocoonName[MAX_PATH];
	ZeroMemory(wszCocoonName, MAX_PATH*2);
	wcsncpy(wszCocoonName, argv[1], wcslen(argv[1])-4);
	wcscat(wszCocoonName, L".cocoon");

	// Create the Structured Storage File in which to store the assemblies
	wprintf(L"Creating Cocoon: %s\n", wszCocoonName);
	IStorage *pRootStorage = NULL;
	HRESULT hr = StgCreateDocfile(wszCocoonName, STGM_DIRECT | STGM_READWRITE | STGM_CREATE | STGM_SHARE_EXCLUSIVE, 0, &pRootStorage); 
	assert(SUCCEEDED(hr));

	// Create the index we'll use to map stream names to binding identities
	CStreamIndex *pStreamIndex = new CStreamIndex(pRootStorage);

	// Initialize and start the CLR.
	ICLRRuntimeHost *pCLR = NULL;
    hr = CorBindToRuntimeEx(
      L"v2.0.40903", 
	  L"wks",		
      STARTUP_CONCURRENT_GC, 
      CLSID_CLRRuntimeHost, 
      IID_ICLRRuntimeHost, 
      (PVOID*) &pCLR);

	assert(SUCCEEDED(hr));
	
	pCLR->Start();

	// Obtain an identity manager.  This is a helper class that wraps the
	// methods provided by ICLRAssemblyIdentityManager
	CCLRIdentityManager *pIdentityManager = new CCLRIdentityManager();

	// Get the binding identity for the application's executable.
	wchar_t *pExeIdentity = pIdentityManager->GetBindingIdentityForFile(argv[1]);
	assert(pExeIdentity);

	// Create a stream to hold the binding identity of the exe file
	hr = CreateStreamForString(pRootStorage, L"_exeBindingIdentity", pExeIdentity);
	assert(SUCCEEDED(hr));

	free(pExeIdentity);
	delete pIdentityManager;

	// Create a stream that contains the name of the type containing the application's
	// main() method.
	hr = CreateStreamForString(pRootStorage, L"_entryPoint", argv[2]);
	assert(SUCCEEDED(hr));

	// Create a stream for the exe file.  
    hr = CreateStreamForAssembly(pRootStorage, pStreamIndex, argv[1]);
	assert(SUCCEEDED(hr));

	// Loop through the current directory creating streams for all dependent assemblies
	wchar_t bCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, bCurrentDir);
	wcsncat(bCurrentDir, L"\\*",2);

	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFile(bCurrentDir, &fileData);

    while (FindNextFile(hFind, &fileData) != 0) 
	{
		// Determine if the file is a dll - ignore everything else
		wchar_t *pDllExtension = wcsstr(fileData.cFileName, L".dll");
		if (pDllExtension)
		{
			// Create a stream in our Compound File for the assembly
			hr = CreateStreamForAssembly(pRootStorage, pStreamIndex, fileData.cFileName);
			assert(SUCCEEDED(hr));
		}
    }

	// Write the index to the sructured storage file.  This creates the 
	// _index stream.
	pStreamIndex->WriteStream();

	// clean up
	delete pStreamIndex;
	FindClose(hFind);
	pRootStorage->Release();

	return 0;
}

