//
//
//

#include "stdafx.h"
#include "CStreamIndex.h"
#include "CCocoonAssemblyStore.h"
#include "CCocoonAssemblyManager.h"
#include "CCLRIdentityManager.h"

typedef HRESULT (__stdcall *CLRIdentityManagerProc)(REFIID, IUnknown **);

// The names of the assemblies we'd like the CLR to load
const wchar_t *wszNonHostAssemblies[] = 
{
	L"CocoonHostRuntime, PublicKeyToken=38c3b24e4a6ee45e",
	L"mscorlib, PublicKeyToken=b77a5c561934e089",
	L"System, PublicKeyToken=b77a5c561934e089",
};

CCocoonAssemblyManager::CCocoonAssemblyManager()
{
	m_cRef=0;
	m_pRootStorage = NULL;
	m_pAssemblyStore = NULL;
}

CCocoonAssemblyManager::CCocoonAssemblyManager(IStorage *pRootStorage)
{
	m_cRef=0;
	m_pAssemblyStore = NULL;

	m_pRootStorage = pRootStorage;
	assert(m_pRootStorage);
	m_pRootStorage->AddRef();
}

CCocoonAssemblyManager::~CCocoonAssemblyManager()
{
	if (m_pRootStorage) m_pRootStorage->Release();
	if (m_pAssemblyStore) delete m_pAssemblyStore;
}


//
// IHostAssemblyManager
//
        
HRESULT STDMETHODCALLTYPE CCocoonAssemblyManager::GetNonHostStoreAssemblies( 
										ICLRAssemblyReferenceList **ppReferenceList)
{
	CCLRIdentityManager *pIdentityClass = new CCLRIdentityManager();

	ICLRAssemblyIdentityManager *pIdentityInterface = pIdentityClass->GetCLRIdentityManager();

	DWORD dwCount = sizeof(wszNonHostAssemblies)/sizeof(wszNonHostAssemblies[0]);
	HRESULT hr = pIdentityInterface->GetCLRAssemblyReferenceList(wszNonHostAssemblies,
																 dwCount,
																 ppReferenceList);
	pIdentityInterface->Release();
	delete pIdentityClass;
	return S_OK;
}

        
HRESULT STDMETHODCALLTYPE CCocoonAssemblyManager::GetAssemblyStore( 
								IHostAssemblyStore **ppAssemblyStore)
{
	assert(!m_pAssemblyStore);

	m_pAssemblyStore = new CCocoonAssemblyStore(m_pRootStorage);

	*ppAssemblyStore = (IHostAssemblyStore *)m_pAssemblyStore;
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CCocoonAssemblyManager::GetHostApplicationPolicy(
								DWORD           dwPolicy,
								DWORD           dwAppDomainId,
								DWORD           *pcbBufferSize,
								BYTE            *pbBuffer)
{
    *pcbBufferSize = 0;
    return S_OK;
}

//
// IUnknown
//
HRESULT STDMETHODCALLTYPE CCocoonAssemblyManager::QueryInterface(const IID &iid,void **ppv)
{
	if (!ppv) return E_POINTER;
	*ppv=this;
	AddRef();
	return S_OK;
}

ULONG STDMETHODCALLTYPE CCocoonAssemblyManager::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

ULONG STDMETHODCALLTYPE CCocoonAssemblyManager::Release()
{
	if(InterlockedDecrement(&m_cRef) == 0){
		delete this;
		return 0;
	}
	return m_cRef;
}