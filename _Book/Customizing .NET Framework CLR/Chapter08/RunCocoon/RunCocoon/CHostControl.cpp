

#include "stdafx.h"
#include "CHostControl.h"
#include "CCLRIdentityManager.h"

const wchar_t *wszDomainNeutralAssemblies[] = {
	L"Microsoft.VisualBasic, PublicKeyToken=b03f5f7f11d50a3a",
	L"mscorlib, PublicKeyToken=b77a5c561934e089",
	L"System.Data, PublicKeyToken=b77a5c561934e089",
	L"System, PublicKeyToken=b77a5c561934e089",
	L"System.Xml, PublicKeyToken=b77a5c561934e089",
	L"Microsoft.VisualC, PublicKeyToken=b03f5f7f11d50a3a",
	L"CustomMarshalers, PublicKeyToken=b03f5f7f11d50a3a",
	L"System.Runtime.Remoting, PublicKeyToken=b77a5c561934e089",
	L"System.Runtime.Serialization.Formatters.Soap, PublicKeyToken=b03f5f7f11d50a3a",
	L"System.Security, PublicKeyToken=b03f5f7f11d50a3a",
	L"System.Web.Services, PublicKeyToken=b03f5f7f11d50a3a",
	L"System.Transactions, PublicKeyToken=b77a5c561934e089",
};

CHostControl::CHostControl()
{
	m_cRef=0;
	m_pHostMemoryManager            = NULL;
	m_pHostTaskManager              = NULL;
	m_pHostThreadpoolManager        = NULL;
	m_pHostIoCompletionManager      = NULL;
	m_pHostSyncManager              = NULL;
	m_pHostAssemblyManager          = NULL;
	m_pHostCrossAssemblyCallManager = NULL;
	m_pHostGCManager                = NULL;
	m_pHostSecurityManager          = NULL;
	m_pDefaultDomainDomainManager   = NULL;
}

CHostControl::CHostControl(IHostMemoryManager			 *pHostMemoryManager,
						   IHostTaskManager				 *pHostTaskManager,
						   IHostThreadpoolManager		 *pHostThreadpoolManager,
        				   IHostIoCompletionManager		 *pHostIoCompletionManager,
						   IHostSyncManager				 *pHostSyncManager,
						   IHostAssemblyManager			 *pHostAssemblyManager,
						   IHostCrossAssemblyCallManager *pHostCrossAssemblyCallManager,
						   IHostGCManager				 *pHostGCManager,
						   IHostSecurityManager			 *pHostSecurityManager)
{
	m_pDefaultDomainDomainManager = NULL;

	m_pHostMemoryManager = pHostMemoryManager;
	if (m_pHostMemoryManager) m_pHostMemoryManager->AddRef();

	m_pHostTaskManager = pHostTaskManager;
	if (m_pHostTaskManager) m_pHostTaskManager->AddRef();

	m_pHostThreadpoolManager = pHostThreadpoolManager;
	if (m_pHostThreadpoolManager) m_pHostThreadpoolManager->AddRef();

	m_pHostIoCompletionManager = pHostIoCompletionManager;
	if (m_pHostIoCompletionManager) m_pHostIoCompletionManager->AddRef();

	m_pHostSyncManager = pHostSyncManager;
	if (m_pHostSyncManager) m_pHostSyncManager->AddRef();
	
	m_pHostAssemblyManager = pHostAssemblyManager;
	if (m_pHostAssemblyManager) m_pHostAssemblyManager->AddRef();

	m_pHostCrossAssemblyCallManager = pHostCrossAssemblyCallManager;
	if (m_pHostCrossAssemblyCallManager) m_pHostCrossAssemblyCallManager->AddRef();

	m_pHostGCManager = pHostGCManager;
	if (m_pHostGCManager) m_pHostGCManager->AddRef();

	m_pHostSecurityManager = pHostSecurityManager;
	if (m_pHostSecurityManager) m_pHostSecurityManager->AddRef();
}

CHostControl::~CHostControl()
{
	if (m_pHostMemoryManager)				m_pHostMemoryManager->Release();
	if (m_pHostTaskManager)					m_pHostTaskManager->Release();
	if (m_pHostThreadpoolManager)			m_pHostThreadpoolManager->Release();
	if (m_pHostIoCompletionManager)			m_pHostIoCompletionManager->Release();
	if (m_pHostSyncManager)					m_pHostSyncManager->Release();
	if (m_pHostAssemblyManager)				m_pHostAssemblyManager->Release();
	if (m_pHostCrossAssemblyCallManager)	m_pHostCrossAssemblyCallManager->Release();
	if (m_pHostGCManager)					m_pHostGCManager->Release();
	if (m_pHostSecurityManager)				m_pHostSecurityManager->Release();
	if (m_pDefaultDomainDomainManager)     	m_pDefaultDomainDomainManager->Release();
}		

ICocoonDomainManager* CHostControl::GetDomainManagerForDefaultDomain()
{
	if (m_pDefaultDomainDomainManager) m_pDefaultDomainDomainManager->AddRef();
	return m_pDefaultDomainDomainManager;
}

//
// IHostControl
//
HRESULT STDMETHODCALLTYPE CHostControl::GetHostManager(REFIID riid,void **ppv)
{
	if ((riid == IID_IHostMemoryManager) && (m_pHostMemoryManager))
	{
		printf("HostControl returning a custom Memory Manager\n");
		*ppv = m_pHostMemoryManager;
		return S_OK;
	}

	if ((riid == IID_IHostTaskManager) && (m_pHostTaskManager))
	{
		printf("HostControl returning a custom Task Manager\n");
		*ppv = m_pHostTaskManager;
		return S_OK;
	}

	if ((riid == IID_IHostThreadpoolManager) && (m_pHostThreadpoolManager))
	{
		printf("HostControl returning a custom Threadpool Manager\n");
		*ppv = m_pHostThreadpoolManager;
		return S_OK;
	}

	if ((riid == IID_IHostIoCompletionManager) && (m_pHostIoCompletionManager))
	{
		printf("HostControl returning a custom IoCompletion Manager\n");
		*ppv = m_pHostIoCompletionManager;
		return S_OK;
	}

	if ((riid == IID_IHostSyncManager) && (m_pHostSyncManager))
	{
		printf("HostControl returning a custom Sync Manager\n");
		*ppv = m_pHostSyncManager;
		return S_OK;
	}

	if ((riid == IID_IHostAssemblyManager) && (m_pHostAssemblyManager))
	{
		printf("HostControl returning a custom Assembly Manager\n");
		*ppv = m_pHostAssemblyManager;
		return S_OK;
	}

	if ((riid == IID_IHostCrossAssemblyCallManager) && (m_pHostCrossAssemblyCallManager))
	{
		printf("HostControl returning a custom Cross Assembly Call Manager Manager\n");
		*ppv = m_pHostCrossAssemblyCallManager;
		return S_OK;
	}

	if ((riid == IID_IHostGCManager) && (m_pHostGCManager))
	{
		printf("HostControl returning a custom GC Manager\n");
		*ppv = m_pHostGCManager;
		return S_OK;
	}

	if ((riid == IID_IHostSecurityManager) && (m_pHostSecurityManager))
	{
		printf("HostControl returning a custom Security Manager\n");
		*ppv = m_pHostSecurityManager;
		return S_OK;
	}

	return E_NOINTERFACE;
}

HRESULT STDMETHODCALLTYPE CHostControl::GetDomainNeutralAssemblies(
                                   ICLRAssemblyReferenceList **ppReferenceList)
{
	CCLRIdentityManager *pIdentityClass = new CCLRIdentityManager();

	ICLRAssemblyIdentityManager *pIdentityInterface = pIdentityClass->GetCLRIdentityManager();

	DWORD dwCount = sizeof(wszDomainNeutralAssemblies)/sizeof(wszDomainNeutralAssemblies[0]);
	HRESULT hr = pIdentityInterface->GetCLRAssemblyReferenceList(wszDomainNeutralAssemblies,
																 dwCount,
																 ppReferenceList);
	pIdentityInterface->Release();
	delete pIdentityClass;
	return S_OK;
}

        
HRESULT STDMETHODCALLTYPE CHostControl::SetAppDomainManager( 
											DWORD dwAppDomainID,
											IUnknown *pUnkAppDomainManager)
{
	HRESULT hr = S_OK;

	// Save the pointer to the default domain for convenience.  We know it's the default domain
	// because it's the first time we're called.
	if (!m_pDefaultDomainDomainManager)
	{
		hr = pUnkAppDomainManager->QueryInterface(__uuidof(ICocoonDomainManager), (PVOID*) &m_pDefaultDomainDomainManager);
		assert(SUCCEEDED(hr));
		assert(m_pDefaultDomainDomainManager);
	}

	return hr;
}

//
// IUnknown
//
HRESULT STDMETHODCALLTYPE CHostControl::QueryInterface(const IID &iid,void **ppv)
{
	if (!ppv) return E_POINTER;
	*ppv=this;
	AddRef();
	return S_OK;
}

ULONG STDMETHODCALLTYPE CHostControl::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

ULONG STDMETHODCALLTYPE CHostControl::Release()
{
	if(InterlockedDecrement(&m_cRef) == 0){
		delete this;
		return 0;
	}
	return m_cRef;
}