//
//
//

class CStreamIndexEntry
{
public:
	CStreamIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity);
	virtual ~CStreamIndexEntry();

	const wchar_t *GetStreamName() { return m_pStreamName; }
	const wchar_t *GetBindingIdentity() { return m_pBindingIdentity; }
	IStream *GetStream(IStorage *pStorage);

private:
	wchar_t *m_pStreamName;
	wchar_t *m_pBindingIdentity;
	IStream *m_pStream;
};

class CStreamIndex
{
public:
	CStreamIndex(IStorage *pRootStorage);
	virtual ~CStreamIndex();

	HRESULT AddIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity);
	HRESULT WriteStream();
	HRESULT ReadStream();
	HRESULT DumpStream();
	HRESULT GetStreamForBindingIdentity(const wchar_t *pszBindingIdentity, UINT64 *pIndexEntry,
										IStream **ppStream);

private:
	IStorage *m_pRootStorage;
	vector<CStreamIndexEntry *> m_index;

};
