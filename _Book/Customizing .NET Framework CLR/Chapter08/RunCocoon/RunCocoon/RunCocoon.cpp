// RunCocoon.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CHostControl.h"
#include "CStreamIndex.h"
#include "CCocoonAssemblyStore.h"
#include "CCocoonAssemblyManager.h"


// Returns the contents of pszStreamName and returns it in pszString.
// This method is used to read the _exeBindingIdentity and _entryPoint streams
// which contain the binding identity of the assembly and the name of the type 
// containing the application�s entry point.
HRESULT GetStringFromStream(IStorage *pStorage, wchar_t *pszStreamName, wchar_t *pszString)
{

	IStream *pStream = NULL;
	HRESULT hr = pStorage->OpenStream(pszStreamName, 0, STGM_READ | STGM_DIRECT | STGM_SHARE_EXCLUSIVE, 0, &pStream);
	assert(SUCCEEDED(hr));

	// Determine how many bytes to read based on the size of the Stream
	STATSTG stats;
	pStream->Stat(&stats, STATFLAG_DEFAULT);

	// Read the bytes into pszString
	DWORD dwBytesRead = 0;
	hr = pStream->Read(pszString, stats.cbSize.LowPart, &dwBytesRead);
	assert(stats.cbSize.LowPart == dwBytesRead);
	assert(SUCCEEDED(hr));

	pStream->Release();

	return S_OK;
}

int wmain(int argc, wchar_t* argv[])
{
	HRESULT hr = S_OK;

	// Make sure a cocoon file was passed as a command line argument.
	if (argc != 2)
	{
		wprintf(L"Usage: RunCocoon <cocoon file name>\n");
		return 0;
	}

	// Open the cocoon using the Structured Storage API's
	IStorage *pRootStorage = NULL;
	hr = StgOpenStorage(argv[1], NULL, STGM_READ | STGM_DIRECT | STGM_SHARE_EXCLUSIVE, NULL, 0, &pRootStorage);

	if (!SUCCEEDED(hr))
	{
		wprintf(L"Error opening cocoon file: %s\n", argv[1]);
		return 0;
	}

	// Start the Whidbey version of the CLR.
	ICLRRuntimeHost *pCLR = NULL;
    hr = CorBindToRuntimeEx(
      L"v2.0.40903", 
	  L"wks",		
      STARTUP_CONCURRENT_GC, 
      CLSID_CLRRuntimeHost, 
      IID_ICLRRuntimeHost, 
      (PVOID*) &pCLR);

	assert(SUCCEEDED(hr));

    // Create an instance of CCocoonAssemblyManager.  This class contains our
	// implementation of the Assembly Loading Manager, specifically the
	// IHostAssemblyStore interface.  We pass the IStorage for the cocoon's root 
	// storage object to the constructor.  CCocoonAssemblyManager saves this
	// pointer and uses it later to load assemblies from the cocoon using
	// IHostAssemblyStore.
	CCocoonAssemblyManager *pAsmManager = new CCocoonAssemblyManager(pRootStorage);
	assert(pAsmManager);

	// Create a host control object that takes the new assembly loading manager.  The
	// CHostControl class implements IHostControl which the CLR calls at startup
	// to determine which managers we support.  In this case, we support just the
	// assembly loading manager.
	CHostControl *pHostControl = new CHostControl(NULL,
												  NULL,
												  NULL,
												  NULL,
												  NULL,
												  (IHostAssemblyManager *)pAsmManager,
												  NULL,
												  NULL,
												  NULL);

	// Tell the CLR about our Host Control object.  Remember that we must do this 
	// before calling ICLRRuntimeHost::Start
	hr = pCLR->SetHostControl((IHostControl *)pHostControl);
	assert(SUCCEEDED(hr));

	// Get the CLRControl object.  We use this to set our AppDomainManager
	ICLRControl *pCLRControl = NULL;
	hr = pCLR->GetCLRControl(&pCLRControl);
	assert(SUCCEEDED(hr));

	hr = pCLRControl->SetAppDomainManagerType(L"CocoonHostRuntime, Version=5.0.0.0, PublicKeyToken=38c3b24e4a6ee45e, Culture=neutral",
		                                 L"CocoonHostRuntime.CocoonDomainManager");
	assert(SUCCEEDED(hr));

	// Start the CLR
	hr = pCLR->Start();

	// Get the binding identity for the exe contained in the cocoon 
	wchar_t wszExeIdentity[MAX_PATH];
	ZeroMemory(wszExeIdentity, MAX_PATH*sizeof(wchar_t));
	hr = GetStringFromStream(pRootStorage, L"_exeBindingIdentity", wszExeIdentity);

	// Get the name of the type containing the application's main method 
	wchar_t wszEntryType[MAX_PATH];
	ZeroMemory(wszEntryType, MAX_PATH*sizeof(wchar_t));
	hr = GetStringFromStream(pRootStorage, L"_entryPoint", wszEntryType);

	// Launch the application using our domain manager
	ICocoonDomainManager *pDomainManager = pHostControl->GetDomainManagerForDefaultDomain();
	assert(pDomainManager);

	//hr = pDomainManager->Run(L"Simple, Version=10.0.0.0, PublicKeyToken=3d9829272b3b00b1, Culture=neutral", L"Simple.Class1");
	hr = pDomainManager->Run(wszExeIdentity, wszEntryType);
	assert(SUCCEEDED(hr));

	pDomainManager->Release();
	pCLRControl->Release();
	pHostControl->Release();
	return 0;
}

