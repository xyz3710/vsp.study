//
//
//

#ifndef CASMSTORE_INCLUDED
#define CASMSTORE_INCLUDED

class CCocoonAssemblyStore : public IHostAssemblyStore
{
public:
	
	// IHostAssemblyStore

	HRESULT STDMETHODCALLTYPE ProvideAssembly(
								AssemblyBindInfo *pBindInfo,
								UINT64           *pAssemblyId,
								UINT64           *pContext,
								IStream          **ppStmAssemblyImage,
								IStream          **ppStmPDB);
        

	HRESULT STDMETHODCALLTYPE ProvideModule(
								ModuleBindInfo *pBindInfo,
								DWORD          *pdwModuleId,
								IStream        **ppStmModuleImage,
								IStream        **ppStmPDB);

	// IUnknown 
	virtual HRESULT STDMETHODCALLTYPE	QueryInterface(const IID &iid, void **ppv);
	virtual ULONG STDMETHODCALLTYPE		AddRef();
	virtual ULONG STDMETHODCALLTYPE		Release();

	CCocoonAssemblyStore();
	CCocoonAssemblyStore(IStorage *pCocoonStorage);
	virtual ~CCocoonAssemblyStore();

private:
	long m_cRef;
	IStorage *m_pCocoonStorage;
	CStreamIndex *m_pStreamIndex;
};

#endif