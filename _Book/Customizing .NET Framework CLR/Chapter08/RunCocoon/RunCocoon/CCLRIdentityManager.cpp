//
//
//
#include "stdafx.h"
#include "CCLRIdentityManager.h"

CCLRIdentityManager::CCLRIdentityManager()
{
	// use GetRealProcAddress to get a pointer to GetCLRIdentityManager
	HRESULT hr = GetRealProcAddress("GetCLRIdentityManager", (void **)&m_pIdentityManagerProc);
}

CCLRIdentityManager::~CCLRIdentityManager()
{

}

ICLRAssemblyIdentityManager *CCLRIdentityManager::GetCLRIdentityManager()
{
	assert(m_pIdentityManagerProc);
	ICLRAssemblyIdentityManager *pIdentityManager = NULL;

	// call GetCLRIdentityManager to get a pointer to ICLRAssemblyIdentityManager
	HRESULT hr = (m_pIdentityManagerProc)(IID_ICLRAssemblyIdentityManager, (IUnknown **)&pIdentityManager);
	assert(pIdentityManager);
	assert(SUCCEEDED(hr));

	return pIdentityManager;
}

