#region Using directives

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

#endregion

namespace CocoonHostRuntime
{
	public interface ICocoonDomainManager
	{
		void Run(string assemblyName, string typeName);
	}

	public class CocoonDomainManager : AppDomainManager, ICocoonDomainManager
	{
		public override void InitializeNewDomain(AppDomainSetup appDomainInfo)
		{
			// Set the flags so that the unmanaged portion of our host gets notified
			// of our domain manager via IHostControl::SetAppDomainManager
			InitializationFlags = DomainManagerInitializationFlags.RegisterWithHost;
		}

		// Run the "main" method from <assemblyName>.<typeName>
		public void Run(string assemblyName, string typeName)
		{
			try
			{
				Assembly asm = Assembly.Load(assemblyName);
				Type t = asm.GetType(typeName, true, true);
				MethodInfo m = t.GetMethod("Main");
				m.Invoke(null, null);
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception executing entry point: " + e.Message);
			}

		}
	}

}
