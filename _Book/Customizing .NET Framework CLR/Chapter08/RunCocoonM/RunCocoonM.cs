using System;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Threading;
using CocoonHostRuntime;

namespace RunCocoonM
{

	class CCocoonHost
	{
		// Import the definitions for the helper routines from
		// CocoonReader.dll
		[ DllImport( "CocoonReader.dll",CharSet=CharSet.Unicode)]
		public static extern int CocoonOpenCocoon(string cocoonName, ref IntPtr pCocoon);

		[ DllImport( "CocoonReader.dll",CharSet=CharSet.Unicode)]
		public static extern int CocoonCloseCocoon(IntPtr pCocoon);

		[ DllImport( "CocoonReader.dll",CharSet=CharSet.Unicode)]
		public static extern int CocoonOpenStream(IntPtr pCocoon, string streamName, ref IntPtr pStream);

		[ DllImport( "CocoonReader.dll",CharSet=CharSet.Unicode)]
		public static extern int CocoonCloseStream(IntPtr pStream);

		[ DllImport( "CocoonReader.dll",CharSet=CharSet.Unicode)]
		public static extern int CocoonGetStreamSize(IntPtr pStream, ref int size);

		[ DllImport( "CocoonReader.dll",CharSet=CharSet.Unicode)]
		public static extern int CocoonGetStreamBytes(IntPtr pStream, IntPtr streamBytes);

		static Assembly AssemblyResolveHandler(Object sender, ResolveEventArgs e) 
		{
			// Get the name of the assembly we need to resolve from the 
			// event args.  We want just the simple text name.  If the name is
			// fully qualified we'll want just the portion before the comma.
			string simpleAssemblyName;
			int commaIndex = e.Name.IndexOf('.');

			if (commaIndex == -1)
				simpleAssemblyName = e.Name;
			else
				simpleAssemblyName = e.Name.Substring(0, commaIndex);

			// Retreive the cocoon from the Application Domain property
			IntPtr pCocoon = (IntPtr) Thread.GetDomain().GetData("Cocoon");

			// Open the stream for the assembly
			IntPtr pStream = IntPtr.Zero;
			CocoonOpenStream(pCocoon, simpleAssemblyName, ref pStream);

			// Call the helper dll to get number of bytes in the Stream we're
			// about to read.  We need the size so we can allocate the correct
			// number of bytes in our managed array.
			int size = 0;
			CocoonGetStreamSize(pStream, ref size);

			// Allocate enough memory to hold the contents of the entire stream
			IntPtr pBytes = Marshal.AllocHGlobal(size);

			// Read the assembly from the cocoon
			CocoonGetStreamBytes(pStream, pBytes);

			// Copy the bytes from unmanaged memory into our managed byte array.
			// We'll need the bytes in this format to call Assembly.Load
			byte[] assemblyBytes = new byte[size];
			Marshal.Copy(pBytes, assemblyBytes, 0 , size);

			// Free the unmanaged memory
			Marshal.FreeHGlobal(pBytes);

			// Close the stream
			CocoonCloseStream(pStream);

			// Load the assembly from the byte array and return it
			return Assembly.Load(assemblyBytes, null, null);
		}

		static string GetTypeNameString()
		{
			// Retreive the cocoon from the Application Domain property
			IntPtr pCocoon = (IntPtr) Thread.GetDomain().GetData("Cocoon");

			// Open the "_entryPoint" Stream
			IntPtr pStream = IntPtr.Zero;
			CocoonOpenStream(pCocoon, "_entryPoint", ref pStream);

			// Get the size of the Stream containing the main type name.  We need
			// to know so size so we can allocate the correct amount of space
			// to hold the name
			int size = 0;
			CocoonGetStreamSize(pStream, ref size);

			// Allocate enough space to hold the main type name
			IntPtr pBytes = Marshal.AllocHGlobal(size);

			// Read the main type name from the cocoon
			CocoonGetStreamBytes(pStream, pBytes);

			// Copy the Stream's contents from unmanaged memory into a managed
			// character array - then create a string from the character array.
			char[] typeNameChars = new char[size];
			Marshal.Copy(pBytes, typeNameChars, 0 , size);
			string typeName = new string(typeNameChars, 0, size/2);

			// Free the unmanaged memory
			Marshal.FreeHGlobal(pBytes);

			// Close the "MainTypeName" stream
			CocoonCloseStream(pStream);

			return typeName;
		}

		[STAThread]
		static void Main(string[] args)
		{
			// Make sure the name of a cocoon file was passed on the command line 
			if (args.Length != 1)
			{
				Console.WriteLine("Usage: RunCocoonM <cocoon file>");
				return;
			}

			// Open the cocoon file and store a pointer to it in 
			// an Application Domain property. We'll need this value in 
			// our AssemblyResolve event handler.
			IntPtr pCocoon = IntPtr.Zero;
			CocoonOpenCocoon(args[0], ref pCocoon);

			Thread.GetDomain().SetData("Cocoon", pCocoon);

			// strip off the .cocoon from the command line argument to get
			// the name of the assembly within the cocoon that contians the
			// Main method.
			int dotIndex = args[0].IndexOf('.');
			string assemblyName = args[0].Substring(0, dotIndex);

			// Get name of type containing the application's Main method
			// from the cocoon
			string typeName = CCocoonHost.GetTypeNameString();

			// set up the delegate for the assembly resolve event
			Thread.GetDomain().AssemblyResolve +=new ResolveEventHandler(CCocoonHost.AssemblyResolveHandler);

			// use CocoonRuntime to invoke Main
			CocoonDomainManager cdm = new CocoonDomainManager();
			cdm.Run(assemblyName, typeName);
	
			// Close the cocoon file
			CocoonCloseCocoon(pCocoon);
		}
	}
}
