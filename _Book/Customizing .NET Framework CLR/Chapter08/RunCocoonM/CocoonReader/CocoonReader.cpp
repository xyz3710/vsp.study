// 
// CocoonReader.cpp : Contains utilities used by RunCocoonM.exe to read 
// assemblies out of OLE Structured Storage cocoon files.
//

#include "stdafx.h"

// Opens a cocoon file given a name.  Each call to CocoonOpenCocoon must
// be matched by a call to CocoonCloseCocoon.
extern "C" __declspec(dllexport) HRESULT CocoonOpenCocoon(LPWSTR pszCocoonName, IStorage **pRootStorage)
{
	return StgOpenStorage(pszCocoonName, NULL, STGM_READ | STGM_DIRECT | STGM_SHARE_EXCLUSIVE, NULL, 0, pRootStorage);
}

// Closes the cocoon file by releasing the cocoon's root storage
extern "C" __declspec(dllexport) HRESULT CocoonCloseCocoon(IStorage *pRootStorage)
{
	if (pRootStorage) pRootStorage->Release();
	return S_OK;
}

// Opens a stream within a cocoon given a name. Each call to CocoonOpenStream must
// be matched by a call to CocoonCloseStream.
extern "C" __declspec(dllexport) HRESULT CocoonOpenStream(IStorage *pRootStorage, LPWSTR pszStreamName, IStream **pStream)
{
	return pRootStorage->OpenStream(pszStreamName, 0, STGM_READ | STGM_DIRECT | STGM_SHARE_EXCLUSIVE, 0, pStream);
}

// Closes a stream.
extern "C" __declspec(dllexport) HRESULT CocoonCloseStream(IStream *pStream)
{
	if (pStream) pStream->Release();
	return S_OK;
}

// Returns the size of a stream in bytes.
extern "C" __declspec(dllexport) HRESULT CocoonGetStreamSize(IStream *pStream, DWORD *pSize)
{
	assert(pStream);

	// Get the statistics for the stream - which includes the size.
	STATSTG stats;
	pStream->Stat(&stats, STATFLAG_DEFAULT);

	// return the size
	*pSize = stats.cbSize.LowPart;

	return S_OK;
}

// Returns the contents of the stream.  The caller is responsible
// for allocating and freeing the memory pointed to by pBytes.
extern "C" __declspec(dllexport) HRESULT CocoonGetStreamBytes(IStream *pStream, BYTE *pBytes)
{
	assert (pStream);

	// Get the number of bytes to read
	STATSTG stats;
	pStream->Stat(&stats, STATFLAG_DEFAULT);
	DWORD dwSize = stats.cbSize.LowPart;

	// Read from the Stream
	DWORD dwBytesRead = 0;
	pStream->Read(pBytes, dwSize, &dwBytesRead);
	assert (dwSize == dwBytesRead);

	return S_OK;
}
