// SvrHost.cpp : Defines the entry point for the console application.
//
//

#include "stdafx.h"
#include <mscoree.h>

// include the tlb for mscorlib for access to the default AppDomain through COM Interop
#import <mscorlib.tlb> raw_interfaces_only high_property_prefixes("_get","_put","_putref")
using namespace mscorlib;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc < 2)
	{
		printf("Usage: SvrHost <Managed Exe> [Arguments for Managed Exe]\n");
		return 0;
	}

    // Use the hosting interfaces from .Net Framework 1.1
	ICorRuntimeHost *pCLR = NULL;

	// Initialize the CLR.  Specify the Server build.  Make sure
    // version 1.1 is loaded.
    HRESULT hr = CorBindToRuntimeEx(
      L"v1.1.4322", 
	  L"svr",		
      NULL, 
      CLSID_CorRuntimeHost, 
      IID_ICorRuntimeHost, 
      (PVOID*) &pCLR);

	assert(SUCCEEDED(hr));

	// Start the CLR
	pCLR->Start();
  
	// Get a pointer to the default AppDomain
    _AppDomain *pDefaultDomain = NULL;
    IUnknown   *pAppDomainPunk = NULL;

    hr = pCLR->GetDefaultDomain(&pAppDomainPunk);
    assert(pAppDomainPunk); 

    hr = pAppDomainPunk->QueryInterface(__uuidof(_AppDomain), (PVOID*) &pDefaultDomain);
    assert(pDefaultDomain);

	// get the name of the exe to run
	long retCode = 0;
	BSTR asmName = SysAllocString(argv[1]);

	// Collect the command line arguments to the managed exe
	SAFEARRAY *psa = NULL;
    SAFEARRAYBOUND rgsabound[1];

    rgsabound[0].lLbound = 0;
    rgsabound[0].cElements = (argc - 2);
    psa = SafeArrayCreate(VT_BSTR, 1, rgsabound);
	assert(psa);

	for (int i = 2; i < argc; i++)
	{
		long idx[1];
		idx[0] = i-2;
		SafeArrayPutElement(psa, idx, SysAllocString(argv[i])); 	}

	// Run the managed exe in the default AppDomain
	hr = pDefaultDomain->ExecuteAssembly_3(asmName, NULL, psa, &retCode);
    assert(SUCCEEDED(hr));

	// clean up
	SafeArrayDestroy(psa);
	SysFreeString(asmName);
	pAppDomainPunk->Release();
	pDefaultDomain->Release();

	_tprintf(L"\nReturn Code: %d\n", retCode);
	return retCode;
}

