___________________________________________________________________

                            Readme for

               Customizing the Microsoft .NET Framework
                     Common Language Runtime


                       by Steven Pratschner

          Copyright (c) 2005 by Microsoft Corporation
        Portions copyright (c) 2005 by Steven Pratschner
                       All Rights Reserved

___________________________________________________________________



README CONTENTS
 - WHAT'S IN THIS COMPANION CONTENT?
 - SUPPORT INFORMATION



WHAT'S IN THIS COMPANION CONTENT?
=================================
This companion content contains the following items:

 - Sample code for the book
   Projects built with the October Technology Preview of
   Visual Studio 2005


 
SUPPORT INFORMATION
===================

Microsoft Learning Technical Support information
------------------------------------------------
Every effort has been made to ensure the accuracy of the book and
the contents of this companion content. Microsoft Press provides
corrections for books and companion content through the World Wide
Web at:

    http://www.microsoft.com/learning/support/

To connect directly to the Microsoft Knowledge Base and enter a
query regarding a question or issue that you may have, go to:

    http://www.microsoft.com/learning/support/search.asp

If you have comments, questions, or ideas regarding the book or
this companion content, or questions that are not answered by
querying the Knowledge Base, please send them to Microsoft Press
via e-mail to:

    mspinput@microsoft.com
    
or via postal mail to:

    Microsoft Press
    Attn:  Customizing the Microsoft .NET Framework Common
           Language Runtime Editor
    One Microsoft Way
    Redmond, WA  98052-6399

Please note that product support is not offered through the above
addresses. 



DISCLAIMER: THIRD-PARTY SOFTWARE OR LINKS TO THIRD-PARTY SITES

For the user's convenience, this companion content may include
third-party software or links to third-party sites.

Please note that these products are not under the control of
Microsoft Corporation and Microsoft is therefore not responsible for
their content, nor should their inclusion in this companion content
be construed as an endorsement of the product. Please check
third-party Web sites for the latest version of their software.
 


