
#include "stdafx.h"
#include <mscoree.h>

// Declare globals to hold function pointers to call to notify the shim when our 
// initialization of the CLR is beginning and ending.
FLockClrVersionCallback g_beginInit;
FLockClrVersionCallback g_endInit;

// This function is registered as the host callback provided to the CLR by LockClrVersion.
// The shim will call this function the first time it receives a request to run managed code.
STDAPI InitializeCLR()
{
	// Notify the CLR that initialization is beginning
	g_beginInit(); 
	
	// Initialize the CLR. 
	ICLRRuntimeHost *pCLR = NULL;
    HRESULT hr = CorBindToRuntimeEx(
      L"v2.0.40903", 
	  NULL,		
      NULL, 
      CLSID_CLRRuntimeHost, 
      IID_ICLRRuntimeHost, 
      (PVOID*) &pCLR);

	assert(SUCCEEDED(hr));

	// Start the CLR
	pCLR->Start();

	// Notify the CLR that intialization has completed.
	g_endInit();

	return S_OK;
}

int wmain(int argc, wchar_t* argv[])
{
    HRESULT hr = S_OK;

	// Call LockClrVersion so my InitializeCLR always get called to set up the runtime.  
	LockClrVersion(InitializeCLR, &g_beginInit, &g_endInit);

	// Initialize COM and create an instance of a managed type through COM Interop.  This 
	// will require the CLR to be loaded - InitializeCLR will be called.
	CoInitialize(NULL);

	CLSID clsid;
	hr = CLSIDFromProgID(L"System.Collections.SortedList", &clsid);
	assert(SUCCEEDED(hr));

	IUnknown *pUnk = NULL;
	hr = CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER, IID_IUnknown, (LPVOID *) &pUnk);
	assert(SUCCEEDED(hr));

	pUnk->Release();

	CoUninitialize();

	return 0;
}

