using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;

// Include the namespaces for the managed wrappers around
// the CLR debugging interfaces.
using Microsoft.Samples.Debugging.MdbgEngine;
using Microsoft.Samples.Debugging.CorPublish;
using Microsoft.Samples.Debugging.CorDebug;

namespace AppDomainViewer
{

	public class ADView : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.Button btnRefresh;

		private System.ComponentModel.Container components = null;

		// The background thread used to call the debugging interfaces
		private Thread m_corPubThread;

		// The list of tree nodes to display in the UI.  This list gets populated
		// in RefreshTreeView.
		private ArrayList m_rootNodes = new ArrayList();


		public ADView()
		{
			InitializeComponent();
			
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.treeView = new System.Windows.Forms.TreeView();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.SuspendLayout();

// 
// treeView
// 
			this.treeView.ImageIndex = -1;
			this.treeView.Location = new System.Drawing.Point(16, 16);
			this.treeView.Name = "treeView";
			this.treeView.SelectedImageIndex = -1;
			this.treeView.Size = new System.Drawing.Size(453, 427);
			this.treeView.TabIndex = 0;

// 
// btnRefresh
// 
			this.btnRefresh.Location = new System.Drawing.Point(489, 16);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(72, 32);
			this.btnRefresh.TabIndex = 2;
			this.btnRefresh.Text = "Refresh";
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);

// 
// ADView
// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(585, 458);
			this.Controls.Add(this.btnRefresh);
			this.Controls.Add(this.treeView);
			this.Name = "ADView";
			this.Text = "Application Domain Viewer";
			this.Load += new System.EventHandler(this.ADView_Load);
			this.ResumeLayout(false);
		}
		#endregion


		[STAThread]
		static void Main() 
		{
			Application.Run(new ADView());
		}

		private void ADView_Load(object sender, System.EventArgs e)
		{
			// Populate the tree view when the applications starts
			RefreshTreeView();
		}

		private void btnRefresh_Click(object sender, System.EventArgs e)
		{
			// Populate the tree view whenever the Refresh Button is clicked
			RefreshTreeView();
		}
		
		private void RefreshTreeView()
		{
			// The CLR Debugging Interfaces must be called from an MTA thread, but
			// ee're currently in an STA.  Start a new MTA thread from which to call
			// the debugging interfaces.
		   	m_corPubThread = new Thread(new ThreadStart(ThreadProc));
			m_corPubThread.IsBackground = true;
			m_corPubThread.ApartmentState = ApartmentState.MTA;
			m_corPubThread.Start();	
		}

		public void ThreadProc() 
		{
			try 
			{
				MethodInvoker mi = new MethodInvoker(this.UpdateProgress);
				
				// Create new instances of the managed debugging objects
				CorPublish cp = new CorPublish();
				MDbgEngine dbg = new MDbgEngine();
				
				m_rootNodes.Clear();

				// Enumerate the processes on the machine that are running 
				// managed code.
				foreach(CorPublishProcess cpp in cp.EnumProcesses())
				{
					// Skip this process - don't display information about the 
					// AppDomainViewer itself.
					if(System.Diagnostics.Process.GetCurrentProcess().Id!=cpp.ProcessId)  
				  	{
						// Create a node in the tree for the Process
				   		TreeNode procNode = new TreeNode(cpp.DisplayName);
				   		
							// Enumerate the domains within the process
   						   foreach(CorPublishAppDomain cpad in cpp.EnumAppDomains())
						   {

							// Create a node for the domain
						   	TreeNode domainNode = new TreeNode(cpad.Name);
						   	
							// We need to actually attach to the process in order
						    // to see information about the assemblies.
							dbg.Attach(cpp.ProcessId);
							try
							{
								// The debugging interfaces (at least for this task) are
								// centered on modules rather than assemblies.  So we enumerate
								// the modules and find out which assemblies they belong to.
								// In the general case, assemblies may contain multiple modules.
								// This code is simpler, however.  It assumes one module per
								// assembly, which may yield incorrect results in some cases.
								foreach(MDbgModule m in dbg.Processes.Active.Modules)								
								{
									CorAssembly ca = m.CorModule.Assembly;
									
									// Make sure we only include assemblies in this domain
									if (ca.AppDomain.Id == cpad.ID) 
									{
										// add a node for the assembly under the Domain node
										domainNode.Nodes.Add(new TreeNode(Path.GetFileNameWithoutExtension(ca.Name)));
									}
								}
							}
							finally
							{		
								// Detach from the proces and move on to the next one.
								dbg.Processes.Active.Detach().WaitOne();
							}
						   	
							// add the domain node under the process node
						   	procNode.Nodes.Add(domainNode);
						   }	
						   
						   m_rootNodes.Add(procNode);
					}		   
		
				}

				// "notify" the tree view control back on the UI thread that new 
				// data is available.
				this.BeginInvoke(mi);

			}
			//Thrown when the thread is interupted by the main thread - exiting the loop
			catch (ThreadInterruptedException) 
			{
				//Simply exit....
			}
			catch (Exception) 
			{
			}
		}

		// This method is called from the MTA thread when new data is 
		// available. 
		private void UpdateProgress() 
		{
			// Clear the tree, enumerate through the nodes of the array
			// and add them to the tree.  Each of the top level nodes represents
			// a process, with nested nodes for domains and assemblies. 
			// m_rootNodes is constructed in RefreshTreeView.
			treeView.Nodes.Clear();
			foreach(TreeNode node in m_rootNodes)
			{
			  treeView.Nodes.Add(node);
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
