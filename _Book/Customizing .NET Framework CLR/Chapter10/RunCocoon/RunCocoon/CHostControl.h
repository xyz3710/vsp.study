//
//
//

#ifndef CHOSTCONTROL_INCLUDED
#define CHOSTCONTROL_INCLUDED

class CHostControl : public IHostControl
{
public:
	
	// IHostControl
	HRESULT STDMETHODCALLTYPE GetHostManager( 
								REFIID riid,
								void **ppObject);

        
    HRESULT STDMETHODCALLTYPE SetAppDomainManager( 
								DWORD dwAppDomainID,
								IUnknown *pUnkAppDomainManager);

	HRESULT STDMETHODCALLTYPE GetDomainNeutralAssemblies(
                                   ICLRAssemblyReferenceList **ppReferenceList);

	// IUnknown 
	virtual HRESULT STDMETHODCALLTYPE	QueryInterface(const IID &iid, void **ppv);
	virtual ULONG STDMETHODCALLTYPE		AddRef();
	virtual ULONG STDMETHODCALLTYPE		Release();

	CHostControl();
	CHostControl(IHostMemoryManager				*pHostMemoryManager,
				 IHostTaskManager				*pHostTaskManager,
				 IHostThreadpoolManager			*pHostThreadpoolManager,
				 IHostIoCompletionManager		*pHostIoCompletionManager,
				 IHostSyncManager				*pHostSyncManager,
				 IHostAssemblyManager			*pHostAssemblyManager,
				 IHostCrossAssemblyCallManager	*pHostCrossAssemblyCallManager,
				 IHostGCManager					*pHostGCManager,
				 IHostSecurityManager			*pHostSecurityManager);

	virtual ~CHostControl();

	ICocoonDomainManager *GetDomainManagerForDefaultDomain();

private:
	long m_cRef;
	IHostMemoryManager				*m_pHostMemoryManager;
	IHostTaskManager				*m_pHostTaskManager;
	IHostThreadpoolManager			*m_pHostThreadpoolManager;
	IHostIoCompletionManager		*m_pHostIoCompletionManager;
	IHostSyncManager				*m_pHostSyncManager;
	IHostAssemblyManager			*m_pHostAssemblyManager;
	IHostCrossAssemblyCallManager	*m_pHostCrossAssemblyCallManager;
	IHostGCManager					*m_pHostGCManager;
	IHostSecurityManager			*m_pHostSecurityManager;
	ICocoonDomainManager            *m_pDefaultDomainDomainManager;

};

#endif