//
//
//

typedef HRESULT (__stdcall *CLRIdentityManagerProc)(REFIID, IUnknown **);

class CCLRIdentityManager
{
public:
	CCLRIdentityManager();
	virtual ~CCLRIdentityManager();

	ICLRAssemblyIdentityManager *GetCLRIdentityManager();

private:
	CLRIdentityManagerProc       m_pIdentityManagerProc;
};
