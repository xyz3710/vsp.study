

#include "stdafx.h"
#include "CStreamIndex.h"

//
// CStreamIndexEntry
//
CStreamIndexEntry::CStreamIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity)
{
	// save copies of the strings
	DWORD cbStreamName = wcslen(pStreamName);
	DWORD cbIdentity  = wcslen(pBindingIdentity);

	m_pStreamName = (wchar_t *)malloc((cbStreamName+1)*sizeof(wchar_t));
	m_pBindingIdentity = (wchar_t *)malloc((cbIdentity+1)*sizeof(wchar_t));

	ZeroMemory(m_pStreamName,(cbStreamName+1)*sizeof(wchar_t));
	ZeroMemory(m_pBindingIdentity,(cbIdentity+1)*sizeof(wchar_t));

	wcsncpy(m_pStreamName, pStreamName, cbStreamName);
	wcsncpy(m_pBindingIdentity, pBindingIdentity, cbIdentity);

	m_pStream = NULL;

}

CStreamIndexEntry::~CStreamIndexEntry()
{
	// free the strings
	free(m_pStreamName);
	free(m_pBindingIdentity);

	if (m_pStream) m_pStream->Release();
}

IStream *CStreamIndexEntry::GetStream(IStorage *pStorage)
{
	if (!m_pStream)
	{
		HRESULT hr = pStorage->OpenStream(m_pStreamName, 0, STGM_READ | STGM_DIRECT | STGM_SHARE_EXCLUSIVE, 0, &m_pStream);
		if (!SUCCEEDED(hr))
		{
			m_pStream = NULL;
		}
	}

	return m_pStream;
}


//
// CStreamIndex
//
CStreamIndex::CStreamIndex(IStorage *pRootStorage)
{
	assert(pRootStorage);
	m_pRootStorage = pRootStorage;
	m_pRootStorage->AddRef();
}

CStreamIndex::~CStreamIndex()
{
	// Free all the objects that represent stream entries
	vector<CStreamIndexEntry *>::iterator iter;

	for (iter = m_index.begin(); iter != m_index.end(); iter ++)
	{
		CStreamIndexEntry *pCurrentEntry = (CStreamIndexEntry *)*iter;
		delete pCurrentEntry;
	}

	m_pRootStorage->Release();
}


HRESULT CStreamIndex::AddIndexEntry(wchar_t *pStreamName, wchar_t *pBindingIdentity)
{
	// add a new instance of CStreamIndexEntry to the vector
	CStreamIndexEntry *pNewEntry = new CStreamIndexEntry(pStreamName, pBindingIdentity);

	m_index.push_back(pNewEntry);
	return S_OK;
}

HRESULT CStreamIndex::WriteStream()
{
	// Create the index stream
	IStream *pIndexStream = NULL;
	HRESULT hr = m_pRootStorage->CreateStream(L"_index", STGM_DIRECT | STGM_CREATE | STGM_WRITE | STGM_SHARE_EXCLUSIVE, 0, 0, &pIndexStream);
	assert(SUCCEEDED(hr));

	// Write the number of index entries
	ULONG ulSizeWritten = 0;
	DWORD dwSize = sizeof(DWORD);
	DWORD *pSize = (DWORD *)malloc(dwSize);

	*pSize = m_index.size();
	hr = pIndexStream->Write((void *)pSize, dwSize, &ulSizeWritten);
	assert(SUCCEEDED(hr));
	assert(ulSizeWritten == dwSize);

	vector<CStreamIndexEntry *>::iterator iter;

	// Iterate through the vector writing to the index for each entry
	for (iter = m_index.begin(); iter != m_index.end(); iter ++)
	{
		CStreamIndexEntry *pCurrentEntry = (CStreamIndexEntry *)*iter;

		// write the length of the identity name
		*pSize = wcslen(pCurrentEntry->GetBindingIdentity());
	
		hr = pIndexStream->Write((void *)pSize, dwSize, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwSize);

		// write the identity name itself
		DWORD dwStringBytes = (*pSize) * sizeof(wchar_t);
		hr = pIndexStream->Write(pCurrentEntry->GetBindingIdentity(), dwStringBytes, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwStringBytes);

		// write the length of the stream name
		*pSize = wcslen(pCurrentEntry->GetStreamName());
	
		hr = pIndexStream->Write((void *)pSize, dwSize, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwSize);

		// write the stream name itself
		dwStringBytes = (*pSize) * sizeof(wchar_t);
		hr = pIndexStream->Write(pCurrentEntry->GetStreamName(), dwStringBytes, &ulSizeWritten);
		assert(SUCCEEDED(hr));
		assert(ulSizeWritten == dwStringBytes);

	}

	free(pSize);
	pIndexStream->Release();
	return S_OK;
}

HRESULT CStreamIndex::ReadStream()
{
	// Open the index stream
	IStream *pIndexStream = NULL;
	HRESULT hr = m_pRootStorage->OpenStream(L"_index", 0, STGM_READ | STGM_DIRECT | STGM_SHARE_EXCLUSIVE, 0, &pIndexStream);
	assert(SUCCEEDED(hr));

	// See how many index entries there are.
	DWORD dwNumEntries = 0;
	DWORD dwBytesRead  = 0;
	hr = pIndexStream->Read((void *)&dwNumEntries, sizeof(DWORD),&dwBytesRead);
	assert(dwBytesRead == sizeof(DWORD));
	assert(SUCCEEDED(hr));

	// for each entry read the strings containing the name of the 
	// stream containing data for the assembly and the binding identity for
	// that assembly.
	for (int i=0; i < dwNumEntries; i++)
	{
		// Read the length of the binding identity string
		DWORD dwStringLength = 0;
		hr = pIndexStream->Read((void *)&dwStringLength, sizeof(DWORD),&dwBytesRead);
		assert(dwBytesRead == sizeof(DWORD));
		assert(SUCCEEDED(hr));

		// Allocate space for the binding identity, then read it
		DWORD dwStringBytes = dwStringLength * sizeof(wchar_t);
		wchar_t *pBindingIdentity = (wchar_t *)malloc(dwStringBytes +sizeof(wchar_t)); 
		ZeroMemory(pBindingIdentity, dwStringBytes+sizeof(wchar_t));

		hr = pIndexStream->Read((void *)pBindingIdentity, dwStringBytes,&dwBytesRead);
		assert(dwBytesRead == dwStringBytes);
		assert(SUCCEEDED(hr));

		// Read the length of the assembly name stream
		hr = pIndexStream->Read((void *)&dwStringLength, sizeof(DWORD),&dwBytesRead);
		assert(dwBytesRead == sizeof(DWORD));
		assert(SUCCEEDED(hr));

		// Read the stream name itself
		dwStringBytes = dwStringLength * sizeof(wchar_t);
		wchar_t *pStreamName = (wchar_t *)malloc(dwStringBytes+sizeof(wchar_t));
		ZeroMemory(pStreamName, dwStringBytes+sizeof(wchar_t));

		hr = pIndexStream->Read((void *)pStreamName, dwStringBytes,&dwBytesRead);
		assert(dwBytesRead == dwStringBytes);
		assert(SUCCEEDED(hr));

		// Add a new entry to the list
		CStreamIndexEntry *pNewEntry = new CStreamIndexEntry(pStreamName, pBindingIdentity);
		m_index.push_back(pNewEntry);

		free(pBindingIdentity);
		free(pStreamName);
	}
	
	pIndexStream->Release();
	return S_OK;
}

HRESULT CStreamIndex::DumpStream()
{
	wprintf(L"Index stream has %d entries\n", m_index.size());

	vector<CStreamIndexEntry *>::iterator iter;

	for (iter = m_index.begin(); iter != m_index.end(); iter ++)
	{
		CStreamIndexEntry *pCurrentEntry = (CStreamIndexEntry *)*iter;

		wprintf(L"Binding Identity: %s\n", pCurrentEntry->GetBindingIdentity());
		wprintf(L"Stream Name     : %s\n", pCurrentEntry->GetStreamName());
		wprintf(L"\n");
	}

	return S_OK;
}

HRESULT CStreamIndex::GetStreamForBindingIdentity(const wchar_t *pszBindingIdentity,
												  UINT64 *pIndexEntry,
												  IStream **ppStream)
{
	vector<CStreamIndexEntry *>::iterator iter;
	int i = 0;

	for (iter = m_index.begin(); iter != m_index.end(); iter ++)
	{
		CStreamIndexEntry *pCurrentEntry = (CStreamIndexEntry *)*iter;

		// if binding identities match, get the stream
		const wchar_t *pszIndexBindingIdentity = pCurrentEntry->GetBindingIdentity();

		if (wcscmp(pszIndexBindingIdentity, pszBindingIdentity) == 0)
		{
			// Got a match
			*ppStream = pCurrentEntry->GetStream(m_pRootStorage);
			*pIndexEntry = i;
			wprintf(L"ProvideAssembly found an assembly with binding identity!: %s\n", pszBindingIdentity);
			return S_OK;
		}
		
		i++;
		
	}
	*ppStream = NULL;
	return HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
}
