//
// Implementation of IHostAssemblyManager for the RunCocoon.exe host
//

#ifndef CASMMANAGER_INCLUDED
#define CASMMANAGER_INCLUDED

class CCocoonAssemblyManager : public IHostAssemblyManager
{
public:
	
	// Methods from IHostAssemblyManager
    HRESULT STDMETHODCALLTYPE GetNonHostStoreAssemblies( 
								ICLRAssemblyReferenceList **ppReferenceList);
        
	HRESULT STDMETHODCALLTYPE GetAssemblyStore( 
								IHostAssemblyStore **ppAssemblyStore);
	    
	HRESULT STDMETHODCALLTYPE GetHostApplicationPolicy(
								DWORD           dwPolicy,
								DWORD           dwAppDomainId,
								DWORD           *pcbBufferSize,
								BYTE   *pbBuffer);

	// Methods from IUnknown 
	virtual HRESULT STDMETHODCALLTYPE	QueryInterface(const IID &iid, void **ppv);
	virtual ULONG STDMETHODCALLTYPE		AddRef();
	virtual ULONG STDMETHODCALLTYPE		Release();

	CCocoonAssemblyManager();
	CCocoonAssemblyManager(IStorage  *pRootStorage);
	
	virtual ~CCocoonAssemblyManager();

private:
	long					    m_cRef;
	IStorage				    *m_pRootStorage;     // interface pointer to the root storage for the cocoon
	CCocoonAssemblyStore	    *m_pAssemblyStore;   // our custom assembly store implementation
};

#endif