//
//
//

#include "stdafx.h"
#include "CStreamIndex.h"
#include "CCocoonAssemblyStore.h"

static const int CocoonAssemblyHostContext = 5;

CCocoonAssemblyStore::CCocoonAssemblyStore()
{
	m_pCocoonStorage = NULL;
	m_cRef=0;
	m_pStreamIndex = NULL;
}

CCocoonAssemblyStore::CCocoonAssemblyStore(IStorage *pCocoonStorage)
{
	assert(pCocoonStorage);
	m_pCocoonStorage = pCocoonStorage;
	m_pCocoonStorage->AddRef();

	m_pStreamIndex = new CStreamIndex(pCocoonStorage);
	m_pStreamIndex->ReadStream();

	m_cRef=0;
}

CCocoonAssemblyStore::~CCocoonAssemblyStore()
{
	if (m_pCocoonStorage)   m_pCocoonStorage->Release();
	if (m_pStreamIndex) delete m_pStreamIndex;
}

//
// IHostAssemblyStore
//
HRESULT STDMETHODCALLTYPE CCocoonAssemblyStore::ProvideAssembly(
								AssemblyBindInfo *pBindInfo,
								UINT64           *pAssemblyId,
								UINT64           *pContext,
								IStream          **ppStmAssemblyImage,
								IStream          **ppStmPDB)
{
	assert(m_pCocoonStorage);
	wprintf(L"ProvideAssembly called for binding identity: %s\n", pBindInfo->lpPostPolicyIdentity);

	// Check to see if Administrator policy was applied.  If so, print an error
	// to the command line and return "file not found".  This will cause the 
	// execution of the cocoon to stop with an exception.
	if (pBindInfo->ePolicyLevel == ePolicyLevelAdmin)
	{
		wprintf(L"Administrator Version Policy is present that redirects: %s to %s .  Stopping execution\n", pBindInfo->lpReferencedIdentity, pBindInfo->lpPostPolicyIdentity);
		return HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
	}


	// The CStreamIndex class contains the contents of the _index stream.
	// We call this class to find and open the stream containing the
	// assembly described by AssemblyBindInfo.lpPostPolicyIdentity.  
	HRESULT hr = m_pStreamIndex->GetStreamForBindingIdentity(pBindInfo->lpPostPolicyIdentity, pAssemblyId, ppStmAssemblyImage);

    // Set the host context to indicate this assembly was loaded from a cocoon.  This data will be used in our application 
    // domain manager's ProvideAssemblyEvidence method to associate our custom evidence with the assembly.
	*pContext    = CocoonAssemblyHostContext;

    // We return no debugging information.
	*ppStmPDB    = NULL;

    return hr;
}

        
HRESULT STDMETHODCALLTYPE CCocoonAssemblyStore::ProvideModule(
								ModuleBindInfo *pBindInfo,
								DWORD          *pdwModuleId,
								IStream        **ppStmModuleImage,
								IStream        **ppStmPDB)
{
	return HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
}

//
// IUnknown
//
HRESULT STDMETHODCALLTYPE CCocoonAssemblyStore::QueryInterface(const IID &iid,void **ppv)
{
	if (!ppv) return E_POINTER;
	*ppv=this;
	AddRef();
	return S_OK;
}

ULONG STDMETHODCALLTYPE CCocoonAssemblyStore::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

ULONG STDMETHODCALLTYPE CCocoonAssemblyStore::Release()
{
	if(InterlockedDecrement(&m_cRef) == 0){
		delete this;
		return 0;
	}
	return m_cRef;
}