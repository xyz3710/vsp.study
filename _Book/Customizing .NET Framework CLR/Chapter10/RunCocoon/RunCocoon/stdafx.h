// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <assert.h>
#include <objbase.h>
#include <comdef.h>
#include <tchar.h>
#include <stdlib.h>
#include <crtdbg.h>


#include <mscoree.h>

// Import our Domain Manager - we use it to launch the application we're
// hosting.
#import "CocoonHostRuntime.tlb"
using namespace CocoonHostRuntime;

#include <vector>
using namespace std;
