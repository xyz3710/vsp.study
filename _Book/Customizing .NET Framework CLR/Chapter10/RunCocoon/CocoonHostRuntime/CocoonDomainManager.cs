#region Using directives

using System;
using System.Collections;
using System.Text;
using System.Reflection;
using System.Security;
using System.Security.Policy;
using System.Security.Permissions;
using System.Diagnostics;

#endregion

namespace CocoonHostRuntime
{

    // This class represents the security evidence that indicates a particular assembly was
    // loaded from a cocoon.  This evidence is evaluated as part of the CocoonMembershipCondition
    // in the policy attached to the application domain.  EvCocoon is an empty class - just its
    // presence in the list of evidence is enough to indicate an assembly came from a cocoon.
    [Serializable]
    public class EvCocoon
    {
    };

    public class CocoonMembershipCondition : IMembershipCondition
    {
        public bool Check(Evidence evidence)
        {
            if (evidence == null)
                return false;

            // loop through the evidence looking for an instance of
            // EvCocoon
            IEnumerator enumerator = evidence.GetHostEnumerator();
            while (enumerator.MoveNext())
            {
                Object obj = enumerator.Current;
                
                if (obj is EvCocoon)
                {
                    // We've found cocoon evidence!
                    return true;
                }
            }
            return false;
        }

        public IMembershipCondition Copy()
        {
            return new CocoonMembershipCondition();
        }

        public override bool Equals(Object o)
        {
            CocoonMembershipCondition that = (o as CocoonMembershipCondition);

            if (that != null)
            {
                return true;
            }
            return false;
        }

        // The Cocoon membership condition cannot be specified in security XML configuration files
        public SecurityElement ToXml(){ throw new NotSupportedException(); }
        public void FromXml(SecurityElement e){ throw new NotSupportedException(); }
        public SecurityElement ToXml(PolicyLevel level) { throw new NotSupportedException(); }
        public void FromXml(SecurityElement e, PolicyLevel level){ throw new NotSupportedException(); }
    };
    
    public class CocoonSecurityManager : HostSecurityManager
    {
        static int CocoonAssemblyHostContext = 5;
        private static byte[] s_msPublicKey = 
	    { 
	    	0,  36,   0,   0,   4, 128,   0,   0, 148,   0,   0,   0,   6,   2,   0,   0,   0,  36,   0,   0,  82,  83,  65,  49,   0,   4,   0,   0,   1,   0,   1,   0,   7, 209, 250,  87, 196, 174, 217, 240, 163,  46, 132, 170,  15, 174, 253,  13, 233, 232, 253, 106, 236, 143, 135, 251,   3, 118, 108, 131,  76, 153, 146,  30, 178,  59, 231, 154, 217, 213, 220, 193, 221, 154, 210,  54,  19,  33,   2, 144,  11, 114,  60, 249, 128, 149, 127, 196, 225, 119,  16, 143, 198,   7, 119,  79,  41, 232,  50,  14, 146, 234,   5, 236, 228, 232,  33, 192, 165, 239, 232, 241, 100,  92,  76,  12, 147, 193, 171, 153,  40,  93,  98,  44, 170, 101,  44,  29, 250, 214,  61, 116,  93, 111,  45, 229, 241, 126,  94, 175,  15, 196, 150,  61,  38,  28, 138,  18,  67, 101,  24,  32, 109, 192, 147,  52,  77,  90, 210, 147
	    };

        private static byte[] s_ecmaPublicKey = 
	    {
	        0,   0,   0,   0,   0,   0,   0,   0,   4,   0,   0,   0,   0,   0,   0,   0
	    };

        private static byte[] s_CocoonHostRuntimePublicKey =
		{
            0,  36,   0,   0,   4, 128,   0,   0, 148,   0,   0,   0,   6,   2,   0,   0,   0,  36,   0,   0,  82,  83,  65,  49,   0,   4,   0,   0,   1,   0,   1,   0, 241, 255, 223,  68, 103,  53,  57, 194,  68, 246,  41,  44, 219, 236, 159,  34, 224, 176, 134, 172, 137,  77,  26, 145, 228, 143, 130,  16,  75,  36, 135,  78, 188, 240,  60, 158, 191,  99, 180,  73, 195, 154,  43,  24, 231, 230,  59,  49, 123, 233,  45, 148,  56,   6, 192,  62, 100, 214,  15, 121,   2, 187, 167,  54, 124,  15, 222,  25, 189, 129, 195,  28, 141, 227, 254, 209, 189, 241,  48, 114, 192, 210, 132, 218,  80,  70, 248, 240, 163,  79, 121, 196,  44,  83,  64, 217,  55,  19,  31, 204, 104, 138,  91,  82, 208,  10,  72, 112, 214,  44, 127,  47, 186,  72,  80, 101, 227, 240, 184,  27, 181,  50, 137, 147, 173, 222, 101, 231
        };

        public override HostSecurityManagerFlags Flags
		{
			get
			{
				return (HostSecurityManagerFlags.HostAssemblyEvidence | HostSecurityManagerFlags.HostPolicyLevel);
			}
		}

		public override Evidence ProvideAssemblyEvidence(Assembly loadedAssembly, Evidence evidence)
		{
            if (loadedAssembly.HostContext == CocoonAssemblyHostContext)
            {
                // Add evidence that identfies this assembly as coming from the MyComputer zone.  Without this evidence,
                // the assembly would get no permissions at the machine level so our grant based on cocoon evidence
                // would get "cancelled out".  In essence, providing this evidence causes the assembly to get "FullTrust"
                // which we then lock back down through Cocoon evidence.
                evidence.AddHost(new Zone(SecurityZone.MyComputer));

                // Add an instance of the cocoon evidence class to the list of host evidence.  This evidence will cause
                // the check in CocoonMembershipCondition to pass during policy evaluation thereby granting the permissions
                // we specify in our app domain level policy.
                evidence.AddHost(new EvCocoon());
                Console.WriteLine("Evidence requested for cocoon assembly: " + loadedAssembly.FullName);
            }

            return evidence;
		}

        public override PolicyLevel DomainPolicy
        {
            get 
            {
                PolicyLevel pol = PolicyLevel.CreateAppDomainLevel();

                pol.RootCodeGroup.PolicyStatement = new PolicyStatement(new PermissionSet(PermissionState.None));

                // create membership condition for the MS platform key
                UnionCodeGroup msKeyCG =
                    new UnionCodeGroup(
                         new StrongNameMembershipCondition(new
                                StrongNamePublicKeyBlob(s_msPublicKey), null, null),
                    new PolicyStatement(new PermissionSet(PermissionState.Unrestricted)));

                pol.RootCodeGroup.AddChild(msKeyCG);

                // create membership condition for the ECMA key
                UnionCodeGroup ecmaKeyCG =
                    new UnionCodeGroup(
                    new StrongNameMembershipCondition(new
                    StrongNamePublicKeyBlob(s_ecmaPublicKey), null, null),
                    new PolicyStatement(new PermissionSet(PermissionState.Unrestricted)));

                pol.RootCodeGroup.AddChild(ecmaKeyCG);

                // create membership condition for the key that signed CocoonHostRuntime
                UnionCodeGroup hostKeyCG =
                    new UnionCodeGroup(
                    new StrongNameMembershipCondition(new
                    StrongNamePublicKeyBlob(s_CocoonHostRuntimePublicKey), null, null),
                    new PolicyStatement(new PermissionSet(PermissionState.Unrestricted)));

                pol.RootCodeGroup.AddChild(hostKeyCG);

                // Create the permission set we'll grant to assemblies that satisfy 
                // our custom membership condition.  We grant the permission to execute, 
                // display UI and access IsolatedStorage
                PermissionSet pSet = new PermissionSet(PermissionState.None);

                // Add permission to execute
                pSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));

                // Add permission to display UI
                pSet.AddPermission(new UIPermission(PermissionState.Unrestricted));

                // Add permission to store 10k of data in isolated storage
                IsolatedStorageFilePermission isoStorePermission = new IsolatedStorageFilePermission(PermissionState.None);
                isoStorePermission.UsageAllowed = IsolatedStorageContainment.DomainIsolationByUser;
                isoStorePermission.UserQuota = 10000;
                pSet.AddPermission(isoStorePermission);

                // Create a code group with our custom membership condition and grant set
                UnionCodeGroup cocoonCG =
                    new UnionCodeGroup(new CocoonMembershipCondition(), new PolicyStatement(pSet));

                pol.RootCodeGroup.AddChild(cocoonCG);

                return pol;
            }
        }


    }

	public interface ICocoonDomainManager
	{
		void Run(string assemblyName, string typeName);
	}

	public class CocoonDomainManager : AppDomainManager, ICocoonDomainManager
	{
		public override void InitializeNewDomain(AppDomainSetup appDomainInfo)
		{
			// Set the flags so that the unmanaged portion of our host gets notified
			// of our domain manager via IHostControl::SetAppDomainManager
			InitializationFlags = DomainManagerInitializationFlags.RegisterWithHost;
		}

		public override HostSecurityManager HostSecurityManager
		{
			get
			{
                // Return a new instance of our security manager
				return new CocoonSecurityManager();
			}
		}

		// Run the "main" method from <assemblyName>.<typeName>
		public void Run(string assemblyName, string typeName)
		{
			try
			{
				Assembly asm = Assembly.Load(assemblyName);
				Type t = asm.GetType(typeName, true, true);
				MethodInfo m = t.GetMethod("Main");
				m.Invoke(null, null);
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception executing entry point: " + e.ToString());
			}

		}
	}

}
