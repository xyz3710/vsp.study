﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sao
{
	public class SaoHello : MarshalByRefObject
	{
		public SaoHello()
		{
			Console.WriteLine("SaoHello 생성됨");
		}

		public string SayHello(string name)
		{
			Console.WriteLine("CaoHello의 SayHello() 메서드 호출, 매개변수 : {0}", name);

			return "안녕하세요 [" + name + "] 님";
		}

		~SaoHello()
		{
			Console.WriteLine("SaoHello 소멸됨");
		}
	}
}
