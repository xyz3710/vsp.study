﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;

namespace SaoServer
{
	public class Server
	{
		static void Main(string[] args)
		{
			TcpServerChannel channel = new TcpServerChannel(9009);
			ChannelServices.RegisterChannel(channel, true);
			WellKnownServiceTypeEntry entry = new WellKnownServiceTypeEntry(
				Type.GetType("Sao.SaoHello, SaoHello"), "BaboSaoHello", WellKnownObjectMode.Singleton);
			RemotingConfiguration.RegisterWellKnownServiceType(entry);
			
			Console.WriteLine("서버를 멈추려면 Enter를 누르시오");
			Console.ReadLine();
		}
	}
}
