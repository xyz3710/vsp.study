﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using Sao;

namespace SaoClient
{
	public class Client3
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			object obj = RemotingServices.Connect(
				typeof(SaoHello), "tcp://localhost:9009/BaboSaoHello");

			SaoHello h = (SaoHello)obj;

			Console.WriteLine(h.SayHello("홍길동3"));
		}
	}
}
