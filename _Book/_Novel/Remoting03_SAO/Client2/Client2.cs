﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using Sao;

namespace SaoClient
{
	public class Client2
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			WellKnownClientTypeEntry entry = new WellKnownClientTypeEntry(
				Type.GetType("Sao.SaoHello, SaoHello"),
				"tcp://localhost:9009/BaboSaoHello");
			RemotingConfiguration.RegisterWellKnownClientType(entry);

			SaoHello h = new SaoHello();

			Console.WriteLine(h.SayHello("홍길동2"));
		}
	}
}
