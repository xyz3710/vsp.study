﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace InvokeMethodTest2
{
	public class Top
	{
		public string SayHello2(string name, string address)
		{
			return name + "님의 주소는 " + address + "입니다.";
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(Top);
			object newObj = Activator.CreateInstance(type);
			MethodInfo mi = type.GetMethod("SayHello2", 
				new Type[] { typeof(string), typeof(string) });
			object[] objParams = new object[] { "jabook", "seoul" };
			object result = mi.Invoke(newObj, objParams);

			Console.WriteLine(result);

			Console.ReadLine();
		}
	}
}
