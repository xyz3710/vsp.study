﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace InvokeMethodTest01
{
	public class Top
	{
		public string SayHello()
		{
			return "Hello World!!";
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(Top);
			object newObj = Activator.CreateInstance(type);
			MethodInfo mi = type.GetMethod("SayHello");
			object result = mi.Invoke(newObj, null);

			Console.WriteLine(result);

			Console.ReadLine();									
		}
	}
}
