﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Services;
using TrackingHello;

namespace Server
{
	public class THelloServer
	{
		static void Main(string[] args)
		{
			TcpServerChannel channel = new TcpServerChannel(9009);
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(THello), "BaboTHello", WellKnownObjectMode.SingleCall);

			TrackingServices.RegisterTrackingHandler(new MyTracking());

			Console.WriteLine("서버를 멈추려면 Enter를 누르세요");

			Console.ReadLine();
		}
	}
}
