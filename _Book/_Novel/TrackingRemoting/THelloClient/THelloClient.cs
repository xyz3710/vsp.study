﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Services;
using TrackingHello;

namespace Client
{
	public class THelloClient
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);

			TrackingServices.RegisterTrackingHandler(new MyTracking());

			THello h = (THello)Activator.GetObject(
				typeof(THello), "tcp://localhost:9009/BaboTHello");

			// 참조 마샬링의 예
			MarshalSample ms = h.GetMarshalSample();

			Console.WriteLine(ms.Sum(100, 100, 100));
			Console.WriteLine(ms.Sum(100, 100, 100));
			Console.WriteLine(ms.Sum(100, 100, 100));

			// 값 마샬링의 예
			SerialSample ss = h.GetSerialSample();

			Console.WriteLine(ss.Sum(200, 200, 200));
			Console.WriteLine(ss.Sum(200, 200, 200));
			Console.WriteLine(ss.Sum(200, 200, 200));
		}
	}
}
