﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Services;

namespace TrackingHello
{
	/// <summary>
	/// Tracking을 위한 클레스
	/// </summary>
	public class MyTracking : ITrackingHandler
	{
		#region ITrackingHandler Members

		public void DisconnectedObject(object obj)
		{
			Console.WriteLine("Disconnected \t\t{0}", obj.ToString());
			Console.WriteLine("Disconnected Tiem\t{0}", DateTime.Now);
		}

		public void MarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
		{
			Console.WriteLine("Marshaled UR\t{0}", or.URI);
			Console.WriteLine("Marshaled Type\t{0}", or.TypeInfo.TypeName);
			Console.WriteLine("Marshaled Time\t{0}", DateTime.Now);
		}

		public void UnmarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
		{
			Console.WriteLine("UnMarshaled URI\t{0}", or.URI);
			Console.WriteLine("UnMarshaled Type\t{0}", or.TypeInfo.TypeName);
			Console.WriteLine("UnMarshaled Time\t{0}", DateTime.Now);
		}
#endregion
	}
}
