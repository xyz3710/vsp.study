﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackingHello
{
	/// <summary>
	/// 참조마샬링(Marshal by Referenct)을 위한 클레스
	/// </summary>
	public class MarshalSample : MarshalByRefObject
	{
		public MarshalSample()
		{
			Console.WriteLine("MarshalSample의 생성자 호출");
		}

		public string Sum(int a, int b, int c)
		{
			Console.WriteLine("MarshalSample의 Sum() 호출");

			return "MarshalSample의 Sum() = " + (a + b + c);
		}
	}
}
