﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Lifetime;

namespace TrackingHello
{
	public class THello : MarshalByRefObject
	{
		public THello()
		{
			Console.WriteLine("THello 생성자 호출");
		}

		public MarshalSample GetMarshalSample()
		{
			Console.WriteLine("THello GetMarshalSample() 호출");
			Console.WriteLine("\t\t in method Current LeaseTime {0}",
				((ILease)base.InitializeLifetimeService()).CurrentLeaseTime);

			return new MarshalSample();
		}

		public SerialSample GetSerialSample()
		{
			Console.WriteLine("THello GetSerialSample() 호출");
			Console.WriteLine("\t\t in method Current LeaseTime {0}",
				((ILease)base.InitializeLifetimeService()).CurrentLeaseTime);

			return new SerialSample();
		}

		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();

			if (lease.CurrentState == LeaseState.Initial)
			{
				lease.InitialLeaseTime = TimeSpan.FromMinutes(1);
				lease.SponsorshipTimeout = TimeSpan.FromMinutes(2);
				lease.RenewOnCallTime = TimeSpan.FromSeconds(2);

				Console.WriteLine("\t\t in method Current LeaseTime {0}",
					((ILease)base.InitializeLifetimeService()).CurrentLeaseTime);
			}

			return lease;
		}
	}
}
