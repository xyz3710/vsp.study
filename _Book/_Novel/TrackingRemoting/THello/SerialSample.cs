﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackingHello
{
	/// <summary>
	/// 값마샬링(Marshal by Value)을 위한 클래스
	/// </summary>
	[Serializable]
	public class SerialSample 
	{
		public SerialSample()
		{
			Console.WriteLine("SerialSample의 생성자 호출");
		}

		public string Sum(int a, int b, int c)
		{
			Console.WriteLine("SerialSample의 Sum() 호출");

			return "SerialSample의 Sum() = " + (a + b + c);
		}
	}
}
