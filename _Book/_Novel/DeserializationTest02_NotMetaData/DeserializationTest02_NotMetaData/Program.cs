﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace DeserializationTest02_NotMetaData
{
	class Program
	{
		private const string _path = @"D:\VSP\Test\_Noble\SerializationTest02_SoapFormatter\SerializationTest02_SoapFormatter\bin\Debug\";

		static void Main(string[] args)
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = new FileStream(_path + "BinarySerial.dat", FileMode.Open);

			object bt = bf.Deserialize(fs);

			fs.Flush();
			fs.Close();

			Console.WriteLine("BinarySerial.dat 파일로 Top bt를 직렬화");

			Console.ReadLine();
		}
	}
}
