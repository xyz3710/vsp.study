﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;

namespace ChHttpHelloClient
{
	class ChHttpHelloClient
	{
		static void Main(string[] args)
		{
			HttpClientChannel channel =  new HttpClientChannel();
			ChannelServices.RegisterChannel(channel, false);
			object obj = Activator.GetObject(typeof(ChHello.ChHello), "http://localhost:8008/BaboChHello");
			ChHello.ChHello h = (ChHello.ChHello)obj;

			Console.WriteLine(h.SayHello("Http 8008 황태영"));
		}
	}
}
