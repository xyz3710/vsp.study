﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;

namespace ChHelloServer
{
	class ChHelloServer
	{
		static void Main(string[] args)
		{
			TcpServerChannel channel1 = new TcpServerChannel(9009);
			HttpServerChannel channel2 = new HttpServerChannel(8008);
			ChannelServices.RegisterChannel(channel1, true);
			ChannelServices.RegisterChannel(channel2, false);
			RemotingConfiguration.RegisterWellKnownServiceType(
				Type.GetType("ChHello.ChHello, ChHello"), "BaboChHello", WellKnownObjectMode.SingleCall);

			Console.WriteLine("서버를 멈추려면 Enter를 누르세요");
			Console.ReadLine();
		}
	}
}
