﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChHello
{
	public class ChHello : MarshalByRefObject
	{
		public ChHello()
		{
			Console.WriteLine("ChHello 생성자 호출");
		}

		public string SayHello(string name)
		{
			Console.WriteLine("ChHello의 SayHello 메서드 호출, 매개변수 : {0}", name);

			return "안녕하세요 ! [" + name + "] 님";
		}

		~ChHello()
		{
			Console.WriteLine("ChHello 소멸자 호출");
		}
	}
}
