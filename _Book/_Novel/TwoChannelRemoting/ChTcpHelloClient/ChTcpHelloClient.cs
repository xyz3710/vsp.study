﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;

namespace ChTcpHelloClient
{
	class ChTcpHelloClient
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			object obj = Activator.GetObject(typeof(ChHello.ChHello), "tcp://localhost:9009/BaboChHello");
			ChHello.ChHello h = (ChHello.ChHello)obj;

			Console.WriteLine(h.SayHello("Tcp 9009 김기원"));
		}
	}
}
