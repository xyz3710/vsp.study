﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;

namespace CommandNonQueryTest
{
	class Program
	{
		static void Main(string[] args)
		{
			string conStr = "Data Source=mobile;Initial Catalog=AdventureWorks;Integrated Security=True";
			string createQuery = "CREATE TABLE Address (id int, name char(20), addr char(40))";

			string query1 = "INSERT INTO Address(id, name, addr) values (1, '김삿갓', '서울시')";
			string query2 = "INSERT INTO Address(id, name, addr) values (2, '홍길동', '부산시')";
			string query3 = "INSERT INTO Address(id, name, addr) values (3, '춘향이', '광주시')";
			string query4 = "INSERT INTO Address(id, name, addr) values (4, '개똥이', '대구시')";
			string query5 = "INSERT INTO Address(id, name, addr) values (5, '김선달', '대전시')";

			Console.WriteLine("1. Connection 생성과 Open");
			SqlConnection conn = new SqlConnection(conStr);

			conn.Open();

			Console.WriteLine("2. Command 생성");
			SqlCommand comm = new SqlCommand(createQuery, conn);
			SqlCommand comm1 = new SqlCommand(query1, conn);
			SqlCommand comm2 = new SqlCommand(query2, conn);
			SqlCommand comm3 = new SqlCommand(query3, conn);
			SqlCommand comm4 = new SqlCommand(query4, conn);
			SqlCommand comm5 = new SqlCommand(query5, conn);

			Console.WriteLine("3. Command 수행");
			comm.ExecuteNonQuery();
			comm1.ExecuteNonQuery();
			comm2.ExecuteNonQuery();
			comm3.ExecuteNonQuery();
			comm4.ExecuteNonQuery();
			comm5.ExecuteNonQuery();

			Console.WriteLine("4. Connection Close");
			conn.Close();
		}
	}
}
