﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace ConnectionTest
{
	class Program
	{
		static void Main(string[] args)
		{
			string strConn = "Data Source=mobile;Initial Catalog=AdventureWorks;Integrated Security=True";
			SqlConnection conn = new SqlConnection(strConn);

			conn.Open();

			Console.WriteLine("1. Database\t\t{0}", conn.Database);
			Console.WriteLine("2. DataSource\t\t{0}", conn.DataSource);
			Console.WriteLine("3. DataServerVersion\t{0}", conn.ServerVersion);
			Console.WriteLine("4. State\t\t{0}", conn.State);
			Console.WriteLine("5. WorkstationID\t", conn.WorkstationId);

			conn.Close();

			Console.WriteLine("6. State\t\t{0}", conn.State);

			Console.ReadLine();
		}
	}
}
