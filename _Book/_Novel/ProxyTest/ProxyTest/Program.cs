﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

namespace ProxyTest
{
	class Program
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			object obj = Activator.GetObject(typeof(Sao.SaoHello), "tcp://localhost:9009/BaboSaoHello");

			if (RemotingServices.IsTransparentProxy(obj) == true)
			{
				Console.WriteLine("obj는 TransparentProxy 입니다");
				Console.WriteLine("obj의 type은 " + obj.ToString());

				RealProxy proxy = RemotingServices.GetRealProxy(obj);
				Console.WriteLine(proxy);
			}

			Console.ReadLine();
		}
	}
}
