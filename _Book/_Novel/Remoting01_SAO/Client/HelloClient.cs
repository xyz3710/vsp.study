﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using HelloNS;

namespace RemotingClient
{
	class HelloClient
	{
		static void Main(string[] args)
		{
			TcpChannel clientChannel = new TcpChannel();
			ChannelServices.RegisterChannel(clientChannel, true);

			object obj = Activator.GetObject(typeof(HelloClass), "tcp://localhost:9009/BaboHello");
			HelloClass h = (HelloClass)obj;

			Console.WriteLine(h.SayHello("홍길동"));
		}
	}
}
