﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
//using HelloNS;

namespace RemotingServer
{
	public class HelloServer
	{
		static void Main(string[] args)
		{
			TcpChannel serverChannel = new TcpChannel(9009);
			ChannelServices.RegisterChannel(serverChannel, true);
			RemotingConfiguration.RegisterWellKnownServiceType(
				Type.GetType("HelloNS.HelloClass, Hello"), "BaboHello", WellKnownObjectMode.Singleton);

			Console.WriteLine("서버를 멈추려면 Enter를 누르시오");
			Console.ReadLine();
		}
	}
}
