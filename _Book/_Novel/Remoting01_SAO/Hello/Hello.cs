﻿using System;

namespace HelloNS
{
	public class HelloClass : MarshalByRefObject
	{
		private int _count;

		public HelloClass()
		{
			Console.WriteLine("Hello 생성자 호출");
			_count = 0;
		}

		public string SayHello(string name)
		{
			Console.WriteLine("Hello의 SayHello() 메서드 호출, count : {0}\t매개변수 : {1}", _count++, name);

			return "안녕하세요! [" + name + "] 님";
		}

		~HelloClass()
		{
			Console.WriteLine("Hello 소멸자 호출");
		}
	} 
}
	
