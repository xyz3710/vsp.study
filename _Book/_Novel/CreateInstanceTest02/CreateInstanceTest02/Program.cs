﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInstanceTest02
{
	class Program
	{
		public Program(string str)
		{

		}
	
		static void Main(string[] args)
		{
			Type type = typeof(Program);
			object[] initParam = new object[] { "Jabook" };
			object obj = Activator.CreateInstance(type, initParam);

			Console.WriteLine(obj.GetType());

			Console.ReadLine();
		}
	}
}
