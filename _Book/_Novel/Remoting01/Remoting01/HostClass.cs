using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;

namespace Remoting01
{
	class HostClass
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Configuration Host...");

			TcpChannel channel = new TcpChannel(8080);
			ChannelServices.RegisterChannel(channel);

			Type type = Type.GetType("SimpleLibrary.SimpleClass, SimpleLibrary");
		}
	}
}
