﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace SerializationTest01
{
	class Program
	{
		static void Main(string[] args)
		{
			Top top = new Top();

			top.Age = 20;
			top.Tel = "016-632-8005";
			top.Addr = "전주시 완산구";

			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = new FileStream("TopSerial.dat", FileMode.OpenOrCreate);

			bf.Serialize(fs, top);

			fs.Flush();
			fs.Close();

			Console.WriteLine("TopSerial.dat 파일로 Top class의 instance top을 직렬화");
		}
	}
}
