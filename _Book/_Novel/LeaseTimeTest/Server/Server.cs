﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Services;
using SHello;

namespace Server
{
	public class Server
	{
		static void Main(string[] args)
		{
			TcpServerChannel channel = new TcpServerChannel(9009);
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterActivatedServiceType(Type.GetType("SHello.SHelloClass, SHello"));
			TrackingServices.RegisterTrackingHandler(new STracking());

			Console.WriteLine("서버를 멈추려면 Enter를 누르세요");

			Console.ReadLine();
		}
	}
}
