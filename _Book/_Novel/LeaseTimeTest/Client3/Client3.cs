﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using SHello;
using System.Runtime.Remoting.Lifetime;

namespace Client3
{
	public class Program
	{
		static void Main(string[] args)
		{
			TcpChannel channel = new TcpChannel(0);
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterActivatedClientType(
				typeof(SHelloClass), "tcp://localhost:9009");

			SHelloClass s = new SHelloClass();
			ILease lease = (ILease)RemotingServices.GetLifetimeService(s);
			MySponsor sponsor = new MySponsor();

			lease.Register(sponsor);

			Console.WriteLine(s.SayHello());
			Console.WriteLine("20초 대기하면 Sponsor가 작동합니다.");

			for (int i = 0; i < 20; i++)
			{
				Console.Write("\r{0}      ", i);

				System.Threading.Thread.Sleep(1000);
			}

			Console.WriteLine("\nSponsor를 제거하고 whdfygkfuaus Enter를 누르세요");

			Console.ReadLine();


			Console.WriteLine("Sponsor를 제거 {0}", DateTime.Now.TimeOfDay);

			lease.Unregister(sponsor);

			channel.StopListening(null);
		}
	}
}
