﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using SHello;

namespace Client
{
	public class Client
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterActivatedClientType(
				typeof(SHello.SHelloClass), "tcp://localhost:9009");

			SHelloClass s = new SHelloClass();

			for (int i = 0; i < 10; i++)
			{
				Console.WriteLine(s.SayHello());
				System.Threading.Thread.Sleep(3000);
			}

			Console.WriteLine("끝내시려면 Enter를 누르세요");

			Console.ReadLine();
		}
	}
}
