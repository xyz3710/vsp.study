﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Lifetime;

namespace SHello
{
	/// <summary>
	/// 원격 클레스
	/// </summary>
	public class SHelloClass : MarshalByRefObject
	{
		public SHelloClass()
		{
			Console.WriteLine("생성된 시간 : {0}", DateTime.Now.TimeOfDay);
		}

		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();

			if (lease.CurrentState == LeaseState.Initial)
			{
				lease.InitialLeaseTime = TimeSpan.FromSeconds(20);
				lease.SponsorshipTimeout = TimeSpan.FromSeconds(5);
				lease.RenewOnCallTime = TimeSpan.FromSeconds(5);
			}

			return lease;
		}

		public string SayHello()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();
			string msg = "SayHello() 남은 임대시간 = " + lease.CurrentLeaseTime;

			Console.WriteLine("{0} 현재시간 : {1}", msg, DateTime.Now.TimeOfDay);

			return msg;
		}
	}
}
