﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Lifetime;

namespace SHello
{
	public class MySponsor : MarshalByRefObject, ISponsor
	{
		private DateTime _lastRenewal;

		public MySponsor()
		{
			_lastRenewal = DateTime.Now;
		}
	
		#region ISponsor Members

		public TimeSpan Renewal(ILease lease)
		{
			Console.WriteLine("Renewal 시간간격 : {0}", (DateTime.Now - _lastRenewal).ToString());

			_lastRenewal = DateTime.Now;

			return TimeSpan.FromSeconds(5);
		}

		#endregion
	}
}
