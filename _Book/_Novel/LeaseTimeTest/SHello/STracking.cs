﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Services;

namespace SHello
{
	/// <summary>
	/// 트레킹 핸들러를 구현한 클레스
	/// </summary>
	public class STracking : ITrackingHandler
	{
		#region ITrackingHandler Members

		public void DisconnectedObject(object obj)
		{
			Console.Write("Disconnected {0}", obj.ToString());
			Console.WriteLine(" 현재시간 : {0}", DateTime.Now.TimeOfDay);
		}

		public void MarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
		{
			//Console.WriteLine("MarshaledObject {0}", obj.ToString());
		}

		public void UnmarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
		{
			//Console.WriteLine("UnmarshaledObject {0}", obj.ToString());
		}

		#endregion
	}
}
