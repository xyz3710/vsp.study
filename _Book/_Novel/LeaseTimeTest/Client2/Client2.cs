﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using SHello;
using System.Runtime.Remoting.Lifetime;

namespace Client2
{
	public class Client2
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterActivatedClientType(
				typeof(SHello.SHelloClass), "tcp://localhost:9009");

			SHelloClass h = new SHelloClass();
			ILease lease = (ILease)RemotingServices.GetLifetimeService(h);
			
			Console.WriteLine("10초 대기하면 ILease의 Renew() 메서드가 호출");

			for (int i = 0; i < 10; i++)
			{
				Console.Write("\r{0}     ", i);
				System.Threading.Thread.Sleep(1000);
			}

			Console.WriteLine("\nRenew(15초) 호출 전 CurrentLeaseTime : {0}", lease.CurrentLeaseTime);
			lease.Renew(TimeSpan.FromSeconds(15));

			Console.WriteLine("Renew(15초) 호출 후 CurrentLeaseTime : {0}", lease.CurrentLeaseTime);

			Console.WriteLine("종료하려면 Enter를 누르세요");

			Console.ReadLine();
		}
	}
}
