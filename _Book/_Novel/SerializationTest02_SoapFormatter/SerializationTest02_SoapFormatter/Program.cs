﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;

namespace SerializationTest02_SoapFormatter
{
	class Program
	{
		static void Main(string[] args)
		{
			Top top = new Top();
			
			top.Age = 32;
			top.Tel = "016-532-5395";
			top.Addr = "전주시 완산구 효자동";

			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = new FileStream("BinarySerial.dat", FileMode.OpenOrCreate);

			bf.Serialize(fs, top);

			fs.Flush();
			fs.Close();

			Console.WriteLine("BinarySerial.dat로 Top top을 직렬화");


			top.Age = 33;
			top.Tel = "016-632-8005";

			SoapFormatter sf = new SoapFormatter();
			fs = new FileStream("SoapSerial.dat", FileMode.OpenOrCreate);

			sf.Serialize(fs, top);

			fs.Flush();
			fs.Close();

			Console.WriteLine("SoapSerial.dat으로 Top top을 직렬화");
		}
	}
}
