﻿using System;
using System.Collections.Generic;
using System.Text;

[Serializable]
public class Top
{
	private int _age;
	private string _tel;
	private string _addr;

	public int Age
	{
		get
		{
			return _age;
		}
		set
		{
			_age = value;
		}
	}

	public string Tel
	{
		get
		{
			return _tel;
		}
		set
		{
			_tel = value;
		}
	}

	public string Addr
	{
		get
		{
			return _addr;
		}
		set
		{
			_addr = value;
		}
	}
}
