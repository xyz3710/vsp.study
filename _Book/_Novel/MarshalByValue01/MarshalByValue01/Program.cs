﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarshalByValue01
{
	public class Top
	{
		private int _num;
		private int _pass;

		public Top(int num, int pass)
		{
			_num = num;
			_pass = pass;
		}

		public int GetNum()
		{
			return _num;
		}

		public int GetPass()
		{
			return _pass;
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Top t = new Top(1000, 1234);

			int temp = t.GetNum();

			Console.WriteLine(temp);

			Console.ReadLine();
		}
	}
}
