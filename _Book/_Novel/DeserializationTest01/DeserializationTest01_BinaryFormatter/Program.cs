﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;

namespace DeserializationTest01_BinaryFormatter
{
	class Program
	{
		private const string _path = @"D:\VSP\Test\_Noble\SerializationTest02_SoapFormatter\SerializationTest02_SoapFormatter\bin\Debug\";

		static void Main(string[] args)
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = new FileStream(_path + "BinarySerial.dat", FileMode.Open);

			Top topBinary = (Top)bf.Deserialize(fs);		// Binary deserialization

			Console.WriteLine("Age\t{0}\nTel\t{1}\nAddr\t{2}",
				topBinary.Age, topBinary.Tel, topBinary.Addr);

			fs.Flush();
			fs.Close();

			SoapFormatter sf = new SoapFormatter();
			fs = new FileStream(_path + "SoapSerial.dat", FileMode.Open);

			Top topSoap = (Top)sf.Deserialize(fs);

			Console.WriteLine("Age\t{0}\nTel\t{1}\nAddr\t{2}",
				topSoap.Age, topSoap.Tel, topSoap.Addr);
			
			Console.ReadLine();
		}
	}
}
