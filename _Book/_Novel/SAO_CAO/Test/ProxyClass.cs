﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestClass
{
	public class ProxyClass : MarshalByRefObject
	{
		private string _name;
		private int _age;

		public ProxyClass()
		{
			_name = "Default name";
			_age = 1;

			Console.WriteLine("소멸자 : {0}, {1}", _name, _age);
		}

		public ProxyClass(string name, int age)
		{
			_name = name;
			_age = age;

			Console.WriteLine("소멸자 : {0}, {1}", _name, _age);
		}

		public void Print()
		{
			Console.WriteLine("이름 : {0}\t나이 : {1}", _name, _age);
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public int Age
		{
			get
			{
				return _age;
			}
			set
			{
				_age = value;
			}
		}

		~ProxyClass()
		{
			Console.WriteLine("소멸자 : {0}, {1}", _name, _age);
		}
	}
}
