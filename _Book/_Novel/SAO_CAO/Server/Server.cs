﻿//#define SAO

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;

namespace Server
{
	public class ServerClass
	{
		static void Main(string[] args)
		{
#if SAO
			// 1. SAO 9009 port
			TcpServerChannel channelSAO = new TcpServerChannel(9009);
			ChannelServices.RegisterChannel(channelSAO, true);
			WellKnownServiceTypeEntry entrySAO = new WellKnownServiceTypeEntry(
				Type.GetType("TestClass.ProxyClass, ProxyClass"), "RemotingTest", WellKnownObjectMode.Singleton);
			RemotingConfiguration.RegisterWellKnownServiceType(entrySAO);		// SAO 방식
#else
			// 2. CAO 9008 port
			TcpServerChannel channelCAO = new TcpServerChannel(9010);
			ChannelServices.RegisterChannel(channelCAO, true);
			ActivatedServiceTypeEntry entryCAO = new ActivatedServiceTypeEntry(
				Type.GetType("TestClass.ProxyClass, ProxyClass"));
			RemotingConfiguration.RegisterActivatedServiceType(entryCAO);
#endif
			Console.WriteLine("서버를 멈추려면 Enter를 누르세요");
			Console.ReadLine();
		}
	}
}
