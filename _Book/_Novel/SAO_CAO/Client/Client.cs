﻿//#define SAO

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using TestClass;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;

namespace Client
{
	public class ClientClass
	{
		static void Main(string[] args)
		{
#if SAO
			TcpClientChannel channelSAO = new TcpClientChannel();
			ChannelServices.RegisterChannel(channelSAO, true);

			// 1-1. use Activator.GetObject() for Proxy instance with Server Activated Object
			object obj1 = Activator.GetObject(typeof(ProxyClass), "tcp://localhost:9009/RemotingTest");
			ProxyClass proxySAO1 = (ProxyClass)obj1;

			Print(proxySAO1);
			
			// 1-2. use new for Proxy instance with Server Activated Object
			WellKnownClientTypeEntry entry = new WellKnownClientTypeEntry(
				Type.GetType("TestClass.ProxyClass, ProxyClass"), "tcp://localhost:9009/RemotingTest");
			RemotingConfiguration.RegisterWellKnownClientType(entry);

			ProxyClass proxySAO2 = new ProxyClass();

			Print(proxySAO2);

			// 1-3. use RemotingServices.Connect() for Proxy instance with Server Activated Object
			object obj2 = RemotingServices.Connect(
				Type.GetType("TestClass.ProxyClass, ProxyClass"), "tcp://localhost:9009/RemotingTest");

			ProxyClass proxySAO3 = (ProxyClass)obj2;

			Print(proxySAO3);
#else

			TcpClientChannel channelCAO = new TcpClientChannel();
			ChannelServices.RegisterChannel(channelCAO, true);

			// 2-1. use Activator.CreateInstance for Proxy instance with Client Activated Object
			object[] paras = { "김기원", 33 };
			object[] attrs = { new UrlAttribute("tcp://localhost:9010") };
			object obj2 = Activator.CreateInstance(
				Type.GetType("TestClass.ProxyClass, ProxyClass"),
				paras, attrs);
			ProxyClass proxyCAO1 = (ProxyClass)obj2;

			Print(proxyCAO1);

			// 2-2. use new for Proxy instance with Client Activated Object
			ActivatedClientTypeEntry entryClient = new ActivatedClientTypeEntry(
				typeof(TestClass.ProxyClass), "tcp://localhost:9010");
			RemotingConfiguration.RegisterActivatedClientType(entryClient);

			ProxyClass proxyCAO2 = new ProxyClass("황태영", 32);

			Print(proxyCAO2);
#endif
		}

		public static void Print(ProxyClass proxy)
		{
			Console.WriteLine("Input name : {0}", proxy.Name);
			proxy.Print();
		}
	}
}
