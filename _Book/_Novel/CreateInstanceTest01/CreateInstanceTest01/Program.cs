﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInstanceTest01
{
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(System.Collections.ArrayList);
			object obj = Activator.CreateInstance(type);

			Console.WriteLine(obj.GetType());

			Console.ReadLine();
		}
	}
}
