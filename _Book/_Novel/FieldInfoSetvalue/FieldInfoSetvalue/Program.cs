﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace FieldInfoSetvalue
{
	public class Top
	{
		public string name = "jabook";
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(Top);
			object obj = Activator.CreateInstance(type);
			FieldInfo fi = type.GetField("name");

			fi.SetValue(obj, "set new jabook");

			Console.WriteLine("필드 값 : {0}", fi.GetValue(obj));

			Console.ReadLine();
		}
	}
}
