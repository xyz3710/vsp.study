﻿namespace BatteryChecker
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.pbBattery = new System.Windows.Forms.ProgressBar();
			this.niBattery = new System.Windows.Forms.NotifyIcon(this.components);
			this.cmGraph = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsmAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.tsmGraph = new System.Windows.Forms.ToolStripMenuItem();
			this.tscboInterval = new System.Windows.Forms.ToolStripComboBox();
			this.tsmTransparent = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.tsmExit = new System.Windows.Forms.ToolStripMenuItem();
			this.cmGraph.SuspendLayout();
			this.SuspendLayout();
			// 
			// pbBattery
			// 
			this.pbBattery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pbBattery.BackColor = System.Drawing.Color.Tomato;
			this.pbBattery.ForeColor = System.Drawing.Color.LawnGreen;
			this.pbBattery.Location = new System.Drawing.Point(1, 0);
			this.pbBattery.MarqueeAnimationSpeed = 1;
			this.pbBattery.Name = "pbBattery";
			this.pbBattery.Size = new System.Drawing.Size(288, 20);
			this.pbBattery.Step = 1;
			this.pbBattery.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.pbBattery.TabIndex = 0;
			this.pbBattery.Value = 50;
			// 
			// niBattery
			// 
			this.niBattery.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.niBattery.BalloonTipTitle = "Battery Life Checker";
			this.niBattery.ContextMenuStrip = this.cmGraph;
			this.niBattery.Text = "Battery Life Checker";
			this.niBattery.Visible = true;
			this.niBattery.DoubleClick += new System.EventHandler(this.tsmGraph_Click);
			// 
			// cmGraph
			// 
			this.cmGraph.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAbout,
            this.toolStripSeparator1,
            this.tsmGraph,
            this.tscboInterval,
            this.tsmTransparent,
            this.toolStripMenuItem1,
            this.tsmExit});
			this.cmGraph.Name = "contextMenuStrip1";
			this.cmGraph.Size = new System.Drawing.Size(183, 128);
			// 
			// tsmAbout
			// 
			this.tsmAbout.Name = "tsmAbout";
			this.tsmAbout.Size = new System.Drawing.Size(182, 22);
			this.tsmAbout.Text = "이 프로그램은...(&A)";
			this.tsmAbout.ToolTipText = "이 프로그램은...";
			this.tsmAbout.Click += new System.EventHandler(this.tsmAbout_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(179, 6);
			// 
			// tsmGraph
			// 
			this.tsmGraph.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.tsmGraph.Name = "tsmGraph";
			this.tsmGraph.Size = new System.Drawing.Size(182, 22);
			this.tsmGraph.Text = "그래프 숨기기(&G)";
			this.tsmGraph.ToolTipText = "그래프 보이고 감추기";
			this.tsmGraph.Click += new System.EventHandler(this.tsmGraph_Click);
			// 
			// tscboInterval
			// 
			this.tscboInterval.Items.AddRange(new object[] {
            "1.0",
            "1.5",
            "2.0",
            "2.5",
            "3.0",
            "3.5",
            "4.0",
            "4.5",
            "5.0"});
			this.tscboInterval.Name = "tscboInterval";
			this.tscboInterval.Size = new System.Drawing.Size(121, 20);
			this.tscboInterval.Sorted = true;
			this.tscboInterval.Text = "갱신 간격";
			this.tscboInterval.ToolTipText = "갱신 간격 조절";
			this.tscboInterval.SelectedIndexChanged += new System.EventHandler(this.tscboInterval_SelectedIndexChanged);
			// 
			// tsmTransparent
			// 
			this.tsmTransparent.Checked = true;
			this.tsmTransparent.CheckState = System.Windows.Forms.CheckState.Checked;
			this.tsmTransparent.Name = "tsmTransparent";
			this.tsmTransparent.Size = new System.Drawing.Size(182, 22);
			this.tsmTransparent.Text = "투명하게(&T)";
			this.tsmTransparent.ToolTipText = "남은 양 투명하게";
			this.tsmTransparent.Click += new System.EventHandler(this.tsmTransparent_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(179, 6);
			// 
			// tsmExit
			// 
			this.tsmExit.Name = "tsmExit";
			this.tsmExit.Size = new System.Drawing.Size(182, 22);
			this.tsmExit.Text = "종료(&X)";
			this.tsmExit.ToolTipText = "끝내기";
			this.tsmExit.Click += new System.EventHandler(this.tsmExit_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(290, 20);
			this.Controls.Add(this.pbBattery);
			this.Font = new System.Drawing.Font("Tahoma", 9F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Battery Life Checker";
			this.TopMost = true;
			this.TransparencyKey = System.Drawing.Color.LawnGreen;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.cmGraph.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ProgressBar pbBattery;
		private System.Windows.Forms.NotifyIcon niBattery;
		private System.Windows.Forms.ContextMenuStrip cmGraph;
		private System.Windows.Forms.ToolStripMenuItem tsmGraph;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem tsmExit;
		private System.Windows.Forms.ToolStripMenuItem tsmAbout;
		private System.Windows.Forms.ToolStripMenuItem tsmTransparent;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripComboBox tscboInterval;


	}
}

