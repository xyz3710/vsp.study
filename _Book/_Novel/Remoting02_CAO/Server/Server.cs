﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;

namespace CaoServer
{
	class Server
	{
		static void Main(string[] args)
		{
			TcpServerChannel channel = new TcpServerChannel(8005);
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterActivatedServiceType(
				Type.GetType("Cao.CaoHello, CaoHello"));

			Console.WriteLine("서버를 멈추려면 Enter를 누르세요");
			Console.ReadLine();
		}
	}
}
