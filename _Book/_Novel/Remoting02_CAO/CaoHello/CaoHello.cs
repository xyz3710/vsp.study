﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cao
{
	public class CaoHello : MarshalByRefObject
	{
		private string _name;

		public CaoHello()
		{

		}
	
		public CaoHello(string name)
		{
			_name = name;

			Console.WriteLine("CaoHello 생성됨");
		}

		public string SayHello()
		{
			Console.WriteLine("CaoHello의 SayHello() 메서드 호출, HashCode : {0}", this.GetHashCode());

			return "안녕하세요 [" + _name + "] 님";
		}

		~CaoHello()
		{
			Console.WriteLine("CaoHello 소멸됨");
		}
	}
}
