﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using Cao;

namespace CaoClient2
{
	class Client
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			RemotingConfiguration.RegisterActivatedClientType(
				typeof(Cao.CaoHello), "tcp://localhost:8005");

			CaoHello h = new CaoHello("홍길동");

			Console.WriteLine(h.SayHello());
			Console.WriteLine(h.SayHello());
			Console.WriteLine(h.SayHello());
			Console.WriteLine(h.SayHello());

			Console.WriteLine("1초 동안 대기 후 다시 호출");
			System.Threading.Thread.Sleep(1000);

			Console.WriteLine(h.SayHello());

			Console.ReadLine();
		}
	}
}
