﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;
using Cao;

namespace CaoClient
{
	class Client
	{
		static void Main(string[] args)
		{
			TcpClientChannel channel = new TcpClientChannel();
			ChannelServices.RegisterChannel(channel, true);
			object[] parms = { "김삿갓" };
			object[] attrs = { new UrlAttribute("tcp://localhost:8005") };
			object obj = Activator.CreateInstance(
				Type.GetType("Cao.CaoHello, CaoHello"), parms, attrs);

			// 인자값이 없는 생성자 호출 시
			//object obj = Activator.CreateInstance(
			//    Type.GetType("Cao.CaoHello, CaoHello"), null, new object[] { new UrlAttribute("tcp://localhost:8005") });
			
			CaoHello h = (CaoHello)obj;

			Console.WriteLine(h.SayHello());
			Console.WriteLine(h.SayHello());
			Console.WriteLine(h.SayHello());
			Console.WriteLine(h.SayHello());

			Console.WriteLine("1초 동안 대기 후 다시 호출");
			System.Threading.Thread.Sleep(1000);

			Console.WriteLine(h.SayHello());

			Console.ReadLine();
		}
	}
}
