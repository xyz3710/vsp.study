﻿/**********************************************************************************************************************/
/*	Domain		:	ProjectEulerSolve.Program
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 2월 13일 월요일 오후 3:59
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://euler.synap.co.kr/ 문제 풀이
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Transactions;
using System.Collections.Concurrent;
using System.IO;

namespace ProjectEulerSolve
{
	partial class Program
	{
		static void Main(string[] args)
		{
			//Exam01();		// 3 또는 5의 합
			//Exam02();		// 피보나치
			//Exam03();		// 소인수분해
			//Exam04();		// 대칭수
			//Exam05();		// 나누어 떨어지는 수
			//Exam06();		// 제굽의 합, 합의 제곱
			//Exam07();		// 몇번째 소수인지
			//Exam08();		// 1000 자리중 가장 큰값
			//Exam09();		// 피타고라스 수
			//Exam10();		// 200백만 이하 소수의 합
			//Exam11();		// 20 x 20 격자 값
			Exam12();		// 
			Exam13();		// 
			Exam14();		// 
			Exam15();		// 
			Exam16();		// 
			Exam17();		// 
			Exam18();		// 
			Exam19();		// 
			//Exam20();		// 100!
			//Exam22();		// 이름 점수
			//Exam30();		// 5제곱의 합이 자신이 된다.
			Exam68();		// 마방진
			//char[] left = { '9', '9' };
			//char[] right = { '9' };

			//var r1 = left[0].ReverseMultiply(right[0]);
			//var r2 = left[1].ReverseMultiply(right[0], 1);

			//var rr = left.ReverseMultiply(right);
			//var abc = FactorialCharArray(6);
		}

		private static void Exam68()
		{
			
		}

		private static void Exam22()
		{
			// 이름 점수
			string namesText = File.ReadAllText("names.txt");
			var names = namesText.Split(',')
				.Select(x => x.Replace("\"", string.Empty))
				.OrderBy(x => x)
				.ToList();
			long sum = 0L;

			for (int i = 0; i < names.Count; i++)
			{
				int nameScore = GetNameScore(names[i]);
				int score = nameScore * (i + 1);

				sum += score;
				Console.WriteLine("{0:#,##0}: {1}\t{2}, {3}", i + 1, names[i], nameScore, score);
			}

			Console.WriteLine(sum);
		}

		private static int GetNameScore(string name)
		{
			return name.Select(x => x - 64).Sum();
		}

		private static void Exam20()
		{
			// 100 !
			uint number = 65;
			/*
			string result = FactorialCharArray(number).ToReverseString();

			for (uint i = 1; i <= number; i++)
			{
				Console.WriteLine("{0}!\t{1}", i, Factorial(i));
				Console.WriteLine("\t{0}", FactorialCharArray(i).ToReverseString());
			}
			*/
			var fac = Factorial(number);
			var array = fac.ToReverseCharArray().Select(x => x - 48);

			Console.WriteLine("{0}! => {1}, sum:  {2}", number, fac, array.Sum());
		}

		private static void Exam19()
		{
			//
		}

		private static void Exam18()
		{
			//
		}

		private static void Exam17()
		{
			//
		}

		private static void Exam16()
		{
			//
		}

		private static void Exam15()
		{
			//
		}

		private static void Exam14()
		{
			//
		}

		private static void Exam13()
		{
			//
		}

		private static void Exam12()
		{
			//
		}

		private static void Exam11()
		{
			// 20 x 20 격자 값
			int[,] matrix = 
			{
				{08, 02, 22, 97, 38, 15, 00, 40, 00, 75, 04, 05, 07, 78, 52, 12, 50, 77, 91, 08},
				{49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48, 04, 56, 62, 00},
				{81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 03, 49, 13, 36, 65},
				{52, 70, 95, 23, 04, 60, 11, 42, 69, 24, 68, 56, 01, 32, 56, 71, 37, 02, 36, 91},
				{22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80},
				{24, 47, 32, 60, 99, 03, 45, 02, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50},
				{32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70},
				{67, 26, 20, 68, 02, 62, 12, 20, 95, 63, 94, 39, 63, 08, 40, 91, 66, 49, 94, 21},
				{24, 55, 58, 05, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72},
				{21, 36, 23, 09, 75, 00, 76, 44, 20, 45, 35, 14, 00, 61, 33, 97, 34, 31, 33, 95},
				{78, 17, 53, 28, 22, 75, 31, 67, 15, 94, 03, 80, 04, 62, 16, 14, 09, 53, 56, 92},
				{16, 39, 05, 42, 96, 35, 31, 47, 55, 58, 88, 24, 00, 17, 54, 24, 36, 29, 85, 57},
				{86, 56, 00, 48, 35, 71, 89, 07, 05, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58},
				{19, 80, 81, 68, 05, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77, 04, 89, 55, 40},
				{04, 52, 08, 83, 97, 35, 99, 16, 07, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66},
				{88, 36, 68, 87, 57, 62, 20, 72, 03, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69},
				{04, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18, 08, 46, 29, 32, 40, 62, 76, 36},
				{20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74, 04, 36, 16},
				{20, 73, 35, 29, 78, 31, 90, 01, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57, 05, 54},
				{01, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52, 01, 89, 19, 67, 48},
			};
			int div = 4;
			int max = 0;
			
			for (int y = 0; y <= matrix.GetLength(1) - div; y++)
			{
				for (int x = 0; x <= matrix.GetLength(0) - div; x++)
				{
					int sumH = 1;	// 가로
					int sumV = 1;	// 세로
					int sumDD = 1;	// \
					int sumDU = 1;	// /

					for (int i = 0; i < div; i++)
					{
						Console.WriteLine("x: {0}, y: {1}\t{2}, {3}", x, y, matrix[x + i, y], matrix[x, y + i]);
						sumH *= matrix[x + i, y];						
						sumV *= matrix[x, y + i];
					}

					sumDD *= matrix[x, y];
					sumDU *= matrix[y, x];
					
					max = (int)Math.Max(sumH, max);
					max = (int)Math.Max(sumV, max);
					max = (int)Math.Max(sumDD, max);
					max = (int)Math.Max(sumDU, max);
				}
			}

			Console.WriteLine(max);
		}

		private static void Exam10()
		{
			// 200만 이하 소수의 합
			var number = 2000000U;
			var primeNumbers = GetPrimeNumbersByRange(number);
			var sum = primeNumbers.Select(x => Convert.ToInt64(x)).Sum();
			//var primeNumbers = File.ReadAllLines("prime.txt").Select(x => Convert.ToInt64(x)).ToList();

			Console.WriteLine(sum);
		}

		private static void Exam09()
		{
			// 피타고라스 수
			for (int a = 1; a < 1000; a++)
			{
				for (int b = 1; b < 1000; b++)
				{
					for (int c = 1; c < 1000; c++)
					{
						if ((int)Math.Pow(c, 2) == (int)Math.Pow(a, 2) + (int)Math.Pow(b, 2))
						{
							if (a + b + c == 1000)
							{
								Console.WriteLine("a: {0}, b: {1}, c: {2}, x: {3}", a, b, c, a * b * c);
								// a: 200, b: 375, c: 425, x: 31875000
								break;
							}
						}
					}
				}
			}
		}

		private static void Exam08()
		{
			// 1000 자리중 가장 큰값
			string numbers = "73167176531330624919225119674426574742355349194934" +
				"96983520312774506326239578318016984801869478851843" +
				"85861560789112949495459501737958331952853208805511" +
				"12540698747158523863050715693290963295227443043557" +
				"66896648950445244523161731856403098711121722383113" +
				"62229893423380308135336276614282806444486645238749" +
				"30358907296290491560440772390713810515859307960866" +
				"70172427121883998797908792274921901699720888093776" +
				"65727333001053367881220235421809751254540594752243" +
				"52584907711670556013604839586446706324415722155397" +
				"53697817977846174064955149290862569321978468622482" +
				"83972241375657056057490261407972968652414535100474" +
				"82166370484403199890008895243450658541227588666881" +
				"16427171479924442928230863465674813919123162824586" +
				"17866458359124566529476545682848912883142607690042" +
				"24219022671055626321111109370544217506941658960408" +
				"07198403850962455444362981230987879927244284909188" +
				"84580156166097919133875499200524063689912560717606" +
				"05886116467109405077541002256983155200055935729725" +
				"71636269561882670428252483600823257530420752963450";
			int maxValue = 0;
			int div = 5;

			for (int i = 0; i < numbers.Length; i++)
			{
				if (i > numbers.Length - div)
					break;

				string num5 = numbers.Substring(i, div);
				var sepNum5 = num5.Aggregate<char, string>(
										string.Empty,
										(acc, next) =>
										{
											string nextValue = next.ToString();

											if (acc == string.Empty)
												return nextValue;

											return string.Format("{0}{1}{2}", acc, ", ", nextValue);
										});
				int sum = 1;

				for (int j = 0; j < num5.Length; j++)
					sum *= num5[j] - 48;

				maxValue = Math.Max(maxValue, sum);
				Console.WriteLine("{0}: {1}\t{2}", sepNum5, sum, (sum == maxValue) ? "Max" : string.Empty);
			}

			Console.WriteLine("Max: {0}", maxValue);
		}

		private static void Exam07()
		{
			// 몇번째 소수인지
			int[] primeNumbers = GetPrimeNumbersByCount(10001);

			Console.WriteLine(primeNumbers[primeNumbers.Length - 1]);
		}

		private static void Exam06()
		{
			int sum1 = 0;
			int sum2 = 0;

			for (int i = 1; i <= 100; i++)
			{
				sum1 += (int)Math.Pow(i, 2);
				sum2 += i;
			}

			Console.WriteLine(Math.Pow(sum2, 2) - sum1);
		}

		private static void Exam05()
		{
			Console.WriteLine(GCD(10, 2));
			// 나누어 떨어지는 수
			var array = new List<int>(11);

			for (int i = 1; i <= 20; i++)
				array.Add(i);

			int count = 1;

			while (true)
			{
				bool result = true;
				
				Parallel.ForEach(array, num =>
				{
					result &= count % num == 0;
				});

				if (result == true)
				{
					Console.WriteLine(count);

					break;
				}

				count++;
			}
		}

		private static void Exam04()
		{
			// 대칭수
			int startNum = 900;
			int endNum = 1000;

			for (int i = startNum; i < endNum; i++)
			{
				for (int j = startNum; j < endNum; j++)
				{
					string target = (i * j).ToString();
					string target1 = target.Substring(0, target.Length / 2);
					string target2 = target.Substring(target.Length / 2)
							.Reverse().Aggregate<char, string>(
													string.Empty,
													(acc, next) =>
													{
														string nextValue = next.ToString();

														if (acc == string.Empty)
															return nextValue;

														return string.Format("{0}{1}{2}", acc, string.Empty, nextValue);
													});

					if (target1 == target2)
						Console.WriteLine("target: {0}\ti: {1}, j: {2}", target, i, j);
				}
			}
		}

		private static void Exam03()
		{
			// 소인수 분해			
			Func<List<int>, int> multiply = x =>
			{
				int sum = 1;

				x.ForEach(y => sum *= y);

				return sum;
			};

			var allPrimes = GetPrimeNumbersByRange(13195);
			long targetNumber = 600851475143;
			var primes = new List<int>();

			foreach (int prime in allPrimes)
			{
				if (targetNumber % prime == 0)
					primes.Add(prime);
				
				if (multiply(primes) == targetNumber)
					break;
			}

			Console.WriteLine(primes.Aggregate<int, string>(
										string.Empty,
										(acc, next) =>
										{
											string nextValue = next.ToString();

											if (acc == string.Empty)
												return nextValue;

											return string.Format("{0}{1}{2}", acc, ", ", nextValue);
										}));
		}

		private static void Exam02()
		{
			// 피보나치
			Func<int, int, int> fibonacci = (x, y) =>
			{
				return x + y;
			};

			int seed = 1;
			int nextNum = 2;
			int result = 0;
			int sum = nextNum;

			Console.WriteLine(seed);

			do
			{
				result = fibonacci(seed, nextNum);

				seed = nextNum;
				nextNum = result;

				if (result % 2 == 0)
				{
					sum += result;

					Console.WriteLine("R: {0}\tS:{1}", result, sum);
				}
			} while (result < 4000000);

			Console.WriteLine("R: {0}\tS:{1}", result, sum);
		}

		private static void Exam01()
		{
			// 3 또는 5의 합
			int sum = 0;

			for (int i = 0; i < 1000; i++)
			{
				if (i % 3 == 0 || i % 5 == 0)
					sum += i;
			}

			Console.WriteLine(sum);
		}

	}
}
