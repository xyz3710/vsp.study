﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectEulerSolve
{
	partial class Program
	{
		private static Func<uint, ulong[]> GetPrimeNumbersByRange = range =>
		{
			List<ulong> results = new List<ulong>();

			if (range == 1)
				throw new InvalidOperationException("1 was not prime number's range");

			results.Add(2);

			for (uint i = 3; i <= range; i++)
			{
				bool continueNext = false;

				for (int x = 0; x < results.Count; x++)
				{
					continueNext |= i % results[x] == 0;

					if (continueNext == true)
						break;
				}

				if (continueNext == false)
					results.Add(i);
			}

			return results.ToArray();
		};
		private static Func<long, int[]> GetPrimeNumbersByCount = count =>
		{
			List<int> results = new List<int>();

			results.Add(2);

			int i = 3;

			while (results.Count < count)
			{
				bool continueNext = false;

				for (int x = 0; x < results.Count; x++)
				{
					continueNext |= i % results[x] == 0;

					if (continueNext == true)
						break;
				}

				if (continueNext == false)
					results.Add(i);

				i++;
			}

			return results.ToArray();
		};
		private static Func<ulong, ulong> Factorial = n =>
		{
			if (n == 1)
				return 1;

			return n * Factorial(n - 1);
		};
		private static Func<uint, char[]> FactorialCharArray = n =>
		{
			if (n == 1)
				return new char[] { '1' };

			var ret = ((int)n).ToReverseCharArray().ReverseMultiply(FactorialCharArray(n - 1));

			//Console.WriteLine(ret.ToReverseString());

			return ret;
		};
		private static Func<int, int, int> GCD = (p, q) =>
		{
			if (q == 0)
				return p;

			return GCD(q, p % q);
		};
	}
}
