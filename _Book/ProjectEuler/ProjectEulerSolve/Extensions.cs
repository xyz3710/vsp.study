﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectEulerSolve
{
	public static class Extensions
	{
		public static char[] ReverseAdd(this char c1, char c2)
		{
			return Convert.ToString((int)(c1 - 48) + (int)(c2 - 48)).Reverse().ToArray();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="left">54 =&gt; { '4', '5' }의 배열이 아니고 { '5', '4' } 숫자 그대로 나타나는 배열</param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static char[] ResersveAdd(this char[] left, char[] right)
		{
			int leftLength = left.Length;
			int rightLength = right.Length;
			int maxLength = Math.Max(rightLength, leftLength);
			var buffer = new List<char[]>();
			int bufferCount = buffer.Count;

			for (int i = 0; i < maxLength; i++)
			{
				if (i < leftLength && i < rightLength)
					buffer.Add(left[i].ReverseAdd(right[i]));
				else
				{
					int lastBufferIndex = buffer.Count - 1;
					char lastBuffersValue = buffer[lastBufferIndex].Length > 1
						? buffer[lastBufferIndex][buffer[lastBufferIndex].Length - 1]
						: '0';

					if (i < leftLength)
						buffer.Add(left[i].ReverseAdd(lastBuffersValue));
					else if (i < rightLength)
						buffer.Add(right[i].ReverseAdd(lastBuffersValue));
				}
			}

			bufferCount = buffer.Count;
			var result = new List<char>(bufferCount + buffer[bufferCount - 1].Length);

			for (int i = 0; i < bufferCount; i++)
			{
				if (i == bufferCount - 1)
					result.AddRange(buffer[i]);		// 마지막 요소는 자리 올림도 같이 추가 한다.
				else
					result.Add(buffer[i][0]);		// 마지막 자리수만 추가 한다.
			}

			return result.ToArray();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="c1"></param>
		/// <param name="c2"></param>
		/// <param name="decimalDigit">뒤에 붙는 0 자리수 개수</param>
		/// <returns></returns>
		public static char[] ReverseMultiply(this char c1, char c2, int decimalDigit = 0)
		{
			if (decimalDigit == 0)
				return Convert.ToString((int)(c1 - 48) * (int)(c2 - 48)).Reverse().ToArray();
			else
				return Convert.ToString((int)(c1 - 48) * (int)(c2 - 48) * Math.Pow(10, decimalDigit)).Reverse().ToArray();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="left">54 =&gt; { '4', '5' }의 배열이 아니고 { '5', '4' } 숫자 그대로 나타나는 배열</param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static char[] ReverseMultiply(this char[] left, char[] right)
		{
			var buffer = new List<char[]>();
			int bufferCount = buffer.Count;
			char[] lSide = null;
			char[] rSide = null;

			if (left.Length >= right.Length)
			{
				lSide = left;
				rSide = right;
			}
			else
			{
				lSide = right;
				rSide = left;
			}

			int leftLength = lSide.Length;		// 큰 배열
			int rightLength = rSide.Length;		// 작은 배열

			for (int l = 0; l < leftLength; l++)
			{
				for (int r = 0; r < rightLength; r++)
				{
					if (l < leftLength && r < rightLength)
						buffer.Add(lSide[l].ReverseMultiply(rSide[r], l + r));
					else
						buffer.Add(lSide[l].ReverseMultiply(rSide[r]));
				}
			}

			//foreach (var item in buffer)
			//	Console.WriteLine(item.ToReverseString());

			var result = new List<char[]>();
			bufferCount = buffer.Count;
			result.Add(new char[] { '0' });

			for (int i = 0; i < bufferCount; i++)
				result.Add(result[i].ResersveAdd(buffer[i]));

			return result[result.Count - 1];
		}

		public static string ToReverseString(this char[] charArray)
		{
			string result = string.Empty;

			for (int i = charArray.Length - 1; i >= 0; i--)
				result += charArray[i];

			return result;
		}

		public static char[] ToReverseCharArray(this int @uint)
		{
			return Convert.ToString(@uint).Reverse().ToArray();
		}

		public static char[] ToReverseCharArray(this ulong @ulong)
		{
			return Convert.ToString(@ulong).Reverse().ToArray();
		}
	}
}
