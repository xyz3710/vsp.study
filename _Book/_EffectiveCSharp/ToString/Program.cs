﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToString
{

	class Program
	{
		static void Main(string[] args)
		{
			Customer c1 = new Customer();
			
			c1.Name = "김기원";
			c1.Revenue = 12.33;
			c1.ContactPhone = "016-632-8005";
			
			Console.WriteLine("Customer record: {0}", c1.ToString("nr", null));
			Console.WriteLine(string.Format(new CustomFormatter(), "{0}", c1));
		}
	}
}
