﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToString
{
	public class CustomFormatter : IFormatProvider
	{
		#region IFormatProvider 멤버

		public object GetFormat(Type formatType)
		{
			if (formatType == typeof(ICustomFormatter))
				return new CustomerFormatterProvider();

			return null;
		}

		#endregion

		private class CustomerFormatterProvider : ICustomFormatter
		{
			#region ICustomFormatter 멤버

			public string Format(string format, object arg, IFormatProvider formatProvider)
			{
				Customer c = arg as Customer;

				if (c == null)
					return arg.ToString();

				return string.Format("{0, 50}, {1, 15}, {2, 10:G}", c.Name, c.ContactPhone, c.Revenue);
			}

			#endregion
		}
	}
}
