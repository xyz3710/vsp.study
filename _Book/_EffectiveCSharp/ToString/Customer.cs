﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToString
{
	public class Customer : IFormattable
	{
		private string _name;
		private double _revenue;
		private string _contactPhone;

		#region Properties
		/// <summary>
		/// Name을(를) 구하거나 설정합니다.
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Revenue을(를) 구하거나 설정합니다.
		/// </summary>
		public double Revenue
		{
			get
			{
				return _revenue;
			}
			set
			{
				_revenue = value;
			}
		}

		/// <summary>
		/// ContactPhone을(를) 구하거나 설정합니다.
		/// </summary>
		public String ContactPhone
		{
			get
			{
				return _contactPhone;
			}
			set
			{
				_contactPhone = value;
			}
		}

		#endregion
        
		public override string ToString()
		{
			return _name;
		}

		#region IFormattable 멤버

		public string ToString(string format, IFormatProvider formatProvider)
		{
			if (formatProvider != null)
			{
				ICustomFormatter fmt = 
					formatProvider.GetFormat(this.GetType()) as ICustomFormatter;

				if (fmt != null)
					return fmt.Format(format, this, formatProvider);
			}

			switch (format)
			{
				case "r":
					return _revenue.ToString();
				case "p":
					return _contactPhone;
				case "nr":
					return string.Format("{0, 20}, {1, 10:G}", _name, _revenue);
				case "n":
				case "G":
				default:
					return _name;
			}
		}

		#endregion
	}
}
