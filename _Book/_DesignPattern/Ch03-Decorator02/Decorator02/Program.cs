using System;
using System.Collections.Generic;
using System.Text;
using Decorator02.Coffee;
using Decorator02.Condiment;
using System.Reflection;

namespace Decorator02
{
	class Program
	{
		static void Main(string[] args)
		{
			getPrice();

			Beverage houseBlend = new HouseBlend();

			houseBlend = new Soy(houseBlend);
			houseBlend = new Milk(houseBlend);
			printReceipt(houseBlend);

			Beverage espresso = new Espresso();

			espresso = new Mocha(espresso);
			printReceipt(espresso);
		}

		private static void printReceipt(Beverage houseBlend)
		{
			Console.WriteLine("{0,-20} {1,12:#,##0} 원", houseBlend.Description, houseBlend.Cost);
		}

		private static void getPrice()
		{
			Assembly asm = Assembly.LoadFile(Environment.CurrentDirectory + "\\Decorator02.exe");
			Type[] types = asm.GetTypes();

			Console.WriteLine("{0,40}", "커    피");

			foreach (Type type in types)
			{
				if (type.IsClass == true && type.BaseType.Name == "Beverage")
				{
					ConstructorInfo ci = type.GetConstructor(Type.EmptyTypes);

					// Parameter가 없을 경우 커피
					if (ci != null)
					{
						Beverage coffee = (Beverage)Activator.CreateInstance(type);

						if (coffee != null)
							printReceipt(coffee);                        
					}
				}
			}

			Console.WriteLine("\n{0,40}", "추 가 물");

			foreach (Type type in types)
			{
				if (type.IsClass == true && type.BaseType.Name == "Beverage")
				{
					ConstructorInfo ci = type.GetConstructor(Type.EmptyTypes);

					// Parameter가 있을 경우 Condiment
					if (ci == null)
					{
						Beverage coffee = (Beverage)Activator.CreateInstance(type, new object[] { new Beverage() });

						if (coffee != null)
							printReceipt(coffee);

					}					
				}
			}

			Console.WriteLine();
		}
	}
}
