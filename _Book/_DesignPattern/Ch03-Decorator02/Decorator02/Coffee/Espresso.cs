using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator02.Coffee
{
	public class Espresso : Beverage
	{
		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Espresso class.
		/// </summary>
		public Espresso()
		{
			Description = "에스프레소";
			Cost = 1000;
		}
		#endregion
	}
}