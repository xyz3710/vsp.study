using System;
using System.Collections.Generic;
using System.Text;
using Decorator02.Coffee;
using System.Collections;

namespace Decorator02.Condiment
{
	public class Milk : Beverage
	{
		private IBeverage _beverage;

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Milk class.
		/// </summary>
		public Milk(Beverage beverage)
		{
			_beverage = beverage;

			base.Description = _beverage.Description + 
				string.Format("{0}����", _beverage.Description == string.Empty ? string.Empty : ", ");
			base.Cost = _beverage.Cost + 150;
		}
		#endregion
	}
}