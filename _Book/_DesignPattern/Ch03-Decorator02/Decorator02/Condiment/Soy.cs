using System;
using System.Collections.Generic;
using System.Text;
using Decorator02.Coffee;

namespace Decorator02.Condiment
{
	public class Soy : Beverage
	{
		private IBeverage _beverage;

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Soy class.
		/// </summary>
		public Soy(Beverage beverage)
		{
			_beverage = beverage;

			base.Description = _beverage.Description + 
				string.Format("{0}����", _beverage.Description == string.Empty ? string.Empty : ", ");
			base.Cost = _beverage.Cost + 300;
		}
		#endregion
				
		public override double Cost
		{
			get
			{
				switch (Size)
				{
					case Coffee.BeverageSize.Tall:
						base.Cost += 55;

						break;
					case Coffee.BeverageSize.Grand:
						base.Cost += 85;

						break;
					case Coffee.BeverageSize.Venti:
						base.Cost += 95;

						break;
				}

				return base.Cost;
			}
			set
			{
				base.Cost = value;
			}
		}
	}
}