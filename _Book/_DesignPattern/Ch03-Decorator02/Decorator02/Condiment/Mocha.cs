using System;
using System.Collections.Generic;
using System.Text;
using Decorator02.Coffee;
using System.Collections;

namespace Decorator02.Condiment
{
	public class Mocha : Beverage
	{
		private IBeverage _beverage;

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Mocha class.
		/// </summary>
		public Mocha(Beverage beverage)
		{
			_beverage = beverage;

			base.Description = _beverage.Description + 
				string.Format("{0}��ī", _beverage.Description == string.Empty ? string.Empty : ", ");
			base.Cost = _beverage.Cost + 350;
		}
		#endregion

		public override double Cost
		{
			get
			{
				switch (Size)
				{
					case Coffee.BeverageSize.Tall:
						base.Cost += 15;

						break;
					case Coffee.BeverageSize.Grand:
						base.Cost += 35;

						break;
					case Coffee.BeverageSize.Venti:
						base.Cost += 65;

						break;
				}

				return base.Cost;
			}
			set
			{
				base.Cost = value;
			}
		}
	}
}
