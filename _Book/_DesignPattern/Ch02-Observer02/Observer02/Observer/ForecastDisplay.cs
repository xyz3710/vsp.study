using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 기상 예보 표시
	/// </summary>
	public class ForecastDisplay : IObserver, IDisplayElement
	{
		#region IObserver Members

		public void Update(WeatherData weatherData)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
