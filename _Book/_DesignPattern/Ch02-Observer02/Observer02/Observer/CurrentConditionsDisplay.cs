using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 현재 측정값을 화면에 표시
	/// </summary>
	public class CurrentConditionsDisplay : IObserver, IDisplayElement
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;
		private ISubject _subject;

		public CurrentConditionsDisplay(ISubject subject)
		{
			_subject = subject;

			_subject.RegisterObserver(this);
		}
	
		#region IObserver Members

		public void Update(WeatherData weatherData)
		{
			_temperature = weatherData.Temperature;
			_humidity = weatherData.Humidity;
			_pressure = weatherData.Pressure;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Current conditions : {0:F1}F degrees and {1:F1}% humidity",
				_temperature, _humidity);
		}

		#endregion
	}
}
