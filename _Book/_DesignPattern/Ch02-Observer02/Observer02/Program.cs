﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	class Program
	{
		static void Main(string[] args)
		{
			Subject subject = new Subject();
            
			CurrentConditionsDisplay currentConditionDisplay = new CurrentConditionsDisplay(subject);
			StatisticsDisplay statisticsDisplay = new StatisticsDisplay(subject);

			WeatherData weatherData = new WeatherData();

			weatherData.SetMeasurements(80, 65, 30.4f);
			subject.WeatherData = weatherData;
			
			weatherData.SetMeasurements(82, 70, 18.2f);
			subject.WeatherData = weatherData;

			weatherData.SetMeasurements(78, 90, 49.2f);
			subject.WeatherData = weatherData;
		}
	}
}
