using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Observer
{
	public class Subject : ISubject
	{
		private ArrayList _observerList;
		private WeatherData _weatherData;
		
		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Subject class.
		/// </summary>
		public Subject()
		{
			_observerList = new ArrayList();
		}
		#endregion        

		public WeatherData WeatherData
		{
			protected get
			{
				return _weatherData;
			}
			set
			{
				_weatherData = value;

				NotifyObserver();
			}
		}

		#region ISubject Members

		public void RegisterObserver(IObserver o)
		{
			_observerList.Add(o);
		}

		public void UnRegisterObserver(IObserver o)
		{
			int index = _observerList.IndexOf(o);

			if (index > 0)
				_observerList.RemoveAt(index);
		}

		public void NotifyObserver()
		{
			foreach (IObserver observer in _observerList)
				observer.Update(_weatherData);
		}

		#endregion

	}
}
