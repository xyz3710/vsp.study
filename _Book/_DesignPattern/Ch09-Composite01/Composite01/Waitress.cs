﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Composite01
{
	public class Waitress
	{
		private MenuComponent _allMenus;

		/// <summary>
		/// Initializes a new instance of the Waitress class.
		/// </summary>
		/// <param name="allMenus"></param>
		public Waitress(MenuComponent allMenus)
		{
			_allMenus = allMenus;
		}

		public void PrintMenu()
		{
			_allMenus.Print();
		}
	}
}
