using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Composite01
{
	public abstract class MenuComponent : IEnumerable
	{
		protected string _name;
		protected string _description;
		protected double _price;
		protected bool _isVegetarian;

		public virtual void Add(MenuComponent menuComponent)
		{
			throw new UnsupportedOperationException();
		}

		public virtual void Remove(MenuComponent menuComponent)
		{
			throw new UnsupportedOperationException();
		}

		public virtual MenuComponent GetChild(int i)
		{
			throw new UnsupportedOperationException();
		}

		public virtual void Print()
		{
			throw new UnsupportedOperationException();
		}

		public virtual string Name
		{
			get
			{
				throw new UnsupportedOperationException();
			}
		}

		public virtual string Description
		{
			get
			{
				throw new UnsupportedOperationException();
			}
		}

		public virtual double Price
		{
			get
			{
				throw new UnsupportedOperationException();
			}
		}

		public virtual bool IsVegetarian
		{
			get
			{
            	throw new UnsupportedOperationException();
            }
		}

		#region IEnumerable Members

		public virtual IEnumerator GetEnumerator()
		{
			throw new UnsupportedOperationException();
		}

		#endregion
	}
}
