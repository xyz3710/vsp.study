﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Composite01
{
	public class CompositeEnumerator : IEnumerator
	{
		private Stack _stack;

		/// <summary>
		/// CompositeEnumerator class의 instance를 생성합니다.
		/// </summary>
		public CompositeEnumerator(IEnumerable enumerator)
		{
			_stack.Push(enumerator);
		}

		#region IEnumerator Members

		public object Current
		{
			get
			{
				if (MoveNext() == true)
				{
					IEnumerator peekData = (IEnumerator)_stack.Peek();
					MenuComponent component = (MenuComponent)peekData.Current;

					if (component is Menu)
						_stack.Push(component.GetEnumerator());

					return component; 
				}
				else
					return null; 
			}
		}

		public bool MoveNext()
		{
			bool ret = false;

			if (_stack.Count > 0)
			{
				IEnumerator peekData = (IEnumerator)_stack.Peek();

				if (peekData.MoveNext() == true)
					ret = true;
				else
					return ((IEnumerator)_stack.Pop()).MoveNext();
			}

			return ret; 
		}

		public void Reset()
		{
			
		}

		#endregion
	}
}
