using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Composite01
{
	public class Menu : MenuComponent
	{
		private ArrayList _menuComponent;

		/// <summary>
		/// Initializes a new instance of the MenuComponent class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="description"></param>
		public Menu(string name, string description)
		{
			_name = name;
			_description = description;

			_menuComponent = new ArrayList();
		}

		public override void Add(MenuComponent menuComponent)
		{
			_menuComponent.Add(menuComponent);
		}

		public override void Remove(MenuComponent menuComponent)
		{
			_menuComponent.Remove(_menuComponent);
		}

		public override MenuComponent GetChild(int i)
		{
			return (MenuComponent)_menuComponent[i]; 
		}
		
		public override void Print()
		{
			string printForm = string.Format("\n{0}, {1}\r\n********** End of Print **********",
				Name, Description);

			Console.WriteLine(printForm);

			foreach (MenuComponent item in _menuComponent)
				item.Print();
		}

		public override string Name
		{
			get
			{
				return _name; 
			}
		}

		public override string Description
		{
			get
			{
				return _description; 
			}
		}

		//public override IEnumerator GetEnumerator()
		//{
		//    return new CompositeEnumerator(_menuComponent.GetEnumerator()); 
		//}
	}
}
