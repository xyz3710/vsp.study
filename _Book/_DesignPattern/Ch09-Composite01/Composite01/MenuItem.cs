using System;
using System.Collections.Generic;
using System.Text;

namespace Composite01
{
	public class MenuItem : MenuComponent
	{
		/// <summary>
		/// Initializes a new instance of the MenuItem class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="description"></param>
		/// <param name="isVegetarian"></param>
		/// <param name="price"></param>
		public MenuItem(string name, string description, bool isVegetarian, double price)
		{
			_name = name;
			_description = description;
			_price = price;
			_isVegetarian = isVegetarian;
		}

		public override void Print()
		{
			Console.Write(" " + Name);

			if (IsVegetarian == true)
				Console.Write("(v)");

			Console.Write(", {0}", Price);
			Console.WriteLine("  -- {0}", Description);
		}

		public override string Name
		{
			get
			{
				return _name; 
			}
		}

		public override string Description
		{
			get
			{
				return _description; 
			}
		}

		public override double Price
		{
			get
			{
				return _price; 
			}
		}

		public override bool IsVegetarian
		{
			get
			{
				return _isVegetarian; 
			}
		}
	}
}
