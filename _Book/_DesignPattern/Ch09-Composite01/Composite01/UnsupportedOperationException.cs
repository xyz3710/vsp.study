using System;
using System.Collections.Generic;
using System.Text;

namespace Composite01
{
	public class UnsupportedOperationException : Exception
	{
		private string _message;

		public UnsupportedOperationException()
			: base()
		{
		}

		public UnsupportedOperationException(string message)
			: base(message)
		{
			_message = message;
		}

		public override string Message
		{
			get
			{
				return _message;
			}
		}
	}
}
