using System;
using System.Collections.Generic;
using System.Text;

namespace Command01
{
	public interface ICommand
	{
		void Execute();
	}
}
