using System;
using System.Collections.Generic;
using System.Text;

namespace Command01
{
	public class GarageDoorOpenCommand : ICommand
	{
		private GarageDoor _garageDoor;

		public GarageDoorOpenCommand(GarageDoor garageDoor)
		{
			_garageDoor = garageDoor;
		}

		#region ICommand Members

		public void Execute()
		{
			_garageDoor.Up();
		}

		#endregion
	}
}
