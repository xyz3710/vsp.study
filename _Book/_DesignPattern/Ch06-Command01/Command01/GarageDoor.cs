﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command01
{
	public class GarageDoor
	{
		public void Up()
		{
			Console.WriteLine("Garage Door is Open");
		}

		public void Down()
		{
			Console.WriteLine("Garage Door is Close");
		}

		public void Stop()
		{
			Console.WriteLine("Emergency stop");
		}

		public void LightOn()
		{
			Console.WriteLine("Garage Light on");
		}

		public void LightOff()
		{
			Console.WriteLine("Garage Light off");
		}
	}
}
