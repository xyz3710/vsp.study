using System;
using System.Collections.Generic;
using System.Text;

namespace Command01
{
	public class RemoteControlTest
	{
		public static void Main(string[] args)
		{
			SimpleRemoteControl remote = new SimpleRemoteControl();

			Light light = new Light();
			LightOnCommand lightOnCommand = new LightOnCommand(light);

			GarageDoor garageDoor = new GarageDoor();
			GarageDoorOpenCommand garageDoorOpenCommand = new GarageDoorOpenCommand(garageDoor);

			remote.SetCommand(lightOnCommand);
			remote.ButtonWasPressed();

			remote.SetCommand(garageDoorOpenCommand);
			remote.ButtonWasPressed();
		}
	}
}
