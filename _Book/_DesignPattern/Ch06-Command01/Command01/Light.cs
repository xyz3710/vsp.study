using System;
using System.Collections.Generic;
using System.Text;

namespace Command01
{
	public class Light
	{
		public void On()
		{
			Console.WriteLine("Light is On");
		}

		public void Off()
		{
			Console.WriteLine("Light is Off");
		}
	}
}
