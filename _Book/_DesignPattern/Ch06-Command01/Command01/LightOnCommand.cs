using System;
using System.Collections.Generic;
using System.Text;

namespace Command01
{
	public class LightOnCommand : ICommand
	{
		private Light _light;

		public LightOnCommand(Light light)
		{
			_light = light;
		}
	
		#region ICommand Members

		public void Execute()
		{
			_light.On();
		}

		#endregion
	}
}
