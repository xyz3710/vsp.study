using System;
using System.Collections.Generic;
using System.Text;
using Factory_Simple;
using FactorySimple01.Ingredient;

namespace FactorySimple01.NewYork
{
	public class NYStyleCheesePizza : Pizza
	{
		IPizzaIngredientFactory _ingredientFactory;
        
		/// <summary>
		/// Initializes a new instance of the NYStyleCheesePizza class.
		/// </summary>
		public NYStyleCheesePizza(IPizzaIngredientFactory ingredient)
		{
			Name = "����ǳ�� �ҽ��� ġ�� ����";

			_ingredientFactory = ingredient;

			Topping = "�����Ƴ� ġ� ��� ����";
		}

		public override void Prepare()
		{
			Console.WriteLine("Preparing {0}", Name);

			Dough = _ingredientFactory.CreateDough();
			Sauce = _ingredientFactory.CreateSauce();
			Cheese = _ingredientFactory.CreateCheese();
			Clam = _ingredientFactory.CreateClam();
			Pepperoni = _ingredientFactory.CreatePepperoni();
		}
	}
}
