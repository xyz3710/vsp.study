using System;
using System.Collections.Generic;
using System.Text;
using FactorySimple01.Ingredient;

namespace FactorySimple01.NewYork
{
	public class NYPizzaIngredientFactory : IPizzaIngredientFactory
	{
		#region IPizzaIngredientFactory ���

		public IDough CreateDough()
		{
			return new ThinCrustDough().CreateDough();
		}

		public ISauce CreateSauce()
		{
			return new MarinaraSauce().CreateSauce();
		}

		public ICheese CreateCheese()
		{
			return new ReggianoCheese().CreateCheese();
		}

		public IVeggies[] CreateVeggies()
		{
			Veggies[] veggies = new Veggies[] { new Garlic(), new Onion(), new Mushroom(), new Garlic() };
			
            
			return veggies;
		}

		public IPepperoni CreatePepperoni()
		{
			return new GreenPepperoni().CreatePepperoni();
		}

		public IClam CreateClam()
		{
			return new FreshClams().CreateClams();
		}

		#endregion
	}
}