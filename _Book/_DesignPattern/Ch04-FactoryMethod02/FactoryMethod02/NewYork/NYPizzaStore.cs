using System;
using System.Collections.Generic;
using System.Text;
using Factory_Simple;
using FactorySimple01.Ingredient;

namespace FactorySimple01.NewYork
{
	public class NYPizzaStore : PizzaStore
	{
		public override Pizza CreatePizza(PizzaType pizzaType)
		{
			Pizza pizza = null;
			IPizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

			switch (pizzaType)
			{
				case PizzaType.Cheese:
					pizza = new NYStyleCheesePizza(ingredientFactory);

					break;
				case PizzaType.Pepperoni:
					

					break;
				case PizzaType.Clam:
					

					break;
				case PizzaType.Veggie:
					

					break;
			}

			return pizza;
		}
	}
}
