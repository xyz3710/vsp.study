﻿using System;
using System.Collections.Generic;
using System.Text;
using FactorySimple01.NewYork;
using FactorySimple01.Chicago;

namespace Factory_Simple
{
	class Program
	{
		static void Main(string[] args)
		{
			PizzaStore nyStore = new NYPizzaStore();
			PizzaStore chiagoStore = new ChicagoPizzaStore();

			Pizza pizza = nyStore.OrderPizza(PizzaType.Cheese);
			Console.WriteLine("영희가 {0} 피자를 주문했다\n", pizza.Name);

			pizza = chiagoStore.OrderPizza(PizzaType.Cheese);
			Console.WriteLine("철수가 {0} 피자를 주문했다\n", pizza.Name);
		}
	}
}
