using System;
using System.Collections.Generic;
using System.Text;

namespace FactorySimple01.Ingredient
{
	public interface IPizzaIngredientFactory
	{
		IDough CreateDough();

		ISauce CreateSauce();

		ICheese CreateCheese();

		IVeggies[] CreateVeggies();

		IPepperoni CreatePepperoni();

		IClam CreateClam();
	}
}
