using System;
using System.Collections.Generic;
using System.Text;
using Factory_Simple;
using FactorySimple01.Ingredient;

namespace FactorySimple01.Chicago
{
	public class ChicagoPizzaStore : PizzaStore
	{
		public override Pizza CreatePizza(PizzaType pizzaType)
		{
			Pizza pizza = null;
			IPizzaIngredientFactory ingredientFactory = new ChicagoPizzaIngredientFactory();

			switch (pizzaType)
			{
				case PizzaType.Cheese:
					pizza = new ChicagoStyleCheesePizza(ingredientFactory);

					break;
				case PizzaType.Pepperoni:


					break;
				case PizzaType.Clam:


					break;
				case PizzaType.Veggie:


					break;
			}

			return pizza;
		}
	}
}
