using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Simple
{
	public abstract class PizzaStore
	{
		public PizzaStore()
		{
		}

		public Pizza OrderPizza(PizzaType pizzaType)
		{
			Pizza pizza = CreatePizza(pizzaType);

			pizza.Prepare();
			
			pizza.Bake();
			pizza.Cut();
			pizza.Box();

			Console.WriteLine("<<< {0}를 만들었습니다. >>>\r\n", pizza.GetType().Name);

			return pizza;
		}

		public abstract Pizza CreatePizza(PizzaType pizzaType);
	}
}
