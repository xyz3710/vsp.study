using System;
using System.Collections.Generic;
using System.Text;
using FactorySimple01.Ingredient;

namespace Factory_Simple
{
	public class PepperoniPizza : Pizza
	{
		IPizzaIngredientFactory _ingredientFactory;
		
		/// <summary>
		/// Initializes a new instance of the VeggiePizza class.
		/// </summary>
		public PepperoniPizza(IPizzaIngredientFactory ingredientFactory)
		{
			_ingredientFactory = ingredientFactory;
		}

		public override void Prepare()
		{
			Console.WriteLine("Preparing {0}", Name);

			Dough = _ingredientFactory.CreateDough();
			Sauce = _ingredientFactory.CreateSauce();
			Cheese = _ingredientFactory.CreateCheese();
			Clam = _ingredientFactory.CreateClam();
			Pepperoni = _ingredientFactory.CreatePepperoni();
		}
	}
}
