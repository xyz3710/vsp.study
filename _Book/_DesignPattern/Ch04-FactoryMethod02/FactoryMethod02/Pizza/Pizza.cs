using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using FactorySimple01.Ingredient;

namespace Factory_Simple
{
	public abstract class Pizza
	{
		private string _name;
		private IDough _dough;
		private ISauce _sauce;
		private IVeggies _veggies;
		private ICheese _cheese;
		private IPepperoni _pepperoni;
		private IClam _clam;
		private ArrayList _alTopping = new ArrayList();

		public abstract void Prepare();

		public virtual void Bake()
		{
			Console.WriteLine("350도에서 25분동안 빵 굽기");
		}

		public virtual void Cut()
		{
			Console.WriteLine("원형으로 조각 내기");
		}

		public virtual void Box()
		{
			Console.WriteLine("피자 가계 박스에 피자 놓기");
		}

		#region Properties
		/// <summary>
		/// Get or set Name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Get or set Dough.
		/// </summary>
		public IDough Dough
		{
			get
			{
				return _dough;
			}
			set
			{
				_dough = value;
			}
		}

		/// <summary>
		/// Get or set Sauce.
		/// </summary>
		public ISauce Sauce
		{
			get
			{
				return _sauce;
			}
			set
			{
				_sauce = value;
			}
		}

		/// <summary>
		/// Get or set Veggies.
		/// </summary>
		public IVeggies Veggies
		{
			get
			{
				return _veggies;
			}
			set
			{
				_veggies = value;
			}
		}

		/// <summary>
		/// Get or set Cheese.
		/// </summary>
		public ICheese Cheese
		{
			get
			{
				return _cheese;
			}
			set
			{
				_cheese = value;
			}
		}

		/// <summary>
		/// Get or set Pepperoni.
		/// </summary>
		public IPepperoni Pepperoni
		{
			get
			{
				return _pepperoni;
			}
			set
			{
				_pepperoni = value;
			}
		}

		/// <summary>
		/// Get or set Clam.
		/// </summary>
		public IClam Clam
		{
			get
			{
				return _clam;
			}
			set
			{
				_clam = value;
			}
		}

		public string Topping
		{
			get
			{
				StringBuilder topping = new StringBuilder();

				foreach (string item in _alTopping)
					topping.AppendFormat("{0}\n\t", item);

				return topping.ToString();
			}
			set
			{
				_alTopping.Add(value);
			}
		}
		#endregion
	}
}
