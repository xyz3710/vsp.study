using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Simple
{
	public enum PizzaType
	{
		Cheese,
		Pepperoni,
		Clam,
		Veggie,
	}
}
