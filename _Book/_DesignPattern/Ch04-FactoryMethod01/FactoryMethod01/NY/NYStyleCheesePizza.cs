using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public class NYStyleCheesePizza : Pizza
	{
		public NYStyleCheesePizza()
		{
			_name = "NY Style Sauce and Cheese Pizza";
			_dough = "Thin Crust Dough";
			_sauce = "Marinara Sauce";
			_topping.Add("Grated Reggiano Cheese");
		}
	
	}
}
