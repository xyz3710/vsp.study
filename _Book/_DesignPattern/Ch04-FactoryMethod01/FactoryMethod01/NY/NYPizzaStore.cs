using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public class NYPizzaStore : PizzaStore
	{
		protected override FactoryMethod.Pizza CreatePizza(FactoryMethod.PizzaType type)
		{
			Pizza pizza = null;

			switch (type)
			{
				case PizzaType.Cheese:
					pizza = new NYStyleCheesePizza();

					break;
				case PizzaType.Pepperoni:
					//return new NYStylePepperoniPizza();

					break;
				case PizzaType.Clam:
					//return new NYStyleClamPizza();

					break;
				case PizzaType.Veggie:
					//return new NYStyleVeggiePizza();

					break;
				default:
					return null;

					break;
			}

			return pizza;
		}
	}
}
