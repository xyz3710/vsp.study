using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public class ChicagoPizzaStore : PizzaStore
	{
		protected override FactoryMethod.Pizza CreatePizza(FactoryMethod.PizzaType type)
		{
			Pizza pizza = null;

			switch (type)
			{
				case PizzaType.Cheese:
					return new ChicagoStyleCheesePizza();

					break;
				case PizzaType.Pepperoni:
					//return new ChicagoStylePepperoniPizza();

					break;
				case PizzaType.Clam:
					//return new ChicagoStyleClamPizza();

					break;
				case PizzaType.Veggie:
					//return new ChicagoStyleVeggiePizza();

					break;
				default:
					return null;

					break;
			}

			return pizza;
		}
	}
}
