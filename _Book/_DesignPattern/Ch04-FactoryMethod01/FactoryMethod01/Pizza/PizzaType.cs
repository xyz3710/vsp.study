using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public enum PizzaType
	{
		Cheese,
		Pepperoni,
		Clam,
		Veggie,
	}
}
