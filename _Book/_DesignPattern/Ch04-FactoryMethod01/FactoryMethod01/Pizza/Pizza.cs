using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace FactoryMethod
{
	public abstract class Pizza
	{
		protected string _name;
		protected string _dough;
		protected string _sauce;
		protected ArrayList _topping = new ArrayList();

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public virtual void Prepare()
		{
			Console.WriteLine("Prepareing {0}", _name);
			Console.WriteLine("Tossing dough...");
			Console.WriteLine("Adding sauce...");

			foreach (object obj in _topping)
				Console.WriteLine("\t{0}", obj);
			
		}

		public virtual void Bake()
		{
			Console.WriteLine("Bake for 25 minutes at 350 degree");
		}

		public virtual void Cut()
		{
			Console.WriteLine("Cutting the pizza into diagonal slices");
		}

		public virtual void Box()
		{
			Console.WriteLine("Place pizza in official PizzaStore box");
		}
	}
}
