﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Decorator
{
	class Decorator01TestDriver
	{
		static void Main(string[] args)
		{
			ShowPrice();

			Beverage espresso = new Espresso();

			PrintOrder(espresso);
			
			
			Beverage darkRoast = new DarkRoast();

			darkRoast = new Mocha(darkRoast);
			darkRoast = new Mocha(darkRoast);
			darkRoast = new Whip(darkRoast);

			PrintOrder(darkRoast);

			
			Beverage houseBlend = new HouseBlend();

			houseBlend = new Soy(houseBlend);
			houseBlend = new Mocha(houseBlend);
			houseBlend = new Whip(houseBlend);
			
			houseBlend.Size = BeverageSize.Grand;

			houseBlend = new Milk(houseBlend);

			PrintOrder(houseBlend);
		}

		private static void PrintOrder(Beverage beverage)
		{
			Console.WriteLine("{0} : ${1}", beverage.Description, beverage.Cost);
		}

		private static void ShowPrice()
		{
			Assembly asm = Assembly.GetExecutingAssembly();
			Type[] types = asm.GetTypes();
			List<Beverage> beverages = new List<Beverage>();
			List<Beverage> condiments = new List<Beverage>();
			Beverage beverage = null;

			foreach (Type type in types)
			{
				if (type.IsClass == true && type.BaseType.Name == "Beverage")
				{
					ConstructorInfo ci = type.GetConstructor(Type.EmptyTypes);

					// Parameter가 없을 경우 Coffe
					if (ci != null)
					{
						beverage = (Beverage)Activator.CreateInstance(type);

						if (beverage != null)
							beverages.Add(beverage);
					}
				}
				if (type.IsClass == true && type.IsAbstract == false && type.BaseType.Name == "CondimentDecorator")
				{
					beverage = (Beverage)Activator.CreateInstance(type, new object[] { new CondimentDump() });

					if (beverage != null)
						condiments.Add(beverage);
				}
			}

			Console.WriteLine("{0,40}", "Coffe");

			foreach (Beverage coffe in beverages)
				PrintOrder(coffe);

			Console.WriteLine("\n{0,40}", "Condiment");

			foreach (Beverage condiment in condiments)
				PrintOrder(condiment);

			Console.WriteLine();
		}
	}
}
