using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Whip : CondimentDecorator
	{
		public Whip(Beverage beverage)
			: base(beverage)
		{
		}

		public override string Description
		{
			get
			{
				return Beverage.Description + ", Whip";
			}
		}

		public override double Cost
		{
			get
			{
				return Beverage.Cost + 0.1;
			}
		}
	}
}
