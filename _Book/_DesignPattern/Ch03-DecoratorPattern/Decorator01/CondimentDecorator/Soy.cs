using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Soy : CondimentDecorator
	{
		public Soy(Beverage beverage)
			: base(beverage)
		{
		}

		public override string Description
		{
			get
			{
				return Beverage.Description + ", Soy";
			}
		}

		public override double Cost
		{
			get
			{
				return Beverage.Cost + 0.15;
			}
		}
	}
}
