using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Milk : CondimentDecorator
	{
		public Milk(Beverage beverage)
			: base(beverage)
		{
		}

		public override string Description
		{
			get
			{
				return Beverage.Description + ", Milk";
			}
		}

		public override double Cost
		{
			get
			{
				double cost = Beverage.Cost + 0.1;

				switch (Beverage.Size)
				{
					case BeverageSize.Tall:
						cost += 0.1;

						break;
					case BeverageSize.Grand:
						cost += 0.15;

						break;
					case BeverageSize.Venti:
						cost += 0.2;

						break;
				}

				return cost;
			}
		}
	}
}
