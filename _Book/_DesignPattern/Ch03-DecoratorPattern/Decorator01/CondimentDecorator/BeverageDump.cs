using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class CondimentDump : Beverage
	{		
		public override string Description
		{
			get
			{
				return "Condiment";
			}
			protected set
			{
				base.Description = value;
			}
		}

		public override double Cost
		{
			get
			{
				return 0d;
			}
		}
	}
}
