using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class HouseBlend : Beverage
	{
		public HouseBlend()
		{
			Description = "House Blend Coffee";
		}

		public override double Cost
		{
			get
			{
				return 0.89;
			}
		}
	}
}
