using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class DarkRoast : Beverage
	{
		public DarkRoast()
		{
			Description = "Dark Roast Coffee";
		}

		public override double Cost
		{
			get
			{
				return 0.99;
			}
		}
	}
}
