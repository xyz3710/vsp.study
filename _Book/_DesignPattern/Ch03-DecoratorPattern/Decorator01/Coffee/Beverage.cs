using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public abstract class Beverage
	{
		private string _description;
		private BeverageSize _beverageSize;

		public Beverage()
		{
			_description = "Untitled";
		}

		public virtual string Description
		{
			get
			{
				return _description;
			}
			protected set
			{
				_description = value;
			}
		}

		public virtual BeverageSize Size
		{
			get
			{
				return _beverageSize;
			}
			set
			{
				_beverageSize = value;		
			}
		}

		public abstract double Cost
		{
			get;
		}
	}
}
