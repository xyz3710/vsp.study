using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Decaf : Beverage
	{
		public Decaf()
		{
			Description = "Decaf";
		}

		public override double Cost
		{
			get
			{
				return 1.05;
			}
		}
	}
}
