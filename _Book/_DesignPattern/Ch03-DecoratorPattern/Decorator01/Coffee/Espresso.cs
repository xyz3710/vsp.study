using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Espresso : Beverage
	{
		public Espresso()
		{
			Description = "Espresso Coffee";
		}

		public override double Cost
		{
			get
			{
				return 1.99;
			}
		}
	}
}
