using System;
using System.Collections.Generic;
using System.Text;
using Decorator02.Coffee;
using System.Collections;

namespace Decorator02.Condiment
{
	public class Whip : Beverage
	{
		private IBeverage _beverage;

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Whip class.
		/// </summary>
		public Whip(Beverage beverage)
		{
			_beverage = beverage;

			base.Description = _beverage.Description + 
				string.Format("{0}����ũ��", _beverage.Description == string.Empty ? string.Empty : ", ");
			base.Cost = _beverage.Cost + 250;
		}
		#endregion
	}
}
