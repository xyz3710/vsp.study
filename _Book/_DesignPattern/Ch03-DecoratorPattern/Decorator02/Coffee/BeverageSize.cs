using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator02.Coffee
{
	public enum BeverageSize
	{
		Tall,
		Grand,
		Venti,
	}
}
