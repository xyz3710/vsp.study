using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator02.Coffee
{
	public class DarkRoast : Beverage
	{
		#region Constructor
		/// <summary>
		/// Initializes a new instance of the DarkRoast class.
		/// </summary>
		public DarkRoast()
		{
			Description = "다크 로스트";
			Cost = 950;
		}
		#endregion
	}
}
