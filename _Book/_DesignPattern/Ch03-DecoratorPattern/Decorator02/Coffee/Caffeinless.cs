using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator02.Coffee
{
	public class Caffeinless : Beverage
	{
		#region Constructor
		/// <summary>
		/// Initializes a new instance of the Caffeinless class.
		/// </summary>
		public Caffeinless()
		{
			Description = "��ī����";
			Cost = 1150;
		}
		#endregion
	}
}
