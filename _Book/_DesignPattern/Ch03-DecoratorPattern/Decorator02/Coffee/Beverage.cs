using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator02.Coffee
{
	public class Beverage : IBeverage
	{
		private string _description;
		private double _cost;
		private BeverageSize _beverageSize;

		/// <summary>
		/// Initializes a new instance of the Beverage class.
		/// </summary>
		public Beverage()
		{
			_description = string.Empty;
			_cost = 0.0;
		}

		#region Properties
		/// <summary>
		/// Get or set Description.
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		/// <summary>
		/// Get or set Cost.
		/// </summary>
		public virtual double Cost
		{
			get
			{
				return _cost;
			}
			set
            {
            	_cost = value;
            }
		}

		/// <summary>
		/// Get or set BeverageSize.
		/// </summary>
		public BeverageSize Size
		{
			get
			{
				return _beverageSize;
			}
			set
			{
				_beverageSize = value;
			}
		}
		#endregion
	}
}
