using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator02.Coffee
{
	interface IBeverage
	{
		string Description
		{
			get;
			set;
		}

		double Cost
		{
			get;
			set;
		}

		BeverageSize Size
		{
			get;
			set;
		}
	}
}