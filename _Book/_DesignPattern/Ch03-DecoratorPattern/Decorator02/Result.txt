<< Test Code >>
Beverage espresso = new Espresso();
PrintOrder(espresso);

Beverage darkRoast = new DarkRoast();

darkRoast = new Mocha(darkRoast);
darkRoast = new Mocha(darkRoast);
darkRoast = new Whip(darkRoast);

PrintOrder(darkRoast);

Beverage houseBlend = new HouseBlend();

houseBlend = new Soy(houseBlend);
houseBlend = new Mocha(houseBlend);
houseBlend = new Whip(houseBlend);

houseBlend.Size = BeverageSize.Grand;

houseBlend = new Milk(houseBlend);

PrintOrder(houseBlend);

<< Result >>
                                  커    피
다크 로스트                         950 원
에스프레소                        1,000 원
다방 커피                           800 원
디카페인                          1,150 원

                                   첨 가 물
두유                                355 원
모카                                365 원
휘핑크림                            250 원
우유                                150 원

에스프레소                        1,000 원
다크 로스트, 모카, 모카, 휘핑크림         1,930 원
다방 커피, 두유, 모카, 휘핑크림, 우유     1,920 원