using System;
using System.Collections.Generic;
using System.Text;
using Decorator02.Coffee;
using Decorator02.Condiment;
using System.Reflection;

namespace Decorator02
{
	class Decorator02TestDriver
	{
		static void Main(string[] args)
		{
			ShowPrice();

			Beverage espresso = new Espresso();

			PrintOrder(espresso);


			Beverage darkRoast = new DarkRoast();

			darkRoast = new Mocha(darkRoast);
			darkRoast = new Mocha(darkRoast);
			darkRoast = new Whip(darkRoast);

			PrintOrder(darkRoast);


			Beverage houseBlend = new HouseBlend();

			houseBlend = new Soy(houseBlend);
			houseBlend = new Mocha(houseBlend);
			houseBlend = new Whip(houseBlend);

			houseBlend.Size = BeverageSize.Grand;

			houseBlend = new Milk(houseBlend);

			PrintOrder(houseBlend);
		}

		private static void PrintOrder(Beverage beverage)
		{
			Console.WriteLine("{0,-24}\t{1,7:#,##0} 원", beverage.Description, beverage.Cost);
		}

		private static void ShowPrice()
		{
			Assembly asm = Assembly.GetExecutingAssembly();
			Type[] types = asm.GetTypes();
			List<Beverage> beverages = new List<Beverage>();
			List<Beverage> condiments = new List<Beverage>();

			foreach (Type type in types)
			{
				if (type.IsClass == true && type.BaseType.Name == "Beverage")
				{
					ConstructorInfo ci = type.GetConstructor(Type.EmptyTypes);
					Beverage beverage = null;

					// Parameter가 없을 경우 커피
					if (ci != null)
					{
						beverage = (Beverage)Activator.CreateInstance(type);

						if (beverage != null)
							beverages.Add(beverage);
					}
					else
					{
						beverage = (Beverage)Activator.CreateInstance(type, new object[] { new Beverage() });

						if (beverage != null)
							condiments.Add(beverage);
					}
				}
			}

			Console.WriteLine("{0,40}", "커    피");
			
			foreach (Beverage coffe in beverages)
				PrintOrder(coffe);

			Console.WriteLine("\n{0,40}", "첨 가 물");
			
			foreach (Beverage condiment in condiments)
				PrintOrder(condiment);

			Console.WriteLine();
		}
	}
}
