using System;
using System.Collections.Generic;
using System.Text;

namespace TemplateMethod01
{
	public abstract class CaffeineBeverage
	{
		public void PrepareRecipe()
		{
			BoilWater();
			Brew();
			PourInCup();

			if (CustomerWantsCondiments() == true)
				AddCondiments();
		}

		public void BoilWater()
		{
			Console.WriteLine("�� ���̴� ��");
		}

		public void PourInCup()
		{
			Console.WriteLine("�ſ� ������ ��");
		}

		public abstract void Brew();

		public abstract void AddCondiments();

		public virtual bool CustomerWantsCondiments()
		{
			return true;
		}
	}
}
