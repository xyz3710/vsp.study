﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemplateMethod01
{
	class Program
	{
		static void Main(string[] args)
		{
			Tea tea = new Tea();

			tea.PrepareRecipe();

			Console.WriteLine();


			Tea2 tea2 = new Tea2();

			tea2.PrepareRecipe();

			Console.WriteLine();
			
			
			Coffee coffee = new Coffee();

			coffee.PrepareRecipe();

			Console.WriteLine();


			CoffeeWithHook coffeeHook = new CoffeeWithHook();

			coffeeHook.PrepareRecipe();
		}
	}
}
