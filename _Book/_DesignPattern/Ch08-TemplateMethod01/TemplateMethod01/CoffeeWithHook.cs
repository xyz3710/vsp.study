using System;
using System.Collections.Generic;
using System.Text;

namespace TemplateMethod01
{
	public class CoffeeWithHook : CaffeineBeverage
	{
		public override void Brew()
		{
			Console.WriteLine("필터로 커피를 우려내는 중");
		}

		public override void AddCondiments()
		{
			Console.WriteLine("우유와 설탕을 추가하는 중");
		}

		public override bool CustomerWantsCondiments()
		{
			string answer = getUserInput();
			bool ret = false;

			if (answer.ToLower() == "y")
				ret = true;
			else
				ret = false;

			return ret;
		}

		private string getUserInput()
		{
			string answer = string.Empty;

			Console.WriteLine("커피에 우유와 설탕을 넣어드릴까요?(y/n) ");
			answer = Console.ReadLine();

			return answer.Substring(0, 1);
		}
	}
}
