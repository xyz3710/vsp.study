﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemplateMethod01
{
	public class Tea : CaffeineBeverage
	{
		public override void Brew()
		{
			Console.WriteLine("차를 우려내는 중");
		}

		public override void AddCondiments()
		{
			Console.WriteLine("레몬을 추가하는 중");
		}
	}
}
