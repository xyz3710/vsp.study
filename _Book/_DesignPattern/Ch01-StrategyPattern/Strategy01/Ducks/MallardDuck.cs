using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class MallardDuck : Duck
	{
		public MallardDuck()
		{
			QuackBehavior = new QuackSound();
			FlyBehavior = new FlyWithWings();
		}
	
		public override void Display()
		{
			Console.WriteLine("MallardDuck");
		}
	}
}
