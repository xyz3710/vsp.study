﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public abstract class Duck
	{
		#region Fields
		private IQuackBehavior _quackBehavior;
		private IFlyBehavior _flyBehavior;
		#endregion

		#region Constructors
		/// <summary>
		/// 생성자
		/// </summary>
		public Duck()
		{
			Console.WriteLine("\n오리 생성자");

			_quackBehavior = null;
			_flyBehavior = null;
		}
		#endregion

		#region Destructor
		/// <summary>
		/// 소멸자
		/// </summary>
		~Duck()
		{
			Console.WriteLine("오리 소멸자");
		}
		#endregion

		#region Properties
		public IQuackBehavior QuackBehavior
		{
			get
			{
				return _quackBehavior;
			}
			set
			{
				_quackBehavior = value;
			}
		}

		public IFlyBehavior FlyBehavior
		{
			get
			{
				return _flyBehavior;
			}
			set
			{
				_flyBehavior = value;
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// 수영
		/// </summary>
		public void Swim()
		{
			Console.WriteLine("모든 오리는 물에 뜬다. 가짜 오리도 물에 뜬다.");
		}

		/// <summary>
		/// 오리모양
		/// </summary>
		public abstract void Display();

		/// <summary>
		/// 소리내는 동작
		/// </summary>
		public void PerformQuack()
		{
			if (_quackBehavior != null)
				_quackBehavior.Quack();
		}

		/// <summary>
		/// 나는 동작
		/// </summary>
		public void PerformFly()
		{
			if (_flyBehavior != null)
				_flyBehavior.Fly();
		}
		#endregion
	}
}
