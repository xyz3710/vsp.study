using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class RedheadDuck : Duck
	{
		public RedheadDuck()
		{
			QuackBehavior = new Squeak();
			FlyBehavior = new FlyWithWings();
		}
	
		public override void Display()
		{
			Console.WriteLine("RedheadDuck");
		}
	}
}
