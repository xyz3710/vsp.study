using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class RubberDuck : Duck
	{
		public RubberDuck()
		{
			QuackBehavior = new QuackSound();
			FlyBehavior = new FlyNoWay();
		}
	
		public override void Display()
		{
			Console.WriteLine("RubberDuck");
		}
	}
}
