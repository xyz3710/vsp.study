using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class FlyWithWings : IFlyBehavior
	{
		#region IFlyBehavior Members

		public void Fly()
		{
			Console.WriteLine("날고 있어요");
		}

		#endregion
	}
}
