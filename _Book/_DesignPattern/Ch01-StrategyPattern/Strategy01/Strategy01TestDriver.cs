﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	class Strategy01TestDriver
	{
		static void Main(string[] args)
		{
			MallardDuck mallardDuck = new MallardDuck();

			mallardDuck.Display();
			mallardDuck.PerformQuack();
			mallardDuck.PerformFly();
			
			RedheadDuck redHeadDuck = new RedheadDuck();

			redHeadDuck.Display();
			redHeadDuck.PerformQuack();
			redHeadDuck.PerformFly();


			RubberDuck rubberDuck = new RubberDuck();

			rubberDuck.Display();
			rubberDuck.PerformQuack();
			rubberDuck.PerformFly();


			ModelDuck modelDuck = new ModelDuck();

			modelDuck.Display();
			modelDuck.PerformQuack();
			modelDuck.PerformFly();

			modelDuck.FlyBehavior = new FlyRocketPowered();

			modelDuck.PerformFly();

			Console.WriteLine();
		}
	}
}
