﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	class Strategy02TestDriver
	{
		static void Main(string[] args)
		{
			King king = new King();

			king.Weapon = new Axe();
			king.Fight();


			Knight knight = new Knight();

			knight.Weapon = null;
			knight.Fight();
			knight.Weapon = new Knife();
			knight.Fight();


			Queen queen = new Queen();

			queen.Fight();

			
			Troll troll = new Troll();

			troll.Fight();
		}
	}
}
