using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class BowAndArrow : IWeapon
	{
		#region IWeapon Members

		public void UseWeapon()
		{
			Console.WriteLine("\t활을 써서 화살을 발사한다");
		}

		#endregion
	}
}
