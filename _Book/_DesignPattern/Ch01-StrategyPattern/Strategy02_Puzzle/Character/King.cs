using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class King : Character
	{
		public King()
			: base("King", new Knife())
		{
			
		}
	}
}
