using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
	public class Character
	{
		private IWeapon _weapon;
		private string _characterClass;

		/// <summary>
		/// Initializes a new instance of the Character class.
		/// </summary>
		/// <param name="characterClass"></param>
		/// <param name="weapon"></param>
		public Character(string characterClass, IWeapon weapon)
		{
			_characterClass = characterClass;
			_weapon = weapon;
		}

		/// <summary>
		/// Initializes a new instance of the Character class.
		/// </summary>
		/// <param name="characterClass"></param>
		public Character(string characterClass)
		{
			_characterClass = characterClass;
		}

		public void Fight()
		{
			Console.WriteLine(_characterClass + "�� �ο�ϴ�");

			if (_weapon != null)
				_weapon.UseWeapon();
            else
				Console.WriteLine("\t���Ⱑ �����ϴ�.(���ָ����� �ο�ϴ�.)");
		}

		public IWeapon Weapon
		{
			get
			{
				return _weapon;
			}
			set
			{
				_weapon = value;
			}
		}
	}
}
