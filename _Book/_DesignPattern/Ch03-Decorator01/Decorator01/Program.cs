﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	class Program
	{
		static void Main(string[] args)
		{
//			ConfigurationSettings config = new ConfigurationSettings();
//
//			config.OpenFile();
//			config.SetSection("Tall");


			Beverage espresso = new Espresso();

			Console.WriteLine("{0} ${1}", espresso.Description, espresso.Cost);

			
			Beverage darkRoast = new DarkRoast();

			darkRoast = new Mocha(darkRoast);
			darkRoast = new Mocha(darkRoast);
			darkRoast = new Whip(darkRoast);

			Console.WriteLine("{0} ${1}", darkRoast.Description, darkRoast.Cost);


			Beverage houseBlend = new HouseBlend();

			houseBlend = new Soy(houseBlend);
			houseBlend = new Mocha(houseBlend);
			houseBlend = new Whip(houseBlend);
			
			houseBlend.Size = BeverageSize.Grand;

			houseBlend = new Milk(houseBlend);

			Console.WriteLine("{0} ${1}", houseBlend.Description, houseBlend.Cost);
		}
	}
}
