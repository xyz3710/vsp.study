using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class DarkRoast : Beverage
	{
		public DarkRoast()
		{
			_description = "Dark Roast Coffee";
		}

		public override string Description
		{
			get
			{
				return _description;
			}
		}
		public override double Cost
		{
			get
			{
				return 0.99;
			}
		}
	}
}
