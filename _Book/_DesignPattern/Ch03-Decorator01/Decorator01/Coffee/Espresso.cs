using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Espresso : Beverage
	{
		public Espresso()
		{
			_description = "Espresso Coffee";
		}

		public override string Description
		{
			get
			{
				return _description;
			}
		}

		public override double Cost
		{
			get
			{
				return 1.99;
			}
		}
	}
}
