using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public enum BeverageSize
	{
		Tall,
		Grand,
		Venti,
	}
}
