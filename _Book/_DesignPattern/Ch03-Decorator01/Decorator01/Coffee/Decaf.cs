using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Decaf : Beverage
	{
		public Decaf()
		{
			_description = "Decaf";
		}

		public override string Description
		{
			get
			{
				return _description;
			}
		}
		public override double Cost
		{
			get
			{
				return 1.05;
			}
		}
	}
}
