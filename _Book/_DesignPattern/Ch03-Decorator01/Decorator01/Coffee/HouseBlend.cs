using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class HouseBlend : Beverage
	{
		public HouseBlend()
		{
			_description = "House Blend Coffee";
		}

		public override string Description
		{
			get
			{
				return _description;
			}
		}
		public override double Cost
		{
			get
			{
				return 0.89;
			}
		}
	}
}
