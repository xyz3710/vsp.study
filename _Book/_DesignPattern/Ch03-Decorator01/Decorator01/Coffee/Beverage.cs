using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public abstract class Beverage
	{
		protected string _description;
		protected BeverageSize _beverageSize;

		public Beverage()
		{
			_description = "Untitled";
		}

		public virtual BeverageSize Size
		{
			get
			{
				return _beverageSize;
			}
			set
			{
				_beverageSize = value;		
			}
		}

		public virtual string Description
		{
			get
			{
				return _description;
			}
		}

		public abstract double Cost
		{
			get;
		}
	}
}
