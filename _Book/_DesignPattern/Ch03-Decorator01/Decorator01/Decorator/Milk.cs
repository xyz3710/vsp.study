using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Milk : CondimentDecorator
	{
		private Beverage _beverage;

		public Milk(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string Description
		{
			get
			{
				return _beverage.Description + ", Milk";
			}
		}

		public override double Cost
		{
			get
			{
				double cost = _beverage.Cost + 0.1;

				switch (_beverage.Size)
				{
					case BeverageSize.Tall:
						cost += 0.1;

						break;
					case BeverageSize.Grand:
						cost += 0.15;

						break;
					case BeverageSize.Venti:
						cost += 0.2;

						break;
				}

				return cost;
			}
		}
	}
}
