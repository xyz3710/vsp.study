using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Soy : CondimentDecorator
	{
		private Beverage _beverage;

		public Soy(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string Description
		{
			get
			{
				return _beverage.Description + ", Soy";
			}
		}

		public override double Cost
		{
			get
			{
				return _beverage.Cost + 0.15;
			}
		}
	}
}
