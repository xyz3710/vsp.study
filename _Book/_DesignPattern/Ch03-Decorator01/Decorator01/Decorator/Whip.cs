using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
	public class Whip : CondimentDecorator
	{
		private Beverage _beverage;

		public Whip(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string Description
		{
			get
			{
				return _beverage.Description + ", Whip";
			}
		}

		public override double Cost
		{
			get
			{
				return _beverage.Cost + 0.1;
			}
		}
	}
}
