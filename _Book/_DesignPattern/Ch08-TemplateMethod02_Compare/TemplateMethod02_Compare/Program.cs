﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemplateMethod02_Compare
{
	class Program
	{
		static void Main(string[] args)
		{
			Duck[] ducks = {
				new Duck("Daffy", 8),
				new Duck("Dewey", 2),
				new Duck("Howard", 7),
				new Duck("Louie", 2),
				new Duck("Huey", 2)};

			Console.WriteLine("정렬 전");

			display(ducks);

			Array.Sort(ducks);

			Console.WriteLine("=======================================================");
			Console.WriteLine("정렬 후");

			display(ducks);
		}

		private static void display(Duck[] ducks)
		{
			foreach (Duck duck in ducks)
				Console.WriteLine(duck);
		}
	}
}