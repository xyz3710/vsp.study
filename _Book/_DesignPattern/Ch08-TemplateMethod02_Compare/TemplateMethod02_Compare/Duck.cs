using System;
using System.Collections.Generic;
using System.Text;

namespace TemplateMethod02_Compare
{
	public class Duck : IComparable
	{
		private string _name;
		private int _weight;

		/// <summary>
		/// Initializes a new instance of the Duck class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="weight"></param>
		public Duck(string name, int weight)
		{
			_name = name;
			_weight = weight;
		}

		public override string ToString()
		{
			return _name + ", ü�� : " + _weight;
		}

		#region IComparable Members

		public int CompareTo(object obj)
		{
			Duck otherDuck = (Duck)obj;

			if (_weight < otherDuck._weight)
				return -1;
			else if (_weight == otherDuck._weight)
				return 0;
			else
				return 1;
		}

		#endregion
	}

}
