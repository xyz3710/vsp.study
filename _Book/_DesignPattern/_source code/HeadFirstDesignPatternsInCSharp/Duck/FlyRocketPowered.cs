using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// FlyRocketPowered
	/// </summary>
	public class FlyRocketPowered : IFlyBehavior
	{
		public FlyRocketPowered()
		{}

		#region IFlyBehavior Members

		public object Fly()
		{
			return "I'm flying with a rocket!";
		}

		#endregion
	}
}
