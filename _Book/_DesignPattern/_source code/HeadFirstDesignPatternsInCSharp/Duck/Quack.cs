using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// Quack
	/// </summary>
	public class Quack : IQuackBehavior
	{
		public string Quacking()
		{
			return "Quack";
		}
	}
}
