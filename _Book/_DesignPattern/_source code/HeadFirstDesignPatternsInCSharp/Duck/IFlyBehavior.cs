using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// IFlyBehavior interface for flying behaviors
	/// </summary>
	public interface IFlyBehavior
	{
		object Fly();
	}
}
