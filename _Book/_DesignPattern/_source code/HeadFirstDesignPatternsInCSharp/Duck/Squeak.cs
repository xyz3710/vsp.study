using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// Squeak
	/// </summary>
	public class Squeak : IQuackBehavior
	{
		public string Quacking()
		{
			return "Squeak";
		}
	}
}
