using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// abstract base class of Ducks
	/// </summary>
	public abstract class Duck
	{
		private string _quackBehavior;
		private object _flyBehavior;
		protected IFlyBehavior flyBehavior;
		protected IQuackBehavior quackBehavior;

		public Duck()
		{}

		public string QuackBehavior
		{
			get
			{
				return _quackBehavior;
			}
			set
			{
				_quackBehavior = value;
			}
		}

		public object FlyBehavoir
		{
			get
			{
				return _flyBehavior;
			}
			set
			{
				_flyBehavior = value;
			}
		}

		
		public abstract object Display();

		public object PerformFly()
		{
			return flyBehavior.Fly();
		}

		public object PerformQuack()
		{
			return quackBehavior.Quacking();
		}

		public string Swim()
		{
			return "All ducks float, even decoys!";
		}
	}
}
