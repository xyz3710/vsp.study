using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// MuteQuack
	/// </summary>
	public class MuteQuack : IQuackBehavior
	{
		public string Quacking()
		{
			return "<<silence>>";
		}
	}
}
