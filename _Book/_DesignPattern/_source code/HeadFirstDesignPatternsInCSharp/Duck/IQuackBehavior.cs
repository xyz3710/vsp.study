using System;

namespace HeadFirstDesignPatterns.Ducks
{
	/// <summary>
	/// IQuackBehavior
	/// </summary>
	public interface IQuackBehavior
	{
		string Quacking();
	}
}
