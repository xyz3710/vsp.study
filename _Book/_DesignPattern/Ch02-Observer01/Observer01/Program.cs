﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	class Program
	{
		static void Main(string[] args)
		{
			WeatherData weatherData = new WeatherData();

			CurrentConditionsDisplay currentConditionDisplay = new CurrentConditionsDisplay(weatherData);
			StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);

			weatherData.SetMeasurements(80, 65, 30.4f);
			Console.WriteLine();
			weatherData.SetMeasurements(82, 70, 29.2f);
			Console.WriteLine();
			weatherData.SetMeasurements(78, 90, 29.2f);
		}
	}
}
