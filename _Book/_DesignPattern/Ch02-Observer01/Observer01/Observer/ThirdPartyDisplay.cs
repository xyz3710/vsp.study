using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 측정값을 바탕으로 또다른 정보 표시
	/// </summary>
	public class ThirdPartyDisplay : IObserver, IDisplayElement
	{
		#region IObserver Members

		public void Update(float temp, float humidity, float pressure)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
