using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 현재 측정값을 화면에 표시
	/// </summary>
	public class CurrentConditionsDisplay : IObserver, IDisplayElement
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;
		private ISubject _weatherData;

		public CurrentConditionsDisplay(ISubject weatherData)
		{
			_weatherData = weatherData;

			weatherData.RegisterObserver(this);
		}
	
		#region IObserver Members

		public void Update(float temp, float humidity, float pressure)
		{
			_temperature = temp;
			_humidity = humidity;
			_pressure = pressure;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Current conditions : {0:F1}F degrees and {1:F1}% humidity",
				_temperature, _humidity);
		}

		#endregion
	}
}
