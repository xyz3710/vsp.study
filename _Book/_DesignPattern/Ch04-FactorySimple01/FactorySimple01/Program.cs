﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Simple
{
	class Program
	{
		static void Main(string[] args)
		{
			SimplePizzaFactory simplePizzaFactory = new SimplePizzaFactory();
			PizzaStore pizzaStore = new PizzaStore(simplePizzaFactory);

			pizzaStore.OrderPizza(PizzaType.Cheese);
			pizzaStore.OrderPizza(PizzaType.Pepperoni);
		}
	}
}
