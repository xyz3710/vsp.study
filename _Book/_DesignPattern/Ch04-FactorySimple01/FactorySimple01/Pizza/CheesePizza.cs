using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Simple
{
	public class CheesePizza : Pizza
	{
		public override void Bake()
		{
			base.Bake();
			Console.WriteLine("치즈를 얹습니다.");
		}
	}
}
