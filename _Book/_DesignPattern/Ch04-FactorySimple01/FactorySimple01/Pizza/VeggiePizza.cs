using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Simple
{
	public class VeggiePizza : Pizza
	{
		public override void Bake()
		{
			Console.WriteLine("야채를 살짝 데칩니다.");
		}
	}
}
