using System;
using System.Collections.Generic;
using System.Text;

namespace Factory_Simple
{
	public class PepperoniPizza : Pizza
	{
		public override void Prepare()
		{
			base.Prepare();
			Console.WriteLine("페파로니를 준비합니다.");
		}
	}
}
