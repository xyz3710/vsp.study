using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
	public class FlyNoWay : IFlyBehavior
	{
		#region IFlyBehavior Members

		public void Fly()
		{
			Console.WriteLine("저는 못 날아요");
		}

		#endregion
	}
}
