using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
	public class FlyRocketPowered : IFlyBehavior
	{
		#region IFlyBehavior Members

		public void Fly()
		{
			Console.WriteLine("로켓 추진으로 날아갑니다.");
		}

		#endregion
	}
}
