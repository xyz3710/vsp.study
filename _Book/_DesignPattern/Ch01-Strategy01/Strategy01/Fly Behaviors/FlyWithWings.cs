using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
	public class FlyWithWings : IFlyBehavior
	{
		#region IFlyBehavior Members

		public void Fly()
		{
			Console.WriteLine("날고 있어요");
		}

		#endregion
	}
}
