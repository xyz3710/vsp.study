using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
	public class MuteQuack : IQuackBehavior
	{
		#region IQuackBehavior Members

		public void Quack()
		{
			Console.WriteLine("소리낼 수 없다");
		}

		#endregion
	}
}
