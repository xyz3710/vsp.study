using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
	public class DecoyDuck : Duck
	{
		public DecoyDuck()
		{
			QuackBehavior = new MuteQuack();
			FlyBehavior = new FlyNoWay();
		}

		public override void Display()
		{
			Console.WriteLine("DecoyDuck");
		}
	}
}
