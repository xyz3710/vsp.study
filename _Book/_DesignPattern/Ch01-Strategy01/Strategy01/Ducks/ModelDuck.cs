using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy
{
	public class ModelDuck : Duck
	{
		public ModelDuck()
		{
			QuackBehavior = new QuackSound();
			FlyBehavior = new FlyWithWings();
		}
	
		public override void Display()
		{
			Console.WriteLine("ModelDuck");
		}
	}
}
