using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy_Puzzle
{
	public interface IWeapon
	{
		void UseWeapon();
	}
}
