using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public interface ISubject<T>
	{	
		void RegisterObserver(IObserver<T> observer);

		void UnRegisterObserver(IObserver<T> observer);

		void NotifyObserver(T args);
	}
}
