using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 기상 예보 표시
	/// </summary>
	public class ForecastDisplay : IObserver, IDisplayElement
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;
		private ISubject _weatherData;

		#region Constructors
		/// <summary>
		/// ForecastDisplay class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="weatherData"></param>
		public ForecastDisplay(ISubject weatherData)
		{
			_weatherData = weatherData;
			_weatherData.RegisterObserver(this);
		}
		#endregion

		#region IObserver Members

		public void Update(float temp, float humidity, float pressure)
		{
			_temperature = temp;
			_humidity = humidity;
			_pressure = pressure;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Forecast\t: {0:F1}F degrees and {1:F1}% humidity",
				_temperature, _humidity);
		}

		#endregion
	}
}
