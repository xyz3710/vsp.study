using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 측정값을 바탕으로 또다른 정보 표시
	/// </summary>
	public class ThirdPartyDisplay : IObserver, IDisplayElement
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;
		private ISubject _weatherData;

		#region Constructors
		/// <summary>
		/// ThirdPartyDisplay class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="weatherData"></param>
		public ThirdPartyDisplay(ISubject weatherData)
		{
			_weatherData = weatherData;
			_weatherData.RegisterObserver(this);
		}
		#endregion

		#region IObserver Members

		public void Update(float temp, float humidity, float pressure)
		{
			_temperature = temp;
			_humidity = humidity;
			_pressure = pressure;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("ThirdParty\t: {0:F1}F degrees and {1:F1}% humidity",
				_temperature, _humidity);
		}

		#endregion
	}
}
