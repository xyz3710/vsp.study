﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Observer
{
	class Observer02TestDriver
	{
		static void Main(string[] args)
		{
			Subject subject = new Subject();
            
			CurrentConditionsDisplay currentConditionDisplay = new CurrentConditionsDisplay(subject);
			StatisticsDisplay statisticsDisplay = new StatisticsDisplay(subject);
			ForecastDisplay forecastDisplay = new ForecastDisplay(subject);

			WeatherData weatherData = new WeatherData();

			weatherData.SetMeasurements(80, 65, 30.4f);
			subject.WeatherData = weatherData;
			Console.WriteLine();
			Thread.Sleep(1000);
	
			weatherData.SetMeasurements(82, 70, 18.2f);
			subject.WeatherData = weatherData;
			Console.WriteLine();
			Thread.Sleep(1000);

			weatherData.SetMeasurements(78, 90, 49.2f);
			subject.WeatherData = weatherData;
		}
	}
}
