using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	public interface ISubject
	{
		void RegisterObserver(IObserver o);

		void UnRegisterObserver(IObserver o);

		void NotifyObserver();
	}
}
