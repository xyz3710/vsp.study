using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	 ///<summary>
	 ///  평균 / 최저 / 최고치 표시
	 ///</summary>
	public class StatisticsDisplay : IObserver, IDisplayElement
	{
		private float _maxTemp = 0.0f;
		private float _minTemp = 200;
		private float _temperatureSum = 0.0f;
		private int _numReading = 0;
		private ISubject _subject;
		private WeatherData _weatherData;

		public StatisticsDisplay(ISubject subject)
		{
			_subject = subject;

			_subject.RegisterObserver(this);
		}

		public int NumReading
		{
			get
			{
				return NumReading;
			}
		}
	
		#region IObserver Members

		public void Update(WeatherData weatherData)
		{
			_weatherData = weatherData;
			_temperatureSum += weatherData.Temperature;
			_numReading++;

			if (weatherData.Temperature > _maxTemp)
				_maxTemp = weatherData.Temperature;

			if (weatherData.Temperature < _minTemp)
				_minTemp = weatherData.Temperature;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Statistics\tAvg / Max / Min temperature = {0}F / {1}F / {2}F", 
				roundFloatToString(_temperatureSum / _numReading),
				_maxTemp, _minTemp);
		}

		private string roundFloatToString(float p)
		{
			System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("en-US");

			cultureInfo.NumberFormat.CurrencyDecimalDigits = 2;
			cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";

			return p.ToString("F", cultureInfo);
		}

		#endregion
	}
}
