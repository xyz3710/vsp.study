using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 기상 예보 표시
	/// </summary>
	public class ForecastDisplay : IObserver, IDisplayElement
	{
		private ISubject _subject;
		private WeatherData _weatherData;

		#region Constructors
		/// <summary>
		/// ForecastDisplay class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="subject"></param>
		public ForecastDisplay(ISubject subject)
		{
			_subject = subject;
			_subject.RegisterObserver(this);
		}
		#endregion

		#region IObserver Members

		public void Update(WeatherData weatherData)
		{
			_weatherData = weatherData;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Forecast\t: {0:F1}F degrees and {1:F1}% humidity",
				_weatherData.Temperature, _weatherData.Humidity);
		}

		#endregion
	}
}
