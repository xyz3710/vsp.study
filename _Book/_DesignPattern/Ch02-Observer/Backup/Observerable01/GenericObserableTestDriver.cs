﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ObserverableClass
{
	class GenericObserableTestDriver
	{
		static void Main(string[] args)
		{
			WeatherData weatherData = new WeatherData();

			CurrentConditionDisplay currentCondition = new CurrentConditionDisplay(weatherData);
			ForecastDisplay forecast = new ForecastDisplay(weatherData);
			StatisticDisplay statistic = new StatisticDisplay(weatherData);

			weatherData.SetMeasurements(80, 65, 30.4f);
			Console.WriteLine();
			Thread.Sleep(1000);
			
			weatherData.SetMeasurements(82, 70, 29.2f);
			Console.WriteLine();
			Thread.Sleep(1000);
			
			weatherData.SetMeasurements(78, 90, 29.2f);
		}
	}
}
