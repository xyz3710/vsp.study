using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ObserverableClass
{
	public class Observerable<T> : ISubject<T>
	{
		private List<IObserver<T>> _observers;
        private bool _changed;

		public Observerable()
		{
			_observers = new List<IObserver<T>>();
			_changed = false;
		}
	
		#region Properties
		public bool Changed
		{
			get
			{
				return _changed;
			}
			set
			{
				_changed = value;
			}
		}

		public List<IObserver<T>> Observers
		{
			get
			{
				return _observers;
			}
			set
			{
				_observers = value;
			}
		}
		#endregion

		public void SetChanged()
		{
			_changed = true;
		}

		public void ClearChanged()
		{
			_changed = false;
		}

		#region ISubject<T> ���

		public void RegisterObserver(IObserver<T> observer)
		{
			_observers.Add(observer);
		}

		public void UnRegisterObserver(IObserver<T> observer)
		{
			_observers.Remove(observer);
		}
		
		public void NotifyObserver(T args)
		{
			if (_changed == true)
			{
				foreach (IObserver<T> item in _observers)
					item.Update(args);

				ClearChanged();
			}
		}
		#endregion
	}
}
