using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class ForecastDisplay : IObserver<WeatherData>, IDisplayElement
	{
		private ISubject<WeatherData> _subject;
		private float _currentPressure = 29.92f;
		private float _lastPressure;

		#region Constructors
		/// <summary>
		/// ForecaseDisplay class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="subject"></param>
		public ForecastDisplay(ISubject<WeatherData> subject)
		{
			_subject = subject;
			_subject.RegisterObserver(this);
		}
		#endregion

		#region IObserver<WeatherData> 멤버

		public void Update(WeatherData args)
		{
			_lastPressure = _currentPressure;

			_currentPressure = args.Pressure;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Forecast\tCurrent Pressure : {0}, Last Pressure {1}",
				_currentPressure, _lastPressure);
		}

		#endregion
	}
}
