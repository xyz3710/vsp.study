using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public interface IObserver<T>
	{
		void Update(T args);
	}
}
