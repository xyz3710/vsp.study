using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public class CaliforniaPizzaStore : PizzaStore
	{
		protected override FactoryMethod.Pizza CreatePizza(FactoryMethod.PizzaType type)
		{
			Pizza pizza = null;

			switch (type)
			{
				case PizzaType.Cheese:
					return new CaliforniaStyleCheesePizza();

					break;
				case PizzaType.Pepperoni:
					//return new CaliforniaStylePepperoniPizza();

					break;
				case PizzaType.Clam:
					//return new CaliforniaStyleClamPizza();

					break;
				case PizzaType.Veggie:
					//return new CaliforniaStyleVeggiePizza();

					break;
				default:
					return null;

					break;
			}

			return pizza;
		}
	}
}
