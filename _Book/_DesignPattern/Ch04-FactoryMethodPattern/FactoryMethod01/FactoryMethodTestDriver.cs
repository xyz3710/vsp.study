﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	class FactoryMethodTestDriver
	{
		static void Main(string[] args)
		{
			PizzaStore nyStore = new NYPizzaStore();
			PizzaStore chigagoStore = new ChicagoPizzaStore();

			Pizza pizza = nyStore.OrderPizza(PizzaType.Cheese);
			
			Console.WriteLine("Ethan ordered a {0}", pizza.Name);

			Console.WriteLine();

			pizza = chigagoStore.OrderPizza(PizzaType.Cheese);

			Console.WriteLine("Joel ordered a {0}", pizza.Name);
		}
	}
}
