using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public class ChicagoStyleCheesePizza : Pizza
	{
		public ChicagoStyleCheesePizza()
		{
			_name = "Chicago Style Deep Dish Cheese Pizza";
			_dough = "Extra Thick Crust Dough";
			_sauce = "Plum Tomato Sauce";
			_topping.Add("Shredded Mizzarella Cheese");
		}

		public override void Cut()
		{
			Console.WriteLine("\t\tCut : Cutting the pizza into square slices");
		}	
	}
}
