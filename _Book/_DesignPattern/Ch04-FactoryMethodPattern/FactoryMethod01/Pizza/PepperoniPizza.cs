using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
	public class PepperoniPizza : Pizza
	{
		public override void Prepare()
		{
			base.Prepare();
			Console.WriteLine("\tPrepare : 페파로니를 준비합니다.");
		}
	}
}
