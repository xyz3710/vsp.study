using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace FactoryMethod
{
	public abstract class Pizza
	{
		protected string _name;
		protected string _dough;
		protected string _sauce;
		protected ArrayList _topping = new ArrayList();

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public virtual void Prepare()
		{
			Console.WriteLine("Base(Prepare)\tPrepareing {0}", _name);
			Console.WriteLine("\t\tTossing dough...");
			Console.WriteLine("\t\tAdding sauce...");

			foreach (object obj in _topping)
				Console.WriteLine("\t\t\t{0}", obj);
			
		}

		public virtual void Bake()
		{
			Console.WriteLine("Base(Bake)\tBake for 25 minutes at 350 degree");
		}

		public virtual void Cut()
		{
			Console.WriteLine("Base(Cut)\tCutting the pizza into diagonal slices");
		}

		public virtual void Box()
		{
			Console.WriteLine("Base(Box)\tPlace pizza in official PizzaStore box");
		}
	}
}
