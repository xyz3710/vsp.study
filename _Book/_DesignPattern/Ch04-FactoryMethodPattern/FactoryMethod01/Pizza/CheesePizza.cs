using System;
using System.Collections.Generic;
using System.Text;
using FactoryMethod;

namespace FactoryMethod
{
	public class CheesePizza : Pizza
	{
		public override void Bake()
		{
			base.Bake();
			Console.WriteLine("\tBake : 치즈를 얹습니다.");
		}
	}
}
