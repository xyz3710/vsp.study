﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleFactory
{
	class SimpleFactoryTestDriver
	{
		static void Main(string[] args)
		{
			SimplePizzaFactory simplePizzaFactory = new SimplePizzaFactory();
			PizzaStore pizzaStore = new PizzaStore(simplePizzaFactory);

			pizzaStore.OrderPizza(PizzaType.Cheese);
			pizzaStore.OrderPizza(PizzaType.Pepperoni);
			pizzaStore.OrderPizza(PizzaType.Veggie);
			pizzaStore.OrderPizza(PizzaType.Clam);
		}
	}
}
