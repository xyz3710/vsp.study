using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleFactory
{
	public enum PizzaType
	{
		Cheese,
		Pepperoni,
		Clam,
		Veggie,
	}
}
