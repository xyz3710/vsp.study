using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleFactory
{
	public class PizzaStore
	{
		private SimplePizzaFactory _factory;

		public PizzaStore(SimplePizzaFactory factory)
		{
			_factory = factory;
		}

		public Pizza OrderPizza(PizzaType type)
		{
			Pizza pizza = _factory.CreatePizza(type);

			pizza.Prepare();
			pizza.Bake();
			pizza.Cut();
			pizza.Box();

			Console.WriteLine("{0}를 만들었습니다.\r\n", pizza.GetType().Name);

			return pizza;
		}
	}
}
