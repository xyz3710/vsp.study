using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleFactory
{
	public class VeggiePizza : Pizza
	{
		public override void Bake()
		{
			Console.WriteLine("\tBake : 야채를 살짝 데칩니다.");
		}
	}
}
