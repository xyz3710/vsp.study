using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleFactory
{
	public abstract class Pizza
	{
		public virtual void Prepare()
		{
			Console.WriteLine("Base(Prepare) :\t���� �غ�");
		}

		public virtual void Bake()
		{
			Console.WriteLine("Base(Bake) :\t�� ����");
		}

		public virtual void Cut()
		{
			Console.WriteLine("Base(Cut) :\t�ڸ���");
		}

		public virtual void Box()
		{
			Console.WriteLine("Base(Box) :\t����");
		}
	}
}
