﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
    public class UserInterface
	{
		private SpeciesType _speciesType;

		#region Constructor
		/// <summary>
		/// UserInterface class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public UserInterface()
		{
			Random random = new Random();
			int speciesType = random.Next(0, 2);

			_speciesType = (SpeciesType)Enum.Parse(typeof(SpeciesType), speciesType.ToString());
		}

		public UserInterface(SpeciesType speciesType)
		{
			_speciesType = speciesType;
		}
        
		#endregion
        
        public SpeciesType SpeciesType
		{
			get
			{
				return _speciesType;
			}
		}

		public IPrimaryFactory PrimaryFactory
		{
			get
			{
				IPrimaryFactory primaryFactory = null;

				switch (_speciesType)
				{
					case SpeciesType.Terran:
						primaryFactory = new Barrack();

						break;
					case SpeciesType.Protoss:
						primaryFactory = new Gateway();

						break;
					case SpeciesType.Zerg:
						primaryFactory = new Hatchery();

						break;
				}

				return primaryFactory;
			}
		}
	}
}