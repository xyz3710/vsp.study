﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	class StarcraftTestDriver
	{
		static void Main(string[] args)
		{
			UserInterface user1 = new UserInterface(SpeciesType.Terran);
			UserInterface user2 = new UserInterface(SpeciesType.Protoss);
			UserInterface user3 = new UserInterface(SpeciesType.Zerg);
			UserInterface user4 = new UserInterface();
			List<IUnit> terranUnits = new List<IUnit>();
			List<IUnit> protossUnits = new List<IUnit>();
			List<IUnit> zergUnits = new List<IUnit>();
			List<IUnit> randomUnits = new List<IUnit>();

			IUnit terranUnit1 = user1.PrimaryFactory.CreateUnit1();
			IUnit terranUnit2 = user1.PrimaryFactory.CreateUnit2();
			IUnit terranUnit3 = user1.PrimaryFactory.CreateUnit3();
			IUnit terranUnit4 = user1.PrimaryFactory.CreateUnit4();

			terranUnits.Add(terranUnit1);
			terranUnits.Add(terranUnit2);
			terranUnits.Add(terranUnit3);
			terranUnits.Add(terranUnit4);

			IUnit protossUnit1 = user2.PrimaryFactory.CreateUnit1();
			IUnit protossUnit2 = user2.PrimaryFactory.CreateUnit3();

			protossUnits.Add(protossUnit1);
			protossUnits.Add(protossUnit2);

			IUnit zergUnit1 = user3.PrimaryFactory.CreateUnit1();
			IUnit zergUnit2 = user3.PrimaryFactory.CreateUnit2();

			zergUnits.Add(zergUnit1);
			zergUnits.Add(zergUnit2);

			IUnit randomUnit1 = user4.PrimaryFactory.CreateUnit1();
			IUnit randomUnit2 = user4.PrimaryFactory.CreateUnit2();
			IUnit randomUnit3 = user4.PrimaryFactory.CreateUnit3();
			IUnit randomUnit4 = user4.PrimaryFactory.CreateUnit4();

			randomUnits.Add(randomUnit1);
			randomUnits.Add(randomUnit2);
			randomUnits.Add(randomUnit3);
			randomUnits.Add(randomUnit4);

			ShowActions(terranUnits);
			ShowActions(protossUnits);
			ShowActions(zergUnits);
			ShowActions(randomUnits);
		}

		private static void ShowActions(List<IUnit> units)
		{

			foreach (IUnit unit in units)
			{
				unit.Attack();
				unit.Move();
				unit.Stop();
			}

			Console.WriteLine("");
		}
	}
}
