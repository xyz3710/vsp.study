using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Zerggling : Unit
	{
		#region Constructors
		/// <summary>
		/// Zerggling class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Zerggling()
		{
			base.ClassName = "Zerggling";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("빠른 지상 공격");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 7보");
		}

		public override void Special2()
		{
			base.Special2("Burrow");
		}
	}
}
