using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Hydra : Unit
	{
		#region Constructors
		/// <summary>
		/// Hydra class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Hydra()
		{
			base.ClassName = "Hydra";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("빠른 지대공 공격");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 5보");
		}

		public override void Special1()
		{
			base.Special1("Lurker 변신");
		}

		public override void Special2()
		{
			base.Special2("Burrow");
		}
	}
}
