﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
    public class Hatchery : IPrimaryFactory
	{
		#region IPrimaryFactory 멤버

		/// <summary>
		/// Zerggling을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit1()
		{
			return new Zerggling();
		}

		/// <summary>
		/// Hydra를 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit2()
		{
			return new Hydra();
		}

		/// <summary>
		/// Lair시 Mutal을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit3()
		{
			if (IsLair == true)
				return new Mutal();

			throw new NotImplementedException("Lair로 업그레이드 해야 합니다.");
		}

		/// <summary>
		/// Lair시 Ultra를 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit4()
		{
			if (IsLair == true)
				return new Ultra();

			throw new NotImplementedException("Lair로 업그레이드 해야 합니다.");
		}

		#endregion

		private bool IsLair
		{
			get
			{
				return true;
			}
		}
	}
}