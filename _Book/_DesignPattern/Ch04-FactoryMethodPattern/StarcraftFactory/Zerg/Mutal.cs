using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Mutal : Unit
	{
		#region Constructors
		/// <summary>
		/// Mutal class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Mutal()
		{
			base.ClassName = "Mutal";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("3연속 지대공 공격");
		}

		public override void Move()
		{
			base.Move("빠른 비행");
		}

		public override void HoldPosition()
		{
			base.HoldPosition("공중에 머무름");
		}

		public override void Special1()
		{
			base.Special1("Gudian 변신");
		}

		public override void Special2()
		{
			base.Special2("Devour 변신");
		}
	}
}
