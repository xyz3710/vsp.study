using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Ultra : Unit
	{
		#region Constructors
		/// <summary>
		/// Ultra class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Ultra()
		{
			base.ClassName = "Ultra";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("빠른 지상 공격");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 6보");
		}
	}
}
