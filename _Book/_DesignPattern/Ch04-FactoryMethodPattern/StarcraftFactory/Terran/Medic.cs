using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Medic : Unit
	{
		#region Constructors
		/// <summary>
		/// Medic class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Medic()
		{
			base.ClassName = "Medic";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("치료");
		}

		public override void Move()
		{
			base.Move("보통 걸음 3보");
		}
	}
}
