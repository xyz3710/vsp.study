using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Marine : Unit
	{
		#region Constructors
		/// <summary>
		/// Marine class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Marine()
		{
			base.ClassName = "Marine";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("지대공 기관총");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 5보");
		}

		public override void Special1()
		{
			base.Special1("Steam Pack");
		}
	}
}
