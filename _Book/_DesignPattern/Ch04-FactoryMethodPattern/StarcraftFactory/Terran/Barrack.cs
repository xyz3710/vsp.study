﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
    public class Barrack : IPrimaryFactory
	{
		#region IPrimaryFactory 멤버

		/// <summary>
		/// Marine을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit1()
		{
			return new Marine();
		}

		/// <summary>
		/// Firebat을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit2()
		{
			return new Firebat();
		}

		/// <summary>
		/// Ghost를 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit3()
		{
			return new Ghost();
		}

		/// <summary>
		/// Medic을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit4()
		{
			return new Medic();
		}

		#endregion
	}
}