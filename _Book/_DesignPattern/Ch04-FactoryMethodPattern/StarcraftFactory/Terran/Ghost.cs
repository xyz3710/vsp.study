using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Ghost : Unit
	{
		#region Constructors
		/// <summary>
		/// Ghost class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Ghost()
		{
			base.ClassName = "Ghost";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("ShotGun 지대공 공격");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 3.5보");
		}

		public override void Special1()
		{
			base.Special1("핵 장착 공격");
		}

		public override void Special2()
		{
			base.Special2("Cloak");
		}
	}
}
