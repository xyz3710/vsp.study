using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Firebat : Unit
	{
		#region Constructors
		/// <summary>
		/// Firebat class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Firebat()
		{
			base.ClassName = "Firebat";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("화염 방사 지상 공격");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 4보");
		}

		public override void Special1()
		{
			base.Special1("Steam Pack");
		}
	}
}
