﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
    public abstract class Unit : IUnit
	{
		#region Fields
		private string _className;
		#endregion

		#region Constructors
		/// <summary>
		/// Unit class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Unit()
		{
			ClassName = "Unit";
		}
		#endregion

		#region Properties
		protected string ClassName
		{
			get
			{
				return _className;
			}
			set
			{
				_className = value;
			}
		}
		#endregion

		#region Protected methods
		protected virtual void Attack(string attack)
		{
			Console.WriteLine("{0}의 {1} 실행됨", ClassName, attack);
		}

		protected virtual void Move(string move)
		{
			Console.WriteLine("{0}의 {1} 실행됨", ClassName, move);
		}

		protected virtual void Stop(string stop)
		{
			Console.WriteLine("{0}의 {1} 실행됨", ClassName, stop);
		}

		protected virtual void HoldPosition(string position)
		{
			Console.WriteLine("{0}의 {1} 실행됨", ClassName, position);
		}

		protected virtual void Special1(string special1)
		{
			Console.WriteLine("{0}의 {1} 실행됨", ClassName, special1);
		}

		protected virtual void Special2(string special2)
		{
			Console.WriteLine("{0}의 {1} 실행됨", ClassName, special2);
		}
		#endregion	

		#region Public methods
		public virtual void Attack()
		{
			Attack("Attack");
		}

		public virtual void Move()
		{
			Move("Move");
		}

		public virtual void Stop()
		{
			Stop("Stop");
		}

		public virtual void HoldPosition()
		{
			HoldPosition("HoldPosition");
		}

		public virtual void Special1()
		{
			Special1("Special1");
		}

		public virtual void Special2()
		{
			Special2("Special2");
		}
		#endregion
	}
}