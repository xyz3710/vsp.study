using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public interface IUnit
	{
		void Attack();
		void Move();
		void Stop();
		void HoldPosition();
	}
}
