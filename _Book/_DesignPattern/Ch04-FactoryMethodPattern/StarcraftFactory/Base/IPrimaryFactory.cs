﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
    public interface IPrimaryFactory 
	{
		IUnit CreateUnit1();
		IUnit CreateUnit2();
		IUnit CreateUnit3();
		IUnit CreateUnit4();
	}
}