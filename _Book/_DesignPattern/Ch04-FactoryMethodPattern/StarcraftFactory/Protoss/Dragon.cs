using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Dragoon : Unit
	{
		#region Constructors
		/// <summary>
		/// Dragoon class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Dragoon()
		{
			base.ClassName = "Dragoon";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("캐논 지대공 포");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 3");
		}
	}
}
