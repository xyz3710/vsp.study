using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class DarkTempler : Unit
	{
		#region Constructors
		/// <summary>
		/// DarkTempler class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public DarkTempler()
		{
			base.ClassName = "DarkTempler";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("지상 공격");
		}

		public override void Move()
		{
			base.Move("느린 걸음 3");
		}

		public override void Special1()
		{
			base.Special1("2명 합체");
		}

		public override void Special2()
		{
			base.Special2("Cloak");
		}
	}
}
