﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
    public class Gateway : IPrimaryFactory
	{
		#region IPrimaryFactory 멤버

		/// <summary>
		/// Zealot을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit1()
		{
			return new Zealot();
		}

		/// <summary>
		/// Dragoon을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit2()
		{
			return new Dragoon();
		}

		/// <summary>
		/// HiTempler를 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit3()
		{
			return new HiTempler();
		}

		/// <summary>
		/// DarkTempler을 생산합니다.
		/// </summary>
		/// <returns></returns>
		public IUnit CreateUnit4()
		{
			return new DarkTempler();
		}

		#endregion
	}
}