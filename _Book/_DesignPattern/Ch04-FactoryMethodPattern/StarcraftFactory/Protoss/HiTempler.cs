using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class HiTempler : Unit
	{
		#region Constructors
		/// <summary>
		/// HiTempler class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public HiTempler()
		{
			base.ClassName = "HiTempler";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("지대공 전자파");
		}

		public override void Move()
		{
			base.Move("느린 걸음 2");
		}

		public override void Special1()
		{
			base.Special1("2명 합체");
		}
	}
}
