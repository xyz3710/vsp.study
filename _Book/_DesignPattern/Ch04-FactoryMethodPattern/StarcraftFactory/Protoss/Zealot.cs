using System;
using System.Collections.Generic;
using System.Text;

namespace StarcraftFactory
{
	public class Zealot : Unit
	{
		#region Constructors
		/// <summary>
		/// Zealot class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Zealot()
		{
			base.ClassName = "Zealot";
		}
		#endregion

		public override void Attack()
		{
			base.Attack("광선검");
		}

		public override void Move()
		{
			base.Move("빠른 걸음 4");
		}

		public override void Special1()
		{
			base.Special1("쾌속 걸음 5");
		}
	}
}
