using System;
using System.Collections.Generic;
using System.Text;
using FactorySimple01.Ingredient;

namespace AbstractFactory
{
	public class CheesePizza : Pizza
	{
		IPizzaIngredientFactory _ingredientFactory;
		
		/// <summary>
		/// Initializes a new instance of the VeggiePizza class.
		/// </summary>
		public CheesePizza(IPizzaIngredientFactory ingredientFactory)
		{
			_ingredientFactory = ingredientFactory;
		}

		public override void Prepare()
		{
			Console.WriteLine("Preparing {0}", Name);

			Dough = _ingredientFactory.CreateDough();
			Sauce = _ingredientFactory.CreateSauce();
			Cheese = _ingredientFactory.CreateCheese();
			Clam = _ingredientFactory.CreateClam();
			Pepperoni = _ingredientFactory.CreatePepperoni();
		}
		
		public override void Bake()
		{
			base.Bake();
			Console.WriteLine("치즈를 얹습니다.");
		}
	}
}
