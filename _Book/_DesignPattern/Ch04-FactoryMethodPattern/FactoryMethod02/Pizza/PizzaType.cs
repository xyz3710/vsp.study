using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory
{
	public enum PizzaType
	{
		Cheese,
		Pepperoni,
		Clam,
		Veggie,
	}
}
