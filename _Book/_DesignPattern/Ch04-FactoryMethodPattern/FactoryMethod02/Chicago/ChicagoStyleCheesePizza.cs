using System;
using System.Collections.Generic;
using System.Text;
using AbstractFactory;
using FactorySimple01.Ingredient;

namespace FactorySimple01.Chicago
{
	class ChicagoStyleCheesePizza : Pizza
	{
		IPizzaIngredientFactory _ingredientFactory;

		/// <summary>
		/// Initializes a new instance of the ChicagoStyleCheesePizza class.
		/// </summary>
		public ChicagoStyleCheesePizza(IPizzaIngredientFactory ingredient)
		{
			Name = "��ī�� ��Ÿ���� ���� ���ÿ� ǳ���� ����";
			
			_ingredientFactory = ingredient;

			Topping = "��¥���� ���� ġ��";
			Topping = "���Ż ġ��";
		}

		public override void Prepare()
		{
			Console.WriteLine("Preparing {0}", Name);

			Dough = _ingredientFactory.CreateDough();
			Sauce = _ingredientFactory.CreateSauce();
			Cheese = _ingredientFactory.CreateCheese();
			Clam = _ingredientFactory.CreateClam();
			Pepperoni = _ingredientFactory.CreatePepperoni();
		}
	}
}
