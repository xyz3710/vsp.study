using System;
using System.Collections.Generic;
using System.Text;
using FactorySimple01.Ingredient;

namespace FactorySimple01.Chicago
{
	public class ChicagoPizzaIngredientFactory : IPizzaIngredientFactory
	{
		#region IPizzaIngredientFactory ���

		public IDough CreateDough()
		{
			return new ThickCrustDough().CreateDough();
		}

		public ISauce CreateSauce()
		{
			return new PlumTomatoSauce().CreateSauce();
		}

		public ICheese CreateCheese()
		{
			return new MozzarellaCheese().CreateCheese();
		}

		public IVeggies[] CreateVeggies()
		{
			Veggies[] veggies = new Veggies[] { new Onion(), new Mushroom(), new Mushroom(), new RedPepper() };


			return veggies;
		}

		public IPepperoni CreatePepperoni()
		{
			return new RedPepperoni().CreatePepperoni();
		}

		public IClam CreateClam()
		{
			return new FrozenClams().CreateClams();
		}

		#endregion
	}
}
