using System;
using System.Collections.Generic;
using System.Text;

namespace FactorySimple01.Ingredient
{
	public interface IDough
	{
		IDough CreateDough();
	}
}
