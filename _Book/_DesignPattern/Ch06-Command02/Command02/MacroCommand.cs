using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class MacroCommand : ICommand
	{
		private ICommand[] _commands;

		public MacroCommand(ICommand[] commands)
		{
			_commands = commands;
		}

		#region ICommand Members

		public void Execute()
		{
			for (int i = 0; i < _commands.Length; i++)
				_commands[i].Execute();
		}

		public void Undo()
		{
			for (int i = 0; i < _commands.Length; i++)
				_commands[i].Undo();
		}

		#endregion
	}
}
