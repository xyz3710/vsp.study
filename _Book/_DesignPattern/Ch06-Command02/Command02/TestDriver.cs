using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class TestDriver
	{
		static void Main(string[] args)
		{
			RemoteControl remocon = new RemoteControl();

			Light livingRoomLight = new Light("Living Room");
			Light kitchenLight = new Light("Kitchen");
			CeilingFan ceilingFan = new CeilingFan("Living Room");
			GarageDoor garageDoor = new GarageDoor(string.Empty);
			Stereo stereo = new Stereo("Living Room");

			ceilingFan.SetMedium();

			LightOnCommand livingRoomLightOn = new LightOnCommand(livingRoomLight);
			LightOffCommand livingRoomLightOff = new LightOffCommand(livingRoomLight);
			LightOnCommand kitchenLightOn = new LightOnCommand(kitchenLight);
			LightOffCommand kitchenLightOff = new LightOffCommand(kitchenLight);
			
			CeilingFanOnCommand ceilingFanOn = new CeilingFanOnCommand(ceilingFan);
			CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);
			
			GarageDoorOpenCommand garageDoorOpen = new GarageDoorOpenCommand(garageDoor);
			GarageDoorCloseCommand garageDoorClose = new GarageDoorCloseCommand(garageDoor);

			StereoOnWithCDCommand stereoOnWithCD = new StereoOnWithCDCommand(stereo);
			StereoOffCommand stereoOff = new StereoOffCommand(stereo);

			remocon.SetCommand(0, livingRoomLightOn, livingRoomLightOff);
			remocon.SetCommand(1, kitchenLightOn, kitchenLightOff);
			remocon.SetCommand(2, ceilingFanOn, ceilingFanOff);
			remocon.SetCommand(3, stereoOnWithCD, stereoOff);

			Console.WriteLine(remocon);

			remocon.OnButtonWasPushed(0);
			remocon.OffButtonWasPushed(0);

			remocon.UndoButtonWasPushed();

			remocon.OnButtonWasPushed(1);
			remocon.OffButtonWasPushed(1);
			remocon.OnButtonWasPushed(2);
			remocon.OffButtonWasPushed(2);

			remocon.UndoButtonWasPushed();

			remocon.OnButtonWasPushed(3);
			remocon.OffButtonWasPushed(3);


			ICommand[] partyOn = { livingRoomLightOn, ceilingFanOn, stereoOnWithCD };
			ICommand[] partyOff = { livingRoomLightOff, ceilingFanOff, stereoOff };
			MacroCommand macroPartyOn = new MacroCommand(partyOn);
			MacroCommand macroPartyOff = new MacroCommand(partyOff);

			RemoteControl macroRemocon = new RemoteControl();

			macroRemocon.SetCommand(0, macroPartyOn, macroPartyOff);

			Console.WriteLine("\n========== Pushing Macro On ==========");
			macroRemocon.OnButtonWasPushed(0);
			Console.WriteLine("========== Pushing Macro Off ==========");
			macroRemocon.OffButtonWasPushed(0);
			Console.WriteLine("========== Pushing Macro UnDo ==========");
			macroRemocon.UndoButtonWasPushed();

		}
	}
}
