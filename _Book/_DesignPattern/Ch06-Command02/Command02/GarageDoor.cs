using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class GarageDoor
	{
		private string _locate;

		public GarageDoor(string locate)
		{
			_locate = locate;
		}

		public void Open()
		{
			Console.WriteLine("{0} garage door is open", _locate);
		}

		public void Close()
		{
			Console.WriteLine("{0} garage door is close", _locate);
		}
	}
}
