using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class RemoteControl
	{
		private ICommand[] _onCommand;
		private ICommand[] _offCommand;
		private ICommand _undoCommand;

		public RemoteControl()
		{
			_onCommand = new ICommand[7];
			_offCommand = new ICommand[7];

			NoCommand noCommand = new NoCommand();

			for (int i = 0; i < 7; i++)
			{
				_onCommand[i] = noCommand;
				_offCommand[i] = noCommand;
			}

			_undoCommand = noCommand;
		}

		public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
		{
			_onCommand[slot] = onCommand;
			_offCommand[slot] = offCommand;
		}

		public void OnButtonWasPushed(int slot)
		{
			_onCommand[slot].Execute();
			_undoCommand = _onCommand[slot];
		}

		public void OffButtonWasPushed(int slot)
		{
			_offCommand[slot].Execute();
			_undoCommand = _offCommand[slot];
		}

		public void UndoButtonWasPushed()
		{
			Console.WriteLine("========== UnDo ==========");
			_undoCommand.Undo();
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("\n========== Remote Control ==========\n");

			for (int i = 0; i < _onCommand.Length; i++)
			{
				sb.AppendFormat("[slot {0}] {1}\t{2}\n", i,
					_onCommand[i].ToString(), _offCommand[i].ToString());
			}

			return sb.ToString();
		}
	}
}
