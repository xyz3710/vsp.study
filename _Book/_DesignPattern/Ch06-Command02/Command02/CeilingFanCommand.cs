using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class CeilingFanOnCommand : ICommand
	{
		private CeilingFan _ceilingFan;
		private FanSpeed _prevSpeed;

		public CeilingFanOnCommand(CeilingFan ceilingFan)
		{
			_ceilingFan = ceilingFan;
		}

		#region ICommand Members

		public void Execute()
		{
			_prevSpeed = _ceilingFan.GetSpeed();
			execCommand(_ceilingFan.GetSpeed());
		}

		public void Undo()
		{
			execCommand(_prevSpeed);	
		}

		#endregion

		private void execCommand(FanSpeed speed)
		{
			switch (speed)
			{
				case FanSpeed.High:
					_ceilingFan.SetHigh();

					break;
				case FanSpeed.Medium:
					_ceilingFan.SetMedium();

					break;
				case FanSpeed.Low:
					_ceilingFan.SetLow();

					break;
				case FanSpeed.Off:
					_ceilingFan.Off();

					break;
			}
		}
	}

	public class CeilingFanOffCommand : ICommand
	{
		private CeilingFan _ceilingFan;
		private FanSpeed _prevSpeed;

		public CeilingFanOffCommand(CeilingFan ceilingFan)
		{
			_ceilingFan = ceilingFan;
		}

		#region ICommand Members

		public void Execute()
		{
			_prevSpeed = _ceilingFan.GetSpeed();
			_ceilingFan.Off();
		}

		public void Undo()
		{
			switch (_prevSpeed)
			{
				case FanSpeed.High:
					_ceilingFan.SetHigh();

					break;
				case FanSpeed.Medium:
					_ceilingFan.SetMedium();

					break;
				case FanSpeed.Low:
					_ceilingFan.SetLow();

					break;
				case FanSpeed.Off:
					_ceilingFan.Off();

					break;
			}
		}

		#endregion
	}
}
