using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class StereoOnWithCDCommand : ICommand
	{
		private Stereo _stereo;

		public StereoOnWithCDCommand(Stereo stereo)
		{
			_stereo = stereo;
		}

		#region ICommand Members

		public void Execute()
		{
			_stereo.On();
			_stereo.SetCD();
			_stereo.SetVolume(11);
		}

		public void Undo()
		{
			_stereo.Off();
		}

		#endregion
	}

	public class StereoOffCommand : ICommand
	{
		private Stereo _stereo;

		public StereoOffCommand(Stereo stereo)
		{
			_stereo = stereo;
		}

		#region ICommand Members

		public void Execute()
		{
			_stereo.Off();
		}

		public void Undo()
		{
			_stereo.On();
			_stereo.SetCD();
			_stereo.SetVolume(11);
		}

		#endregion
	}
}
