using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class Light
	{
		private string _locate;

		public Light(string locate)
		{
			_locate = locate;
		}

		public void On()
		{
			Console.WriteLine("{0} light is On", _locate);
		}

		public void Off()
		{
			Console.WriteLine("{0} light is Off", _locate);
		}
	}
}
