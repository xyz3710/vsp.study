using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class LightOnCommand : ICommand
	{
		private Light _light;

		public LightOnCommand(Light light)
		{
			_light = light;
		}

		#region ICommand Members

		public void Execute()
		{
			_light.On();
		}

		public void Undo()
		{
			_light.Off();
		}

		#endregion
	}

	public class LightOffCommand : ICommand
	{
		private Light _light;

		public LightOffCommand(Light light)
		{
			_light = light;
		}

		#region ICommand Members

		public void Execute()
		{
			_light.Off();
		}

		public void Undo()
		{
			_light.On();
		}

		#endregion
	}
}
