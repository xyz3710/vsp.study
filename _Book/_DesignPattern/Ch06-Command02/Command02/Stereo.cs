using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class Stereo
	{
		private string _locate;
		private int _volume;

		public Stereo(string locate)
		{
			_locate = locate;
		}

		public void On()
		{
			Console.WriteLine("{0} stereo is on", _locate);
		}

		public void Off()
		{
			Console.WriteLine("{0} stereo is off", _locate);
		}

		public void SetCD()
		{
			Console.WriteLine("{0} stereo is set for CD input", _locate);
		}

		public void SetVolume(int volume)
		{
			_volume = volume;

			Console.WriteLine("{0} stereo volume set to {1}", _locate, _volume);
		}
	}
}
