using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class CeilingFan
	{
		private FanSpeed _speed;
		private string _locate;

		public CeilingFan(string locate)
		{
			_locate = locate;
			_speed = FanSpeed.Off;
		}

		public FanSpeed GetSpeed()
		{
			return _speed;
		}
			
		public void SetHigh()
		{
			on(FanSpeed.High);
		}

		public void SetMedium()
		{
			on(FanSpeed.Medium);
		}

		public void SetLow()
		{
			on(FanSpeed.Low);
		}

		public void Off()
		{
			on(FanSpeed.Off);
		}

		private void on(FanSpeed speed)
		{
			_speed = speed;
			Console.WriteLine("{0} ceiling fan is {1}", _locate, _speed.ToString());
		}
	}

	public enum FanSpeed
	{
		High = 3,
		Medium = 2,
		Low = 1,
		Off = 0,
	}
}
