using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public interface ICommand
	{
		void Execute();

		void Undo();
	}
}
