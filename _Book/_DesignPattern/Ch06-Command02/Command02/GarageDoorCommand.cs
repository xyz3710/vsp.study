using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class GarageDoorOpenCommand : ICommand
	{
		private GarageDoor _garageDoor;

		public GarageDoorOpenCommand(GarageDoor garageDoor)
		{
			_garageDoor = garageDoor;
		}

		#region ICommand Members

		public void Execute()
		{
			_garageDoor.Open();
		}

		public void Undo()
		{
			_garageDoor.Close();
		}

		#endregion
	}

	public class GarageDoorCloseCommand : ICommand
	{
		private GarageDoor _garageDoor;

		public GarageDoorCloseCommand(GarageDoor garageDoor)
		{
			_garageDoor = garageDoor;
		}

		#region ICommand Members

		public void Execute()
		{
			_garageDoor.Close();
		}

		public void Undo()
		{
			_garageDoor.Open();
		}

		#endregion
	}

}
