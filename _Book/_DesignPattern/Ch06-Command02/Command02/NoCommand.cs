using System;
using System.Collections.Generic;
using System.Text;

namespace Command02
{
	public class NoCommand : ICommand
	{
		#region ICommand Members

		public void Execute()
		{			
		}

		public void Undo()
		{
		}

		#endregion
	}
}
