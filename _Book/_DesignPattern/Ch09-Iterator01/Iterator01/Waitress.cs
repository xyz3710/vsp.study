﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class Waitress
	{
		private ArrayList _menus;

		public Waitress(ArrayList menus)
		{
			_menus = menus;
		}
		
		public void PrintMenu()
		{
			foreach (IEnumerable menu in _menus)
				printMenuItem(menu);
		}

		private void printMenuItem(IEnumerable items)
		{
			string bar = "*";
			Console.WriteLine("\n{0} Menus {1}", bar.PadLeft(10, '*'), bar.PadRight(10, '*'));

			foreach (MenuItem item in items)
				Console.WriteLine(String.Format("{0,-15} {1} {2} {3}",
						item.Name, item.IsVegetarian, item.Price, item.Description));
		}
	}
}
