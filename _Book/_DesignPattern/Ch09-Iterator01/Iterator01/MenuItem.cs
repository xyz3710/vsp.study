﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class MenuItem : IMenuItem
	{
		private string _name;
		private string _description;
		private bool _isVegetarian;
		private double _price;

		/// <summary>
		/// Initializes a new instance of the MenuItem class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="description"></param>
		/// <param name="isVegetarian"></param>
		/// <param name="price"></param>
		public MenuItem(string name, string description, bool isVegetarian, double price)
		{
			_name = name;
			_description = description;
			_isVegetarian = isVegetarian;
			_price = price;
		}

		public string Name
		{
			get
			{
				return _name;
			}
		}
	
		public string Description
		{
			get
			{
				return _description;
			}
		}

		public bool IsVegetarian
		{
			get
			{
				return _isVegetarian;
			}
		}

		public double Price
		{
			get
			{
				return _price;
			}
		}
	}
}
