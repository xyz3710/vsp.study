﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class DinerMenuCollection : IEnumerable
	{
		private const int MAX_ITEMS = 6;
		private int _numberOfItems;
		private MenuItem[] _menuItems;

		public DinerMenuCollection()
		{
			_menuItems = new MenuItem[MAX_ITEMS];

			AddItem("채식주의자용 BLT", "통밀 위에(식물성) 베이컨, 상추, 토마토를 얹은 메뉴", true, 2.99);
			AddItem("BLT", "통밀 위에 베이컨, 상추, 토마토를 얹은 메뉴", false, 2.99);
			AddItem("오늘의 스프", "감자 샐러드를 곁들인 오늘의 스프", false, 3.29);
			AddItem("핫도그", "샤워크라우트, 갖은 양념, 양파, 치즈가 곁들여진 핫도그", false, 3.05);
		}

		public void AddItem(string name, string description, bool isVegetarian, double price)
		{
			if (_numberOfItems >= MAX_ITEMS)
				Console.WriteLine("죄송합니다. 메뉴가 꽉 찼습니다. 더 이상 추가할 수 없습니다.");

			MenuItem menuItem = new MenuItem(name, description, isVegetarian, price);
			
			_menuItems[_numberOfItems++] = menuItem;
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return new DinnerMenuEnumerator(_menuItems);
		}

		#endregion
	}
}