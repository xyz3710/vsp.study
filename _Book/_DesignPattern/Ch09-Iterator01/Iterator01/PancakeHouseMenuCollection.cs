﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class PancakeHouseMenuCollection : IEnumerable
	{
		private ArrayList _menuItems;

		public PancakeHouseMenuCollection()
		{
			_menuItems = new ArrayList();

			AddItem("K&B 팬케이크 세트", "스크램블드 에그와 토스트가 곁들여진 팬케이크", true, 2.99);
			AddItem("레귤러 팬케이크 세트", "달걀 후라이와 소시지가 곁들여진 팬케이크", false, 2.99);
			AddItem("블루베리 팬케이크", "신선한 블루베리와 블루베리 시럽으로 만든 팬케이크", true, 3.49);
			AddItem("와플", "와플, 취향에 따라 블루베리나 딸기를 얹을 수 있습니다.", true, 3.59);
		}

		public void AddItem(string name, string description, bool isVegetarian, double price)
		{
			MenuItem menuItem = new MenuItem(name, description, isVegetarian, price);

			_menuItems.Add(menuItem);
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _menuItems.GetEnumerator();
		}

		#endregion
	}
}