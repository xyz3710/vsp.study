﻿using System;
namespace Iterator01
{
	interface IMenuItem
	{
		string Description
		{
			get;
		}

		bool IsVegetarian
		{
			get;
		}

		string Name
		{
			get;
		}

		double Price
		{
			get;
		}
	}
}
