using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class DinnerMenuEnumerator : IEnumerator
	{
		private MenuItem[] _menuItems;
		private int _enumerableIdx;

		/// <summary>
		/// Initializes a new instance of the DinnerMenuEnumerator class.
		/// </summary>
		/// <param name="menuItems"></param>
		public DinnerMenuEnumerator(MenuItem[] menuItems)
		{
			_menuItems = menuItems;
		}

		#region IEnumerator Members

		public object Current
		{
			get
			{
				return _menuItems[_enumerableIdx++];
			}
		}

		public bool MoveNext()
		{
			bool ret = false;

			if (_enumerableIdx < _menuItems.Length && _menuItems[_enumerableIdx] != null)
				ret = true;

			return ret;
		}

		public void Reset()
		{
			_enumerableIdx = 0;
		}

		#endregion
	}

}
