﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class CafeMenuCollection : IEnumerable
	{
		private Hashtable _menuItems;

		public CafeMenuCollection()
		{
			_menuItems = new Hashtable();

			AddItem("베지 버거와 에어 프라이", "통밀빵, 상추, 토마토, 감자튀김이 첨가된 베지 버거", true, 3.99);
			AddItem("오늘의 스프", "샐러드가 곁들어진 오늘의 스프", false, 3.69);
			AddItem("베리또", "샐러드가 곁들어진 오늘의 스프", false, 3.69);
		}

		public void AddItem(string name, string description, bool isVegetarian, double price)
		{
			MenuItem menuItem = new MenuItem(name, description, isVegetarian, price);

			_menuItems.Add(name, menuItem);
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _menuItems.Values.GetEnumerator();
		}

		#endregion
	}
}