﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Iterator01
{
	public class Program 
	{
		public static void Main(string[] args)
		{
			ArrayList menus = new ArrayList();
			PancakeHouseMenuCollection pancakeHouseMenu = new PancakeHouseMenuCollection();
			DinerMenuCollection dinerMenu = new DinerMenuCollection();
			CafeMenuCollection cafeMenu = new CafeMenuCollection();

			menus.Add(pancakeHouseMenu);
			menus.Add(dinerMenu);
			menus.Add(cafeMenu);

			Waitress waitress = new Waitress(menus);

			waitress.PrintMenu();
		}
	}
}
