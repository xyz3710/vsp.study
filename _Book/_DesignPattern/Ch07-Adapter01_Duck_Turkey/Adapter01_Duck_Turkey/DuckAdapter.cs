﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter01_Duck_Turkey
{
	public class DuckAdapter : ITurkey
	{
		private IDuck _duck;

		public DuckAdapter(IDuck duck)
		{
			_duck = duck;
		}

		#region ITurkey Members

		public void Gobble()
		{
			_duck.Quack();
		}

		public void Fly()
		{
			_duck.Fly();
		}

		#endregion
	}
}
