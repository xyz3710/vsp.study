﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter01_Duck_Turkey
{
	class TestDriver
	{
		static void Main(string[] args)
		{
			MallardDuck duck = new MallardDuck();
			WildTurkey turkey = new WildTurkey();
			IDuck turkeyAdapter = new TurkeyAdapter(turkey);
			ITurkey duckAdapter = new DuckAdapter(duck);

			Console.WriteLine("The Turkey says...");
			turkey.Gobble();
			turkey.Fly();

			Console.WriteLine("\nThe Duck says...");
			testDuck(duck);

			Console.WriteLine("\nThe TurkeyAdapter says...");
			testDuck(turkeyAdapter);

			Console.WriteLine("\nThe turkeyAdapter says...");
			testTurkey(duckAdapter);
		}

		private static void testDuck(IDuck duck)
		{
 			duck.Quack();
			duck.Fly();
		}

		private static void testTurkey(ITurkey turkey)
		{
			turkey.Gobble();
			turkey.Fly();
		}
	}
}
