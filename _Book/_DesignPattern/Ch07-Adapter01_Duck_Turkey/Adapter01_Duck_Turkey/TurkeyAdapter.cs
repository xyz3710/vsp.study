using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter01_Duck_Turkey
{
	public class TurkeyAdapter : IDuck
	{
		private ITurkey _turkey;

		public TurkeyAdapter(ITurkey turkey)
		{
			_turkey = turkey;
		}
	
		#region IDuck Members
		
		public void Quack()
		{
			_turkey.Gobble();
		}

		public void Fly()
		{
			for (int i = 0; i < 5; i++)
			{
				_turkey.Fly();
			}
		}

		#endregion
	}
}
