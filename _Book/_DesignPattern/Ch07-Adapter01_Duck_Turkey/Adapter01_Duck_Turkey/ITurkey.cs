using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter01_Duck_Turkey
{
	public interface ITurkey
	{
		void Gobble();

		void Fly();
	}
}
