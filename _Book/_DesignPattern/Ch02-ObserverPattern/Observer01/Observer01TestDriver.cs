﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Observer
{
	class Observer01TestDriver
	{
		static void Main(string[] args)
		{
			WeatherData weatherData = new WeatherData();

			CurrentConditionsDisplay currentConditionDisplay = new CurrentConditionsDisplay(weatherData);
			StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
			ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
			ThirdPartyDisplay thirdPartyDisplay = new ThirdPartyDisplay(weatherData);

			weatherData.SetMeasurements(80, 65, 30.4f);
			Console.WriteLine();
			Thread.Sleep(1000);

			weatherData.SetMeasurements(82, 70, 29.2f);
			Console.WriteLine();
			Thread.Sleep(1000);

			weatherData.SetMeasurements(78, 90, 29.2f);
			Thread.Sleep(1000);
		}
	}
}
