using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Observer
{
	public class WeatherData : ISubject
	{
		private List<IObserver> _observerList;
		private float _temperature;
		private float _humidity;
		private float _pressure;

		public WeatherData()
		{
			_observerList = new List<IObserver>();
		}
	
		public void SetMeasurements(float temperature, float humidity, float pressure)
		{
			_temperature = temperature;
			_humidity = humidity;
			_pressure = pressure;

			MeasurementsChanged();
		}
		
		public void MeasurementsChanged()
		{
			NotifyObserver();
		}

		#region ISubject Members

		public void RegisterObserver(IObserver o)
		{
			_observerList.Add(o);
		}

		public void UnRegisterObserver(IObserver o)
		{
			int index = _observerList.IndexOf(o);

			if (index > 0)
				_observerList.RemoveAt(index);
		}

		public void NotifyObserver()
		{
			foreach (IObserver observer in _observerList)
				observer.Update(_temperature, _humidity, _pressure);
		}

		#endregion
	}
}
