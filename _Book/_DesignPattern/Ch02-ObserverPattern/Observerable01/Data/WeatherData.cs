using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class WeatherData : Observerable<WeatherData>
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;

		public WeatherData()
		{

		}

		#region Properties
		public float Temperature
		{
			get
			{
				return _temperature;
			}
			set
			{
				_temperature = value;
			}
		}

		public float Humidity
		{
			get
			{
				return _humidity;
			}
			set
			{
				_humidity = value;
			}
		}

		public float Pressure
		{
			get
			{
				return _pressure;
			}
			set
			{
				_pressure = value;
			}
		}
		#endregion

		public void MeasurementChanged()
		{
			SetChanged();
			NotifyObserver(this);
		}

		public void SetMeasurements(float temperature, float humidity, float pressure)
		{
			_temperature = temperature;
			_humidity = humidity;
			_pressure = pressure;

			MeasurementChanged();
		}
				
		#region ISubject<WeatherData> ���

		public void RegisterObserver(IObserver<WeatherData> observer)
		{
			base.RegisterObserver(observer);
		}

		public void UnRegisterObserver(IObserver<WeatherData> observer)
		{
			base.UnRegisterObserver(observer);
		}

		#endregion
	}
}
