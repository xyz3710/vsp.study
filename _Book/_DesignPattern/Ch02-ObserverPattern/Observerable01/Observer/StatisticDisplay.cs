using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class StatisticDisplay : IObserver<WeatherData>, IDisplayElement
	{
		private ISubject<WeatherData> _subject;
		private float _maxTemp = 0.0f;
		private float _minTemp = 200;
		private float _temperatureSum = 0.0f;
		private int _numReading = 0;

		#region Constructors
		/// <summary>
		/// StatisticDisplay class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="subject"></param>
		public StatisticDisplay(ISubject<WeatherData> subject)
		{
			_subject = subject;
			_subject.RegisterObserver(this);
		}
		#endregion

		#region IObserver<WeatherData> 멤버

		public void Update(WeatherData args)
		{
			_temperatureSum += args.Temperature;
			_numReading++;

			if (args.Temperature > _maxTemp)
				_maxTemp = args.Temperature;

			if (args.Temperature < _minTemp)
				_minTemp = args.Temperature;

			Display();
		}

		#endregion

		#region IDisplayElement 멤버

		public void Display()
		{
			Console.WriteLine("Statistics\tAvg / Max / Min temperature = {0}F / {1}F / {2}F",
				roundFloatToString(_temperatureSum/_numReading),
				_maxTemp, _minTemp);
		}

		#endregion

		private string roundFloatToString(float p)
		{
			System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("en-US");

			cultureInfo.NumberFormat.CurrencyDecimalDigits = 2;
			cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";

			return p.ToString("F", cultureInfo);
		}
	}
}
