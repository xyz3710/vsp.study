using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class CurrentConditionDisplay : IObserver<WeatherData>, IDisplayElement
	{
		private ISubject<WeatherData> _subject;
		private float _temperature;
		private float _humidity;
		private float _pressure;

		#region Constructors
		/// <summary>
		/// CurrentConditionDisplay class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="subject"></param>
		public CurrentConditionDisplay(ISubject<WeatherData> subject)
		{
			_subject = subject;
			_subject.RegisterObserver(this);
		}
		#endregion

		#region IObserver<WeatherData> 멤버

		public void Update(WeatherData args)
		{
			_temperature = args.Temperature;
			_humidity = args.Humidity;
			_pressure = args.Pressure;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Current condition : {0} F, {1} degree and {2}% humidity.",
				_temperature, _pressure, _humidity);
		}

		#endregion
	}
}
