using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public interface IDisplayElement
	{
		void Display();
	}
}
