using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Observer
{
	public class WeatherData
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;

		public void SetMeasurements(float temperature, float humidity, float pressure)
		{
			_temperature = temperature;
			_humidity = humidity;
			_pressure = pressure;
		}

		#region Properties
		/// <summary>
		/// Get or set Temperature.
		/// </summary>
		public float Temperature
		{
			get
			{
				return _temperature;
			}
			set
			{
				_temperature = value;
			}
		}
        
        /// <summary>
        /// Get or set Humidity.
        /// </summary>
		public float Humidity
		{
			get
			{
				return _humidity;
			}
			set
			{
				_humidity = value;
			}
		}
        
        /// <summary>
        /// Get or set Pressure.
        /// </summary>
		public float Pressure
		{
			get
			{
				return _pressure;
			}
			set
			{
				_pressure = value;
			}
		}
        
        #endregion
        
	}
}
