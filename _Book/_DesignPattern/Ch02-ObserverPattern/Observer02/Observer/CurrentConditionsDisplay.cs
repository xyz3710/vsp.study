using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
	/// <summary>
	/// 현재 측정값을 화면에 표시
	/// </summary>
	public class CurrentConditionsDisplay : IObserver, IDisplayElement
	{
		private ISubject _subject;
		private WeatherData _weatherData;

		public CurrentConditionsDisplay(ISubject subject)
		{
			_subject = subject;

			_subject.RegisterObserver(this);
		}
	
		#region IObserver Members

		public void Update(WeatherData weatherData)
		{
			_weatherData = weatherData;

			Display();
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Current conditions : {0:F1}F degrees and {1:F1}% humidity",
				_weatherData.Temperature, _weatherData.Humidity);
		}

		#endregion
	}
}
