using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class ForecaseDisplay : IObserver, IDisplayElement
	{
		private Observerable _weatherData;
		private float _currentPressure = 29.92f;
		private float _lastPressure;

		public ForecaseDisplay(Observerable weatherData)
		{
			_weatherData = weatherData;

			_weatherData.AddObserver(this);
		}

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("ForecaseDisplay class Current Pressure : {0}, Last Pressure {1}",
				_currentPressure, _lastPressure);
		}

		#endregion

		#region IObserver Members

		public void Update(Observerable o, object arg)
		{
			if (o is WeatherData)
			{
				WeatherData weatherData = (WeatherData)o;

				_lastPressure = _currentPressure;

				_currentPressure = weatherData.Pressure;

				Display();
			}
		}

		#endregion
	}
}
