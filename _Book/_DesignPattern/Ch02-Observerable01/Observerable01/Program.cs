﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	class Program
	{
		static void Main(string[] args)
		{
			WeatherData weather = new WeatherData();

			CurrentConditionDisplay currentCondition = new CurrentConditionDisplay(weather);
			ForecaseDisplay forecast = new ForecaseDisplay(weather);

			weather.SetMeasurements(80, 65, 30.4f);
			weather.SetMeasurements(82, 70, 29.2f);
			weather.SetMeasurements(78, 90, 29.2f);
		}
	}
}
