using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class StatisticDisplay : IObserver, IDisplayElement
	{
		#region IObserver Members

		public void Update(Observerable o, object arg)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
