using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ObserverableClass
{
	public class Observerable
	{
		private ArrayList _observerList;
		private bool _changed;

		public Observerable()
		{
			_observerList = new ArrayList();
			_changed = false;
		}
	
		public bool Changed
		{
			get
			{
				return _changed;
			}
			set
			{
				_changed = value;
			}
		}
	
		public void AddObserver(IObserver observer)
		{
			_observerList.Add(observer);
		}

		public void DeleteObserver(IObserver observer)
		{
			_observerList.Remove(observer);
		}

		public void NotifyObserver()
		{
			NotifyObserver(null);
		}
		
		public void NotifyObserver(object arg)
		{
			if (_changed == true)
			{
				foreach (IObserver observer in _observerList)
					observer.Update(this, arg);

				ClearChanged();
			}
		}

		public void SetChanged()
		{
			_changed = true;
		}

		public void ClearChanged()
		{
			_changed = false;
		}
	}
}
