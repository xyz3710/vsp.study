using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class CurrentConditionDisplay : IObserver, IDisplayElement
	{
		private Observerable _weatherData;
		private float _temperature;
		private float _humidity;
		private float _pressure;

		public CurrentConditionDisplay(Observerable weatherData)
		{
			_weatherData = weatherData;

			_weatherData.AddObserver(this);
		}
	
		#region IObserver Members

		public void Update(Observerable o, object arg)
		{
			if (o is WeatherData)
			{
				WeatherData weather = (WeatherData)o;

				_temperature = weather.Temperature;
				_humidity = weather.Humidity;
				_pressure = weather.Pressure;

				Display();
			}
		}

		#endregion

		#region IDisplayElement Members

		public void Display()
		{
			Console.WriteLine("Current condition : {0} F, {1} degree and {2}% humidity.",
				_temperature, _pressure, _humidity);
		}

		#endregion
	}
}
