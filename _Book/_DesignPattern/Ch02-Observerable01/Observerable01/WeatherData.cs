using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public class WeatherData : Observerable
	{
		private float _temperature;
		private float _humidity;
		private float _pressure;

		public WeatherData()
		{

		}

		#region properties
		public float Temperature
		{
			get
			{
				return _temperature;
			}
			set
			{
				_temperature = value;
			}
		}

		public float Humidity
		{
			get
			{
				return _humidity;
			}
			set
			{
				_humidity = value;
			}
		}

		public float Pressure
		{
			get
			{
				return _pressure;
			}
			set
			{
				_pressure = value;
			}
		}
		#endregion

		public void MeasurementChanged()
		{
			SetChanged();
			NotifyObserver();
		}

		public void SetMeasurements(float temperature, float humidity, float pressure)
		{
			_temperature = temperature;
			_humidity = humidity;
			_pressure = pressure;

			MeasurementChanged();
		}
	}
}
