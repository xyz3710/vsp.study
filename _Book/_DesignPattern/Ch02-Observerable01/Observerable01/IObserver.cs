using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverableClass
{
	public interface IObserver
	{
		void Update(Observerable o, object arg);
	}
}
