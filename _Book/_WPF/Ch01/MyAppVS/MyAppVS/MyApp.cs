using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace MyFirstWpfApp
{
	public partial class MyApp : Application
	{
		public void AppStartingUp(object sender, StartupEventArgs e)
		{
			/*
			Window window = new Window();

			window.Title = "Hello, WPF";
			window.Show();
			*/

			MyWindow mWindow = new MyWindow();

			mWindow.Show();
		}
	}

	public class MyWindow : Window
	{
		public MyWindow()
		{
			Title = "Hello, WPF";
			
			BitmapImage bitmap = new BitmapImage();
			
			bitmap.BeginInit();
			bitmap.UriSource = new Uri("Flag.jpg",  UriKind.Relative);
			bitmap.EndInit();

			Image buttonImage = new Image
			{
				Stretch = System.Windows.Media.Stretch.Fill,
				Source = bitmap,
			};
			Button button = new Button
			{
				Content = buttonImage,
				Width = 200,
				Height = 50,
			};
			button.Click += (s, e) =>
			{
				MessageBox.Show("버튼을 클릭했습니다.", "Hello, WPF Button");
			};

			var btnMargin = button.Margin;
			Button btnClose = new Button
			{
				Content = "닫기",
				Width = 200,
				Height = 25,
				Margin = new Thickness(btnMargin.Left, button.Margin.Top + 50, btnMargin.Right, btnMargin.Bottom),
			};
			btnClose.Click += (s, e) =>
			{
				Close();
			};

			Grid grid = new Grid();

			grid.Children.Add(button);
			grid.Children.Add(btnClose);
			
			AddChild(grid);
		}
	}

	class Program
	{
		[STAThread]
		static void Main2(string[] args)
		{
			MyApp app = new MyApp();

			app.Startup += app.AppStartingUp;
			app.Run();

			//MessageBox.Show("Hello, WPF");
		}
	}
}
