﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssemblyA
{
	class Program
	{
		public Program (int val1, int val2)
		{
			Console.WriteLine("Constructor with the values {0}, {1} in domain {2} called", val1, val2, AppDomain.CurrentDomain.FriendlyName);
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Main in domain {0} called", AppDomain.CurrentDomain.FriendlyName);
		}
	}
}
