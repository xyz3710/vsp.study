﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Windows.Forms;
using RemoteObjectTest;

namespace HelloClientByConfig
{
	class HelloClientByConfig
	{
		static void Main(string[] args)
		{
			RemotingConfiguration.Configure(string.Format("{0}.config", Application.ExecutablePath), false);
			Hello obj = new Hello();

			if (obj == null)
			{
				Console.WriteLine("Could not locate server");

				return;
			}

			for (int i = 0; i < 3; i++)
				Console.WriteLine(obj.Greeting("기원"));
			
			//InvokeActivatedClientTypes();
			//InvokeWellKnownClientTypes();
		}

		private static void InvokeWellKnownClientTypes()
		{
			WellKnownClientTypeEntry[] entries = RemotingConfiguration.GetRegisteredWellKnownClientTypes();
			Hello obj = null;

			foreach (WellKnownClientTypeEntry entry in entries)
			{
				obj = Activator.GetObject(entry.ObjectType, entry.ApplicationUrl) as Hello;

				if (obj != null && RemotingServices.IsTransparentProxy(obj) == true)
					break;
			}

			if (obj == null)
			{
				Console.WriteLine("Could not locate WellKnown server");

				return;
			}

			for (int i = 0; i < 3; i++)
				Console.WriteLine(obj.Greeting("기원"));
		}

		private static void InvokeActivatedClientTypes()
		{
			ActivatedClientTypeEntry[] entries = RemotingConfiguration.GetRegisteredActivatedClientTypes();
			Hello obj = null;

			foreach (ActivatedClientTypeEntry entry in entries)
			{
				obj = Activator.GetObject(typeof(Hello), entry.ApplicationUrl) as Hello;

				if (obj != null && RemotingServices.IsTransparentProxy(obj) == true)
					break;
			}

			if (obj == null)
			{
				Console.WriteLine("Could not locate Activated server");

				return;
			}

			for (int i = 0; i < 3; i++)
				Console.WriteLine(obj.Greeting("기원"));
		}
	}
}
