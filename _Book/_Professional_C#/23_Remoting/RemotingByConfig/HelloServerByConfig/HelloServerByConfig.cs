﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Windows.Forms;

namespace RemotingByConfig
{
	class HelloServerByConfig
	{
		static void Main(string[] args)
		{
			RemotingConfiguration.Configure(string.Format("{0}.config", Application.ExecutablePath), false);

			Console.WriteLine("Application : {0}", RemotingConfiguration.ApplicationName);

			ShowActivatedServiceTypes();
			ShowWellKnownServiceTypes();

			Console.WriteLine("Hit to exit");
			Console.ReadLine();
		}

		private static void ShowActivatedServiceTypes()
		{
			ActivatedServiceTypeEntry[] entries = RemotingConfiguration.GetRegisteredActivatedServiceTypes();

			foreach (ActivatedServiceTypeEntry entry in entries)
			{
				Console.WriteLine("Assembly\t: {0}", entry.AssemblyName);
				Console.WriteLine("Type\t\t: {0}", entry.TypeName);
			}
		}

		private static void ShowWellKnownServiceTypes()
		{
			WellKnownServiceTypeEntry[] entries = RemotingConfiguration.GetRegisteredWellKnownServiceTypes();

			foreach (WellKnownServiceTypeEntry entry in entries)
			{
				Console.WriteLine("Assembly\t: {0}", entry.AssemblyName);
				Console.WriteLine("Mode\t\t: {0}", entry.Mode);
				Console.WriteLine("URI\t\t: {0}", entry.ObjectUri);
				Console.WriteLine("Type\t\t: {0}", entry.TypeName);
			}
		}
	}
}
