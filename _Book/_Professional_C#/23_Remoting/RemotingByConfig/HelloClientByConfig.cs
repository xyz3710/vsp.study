﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using RemoteObjectTest;

namespace HelloClientByConfig
{
	class HelloClientByConfig
	{
		static void Main(string[] args)
		{
			RemotingConfiguration.Configure("HelloClientByConfig.exe.config", false);
			Hello obj = new Hello();

			if (obj != null)
			{
				Console.WriteLine("Could not locate server");

				return;
			}

			for (int i = 0; i < 3; i++)
				Console.WriteLine(obj.Greeting("기원"));
		}
	}
}
