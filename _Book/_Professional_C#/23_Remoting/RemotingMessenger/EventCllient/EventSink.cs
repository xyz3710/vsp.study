﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;

namespace RemoteCollection
{
	public class EventSink : MarshalByRefObject
	{
		/// <summary>
		/// EventSink class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public EventSink()
		{
		}

		[OneWay]
		public void StatusHandler(object sender, StatusEventArgs e)
		{
			Console.WriteLine("EventSink : Event occurred : {0}", e.Message);
		}
	}
}
