﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RemotingEventTest
{
	[Serializable]
	public class StatusEventArgs
	{
		private string _message;

		/// <summary>
		/// StatusEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="message"></param>
		public StatusEventArgs(string message)
		{
			_message = message;
		}

		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}
	}	
}
