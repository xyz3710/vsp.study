﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace RemotingEventTest
{
	public delegate void StatusEvent(object sender, StatusEventArgs e);

	public class RemoteObject : MarshalByRefObject
	{
		public event StatusEvent Status;

		private string _hostIpAddress;

		/// <summary>
		/// RemoteObject class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public RemoteObject()
			: this("localhost")
		{
		}

        /// <summary>
		/// RemoteObject class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="hostName"></param>
		public RemoteObject(string hostName)
		{
			_hostIpAddress = hostName;
			Console.WriteLine("".PadRight(79, '='));
			Console.WriteLine("{0} => {1}, {2}", DateTime.Now, _hostIpAddress, "RemoteObject 생성자 호출");
		}

		~RemoteObject()
		{
			Console.WriteLine("{0} => {1}, {2}", DateTime.Now, _hostIpAddress, "RemoteObject 소멸자 호출");
		}


		public string HostIpAddress
		{
			get
			{
				return _hostIpAddress;
			}
			set
			{
				_hostIpAddress = value;
			}
		}

		public void LongWorking(int ms)
		{
			Console.WriteLine("".PadRight(79, '+'));
			Console.WriteLine("{0} => {1}, {2}", _hostIpAddress, "RemoteObject", "LongWorking() 시작됨");

			StatusEventArgs e = new StatusEventArgs("Client를 위한 메세지 : LongWorking() 시작됨");

			Debug.WriteLine(Status == null ? "Null" : Status.ToString(), "My");
			
			// 이벤트 발생
			if (Status != null)
			{
				Console.WriteLine("\tRemoteObject : 시작 이벤트 발생");
				Status(this, e);
			}

			Thread.Sleep(ms);

			e.Message = "Client를 위한 메세지 : LongWorking() 종료";

			// 끝 이벤트 발생
			if (Status != null)
			{
				Console.WriteLine("\tRemoteObject : 종료 이벤트 발생");
				Status(this, e);
			}

			Console.WriteLine("{0} => {1}, {2}", _hostIpAddress, "RemoteObject", "LongWorking() 종료");
		}
	}
}
