﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Windows.Forms;
using System.Net;

namespace RemotingEventTest
{
	class EventCllient
	{
		static void Main(string[] args)
		{
			string localIp = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();
			
			RemotingConfiguration.Configure(string.Format("{0}.config", Application.ExecutablePath), false);
			EventSink sink = new EventSink();

			RemoteObject obj = new RemoteObject(localIp);

			// 클라이언트 싱크를 서버에 등록 한다-이벤트를 추가한다.
			obj.Status += new StatusEvent(sink.StatusHandler);
						
			obj.LongWorking(3000);

			// 이벤트를 해지 한다.
			//obj.Status -= new StatusEvent(sink.StatusHandler);
			//obj.LongWorking(3000);
			Console.ReadLine();
		}
	}
}
