using System;
using System.Runtime.Remoting;
using System.Collections;

// Class to be used by clients to submit delegates to the ChatCoordinator object.
// Clients must derive a custom class from this and override the InternalSubmissionCallback
// function.  InternalSubmissionCallback is where they need to implement their callback
// logic.  They must use the SubmissionCallback function in their remotable delegates. 
public abstract class RemotelyDelegatableObject : MarshalByRefObject
{
	public void SubmissionCallback(object sender, SubmitEventArgs submitArgs)
	{
		InternalSubmissionCallback(sender, submitArgs);
	}

	protected abstract void InternalSubmissionCallback(object sender, SubmitEventArgs submitArgs);
}
