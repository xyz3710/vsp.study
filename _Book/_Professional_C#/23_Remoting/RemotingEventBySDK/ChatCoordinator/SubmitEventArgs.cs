﻿using System;
using System.Runtime.Remoting;
using System.Collections;

// Define the class that contains the information for a Submission event.
[Serializable]
public class SubmitEventArgs : EventArgs
{
	private string _string = null;
	private string _alias = null;

	public SubmitEventArgs(string contribution, string contributor)
	{
		this._string = contribution;
		this._alias = contributor;
	}

	public string Contribution
	{
		get
		{
			return _string;
		}
	}

	public string Contributor
	{
		get
		{
			return _alias;
		}
	}
}
