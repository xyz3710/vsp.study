﻿using System;
using System.Runtime.Remoting;

public class ServerProcess
{
	// This simply keeps the ChatCoordinator application domain alive.
	public static void Main(string[] Args)
	{
		RemotingConfiguration.Configure("ChatCentral.exe.config", false);

		Console.WriteLine("The host application domain is running. Press Enter again to stop the application domain.");
		Console.ReadLine();
	}
}
