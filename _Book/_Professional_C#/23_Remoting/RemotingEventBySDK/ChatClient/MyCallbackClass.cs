using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;


// MyCallbackClass is the class that contains the callback function to
// which ChatClient will submit a delegate to the server.
// To to pass a reference to this method (that is, a delegate)
// across an application domain boundary, this class must extend 
// MarshalByRefObject or a class that extends MarshallByRefObject like all 
// other remotable types.  MyCallbackClass extends RemotelyDelegatableObject because 
// RemotelyDelegatableObject is a class that the server can obtain type information
// for.
class MyCallbackClass : RemotelyDelegatableObject
{
	private string _alias = null;

	public MyCallbackClass()
	{
	}
	public MyCallbackClass(string alias)
	{
		_alias = alias;
	}

	// InternalSubmissionCallback is the method that is called by 
	// RemotelyDelegatableObject.SubmissionCallback().  SubmissionCallback() is
	// sent to the server via a delegate.  You want the chat server to call
	// when the Submission event occurs -- even if the submission is yours.
	// The SubmissionEventHandler delegate wraps this function and is passed 
	// to the Add_Submission (SubmissionEventHandler delegate) member of 
	// the ChatCoordinator object. The .NET Remoting system handles the transfer
	// of all information about the client object and channel necessary to 
	// make a return remote call when the event occurs.
	protected override void InternalSubmissionCallback(object sender, SubmitEventArgs submitArgs)
	{
		// Block out your own submission.
		// This simple chat server does not filter anything.
		if (String.Compare(submitArgs.Contributor, _alias, true) == 0)
		{
			Console.WriteLine("Your message was broadcast.");
		}
		else
			Console.WriteLine(submitArgs.Contributor 
				+ " says:\r\n" 
				+ new String('-', 80) 
				+ submitArgs.Contribution 
				+ "\r\n" 
				+ new String('-', 80) 
				+ "\r\n");
	}

	// This override ensures that if the object is idle for an extended 
	// period, waiting for messages, it won't lose its lease. Without this 
	// override (or an alternative, such as implementation of a lease 
	// sponsor), an idle object that inherits from MarshalByRefObject 
	// may be collected even though references to it still exist.
	public override object InitializeLifetimeService()
	{
		return null;
	}
}
