﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

public class ChatClient
{
	private string _alias = null;

	public ChatClient(string alias)
	{
		this._alias = alias;
	}

	public void Run()
	{
		RemotingConfiguration.Configure("ChatClient.exe.config", false);

		// Create a proxy to the remote object.
		ChatCoordinator chatcenter = new ChatCoordinator();

		// Create a remotely delegatable object
		MyCallbackClass callback = new MyCallbackClass(_alias);

		// Create a new delegate for the method that you want the event to call
		// when it occurs on the server. The SubmissionReceiver method will
		// then be a remote call for the server object; therefore, you must 
		// register a channel to listen for this call back from the server.
		chatcenter.Submission += new SubmissionEventHandler(callback.SubmissionCallback);
		String keyState = "";

		while (true)
		{
			Console.WriteLine("Press 0 (zero) and ENTER to Exit:\r\n");
			keyState = Console.ReadLine();

			if (String.Compare(keyState, "0", true) == 0)
				break;
			// Call the server with the string you submitted and your alias.
			chatcenter.Submit(keyState, _alias);
		}
		//chatcenter.Submission -= new SubmissionEventHandler(callback.SubmissionCallback);
	}


	// Args[0] is the alias that will be used.
	public static void Main(string[] Args)
	{

		if (Args.Length != 1)
		{
			Console.WriteLine("You need to type an alias.");
			return;
		}

		ChatClient client = new ChatClient(Args[0]);
		client.Run();
	}
}
