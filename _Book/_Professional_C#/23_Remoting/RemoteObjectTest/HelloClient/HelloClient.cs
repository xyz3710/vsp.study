﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Services;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Remoting.Lifetime;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace RemoteObjectTest
{
	public class HelloClient
	{
		public static void Main(string[] args)
		{
			// Phase 1
			ChannelServices.RegisterChannel(new TcpClientChannel(), false);
			Hello obj = (Hello)Activator.GetObject(typeof(Hello), "tcp://localhost:8086/Hi123");
			Hello hello1 = RemotingServices.Connect(typeof(Hello), "tcp://localhost:8086/Hi123") as Hello;
			Hello hello2 = new Hello();

			if (obj == null || hello1 == null)
			{
				Console.WriteLine("Could not locate server");
				
				return;
			}

			#region Phase 4 Call Context
			CallContextData cookie = new CallContextData();

			cookie.Data = "Information for the server";

			CallContext.SetData("myCookie", cookie);
			#endregion

			for (int i = 0; i < 3; i++)
			{
				string greeting = string.Empty;
				// 동기 버전
				//greeting = obj.Greeting("Christian");
				// 비동기 버전
				GreetingDelegate d = new GreetingDelegate(obj.Greeting);
				IAsyncResult ar = d.BeginInvoke("기원", null, null);

				// 작업을 수행하고 기다린다.
				ar.AsyncWaitHandle.WaitOne();

				if (ar.IsCompleted == true)
					greeting = d.EndInvoke(ar);

				Console.WriteLine("obj's Greeting : {0}", greeting);
				Console.WriteLine("hello1's Greeting : {0}", hello1.Greeting("Charlse"));
			}

			if (RemotingServices.IsTransparentProxy(obj) == true)
			{
				Console.WriteLine("Using a transparent proxy");

				RealProxy realProxy = RemotingServices.GetRealProxy(obj);

				//realProxy.Invoke(message);
			}

			// Phase 2
			MySerialized mySerialized = obj.GetMySerialized();

			if (RemotingServices.IsTransparentProxy(mySerialized) == false)
				Console.WriteLine("mySerialized is not a transparent proxy");

			mySerialized.Foo();

			MyRemote myRemote = obj.GetMyRemote();

			if (RemotingServices.IsTransparentProxy(myRemote) == true)
				Console.WriteLine("myRemote is a transparent proxy");

			myRemote.Foo();

			ILease lease = obj.GetLifetimeService() as ILease;

			if (lease != null)
			{
				Console.WriteLine("Lease Configuration :");
				Console.WriteLine("InitialLeaseTime : {0}", lease.InitialLeaseTime);
				Console.WriteLine("RenewOnCallTime : {0}", lease.RenewOnCallTime);
				Console.WriteLine("SponsorshipTimeout : {0}", lease.SponsorshipTimeout);
				Console.WriteLine(lease.CurrentLeaseTime);
			}
		}

		private static void CallBack(IAsyncResult ar)
		{
			string greeting = string.Empty;

			if (ar.IsCompleted == true)
				greeting = ar.AsyncState as string;

			Console.WriteLine("obj's Greeting : {0}", greeting);
		}

		// Phase 3 -- Asynchronous Remoting
		private delegate string GreetingDelegate(string name);
		private static string _greeting;
	}
}
