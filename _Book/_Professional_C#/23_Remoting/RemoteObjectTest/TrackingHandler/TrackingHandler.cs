﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Services;
using System.Runtime.Remoting.Channels;

namespace RemoteObjectTest
{
	public class TrackingHandler : ITrackingHandler
	{
		#region ITrackingHandler 멤버

		public void DisconnectedObject(object obj)
		{
			Console.WriteLine("Disconnect");
		}

		public void MarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
		{
			Console.WriteLine(string.Format("--- Marshaled Object {0} ---", obj.GetType()));
			Console.WriteLine("Object URI : {0}", or.URI);

			object[] channelData = or.ChannelInfo.ChannelData;

			foreach (object data in channelData)
			{
				ChannelDataStore dataStore = data as ChannelDataStore;

				if (dataStore != null)
					foreach (string uri in dataStore.ChannelUris)
						Console.WriteLine("Channel URI : {0}", uri);
			}

			Console.WriteLine("----------------------");
			Console.WriteLine();
		}

		public void UnmarshaledObject(object obj, System.Runtime.Remoting.ObjRef or)
		{
			Console.WriteLine("Unmarshal");
		}

		#endregion
	}
}
