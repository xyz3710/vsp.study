﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace RemoteObjectTest
{
	public class CustomClientChannel : CustomChannel, IChannelSender
	{
		#region IChannelSender 멤버

		public System.Runtime.Remoting.Messaging.IMessageSink CreateMessageSink(string url, object remoteChannelData, out string objectURI)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
