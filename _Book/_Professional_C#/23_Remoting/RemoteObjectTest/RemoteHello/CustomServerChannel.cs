﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels;

namespace RemoteObjectTest
{
	public class CustomServerChannel : CustomChannel, IChannelReceiver
	{
		#region IChannelReceiver 멤버

		public object ChannelData
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		public string[] GetUrlsForUri(string objectURI)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void StartListening(object data)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void StopListening(object data)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
