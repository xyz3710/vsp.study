﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace RemoteObjectTest
{
	[Serializable]
	public class MySerialized
	{
		protected int _a;

		/// <summary>
		/// MySerialized class의 새 인스턴스를 초기화합니다.
		/// </summary>
		/// <param name="val"></param>
		public MySerialized(int val)
		{
			_a = val;
		}

		public int A
		{
			get
			{
				Console.WriteLine("MySerialized.A called");

				return _a;
			}
			set
			{
				_a = value;
			}
		}

		public void Foo()
		{
			Console.WriteLine("MySerialized.Foo called");
		}
	}
}
