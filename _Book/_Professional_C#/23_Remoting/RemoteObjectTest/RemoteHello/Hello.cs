﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Lifetime;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace RemoteObjectTest
{
	public class Hello : MarshalByRefObject
	{
		// Phase 1
		/// <summary>
		/// Hello class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Hello()
		{
			Console.WriteLine("생성자 호출");
		}

		~Hello()
		{
			Console.WriteLine("소멸자 호출");
		}

		public string Greeting(string name)
		{
			string result = string.Format("Hello, {0}", name);

			Console.WriteLine("Greeting 호출");

			// Phase 4 Call Context
			#region Phase 4 Call Context
			CallContextData cookie = CallContext.GetData("myCookie") as CallContextData;

			//if (cookie != null)
				Console.WriteLine("Cookie : {0}", cookie.Data);            
			#endregion

			return result;
		}

		// Phase 2
		public MySerialized GetMySerialized()
		{
			return new MySerialized(4711);
		}

		public MyRemote GetMyRemote()
		{
			return new MyRemote(4712);
		}

		public override object InitializeLifetimeService()
		{
			ILease lease = base.InitializeLifetimeService() as ILease;

			if (lease != null)
			{
				lease.InitialLeaseTime = TimeSpan.FromMinutes(10d);
				lease.RenewOnCallTime = TimeSpan.FromSeconds(40);
			}

			return lease;
		}

		// Phase 3 Asynchronous Remoting
		[OneWay]
		public void TakeAWhile(int ms)
		{
			Console.WriteLine("Take a while started.");
			Thread.Sleep(ms);
			Console.WriteLine("Take a while finished");
		}
	}
}
