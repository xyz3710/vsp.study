﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels;

namespace RemoteObjectTest
{
	public class CustomChannel : IChannel
	{
		#region IChannel 멤버

		public string ChannelName
		{
			get
			{
				return "cch";
			}
		}

		public int ChannelPriority
		{
			get
			{
				return 1;
			}
		}

		public string Parse(string url, out string objectURI)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
