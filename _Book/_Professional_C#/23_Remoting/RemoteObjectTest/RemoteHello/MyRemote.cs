﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RemoteObjectTest
{
	public class MyRemote : MarshalByRefObject
	{
		protected int _a;

		/// <summary>
		/// MyRemote class의 새 인스턴스를 초기화합니다.
		/// </summary>
		/// <param name="val"></param>
		public MyRemote(int val)
		{
			_a = val;
		}

		public void Foo()
		{
			Console.WriteLine("MyRemote.Foo called");	
		}

		public int A
		{
			get
			{
				Console.WriteLine("MyRemote.A called");

				return _a;
			}
			set
			{
				_a = value;
			}
		}
	}
}
