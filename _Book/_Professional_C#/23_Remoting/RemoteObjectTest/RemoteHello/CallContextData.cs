﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;

namespace RemoteObjectTest
{
	[Serializable]
	public class CallContextData : ILogicalThreadAffinative
	{
		/// <summary>
		/// CallContextData class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public CallContextData()
		{
		}

		protected string data;
		
		public string Data
		{
			get
			{
				return data;
			}
			set
			{
				data = value;
			}
		}
	}
}
