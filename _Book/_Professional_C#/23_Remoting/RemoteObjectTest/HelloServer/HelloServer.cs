﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Collections.Specialized;
using System.Runtime.Remoting.Services;

namespace RemoteObjectTest
{
	public class HelloServer
	{
		public static void Main(string[] args)
		{
			ListDictionary properties = new ListDictionary();

			properties.Add("name", "TCP Channel with a SOAP Formatter");
			properties.Add("priority", 1);
			properties.Add("port", 8086);

			SoapServerFormatterSinkProvider sinkProvider = new SoapServerFormatterSinkProvider();
			//TcpServerChannel tcpChannel = new TcpServerChannel(properties, sinkProvider);
			TrackingServices.RegisterTrackingHandler(new TrackingHandler());		// Tracking Service
			TcpServerChannel tcpChannel = new TcpServerChannel(8086);
			HttpChannel httpChannel = new HttpChannel(8085);

			ShowChannelProperties(tcpChannel);
			ShowChannelProperties(httpChannel);

			ChannelServices.RegisterChannel(tcpChannel, false);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(Hello),						// 형식
                "Hi123",							// URI
                WellKnownObjectMode.Singleton);	// 모드
			
			Console.WriteLine("Hit to exit");
			Console.ReadLine();
		}

		protected static void ShowChannelProperties(IChannelReceiver channel)
		{
			Console.WriteLine("Name :\t{0}", channel.ChannelName);
			Console.WriteLine("Priority :\t{0}", channel.ChannelPriority);

			if (channel is HttpChannel)
			{
				HttpChannel httpChannel = channel as HttpChannel;

				if (httpChannel != null)
					Console.WriteLine("Scheme :\t{0}", httpChannel.ChannelScheme);
			}

			ChannelDataStore data = (ChannelDataStore)channel.ChannelData;

			foreach (string uri in data.ChannelUris)
				Console.WriteLine("URI :\t{0}", uri);

			Console.WriteLine();
		}
	}
}
