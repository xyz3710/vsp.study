﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace EventLogForm
{
	public partial class EventLogForm : Form
	{
		public EventLogForm()
		{
			InitializeComponent();
		}

		private void EventLogForm_Load(object sender, EventArgs e)
		{
			EventLogTraceListener eventLogTraceListener = new EventLogTraceListener(eventLog);
			Trace.Listeners.Add(eventLogTraceListener);
		}

		private void btnCreateEvent_Click(object sender, EventArgs e)
		{
			const string MESSAGE = "Test EventLog message";
            
			EventLog.WriteEntry("EventLogForm", MESSAGE, EventLogEntryType.Information);
			Trace.WriteLine(MESSAGE);
		}

		private void eventLog_EntryWritten(object sender, EntryWrittenEventArgs e)
		{
			DateTime time = e.Entry.TimeGenerated;
			string message = e.Entry.Message;

			lstEventLog.Items.Add(string.Format("{0} {1}", time, message));
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}