﻿namespace EventLogForm
{
	partial class EventLogForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.eventLog = new System.Diagnostics.EventLog();
			this.btnCreateEvent = new System.Windows.Forms.Button();
			this.lstEventLog = new System.Windows.Forms.ListBox();
			this.btnClose = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.eventLog)).BeginInit();
			this.SuspendLayout();
			// 
			// eventLog
			// 
			this.eventLog.EnableRaisingEvents = true;
			this.eventLog.Log = "Application";
			this.eventLog.Source = "QuoteService";
			this.eventLog.SynchronizingObject = this;
			this.eventLog.EntryWritten += new System.Diagnostics.EntryWrittenEventHandler(this.eventLog_EntryWritten);
			// 
			// btnCreateEvent
			// 
			this.btnCreateEvent.Location = new System.Drawing.Point(130, 334);
			this.btnCreateEvent.Name = "btnCreateEvent";
			this.btnCreateEvent.Size = new System.Drawing.Size(75, 23);
			this.btnCreateEvent.TabIndex = 0;
			this.btnCreateEvent.Text = "Event 생성";
			this.btnCreateEvent.UseVisualStyleBackColor = true;
			this.btnCreateEvent.Click += new System.EventHandler(this.btnCreateEvent_Click);
			// 
			// lstEventLog
			// 
			this.lstEventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lstEventLog.FormattingEnabled = true;
			this.lstEventLog.ItemHeight = 12;
			this.lstEventLog.Location = new System.Drawing.Point(12, 13);
			this.lstEventLog.Name = "lstEventLog";
			this.lstEventLog.Size = new System.Drawing.Size(450, 304);
			this.lstEventLog.TabIndex = 1;
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Location = new System.Drawing.Point(270, 334);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "&Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// EventLogForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(475, 369);
			this.Controls.Add(this.lstEventLog);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnCreateEvent);
			this.Name = "EventLogForm";
			this.Text = "Quote Service Event Listener";
			this.Load += new System.EventHandler(this.EventLogForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.eventLog)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Diagnostics.EventLog eventLog;
		private System.Windows.Forms.Button btnCreateEvent;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ListBox lstEventLog;
	}
}

