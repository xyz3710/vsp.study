﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace QuoteServices
{
	public partial class QuoteClient : Form
	{
		public QuoteClient()
		{
			InitializeComponent();
		}

		private void btnGetQuote_Click(object sender, EventArgs e)
		{
			statusMessage.Text = string.Empty;
			string serverIp = txtHost.Text;
			int port = 80;

			try
			{
				port = Convert.ToInt32(txtPort.Text);
			}
			catch (FormatException ex)
			{
				statusMessage.Text = ex.Message;

				return;
			}

			TcpClient client = new TcpClient();

			try
			{
				IPAddress ipAddress = Dns.GetHostEntry(serverIp).AddressList[0];//IPAddress.Parse(serverIp);

				client.Connect(ipAddress, port);

				NetworkStream stream = client.GetStream();
				byte[] buffer = new byte[1024];

				stream.ReadTimeout = 2500;

				int received = stream.Read(buffer, 0, buffer.Length);

				if (received <= 0)
				{
					statusMessage.Text = "Read fail !!!";

					return;
				}

				lstQuote.Items.Insert(0, Encoding.Unicode.GetString(buffer));
			}
			catch (SocketException ex)
			{
				statusMessage.Text = ex.Message;
			}
			catch (Exception ex)
			{
				statusMessage.Text = ex.Message;
			}
			finally
			{
				if (client != null)
					client.Close();
			}
		}

		private void QuoteClient_Load(object sender, EventArgs e)
		{
			// Get local ip address
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());

			txtHost.Text = ipEntry.AddressList[0].ToString(); 
		}
	}
}