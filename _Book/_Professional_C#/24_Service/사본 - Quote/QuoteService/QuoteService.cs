﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

namespace QuoteServices
{
	public partial class QuoteService : ServiceBase
	{
		private QuoteServer _quoteServer;

		public QuoteService()
		{
			InitializeComponent();

			_quoteServer = new QuoteServer(@"C:\QuoteServer.cs", 8005);
		}

		protected override void OnStart(string[] args)
		{
			_quoteServer.Start();
		}

		protected override void OnStop()
		{
			_quoteServer.Stop();
		}

		protected override void OnPause()
		{
			_quoteServer.Suspend();
		}

		protected override void OnContinue()
		{
			_quoteServer.Resume();
		}

		protected override void OnShutdown()
		{
			_quoteServer.Stop();
		}

		private const int COMMAND_REFRESH = 128;

		protected override void OnCustomCommand(int command)
		{
			switch (command)
			{
				case COMMAND_REFRESH:
					_quoteServer.RefreshQuotes();

					break;
				default:
					break;
			}
		}
	}
}
