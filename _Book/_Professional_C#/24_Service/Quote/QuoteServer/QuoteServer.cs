﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Collections.Specialized;
using System.Threading;
using System.IO;
using System.Net;
using System.ComponentModel;

namespace QuoteServices
{
	public class QuoteServer : Component
	{
		private TcpListener _listener;
		private int _port;
		private string _filename;
		private StringCollection _quotes;
		private Random _random;
		private Thread _listenerThread;
		private bool _aborted;
        private System.Diagnostics.PerformanceCounter pcBytesSent;
		private System.Diagnostics.PerformanceCounter pcBytesSentPerSec;
		private System.Diagnostics.PerformanceCounter pcRequests;
		private System.Diagnostics.PerformanceCounter pcRequestsPerSec;
		private System.Diagnostics.EventLog eventLog;
		private System.Windows.Forms.Timer timer;
		private bool _suspend;
		private IContainer components;
		private long _bytesPreSec;
		private long _requestPerSec;

		/// <summary>
		/// QuoteServer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public QuoteServer()
			: this("quotes.txt")
		{
		}

        /// <summary>
		/// QuoteServer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="filename"></param>
		public QuoteServer(string filename)
			: this(filename, 8005)
		{
		}

        /// <summary>
		/// QuoteServer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="port"></param>
		public QuoteServer(string filename, int port)
		{
			_port = port;
			_filename = filename;
		}

		protected void ReadQuotes()
		{
			_quotes = new StringCollection();
			Stream stream = File.OpenRead(_filename);
			StreamReader streamReader = new StreamReader(stream);
			string quote = string.Empty;

			try
			{
				while ((quote = streamReader.ReadLine()) != null)
				{
					if (string.IsNullOrEmpty(quote) == true)
						continue;

					_quotes.Add(quote);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				if (streamReader != null)
					streamReader.Close();

				if (stream != null)
					stream.Close();                
			}

			_random = new Random();
		}

		protected string GetRandomQuoteOfDay()
		{
			int index = _random.Next(0, _quotes.Count);

			return string.Format("{0, 6} : {1}", index, _quotes[index]);
		}

		protected void Listener()
		{
			_listener = new TcpListener(IPAddress.Loopback, _port);
			_listener.Start();

			while (_aborted == false)
			{
				if (_suspend == false)
				{
					//_listener.BeginAcceptSocket(Callback, _listener);
					#region 동기 방식
					try
					{
						Socket socket = _listener.AcceptSocket();

						if (socket == null)
							return;

						string message = GetRandomQuoteOfDay();
						UnicodeEncoding encoder = new UnicodeEncoding();
						byte[] buffer = encoder.GetBytes(message);

						try
						{
							socket.Send(buffer);

							// 성능 카운터를 위해 추가된 부분
							pcRequests.Increment();
							pcBytesSent.IncrementBy((long)buffer.Length);
							// 개선
							_requestPerSec++;
							_bytesPreSec += buffer.Length;
						}
						catch
						{
						}
						finally
						{
							if (socket != null)
								socket.Close();
						}
					}
					catch (SocketException ex)
					{
						if (ex.ErrorCode == 10004)
							return;

						string message = string.Format("Quote Server failed in LIstener : {0}", ex.Message);

						eventLog.WriteEntry(message, System.Diagnostics.EventLogEntryType.Error);
					}
					#endregion
				}

				Thread.Sleep(1);
			}
		}

		private void Callback(IAsyncResult ar)
		{			
			TcpListener tcpListener = ar.AsyncState as TcpListener;

			if (tcpListener == null)
            	return;

			try
			{
				Socket socket = tcpListener.EndAcceptSocket(ar);

				if (socket == null)
					return;

				string message = GetRandomQuoteOfDay();
				UnicodeEncoding encoder = new UnicodeEncoding();
				byte[] buffer = encoder.GetBytes(message);

				try
				{
					socket.Send(buffer);
				}
				catch
				{
				}
				finally
				{
					if (socket != null)
						socket.Close();
				}
			}
			catch 
			{				
			}
		}

		public void RefreshQuotes()
		{
			ReadQuotes();
		}

		public void Start()
		{
			ReadQuotes();

			_aborted = false;
			_listenerThread = new Thread(new ThreadStart(Listener));
			_listenerThread.Start();
			_listenerThread.Join(10);
		}

		public void Stop()
		{
			_aborted = true;
			_listener.Stop();
		}

		public void Suspend()
		{
			_suspend = true;
		}

		public void Resume()
		{
			_suspend = false;
		}

		// 성능 카운터를 위한 method
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.pcBytesSent = new System.Diagnostics.PerformanceCounter();
			this.pcBytesSentPerSec = new System.Diagnostics.PerformanceCounter();
			this.pcRequests = new System.Diagnostics.PerformanceCounter();
			this.pcRequestsPerSec = new System.Diagnostics.PerformanceCounter();
			this.eventLog = new System.Diagnostics.EventLog();
			this.timer = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.pcBytesSent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pcBytesSentPerSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pcRequests)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pcRequestsPerSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.eventLog)).BeginInit();
			// 
			// pcBytesSent
			// 
			this.pcBytesSent.CategoryName = "Quote Service Counts";
			this.pcBytesSent.CounterName = "# of bytes sent";
			this.pcBytesSent.MachineName = "KimKiWon";
			this.pcBytesSent.ReadOnly = false;
			// 
			// pcBytesSentPerSec
			// 
			this.pcBytesSentPerSec.CategoryName = "Quote Service Counts";
			this.pcBytesSentPerSec.CounterName = "# of bytes sent / sec";
			this.pcBytesSentPerSec.MachineName = "KimKiWon";
			this.pcBytesSentPerSec.ReadOnly = false;
			// 
			// pcRequests
			// 
			this.pcRequests.CategoryName = "Quote Service Counts";
			this.pcRequests.CounterName = "# of requests";
			this.pcRequests.MachineName = "KimKiWon";
			this.pcRequests.ReadOnly = false;
			// 
			// pcRequestsPerSec
			// 
			this.pcRequestsPerSec.CategoryName = "Quote Service Counts";
			this.pcRequestsPerSec.CounterName = "# of requests /sec";
			this.pcRequestsPerSec.MachineName = "KimKiWon";
			this.pcRequestsPerSec.ReadOnly = false;
			// 
			// eventLog
			// 
			this.eventLog.EnableRaisingEvents = true;
			this.eventLog.Log = "Application";
			this.eventLog.Source = "QuoteService";
			// 
			// timer
			// 
			this.timer.Enabled = true;
			this.timer.Interval = 1000;
			this.timer.Tick += new System.EventHandler(this.timer_Tick);
			((System.ComponentModel.ISupportInitialize)(this.pcBytesSent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pcBytesSentPerSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pcRequests)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pcRequestsPerSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.eventLog)).EndInit();

		}

		private void timer_Tick(object sender, EventArgs e)
		{
			pcBytesSentPerSec.RawValue = _bytesPreSec;
			pcRequestsPerSec.RawValue = _requestPerSec;

			_bytesPreSec = 0L;
			_requestPerSec = 0L;
		}

		static void Main(string[] args)
		{
			int port = 8005;
			string quoteFile = "QuoteServer.cs";

			if (args.Length >= 1)
			{
				int.TryParse(args[0], out port);

				if (args.Length == 2)
					quoteFile = args[1];
			}
			
			QuoteServer qs = new QuoteServer(quoteFile, port);

			qs.Start();

			Console.WriteLine("Services in [{0}] port with [{1}] file", port, quoteFile);
			Console.WriteLine("Hit return to Suspend or 'X' to Stop");
			string quit = Console.ReadLine();

			if (quit.ToLower() == "x")
			{
				qs.Stop();

				return;
			}

			qs.Suspend();
			Console.WriteLine("\tSuspending...");

			Console.WriteLine("Hit return to Resume");
			Console.ReadLine();
			qs.Resume();
			Console.WriteLine("\tResuming...");


			Console.WriteLine("Hit return to Stop");
			Console.ReadLine();

			qs.Stop();
		}
	}
}
