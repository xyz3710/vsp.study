﻿namespace QuoteServices
{
	partial class QuoteClient
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnGetQuote = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtHost = new System.Windows.Forms.TextBox();
			this.txtPort = new System.Windows.Forms.TextBox();
			this.lstQuote = new System.Windows.Forms.ListBox();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.statusMessage = new System.Windows.Forms.ToolStripStatusLabel();
			this.tableLayoutPanel1.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 4;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tableLayoutPanel1.Controls.Add(this.btnGetQuote, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.txtHost, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.txtPort, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.lstQuote, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(653, 414);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// btnGetQuote
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.btnGetQuote, 4);
			this.btnGetQuote.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnGetQuote.Location = new System.Drawing.Point(3, 385);
			this.btnGetQuote.Name = "btnGetQuote";
			this.btnGetQuote.Size = new System.Drawing.Size(647, 26);
			this.btnGetQuote.TabIndex = 3;
			this.btnGetQuote.Text = "Get Quote";
			this.btnGetQuote.UseVisualStyleBackColor = true;
			this.btnGetQuote.Click += new System.EventHandler(this.btnGetQuote_Click);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(124, 28);
			this.label1.TabIndex = 1;
			this.label1.Text = "Host IP Address";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(328, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(124, 28);
			this.label2.TabIndex = 1;
			this.label2.Text = "Port";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtHost
			// 
			this.txtHost.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtHost.Location = new System.Drawing.Point(133, 3);
			this.txtHost.Name = "txtHost";
			this.txtHost.Size = new System.Drawing.Size(189, 21);
			this.txtHost.TabIndex = 0;
			this.txtHost.Text = "localhost";
			// 
			// txtPort
			// 
			this.txtPort.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtPort.Location = new System.Drawing.Point(458, 3);
			this.txtPort.Name = "txtPort";
			this.txtPort.Size = new System.Drawing.Size(192, 21);
			this.txtPort.TabIndex = 1;
			this.txtPort.Text = "8005";
			// 
			// lstQuote
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.lstQuote, 4);
			this.lstQuote.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstQuote.FormattingEnabled = true;
			this.lstQuote.ItemHeight = 12;
			this.lstQuote.Location = new System.Drawing.Point(3, 31);
			this.lstQuote.Name = "lstQuote";
			this.lstQuote.Size = new System.Drawing.Size(647, 340);
			this.lstQuote.TabIndex = 4;
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusMessage});
			this.statusStrip.Location = new System.Drawing.Point(0, 414);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(653, 22);
			this.statusStrip.TabIndex = 3;
			this.statusStrip.Text = "statusStrip1";
			// 
			// statusMessage
			// 
			this.statusMessage.Name = "statusMessage";
			this.statusMessage.Size = new System.Drawing.Size(638, 17);
			this.statusMessage.Spring = true;
			this.statusMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// QuoteClient
			// 
			this.AcceptButton = this.btnGetQuote;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(653, 436);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.statusStrip);
			this.Name = "QuoteClient";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Quote Client";
			this.Load += new System.EventHandler(this.QuoteClient_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnGetQuote;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtHost;
		private System.Windows.Forms.TextBox txtPort;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel statusMessage;
		private System.Windows.Forms.ListBox lstQuote;
	}
}

