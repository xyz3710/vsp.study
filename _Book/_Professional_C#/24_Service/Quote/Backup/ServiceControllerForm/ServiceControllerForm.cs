﻿/**********************************************************************************************************************/
/*	Domain		:	ServiceControllerForm.ServiceControllerForm
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 26일 목요일 오전 10:51
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	이벤트 로그에 아래와 같은 메세지가 나올 경우
 *						이벤트 ID( 0 )(원본 ( Test )에 있음)에 대한 설명을 찾을 수 없습니다. 로컬 컴퓨터에 원격 컴퓨터에서 보낸 메시지를 표시하기 위해 필요한 레지스트리 정보 또는 메시지 DLL 파일이 없을 수 있습니다. 이 설명을 검색하는 데 /AUXSOURCE= 플래그를 사용할 수 있습니다. 자세한 정보는 도움말 및 지원을 참조하십시오. 다음 정보는 이벤트의 일부입니다
 *					HKLM\SYSTEM\CUrrentControlSet\Services\EventLog\Applicatoin\[Source]에 Source에 대한 Key가 없을 경우이다
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Diagnostics;

namespace ServiceControllerForm
{
	public partial class ServiceControllerForm : Form
	{
		private ServiceController[] _services;

		public ServiceControllerForm()
		{
			InitializeComponent();
		}

		private void ServiceControllerForm_Shown(object sender, EventArgs e)
		{
			RefreshServiceList();
		}

		protected void RefreshServiceList()
		{
			_services = ServiceController.GetServices();

			lstServices.DataSource = _services;
			lstServices.DisplayMember = "DisplayName";
		}
		
		protected string GetServiceTypeName(ServiceType type)
		{
			string serviceType = string.Empty;

			if ((type & ServiceType.InteractiveProcess) != 0)
			{
				serviceType = "Interactive ";

				type -= ServiceType.InteractiveProcess;
			}

			switch (type)
			{
				case ServiceType.Adapter:
					serviceType += "Adapter";

					break;
				case ServiceType.FileSystemDriver:
				case ServiceType.KernelDriver:
				case ServiceType.RecognizerDriver:
					serviceType += "Driver";

					break;
				case ServiceType.Win32OwnProcess:
					serviceType += "Win32 Service Process";

					break;
				case ServiceType.Win32ShareProcess:
					serviceType += "Win32 Shared Process";

					break;
				default:
					serviceType += "unknown type " + type.ToString();

					break;
			}

			return serviceType;
		}
	
		protected void SetServiceStatus(ServiceController controller)
		{
			btnStart.Enabled = true;
			btnStop.Enabled = true;
			btnPause.Enabled = true;
			btnContinue.Enabled = true;

			if (controller.CanPauseAndContinue == false)
			{
				btnPause.Enabled = false;
				btnContinue.Enabled = false;
			}

			if (controller.CanStop == false)
				btnStop.Enabled = false;

			ServiceControllerStatus status = controller.Status;

			switch (status)
			{
				case ServiceControllerStatus.ContinuePending:
					txtStatus.Text = "Continue Pending";
					btnContinue.Enabled = false;

					break;
				case ServiceControllerStatus.Paused:
					txtStatus.Text = "Paused";
					btnPause.Enabled = false;
					btnStart.Enabled = false;

					break;
				case ServiceControllerStatus.PausePending:
					txtStatus.Text = "Pause Pending";
					btnPause.Enabled = false;
					btnStart.Enabled = false;

					break;
				case ServiceControllerStatus.Running:
					txtStatus.Text = "Running";
					btnStart.Enabled = false;
					btnContinue.Enabled = false;

					break;
				case ServiceControllerStatus.StartPending:
					txtStatus.Text = "Start Pending";
					btnStart.Enabled = false;

					break;
				case ServiceControllerStatus.Stopped:
					txtStatus.Text = "Stopped";
					btnStop.Enabled = false;

					break;
				case ServiceControllerStatus.StopPending:
					txtStatus.Text = "Stop Pending";
					btnStop.Enabled = false;

					break;
				default:
					txtStatus.Text = "Unknown status";

					break;
			}
		}

		private void lstServices_SelectedIndexChanged(object sender, EventArgs e)
		{
			ServiceController controller = lstServices.SelectedItem as ServiceController;

			if (controller != null)
			{
				txtDisplayName.Text = controller.DisplayName;
				txtServiceType.Text = GetServiceTypeName(controller.ServiceType);
				txtServiceName.Text = controller.ServiceName;

				SetServiceStatus(controller);
			}
		}

		private void btnCommand_Click(object sender, EventArgs e)
		{
			ServiceController controller = lstServices.SelectedItem as ServiceController;

			if (controller != null)
			{
				Cursor saveCursor = Cursor.Current;

				try
				{
					Cursor.Current = Cursors.WaitCursor;
					if (sender == btnStart)
					{
						controller.Start();
						controller.WaitForStatus(ServiceControllerStatus.Running);
					}
					else if (sender == btnStop)
					{
						controller.Stop();
						controller.WaitForStatus(ServiceControllerStatus.Stopped);
					}
					else if (sender == btnPause)
					{
						controller.Pause();
						controller.WaitForStatus(ServiceControllerStatus.Paused);
					}
					else if (sender == btnContinue)
					{
						controller.Continue();
						controller.WaitForStatus(ServiceControllerStatus.Running);
					}

					RefreshClick();
				}
				finally
				{
					Cursor.Current = saveCursor;
				}
			}            
		}

		private void RefreshClick()
		{
			int index = lstServices.SelectedIndex;

			RefreshServiceList();

			lstServices.SelectedIndex = index;
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			RefreshClick();
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}