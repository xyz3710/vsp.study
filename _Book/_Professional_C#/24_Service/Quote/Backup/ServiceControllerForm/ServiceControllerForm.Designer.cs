﻿namespace ServiceControllerForm
{
	partial class ServiceControllerForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.txtDisplayName = new System.Windows.Forms.TextBox();
			this.txtServiceName = new System.Windows.Forms.TextBox();
			this.txtServiceType = new System.Windows.Forms.TextBox();
			this.txtStatus = new System.Windows.Forms.TextBox();
			this.btnStart = new System.Windows.Forms.Button();
			this.btnStop = new System.Windows.Forms.Button();
			this.btnPause = new System.Windows.Forms.Button();
			this.btnContinue = new System.Windows.Forms.Button();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.btnExit = new System.Windows.Forms.Button();
			this.lstServices = new System.Windows.Forms.ListBox();
			this.serviceController1 = new System.ServiceProcess.ServiceController();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.lstServices, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(20);
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(794, 479);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.txtDisplayName, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.txtServiceName, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.txtServiceType, 0, 4);
			this.tableLayoutPanel2.Controls.Add(this.txtStatus, 0, 6);
			this.tableLayoutPanel2.Controls.Add(this.btnStart, 0, 8);
			this.tableLayoutPanel2.Controls.Add(this.btnStop, 1, 8);
			this.tableLayoutPanel2.Controls.Add(this.btnPause, 0, 10);
			this.tableLayoutPanel2.Controls.Add(this.btnContinue, 1, 10);
			this.tableLayoutPanel2.Controls.Add(this.btnRefresh, 0, 12);
			this.tableLayoutPanel2.Controls.Add(this.btnExit, 1, 12);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(410, 23);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 13;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(361, 433);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// txtDisplayName
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.txtDisplayName, 2);
			this.txtDisplayName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtDisplayName.Location = new System.Drawing.Point(3, 3);
			this.txtDisplayName.Name = "txtDisplayName";
			this.txtDisplayName.Size = new System.Drawing.Size(355, 21);
			this.txtDisplayName.TabIndex = 0;
			// 
			// txtServiceName
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.txtServiceName, 2);
			this.txtServiceName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtServiceName.Location = new System.Drawing.Point(3, 51);
			this.txtServiceName.Name = "txtServiceName";
			this.txtServiceName.Size = new System.Drawing.Size(355, 21);
			this.txtServiceName.TabIndex = 0;
			// 
			// txtServiceType
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.txtServiceType, 2);
			this.txtServiceType.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtServiceType.Location = new System.Drawing.Point(3, 99);
			this.txtServiceType.Name = "txtServiceType";
			this.txtServiceType.Size = new System.Drawing.Size(355, 21);
			this.txtServiceType.TabIndex = 0;
			// 
			// txtStatus
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.txtStatus, 2);
			this.txtStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtStatus.Location = new System.Drawing.Point(3, 147);
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Size = new System.Drawing.Size(355, 21);
			this.txtStatus.TabIndex = 0;
			// 
			// btnStart
			// 
			this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStart.Location = new System.Drawing.Point(5, 214);
			this.btnStart.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.btnStart.Name = "btnStart";
			this.btnStart.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
			this.btnStart.Size = new System.Drawing.Size(170, 26);
			this.btnStart.TabIndex = 1;
			this.btnStart.Text = "&Start";
			this.btnStart.UseVisualStyleBackColor = true;
			this.btnStart.Click += new System.EventHandler(this.btnCommand_Click);
			// 
			// btnStop
			// 
			this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStop.Location = new System.Drawing.Point(185, 214);
			this.btnStop.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.btnStop.Name = "btnStop";
			this.btnStop.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
			this.btnStop.Size = new System.Drawing.Size(171, 26);
			this.btnStop.TabIndex = 1;
			this.btnStop.Text = "S&top";
			this.btnStop.UseVisualStyleBackColor = true;
			this.btnStop.Click += new System.EventHandler(this.btnCommand_Click);
			// 
			// btnPause
			// 
			this.btnPause.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPause.Location = new System.Drawing.Point(5, 285);
			this.btnPause.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.btnPause.Name = "btnPause";
			this.btnPause.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
			this.btnPause.Size = new System.Drawing.Size(170, 26);
			this.btnPause.TabIndex = 1;
			this.btnPause.Text = "&Pause";
			this.btnPause.UseVisualStyleBackColor = true;
			this.btnPause.Click += new System.EventHandler(this.btnCommand_Click);
			// 
			// btnContinue
			// 
			this.btnContinue.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnContinue.Location = new System.Drawing.Point(185, 285);
			this.btnContinue.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.btnContinue.Name = "btnContinue";
			this.btnContinue.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
			this.btnContinue.Size = new System.Drawing.Size(171, 26);
			this.btnContinue.TabIndex = 1;
			this.btnContinue.Text = "&Continue";
			this.btnContinue.UseVisualStyleBackColor = true;
			this.btnContinue.Click += new System.EventHandler(this.btnCommand_Click);
			// 
			// btnRefresh
			// 
			this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnRefresh.Location = new System.Drawing.Point(5, 356);
			this.btnRefresh.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
			this.btnRefresh.Size = new System.Drawing.Size(170, 74);
			this.btnRefresh.TabIndex = 1;
			this.btnRefresh.Text = "&Refresh";
			this.btnRefresh.UseVisualStyleBackColor = true;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// btnExit
			// 
			this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnExit.Location = new System.Drawing.Point(185, 356);
			this.btnExit.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
			this.btnExit.Name = "btnExit";
			this.btnExit.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
			this.btnExit.Size = new System.Drawing.Size(171, 74);
			this.btnExit.TabIndex = 1;
			this.btnExit.Text = "E&xit";
			this.btnExit.UseVisualStyleBackColor = true;
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// lstServices
			// 
			this.lstServices.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstServices.FormattingEnabled = true;
			this.lstServices.ItemHeight = 12;
			this.lstServices.Location = new System.Drawing.Point(23, 23);
			this.lstServices.Name = "lstServices";
			this.lstServices.Size = new System.Drawing.Size(361, 424);
			this.lstServices.TabIndex = 1;
			this.lstServices.SelectedIndexChanged += new System.EventHandler(this.lstServices_SelectedIndexChanged);
			// 
			// serviceController1
			// 
			this.serviceController1.MachineName = "KimKiWon";
			this.serviceController1.ServiceName = "QuoteService";
			// 
			// ServiceControllerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(794, 479);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "ServiceControllerForm";
			this.Text = "Service Controller";
			this.Shown += new System.EventHandler(this.ServiceControllerForm_Shown);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TextBox txtDisplayName;
		private System.Windows.Forms.TextBox txtServiceName;
		private System.Windows.Forms.TextBox txtServiceType;
		private System.Windows.Forms.TextBox txtStatus;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Button btnPause;
		private System.Windows.Forms.Button btnContinue;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.ListBox lstServices;
		private System.ServiceProcess.ServiceController serviceController1;
	}
}

