//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AddIn.rc
//
#define IDS_PROJNAME                    100
#define IDR_ADDIN                       101
#define IDR_CONNECT                     102
#define IDD_KEYMAPDLG                   103
#define IDC_COPY                        201
#define IDI_ICON1                       201
#define IDC_FILTERCB                    202
#define IDC_KEYSLIST                    203

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         204
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
