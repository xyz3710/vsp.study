//Copyright (c) Microsoft Corporation.  All rights reserved.

// KeyMapDlg.h : Declaration of the CKeyMapDlg

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>


#define MAX_SCOPES 50

class CSortParam
{
public:
	int m_nItem;
	HWND m_hWnd;
};

// CKeyMapDlg

class CKeyMapDlg : 
	public CAxDialogImpl<CKeyMapDlg>
{
public:
	CKeyMapDlg()
	{
		nCurrentScope = 0;
		for (int i = 0 ; i < MAX_SCOPES ; i++)
			m_pszScopes[i] = NULL;
	}

	~CKeyMapDlg()
	{
		for (int i = 0 ; i < nCurrentScope ; i++)
		{
			if(m_pszScopes[i])
				delete m_pszScopes[i];
		}
	}

	enum { IDD = IDD_KEYMAPDLG };

	HRESULT GenerateListOfScopes();
	HRESULT FillWithBoundCommands();
	HRESULT FillWithScope(WCHAR *pszScope);
	HRESULT FillWithUnboundCommands();
	HRESULT FillWithAllCommands();

	CComPtr<EnvDTE::_DTE> m_pDTE;

	void SetDTE(EnvDTE::_DTE *pDTE)
	{
		m_pDTE = pDTE;
	}

	int nCurrentScope;
	WCHAR *m_pszScopes[MAX_SCOPES]; //Assume we will not have more than 50 scopes.

BEGIN_MSG_MAP(CKeyMapDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	CHAIN_MSG_MAP(CAxDialogImpl<CKeyMapDlg>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};


