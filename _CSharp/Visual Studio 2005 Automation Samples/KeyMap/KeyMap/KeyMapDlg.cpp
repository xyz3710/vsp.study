//Copyright (c) Microsoft Corporation.  All rights reserved.

// KeyMapDlg.cpp : Implementation of CKeyMapDlg

#include "stdafx.h"
#include "KeyMapDlg.h"
#include "commctrl.h"

#define CBI_BOUNDCOMMANDS 0
#define CBI_ALLCOMMANDS 1
#define CBI_UNBOUNDCOMMANDS 2
#define CBI_STARTOFSCOPES 3

// CKeyMapDlg

void ShowDialog(IUnknown *pUnk)
{
	CKeyMapDlg KeyMapDlg;
	CComQIPtr<EnvDTE::_DTE> pDTE(pUnk);
	KeyMapDlg.SetDTE(pDTE);
	KeyMapDlg.DoModal();
}

class CWaitCursor
{
public:
	CWaitCursor()
	{
		SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_WAIT)));
	}
	~CWaitCursor()
	{
		SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)));
	}
};

class CStatBarProgress
{
public:
	CComPtr<EnvDTE::StatusBar> pStatBar;

	CStatBarProgress(EnvDTE::_DTE *pDTE)
	{
		HRESULT hr;
		IfFailGo(pDTE->get_StatusBar(&pStatBar));
		pStatBar->Progress(VARIANT_TRUE, CComBSTR(L"Generating List..."), 0, 1);
		pStatBar->put_Text(CComBSTR(L"Generating List..."));
Error:
		hr;
	}

	~CStatBarProgress()
	{
		HRESULT hr;
		IfFailGo(pStatBar->Progress(VARIANT_FALSE, CComBSTR(L"Generating List..."), 0, 1));
		IfFailGo(pStatBar->Clear());
Error:
		hr;
	}

	HRESULT SetStatBarProgress(long lDone, long lNumberItems)
	{
		HRESULT hr;
		IfFailGo(pStatBar->Progress(VARIANT_TRUE, CComBSTR(L"Generating List..."), lDone, lNumberItems));
		pStatBar->put_Text(CComBSTR(L"Generating List..."));
Error:
		return hr;
	}
};

HRESULT CKeyMapDlg::GenerateListOfScopes()
{
	USES_CONVERSION;
	HRESULT hr = S_OK;
	long lCommandCount, i;
	CComPtr<EnvDTE::Commands> pCommands;

	CWaitCursor WaitCursor;
	CStatBarProgress StatBarProgress(m_pDTE);

	ListView_DeleteAllItems(GetDlgItem(IDC_KEYSLIST));
	IfFailGo(m_pDTE->get_Commands(&pCommands));
	IfFailGo(pCommands->get_Count(&lCommandCount));
	for(i = 0 ; i < lCommandCount ; i++)
	{
		CComPtr<EnvDTE::Command> pCommand;
		CComVariant varBindings;
		CComBSTR bstrName;
		StatBarProgress.SetStatBarProgress(i+1, lCommandCount+1);
		IfFailGo(pCommands->Item(CComVariant(i+1), 0, &pCommand));
		IfFailGo(pCommand->get_Name(&bstrName));
		if(bstrName.Length() > 0)
		{
			IfFailGo(pCommand->get_Bindings(&varBindings));
			for(ULONG j = 0 ; j < varBindings.parray->rgsabound[0].cElements ; j++)
			{
				bool fFound = false;
				long lFirstIndex = j;
				CComVariant varBinding;
				WCHAR *pszString;
				WCHAR *pszTemp;

				IfFailGo(SafeArrayGetElement(varBindings.parray, &lFirstIndex, (LPVOID)&varBinding));
				pszString = new WCHAR[SysStringLen(varBinding.bstrVal) + 1];
				wcscpy(pszString, varBinding.bstrVal);
				pszTemp = wcschr(pszString, ':');
				*pszTemp = 0;

				//loop through the known scopes, and see if it exists...
				for (int k = 0 ; k < nCurrentScope ; k++)
				{
					if(!wcscmp(pszString, m_pszScopes[k]))
					{
						fFound = true;
						break;
					}
				}
				if(fFound == false)
				{
					m_pszScopes[nCurrentScope] = new WCHAR[wcslen(pszString) + 1];
					wcscpy(m_pszScopes[nCurrentScope], pszString);
					SendDlgItemMessage(IDC_FILTERCB, CB_INSERTSTRING, CBI_STARTOFSCOPES+nCurrentScope, (LPARAM)W2T(pszString));
					nCurrentScope++;
				}
			} 
		}
	}
Error:
	return hr;
}

HRESULT CKeyMapDlg::FillWithBoundCommands()
{
	USES_CONVERSION;
	HRESULT hr = S_OK;
	long lCommandCount, i;
	LVITEM lvi;
	CComPtr<EnvDTE::Commands> pCommands;

	CWaitCursor WaitCursor;
	CStatBarProgress StatBarProgress(m_pDTE);

	lvi.mask = LVIF_TEXT;
	ListView_DeleteAllItems(GetDlgItem(IDC_KEYSLIST));
	IfFailGo(m_pDTE->get_Commands(&pCommands));

	IfFailGo(pCommands->get_Count(&lCommandCount));

	for(i = 0 ; i < lCommandCount ; i++)
	{
		CComPtr<EnvDTE::Command> pCommand;
		CComVariant varBindings;
		CComBSTR bstrName;
		StatBarProgress.SetStatBarProgress(i+1, lCommandCount+1);
		IfFailGo(pCommands->Item(CComVariant(i+1), 0, &pCommand));
		IfFailGo(pCommand->get_Name(&bstrName));
		if(bstrName.Length() > 0)
		{
			IfFailGo(pCommand->get_Bindings(&varBindings));
			for(ULONG j = 0 ; j < varBindings.parray->rgsabound[0].cElements ; j++)
			{
				long lFirstIndex = j;
				CComVariant varBinding;
				WCHAR *pszString;
				WCHAR *pszTemp, *pszTemp2;
				lvi.mask = LVIF_TEXT;
				lvi.iItem	 = ListView_GetItemCount(GetDlgItem(IDC_KEYSLIST));
				lvi.iSubItem = 0;
				lvi.pszText = W2T(bstrName);
				int nItem = ListView_InsertItem(GetDlgItem(IDC_KEYSLIST), &lvi);

				IfFailGo(SafeArrayGetElement(varBindings.parray, &lFirstIndex, (LPVOID)&varBinding));
				pszString = new WCHAR[SysStringLen(varBinding.bstrVal) + 1];
				wcscpy(pszString, varBinding.bstrVal);
				pszTemp2 = pszTemp = wcschr(pszString, ':');
				pszTemp2 = CharNextW(pszTemp2);
				pszTemp2 = CharNextW(pszTemp2);
				*pszTemp = 0;

				lvi.iSubItem = 1;
				lvi.iItem = nItem;
				lvi.pszText = W2T(pszString);
				ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

				lvi.iSubItem = 2;
				lvi.iItem = nItem;
				lvi.pszText = W2T(pszTemp2);
				ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

#ifdef _DEBUG
				{
					CComBSTR bstrGUID;
					long lID;
					TCHAR szID[10];
					pCommand->get_Guid(&bstrGUID);
					pCommand->get_ID(&lID);
					wsprintf(szID, _T("%d"), lID);

					lvi.iSubItem = 3;
					lvi.iItem = nItem;
					lvi.pszText = W2T(bstrGUID);
					ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

					lvi.iSubItem = 4;
					lvi.iItem = nItem;
					lvi.pszText = szID;
					ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);
				}
#endif

				delete []pszString;
			} 
		}
	}
Error:
	return hr;
}

HRESULT CKeyMapDlg::FillWithScope(WCHAR *pszScope)
{
	USES_CONVERSION;
	HRESULT hr = S_OK;
	long lCommandCount, i;
	LVITEM lvi;
	CComPtr<EnvDTE::Commands> pCommands;

	CWaitCursor WaitCursor;
	CStatBarProgress StatBarProgress(m_pDTE);

	lvi.mask = LVIF_TEXT;
	ListView_DeleteAllItems(GetDlgItem(IDC_KEYSLIST));
	IfFailGo(m_pDTE->get_Commands(&pCommands));
	IfFailGo(pCommands->get_Count(&lCommandCount));
	for(i = 0 ; i < lCommandCount ; i++)
	{
		CComPtr<EnvDTE::Command> pCommand;
		CComVariant varBindings;
		CComBSTR bstrName;
		StatBarProgress.SetStatBarProgress(i+1, lCommandCount+1);
		IfFailGo(pCommands->Item(CComVariant(i+1), 0, &pCommand));
		IfFailGo(pCommand->get_Name(&bstrName));
		if(bstrName.Length() > 0)
		{
			IfFailGo(pCommand->get_Bindings(&varBindings));
			for(ULONG j = 0 ; j < varBindings.parray->rgsabound[0].cElements ; j++)
			{
				long lFirstIndex = j;
				CComVariant varBinding;
				WCHAR *pszString;
				WCHAR *pszTemp, *pszTemp2;


				IfFailGo(SafeArrayGetElement(varBindings.parray, &lFirstIndex, (LPVOID)&varBinding));
				pszString = new WCHAR[SysStringLen(varBinding.bstrVal) + 1];
				wcscpy(pszString, varBinding.bstrVal);
				pszTemp2 = pszTemp = wcschr(pszString, ':');
				pszTemp2 = CharNextW(pszTemp2);
				pszTemp2 = CharNextW(pszTemp2);
				*pszTemp = 0;

				if(!wcscmp(pszString, pszScope))
				{        
					lvi.mask = LVIF_TEXT;
					lvi.iItem	 = ListView_GetItemCount(GetDlgItem(IDC_KEYSLIST));
					lvi.iSubItem = 0;
					lvi.pszText = W2T(bstrName);
					int nItem = ListView_InsertItem(GetDlgItem(IDC_KEYSLIST), &lvi);

					lvi.iSubItem = 1;
					lvi.iItem = nItem;
					lvi.pszText = W2T(pszString);
					ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

					lvi.iSubItem = 2;
					lvi.iItem = nItem;
					lvi.pszText = W2T(pszTemp2);
					ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);
#ifdef _DEBUG
					{
						CComBSTR bstrGUID;
						long lID;
						TCHAR szID[10];
						pCommand->get_Guid(&bstrGUID);
						pCommand->get_ID(&lID);
						wsprintf(szID, _T("%d"), lID);

						lvi.iSubItem = 3;
						lvi.iItem = nItem;
						lvi.pszText = W2T(bstrGUID);
						ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

						lvi.iSubItem = 4;
						lvi.iItem = nItem;
						lvi.pszText = szID;
						ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);
					}
#endif
				}

				delete []pszString;
			} 
		}
	}
Error:
	return hr;
}


HRESULT CKeyMapDlg::FillWithUnboundCommands()
{
	USES_CONVERSION;
	HRESULT hr = S_OK;
	long lCommandCount, i;
	LVITEM lvi;
	CComPtr<EnvDTE::Commands> pCommands;

	lvi.mask = LVIF_TEXT;

	CWaitCursor WaitCursor;
	CStatBarProgress StatBarProgress(m_pDTE);

	ListView_DeleteAllItems(GetDlgItem(IDC_KEYSLIST));
	IfFailGo(m_pDTE->get_Commands(&pCommands));
	IfFailGo(pCommands->get_Count(&lCommandCount));
	for(i = 0 ; i < lCommandCount ; i++)
	{
		CComPtr<EnvDTE::Command> pCommand;
		CComVariant varBindings;
		CComBSTR bstrName;
		StatBarProgress.SetStatBarProgress(i+1, lCommandCount+1);
		IfFailGo(pCommands->Item(CComVariant(i+1), 0, &pCommand));
		IfFailGo(pCommand->get_Name(&bstrName));
		if(bstrName.Length() > 0)
		{
			IfFailGo(pCommand->get_Bindings(&varBindings));
			if(varBindings.parray->rgsabound[0].cElements == 0)
			{
				lvi.mask = LVIF_TEXT;
				lvi.iItem	 = ListView_GetItemCount(GetDlgItem(IDC_KEYSLIST));
				lvi.iSubItem = 0;
				lvi.pszText = W2T(bstrName);
				int nItem = ListView_InsertItem(GetDlgItem(IDC_KEYSLIST), &lvi);
			}
		}
	}
Error:
	return hr;
}

HRESULT CKeyMapDlg::FillWithAllCommands()
{
	USES_CONVERSION;
	HRESULT hr = S_OK;
	long lCommandCount, i;
	LVITEM lvi;
	CComPtr<EnvDTE::Commands> pCommands;

	lvi.mask = LVIF_TEXT;

	CWaitCursor WaitCursor;
	CStatBarProgress StatBarProgress(m_pDTE);

	ListView_DeleteAllItems(GetDlgItem(IDC_KEYSLIST));
	IfFailGo(m_pDTE->get_Commands(&pCommands));
	IfFailGo(pCommands->get_Count(&lCommandCount));
	for(i = 0 ; i < lCommandCount ; i++)
	{
		CComPtr<EnvDTE::Command> pCommand;
		CComVariant varBindings;
		CComBSTR bstrName;
		StatBarProgress.SetStatBarProgress(i+1, lCommandCount+1);
		IfFailGo(pCommands->Item(CComVariant(i+1), 0, &pCommand));
		IfFailGo(pCommand->get_Name(&bstrName));
		if(bstrName.Length() > 0)
		{
			IfFailGo(pCommand->get_Bindings(&varBindings));
			if(varBindings.parray->rgsabound[0].cElements > 0)
			{
				for(ULONG j = 0 ; j < varBindings.parray->rgsabound[0].cElements ; j++)
				{
					long lFirstIndex = j;
					CComVariant varBinding;
					WCHAR *pszString;
					WCHAR *pszTemp, *pszTemp2;
					lvi.mask = LVIF_TEXT;
					lvi.iItem	 = ListView_GetItemCount(GetDlgItem(IDC_KEYSLIST));
					lvi.iSubItem = 0;
					lvi.pszText = W2T(bstrName);
					int nItem = ListView_InsertItem(GetDlgItem(IDC_KEYSLIST), &lvi);

					IfFailGo(SafeArrayGetElement(varBindings.parray, &lFirstIndex, (LPVOID)&varBinding));
					pszString = new WCHAR[SysStringLen(varBinding.bstrVal) + 1];
					wcscpy(pszString, varBinding.bstrVal);
					pszTemp2 = pszTemp = wcschr(pszString, ':');
					pszTemp2 = CharNextW(pszTemp2);
					pszTemp2 = CharNextW(pszTemp2);
					*pszTemp = 0;

					lvi.iSubItem = 1;
					lvi.iItem = nItem;
					lvi.pszText = W2T(pszString);
					ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

					lvi.iSubItem = 2;
					lvi.iItem = nItem;
					lvi.pszText = W2T(pszTemp2);
					ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi); 
#ifdef _DEBUG
					{
						CComBSTR bstrGUID;
						long lID;
						TCHAR szID[10];
						pCommand->get_Guid(&bstrGUID);
						pCommand->get_ID(&lID);
						wsprintf(szID, _T("%d"), lID);

						lvi.iSubItem = 3;
						lvi.iItem = nItem;
						lvi.pszText = W2T(bstrGUID);
						ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);

						lvi.iSubItem = 4;
						lvi.iItem = nItem;
						lvi.pszText = szID;
						ListView_SetItem(GetDlgItem(IDC_KEYSLIST), &lvi);
					}
#endif

					delete []pszString;
				} 
			}
			else  //No bindings for this item, just insert the command name...
			{
				lvi.mask = LVIF_TEXT;
				lvi.iItem	 = ListView_GetItemCount(GetDlgItem(IDC_KEYSLIST));
				lvi.iSubItem = 0;
				lvi.pszText = W2T(bstrName);
				int nItem = ListView_InsertItem(GetDlgItem(IDC_KEYSLIST), &lvi);
			}
		}
	}
Error:
	return hr;
}

LRESULT CKeyMapDlg::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CAxDialogImpl<CKeyMapDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);

	long lCountColumns = 0;
	LVCOLUMN lvc;

	DWORD dwStyleEx = (DWORD)SendMessage(GetDlgItem(IDC_KEYSLIST), LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0);
	dwStyleEx |= LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT;
	SendMessage(GetDlgItem(IDC_KEYSLIST), LVM_SETEXTENDEDLISTVIEWSTYLE, 0, dwStyleEx);

	SendDlgItemMessage(IDC_FILTERCB, CB_INSERTSTRING, CBI_BOUNDCOMMANDS, (LPARAM)_T("Bound Commands"));
	SendDlgItemMessage(IDC_FILTERCB, CB_INSERTSTRING, CBI_ALLCOMMANDS, (LPARAM)_T("All Commands"));
	SendDlgItemMessage(IDC_FILTERCB, CB_INSERTSTRING, CBI_UNBOUNDCOMMANDS, (LPARAM)_T("Unbound Commands"));

	GenerateListOfScopes();

	SendDlgItemMessage(IDC_FILTERCB, CB_SETCURSEL, 0, 0);

	SetIcon(LoadIcon(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDI_ICON1)));

	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = _T("Command Name");
	lvc.cx = 150;
	ListView_InsertColumn(GetDlgItem(IDC_KEYSLIST), 0, &lvc);

	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = _T("Scope");
	lvc.cx = 150;
	ListView_InsertColumn(GetDlgItem(IDC_KEYSLIST), 1, &lvc);

	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = _T("Key Binding");
	lvc.cx = 150;
	ListView_InsertColumn(GetDlgItem(IDC_KEYSLIST), 2, &lvc);

#ifdef _DEBUG
	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = _T("GUID");
	lvc.cx = 150;
	ListView_InsertColumn(GetDlgItem(IDC_KEYSLIST), 3, &lvc);

	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = _T("ID");
	lvc.cx = 150;
	ListView_InsertColumn(GetDlgItem(IDC_KEYSLIST), 4, &lvc);
#endif

	FillWithBoundCommands();

	bHandled = TRUE;
	return 1;  // Let the system set the focus
}