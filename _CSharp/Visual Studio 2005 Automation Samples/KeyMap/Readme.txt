Copyright (c) Microsoft Corporation.  All rights reserved.

This add-in puts a Keybindings command in the Help menu that is similar to the Visual C++ 6.0 Keyboard Map Help command.

Once you load this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin KeyMap.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)
