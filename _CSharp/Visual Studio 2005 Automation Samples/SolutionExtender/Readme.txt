Copyright (c) Microsoft Corporation.  All rights reserved.

This sample demonstrates the use of Automation Extenders to "extend" existing Visual Studio objects with new properties and methods. You can extend property grid display objects as well as automation objects, so that more methods and properties are available to the caller. This sample extends both the Solution property grid display object as well as the DTE.Solution automation object.

To run this sample:

1. Build this solution. 
2. Run Visual Studio 2005 and open or create a new solution. 
3. Select the Solution node in the Solution Explorer and view properties on it.