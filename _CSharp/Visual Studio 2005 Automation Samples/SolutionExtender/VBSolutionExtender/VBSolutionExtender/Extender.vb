'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports EnvDTE

'The interface for the Extender object. It implements three properties:
' [get/set]     Property Notes As String
' [get]         Property Created As Date
' [get]         Property LastAccessed As Date
' [get]         Property LastModified As Date
<System.Runtime.InteropServices.InterfaceType(System.Runtime.InteropServices.ComInterfaceType.InterfaceIsDual)> _
Public Interface ISolutionExtender

    Property Notes() As String
    ReadOnly Property Created() As Date
    ReadOnly Property LastAccessed() As Date
    ReadOnly Property LastModified() As Date

End Interface

'This is the Extender Object itself. 
<System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.None)> _
Public Class SolnExtender
    Implements ISolutionExtender

    'Notes property stores its value in the Solution.Globals object and hence 
    'is persisted with the solution.
    Property Notes() As String Implements ISolutionExtender.Notes

        Get
            Dim glob As EnvDTE.Globals
            glob = Sol.Globals()
            If Not glob.VariableExists(NotesProperty) Then
                glob.VariableValue(NotesProperty) = ""
                glob.VariablePersists(NotesProperty) = True
            End If
            Notes = glob.VariableValue(NotesProperty).ToString()
        End Get

        Set(ByVal Value As String)
            Dim glob As EnvDTE.Globals
            glob = Sol.Globals()
            glob.VariableValue(NotesProperty) = Value
            glob.VariablePersists(NotesProperty) = True
        End Set

    End Property

    'Created property indicates when the Solution file was created.
    ReadOnly Property Created() As Date Implements ISolutionExtender.Created
        Get
            'Simply get the file creation date/time.
            Created = System.IO.File.GetCreationTime(Sol.FullName)
        End Get
    End Property

    'LastAccessed property indicates when the Solution file was last accessed.
    ReadOnly Property LastAccessed() As Date Implements ISolutionExtender.LastAccessed
        Get
            'Simply get the last file access date/time.
            LastAccessed = System.IO.File.GetLastAccessTime(Sol.FullName)
        End Get
    End Property

    'LastModified property indicates when the Solution file was saved.
    ReadOnly Property LastModified() As Date Implements ISolutionExtender.LastModified
        Get
            'Simply get the last file write date/time.
            LastModified = System.IO.File.GetLastWriteTime(Sol.FullName)
        End Get
    End Property

    'Initializes the members of the SolnExtender class.
    Public Sub Init(ByVal sln As EnvDTE.Solution, ByVal ExtenderCookie As Integer, ByVal ExtenderSite As EnvDTE.IExtenderSite)
        Site = ExtenderSite
        Cookie = ExtenderCookie
        Sol = sln
    End Sub

    'Tells the Site object we are going away.
    Protected Overrides Sub Finalize()
        'We ignore any failure codes the Site may return. For instance, 
        'since this object is GC'ed, the Site may have already let go of its Cookie.
        On Error Resume Next

        If Not (Site Is Nothing) Then
            Site.NotifyDelete(Cookie)
        End If
    End Sub

    ' Data members of the class.
    Private Cookie As Integer
    Private Site As EnvDTE.IExtenderSite
    Private Sol As EnvDTE.Solution
    Private NotesProperty As String = "ThisSolutionNotes"

End Class
