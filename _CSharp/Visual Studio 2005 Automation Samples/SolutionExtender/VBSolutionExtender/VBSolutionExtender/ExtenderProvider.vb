'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports EnvDTE
Imports System.Runtime.InteropServices

'Extender Provider object. This is the "class factory" for the Extender and is registered
'under the CATIDs of the solution objects (vsCATIDSolution & vsCATIDSolutionBrowseObject)
'it is extending. See SolutionExtender.reg for the registration entries.
<Guid("8DBF8D1E-AC54-4a06-B92C-BA728AE9ACE5"), ProgId("SolutionExtender.VBSolnExtProvider")> _
Public Class SolnExtenderProvider

    Implements EnvDTE.IExtenderProvider

    'IExtenderProvider implementation

    'Implementation of IExtenderProvider::CanExtend.
    Public Function CanExtend(ByVal ExtenderCATID As String, ByVal ExtenderName As String, ByVal ExtendeeObject As Object) As Boolean Implements EnvDTE.IExtenderProvider.CanExtend

        'Extends the solution automation and browseobject unconditionally. 
        'The extension name is given by SlnAutExtension ("SolutionMisc").
        If (UCase(ExtenderCATID) = UCase(EnvDTE.Constants.vsCATIDSolutionBrowseObject) Or _
            UCase(ExtenderCATID) = UCase(EnvDTE.Constants.vsCATIDSolution)) And _
            (UCase(ExtenderName) = UCase(SlnAutExtension)) Then
            CanExtend = True
        Else
            CanExtend = False
        End If

    End Function

    'Implementation of IExtenderProvider::GetExtender.
    Public Function GetExtender(ByVal ExtenderCATID As String, ByVal ExtenderName As String, ByVal ExtendeeObject As Object, ByVal ExtenderSite As EnvDTE.IExtenderSite, ByVal Cookie As Integer) As Object Implements EnvDTE.IExtenderProvider.GetExtender

        GetExtender = Nothing 'In case of failure.

        If CanExtend(ExtenderCATID, ExtenderName, ExtendeeObject) Then
            'Note: More complicated implementations can keep a map of Extendees and Extenders 
            'they have given out, so that if asked for again for the same Extendee Extender
            'you don't have to recreate it again. In our case, the Extenders are very
            'light-weight objects and have little direct interaction with the Extendee, so
            'we don't bother.

            Dim slnext As New SolnExtender()
            Dim sln As EnvDTE.Solution

            'Get the solution automation object.
            'We don't need to interact with the Extendee object much since we go
            'directly to the Solution automation object for our needs, but most
            'implementations would talk directly to the Extendee object to extend/shadow
            'its properties.
            If TypeOf ExtendeeObject Is EnvDTE.Solution Then
                'Extending the DTE.Solution object.
                sln = ExtendeeObject
            Else
                'Extending the Soltuion browse object (the object that's displayed
                'in the property browser when the Solution Node is selected in
                'the Solution Explorer). In this case, get the DTE.Solution object
                'directly from the object model.
                Dim root As EnvDTE.DTE
                root = ExtenderSite.GetObject()
                sln = root.Solution
            End If

            If Not (sln Is Nothing) Then
                'Init our extender.
                slnext.Init(sln, Cookie, ExtenderSite)
                GetExtender = slnext
            End If

        End If

    End Function

    'The Extension name for this Extender.
    Private SlnAutExtension As String = "SolutionMisc"

End Class
