//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using EnvDTE;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WizardContextCS
{
	/// <summary>
	/// Summary description for ContextCSWizard
	/// </summary>
	[ProgId("WizardContextCS.ContextCSWizard")]
	public class ContextCSWizard : IDTWizard
	{

		public ContextCSWizard()
		{
		}

		//If you were to do DTE.LaunchWizard for this customized wizard, the call would like two methods listed at the
		//end of this class -- launchProject() and launchItem()
		//When the LaunchWizard call is made, the following Execute function is invoked.
			
		public void Execute ( object Application , int hwndOwner , ref object[] ContextParams , ref object[] CustomParams , ref EnvDTE.wizardResult retval )
		{
			showMsg(ref ContextParams, ref CustomParams);
	
			//If you want to create project, code should be included in this function.

		}

		private void showMsg(ref object[] ContextParams, ref object[] CustomParams)
		{
			string [] temp = { EnvDTE.Constants.vsWizardAddItem, EnvDTE.Constants.vsWizardAddSubProject, EnvDTE.Constants.vsWizardNewProject };
			string conStr = ((string)ContextParams[0]).ToUpper();
			string msg;
			int i;
			if ( conStr == temp[0] )
			{
				msg = "This sample wizard is to show the context and custom parameters passed to AddItem wizard.\n";
				msg = msg + "=========================================================================\n";
				msg = msg + "The ContextParams are:\n\n";
				msg = msg + "This is vsWizardAddItem.  GUID is specified by -- ContextParams[0]\n";
				msg = msg + "Add an item to Project \"" + ContextParams[1] + "\" -- ContextParams[1]\n";
				msg = msg + "Adding item to ProjectItems collection. This collection is stored in -- ContextParams[2]\n";
				msg = msg + "The destination working project files directory is \"" + ContextParams[3] + "\" -- ContextParames[3]\n";
				msg = msg + "The new item name is \"" + ContextParams[4] + "\" -- ContextParams[4]\n";
				msg = msg + "\"" + ContextParams[5] + "\" this is the content of -- ContextParams[5]\n";
				msg = msg + "The last one in the array is in boolean type of value false -- ContextParams[6]\n\n";
				msg = msg + "-------------------------------------------------------------------------\n";

				msg = msg + "The CustomParams are: (specified in .vsz file)\n\n";
				for (i = 0; i<CustomParams.Length ; i++)
				{
					msg = msg + "CustomParams[" + i + "] is " + CustomParams[i] + " \n";
				}
				msg = msg + "=========================================================================\n";
				msg = msg + "This is only for showing values passed to a wizard when invoked AddItem. No real item is created and added.";

				MessageBox.Show(msg, "Development Environment Sample Wizard");
			}
			else if ( conStr == temp[1] )
			{
				MessageBox.Show("This is add sub project wizard.");
			}
			else if ( conStr == temp[2] )
			{
				msg = "This sample wizard is to show the context and custom parameters passed to NewProject wizard.\n";
				msg = msg + "=========================================================================\n";
				msg = msg + "The ContextParams are:\n\n";
				msg = msg + "This is vsWizardNewProject.  GUID is specified by -- ContextParams[0]\n";
				msg = msg + "The name of the project is \"" + ContextParams[1] + "\" -- ContextParams[1]\n";
				msg = msg + "The destination working directory for new project is \"" + ContextParams[2] + "\" -- ContextParames[2]\n";
				msg = msg + "The installation directory for devenv.exe is \"" + ContextParams[3] + "\" -- ContextParams[3]\n";
				if ((bool)ContextParams[4])
					msg = msg + "Create a new solution for the new project. \"True\" value is specified by -- ContextParams[4]\n";
				else
					msg = msg + "Add the new project ot the current solution. \"False\" value is specified by -- ContextParams[4]\n";
				msg = msg + "Nothing is specified by -- ContextParams[5]\n";
				msg = msg + "The last one in the array is in boolean type of value false -- ContextParams[6]\n\n";
				msg = msg + "-------------------------------------------------------------------------\n";

				msg = msg + "The CustomParams are: (specified in .vsz file)\n\n";
				for (i = 0; i<CustomParams.Length ; i++)
				{
					msg = msg + "CustomParams[" + i + "] is " + CustomParams[i] + " \n";
				}

				msg = msg + "=========================================================================\n";
				msg = msg + "This is only for showing values passed to a wizard when invoked NewProject. No real project is created and added.";

				MessageBox.Show(msg, "Development Environment Sample Wizard");
			}
			else
			{
				MessageBox.Show("A set of parameters which cannot be decoded was passed to the wizard.\n\nIf the wizard type is a custom wizard, then modify the sample to\naccept your custom argument settings");
			}
		}


//	The following is a code sample describing how a wizard is called. If you wished to invoke a wizard
//		using the DTE.LaunchWizard API, you would do it with code such as the following:

//		private _DTE applicationObject;
//		private void launchProject()
//		{
//		//here, applicationObject should already get DTE instance.
//			int com_ind = applicationObject.FullName.IndexOf("Common7");
//			string vszpath = applicationObject.FullName.Remove(com_ind, applicationObject.FullName.Length - com_ind);
//			vszpath = vszpath + "VC#\\CSharpProjects\\CSharpSampleProjWizard.vsz";
//			string projpath = "c:\\testlaunchwiz";
//
//			object [] safearray = new object[7];
//			safearray[0] = EnvDTE.Constants.vsWizardNewProject;
//			safearray[1] = "testLaunchProjName";
//			safearray[2] = projpath;
//			com_ind = applicationObject.FullName.IndexOf("devenv.exe");
//			safearray[3] = applicationObject.FullName.Remove(com_ind, applicationObject.FullName.Length - com_ind);
//			safearray[4] = true;
//			safearray[5] = "";
//			safearray[6] = false;
//					
//			try
//			{
//				applicationObject.LaunchWizard(vszpath, ref safearray);
//			}
//			catch (Exception e)
//			{
//				MessageBox.Show(e.ToString());
//			}
//		}
//
//		private void launchItem()
//		{
//		//here, applicationObject should already get DTE instance.
//			int com_ind = applicationObject.FullName.IndexOf("Common7");
//			string vszpath1 = applicationObject.FullName.Remove(com_ind, applicationObject.FullName.Length - com_ind);
//			string vszpath = vszpath1 + "VC#\\CSharpProjectItems\\LocalProjectItems\\CSharpSampleItemWizard.vsz";
//
//			object [] safearray = new object[7];
//			safearray[0] = EnvDTE.Constants.vsWizardAddItem;
//			try
//			{//to launch "Add New Item", an project is required in the solution.
//				safearray[1] = applicationObject.Solution.Projects.Item(1).Name;
//				safearray[2] = applicationObject.Solution.Projects.Item(1).ProjectItems;
//				com_ind = applicationObject.Solution.Projects.Item(1).FullName.LastIndexOf(applicationObject.Solution.Projects.Item(1).Name);
//				safearray[3] = applicationObject.Solution.Projects.Item(1).FullName.Remove(com_ind, applicationObject.Solution.Projects.Item(1).FullName.Length - com_ind);
//				safearray[4] = "SampleItemName";
//				safearray[5] = vszpath1 + "Vc7";
//				safearray[6] = false;
//
//				applicationObject.LaunchWizard(vszpath, ref safearray);
//			}
//			catch (Exception e)
//			{
//				MessageBox.Show(e.ToString());
//			}
//
//		}
//
	}
}
