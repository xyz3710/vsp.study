Copyright (c) Microsoft Corporation.  All rights reserved.

This wizard displays the values of context and custom parameters passed to a custom wizard when it is invoked. You will be able to double click on the custom icon in the New Project dialog under C# projects, or in the the Add New Item dialog for a C# project to run the wizard.

To install and run this wizard:

1) a. For showing add new project through wizard, copy all the files in "NewProjectMisc" to the CSharpProjects directory.
      (e.g., C:\Program Files\Microsoft Visual Studio 8\VC#\CSharpProjects)
   b. For showing add new item through wizard, copy all the files in "AddItemMisc" to the CSharp local project items directory.
      (e.g., C:\Program Files\Microsoft Visual Studio 8\VC#\CSharpProjectItems\LocalProjectItems)
   

2) Build WizardContextCS project.

To launch add new project sample wizard, choose New Project -> Visual C# Projects -> C# Sample Wizard. This wizard only shows what values passed to a wizard, no new project is really created.

To launch add new item sample wizard, you need to open a CSharp project in solution, then for the project, choose "Add New Item ...".  In the dialog, choose "CSharpSampleWizard", the last icon.  Again, this wizard only shows what values passed to a wizard, no new item is really created.