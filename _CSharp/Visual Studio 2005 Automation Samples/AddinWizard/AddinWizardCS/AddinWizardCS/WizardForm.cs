//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EnvDTE;
using Microsoft.Win32;
using System.Reflection;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace AddinWizardCS
{
    public partial class WizardForm : Form
    {
        public WizardForm()
        {
            InitializeComponent();
        }

        private DTE _appDTE;

        private wizardResult _wizardResult;

        private string _installationDirectory;
        private string _projectName, _localProjectPath;
        private string _addinName, _addinDescription, _safeName;
        private string _templateName, _templatePath;
        private bool _isAddinLangVB, _isAddinLangCS, _isAddinLangVC;
        private Project _addinProject;

        /// <summary>
        /// Gets and sets the ProjectPath
        /// </summary>
        internal string LocalProjectPath
        {
            get { return _localProjectPath; }
            set { _localProjectPath = value; }
        }

        /// <summary>
        /// Gets and sets the ProjectName
        /// </summary>
        internal string ProjectName
        {
            set
            {
                _projectName = value;
                this.textBoxAddinName.Text = String.Format("{0} - No name provided", _projectName);
            }
        }

        /// <summary>
        /// Gets and Sets the DTE object
        /// </summary>
        internal DTE AppDTE
        {
            set { _appDTE = value; }
        }

        /// <summary>
        /// Gets the WizardResult
        /// </summary>
        internal wizardResult WizardResult
        {
            get { return _wizardResult; }
        }

        /// <summary>
        /// Installation directory of the Add-in
        /// </summary>
        /// <value>installation directory</value>
        internal string InstallationDirectory
        {
            set { _installationDirectory = value; }
        }

        /// <summary>
        /// This method creates a .addin file given the filename.
        /// </summary>
        /// <param name="bAddinUI">determines if the CommandPreLoad value needs to be set</param>
        /// <param name="addinFileName">addin filename</param>
        private void CreateAddinXml(bool bAddinUI, string addinFileName)
        {
            // Get the resource stream for the TokenizedAddinXml.Addin file and then replace the tokens with the
            // appropriate values.
            Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Stream resStream = assembly.GetManifestResourceStream("AddinWizardCS.TokenizedAddinXml.Addin");
            string addinFileContents = "";
            using (StreamReader reader = new StreamReader(resStream))
            {
                addinFileContents = reader.ReadToEnd();
                addinFileContents = addinFileContents.Replace("<%=ADDINNAME%>", _addinName);
                addinFileContents = addinFileContents.Replace("<%=ADDINDESCRIPTION%>", _addinDescription);
                string addinDllPath = addinFileName.EndsWith(" - Test.addin") ? Path.Combine(LocalProjectPath, "bin") : "";
                addinFileContents = addinFileContents.Replace("<%=ADDINDLL%>", Path.Combine(addinDllPath, _addinProject.Name + ".dll"));
                addinFileContents = addinFileContents.Replace("<%=ADDINCLASSNAME%>", _addinProject.Name + ".Connect");
                string preLoad = (bAddinUI) ? "1" : "0";
                addinFileContents = addinFileContents.Replace("<%=PRELOAD%>", preLoad);
                using (StreamWriter writer = new StreamWriter(addinFileName, false, Encoding.Unicode))
                {
                    writer.WriteLine(addinFileContents);
                }
            }
        }

        /// <summary>
        /// This method reads the tokenized registration information from resources and then replaces the tokens
        /// with the appropriate values and returns the string.
        /// </summary>
        /// <returns>string with tokens replaced with correct values</returns>
        private string GetRegistrationInfo()
        {
            Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

            Stream resStream = assembly.GetManifestResourceStream("AddinWizardCS.AddinRegistrationInfo.txt");
            string fileContents = "";
            using (StreamReader reader = new StreamReader(resStream))
            {
                fileContents = reader.ReadToEnd();
                fileContents = fileContents.Replace("<%=ADDINNAME%>", _addinName);
                fileContents = fileContents.Replace("<%=ADDINDESCRIPTION%>", _addinDescription);
            }
            return fileContents;
        }

        /// <summary>
        /// This method reads the ergistry and returns the path where the Extensibility project templates are stored.
        /// </summary>
        /// <returns>project template path</returns>
        string GetTemplatePath()
        {
            RegistryKey regKeyTemplatePath;
            string strRegistryRoot, strRegistryPath;

            strRegistryRoot = _appDTE.RegistryRoot;
            strRegistryPath = strRegistryRoot + @"\NewProjectTemplates\TemplateDirs\{DA9FB551-C724-11d0-AE1F-00A0C90FFFC3}\/2";
            regKeyTemplatePath = Registry.LocalMachine.OpenSubKey(strRegistryPath);
            return (string)regKeyTemplatePath.GetValue("TemplatesDir");
        }

        /// <summary>
        /// This method adds a reference to the given project
        /// </summary>
        /// <param name="project">the project to which the reference will be added</param>
        /// <param name="strAssemblyName">the name of hte assembly whose reference will be added</param>
        static void AddReference(EnvDTE.Project project, string strAssemblyName)
        {
            VSLangProj.VSProject vsProject;

            vsProject = (VSLangProj.VSProject)project.Object;
            vsProject.References.Add(strAssemblyName);
        }

        /// <summary>
        /// This method deletes the lines between the specified tokens.
        /// </summary>
        /// <param name="window">the window containing the text in which to delete lines</param>
        /// <param name="strStartSearchToken">start token</param>
        /// <param name="strEndSearchToken">end token</param>
        static void DeleteBetweenLines(Window window, string strStartSearchToken, string strEndSearchToken)
        {
            EnvDTE.EditPoint editPointStart, editPointEnd, editPointTemp;
            EnvDTE.TextSelection textSelection;
            EnvDTE.TextRanges textRanges;
            EnvDTE.TextDocument textDoc;

            textDoc = (TextDocument)window.DTE.ActiveDocument.Object("TextDocument");
            textSelection = (TextSelection)window.DTE.ActiveDocument.Selection;
            textRanges = textSelection.TextRanges;
            editPointStart = textDoc.CreateEditPoint(textDoc.StartPoint);

            editPointTemp = editPointEnd = editPointStart;

            while (false != editPointStart.FindPattern(strStartSearchToken, 0, ref editPointTemp, ref textRanges))
            {
                editPointTemp.FindPattern(strEndSearchToken, 0, ref editPointEnd, ref textRanges);
                textSelection.MoveToPoint(editPointStart, false);
                textSelection.MoveToPoint(editPointEnd, true);
                textSelection.Text = String.Empty;
            }
        }

        /// <summary>
        /// This method replaces a token with the given string
        /// </summary>
        /// <param name="window">the window containing the text in which to replace tokens</param>
        /// <param name="strSearchToken">the token to be replaced</param>
        /// <param name="strReplaceWith">the replacement string</param>
        static void ReplaceToken(Window window, string strSearchToken, string strReplaceWith)
        {
            //window.Document.ReplaceText(strSearchToken, strReplaceWith, (int)vsFindOptions.vsFindOptionsFromStart);
            EnvDTE.EditPoint editPointStart, editPointEnd;
            EnvDTE.TextSelection textSelection;
            EnvDTE.TextRanges textRanges;
            EnvDTE.TextDocument textDoc;

            textDoc = (TextDocument)window.DTE.ActiveDocument.Object("TextDocument");
            textSelection = (TextSelection)window.DTE.ActiveDocument.Selection;
            textRanges = textSelection.TextRanges;
            editPointStart = textDoc.CreateEditPoint(textDoc.StartPoint);

            editPointEnd = editPointStart;

            while (false != editPointStart.FindPattern(strSearchToken, 0, ref editPointEnd, ref textRanges))
            {
                textSelection.MoveToPoint(editPointStart, false);
                textSelection.MoveToPoint(editPointEnd, true);
                textSelection.Text = strReplaceWith;
                editPointStart = editPointEnd;
            }
        }

        /// <summary>
        /// this method makes some standard replacements.
        /// </summary>
        /// <param name="window">the window containing the text in which to replace tokens</param>
        /// <param name="strAddinGUID">the addin GUID</param>
        /// <param name="strCLSIDIConnect">the Connect class GUID</param>
        void MakeDefaultReplacements(Window window, string strAddinGUID, string strCLSIDIConnect)
        {
            string strPROGID;

            strPROGID = this._safeName + ".Connect";
            ReplaceToken(window, "<%=ICONNECTCLSID%>", strCLSIDIConnect);
            ReplaceToken(window, "<%=VERPROGID%>", strPROGID);
            ReplaceToken(window, "<%=SAFEOBJNAME%>", _safeName);
            ReplaceToken(window, "<%=LIBID%>", strAddinGUID);
        }

        private void buttonFinish_Click(object sender, System.EventArgs e)
        {
            ProjectItem addinPrjItemCnt, addinPrjItem;
            string strAddinGUID, strCLSIDIConnect;
            Window windowCnt, window;
            bool bAddinUI = false;
            Guid LIBID_Addin, CLSIDIConnect;

            _appDTE.Solution.Close(true);
            _isAddinLangCS = _isAddinLangVB = _isAddinLangVC = false;
            if (radioButtonCS.Checked) _isAddinLangCS = true;
            else if (radioButtonVB.Checked) _isAddinLangVB = true;
            else if (radioButtonVC.Checked) _isAddinLangVC = true;

            this._addinName = textBoxAddinName.Text;
            this._addinDescription = textBoxAddinDescp.Text;

            // Get the location of the project templates
            _templatePath = GetTemplatePath();

            //create GUID
            LIBID_Addin = Guid.NewGuid();
            strAddinGUID = LIBID_Addin.ToString("D");
            CLSIDIConnect = Guid.NewGuid();
            strCLSIDIConnect = CLSIDIConnect.ToString("D");
            _safeName = _projectName;

            // Get the template and the project names.
            if (_isAddinLangVC)
            {
                _templateName = _templatePath + @"\VCATL\AddIn.vcproj";
                _projectName += ".vcproj";
            }
            else if (_isAddinLangVB)
            {
                _templateName = _templatePath + @"\VBasic\Project1.vbproj";
                _projectName = _projectName + ".vbproj";
            }
            else //bAddinLangCS
            {
                _templateName = _templatePath + @"\CSharp\Project1.csproj";
                _projectName = _projectName + ".csproj";
            }

            // Add the projects to the Solution
            _appDTE.Solution.AddFromTemplate(_templateName, _localProjectPath, _projectName, false);
            _addinProject = _appDTE.Solution.Projects.Item(1);

            // Add references
            if (_isAddinLangVB || _isAddinLangCS)
            {
                AddReference(_addinProject, "EnvDTE");
                AddReference(_addinProject, "EnvDTE80");
                AddReference(_addinProject, "Extensibility");
                if (checkBoxUI.Checked)
                {
                    AddReference(_addinProject, "Microsoft.VisualStudio.CommandBars");
                }
            }

            // Get the file containing the Connect class implementation. This is needed to replace tokens in this file.
            if (_isAddinLangVC)
                addinPrjItemCnt = _addinProject.ProjectItems.Item("Source Files").ProjectItems.Item("Connect.cpp");
            else if (_isAddinLangVB)
                addinPrjItemCnt = _addinProject.ProjectItems.Item("Connect.vb");
            else //if (bAddinLangCS)
                addinPrjItemCnt = _addinProject.ProjectItems.Item("Connect.cs");


            windowCnt = addinPrjItemCnt.Open(Constants.vsViewKindCode);
            windowCnt.Activate();
            MakeDefaultReplacements(windowCnt, strAddinGUID, strCLSIDIConnect);

            // Make changes based on whether a Tools menu item has been requested or not.
            if (checkBoxUI.Checked == true)
            {
                bAddinUI = true;
                DeleteBetweenLines(windowCnt, "<%BEGIN NOT VSCommand%>", "<%END NOT VSCommand%>");
                ReplaceToken(windowCnt, "<%BEGIN VSCommand%>", "");
                ReplaceToken(windowCnt, "<%END VSCommand%>", "");
            }
            else
            {
                DeleteBetweenLines(windowCnt, "<%BEGIN VSCommand%>", "<%END VSCommand%>");
                ReplaceToken(windowCnt, "<%BEGIN NOT VSCommand%>", "");
                ReplaceToken(windowCnt, "<%END NOT VSCommand%>", "");

                if (_isAddinLangVB)
                {
                    ReplaceToken(windowCnt, "Imports Microsoft.VisualStudio.CommandBars", "");
                }
                if (_isAddinLangCS)
                {
                    ReplaceToken(windowCnt, "using Microsoft.VisualStudio.CommandBars", "");
                }
            }
            windowCnt.Close(vsSaveChanges.vsSaveChangesYes);

            if (_isAddinLangCS || _isAddinLangVB)
            {
                // Create the .addin files (one in the local folder and one in the My Documents\Visual Studio 2005\Addins folder)
                // so that the add-in can be debugged.
                string addinFileName = Path.Combine(LocalProjectPath, _addinProject.Name + ".Addin");
                CreateAddinXml(bAddinUI, addinFileName);
                addinPrjItem = _addinProject.ProjectItems.AddFromFileCopy(addinFileName);
                addinFileName = Environment.ExpandEnvironmentVariables(@"%userprofile%\My Documents\Visual Studio 2005\Addins\" + _addinProject.Name + " - Test.addin");
                CreateAddinXml(bAddinUI, addinFileName);
                addinPrjItem = _addinProject.ProjectItems.AddFromFile(addinFileName);
            }
            if (_isAddinLangVC)
            {
                // Replace the tokens with the correct information.
                // Create the registry information for registering the CPP add-in.
                addinPrjItem = _addinProject.ProjectItems.Item("Resource Files").ProjectItems.Item("AddIn.rgs");
                window = addinPrjItem.Open(Constants.vsViewKindCode);
                window.Activate();
                MakeDefaultReplacements(window, strAddinGUID, strCLSIDIConnect);
                ReplaceToken(window, "<%=REGISTRATIONINFO%>", GetRegistrationInfo());
                window.Close(vsSaveChanges.vsSaveChangesYes);

                addinPrjItem = _addinProject.ProjectItems.Item("Source Files").ProjectItems.Item("AddIn.idl");
                window = addinPrjItem.Open(Constants.vsViewKindCode);
                window.Activate();
                MakeDefaultReplacements(window, strAddinGUID, strCLSIDIConnect);
                window.Close(vsSaveChanges.vsSaveChangesYes);

                addinPrjItem = _addinProject.ProjectItems.Item("Header Files").ProjectItems.Item("stdafx.h");
                window = addinPrjItem.Open(Constants.vsViewKindCode);
                window.Activate();
                MakeDefaultReplacements(window, strAddinGUID, strCLSIDIConnect);
                window.Close(vsSaveChanges.vsSaveChangesYes);

                addinPrjItem = _addinProject.ProjectItems.Item("Resource Files").ProjectItems.Item("AddIn.rgs");
                window = addinPrjItem.Open(Constants.vsViewKindCode);
                window.Activate();
                MakeDefaultReplacements(window, strAddinGUID, strCLSIDIConnect);
                window.Close(vsSaveChanges.vsSaveChangesYes);

                addinPrjItem = _addinProject.ProjectItems.Item("Header Files").ProjectItems.Item("Connect.h");
                window = addinPrjItem.Open(Constants.vsViewKindCode);
                window.Activate();
                if (checkBoxUI.Checked == true)
                {
                    bAddinUI = true;
                    DeleteBetweenLines(window, "<%BEGIN NOT VSCommand%>", "<%END NOT VSCommand%>");
                    ReplaceToken(window, "<%BEGIN VSCommand%>", "");
                    ReplaceToken(window, "<%END VSCommand%>", "");
                }
                else
                {
                    DeleteBetweenLines(window, "<%BEGIN VSCommand%>", "<%END VSCommand%>");
                    ReplaceToken(window, "<%BEGIN NOT VSCommand%>", "");
                    ReplaceToken(window, "<%END NOT VSCommand%>", "");
                }
                window.Close(vsSaveChanges.vsSaveChangesYes);
            }//bAddinLangCPP

            string strDevenvPath;
            strDevenvPath = _installationDirectory + "devenv.exe";
            //Set up the target application (debugging host) for debugging
            long lCfgCount, i;
            ConfigurationManager cfgManager;
            Configuration configuration;

            cfgManager = _addinProject.ConfigurationManager;
            lCfgCount = cfgManager.Count;
            for (i = 1; i <= lCfgCount; i++)
            {
                configuration = cfgManager.Item(i, "Any CPU");

                //Start action == program
                if (!_isAddinLangVC)
                {
                    configuration.Properties.Item("OutputPath").Value = @"bin\";
                    configuration.Properties.Item("StartArguments").Value = String.Format("/resetaddin {0}.Connect", _addinProject.Name);
                    configuration.Properties.Item("StartAction").Value = VSLangProj.prjStartAction.prjStartActionProgram;
                    configuration.Properties.Item("StartWorkingDirectory").Value = _installationDirectory;
                    configuration.Properties.Item("StartProgram").Value = strDevenvPath;
                }
                else
                {
                    configuration.Properties.Item("Command").Value = strDevenvPath;
                }
            }
            if (_isAddinLangCS)
                addinPrjItemCnt = _addinProject.ProjectItems.Item("Connect.cs");
            else if (_isAddinLangVB)
                addinPrjItemCnt = _addinProject.ProjectItems.Item("Connect.vb");
            else addinPrjItemCnt = _addinProject.ProjectItems.Item("Source Files").ProjectItems.Item("Connect.cpp");

            windowCnt = addinPrjItemCnt.Open(EnvDTE.Constants.vsViewKindCode);
            windowCnt.Activate();

            _wizardResult = wizardResult.wizardResultSuccess;
            this.Close();
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            _wizardResult = wizardResult.wizardResultCancel;
            this.Close();
        }

        private void buttonBack_Click(object sender, System.EventArgs e)
        {
            _wizardResult = wizardResult.wizardResultBackOut;
            this.Close();
        }

        private void buttonHelp_Click(object sender, System.EventArgs e)
        {
            Microsoft.VisualStudio.VSHelp.Help vsHelp;
            vsHelp = (Microsoft.VisualStudio.VSHelp.Help)_appDTE.GetObject("Help");
            vsHelp.DisplayTopicFromF1Keyword("IDTWizard");
        }
    }
}