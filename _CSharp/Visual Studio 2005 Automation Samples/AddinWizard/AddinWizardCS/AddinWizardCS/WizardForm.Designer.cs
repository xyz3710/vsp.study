//Copyright (c) Microsoft Corporation.  All rights reserved.

namespace AddinWizardCS
{
    partial class WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonHelp = new System.Windows.Forms.Button();
            this.labelAddinDescp = new System.Windows.Forms.Label();
            this.textBoxAddinName = new System.Windows.Forms.TextBox();
            this.textBoxAddinDescp = new System.Windows.Forms.TextBox();
            this.labelAddinName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.radioButtonVC = new System.Windows.Forms.RadioButton();
            this.radioButtonVB = new System.Windows.Forms.RadioButton();
            this.radioButtonCS = new System.Windows.Forms.RadioButton();
            this.buttonFinish = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxUI = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(37, 390);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(72, 24);
            this.buttonHelp.TabIndex = 30;
            this.buttonHelp.Text = "&Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // labelAddinDescp
            // 
            this.labelAddinDescp.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelAddinDescp.Location = new System.Drawing.Point(22, 74);
            this.labelAddinDescp.Name = "labelAddinDescp";
            this.labelAddinDescp.Size = new System.Drawing.Size(256, 23);
            this.labelAddinDescp.TabIndex = 32;
            this.labelAddinDescp.Text = "What is the &description of your Add-in?";
            // 
            // textBoxAddinName
            // 
            this.textBoxAddinName.Location = new System.Drawing.Point(40, 40);
            this.textBoxAddinName.Name = "textBoxAddinName";
            this.textBoxAddinName.Size = new System.Drawing.Size(392, 22);
            this.textBoxAddinName.TabIndex = 24;
            this.textBoxAddinName.Text = "SimpleAddin - No Name provided.";
            // 
            // textBoxAddinDescp
            // 
            this.textBoxAddinDescp.Location = new System.Drawing.Point(40, 100);
            this.textBoxAddinDescp.Name = "textBoxAddinDescp";
            this.textBoxAddinDescp.Size = new System.Drawing.Size(392, 22);
            this.textBoxAddinDescp.TabIndex = 25;
            this.textBoxAddinDescp.Text = "No Description provided.";
            // 
            // labelAddinName
            // 
            this.labelAddinName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddinName.Location = new System.Drawing.Point(22, 18);
            this.labelAddinName.Name = "labelAddinName";
            this.labelAddinName.Size = new System.Drawing.Size(240, 21);
            this.labelAddinName.TabIndex = 29;
            this.labelAddinName.Text = "What is the n&ame of your Add-in?";
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(224, 390);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(68, 24);
            this.buttonBack.TabIndex = 26;
            this.buttonBack.Text = "<&Back";
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // radioButtonVC
            // 
            this.radioButtonVC.Location = new System.Drawing.Point(25, 77);
            this.radioButtonVC.Name = "radioButtonVC";
            this.radioButtonVC.Size = new System.Drawing.Size(290, 24);
            this.radioButtonVC.TabIndex = 23;
            this.radioButtonVC.Text = "Create an Add-in using Visual C++ / &ATL.";
            // 
            // radioButtonVB
            // 
            this.radioButtonVB.Location = new System.Drawing.Point(25, 49);
            this.radioButtonVB.Name = "radioButtonVB";
            this.radioButtonVB.Size = new System.Drawing.Size(255, 24);
            this.radioButtonVB.TabIndex = 22;
            this.radioButtonVB.Text = "Create an Add-in using &Visual Basic.";
            // 
            // radioButtonCS
            // 
            this.radioButtonCS.Checked = true;
            this.radioButtonCS.Location = new System.Drawing.Point(25, 21);
            this.radioButtonCS.Name = "radioButtonCS";
            this.radioButtonCS.Size = new System.Drawing.Size(240, 24);
            this.radioButtonCS.TabIndex = 21;
            this.radioButtonCS.TabStop = true;
            this.radioButtonCS.Text = "Create an Add-in using Visual &C#.";
            // 
            // buttonFinish
            // 
            this.buttonFinish.Location = new System.Drawing.Point(293, 390);
            this.buttonFinish.Name = "buttonFinish";
            this.buttonFinish.Size = new System.Drawing.Size(68, 24);
            this.buttonFinish.TabIndex = 27;
            this.buttonFinish.Text = "&Finish";
            this.buttonFinish.Click += new System.EventHandler(this.buttonFinish_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(400, 390);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(68, 24);
            this.buttonCancel.TabIndex = 28;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(11, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(457, 40);
            this.label1.TabIndex = 20;
            this.label1.Text = "Welcome to the Add-in Wizard";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonCS);
            this.groupBox1.Controls.Add(this.radioButtonVB);
            this.groupBox1.Controls.Add(this.radioButtonVC);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 117);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Programming Language";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelAddinName);
            this.groupBox2.Controls.Add(this.textBoxAddinDescp);
            this.groupBox2.Controls.Add(this.labelAddinDescp);
            this.groupBox2.Controls.Add(this.textBoxAddinName);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 176);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(456, 128);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Name and Description";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxUI);
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 311);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(456, 60);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tools Menu Item";
            // 
            // checkBoxUI
            // 
            this.checkBoxUI.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUI.Location = new System.Drawing.Point(40, 21);
            this.checkBoxUI.Name = "checkBoxUI";
            this.checkBoxUI.Size = new System.Drawing.Size(369, 24);
            this.checkBoxUI.TabIndex = 7;
            this.checkBoxUI.Text = "Yes, create a \'&Tools\' menu item to interact with the Add-in.";
            // 
            // WizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 419);
            this.Controls.Add(this.buttonHelp);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonFinish);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Name = "WizardForm";
            this.Text = "A Wizard Sample in C# for simple Visual Studio Add-in";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonHelp;
        private System.Windows.Forms.Label labelAddinDescp;
        private System.Windows.Forms.TextBox textBoxAddinName;
        private System.Windows.Forms.TextBox textBoxAddinDescp;
        private System.Windows.Forms.Label labelAddinName;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.RadioButton radioButtonVC;
        private System.Windows.Forms.RadioButton radioButtonVB;
        private System.Windows.Forms.RadioButton radioButtonCS;
        private System.Windows.Forms.Button buttonFinish;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxUI;
    }
}