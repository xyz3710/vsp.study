//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using System.Collections.Generic;
using System.Text;
using EnvDTE;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace AddinWizardCS
{
    [GuidAttribute("E021A7D3-B3FB-11D2-97DC-000000000000"), ProgId("AddinWizardCS.AddinWizard")]
    [ComVisible(true)]
    public class AddinWizard : IDTWizard
    {
        private DTE _appDTE;
        private string _projectName;
        private string _localProjectPath;
        private string _installationDirectory;

        #region IDTWizard Members

        public void Execute(object Application, int hwndOwner, ref object[] ContextParams, ref object[] CustomParams, ref wizardResult retval)
        {
            _appDTE = (DTE)Application;

            //get information from ContextParams & CustomParams
            _projectName = (string)ContextParams[1];
            _localProjectPath = (string)ContextParams[2];
            _installationDirectory = (string)ContextParams[3];

            //check project name
            bool bValid = true;
            Regex regex = new Regex("[^a-zA-Z0-9_]");
            Match match = regex.Match(_projectName);
            if (match.Success)
            {
                System.Windows.Forms.MessageBox.Show("A name containing invalid characters was entered. All the characters in the name must be \n in the range of a to z, A to Z, and 0 to 9, or '_'", "Visual Studio Simple Add-in Wizard");
                bValid = false;
            }
            if (bValid)
            {
                WizardForm rootFrm = new WizardForm();
                rootFrm.AppDTE = _appDTE;
                rootFrm.ProjectName = _projectName;
                rootFrm.LocalProjectPath = _localProjectPath;
                rootFrm.InstallationDirectory = _installationDirectory;
                rootFrm.ShowDialog();
                retval = rootFrm.WizardResult;
            }
            else
                retval = wizardResult.wizardResultBackOut;
        }

        #endregion

    }
}
