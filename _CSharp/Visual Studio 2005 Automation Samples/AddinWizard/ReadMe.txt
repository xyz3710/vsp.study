Copyright (c) Microsoft Corporation.  All rights reserved.

This is a wizard sample written in C# that creates simple Visual Studio .NET Add-in. 

How to install this wizard:

Once you download the sample, you need to do the following before running it:

1. Copy the files "Simple VS Add-in.vsz" and "Simple VS Add-in.ico" to the extensibility project items directory.
   (e.g., C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\Extensibility Projects)

2. Open the file "Add-in Projects.vsdir" in the above mentioned directory in notepad or another editor. Add this line:
   " Simple VS Add-in.vsz|{DA9FB551-C724-11d0-AE1F-00A0C90FFFC3}|0|3|#14278|{DA9FB551-C724-11d0-AE1F-00A0C90FFFC3}|6777| |#14279 "

3. Build AddinWizardCS project.

Then you can launch the wizard from New Project -> Other Projects -> Extensibility Projects -> Simple VS Add-in
Add-in created by this wizard only shows up in add-in manager and can be debugged and deployed successfully.