'Copyright (c) Microsoft Corporation.  All rights reserved.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CmdBrowserFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.closeButton = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.searchButton = New System.Windows.Forms.Button
        Me.searchTextBox = New System.Windows.Forms.TextBox
        Me.informationButton = New System.Windows.Forms.Button
        Me.infoTreeView = New System.Windows.Forms.TreeView
        Me.SuspendLayout()
        '
        'closeButton
        '
        Me.closeButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.closeButton.Location = New System.Drawing.Point(441, 41)
        Me.closeButton.Name = "closeButton"
        Me.closeButton.Size = New System.Drawing.Size(75, 23)
        Me.closeButton.TabIndex = 8
        Me.closeButton.Text = "Close"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(12, 268)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(424, 8)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(9, 284)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 23)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "S&earch:"
        '
        'searchButton
        '
        Me.searchButton.Location = New System.Drawing.Point(361, 279)
        Me.searchButton.Name = "searchButton"
        Me.searchButton.Size = New System.Drawing.Size(75, 23)
        Me.searchButton.TabIndex = 12
        Me.searchButton.Text = "&Search"
        '
        'searchTextBox
        '
        Me.searchTextBox.Location = New System.Drawing.Point(63, 281)
        Me.searchTextBox.Name = "searchTextBox"
        Me.searchTextBox.Size = New System.Drawing.Size(292, 20)
        Me.searchTextBox.TabIndex = 10
        '
        'informationButton
        '
        Me.informationButton.Location = New System.Drawing.Point(442, 12)
        Me.informationButton.Name = "informationButton"
        Me.informationButton.Size = New System.Drawing.Size(75, 23)
        Me.informationButton.TabIndex = 7
        Me.informationButton.Text = "&Information"
        '
        'infoTreeView
        '
        Me.infoTreeView.HideSelection = False
        Me.infoTreeView.Location = New System.Drawing.Point(12, 12)
        Me.infoTreeView.Name = "infoTreeView"
        Me.infoTreeView.Size = New System.Drawing.Size(424, 256)
        Me.infoTreeView.TabIndex = 6
        '
        'CmdBrowserFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(528, 311)
        Me.ControlBox = False
        Me.Controls.Add(Me.closeButton)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.searchButton)
        Me.Controls.Add(Me.searchTextBox)
        Me.Controls.Add(Me.informationButton)
        Me.Controls.Add(Me.infoTreeView)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CmdBrowserFrm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Command Browser"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents closeButton As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents searchButton As System.Windows.Forms.Button
    Friend WithEvents searchTextBox As System.Windows.Forms.TextBox
    Friend WithEvents informationButton As System.Windows.Forms.Button
    Friend WithEvents infoTreeView As System.Windows.Forms.TreeView
End Class
