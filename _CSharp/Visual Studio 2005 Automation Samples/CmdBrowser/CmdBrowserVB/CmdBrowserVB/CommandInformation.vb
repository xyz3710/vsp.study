'Copyright (c) Microsoft Corporation.  All rights reserved.

Public Class CommandInformation
    Public Sub New(ByVal dteInstance As EnvDTE80.DTE2, ByVal cmdName As String)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Dim cmd As EnvDTE.Command
        Dim bindings As Object()
        Dim i As Integer
        cmd = dteInstance.Commands.Item(cmdName, -1)
        cmdNameTextBox.Text = cmdName
        guidTextBox.Text = cmd.Guid
        idTextBox.Text = cmd.ID.ToString()
        bindings = CType(cmd.Bindings, Object())
        For i = 0 To bindings.Length - 1
            keyboardBindings.Items.Add(CStr(bindings(i)))
        Next
        locCmdNameTextBox.Text = cmd.LocalizedName
        If (bindings.Length > 0) Then
            keyboardBindings.SelectedIndex = 0
        End If
    End Sub

End Class