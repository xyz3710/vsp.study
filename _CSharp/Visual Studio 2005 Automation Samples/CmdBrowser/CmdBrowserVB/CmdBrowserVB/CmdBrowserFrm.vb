'Copyright (c) Microsoft Corporation.  All rights reserved.

Public Class CmdBrowserFrm

    Dim _applicationObject As EnvDTE80.DTE2
    Dim searchNode As System.Windows.Forms.TreeNode

    Public Sub New(ByVal dteInstance As EnvDTE80.DTE2)
        MyBase.New()

        _applicationObject = dteInstance

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    Structure commandBarControlData
        Dim commandName As String
    End Structure

    Private Sub WalkCommandBars(ByVal currNode As System.Windows.Forms.TreeNode, ByVal cmdBar As Microsoft.VisualStudio.CommandBars.CommandBar)
        Dim cmdBarCtl As Microsoft.VisualStudio.CommandBars.CommandBarControl
        Dim addedNode As System.Windows.Forms.TreeNode
        addedNode = currNode.Nodes.Add(cmdBar.Name)

        For Each cmdBarCtl In cmdBar.Controls
            If (cmdBarCtl.Type = Microsoft.VisualStudio.CommandBars.MsoControlType.msoControlButton) Then
                Dim tmpCtl As System.Windows.Forms.TreeNode
                Dim cbcData As commandBarControlData
                Dim guidCommand As String = ""
                Dim idCommand As Integer = 0
                tmpCtl = addedNode.Nodes.Add(cmdBarCtl.Caption)
                Try
                    _applicationObject.Commands.CommandInfo(cmdBarCtl, guidCommand, idCommand)
                    cbcData.commandName = _applicationObject.Commands.Item(guidCommand, idCommand).Name
                    tmpCtl.Tag = cbcData
                Catch ex As System.Exception
                End Try
            ElseIf (cmdBarCtl.Type = Microsoft.VisualStudio.CommandBars.MsoControlType.msoControlPopup) Then
                Dim cmdBarPopup As Microsoft.VisualStudio.CommandBars.CommandBarPopup

                cmdBarPopup = CType(cmdBarCtl, Microsoft.VisualStudio.CommandBars.CommandBarPopup)
                WalkCommandBars(addedNode, cmdBarPopup.CommandBar)
            End If
        Next
    End Sub

    Private Sub CmdBrowserFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cmd As EnvDTE.Command
        Dim commandsNode As System.Windows.Forms.TreeNode
        Dim toolBars As System.Windows.Forms.TreeNode

        commandsNode = infoTreeView.Nodes.Add("Commands")
        toolBars = infoTreeView.Nodes.Add("Tool Bars")

        For Each cmd In _applicationObject.Commands
            If (cmd.Name <> Nothing) Then
                commandsNode.Nodes.Add(cmd.Name)
            End If
        Next

        Dim cmdBar As Microsoft.VisualStudio.CommandBars.CommandBar
        For Each cmdBar In CType(_applicationObject.CommandBars, Microsoft.VisualStudio.CommandBars.CommandBars)
            WalkCommandBars(toolBars, cmdBar)
        Next
    End Sub

    Private Sub informationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles informationButton.Click
        Dim currentNode As System.Windows.Forms.TreeNode
        Dim tempNode As System.Windows.Forms.TreeNode
        currentNode = infoTreeView.SelectedNode
        If ((currentNode Is Nothing) Or (currentNode.Parent Is Nothing)) Then
            MsgBox("Cannot get information for the currently selected node", MsgBoxStyle.Information, "Command Browser")
        Else
            tempNode = currentNode
            While (Not (tempNode.Parent.Parent Is Nothing))
                tempNode = tempNode.Parent
            End While
            If (tempNode.Parent.Text = "Commands") Then
                Dim cmdInfoFrm As CommandInformation
                cmdInfoFrm = New CommandInformation(_applicationObject, infoTreeView.SelectedNode.Text)
                cmdInfoFrm.ShowDialog(Me)
            ElseIf (tempNode.Parent.Text = "Tool Bars") Then
                If (currentNode.Tag Is Nothing) Then
                    MsgBox("Cannot get information for the currently selected node", MsgBoxStyle.Information, "Command Browser")
                Else
                    Dim cbcData As commandBarControlData
                    Dim cmdInfoFrm As CommandInformation
                    cbcData = CType(currentNode.Tag, commandBarControlData)
                    cmdInfoFrm = New CommandInformation(_applicationObject, cbcData.commandName)
                    cmdInfoFrm.ShowDialog(Me)
                End If
            End If
        End If
    End Sub

    Private Function FindNextNode(ByVal searchNode As System.Windows.Forms.TreeNode) As System.Windows.Forms.TreeNode
        'Strategy for finding nodes: 
        ' 1) Start at the current node, and check if there are any sub items.
        ' 2) If there are, then get the first one
        ' 3) If there are not, then get the sibling of this node
        ' 4) If there are no siblings, get the parent, and find the sibling of that item
        ' 5) If the parent does not have siblings, find the parent of the parent and repeat step 4
        ' 6) If nothing is left, then return with Nothing.
        While (Not (searchNode Is Nothing))
            Dim tnCln As System.Collections.ICollection
            tnCln = searchNode.Nodes

            If (tnCln.Count = 0) Then
                If (searchNode.NextNode Is Nothing) Then
                    Dim tmp As System.Windows.Forms.TreeNode
                    tmp = searchNode.Parent.NextNode
                    If (tmp Is Nothing) Then
                        tmp = searchNode.Parent
                        If (tmp Is Nothing) Then
                            Return Nothing
                        Else
                            While (tmp.NextNode Is Nothing)
                                tmp = tmp.Parent
                                If (tmp Is Nothing) Then
                                    Return Nothing
                                End If
                            End While
                        End If
                        Return tmp.NextNode
                    End If
                    Return tmp
                Else
                    Return searchNode.NextNode
                End If
            Else
                Return searchNode.Nodes.Item(0)
            End If
        End While
        Return Nothing
    End Function

    Private Sub ItemNotFound()
        MsgBox("Search Complete", MsgBoxStyle.Information, "Command Browser")
        searchButton.Enabled = True
        searchButton.Text = "&Search"
    End Sub


    Private Sub searchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles searchButton.Click
        searchButton.Enabled = False
        searchButton.Text = "Searching..."

        'If a search has not started, prime the current node:
        If (searchNode Is Nothing) Then
            searchNode = infoTreeView.Nodes.Item(0)
        Else
            searchNode = FindNextNode(searchNode)

            'Reached the end of the available items:
            If (searchNode Is Nothing) Then
                ItemNotFound()
                Return
            End If
        End If

        'Loop until either something is found, or nothing is left to be found:
        While (True)
            'When getting information from the command bars, wherever there is an '_' on a character, 
            ' it will appear with an '&' in the name. Search with and without the '&':
            Dim strNode As String = searchNode.Text.Replace("&", "")
            If (searchNode.Text.IndexOf(searchTextBox.Text) <> -1) Or (strNode.IndexOf(searchTextBox.Text) <> -1) Then
                searchNode.Expand()
                searchNode.EnsureVisible()
                infoTreeView.SelectedNode = searchNode
                Exit While
            End If

            searchNode = FindNextNode(searchNode)

            'Reached the end of the available items:
            If (searchNode Is Nothing) Then
                ItemNotFound()
                Return
            End If
        End While

        searchButton.Text = "Find &Next"
        searchButton.Enabled = True
    End Sub

    Private Sub searchTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles searchTextBox.TextChanged
        searchButton.Text = "&Search"
        searchNode = Nothing
    End Sub
End Class