'Copyright (c) Microsoft Corporation.  All rights reserved.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CommandInformation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label5 = New System.Windows.Forms.Label
        Me.locCmdNameTextBox = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.keyboardBindings = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.idTextBox = New System.Windows.Forms.TextBox
        Me.guidTextBox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdNameTextBox = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(255, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 16)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "&ID:"
        '
        'locCmdNameTextBox
        '
        Me.locCmdNameTextBox.Location = New System.Drawing.Point(15, 70)
        Me.locCmdNameTextBox.Name = "locCmdNameTextBox"
        Me.locCmdNameTextBox.Size = New System.Drawing.Size(280, 20)
        Me.locCmdNameTextBox.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 51)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(272, 16)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "&Localized Command Name:"
        '
        'keyboardBindings
        '
        Me.keyboardBindings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.keyboardBindings.Location = New System.Drawing.Point(15, 154)
        Me.keyboardBindings.Name = "keyboardBindings"
        Me.keyboardBindings.Size = New System.Drawing.Size(280, 21)
        Me.keyboardBindings.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(12, 135)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(168, 16)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "&Keyboard Bindings:"
        '
        'Button1
        '
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Button1.Location = New System.Drawing.Point(220, 181)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "OK"
        '
        'idTextBox
        '
        Me.idTextBox.Location = New System.Drawing.Point(255, 112)
        Me.idTextBox.Name = "idTextBox"
        Me.idTextBox.Size = New System.Drawing.Size(40, 20)
        Me.idTextBox.TabIndex = 18
        '
        'guidTextBox
        '
        Me.guidTextBox.Location = New System.Drawing.Point(15, 112)
        Me.guidTextBox.Name = "guidTextBox"
        Me.guidTextBox.Size = New System.Drawing.Size(224, 20)
        Me.guidTextBox.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(12, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "&GUID:"
        '
        'cmdNameTextBox
        '
        Me.cmdNameTextBox.Location = New System.Drawing.Point(15, 28)
        Me.cmdNameTextBox.Name = "cmdNameTextBox"
        Me.cmdNameTextBox.Size = New System.Drawing.Size(280, 20)
        Me.cmdNameTextBox.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "&Command Name:"
        '
        'CommandInformation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(302, 213)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.locCmdNameTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.keyboardBindings)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.idTextBox)
        Me.Controls.Add(Me.guidTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdNameTextBox)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CommandInformation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Command Information"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents locCmdNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents keyboardBindings As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents idTextBox As System.Windows.Forms.TextBox
    Friend WithEvents guidTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
