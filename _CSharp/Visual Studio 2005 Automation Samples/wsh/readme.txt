Copyright (c) Microsoft Corporation.  All rights reserved.

The wsh-vs.js sample demonstrates using Windows Scripting Host to launch Visual Studio. It will create an instance of VS and add a task item. If you do not immediately see the task item, right click on the task list, select 'Show Tasks', and select 'UserReminder' from the context menu.

To use this sample, just double click on it from explorer, or type wsh-vs.js at a Windows command prompt.