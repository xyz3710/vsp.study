'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports EnvDTE
Imports System.Runtime.InteropServices

<GuidAttribute("D3F2904D-322C-4afc-9345-BE32E27D7839"), ProgId("XSLWizard.Wizard")> _
Public Class XSLWizard

    Implements IDTWizard

    Public Sub Execute(ByVal Application As Object, ByVal hwndOwner As Integer, ByRef ContextParams() As Object, ByRef CustomParams() As Object, ByRef retval As EnvDTE.wizardResult) Implements EnvDTE.IDTWizard.Execute
        Dim projectItems As ProjectItems
        Dim projectItemXML As ProjectItem
        Dim projectItemXSL As ProjectItem
        Dim xmlWindow As Window
        Dim xmlTextDocument As TextDocument

        projectItems = CType(ContextParams(2), ProjectItems)
        projectItemXML = projectItems.AddFromTemplate(CStr(CustomParams(0)) & "\xmltemplate.xml", CStr(ContextParams(4)) & ".xml")
        projectItemXSL = projectItems.AddFromTemplate(CStr(CustomParams(0)) & "\xsltemplate.xsl", CStr(ContextParams(4)) & ".xsl")
        xmlWindow = projectItemXML.Open(Constants.vsViewKindPrimary)
        xmlTextDocument = CType(xmlWindow.Document.Object("TextDocument"), EnvDTE.TextDocument)

        xmlTextDocument.ReplacePattern("%FILENAME%", CStr(ContextParams(4)))
        projectItemXSL.Open(Constants.vsViewKindPrimary)
    End Sub

End Class
