//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using EnvDTE;

namespace CSXSLWizard
{
    [GuidAttribute("322AF129-AEFD-4c24-9AAB-6A2D2CF54975"), ProgId("XSLWizard.Wizard")]
    public class XSLWizard : EnvDTE.IDTWizard
    {
        #region IDTWizard Members

        public void Execute(object Application, int hwndOwner, ref object[] ContextParams, ref object[] CustomParams, ref EnvDTE.wizardResult retval)
        {
            TextRanges textRanges = null;
            ProjectItems projectItems = (ProjectItems)ContextParams[2];
            ProjectItem projectItemXML = projectItems.AddFromTemplate((string)CustomParams[0] + "\\xmltemplate.xml", (string)ContextParams[4] + ".xml");
            ProjectItem projectItemXSL = projectItems.AddFromTemplate((string)CustomParams[0] + "\\xsltemplate.xsl", (string)ContextParams[4] + ".xsl");
            Window xmlWindow = projectItemXML.Open(Constants.vsViewKindPrimary);
            TextDocument xmlTextDocument = (TextDocument)xmlWindow.Document.Object("TextDocument");
            xmlTextDocument.ReplacePattern("%FILENAME%", (string)ContextParams[4], (int)vsFindOptions.vsFindOptionsNone, ref textRanges);
            projectItemXSL.Open(Constants.vsViewKindPrimary);
        }

        #endregion
    }
}
