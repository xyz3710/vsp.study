Copyright (c) Microsoft Corporation.  All rights reserved.

This sample creates a wizard that lets you add a template in the Add New Item/Local Project Items dialog to create xml and xsl files. 

How to install the XSL Wizard:
1) Copy the file "XSL Wizard.vsz" into a project items directory. For example, for VB this would be the "VBProjectItems" of the install directory (see the list of directories to use at the end of this file)

2) Create a hidden folder named "Templates" under the above mentioned project items directory, and copy the files xsltemplate.xsl and xmltemplate.xml into it. 

3) Open the copied "XSL Wizard.vsz" file in notepad or another editor. Change the string "C:\Program Files\Microsoft Visual Studio 8\Vb7\VBProjectItems\Local Project Items\Templates" to the path of copied Templates folder (if you are not using this wizard for VB).
4) Build the project for the language you wish to use.
5) Create a project (if you used VBProjectItems in step 1, create a VB project, etc), right click the project node in the Solution Explorer, select Add Items, and select "XSL Wizard"

List of folders to use if the default installation path has been used:
VB: C:\Program Files\Microsoft Visual Studio 8\Vb7\VBProjectItems\Local Project Items
C#: C:\Program Files\Microsoft Visual Studio 8\VC#\CSharpProjectItems\LocalProjectItems
VC: C:\Program Files\Microsoft Visual Studio 8\Vc7\vcprojectitems\Utility