Copyright (c) Microsoft Corporation.  All rights reserved.

This add-in sample will show as a customized command in the context menu in Code Window. 
It demonstrates how to dynamically enable and disable the command based on the current file open.

Once you load the sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin CodeWinContextMenu<language>.Connect (where <language> is either CS or VB depending on which version of the sample you loaded)
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

4. Open the solution, in Connect.vb (Connect.cs for C# solution), replace Message box code in Exec( ) with your code regarding your specific command.

5. In QueryStatus(), change "File1.cs" with the filename you want to be associated with the command.

6. Build the solution

7. Load the add-in in Add-in Manager, open the soluion you are working, open the file, right click and you'll see the command enabled or disabled based on current active file.

