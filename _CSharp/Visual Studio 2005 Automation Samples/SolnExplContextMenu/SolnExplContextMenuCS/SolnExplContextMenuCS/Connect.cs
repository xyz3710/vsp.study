//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
namespace SolnExplContextMenuCS
{
	/// <summary>The object for implementing an Add-in.</summary>
	/// <seealso class='IDTExtensibility2' />
	public class Connect : IDTExtensibility2, IDTCommandTarget
	{
		/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
		public Connect()
		{
		}

		/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
		/// <param term='application'>Root object of the host application.</param>
		/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
		/// <param term='addInInst'>Object representing this Add-in.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			_applicationObject = (DTE2)application;
			_addInInstance = (AddIn)addInInst;
            if (connectMode == Extensibility.ext_ConnectMode.ext_cm_UISetup)
            {
                Command command;
                CommandBar itemCmdBar;
                object[] contextGUIDS = new object[] { };
                try
                {
                    // Create a Command with name SolnExplContextMenuCS and then add it to the "Item" menubar for the SolutionExplorer
                    command = _applicationObject.Commands.AddNamedCommand(_addInInstance, "SolnExplContextMenuCS", "My CS Command", "Executes My command 1", true, 59, ref contextGUIDS, (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled);
                    itemCmdBar = ((CommandBars)_applicationObject.CommandBars)["Item"];
                    if (itemCmdBar == null)
                    {
                        System.Windows.Forms.MessageBox.Show("Cannot get the Item menubar", "Error",
                            System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    else
                    {
                        command.AddControl(itemCmdBar, 1);
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
		}

		/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
		/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />		
		public void OnAddInsUpdate(ref Array custom)
		{
		}

		/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref Array custom)
		{
		}

		/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref Array custom)
		{
		}
		
		private DTE2 _applicationObject;
		private AddIn _addInInstance;

        #region IDTCommandTarget Members

        /// <summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
        /// <param term='CmdName'>The name of the command to execute.</param>
        /// <param term='ExecuteOption'>Describes how the command should be run.</param>
        /// <param term='VariantIn'>Parameters passed from the caller to the command handler.</param>
        /// <param term='VariantOut'>Parameters passed from the command handler to the caller.</param>
        /// <param term='Handled'>Informs the caller if the command was handled or not.</param>
        /// <seealso class='Exec' />
        public void Exec(string CmdName, vsCommandExecOption ExecuteOption, ref object VariantIn, ref object VariantOut, ref bool Handled)
        {
            Handled = false;
            if (ExecuteOption == vsCommandExecOption.vsCommandExecOptionDoDefault)
            {
                if (CmdName == "SolnExplContextMenuCS.Connect.SolnExplContextMenuCS")
                {
                    System.Windows.Forms.MessageBox.Show("My Command for ContextMenu in Solution Explorer", "Message");
                    Handled = true;
                    return;
                }
            }
        }

        /// <summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
        /// <param term='CmdName'>The name of the command to determine state for.</param>
        /// <param term='NeededText'>Text that is needed for the command.</param>
        /// <param term='StatusOption'>The state of the command in the user interface.</param>
        /// <param term='CommandText'>Text requested by the neededText parameter.</param>
        /// <seealso class='Exec' />
        public void QueryStatus(string CmdName, vsCommandStatusTextWanted NeededText, ref vsCommandStatus StatusOption, ref object CommandText)
        {
            if (NeededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
            {
                if (CmdName == "SolnExplContextMenuCS.Connect.SolnExplContextMenuCS")
                {
                    //Dynamically enable & disable the command. If the selected file name is File1.cs, then make the command visible.
                    UIHierarchyItem item;
                    UIHierarchy UIH = _applicationObject.ToolWindows.SolutionExplorer;
                    item = (UIHierarchyItem)((System.Array)UIH.SelectedItems).GetValue(0);
                    StatusOption = vsCommandStatus.vsCommandStatusSupported;
                    if (item.Name != "File1.cs")
                    {
                        StatusOption |= vsCommandStatus.vsCommandStatusInvisible;
                    }
                    else
                    {
                        StatusOption |= vsCommandStatus.vsCommandStatusEnabled;
                    }
                }
                else
                {
                    StatusOption = vsCommandStatus.vsCommandStatusUnsupported;
                }
            }
        }

        #endregion
    }
}