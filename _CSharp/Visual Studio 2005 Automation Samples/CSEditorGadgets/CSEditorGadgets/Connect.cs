//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using System.Collections.Specialized;

namespace CSEditorGadgets
{
	/// <summary>The object for implementing an Add-in.</summary>
	/// <seealso class='IDTExtensibility2' />
	public class Connect : IDTExtensibility2
	{
		/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
		public Connect()
		{
		}

		/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
		/// <param term='application'>Root object of the host application.</param>
		/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
		/// <param term='addInInst'>Object representing this Add-in.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			_applicationObject = (DTE2)application;
			_addInInstance = (AddIn)addInInst;

            _stringDictionary = new StringDictionary();

            //Connect to the BeforeKeyPress event handler:
            _keyPressEvents = (TextDocumentKeyPressEvents)((Events2)_applicationObject.Events).get_TextDocumentKeyPressEvents(null);
            _keyPressEvents.BeforeKeyPress += new _dispTextDocumentKeyPressEvents_BeforeKeyPressEventHandler(BeforeKeyPress);

            //Search for a file named abbrev.xml in the same directory as the Add-in.
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            string xmlDocumentPath = System.IO.Path.GetDirectoryName(asm.Location) + "\\abbrev.xml";

            //If the abbrev.xml file is found, then load the XML
            if (System.IO.File.Exists(xmlDocumentPath))
            {
                xmlDoc.Load(xmlDocumentPath);

                //search for nodes containing replacements
                System.Xml.XmlNodeList nodes = xmlDoc.SelectNodes("/Abbrev/replacement");
                foreach (System.Xml.XmlNode node in nodes)
                {
                    //Add the replacements to a dictionary
                    _stringDictionary[node.Attributes.GetNamedItem("keys").Value] = node.Attributes.GetNamedItem("replacewith").Value;
                }
            }
		}

		/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
		/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />		
		public void OnAddInsUpdate(ref Array custom)
		{
		}

		/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref Array custom)
		{
		}

		/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref Array custom)
		{
		}

        void BeforeKeyPress(string Keypress, EnvDTE.TextSelection Selection, bool InStatementCompletion, ref bool CancelKeypress)
        {
            //When a key is pressed within the editor, check to see if the key is a space, enter key, or a tab
            if ((Keypress == " ") || (Keypress[0] == 13) || (Keypress == "\t"))
            {
                //If it is one of our special keys, then we want to walk back from the current caret position to the beginning of the 
                //  line using edit points. With each character, we look to see if it is our dictionary of replacements. If so, then remove 
                //  that text, and replace it with the replacement value.
                EditPoint ep = Selection.ActivePoint.CreateEditPoint();
                EditPoint sp = ep.CreateEditPoint();

                //Prime the selection by getting one character:
                sp.CharLeft(1);

                while (true)
                {
                    //Get the text from the last character typed by the user until the current location of what we are searching for:
                    string txt = sp.GetText(ep);

                    if (_stringDictionary.ContainsKey(txt))
                    {
                        //If the text exists in the dictionary, then delete it from the buffer:
                        sp.Delete(txt.Length);
                        //Replace with the replacement text:
                        sp.Insert(_stringDictionary[txt]);
                        //And ignore the tab, space, or Enter key press:
                        CancelKeypress = true;
                        return; //Nothing more to do, we made our replacement.
                    }

                    //Keep walking backward
                    sp.CharLeft(1);
                    if ((ep.Line != sp.Line) || ((ep.DisplayColumn == 1) && (ep.Line == 1)))
                        return; //If the current location is at the beginning of the line, then there is nothing more to process, so return
                }
            }
        }

        private TextDocumentKeyPressEvents _keyPressEvents;
        StringDictionary _stringDictionary;	

		private DTE2 _applicationObject;
		private AddIn _addInInstance;
	}
}