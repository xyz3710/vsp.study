Copyright (c) Microsoft Corporation.  All rights reserved.

This sample shows how you can implement your own statement completion tool. This Add-in will load the accompanying .xml file, and when you type a specific keystroke sequence, that text is replaced with other text supplied from the xml file. For example, typing ?? into the text editor will automatically expand into the string System.Windows.Forms.MessageBox.Show("") and place the caret within the quotes. 

Once you load the sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin CSEditorGadgets.Connect
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

4. Copy abrev.xml to your My Documents\Visual Studio 2005\Addins directory