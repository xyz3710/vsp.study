Copyright (c) Microsoft Corporation.  All rights reserved.

RegExplore is an Add-in (written in VC++) that emulates the Registry Editor, but as a tool window within the environment. Some of the object model functionality demonstrated includes: 
1) Add-In 
2) ToolsOptions page 
3) ToolsOptions Properties 
4) Using a satellite dll for resources 
5) Adding custom toolbox items.


Once you load this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin RegExplore.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)
