//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RegExploreUI.rc
//
#define IDS_REGISTRYEXPLORER            1
#define IDS_DESCRIPTION                 2
#define IDS_FRIENDLYNAME                3
#define IDS_MYCOMPUTER                  4
#define IDS_TOOLBOXTABNAME              5
#define IDS_DATA                        6
#define IDS_VALUE                       7
#define IDS_NAME                        8
#define IDS_SUREDELETEVAL               9
#define IDS_SUREDELETEKEY               10
#define IDS_DEFAULT                     11
#define IDS_CANNOTRENDELDEFAULT         12
#define IDS_ZEROLENBINARY               13
#define IDS_COULDNOTCREATEVAL           14
#define IDS_COULDNOTOPENKEY             15
#define IDS_COULDNOTSETVAL              16
#define IDS_CANNOTEDIT                  17
#define IDS_COULDNOTREADVAL             18
#define IDS_TYPE                        19
#define IDS_NAMEEXISTKEY                20
#define IDS_CANNOTCREATENEWKEY          21
#define IDS_CANNOTCREATENEWVALUE        22
#define IDS_UNKTYPE                     23
#define IDS_UNSUPPTYPE                  24
#define IDS_ERROROPENKEY                25
#define IDS_NOTYPE                      26
#define IDS_NOVALUE                     27
#define IDS_RENAMETOEXIST               28
#define IDS_ENTERATLEASTONECHAR         29
#define IDS_GENERATINGVIEW              30
#define IDS_FINISHEDGENERATINGVIEW      31
#define IDS_NOHELPAVAIL                 32
#define IDB_CMDBARBITMAP                102

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
