//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AddIn.rc
//
#define IDS_PROJNAME                    100
#define IDR_ADDIN                       101
#define IDR_CONNECT                     102
#define IDB_REGEXPLORECTL               103
#define IDR_REGEXPLORECTL               104
#define IDD_REGEXPLORECTL               105
#define IDR_REGISTRYEXPLORER            106
#define IDB_REGEXPLOREROPTIONS          107
#define IDR_REGEXPLOREROPTIONS          108
#define IDD_REGEXPLOREROPTIONS          109
#define IDR_REGEXPLORERPROPERTIES       110
#define IDD_RENAMEDLG                   111
#define IDD_NEWKEYNAME                  112
#define IDD_NEWVALUENAMEDLG             113
#define IDR_KEYPROPERTIES               114
#define IDD_EDITSTRING                  115
#define IDD_DWORDEDIT                   116
#define IDC_PERSISTSHORTCUTS            202
#define IDI_STRINGDATA                  203
#define IDC_REGTREE                     203
#define IDI_BINARYDATA                  204
#define IDC_DATALIST                    204
#define IDI_CLOSEDFOLDER                205
#define IDC_MOUSEFRAME                  205
#define IDI_COMPUTER                    206
#define IDC_NAME                        206
#define IDI_OPENFOLDER                  207
#define IDC_NEWKEYNAME                  207
#define IDB_STATUSANIMATION             208
#define IDC_NEWVALUENAME                208
#define IDB_REGBITMAP                   209
#define IDC_VALUENAME                   209
#define IDB_BITMAP3                     210
#define IDC_VALUE                       210
#define IDB_BITMAP4                     211
#define IDB_BITMAP5                     212
#define IDC_DWORDVALUE                  213
#define IDR_CONTEXTMENUS                214
#define ID_LISTMENUNOSELECT_NEW_KEY     32769
#define ID_LISTMENUNOSELECT_NEW_STRING  32770
#define ID_LISTMENUNOSELECT_NEW_BINARY  32771
#define ID_LISTMENUNOSELECT_NEW_DWORD   32772
#define ID_LISTMENUSELECT_MODIFY        32773
#define ID_LISTMENUSELECT_DELETE        32774
#define ID_LISTMENUSELECT_RENAME        32775
#define ID_TREEMENU_EXPAND              32776
#define ID_TREEMENU_DELETE              32778
#define ID_TREEMENU_RENAME              32779
#define ID_TREEMENU_COPYKEYNAME         32780
#define ID_TREEMENUMYCOMPUTERSELECT_COLLAPSE 32781
#define ID_TREEMENUMYCOMPUTERSELECT_DISCONNECT 32782
#define ID_DEFAULT_REFRESH              32783
#define ID_DEFAULT_FIND                 32784
#define IDM_CREATESHORTCUT              32785

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        215
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         214
#define _APS_NEXT_SYMED_VALUE           117
#endif
#endif
