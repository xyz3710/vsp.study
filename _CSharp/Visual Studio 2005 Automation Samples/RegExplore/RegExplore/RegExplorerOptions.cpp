//Copyright (c) Microsoft Corporation.  All rights reserved.

// RegExplorerOptions.cpp : Implementation of CRegExplorerOptions
#include "stdafx.h"
#include "RegExplorerOptions.h"
#include "util.h"
#include "ResLoader.h"

// CRegExplorerOptions

STDMETHODIMP CRegExplorerOptions::OnAfterCreated(EnvDTE::_DTE * /*DTEObject*/)
{
	return S_OK;
}

STDMETHODIMP CRegExplorerOptions::GetProperties(IDispatch **PropertiesObject)
{
	HRESULT hr = S_OK;
	if(!m_pProperties)
	{
		IfFailGoCheck(CComObject<CRegExplorerProperties>::CreateInstance(&m_pProperties), m_pProperties);
		m_pProperties->AddRef();
	}
	hr = m_pProperties->QueryInterface(IID_IDispatch, (LPVOID*)PropertiesObject);
Error:
	return hr;
}

STDMETHODIMP CRegExplorerOptions::OnOK()
{
	BOOL fChecked = (::SendMessage(GetDlgItem(IDC_PERSISTSHORTCUTS), BM_GETCHECK, BST_CHECKED, 0) == BST_CHECKED);
	SaveOptions(fChecked);
	return S_OK;
}

STDMETHODIMP CRegExplorerOptions::OnCancel()
{
	return S_OK;
}

STDMETHODIMP CRegExplorerOptions::OnHelp()
{
	MessageBox(g_ResLoader.TLoadString(IDS_NOHELPAVAIL));
	return S_OK;
}

LRESULT CRegExplorerOptions::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(LoadOptions())
	{
		::SendMessage(GetDlgItem(IDC_PERSISTSHORTCUTS), BM_SETCHECK, BST_CHECKED, 0);
	}
	return 0;
}
