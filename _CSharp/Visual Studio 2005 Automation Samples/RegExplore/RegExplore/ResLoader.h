//Copyright (c) Microsoft Corporation.  All rights reserved.

#pragma once

class CResLoader
{
	HMODULE m_hInst;
public:
	CComVariant VarLoadString(UINT nResID)
	{
		TCHAR szString[2000];
		::LoadString(m_hInst, nResID, szString, 2000);
		return CComVariant(szString);
	}

	/*CString StrLoadString(UINT nResID)
	{
		TCHAR szString[2000];
		::LoadString(m_hInst, nResID, szString, 2000);
		return CString(szString);
	}*/

	CComBSTR LoadString(UINT nResID)
	{
		TCHAR szString[2000];
		::LoadString(m_hInst, nResID, szString, 2000);
		return CComBSTR(szString);
	}
	TCHAR *TLoadString(UINT nResID)
	{
		static TCHAR szString[2000];
		::LoadString(m_hInst, nResID, szString, 2000);
		return szString;
	}
	BOOL SetDLLPath(TCHAR *pszSatelliteDLLPath)
	{
		m_hInst = LoadLibrary(pszSatelliteDLLPath);
		if(m_hInst)
			return FALSE;
		return TRUE;
	}
	BOOL SetDLLPath(CComBSTR pszSatelliteDLLPath)
	{
		USES_CONVERSION;
		m_hInst = LoadLibrary(W2T(pszSatelliteDLLPath));
		if(m_hInst)
			return FALSE;
		return TRUE;
	}
	CResLoader()
	{
		m_hInst = NULL;
	}
	~CResLoader()
	{
		if(m_hInst)
			FreeLibrary(m_hInst);
	}
};

extern CResLoader g_ResLoader;