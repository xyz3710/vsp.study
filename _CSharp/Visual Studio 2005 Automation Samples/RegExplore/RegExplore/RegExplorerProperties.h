//Copyright (c) Microsoft Corporation.  All rights reserved.

// RegExplorerProperties.h : Declaration of the CRegExplorerProperties

#pragma once
#include "resource.h"       // main symbols

#include "addin.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CRegExplorerProperties

class ATL_NO_VTABLE CRegExplorerProperties :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRegExplorerProperties, &CLSID_RegExplorerProperties>,
	public ISupportErrorInfo,
	public IDispatchImpl<IRegExplorerProperties, &__uuidof(IRegExplorerProperties), &__uuidof(RegExploreLib), /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CRegExplorerProperties()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_REGEXPLORERPROPERTIES)


BEGIN_COM_MAP(CRegExplorerProperties)
	COM_INTERFACE_ENTRY(IRegExplorerProperties)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

public:
	STDMETHOD(get_PersistOptions)(VARIANT_BOOL* pVal);
public:
	STDMETHOD(put_PersistOptions)(VARIANT_BOOL newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(RegExplorerProperties), CRegExplorerProperties)
