//Copyright (c) Microsoft Corporation.  All rights reserved.

// KeyProperties.h : Declaration of the CKeyProperties

#pragma once
#include "resource.h"       // main symbols

#include "Addin.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CKeyProperties

class ATL_NO_VTABLE CKeyProperties :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CKeyProperties, &CLSID_KeyProperties>,
	public ISupportErrorInfo,
	public IDispatchImpl<IKeyProperties, &__uuidof(IKeyProperties), &__uuidof(RegExploreLib), /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CKeyProperties()
	{
	}

	void SetData(HKEY hKey)
	{
		RegQueryInfoKey(hKey, NULL, NULL, NULL, &m_dwSubKeys, &m_dwLongestSubkeyNameLength, NULL, &m_dwValueEntries, NULL, NULL, NULL, &m_LastWriteTime);
	}

	DWORD m_dwSubKeys;
	DWORD m_dwLongestSubkeyNameLength;
	DWORD m_dwValueEntries;
	FILETIME m_LastWriteTime;

DECLARE_REGISTRY_RESOURCEID(IDR_KEYPROPERTIES)


BEGIN_COM_MAP(CKeyProperties)
	COM_INTERFACE_ENTRY(IKeyProperties)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	// IKeyProperties
	STDMETHOD(get_LastWriteTime)(/*[out, retval]*/ DATE *pVal);
	STDMETHOD(get_Values)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_LongestSubkeyNameLength)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_NumberSubkeys)(/*[out, retval]*/ long *pVal);

};

OBJECT_ENTRY_AUTO(__uuidof(KeyProperties), CKeyProperties)
