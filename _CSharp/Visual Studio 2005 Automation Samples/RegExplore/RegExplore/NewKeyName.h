//Copyright (c) Microsoft Corporation.  All rights reserved.

// NewKeyName.h : Declaration of the CNewKeyName

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>


// CNewKeyName

class CNewKeyName : 
	public CAxDialogImpl<CNewKeyName>
{
public:
	CNewKeyName()
	{
	}

	~CNewKeyName()
	{
	}

	CString m_pszValue;

	enum { IDD = IDD_NEWKEYNAME };

BEGIN_MSG_MAP(CNewKeyName)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	CHAIN_MSG_MAP(CAxDialogImpl<CNewKeyName>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CNewKeyName>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		CenterWindow();
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		GetDlgItem(IDC_NEWKEYNAME).GetWindowText(m_pszValue);
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		EndDialog(wID);
		return 0;
	}
};


