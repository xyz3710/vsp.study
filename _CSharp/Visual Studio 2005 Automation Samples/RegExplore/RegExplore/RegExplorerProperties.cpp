//Copyright (c) Microsoft Corporation.  All rights reserved.

// RegExplorerProperties.cpp : Implementation of CRegExplorerProperties

#include "stdafx.h"
#include "RegExplorerProperties.h"


// CRegExplorerProperties

STDMETHODIMP CRegExplorerProperties::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&__uuidof(IRegExplorerProperties)
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CRegExplorerProperties::get_PersistOptions(VARIANT_BOOL* pVal)
{
	// TODO: Add your implementation code here

	return S_OK;
}

STDMETHODIMP CRegExplorerProperties::put_PersistOptions(VARIANT_BOOL newVal)
{
	// TODO: Add your implementation code here

	return S_OK;
}
