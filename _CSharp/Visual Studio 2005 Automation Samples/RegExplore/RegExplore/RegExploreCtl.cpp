//Copyright (c) Microsoft Corporation.  All rights reserved.

// RegExploreCtl.cpp : Implementation of CRegExploreCtl
#include "stdafx.h"
#include "RegExploreCtl.h"
#include "util.h"
#include "shlobj.h"
#include "ResLoader.h"
#include "NewKeyName.h"
#include "NewValueNameDlg.h"
#include "KeyProperties.h"
#include "RenameDlg.h"
#include "EditString.h"
#include "DWORDEdit.h"

// CRegExploreCtl

CRegExploreCtl *g_CRegExploreCtl;
extern CResLoader g_ResLoader;
extern CComBSTR bstrGUIDTool;

LRESULT CRegExploreCtl::OnTreeMenu_Delete(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if (MessageBox(g_ResLoader.TLoadString(IDS_SUREDELETEKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_YESNO) == IDYES)
	{
		//If running on NT, RegDeleteKey will not delete the key if it has
		//	subkeys. So test if running on NT, if so recursively delete. If
		//	not, just delete...
		TCHAR *pszKey;
		TCHAR *pszRoot;

		HKEY hRootKey;
		CString pszConstructedRegKey;

		GetKey(m_hTree, hTreeCurrentHilighted, &hRootKey, pszConstructedRegKey);

		StripKeys(pszConstructedRegKey, &pszRoot, &pszKey);

		RecursiveDeleteKey(hRootKey, pszConstructedRegKey);
		TreeView_DeleteItem(m_hTree, hTreeCurrentHilighted);

		delete []pszRoot;
		delete []pszKey;
	}
	return 0;
}


LRESULT CRegExploreCtl::OnTreeMenu_CreateShortcut(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CComPtr<EnvDTE::Windows> pWindows;
	CComPtr<EnvDTE::Window> pWindow;
	CComQIPtr<EnvDTE::TaskList> pTaskList;
	CComPtr<IDispatch> pDisp;
	CComPtr<EnvDTE::TaskItems> pTaskItems;
	CComPtr<EnvDTE::TaskItem> pTaskItem;
	CComVariant varPic;
	long lTemp;
	CString pszConstructedRegKey;
	HKEY hRootKey;
	
	GetKey(m_hTree, hTreeCurrentHilighted, &hRootKey, pszConstructedRegKey);
	CString strTemp;
	strTemp.Format("%s\\%s", g_ResLoader.TLoadString(IDS_MYCOMPUTER), GetRootNameFromKey(hRootKey));
	if (pszConstructedRegKey.GetLength() > 0)
	{
		strTemp.AppendChar('\\');
		strTemp.Append(pszConstructedRegKey);
	}
	m_pDTE->get_Windows(&pWindows);
	pWindows->Item(CComVariant(EnvDTE::vsWindowKindTaskList), &pWindow);  //GUID == GUID for TaskList
	pWindow->get_Object(&pDisp);
	pTaskList = pDisp;
	pTaskList->get_TaskItems(&pTaskItems);

	CComPtr<IPictureDisp> pPictureDisp;
	PICTDESC pd;
	pd.cbSizeofstruct=sizeof(PICTDESC);
	pd.picType=PICTYPE_BITMAP;
	pd.bmp.hbitmap = LoadResourceBitmap(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDB_REGBITMAP), &pd.bmp.hpal);
	OleCreatePictureIndirect(&pd, IID_IPictureDisp, FALSE, (LPVOID*)&pPictureDisp);
	pPictureDisp->QueryInterface(IID_IUnknown, (LPVOID*)&varPic.punkVal);

	varPic.vt = VT_UNKNOWN;
	pTaskItems->Add(CComBSTR("Registry Shortcuts"), CComBSTR("Registry Shortcut"), CComBSTR(strTemp), EnvDTE::vsTaskPriorityMedium, varPic, VARIANT_FALSE, CComBSTR(""), 0, VARIANT_TRUE, VARIANT_TRUE, &pTaskItem);
	lTemp = (long)pTaskItem.p;
	ArrayAddedItems.Add(lTemp);
	return 0;
}

LRESULT CRegExploreCtl::OnTreeMenu_Rename(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CRenameDlg RenameDlg;
	HKEY hRootKey;
	CString pszConstructedRegKey;

	GetKey(m_hTree, hTreeCurrentHilighted, &hRootKey, pszConstructedRegKey);
	CString pszName = pszConstructedRegKey.Left(pszConstructedRegKey.Find('\\'));
	if (pszName.GetLength() == 0)
		RenameDlg.m_szName = pszConstructedRegKey;
	else
		RenameDlg.m_szName = CharNext(pszName);
	
	if (RenameDlg.DoModal() == IDOK)
	{
		//TODO: Change the name of the key.
	}
	return 0;
}

LRESULT CRegExploreCtl::OnTreeMenu_CopyKeyName(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	HKEY hRootKey;
	CString pszConstructedRegKey;
	CString pszTemp;

	GetKey(m_hTree, hTreeCurrentHilighted, &hRootKey, pszConstructedRegKey);

	pszTemp.Format("%s\\%s", g_ResLoader.TLoadString(IDS_MYCOMPUTER), GetRootNameFromKey(hRootKey));
	if (pszConstructedRegKey.GetLength() > 0)
	{
		pszTemp.AppendChar('\\');
		pszTemp.Append(pszConstructedRegKey);
	}

	CComPtr<EnvDTE::Windows> pWindows;
	CComPtr<IDispatch> pDisp;
	CComPtr<EnvDTE::ToolBoxTabs> pTabs;
	CComQIPtr<EnvDTE::ToolBox> pToolbox;
	CComPtr<EnvDTE::ToolBoxTab> pTab;
	CComPtr<EnvDTE::Window> pWindow;
	CComPtr<EnvDTE::ToolBoxItems> pItems;
	CComPtr<EnvDTE::ToolBoxItem> pItem;

	m_pDTE->get_Windows(&pWindows);
	pWindows->Item(CComVariant(EnvDTE::vsWindowKindToolbox), &pWindow);
	pWindow->get_Object(&pDisp);
	pToolbox = pDisp;
	pToolbox->get_ToolBoxTabs(&pTabs);
	if(FAILED(pTabs->Item(CComVariant(g_ResLoader.TLoadString(IDS_TOOLBOXTABNAME)), &pTab)))  //Tab we want is not available, need to create it
	{
		pTabs->Add(g_ResLoader.LoadString(IDS_TOOLBOXTABNAME), &pTab);
	}
	pTab->get_ToolBoxItems(&pItems);
	pItems->Add(CComBSTR(pszTemp), CComVariant(pszTemp), EnvDTE::vsToolBoxItemFormatText, &pItem);

	return 0;
}

LRESULT CRegExploreCtl::OnTreeMenuMyComputerSelect_Collapse(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	TreeView_Expand(m_hTree, hTreeCurrentHilighted, TVE_TOGGLE);
	return 0;
}

LRESULT CRegExploreCtl::OnRefresh(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	LVCOLUMN lvc;
	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = g_ResLoader.TLoadString(IDS_DATA);
	lvc.cx = 200;
	ListView_InsertColumn(m_hList, 0, &lvc);
	lvc.pszText = g_ResLoader.TLoadString(IDS_NAME);
	ListView_InsertColumn(m_hList, 0, &lvc);

	TVINSERTSTRUCT lpis;
	lpis.hParent = TVI_ROOT;
	lpis.hInsertAfter = TVI_LAST;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.lParam = -1;
	lpis.item.iImage = nComputer;
	lpis.item.iSelectedImage = nComputer;
	lpis.item.pszText = g_ResLoader.TLoadString(IDS_MYCOMPUTER);
	HTREEITEM hItem = hTreeCurrentHilighted = TreeView_InsertItem(m_hTree, &lpis);

	//Insert the nodes that we know must exist. An item is inserted as a child
	//	so that there will be a + next to the tree item. The first time the node
	//	is expanded, we need to remove this item then look to see if there is any
	//	keys below it.

	//lpis.item.mask = TVIF_PARAM|TVIF_TEXT;
	lpis.item.lParam = 0;
	lpis.item.iImage = nClosedFolder;
	lpis.item.iSelectedImage = nOpenFolder;
	lpis.hParent = hItem;
	lpis.item.pszText = GetRootNameFromKey(HKEY_CLASSES_ROOT).GetBuffer();
	HTREEITEM hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.pszText = _T("");
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.pszText = GetRootNameFromKey(HKEY_CURRENT_CONFIG).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.pszText = _T("");
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.pszText = GetRootNameFromKey(HKEY_CURRENT_USER).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.pszText = _T("");
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.pszText = GetRootNameFromKey(HKEY_LOCAL_MACHINE).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.pszText = _T("");
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.pszText = GetRootNameFromKey(HKEY_USERS).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.pszText = _T("");
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.item.pszText = GetRootNameFromKey(HKEY_PERFORMANCE_DATA).GetBuffer();

	lpis.hParent = hItem;
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.pszText = _T("");
	TreeView_InsertItem(m_hTree, &lpis);

	TreeView_Expand(m_hTree, hTreeCurrentHilighted, TVE_TOGGLE);
	return 0;
}

LRESULT CRegExploreCtl::OnFind(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MessageBox(_T("NYI"));
	return 0;
}

BOOL CRegExploreCtl::IsRootSelected()
{
	TVITEM tvi;
	tvi.mask = TVIF_PARAM;
	HTREEITEM hParent = TreeView_GetParent(m_hTree, hTreeCurrentHilighted);
	hParent = TreeView_GetParent(m_hTree, hParent);

	return ((hTreeCurrentHilighted) && (!hParent)) ? TRUE : FALSE;
}

BOOL CRegExploreCtl::IsComputerSelected()
{
	TVITEM tvi;
	tvi.mask = TVIF_PARAM;
	tvi.hItem = hTreeCurrentHilighted;
	TreeView_GetItem(m_hTree, &tvi);
	return (tvi.lParam == -1) ? TRUE : FALSE;
}


void CRegExploreCtl::ModifyValue()
{
	TCHAR *pszName = new TCHAR[MAX_PATH];
	TCHAR *pszValue = new TCHAR[MAX_PATH];
	ListView_GetItemText(m_hList, nListCurrentHilighted, 0, pszName, MAX_PATH);
	CString pszKeyName;
	HKEY hKey;
	HKEY hKeyValue;
	DWORD dwSize = 2000;

	GetKey(m_hTree, hTreeCurrentHilighted, &hKey, pszKeyName);
	if(RegOpenKeyEx(hKey, pszKeyName, 0, KEY_ALL_ACCESS, &hKeyValue) == ERROR_SUCCESS)
	{
		DWORD dwType;
		LONG lQueryRetVal;
		if(!_tcsicmp(pszName, g_ResLoader.TLoadString(IDS_DEFAULT)))
		{
			//On NT, a key can be without a default value, theis fails if the default value is not there, need to set one
			lQueryRetVal = RegQueryValueEx(hKeyValue, NULL, 0, &dwType, (LPBYTE)pszValue, &dwSize);
			if(lQueryRetVal != ERROR_SUCCESS)
			{
				CEditString EditString(pszName, _T(""));
				if(EditString.DoModal() == IDOK)
				{
					DWORD dwSize = (DWORD)(EditString.m_pszValue.GetLength() + sizeof(TCHAR));
					if(RegSetValueEx(hKeyValue, NULL, 0, REG_SZ, (LPBYTE)EditString.m_pszValue.GetBuffer(), dwSize) != ERROR_SUCCESS)
					{
						MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTSETVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
					}
					else
					{
						ListView_SetItemText(m_hList, nListCurrentHilighted, 1, EditString.m_pszValue.GetBuffer());
						ListView_SetItemText(m_hList, nListCurrentHilighted, 2, GetDataTypeName(REG_SZ).GetBuffer());
					}
				}
				goto Exit;
			}
		}
		else
			lQueryRetVal = RegQueryValueEx(hKeyValue, pszName, 0, &dwType, (LPBYTE)pszValue, &dwSize);
		if(lQueryRetVal == ERROR_SUCCESS)
		{
			if(dwType == REG_SZ)
			{
				CEditString EditString(pszName, pszValue);
				if (EditString.DoModal() == IDOK)
				{
					DWORD dwSize = (DWORD)_tcslen(EditString.m_pszValue) + sizeof(TCHAR);
					if(!_tcsicmp(pszName, g_ResLoader.TLoadString(IDS_DEFAULT)))
						lQueryRetVal = RegSetValueEx(hKeyValue, NULL, 0, REG_SZ, (LPBYTE)EditString.m_pszValue.GetBuffer(), dwSize);
					else
						lQueryRetVal = RegSetValueEx(hKeyValue, pszName, 0, REG_SZ, (LPBYTE)EditString.m_pszValue.GetBuffer(), dwSize);
					if(lQueryRetVal == ERROR_SUCCESS)
					{
						ListView_SetItemText(m_hList, nListCurrentHilighted, 1, EditString.m_pszValue.GetBuffer());
					}
					else
						MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTSETVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
				}
			}
			else if(dwType == REG_DWORD)
			{
				DWORD dwValue = (DWORD)*pszValue;
				CDWORDEdit DWORDEdit(pszName, dwValue);
				if (DWORDEdit.DoModal() == IDOK)
				{
					DWORD dwSize = sizeof(DWORD);
					if(!_tcsicmp(pszName, g_ResLoader.TLoadString(IDS_DEFAULT)))
						lQueryRetVal = RegSetValueEx(hKeyValue, NULL, 0, REG_DWORD, (LPBYTE)&DWORDEdit.m_dwValue, dwSize);
					else
						lQueryRetVal = RegSetValueEx(hKeyValue, pszName, 0, REG_DWORD, (LPBYTE)&DWORDEdit.m_dwValue, dwSize);
					if(lQueryRetVal == ERROR_SUCCESS)
					{
						TCHAR szTemp[20];
						wsprintf(szTemp, _T("%d"), DWORDEdit.m_dwValue);
						ListView_SetItemText(m_hList, nListCurrentHilighted, 1, szTemp);
					}
					else
						MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTSETVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
				}
			}
			else
			{
				MessageBox(g_ResLoader.TLoadString(IDS_CANNOTEDIT), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
			}
		}
		else
			MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTREADVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
		RegCloseKey(hKeyValue);
	}
	else
		MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTREADVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
Exit:
	delete []pszName;
	delete []pszValue;
}

void CRegExploreCtl::GetKey(HWND m_hTree, HTREEITEM hItem, HKEY *hKey, CString &strKey)
{
	strKey.Empty();
	while(hItem)
	{
		HTREEITEM hTemp1, hTemp2;
		TVITEM tvi;
		tvi.mask = TVIF_TEXT;
		tvi.pszText = new TCHAR[MAX_PATH];
		tvi.cchTextMax = MAX_PATH;
		tvi.hItem = hItem;
		TreeView_GetItem(m_hTree, &tvi);

		hTemp1 = TreeView_GetParent(m_hTree, hItem);
		hTemp2 = TreeView_GetParent(m_hTree, hTemp1);
		if (!hTemp2)
		{
			TreeView_GetItem(m_hTree, &tvi);
			*hKey = GetKeyFromRoot(tvi.pszText);
			delete []tvi.pszText;
			return;
		}
		else
		{
			if (strKey.GetLength() == 0)
			{
				strKey = tvi.pszText;
			}
			else
			{
				strKey.Format("%s\\%s", tvi.pszText, strKey);
			}
			hItem = TreeView_GetParent(m_hTree, hItem);
		}
		delete []tvi.pszText;
	}
}

int CRegExploreCtl::GetImageOfType(DWORD dwType)
{
	switch (dwType)
	{
	case REG_SZ:
		return nStringData;
		/*case REG_DWORD:
		case REG_BINARY:
		case REG_DWORD_BIG_ENDIAN:
		case REG_EXPAND_SZ:
		case REG_LINK:
		case REG_MULTI_SZ:
		case REG_NONE:
		case REG_RESOURCE_LIST:*/
	default:
		return nBinaryData;
	}
}

STDMETHODIMP CRegExploreCtl::SetParent(IUnknown* pUnk)
{
	m_pDTE = pUnk;
	if(!pUnk)
		return S_OK;
	g_CRegExploreCtl = this;
	if (LoadOptions())
	{
		USES_CONVERSION;
		CComPtr<IStorage> pStgRoot;
		CComPtr<IStream> pStreamShortcuts;
		CComPtr<EnvDTE::Windows> pWindows;
		CComPtr<IDispatch> pDisp;
		CComPtr<EnvDTE::TaskItems> pTaskItems;
		CComQIPtr<EnvDTE::TaskList> pTaskList;
		CComPtr<EnvDTE::Window> pWindow;
		TCHAR szPath[MAX_PATH];
		CComVariant varPic;

		m_pDTE->get_Windows(&pWindows);
		pWindows->Item(CComVariant(EnvDTE::vsWindowKindTaskList), &pWindow);
		pWindow->get_Object(&pDisp);
		pTaskList = pDisp;
		pTaskList->get_TaskItems(&pTaskItems);

		CComPtr<IPictureDisp> pPictureDisp;
		PICTDESC pd;
		pd.cbSizeofstruct=sizeof(PICTDESC);
		pd.picType=PICTYPE_BITMAP;
		pd.bmp.hbitmap = LoadResourceBitmap(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDB_REGBITMAP), &pd.bmp.hpal);
		OleCreatePictureIndirect(&pd, IID_IPictureDisp, FALSE, (LPVOID*)&pPictureDisp);
		pPictureDisp->QueryInterface(IID_IUnknown, (LPVOID*)&varPic.punkVal);
		varPic.vt = VT_UNKNOWN;

		//Load any shortcuts we may have created:
		SHGetSpecialFolderPath(NULL, szPath, CSIDL_APPDATA, TRUE);
		_tcsncat(szPath, _T("\\RegistryExplorer.dat"), MAX_PATH);
		if(SUCCEEDED(StgOpenStorage(T2W(szPath), NULL, STGM_READ | STGM_SHARE_EXCLUSIVE, NULL, 0, &pStgRoot)))
		{
			if SUCCEEDED(pStgRoot->OpenStream(L"Shortcuts", NULL, STGM_READ | STGM_SHARE_EXCLUSIVE, NULL, &pStreamShortcuts)) 
			{
				DWORD dwCount = 0;
				pStreamShortcuts->Read(&dwCount, sizeof(DWORD), NULL);
				for (DWORD i = 0 ; i < dwCount ; i++)
				{
					long lTemp;
					CComPtr<EnvDTE::TaskItem> pTaskItem;
					CComBSTR bstrShortcut;
					bstrShortcut.ReadFromStream(pStreamShortcuts);
					pTaskItems->Add(CComBSTR("Registry Shortcut"), CComBSTR("Registry Shortcut"), bstrShortcut, EnvDTE::vsTaskPriorityMedium, varPic/*CComVariant(vsTaskIconShortcut)*/, VARIANT_FALSE, CComBSTR(""), 0, VARIANT_TRUE, VARIANT_TRUE, &pTaskItem);
					lTemp = (long)pTaskItem.p;
					ArrayAddedItems.Add(lTemp);
				}
			}
		}
	}
	return S_OK;
}

STDMETHODIMP CRegExploreCtl::Navigate(BSTR Location)
{
	SelectItem(Location);
	return S_OK;
}

void CRegExploreCtl::SelectItem(CComBSTR bstrItemText)
{
	USES_CONVERSION;
	TCHAR szCurrentChunk[2000];
	TCHAR *pszCurrentLoc = szCurrentChunk;
	TCHAR *pszTemp;
	HTREEITEM hRoot = TreeView_GetRoot(m_hTree);
	TreeView_SelectItem(m_hTree, hRoot);
	_tcsncpy(szCurrentChunk, W2T(bstrItemText), 2000);

	//Move past 'My Computer'
	pszCurrentLoc = _tcschr(pszCurrentLoc, '\\');
	if(pszCurrentLoc)
	{
		pszCurrentLoc = CharNext(pszCurrentLoc);
		pszTemp = _tcschr(pszCurrentLoc, '\\');
		if(pszTemp)
		{
			*pszTemp = 0;
			pszTemp = CharNext(pszTemp);
		}
		HTREEITEM htic = TreeView_GetChild(m_hTree, hRoot);
		while(htic)
		{
			TCHAR szTextItem[MAX_PATH];
			TVITEM tvi;
			tvi.mask = TVIF_TEXT;
			tvi.pszText = szTextItem;
			tvi.cchTextMax = MAX_PATH;
			tvi.hItem = htic;
			TreeView_GetItem(m_hTree, &tvi);
			if(!_tcscmp(tvi.pszText, pszCurrentLoc))
			{
				TreeView_Expand(m_hTree, htic, TVE_EXPAND);
				TreeView_SelectItem(m_hTree, htic);
				_tcsncpy(szCurrentChunk, W2T(bstrItemText), 2000);
				if(!pszTemp)
					break;
				pszTemp = _tcschr(pszTemp, '\\');

				pszCurrentLoc = CharNext(pszTemp);
				pszTemp = _tcschr(pszCurrentLoc, '\\');
				if(pszTemp)
					*pszTemp = 0;
				htic = TreeView_GetChild(m_hTree, htic);
			}
			else
			{
				htic = TreeView_GetNextSibling(m_hTree, htic);
			}
		}
		if(!htic)
		{
			//TODO: Error, Item not found. Ask to remove
		}

	}
}


LRESULT CRegExploreCtl::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CComPtr<EnvDTE::Events> pEvents;

	//Connect events on the task list:
	HRESULT hr = m_pDTE->get_Events(&pEvents);

	hr = pEvents->get_TaskListEvents(CComBSTR(""), (EnvDTE::_TaskListEvents**)&pTLE);
	m_TaskListEventSink.SetOwner(this);
	m_TaskListEventSink.DispEventAdvise((IUnknown*)pTLE.p);

	m_hTree = GetDlgItem(IDC_REGTREE);
	m_hList = GetDlgItem(IDC_DATALIST);

	ListView_SetExtendedListViewStyleEx(m_hList, LVS_EX_FULLROWSELECT|LVS_EX_HEADERDRAGDROP, LVS_EX_FULLROWSELECT|LVS_EX_HEADERDRAGDROP);

	HIMAGELIST hImageList = ImageList_Create(16, 16, ILC_MASK, 5, 1);
	HIMAGELIST hImageList2 = ImageList_Create(16, 16, ILC_MASK, 2, 1);

	nBinaryData = ImageList_AddIcon(hImageList2, LoadIcon(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDI_BINARYDATA)));
	nStringData = ImageList_AddIcon(hImageList2, LoadIcon(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDI_STRINGDATA)));

	nClosedFolder = ImageList_AddIcon(hImageList, LoadIcon(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDI_CLOSEDFOLDER)));
	nComputer = ImageList_AddIcon(hImageList, LoadIcon(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDI_COMPUTER)));
	nOpenFolder = ImageList_AddIcon(hImageList, LoadIcon(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDI_OPENFOLDER)));

	ListView_SetImageList(m_hList, hImageList2, LVSIL_SMALL);
	TreeView_SetImageList(m_hTree, hImageList, TVSIL_NORMAL);

	LVCOLUMN lvc;
	lvc.mask = LVCF_TEXT|LVCF_WIDTH;
	lvc.pszText = g_ResLoader.TLoadString(IDS_TYPE);
	lvc.cx = 200;
	ListView_InsertColumn(m_hList, 0, &lvc);
	lvc.pszText = g_ResLoader.TLoadString(IDS_DATA);
	ListView_InsertColumn(m_hList, 0, &lvc);
	lvc.pszText = g_ResLoader.TLoadString(IDS_NAME);
	ListView_InsertColumn(m_hList, 0, &lvc);

	TVINSERTSTRUCT lpis;
	lpis.hParent = TVI_ROOT;
	lpis.hInsertAfter = TVI_LAST;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.lParam = -1;
	lpis.item.iImage = nComputer;
	lpis.item.iSelectedImage = nComputer;
	lpis.item.pszText = g_ResLoader.TLoadString(IDS_MYCOMPUTER);
	HTREEITEM hItem = hTreeCurrentHilighted = TreeView_InsertItem(m_hTree, &lpis);

	//Insert the nodes that we know must exist. An item is inserted as a child
	//	so that there will be a + next to the tree item. The first time the node
	//	is expanded, we need to remove this item then look to see if there is any
	//	keys below it.

	//lpis.item.mask = TVIF_PARAM|TVIF_TEXT;
	lpis.item.lParam = 0;
	lpis.item.iImage = nClosedFolder;
	lpis.item.iSelectedImage = nOpenFolder;
	lpis.hParent = hItem;
	lpis.item.pszText = GetRootNameFromKey(HKEY_CLASSES_ROOT).GetBuffer();
	HTREEITEM hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.item.mask = TVIF_PARAM;
	lpis.hParent = hItemPart;
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.pszText = GetRootNameFromKey(HKEY_CURRENT_CONFIG).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.item.mask = TVIF_PARAM;
	lpis.hParent = hItemPart;
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.pszText = GetRootNameFromKey(HKEY_CURRENT_USER).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.mask = TVIF_PARAM;
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.pszText = GetRootNameFromKey(HKEY_LOCAL_MACHINE).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.mask = TVIF_PARAM;
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.hParent = hItem;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.pszText = GetRootNameFromKey(HKEY_USERS).GetBuffer();
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.mask = TVIF_PARAM;
	TreeView_InsertItem(m_hTree, &lpis);

	lpis.item.pszText = GetRootNameFromKey(HKEY_PERFORMANCE_DATA).GetBuffer();
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.hParent = hItem;
	hItemPart = TreeView_InsertItem(m_hTree, &lpis);
	lpis.hParent = hItemPart;
	lpis.item.mask = TVIF_PARAM;
	TreeView_InsertItem(m_hTree, &lpis);

	TreeView_Expand(m_hTree, hTreeCurrentHilighted, TVE_TOGGLE);

	return 0;
}

LRESULT CRegExploreCtl::OnTvnItemexpandingRegtree(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	LPNMTREEVIEW pnmtv = (LPNMTREEVIEW) pNMHDR;
	if (pnmtv->action != 2)
		return 0;
	SetCursor(LoadCursor(NULL, IDC_WAIT));

	//if pnmtv->lParam == -1 then the root node is being expanded, and we have allready populated it.
	//if pnmtv->lParam == 1 then the node that has allready been populated is exapanding.
	//if pnmtv->lParam == 0 then the node needs populating.
	if (pnmtv->itemNew.lParam == 0)	//Needs populating.
	{
		CComVariant varPic;
		CComPtr<EnvDTE::StatusBar> pStatusBar;
		CString szConstructedRegKey;
		TCHAR szKeyName[MAX_PATH];
		HTREEITEM hItem = TreeView_GetChild(m_hTree, pnmtv->itemNew.hItem);
		TreeView_DeleteItem(m_hTree, hItem);

		if(SUCCEEDED(m_pDTE->get_StatusBar(&pStatusBar)))
		{
			//Create an animation in the status bar (this may take a while):
			CComPtr<IPictureDisp> pPictureDisp;
			PICTDESC pd;
			pd.cbSizeofstruct=sizeof(PICTDESC);
			pd.picType=PICTYPE_BITMAP;
			pd.bmp.hbitmap = LoadResourceBitmap(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDB_STATUSANIMATION), &pd.bmp.hpal);
			OleCreatePictureIndirect(&pd, IID_IPictureDisp, FALSE, (LPVOID*)&pPictureDisp);
			pPictureDisp->QueryInterface(IID_IUnknown, (LPVOID*)&varPic.punkVal);
			varPic.vt = VT_UNKNOWN;
			pStatusBar->put_Text(g_ResLoader.LoadString(IDS_GENERATINGVIEW));
			pStatusBar->Animate(VARIANT_TRUE, varPic);
		}

		HKEY hRootKey;
		GetKey(m_hTree, pnmtv->itemNew.hItem, &hRootKey, szConstructedRegKey);

		HKEY hEnumKey;
		if (RegOpenKey(hRootKey, szConstructedRegKey, &hEnumKey) == ERROR_SUCCESS)
		{
			DWORD dwIndex = 0;
			TVINSERTSTRUCT lpis;
			lpis.item.iImage = nClosedFolder;
			lpis.item.iSelectedImage = nOpenFolder;
			lpis.item.lParam = 0;
			lpis.hInsertAfter = TVI_LAST;
			while (RegEnumKey(hEnumKey, dwIndex, szKeyName, MAX_PATH) == ERROR_SUCCESS)
			{
				HKEY hKeyTemp;
				DWORD dwSubKeys;
				lpis.hParent = pnmtv->itemNew.hItem;
				lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
				lpis.item.pszText = szKeyName;
				HTREEITEM hItem = TreeView_InsertItem(m_hTree, &lpis);
				if(RegOpenKeyEx(hEnumKey, szKeyName, 0, KEY_READ, &hKeyTemp) == ERROR_SUCCESS)
				{
					if(RegQueryInfoKey(hKeyTemp, NULL, NULL, NULL, &dwSubKeys, NULL, NULL, NULL, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
					{
						if(dwSubKeys > 0)
						{
							lpis.hParent = hItem;
							lpis.item.mask = TVIF_PARAM;
							TreeView_InsertItem(m_hTree, &lpis);
						}
						else
						{
							TVITEM tvi;
							tvi.mask = TVIF_PARAM;
							tvi.hItem = pnmtv->itemNew.hItem;
							TreeView_GetItem(m_hTree, &tvi);
							tvi.lParam = 1;
							TreeView_SetItem(m_hTree, &tvi);
						}
					}
					else
					{
						lpis.hParent = hItem;
						lpis.item.mask = TVIF_PARAM;
						TreeView_InsertItem(m_hTree, &lpis);
					}
					RegCloseKey(hKeyTemp);
				}
				else
				{
					lpis.hParent = hItem;
					lpis.item.mask = TVIF_PARAM;
					TreeView_InsertItem(m_hTree, &lpis);
				}
				dwIndex++;
			}
			RegCloseKey(hEnumKey);
		}
		pStatusBar->put_Text(g_ResLoader.LoadString(IDS_FINISHEDGENERATINGVIEW));
		pStatusBar->Animate(VARIANT_FALSE, varPic);
	}
	SetCursor(LoadCursor(NULL, IDC_ARROW));
	return 0;
}

LRESULT CRegExploreCtl::OnTvnSelchangedRegtree(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	LPNMTREEVIEW pnmtv = (LPNMTREEVIEW) pNMHDR;
	hTreeCurrentHilighted = pnmtv->itemNew.hItem;
	ListView_DeleteAllItems(m_hList);
	TCHAR szValueName[MAX_PATH];
	DWORD dwNameReceived = MAX_PATH;
	if (pnmtv->itemNew.lParam != -1)	//Dont want info about the 'My Computer' node.
	{
		HKEY hRootKey;
		CString pszConstructedRegKey;
		CString pszTemp;
		
		GetKey(m_hTree, pnmtv->itemNew.hItem, &hRootKey, pszConstructedRegKey);

		pszTemp.Format("%s\\%s", g_ResLoader.TLoadString(IDS_MYCOMPUTER), GetRootNameFromKey(hRootKey));
		if (pszConstructedRegKey.GetLength() > 0)
		{
			pszTemp.AppendChar('\\');
			pszTemp.Append(pszConstructedRegKey);
		}

		CComPtr<EnvDTE::StatusBar> pStatusBar;
		m_pDTE->get_StatusBar(&pStatusBar);
		pStatusBar->put_Text(CComBSTR(pszTemp));

		HKEY hEnumKey;
		DWORD dwSize;
		DWORD dwType;
		BOOL fDefaultFound = FALSE;

		if (RegOpenKey(hRootKey, pszConstructedRegKey, &hEnumKey) == ERROR_SUCCESS)
		{
			DWORD dwIndex = 0;
			//The key that is selected is open. Create the Selection object, push it to the Property window.
			CComObject<CKeyProperties> *pKeyProperties;
			if(SUCCEEDED(CComObject<CKeyProperties>::CreateInstance(&pKeyProperties)))
			{
				CComPtr<IDispatch> pDisp;
				CComSafeArray<VARIANT> sa((ULONG)0);
				CComVariant var;
				CComPtr<EnvDTE::Window> pWindow;
				CComPtr<EnvDTE::Windows> pWindows;

				pKeyProperties->SetData(hEnumKey);
				pKeyProperties->QueryInterface(IID_IDispatch, (LPVOID*)&pDisp);

				var = pDisp;
				sa.Add(var);

				m_pDTE->get_Windows(&pWindows);
				pWindows->Item(CComVariant(bstrGUIDTool), &pWindow);
				if(pWindow.p)
				{
					pWindow->SetSelectionContainer(sa.GetSafeArrayPtr());
				}
			}

			while(RegEnumValue(hEnumKey, dwIndex, szValueName, &dwNameReceived, 0, &dwType, NULL, NULL) == ERROR_SUCCESS) //TODO: Get the value
			{
				dwIndex++;
				dwNameReceived = MAX_PATH;

				if(RegQueryValueEx(hEnumKey, szValueName, NULL, &dwType, NULL, &dwSize) == ERROR_SUCCESS)
				{
					LVITEM lvi;
					lvi.mask = LVIF_TEXT|LVIF_IMAGE;

					lvi.iItem = dwIndex;
					lvi.iSubItem = 0;
					lvi.iImage = GetImageOfType(dwType);
					int nItem;
					if (!szValueName[0])
					{
						lvi.pszText = szValueName;
						nItem = ListView_InsertItem(m_hList, &lvi); 
					}
					else
					{
						lvi.iImage = nStringData;
						lvi.iItem = 0;
						lvi.pszText = g_ResLoader.TLoadString(IDS_DEFAULT);
						nItem = ListView_InsertItem(m_hList, &lvi); 
						fDefaultFound = TRUE;
					}

					LPBYTE byteValue = new BYTE[dwSize];
					lvi.mask = LVIF_TEXT;
					lvi.iItem = nItem;
					lvi.iSubItem = 1;

					RegQueryValueEx(hEnumKey, szValueName, NULL, &dwType, byteValue, &dwSize);

					lvi.pszText = GetStringOfValue(byteValue, dwType, dwSize).GetBuffer();
					ListView_SetItem(m_hList, &lvi);
					lvi.iSubItem = 2;
					lvi.pszText = GetDataTypeName(dwType).GetBuffer();
					ListView_SetItem(m_hList, &lvi);
					delete []byteValue;
				}
			}
			if (!fDefaultFound)
			{
				LVITEM lvi;
				lvi.mask = LVIF_TEXT|LVIF_IMAGE;
				lvi.iItem = 0;
				lvi.iSubItem = 0;
				lvi.iImage = nStringData;
				lvi.pszText = g_ResLoader.TLoadString(IDS_DEFAULT);
				int nItem = ListView_InsertItem(m_hList, &lvi); 
				lvi.mask = LVIF_TEXT|LVIF_IMAGE;
				lvi.iItem = nItem;
				lvi.iImage = nStringData;
				lvi.iSubItem = 1;
				lvi.pszText = g_ResLoader.TLoadString(IDS_NOVALUE);
				ListView_SetItem(m_hList, &lvi);
				lvi.iSubItem = 2;
				lvi.pszText = g_ResLoader.TLoadString(IDS_NOTYPE);
				ListView_SetItem(m_hList, &lvi);
			}
			RegCloseKey(hRootKey);
		}
		else
		{
			MessageBox(g_ResLoader.TLoadString(IDS_ERROROPENKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_OK|MB_ICONSTOP);
		}
	}
	else
	{
		CComPtr<EnvDTE::StatusBar> pStatusBar;
		m_pDTE->get_StatusBar(&pStatusBar);
		pStatusBar->put_Text(g_ResLoader.LoadString(IDS_MYCOMPUTER));
	}

	//Record the selection change if the recorder is running:
	CComPtr<EnvDTE::Macros> pMacros;
	VARIANT_BOOL vbIsRecording;
	m_pDTE->get_Macros(&pMacros);
	pMacros->get_IsRecording(&vbIsRecording);
	if(vbIsRecording == VARIANT_TRUE)
	{
		CComBSTR bstrRecorderText;
		HKEY hKeyLoc;
		CString szConstructedRegKey;
		GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, szConstructedRegKey);
		bstrRecorderText = CComBSTR("DTE.AddIns.Item(\"RegExplore.Connect.1\").Object.NavigateToLocation(\"My Computer\\");
		bstrRecorderText.Append(GetRootNameFromKey(hKeyLoc));
		if(szConstructedRegKey.GetLength() > 0)
		{
			bstrRecorderText.Append("\\");
			bstrRecorderText.Append(szConstructedRegKey);
		}
		bstrRecorderText.Append(")");
		pMacros->EmitMacroCode(bstrRecorderText);
	}
	return 0;
}

LRESULT CRegExploreCtl::OnLvnKeydownDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	NMLVKEYDOWN* pkd = (NMLVKEYDOWN*)pNMHDR;
	if((pkd->wVKey == VK_DELETE) && (nListCurrentHilighted != -1))
	{
		HKEY hKeyLoc;
		CString szConstructedRegKey;
		TCHAR szValueName[MAX_PATH];
		GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, szConstructedRegKey);
		ListView_GetItemText(m_hList, nListCurrentHilighted, 0, szValueName, MAX_PATH);
		if(!_tcscmp(szValueName, g_ResLoader.TLoadString(IDS_DEFAULT)))
		{
			MessageBox(g_ResLoader.TLoadString(IDS_CANNOTRENDELDEFAULT), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_OK);
		}
		else
		{
			if(MessageBox(g_ResLoader.TLoadString(IDS_SUREDELETEVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_YESNO) == IDYES)
			{
				HKEY hKeyValue;
				RegOpenKeyEx(hKeyLoc, szConstructedRegKey, NULL, KEY_ALL_ACCESS, &hKeyValue);
				RegDeleteValue(hKeyValue, szValueName);
				ListView_DeleteItem(m_hList, nListCurrentHilighted);
				nListCurrentHilighted = -1;
			}
		}
		return 1;
	}
	return 0;
}

LRESULT CRegExploreCtl::OnLvnEndlabeleditDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	TCHAR szName[MAX_PATH];
	HKEY hKeyLoc;
	CString szConstructedRegKey;
	NMLVDISPINFO* pdi = (NMLVDISPINFO*)pNMHDR;
	ListView_GetItemText(m_hList, pdi->item.iItem, 0, szName, MAX_PATH);
	GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, szConstructedRegKey);
	HKEY hKey;

	RegOpenKeyEx(hKeyLoc, szConstructedRegKey, NULL, KEY_READ, &hKey);
	if(RegQueryValueEx(hKey, pdi->item.pszText, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
	{
		MessageBox(g_ResLoader.TLoadString(IDS_RENAMETOEXIST), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_OK);
	}
	else
	{
		DWORD dwType;
		DWORD dwSize;
		RegQueryValueEx(hKey, szName, NULL, NULL, NULL, &dwSize);
		BYTE *pByte = new BYTE[dwSize];    
		RegQueryValueEx(hKey, szName, NULL, &dwType, pByte, &dwSize);
		RegSetValueEx(hKey, pdi->item.pszText, NULL, dwType, pByte, dwSize);
		RegDeleteValue(hKey, szName);
		ListView_SetItemText(m_hList, pdi->item.iItem, 0, pdi->item.pszText);
		delete []pByte;
	}
	RegCloseKey(hKey);
	return 0;
}

LRESULT CRegExploreCtl::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	WORD wWidth = LOWORD(lParam);
	WORD wHeight = HIWORD(lParam);
	HWND hMouseFrame = GetDlgItem(IDC_MOUSEFRAME);
	RECT rectTree;

	::GetWindowRect(m_hTree, &rectTree);

	::MoveWindow(m_hTree, 0, 0, lTreeWidth, wHeight, TRUE);
	::MoveWindow(m_hList, lTreeWidth+3, 0, (wWidth-lTreeWidth-3), wHeight, TRUE);
	::MoveWindow(hMouseFrame, lTreeWidth, 0, 3, wHeight-3, TRUE);

	::SetClassLong(hMouseFrame, GCL_HCURSOR, (long)LoadCursor(NULL, IDC_SIZEWE));

	return 0;
}

LRESULT CRegExploreCtl::OnMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	if (fCaptured)
	{
		WORD wWidth = LOWORD(lParam);
		lTreeWidth = (long)wWidth;
		if ((lTreeWidth < 40) || (lTreeWidth & 0x8000))
			lTreeWidth = 40;

		RECT rectWnd;
		GetWindowRect(&rectWnd);

		WORD wWidth2 = (WORD)(rectWnd.right-rectWnd.left);
		WORD wHeight = (WORD)(rectWnd.bottom-rectWnd.top);
		HWND hMouseFrame = GetDlgItem(IDC_MOUSEFRAME);
		RECT rectTree;

		::GetWindowRect(m_hTree, &rectTree);

		::SetWindowPos(m_hTree, NULL, 0, 0, lTreeWidth, wHeight, SWP_NOREDRAW|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_NOMOVE);
		::SetWindowPos(m_hList, NULL, lTreeWidth+3, 0, (wWidth2-lTreeWidth-3), wHeight, SWP_NOREDRAW|SWP_NOOWNERZORDER|SWP_NOZORDER);
		::SetWindowPos(hMouseFrame, NULL, lTreeWidth, 0, 3, wHeight, SWP_NOREDRAW|SWP_NOOWNERZORDER|SWP_NOZORDER);
		Invalidate(FALSE);
	}
	return 0;
}

LRESULT CRegExploreCtl::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	USES_CONVERSION;
	CComPtr<IStorage> pStgRoot;
	CComPtr<IStream> pStreamShortcuts;
	CComPtr<EnvDTE::Windows> pWindows;
	CComPtr<IDispatch> pDisp;
	CComPtr<EnvDTE::TaskItems> pTaskItems;
	CComQIPtr<EnvDTE::TaskList> pTaskList;
	CComPtr<EnvDTE::Window> pWindow;
	TCHAR szPath[MAX_PATH];
	CComVariant varPic;

	m_TaskListEventSink.DispEventUnadvise((IUnknown*)pTLE.p);

	int nSize = ArrayAddedItems.GetSize();
	DWORD dwSize = (DWORD)nSize;

	//Store away any shortcuts we may have created:
	SHGetSpecialFolderPath(NULL, szPath, CSIDL_APPDATA, TRUE);
	_tcsncat(szPath, _T("\\RegistryExplorer.dat"), MAX_PATH);
	if(SUCCEEDED(StgCreateDocfile(T2W(szPath), STGM_WRITE|STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_CREATE, 0, &pStgRoot)))
	{
		if(SUCCEEDED(pStgRoot->CreateStream(L"Shortcuts", STGM_WRITE|STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_CREATE, 0,0, &pStreamShortcuts)))
		{
			CComBSTR bstrShortcut;
			pStreamShortcuts->Write(&dwSize, sizeof(DWORD), NULL);
			for (int i = 0 ; i < nSize ; i++)
			{
				CComBSTR bstrShortcut;
				long lCastedTaskItem = ArrayAddedItems[i];
				EnvDTE::TaskItem *pTaskItem = (EnvDTE::TaskItem*)lCastedTaskItem;
				pTaskItem->get_Description(&bstrShortcut);
				bstrShortcut.WriteToStream(pStreamShortcuts);
			}
		}
	}
	return 0;
}


LRESULT CRegExploreCtl::OnClickedMouseFrame(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	hOldCapture = SetCapture();
	fCaptured = TRUE;
	return 0;
}


LRESULT CRegExploreCtl::OnListMenuNoSelect_New_Key(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CNewKeyName NewKeyName;
	if(NewKeyName.DoModal() != IDOK)
		return 0;

	TVINSERTSTRUCT lpis;
	lpis.hParent = hTreeCurrentHilighted;
	lpis.hInsertAfter = TVI_LAST;
	lpis.item.mask = TVIF_PARAM|TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	lpis.item.lParam = 1;
	lpis.item.iImage = nClosedFolder;
	lpis.item.iSelectedImage = nOpenFolder;

	lpis.item.pszText = NewKeyName.m_pszValue.GetBuffer();
	HKEY hKeyLoc;
	CString pszNew;
	GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, pszNew);
	HKEY hKeyNew;
	if (RegOpenKey(hKeyLoc, pszNew, &hKeyNew) == ERROR_SUCCESS)
	{
		HKEY NewCreated;
		//If we can open the key, then it must already exist, don't create it again:
		if(RegOpenKey(hKeyNew, NewKeyName.m_pszValue, &NewCreated) == ERROR_SUCCESS)
		{
			RegCloseKey(NewCreated);
			MessageBox(g_ResLoader.TLoadString(IDS_NAMEEXISTKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_OK);
		}
		else if(RegCreateKey(hKeyNew, NewKeyName.m_pszValue, &NewCreated) == ERROR_SUCCESS)
		{
			HTREEITEM hItem = TreeView_InsertItem(m_hTree, &lpis);
			TreeView_SelectItem(m_hTree, hItem);
			TreeView_EditLabel(m_hTree, hItem);
		}
		else
		{
			MessageBox(g_ResLoader.TLoadString(IDS_CANNOTCREATENEWKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_OK);
		}
		RegCloseKey(hKeyNew);
	}
	else
	{
		MessageBox(g_ResLoader.TLoadString(IDS_CANNOTCREATENEWKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_OK);
	}
	delete []pszNew;
	return 0;
}

LRESULT CRegExploreCtl::OnListMenuNoSelect_New_String(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CNewValueNameDlg NewValueNameDlg;
	if (NewValueNameDlg.DoModal() != IDOK)
		return 0;

	HKEY hKeyLoc;
	CString pszNew;
	GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, pszNew);
	HKEY hKeyNew;
	if (RegOpenKey(hKeyLoc, pszNew, &hKeyNew) == ERROR_SUCCESS)
	{
		if (RegSetValueEx(hKeyNew, NewValueNameDlg.m_pszValue, NULL, REG_SZ, (LPBYTE)_T(""), 1) == ERROR_SUCCESS)
		{
			LVITEM lvi;
			lvi.mask = LVIF_TEXT|LVIF_IMAGE;
			lvi.iItem = ListView_GetItemCount(m_hList)+1;
			lvi.iSubItem = 0;
			lvi.iImage = nStringData;
			lvi.pszText = NewValueNameDlg.m_pszValue.GetBuffer();
			int nItem = ListView_InsertItem(m_hList, &lvi); 

			lvi.mask = LVIF_TEXT;
			lvi.iItem = nItem;
			lvi.iSubItem = 1;
			lvi.pszText = _T("");
			ListView_SetItem(m_hList, &lvi);
			lvi.iSubItem = 2;
			lvi.pszText = _T("REG_SZ");
			ListView_SetItem(m_hList, &lvi);
		}
		else
		{
			MessageBox(g_ResLoader.TLoadString(IDS_CANNOTCREATENEWVALUE), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
		}
		RegCloseKey(hKeyNew);
	}
	else
	{
		MessageBox(g_ResLoader.TLoadString(IDS_CANNOTCREATENEWVALUE), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
	}
	delete []pszNew;
	return 0;
}

LRESULT CRegExploreCtl::OnListMenuNoSelect_New_Binary(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CNewValueNameDlg NewValueNameDlg;
	if (NewValueNameDlg.DoModal() != IDOK)
		return 0;

	HKEY hKeyLoc;
	CString pszNew;
	GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, pszNew);
	HKEY hKeyNew;
	if (RegOpenKey(hKeyLoc, pszNew, &hKeyNew) == ERROR_SUCCESS)
	{
		if (RegSetValueEx(hKeyNew, NewValueNameDlg.m_pszValue, NULL, REG_BINARY, NULL, 0) == ERROR_SUCCESS)
		{
			LVITEM lvi;
			lvi.mask = LVIF_TEXT|LVIF_IMAGE;
			lvi.iItem = ListView_GetItemCount(m_hList)+1;
			lvi.iSubItem = 0;
			lvi.iImage = nBinaryData;
			lvi.pszText = NewValueNameDlg.m_pszValue.GetBuffer();
			int nItem = ListView_InsertItem(m_hList, &lvi); 

			lvi.mask = LVIF_TEXT;
			lvi.iItem = nItem;
			lvi.iSubItem = 1;
			lvi.pszText = g_ResLoader.TLoadString(IDS_ZEROLENBINARY);
			ListView_SetItem(m_hList, &lvi);
		}
		else
		{
			MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTCREATEVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
		}
		RegCloseKey(hKeyNew);
	}
	else
	{
		MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTOPENKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
	}
	delete []pszNew;
	return 0;
}

LRESULT CRegExploreCtl::OnListMenuNoSelect_New_DWORD(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CNewValueNameDlg NewValueNameDlg;
	if (NewValueNameDlg.DoModal() != IDOK)
		return 0;

	HKEY hKeyLoc;
	CString pszNew;
	GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, pszNew);
	HKEY hKeyNew;
	if (RegOpenKey(hKeyLoc, pszNew, &hKeyNew) == ERROR_SUCCESS)
	{
		DWORD dwValue = 0;
		if (RegSetValueEx(hKeyNew, NewValueNameDlg.m_pszValue, NULL, REG_DWORD, (LPBYTE)&dwValue, sizeof(DWORD)) == ERROR_SUCCESS)
		{
			LVITEM lvi;
			lvi.mask = LVIF_TEXT|LVIF_IMAGE;
			lvi.iItem = ListView_GetItemCount(m_hList)+1;
			lvi.iSubItem = 0;
			lvi.iImage = nBinaryData;
			lvi.pszText = NewValueNameDlg.m_pszValue.GetBuffer();
			int nItem = ListView_InsertItem(m_hList, &lvi); 

			lvi.mask = LVIF_TEXT;
			lvi.iItem = nItem;
			lvi.iSubItem = 1;
			lvi.pszText = _T("0");
			ListView_SetItem(m_hList, &lvi);
			lvi.iSubItem = 2;
			lvi.pszText = _T("REG_DWORD");
			ListView_SetItem(m_hList, &lvi);
		}
		else
		{
			MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTCREATEVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
		}
		RegCloseKey(hKeyNew);
	}
	else
	{
		MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTOPENKEY), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
	}
	delete []pszNew;
	return 0;
}

LRESULT CRegExploreCtl::OnListMenuSelect_Modify(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	ModifyValue();
	return 0;
}

LRESULT CRegExploreCtl::OnListMenuSelect_Delete(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	HKEY hKeyLoc;
	CString szNew;
	TCHAR szDataValue[MAX_PATH];
	ListView_GetItemText(m_hList, nListCurrentHilighted, 0, szDataValue, MAX_PATH);
	if(!_tcscmp(szDataValue, g_ResLoader.TLoadString(IDS_DEFAULT)))
	{
		MessageBox(g_ResLoader.TLoadString(IDS_CANNOTRENDELDEFAULT), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
	}
	else if(MessageBox(g_ResLoader.TLoadString(IDS_SUREDELETEVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_YESNO) == IDYES)
	{
		GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, szNew);
		HKEY hKeyNew;
		if (RegOpenKey(hKeyLoc, szNew, &hKeyNew) == ERROR_SUCCESS)
		{
			RegDeleteValue(hKeyNew, szDataValue);
			ListView_DeleteItem(m_hList, nListCurrentHilighted);
			RegCloseKey(hKeyNew);
		}
	}
	return 0;
}

LRESULT CRegExploreCtl::OnListMenuSelect_Rename(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	//HWND hwndEdit = ListView_EditLabel(m_hList, nListCurrentHilighted);
	TCHAR szItemName[MAX_PATH];
	CRenameDlg RenameDlg;
	ListView_GetItemText(m_hList, nListCurrentHilighted, 0, szItemName, MAX_PATH);
	if(!_tcscmp(szItemName, g_ResLoader.TLoadString(IDS_DEFAULT)))
	{
		MessageBox(g_ResLoader.TLoadString(IDS_CANNOTRENDELDEFAULT), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
		return 0;
	}
	RenameDlg.m_szName = szItemName;
	if (RenameDlg.DoModal() == IDOK)
	{
		//TODO: Change the name of the key.
		HKEY hKeyLoc;
		CString pszNew;
		GetKey(m_hTree, hTreeCurrentHilighted, &hKeyLoc, pszNew);
		HKEY hKeyNew;
		if (RegOpenKey(hKeyLoc, pszNew, &hKeyNew) == ERROR_SUCCESS)
		{
			DWORD dwType, dwSize;
			if(RegQueryValueEx(hKeyNew, szItemName, NULL, &dwType, NULL, &dwSize) == ERROR_SUCCESS)
			{
				LPBYTE lpData = new BYTE[dwSize];
				RegQueryValueEx(hKeyNew, szItemName, NULL, &dwType, lpData, &dwSize);
				if (RegSetValueEx(hKeyNew, RenameDlg.m_szName, NULL, dwType, lpData, dwSize) == ERROR_SUCCESS)
				{
					ListView_SetItemText(m_hList, nListCurrentHilighted, 0, RenameDlg.m_szName.GetBuffer());
					RegDeleteValue(hKeyNew, szItemName);
				}
				else
				{
					MessageBox(g_ResLoader.TLoadString(IDS_COULDNOTCREATEVAL), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER), MB_ICONEXCLAMATION|MB_OK);
				}
				delete []lpData;
			}
			//RegSetValue(hKeyNew, pszNew, 
		}
		RegCloseKey(hKeyNew);
	}
	return 0;
}

LRESULT CRegExploreCtl::OnTreeMenu_Expand(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	TreeView_Expand(m_hTree, hTreeCurrentHilighted, TVE_TOGGLE);
	return 0;
}


LRESULT CRegExploreCtl::OnLButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (fCaptured)
	{
		::SetCapture(hOldCapture);
		fCaptured = FALSE;
	}
	return 0;
}

LRESULT CRegExploreCtl::OnNMRclickRegtree(int /*idCtrl*/, LPNMHDR /*pNMHDR*/, BOOL& /*bHandled*/)
{
	POINT pointPos;
	TVHITTESTINFO tvhti;
	GetCursorPos(&pointPos);
	RECT rectTree;
	::GetWindowRect(m_hTree, &rectTree);
	pointPos.x -= rectTree.left;
	pointPos.y -= rectTree.top;
	tvhti.pt = pointPos;
	TreeView_HitTest(m_hTree, &tvhti);
	hTreeCurrentHilighted = tvhti.hItem;
	if (tvhti.flags & TVHT_ONITEM)
	{
		if (IsComputerSelected())
		{
			HMENU hMenu = LoadMenu(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_CONTEXTMENUS));
			HMENU hMenuTree = GetSubMenu(hMenu, 3);
			TrackPopupMenu(hMenuTree, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pointPos.x+rectTree.left, pointPos.y+rectTree.top, NULL, m_hWnd, NULL);
		}
		else if (IsRootSelected())
		{
			HMENU hMenu = LoadMenu(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_CONTEXTMENUS));
			HMENU hMenuTree = GetSubMenu(hMenu, 2);
			MENUITEMINFO mii;
			mii.fMask = MIIM_STATE;
			mii.fState = MFS_DISABLED;
			SetMenuItemInfo(hMenuTree, ID_TREEMENU_DELETE, FALSE, &mii);
			SetMenuItemInfo(hMenuTree, ID_TREEMENU_RENAME, FALSE, &mii);

			TrackPopupMenu(hMenuTree, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pointPos.x+rectTree.left, pointPos.y+rectTree.top, NULL, m_hWnd, NULL);
		}
		else
		{
			HMENU hMenu = LoadMenu(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_CONTEXTMENUS));
			HMENU hMenuTree = GetSubMenu(hMenu, 2);
			TrackPopupMenu(hMenuTree, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pointPos.x+rectTree.left, pointPos.y+rectTree.top, NULL, m_hWnd, NULL);
		}
	}
	else
	{
		HMENU hMenu = LoadMenu(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_CONTEXTMENUS));
		HMENU hMenuTree = GetSubMenu(hMenu, 4);
		TrackPopupMenu(hMenuTree, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pointPos.x+rectTree.left, pointPos.y+rectTree.top, NULL, m_hWnd, NULL);
	}
	return 0;
}

LRESULT CRegExploreCtl::OnNMRclickDatalist(int /*idCtrl*/, LPNMHDR /*pNMHDR*/, BOOL& /*bHandled*/)
{
	POINT pointPos;
	LVHITTESTINFO lvhti;
	GetCursorPos(&pointPos);
	RECT rectList;
	::GetWindowRect(m_hList, &rectList);
	pointPos.x -= rectList.left;
	pointPos.y -= rectList.top;
	lvhti.pt = pointPos;
	ListView_HitTest(m_hList, &lvhti);
	nListCurrentHilighted = lvhti.iItem;
	if (lvhti.flags & LVHT_ONITEM)
	{
		HMENU hMenu = LoadMenu(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_CONTEXTMENUS));
		HMENU hMenuList = GetSubMenu(hMenu, 1);
		if (lvhti.iItem == 0)
		{
			MENUITEMINFO mii;
			mii.fMask = MIIM_STATE;
			mii.fState = MFS_DISABLED;
			SetMenuItemInfo(hMenuList, ID_LISTMENUSELECT_DELETE, FALSE, &mii);
			SetMenuItemInfo(hMenuList, ID_LISTMENUSELECT_RENAME, FALSE, &mii);
		}

		TrackPopupMenu(hMenuList, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pointPos.x+rectList.left, pointPos.y+rectList.top, NULL, m_hWnd, NULL);
	}
	else
	{	
		HMENU hMenu = LoadMenu(_AtlModule.GetResourceInstance(), MAKEINTRESOURCE(IDR_CONTEXTMENUS));
		HMENU hMenuList = GetSubMenu(hMenu, 0);	

		if (IsComputerSelected())
		{
			HMENU hSubMenu = GetSubMenu(hMenuList, 0);
			MENUITEMINFO mii;
			mii.fMask = MIIM_STATE;
			mii.fState = MFS_DISABLED;
			SetMenuItemInfo(hSubMenu, ID_LISTMENUNOSELECT_NEW_KEY, FALSE, &mii);
			SetMenuItemInfo(hSubMenu, ID_LISTMENUNOSELECT_NEW_STRING, FALSE, &mii);
			SetMenuItemInfo(hSubMenu, ID_LISTMENUNOSELECT_NEW_BINARY, FALSE, &mii);
			SetMenuItemInfo(hSubMenu, ID_LISTMENUNOSELECT_NEW_DWORD, FALSE, &mii);
		}

		TrackPopupMenu(hMenuList, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pointPos.x+rectList.left, pointPos.y+rectList.top, NULL, m_hWnd, NULL);
	}
	return 0;
}

LRESULT CRegExploreCtl::OnNMClickDatalist(int /*idCtrl*/, LPNMHDR /*pNMHDR*/, BOOL& /*bHandled*/)
{
	POINT pointPos;
	LVHITTESTINFO lvhti;
	GetCursorPos(&pointPos);
	RECT rectList;
	::GetWindowRect(m_hList, &rectList);
	pointPos.x -= rectList.left;
	pointPos.y -= rectList.top;
	lvhti.pt = pointPos;
	ListView_HitTest(m_hList, &lvhti);
	nListCurrentHilighted = lvhti.iItem;
	return 0;
}

LRESULT CRegExploreCtl::OnLvnItemActivateDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	nListCurrentHilighted = pNMIA->iItem;
	/*TODO: 
	POINT pointPos;
	LVHITTESTINFO lvhti;
	GetCursorPos(&pointPos);
	RECT rectList;
	::GetWindowRect(m_hList, &rectList);
	pointPos.x -= rectList.left;
	pointPos.y -= rectList.top;
	lvhti.pt = pointPos;
	ListView_HitTest(m_hList, &lvhti);
	nListCurrentHilighted = lvhti.iItem;*/
	return 0;
}

LRESULT CRegExploreCtl::OnNMDblclkDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	LVHITTESTINFO *lvhti = (LVHITTESTINFO*)pNMHDR;
	nListCurrentHilighted = lvhti->iItem;
	ModifyValue();
	return 0;
}
