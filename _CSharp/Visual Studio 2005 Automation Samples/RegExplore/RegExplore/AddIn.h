

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Mon Dec 12 12:13:26 2005
 */
/* Compiler settings for .\AddIn.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __AddIn_h__
#define __AddIn_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IRegExploreCtl_FWD_DEFINED__
#define __IRegExploreCtl_FWD_DEFINED__
typedef interface IRegExploreCtl IRegExploreCtl;
#endif 	/* __IRegExploreCtl_FWD_DEFINED__ */


#ifndef __IRegistryExplorer_FWD_DEFINED__
#define __IRegistryExplorer_FWD_DEFINED__
typedef interface IRegistryExplorer IRegistryExplorer;
#endif 	/* __IRegistryExplorer_FWD_DEFINED__ */


#ifndef __IRegExplorerOptions_FWD_DEFINED__
#define __IRegExplorerOptions_FWD_DEFINED__
typedef interface IRegExplorerOptions IRegExplorerOptions;
#endif 	/* __IRegExplorerOptions_FWD_DEFINED__ */


#ifndef __IRegExplorerProperties_FWD_DEFINED__
#define __IRegExplorerProperties_FWD_DEFINED__
typedef interface IRegExplorerProperties IRegExplorerProperties;
#endif 	/* __IRegExplorerProperties_FWD_DEFINED__ */


#ifndef __IKeyProperties_FWD_DEFINED__
#define __IKeyProperties_FWD_DEFINED__
typedef interface IKeyProperties IKeyProperties;
#endif 	/* __IKeyProperties_FWD_DEFINED__ */


#ifndef __Connect_FWD_DEFINED__
#define __Connect_FWD_DEFINED__

#ifdef __cplusplus
typedef class Connect Connect;
#else
typedef struct Connect Connect;
#endif /* __cplusplus */

#endif 	/* __Connect_FWD_DEFINED__ */


#ifndef __RegExploreCtl_FWD_DEFINED__
#define __RegExploreCtl_FWD_DEFINED__

#ifdef __cplusplus
typedef class RegExploreCtl RegExploreCtl;
#else
typedef struct RegExploreCtl RegExploreCtl;
#endif /* __cplusplus */

#endif 	/* __RegExploreCtl_FWD_DEFINED__ */


#ifndef __RegistryExplorer_FWD_DEFINED__
#define __RegistryExplorer_FWD_DEFINED__

#ifdef __cplusplus
typedef class RegistryExplorer RegistryExplorer;
#else
typedef struct RegistryExplorer RegistryExplorer;
#endif /* __cplusplus */

#endif 	/* __RegistryExplorer_FWD_DEFINED__ */


#ifndef __RegExplorerOptions_FWD_DEFINED__
#define __RegExplorerOptions_FWD_DEFINED__

#ifdef __cplusplus
typedef class RegExplorerOptions RegExplorerOptions;
#else
typedef struct RegExplorerOptions RegExplorerOptions;
#endif /* __cplusplus */

#endif 	/* __RegExplorerOptions_FWD_DEFINED__ */


#ifndef __RegExplorerProperties_FWD_DEFINED__
#define __RegExplorerProperties_FWD_DEFINED__

#ifdef __cplusplus
typedef class RegExplorerProperties RegExplorerProperties;
#else
typedef struct RegExplorerProperties RegExplorerProperties;
#endif /* __cplusplus */

#endif 	/* __RegExplorerProperties_FWD_DEFINED__ */


#ifndef __KeyProperties_FWD_DEFINED__
#define __KeyProperties_FWD_DEFINED__

#ifdef __cplusplus
typedef class KeyProperties KeyProperties;
#else
typedef struct KeyProperties KeyProperties;
#endif /* __cplusplus */

#endif 	/* __KeyProperties_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IRegExploreCtl_INTERFACE_DEFINED__
#define __IRegExploreCtl_INTERFACE_DEFINED__

/* interface IRegExploreCtl */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRegExploreCtl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("822DDBB6-FC97-4734-89D7-299AF92D7DF0")
    IRegExploreCtl : public IDispatch
    {
    public:
        virtual /* [restricted][local][hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE SetParent( 
            IUnknown *pUnk) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Navigate( 
            BSTR Location) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRegExploreCtlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRegExploreCtl * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRegExploreCtl * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRegExploreCtl * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRegExploreCtl * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRegExploreCtl * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRegExploreCtl * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRegExploreCtl * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [restricted][local][hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetParent )( 
            IRegExploreCtl * This,
            IUnknown *pUnk);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Navigate )( 
            IRegExploreCtl * This,
            BSTR Location);
        
        END_INTERFACE
    } IRegExploreCtlVtbl;

    interface IRegExploreCtl
    {
        CONST_VTBL struct IRegExploreCtlVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRegExploreCtl_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRegExploreCtl_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRegExploreCtl_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRegExploreCtl_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IRegExploreCtl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IRegExploreCtl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IRegExploreCtl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IRegExploreCtl_SetParent(This,pUnk)	\
    (This)->lpVtbl -> SetParent(This,pUnk)

#define IRegExploreCtl_Navigate(This,Location)	\
    (This)->lpVtbl -> Navigate(This,Location)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [restricted][local][hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE IRegExploreCtl_SetParent_Proxy( 
    IRegExploreCtl * This,
    IUnknown *pUnk);


void __RPC_STUB IRegExploreCtl_SetParent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IRegExploreCtl_Navigate_Proxy( 
    IRegExploreCtl * This,
    BSTR Location);


void __RPC_STUB IRegExploreCtl_Navigate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IRegExploreCtl_INTERFACE_DEFINED__ */


#ifndef __IRegistryExplorer_INTERFACE_DEFINED__
#define __IRegistryExplorer_INTERFACE_DEFINED__

/* interface IRegistryExplorer */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRegistryExplorer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D2D540DF-AB85-4B2B-9754-536F2F026602")
    IRegistryExplorer : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE NavigateToLocation( 
            BSTR KeyName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRegistryExplorerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRegistryExplorer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRegistryExplorer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRegistryExplorer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRegistryExplorer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRegistryExplorer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRegistryExplorer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRegistryExplorer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NavigateToLocation )( 
            IRegistryExplorer * This,
            BSTR KeyName);
        
        END_INTERFACE
    } IRegistryExplorerVtbl;

    interface IRegistryExplorer
    {
        CONST_VTBL struct IRegistryExplorerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRegistryExplorer_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRegistryExplorer_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRegistryExplorer_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRegistryExplorer_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IRegistryExplorer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IRegistryExplorer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IRegistryExplorer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IRegistryExplorer_NavigateToLocation(This,KeyName)	\
    (This)->lpVtbl -> NavigateToLocation(This,KeyName)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IRegistryExplorer_NavigateToLocation_Proxy( 
    IRegistryExplorer * This,
    BSTR KeyName);


void __RPC_STUB IRegistryExplorer_NavigateToLocation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IRegistryExplorer_INTERFACE_DEFINED__ */


#ifndef __IRegExplorerOptions_INTERFACE_DEFINED__
#define __IRegExplorerOptions_INTERFACE_DEFINED__

/* interface IRegExplorerOptions */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRegExplorerOptions;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BF0C16A9-0FA1-4CB4-949D-747A0CF2A337")
    IRegExplorerOptions : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IRegExplorerOptionsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRegExplorerOptions * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRegExplorerOptions * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRegExplorerOptions * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRegExplorerOptions * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRegExplorerOptions * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRegExplorerOptions * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRegExplorerOptions * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IRegExplorerOptionsVtbl;

    interface IRegExplorerOptions
    {
        CONST_VTBL struct IRegExplorerOptionsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRegExplorerOptions_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRegExplorerOptions_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRegExplorerOptions_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRegExplorerOptions_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IRegExplorerOptions_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IRegExplorerOptions_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IRegExplorerOptions_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRegExplorerOptions_INTERFACE_DEFINED__ */


#ifndef __IRegExplorerProperties_INTERFACE_DEFINED__
#define __IRegExplorerProperties_INTERFACE_DEFINED__

/* interface IRegExplorerProperties */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRegExplorerProperties;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("12E6D6A2-1192-48FB-B8A0-58FD95F05863")
    IRegExplorerProperties : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PersistOptions( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PersistOptions( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRegExplorerPropertiesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRegExplorerProperties * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRegExplorerProperties * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRegExplorerProperties * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRegExplorerProperties * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRegExplorerProperties * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRegExplorerProperties * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRegExplorerProperties * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PersistOptions )( 
            IRegExplorerProperties * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PersistOptions )( 
            IRegExplorerProperties * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        END_INTERFACE
    } IRegExplorerPropertiesVtbl;

    interface IRegExplorerProperties
    {
        CONST_VTBL struct IRegExplorerPropertiesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRegExplorerProperties_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRegExplorerProperties_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRegExplorerProperties_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRegExplorerProperties_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IRegExplorerProperties_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IRegExplorerProperties_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IRegExplorerProperties_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IRegExplorerProperties_get_PersistOptions(This,pVal)	\
    (This)->lpVtbl -> get_PersistOptions(This,pVal)

#define IRegExplorerProperties_put_PersistOptions(This,newVal)	\
    (This)->lpVtbl -> put_PersistOptions(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IRegExplorerProperties_get_PersistOptions_Proxy( 
    IRegExplorerProperties * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IRegExplorerProperties_get_PersistOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IRegExplorerProperties_put_PersistOptions_Proxy( 
    IRegExplorerProperties * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IRegExplorerProperties_put_PersistOptions_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IRegExplorerProperties_INTERFACE_DEFINED__ */


#ifndef __IKeyProperties_INTERFACE_DEFINED__
#define __IKeyProperties_INTERFACE_DEFINED__

/* interface IKeyProperties */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IKeyProperties;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9E9F0439-89AC-4093-B484-D65C69D1E63D")
    IKeyProperties : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NumberSubkeys( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LongestSubkeyNameLength( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Values( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LastWriteTime( 
            /* [retval][out] */ DATE *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IKeyPropertiesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IKeyProperties * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IKeyProperties * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IKeyProperties * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IKeyProperties * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IKeyProperties * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IKeyProperties * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IKeyProperties * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NumberSubkeys )( 
            IKeyProperties * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LongestSubkeyNameLength )( 
            IKeyProperties * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Values )( 
            IKeyProperties * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LastWriteTime )( 
            IKeyProperties * This,
            /* [retval][out] */ DATE *pVal);
        
        END_INTERFACE
    } IKeyPropertiesVtbl;

    interface IKeyProperties
    {
        CONST_VTBL struct IKeyPropertiesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IKeyProperties_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IKeyProperties_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IKeyProperties_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IKeyProperties_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IKeyProperties_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IKeyProperties_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IKeyProperties_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IKeyProperties_get_NumberSubkeys(This,pVal)	\
    (This)->lpVtbl -> get_NumberSubkeys(This,pVal)

#define IKeyProperties_get_LongestSubkeyNameLength(This,pVal)	\
    (This)->lpVtbl -> get_LongestSubkeyNameLength(This,pVal)

#define IKeyProperties_get_Values(This,pVal)	\
    (This)->lpVtbl -> get_Values(This,pVal)

#define IKeyProperties_get_LastWriteTime(This,pVal)	\
    (This)->lpVtbl -> get_LastWriteTime(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IKeyProperties_get_NumberSubkeys_Proxy( 
    IKeyProperties * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IKeyProperties_get_NumberSubkeys_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IKeyProperties_get_LongestSubkeyNameLength_Proxy( 
    IKeyProperties * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IKeyProperties_get_LongestSubkeyNameLength_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IKeyProperties_get_Values_Proxy( 
    IKeyProperties * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IKeyProperties_get_Values_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IKeyProperties_get_LastWriteTime_Proxy( 
    IKeyProperties * This,
    /* [retval][out] */ DATE *pVal);


void __RPC_STUB IKeyProperties_get_LastWriteTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IKeyProperties_INTERFACE_DEFINED__ */



#ifndef __RegExploreLib_LIBRARY_DEFINED__
#define __RegExploreLib_LIBRARY_DEFINED__

/* library RegExploreLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_RegExploreLib;

EXTERN_C const CLSID CLSID_Connect;

#ifdef __cplusplus

class DECLSPEC_UUID("E588177B-DE48-464F-B731-B9EFDDECBE07")
Connect;
#endif

EXTERN_C const CLSID CLSID_RegExploreCtl;

#ifdef __cplusplus

class DECLSPEC_UUID("98B6C0EB-542C-4D82-83F3-013E66ECE8D1")
RegExploreCtl;
#endif

EXTERN_C const CLSID CLSID_RegistryExplorer;

#ifdef __cplusplus

class DECLSPEC_UUID("7D597871-EAE2-4046-8791-BD9655514A3D")
RegistryExplorer;
#endif

EXTERN_C const CLSID CLSID_RegExplorerOptions;

#ifdef __cplusplus

class DECLSPEC_UUID("72356BE7-B4CC-4518-9124-75EDC4F8C6A2")
RegExplorerOptions;
#endif

EXTERN_C const CLSID CLSID_RegExplorerProperties;

#ifdef __cplusplus

class DECLSPEC_UUID("E38F0506-14C2-425B-BD6A-E397E6F11F3E")
RegExplorerProperties;
#endif

EXTERN_C const CLSID CLSID_KeyProperties;

#ifdef __cplusplus

class DECLSPEC_UUID("1D2EF55C-C73B-4094-A0F1-612997CBBA8B")
KeyProperties;
#endif
#endif /* __RegExploreLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


