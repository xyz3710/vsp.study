//Copyright (c) Microsoft Corporation.  All rights reserved.

// KeyProperties.cpp : Implementation of CKeyProperties

#include "stdafx.h"
#include "KeyProperties.h"


// CKeyProperties

STDMETHODIMP CKeyProperties::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IKeyProperties
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CKeyProperties::get_NumberSubkeys(long *pVal)
{
	*pVal = (long)m_dwSubKeys;
	return S_OK;
}

STDMETHODIMP CKeyProperties::get_LongestSubkeyNameLength(long *pVal)
{
	*pVal = (long)m_dwLongestSubkeyNameLength;
	return S_OK;
}

STDMETHODIMP CKeyProperties::get_Values(long *pVal)
{
	*pVal = (long)m_dwValueEntries;
	return S_OK;
}

STDMETHODIMP CKeyProperties::get_LastWriteTime(DATE *pVal)
{
	SYSTEMTIME systime;
	SYSTEMTIME localtime;
	DOUBLE vartime;
	TIME_ZONE_INFORMATION TimeZoneInformation;

	GetTimeZoneInformation(&TimeZoneInformation);
	FileTimeToSystemTime(&m_LastWriteTime, &systime);
	SystemTimeToTzSpecificLocalTime(&TimeZoneInformation, &systime, &localtime);
	SystemTimeToVariantTime(&localtime, &vartime);
	*pVal = (DATE)vartime;
	return S_OK;
}
