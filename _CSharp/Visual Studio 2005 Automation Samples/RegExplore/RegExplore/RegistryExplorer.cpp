//Copyright (c) Microsoft Corporation.  All rights reserved.

// RegistryExplorer.cpp : Implementation of CRegistryExplorer

#include "stdafx.h"
#include "RegistryExplorer.h"
#include "RegExploreCtl.h"

// CRegistryExplorer
extern CRegExploreCtl *g_CRegExploreCtl;
STDMETHODIMP CRegistryExplorer::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&__uuidof(IRegistryExplorer)
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CRegistryExplorer::NavigateToLocation(BSTR KeyName)
{
	g_CRegExploreCtl->SelectItem(KeyName);
	return S_OK;
}
