//Copyright (c) Microsoft Corporation.  All rights reserved.

// RenameDlg.h : Declaration of the CRenameDlg

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>
#include "resloader.h"

// CRenameDlg

class CRenameDlg : 
	public CAxDialogImpl<CRenameDlg>
{
public:
	CRenameDlg()
	{
	}

	~CRenameDlg()
	{
	}

	CString m_szName;

	enum { IDD = IDD_RENAMEDLG };

BEGIN_MSG_MAP(CRenameDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	CHAIN_MSG_MAP(CAxDialogImpl<CRenameDlg>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CRenameDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		SetDlgItemText(IDC_NAME, m_szName);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		GetDlgItem(IDC_NAME).GetWindowText(m_szName);
		if (m_szName.GetLength() == 0)
		{
			MessageBox(g_ResLoader.TLoadString(IDS_ENTERATLEASTONECHAR), g_ResLoader.TLoadString(IDS_REGISTRYEXPLORER));
		}
		else
		{
			EndDialog(wID);
		}
		return 0;
	}

	LRESULT OnClickedCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		EndDialog(wID);
		return 0;
	}
};


