//Copyright (c) Microsoft Corporation.  All rights reserved.

// DWORDEdit.h : Declaration of the CDWORDEdit

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>

// CDWORDEdit

class CDWORDEdit : 
	public CAxDialogImpl<CDWORDEdit>
{
public:
	CDWORDEdit(CString strValueName, DWORD dwCurrentValue)
	{
		m_strValueName = strValueName;
		m_dwValue = dwCurrentValue;
	}

	~CDWORDEdit()
	{
	}

	CString m_strValueName;
	DWORD m_dwValue;

	enum { IDD = IDD_DWORDEDIT };

BEGIN_MSG_MAP(CDWORDEdit)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	CHAIN_MSG_MAP(CAxDialogImpl<CDWORDEdit>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CDWORDEdit>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		CenterWindow();
		TCHAR szTemp[20];
		wsprintf(szTemp, _T("%d"), m_dwValue);
		SetDlgItemText(IDC_VALUENAME, m_strValueName);
		SetDlgItemText(IDC_DWORDVALUE, szTemp);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		CString szText;
		GetDlgItem(IDC_VALUE).GetWindowText(szText);
		m_dwValue = (DWORD)_ttol(szText);
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		EndDialog(wID);
		return 0;
	}
};


