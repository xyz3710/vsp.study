//Copyright (c) Microsoft Corporation.  All rights reserved.

LONG RecursiveDeleteKey(HKEY hKeyRoot, CString lpszSubKey);
void SaveOptions(BOOL fPersist);
BOOL LoadOptions();
CString GetStringOfValue(BYTE* byteValue, DWORD dwType, DWORD dwSize);
CString GetDataTypeName(DWORD dwType);
CString GetRootNameFromKey(HKEY hKey);
HKEY GetKeyFromRoot(TCHAR *pszName);
void StripKeys(CString pszIn, TCHAR **ppszOutMain, TCHAR **ppszKey);
inline LONG RecursiveDeleteKey(HKEY hKeyRoot, TCHAR *lpszSubKey);
HPALETTE CreateDIBPalette (LPBITMAPINFO lpbmi, LPINT lpiNumColors);
HBITMAP LoadResourceBitmap(HINSTANCE hInstance, TCHAR *pszString, HPALETTE FAR* lphPalette);
HKEY GetKeyFromRoot(TCHAR *pszName);