//Copyright (c) Microsoft Corporation.  All rights reserved.

// EditString.h : Declaration of the CEditString

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>


// CEditString

class CEditString : 
	public CAxDialogImpl<CEditString>
{
public:
	CEditString(CString pszName, CString pszValue)
	{
		m_pszName = pszName;
		m_pszValue = pszValue;
	}

	~CEditString()
	{
		delete []m_pszValue;
	}

	CString m_pszName;
	CString m_pszValue;

	enum { IDD = IDD_EDITSTRING };

BEGIN_MSG_MAP(CEditString)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	CHAIN_MSG_MAP(CAxDialogImpl<CEditString>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CEditString>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		CenterWindow();
		SetDlgItemText(IDC_VALUENAME, m_pszName);
		SetDlgItemText(IDC_VALUE, m_pszValue);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		GetDlgItem(IDC_VALUE).GetWindowText(m_pszValue);
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		m_pszValue.Empty();
		EndDialog(wID);
		return 0;
	}
};


