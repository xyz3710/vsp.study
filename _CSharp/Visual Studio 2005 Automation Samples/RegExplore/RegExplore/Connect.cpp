//Copyright (c) Microsoft Corporation.  All rights reserved.

// Connect.cpp : Implementation of CConnect
#include "stdafx.h"
#include "AddIn.h"
#include "Connect.h"
#include "ResLoader.h"
#include "RegistryExplorer.h"

extern CAddInModule _AtlModule;
CResLoader g_ResLoader;
extern HBITMAP LoadResourceBitmap(HINSTANCE hInstance, TCHAR *pszString, HPALETTE FAR* lphPalette);


CComBSTR bstrGUIDTool = _T("{549D8DD9-1C56-11D2-938C-00C0DFE31582}");

// CConnect
STDMETHODIMP CConnect::OnConnection(IDispatch *pApplication, ext_ConnectMode ConnectMode, IDispatch *pAddInInst, SAFEARRAY ** /*custom*/ )
{
	HRESULT hr = S_OK;
	CComBSTR bstrDocObjProgID = _T("RegExplore.RegExploreCtl.1");
	CComPtr<Windows> pWindows;
	CComBSTR bstrSatelliteDLLPath;
	CComPtr<AddIns> pAddIns;
	CComQIPtr<IRegExploreCtl> pCtl;
	CComPtr<IPictureDisp> pPictureDisp;
	CComVariant varPic;
	PICTDESC pd;

	pApplication->QueryInterface(__uuidof(DTE2), (LPVOID*)&m_pDTE);
	pAddInInst->QueryInterface(__uuidof(AddIn), (LPVOID*)&m_pAddInInstance);

	if(ConnectMode == 5) //5 == ext_cm_UISetup
	{
		HRESULT hr = S_OK;
		CComPtr<IDispatch> pDisp;
		CComQIPtr<Commands> pCommands;
		CComQIPtr<Commands2> pCommands2;
		CComQIPtr<_CommandBars> pCommandBars;
		CComPtr<CommandBarControl> pCommandBarControl;
		CComPtr<Command> pCreatedCommand;
		CComPtr<CommandBar> pMenuBarCommandBar;
		CComPtr<CommandBarControls> pMenuBarControls;
		CComPtr<CommandBarControl> pToolsCommandBarControl;
		CComQIPtr<CommandBar> pToolsCommandBar;
		CComQIPtr<CommandBarPopup> pToolsPopup;

		IfFailGoCheck(m_pDTE->get_Commands(&pCommands), pCommands);
		pCommands2 = pCommands;
		if(SUCCEEDED(pCommands2->AddNamedCommand2(m_pAddInInstance, CComBSTR("RegExplore"), CComBSTR("RegExplore"), CComBSTR("Executes the command for RegExplore"), VARIANT_FALSE, CComVariant(IDB_CMDBARBITMAP), NULL, vsCommandStatusSupported+vsCommandStatusEnabled, vsCommandStylePictAndText, vsCommandControlTypeButton, &pCreatedCommand)) && (pCreatedCommand))
		{
			//Add a button to the tools menu bar.
			IfFailGoCheck(m_pDTE->get_CommandBars(&pDisp), pDisp);
			pCommandBars = pDisp;

			//Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
			IfFailGoCheck(pCommandBars->get_Item(CComVariant(L"MenuBar"), &pMenuBarCommandBar), pMenuBarCommandBar);
			IfFailGoCheck(pMenuBarCommandBar->get_Controls(&pMenuBarControls), pMenuBarControls);

			//Find the Tools command bar on the MenuBar command bar:
			IfFailGoCheck(pMenuBarControls->get_Item(CComVariant(L"Tools"), &pToolsCommandBarControl), pToolsCommandBarControl);
			pToolsPopup = pToolsCommandBarControl;
			IfFailGoCheck(pToolsPopup->get_CommandBar(&pToolsCommandBar), pToolsCommandBar);

			//Add the control:
			pDisp = NULL;
			IfFailGoCheck(pCreatedCommand->AddControl(pToolsCommandBar, 1, &pDisp), pDisp);
		}
	}
	else
	{
		//We want to customize the picture that is displayed on the tab of the tool window. 
		// Find the satellite dll that contains that picture:
		m_pAddInInstance->get_SatelliteDllPath(&bstrSatelliteDLLPath);
		g_ResLoader.SetDLLPath(bstrSatelliteDLLPath);

		IfFailGoCheck(m_pDTE->get_Windows(&pWindows), pWindows);

		pd.cbSizeofstruct=sizeof(PICTDESC);
		pd.picType=PICTYPE_BITMAP;
		pd.bmp.hbitmap = LoadResourceBitmap(_AtlModule.GetResourceInstance(), /*MAKEINTRESOURCE*/(TCHAR*)(IDB_REGBITMAP), &pd.bmp.hpal);
		OleCreatePictureIndirect(&pd, IID_IPictureDisp, FALSE, (LPVOID*)&pPictureDisp);
		pPictureDisp->QueryInterface(IID_IUnknown, (LPVOID*)&varPic.punkVal);
		varPic.vt = VT_UNKNOWN;

		if (ConnectMode == AddInDesignerObjects::ext_cm_External)
		{
			IfFailGoCheck(m_pDTE->get_AddIns(&pAddIns), pAddIns);
			IfFailGoCheck(pAddIns->Item(CComVariant(_T("RegExplore.Connect")), &m_pAddInInstance), m_pAddInInstance);

			if (m_pAddInInstance == NULL)
			{
				IfFailGoCheck(m_pDTE->get_AddIns(&pAddIns), pAddIns);
				IfFailGoCheck(pAddIns->Item(CComVariant(1), &m_pAddInInstance), m_pAddInInstance);
				IfFailGoCheck(pWindows->CreateToolWindow(m_pAddInInstance, bstrDocObjProgID, g_ResLoader.LoadString(IDS_REGISTRYEXPLORER), bstrGUIDTool, &m_pDocObjDisp, &m_pToolWindow), m_pToolWindow);
				pCtl = m_pDocObjDisp;
				pCtl->SetParent(m_pDTE);
			}
			else
			{
				VARIANT_BOOL vbConnected;

				IfFailGo(m_pAddInInstance->get_Connected(&vbConnected));
				if (vbConnected == VARIANT_FALSE)
				{
					IfFailGoCheck(pWindows->CreateToolWindow(m_pAddInInstance, bstrDocObjProgID, g_ResLoader.LoadString(IDS_REGISTRYEXPLORER), bstrGUIDTool, &m_pDocObjDisp, &m_pToolWindow), m_pToolWindow);
					pCtl = m_pDocObjDisp;
					pCtl->SetParent(m_pDTE);
				}
			}
		}
		else
		{
			IfFailGoCheck(pWindows->CreateToolWindow(m_pAddInInstance, bstrDocObjProgID, g_ResLoader.LoadString(IDS_REGISTRYEXPLORER), bstrGUIDTool, &m_pDocObjDisp, &m_pToolWindow), m_pToolWindow);
			pCtl = m_pDocObjDisp;
			pCtl->SetParent(m_pDTE);
		}

		if(SUCCEEDED(CComObject<CRegistryExplorer>::CreateInstance(&m_pRegistryExplorerAutObj)))
		{
			m_pRegistryExplorerAutObj->AddRef();
			CComQIPtr<IDispatch> pDisp;
			pDisp = m_pRegistryExplorerAutObj;
			m_pAddInInstance->put_Object(pDisp);
		}

		if(m_pToolWindow.p)
		{
			m_pToolWindow->SetTabPicture(varPic);
			m_pToolWindow->put_Visible(VARIANT_TRUE);
			m_pToolWindow->Activate();
		}
	}


	return S_OK;
Error:
	return hr;
}

STDMETHODIMP CConnect::OnDisconnection(ext_DisconnectMode /*RemoveMode*/, SAFEARRAY ** /*custom*/ )
{
	m_pDTE = NULL;
	m_pAddInInstance = NULL;
	return S_OK;
}

STDMETHODIMP CConnect::OnAddInsUpdate (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnStartupComplete (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnBeginShutdown (SAFEARRAY ** /*custom*/ )
{
	//Clear the parent on the control so it does not try to call back on us:
	CComQIPtr<IRegExploreCtl> pCtl;
	pCtl = m_pDocObjDisp;
	pCtl->SetParent(NULL);

	//Clear out any other interfaces we may have:
	m_pDTE = NULL;
	m_pToolWindow = NULL;
	m_pDocObjDisp = NULL;
	return S_OK;
}

STDMETHODIMP CConnect::QueryStatus(BSTR bstrCmdName, vsCommandStatusTextWanted NeededText, vsCommandStatus *pStatusOption, VARIANT * /*pvarCommandText*/)
{
	if(NeededText == vsCommandStatusTextWantedNone)
	{
		if(!_wcsicmp(bstrCmdName, L"RegExplore.Connect.RegExplore"))
		{
			*pStatusOption = (vsCommandStatus)(vsCommandStatusEnabled+vsCommandStatusSupported);
		}
	}
	return S_OK;
}

STDMETHODIMP CConnect::Exec(BSTR bstrCmdName, vsCommandExecOption ExecuteOption, VARIANT *pvarVariantIn, VARIANT * /*pvarVariantOut*/, VARIANT_BOOL *pvbHandled)
{
	*pvbHandled = VARIANT_FALSE;
	if(ExecuteOption == vsCommandExecOptionDoDefault)
	{
		if(!_wcsicmp(bstrCmdName, L"RegExplore.Connect.RegExplore"))
		{
			if((pvarVariantIn) && (pvarVariantIn->vt == VT_BSTR) && (pvarVariantIn->bstrVal))
			{
				USES_CONVERSION;
				//A value here means that the user typed a command (mostlikely on the command line) and it had a parameter. Try to discover that parameter here:
				WCHAR *pszCommandArg = wcschr(pvarVariantIn->bstrVal, ' ');
				if(pszCommandArg)
				{
					pszCommandArg++;
					if(m_pRegistryExplorerAutObj && pszCommandArg)
						m_pRegistryExplorerAutObj->NavigateToLocation(CComBSTR(pszCommandArg));
				}
			}
			m_pToolWindow->put_Visible(VARIANT_TRUE);

			*pvbHandled = VARIANT_TRUE;
			return S_OK;
		}
	}
	return S_OK;
}