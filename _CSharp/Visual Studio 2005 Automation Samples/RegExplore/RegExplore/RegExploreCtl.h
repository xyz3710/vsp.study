//Copyright (c) Microsoft Corporation.  All rights reserved.

// RegExploreCtl.h : Declaration of the CRegExploreCtl
#pragma once
#include "resource.h"       // main symbols
#include <atlctl.h>
#include "addin.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif


// CRegExploreCtl
class ATL_NO_VTABLE CRegExploreCtl :
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<IRegExploreCtl, &__uuidof(IRegExploreCtl), &__uuidof(RegExploreLib), /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IPersistStreamInitImpl<CRegExploreCtl>,
	public IOleControlImpl<CRegExploreCtl>,
	public IOleObjectImpl<CRegExploreCtl>,
	public IOleInPlaceActiveObjectImpl<CRegExploreCtl>,
	public IViewObjectExImpl<CRegExploreCtl>,
	public IOleInPlaceObjectWindowlessImpl<CRegExploreCtl>,
	public ISupportErrorInfo,
	public IPersistStorageImpl<CRegExploreCtl>,
	public ISpecifyPropertyPagesImpl<CRegExploreCtl>,
	public IQuickActivateImpl<CRegExploreCtl>,
#ifndef _WIN32_WCE
	public IDataObjectImpl<CRegExploreCtl>,
#endif
	public IProvideClassInfo2Impl<&CLSID_RegExploreCtl, NULL, &__uuidof(RegExploreLib)>,
#ifdef _WIN32_WCE // IObjectSafety is required on Windows CE for the control to be loaded correctly
	public IObjectSafetyImpl<CRegExploreCtl, INTERFACESAFE_FOR_UNTRUSTED_CALLER>,
#endif
	public CComCoClass<CRegExploreCtl, &CLSID_RegExploreCtl>,
	public CComCompositeControl<CRegExploreCtl>
{
public:

	CComQIPtr<EnvDTE::_DTE> m_pDTE; 
	HWND m_hTree;
	HWND m_hList;
	int nBinaryData;
	int nClosedFolder;
	int nComputer;
	int nOpenFolder;
	int nStringData;
	long lTreeWidth;
	HTREEITEM hTreeCurrentHilighted;
	int nListCurrentHilighted;
	HWND hOldCapture;
	BOOL fCaptured;
	CSimpleArray<long> ArrayAddedItems;

	void ModifyValue();
	BOOL IsComputerSelected();
	BOOL IsRootSelected();
	void GetKey(HWND m_hTree, HTREEITEM hItem, HKEY *hKey, CString &strKey);
	int GetImageOfType(DWORD dwType);

	class TaskListEventSink : public IDispEventImpl<1, TaskListEventSink, &__uuidof(EnvDTE::_dispTaskListEvents), &__uuidof(EnvDTE::__EnvDTE), 7, 0>
	{
	public:
		BEGIN_SINK_MAP(TaskListEventSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTaskListEvents), 4, OnTaskNavigated)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTaskListEvents), 2, OnTaskRemoved)
		END_SINK_MAP()
	public:
		CRegExploreCtl *m_pOwner;
		void SetOwner(CRegExploreCtl *pOwner)
		{
			m_pOwner = pOwner;
		}

		void __stdcall OnTaskNavigated(EnvDTE::TaskItem *pTaskItem, VARIANT_BOOL *NavigateHandled)
		{
			long lTemp = (long)pTaskItem;
			int nFound = m_pOwner->ArrayAddedItems.Find(lTemp);
			if (nFound != -1)
			{
				CComBSTR bstrDesc;
				*NavigateHandled = VARIANT_TRUE;
				pTaskItem->get_Description(&bstrDesc);
				m_pOwner->SelectItem(bstrDesc);
			}
		}

		void __stdcall OnTaskRemoved(EnvDTE::TaskItem *pTaskItem)
		{
			long lTemp = (long)pTaskItem;
			m_pOwner->ArrayAddedItems.Remove(lTemp);
		}
	};

	TaskListEventSink m_TaskListEventSink;

	CComPtr<EnvDTE::_TaskListEvents> pTLE;

	CRegExploreCtl()
	{
		m_bWindowOnly = TRUE;
		CalcExtent(m_sizeExtent);
	}

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_INSIDEOUT |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST
)

DECLARE_REGISTRY_RESOURCEID(IDR_REGEXPLORECTL)


BEGIN_COM_MAP(CRegExploreCtl)
	COM_INTERFACE_ENTRY(IRegExploreCtl)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IViewObjectEx)
	COM_INTERFACE_ENTRY(IViewObject2)
	COM_INTERFACE_ENTRY(IViewObject)
	COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceObject)
	COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
	COM_INTERFACE_ENTRY(IOleControl)
	COM_INTERFACE_ENTRY(IOleObject)
	COM_INTERFACE_ENTRY(IPersistStreamInit)
	COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
	COM_INTERFACE_ENTRY(IQuickActivate)
	COM_INTERFACE_ENTRY(IPersistStorage)
#ifndef _WIN32_WCE
	COM_INTERFACE_ENTRY(IDataObject)
#endif
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
#ifdef _WIN32_WCE // IObjectSafety is required on Windows CE for the control to be loaded correctly
	COM_INTERFACE_ENTRY_IID(IID_IObjectSafety, IObjectSafety)
#endif
END_COM_MAP()

BEGIN_PROP_MAP(CRegExploreCtl)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	// Example entries
	// PROP_ENTRY("Property Description", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()


BEGIN_MSG_MAP(CRegExploreCtl)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	NOTIFY_HANDLER(IDC_REGTREE, TVN_ITEMEXPANDING, OnTvnItemexpandingRegtree)
	NOTIFY_HANDLER(IDC_REGTREE, TVN_SELCHANGED, OnTvnSelchangedRegtree)
	NOTIFY_HANDLER(IDC_DATALIST, LVN_KEYDOWN, OnLvnKeydownDatalist)
	NOTIFY_HANDLER(IDC_DATALIST, LVN_ENDLABELEDIT, OnLvnEndlabeleditDatalist)
	COMMAND_HANDLER(IDC_MOUSEFRAME, BN_CLICKED, OnClickedMouseFrame)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

	COMMAND_ID_HANDLER(ID_LISTMENUNOSELECT_NEW_KEY, OnListMenuNoSelect_New_Key)
	COMMAND_ID_HANDLER(ID_LISTMENUNOSELECT_NEW_STRING, OnListMenuNoSelect_New_String)
	COMMAND_ID_HANDLER(ID_LISTMENUNOSELECT_NEW_BINARY, OnListMenuNoSelect_New_Binary)
	COMMAND_ID_HANDLER(ID_LISTMENUNOSELECT_NEW_DWORD, OnListMenuNoSelect_New_DWORD)
	COMMAND_ID_HANDLER(ID_LISTMENUSELECT_MODIFY, OnListMenuSelect_Modify)
	COMMAND_ID_HANDLER(ID_LISTMENUSELECT_DELETE, OnListMenuSelect_Delete)
	COMMAND_ID_HANDLER(ID_LISTMENUSELECT_RENAME, OnListMenuSelect_Rename)
	COMMAND_ID_HANDLER(ID_TREEMENU_EXPAND, OnTreeMenu_Expand)
	COMMAND_ID_HANDLER(ID_TREEMENU_DELETE, OnTreeMenu_Delete)
	COMMAND_ID_HANDLER(ID_TREEMENU_RENAME, OnTreeMenu_Rename)
	COMMAND_ID_HANDLER(IDM_CREATESHORTCUT, OnTreeMenu_CreateShortcut)
	COMMAND_ID_HANDLER(ID_TREEMENU_COPYKEYNAME, OnTreeMenu_CopyKeyName)
	COMMAND_ID_HANDLER(ID_TREEMENUMYCOMPUTERSELECT_COLLAPSE, OnTreeMenuMyComputerSelect_Collapse)
	COMMAND_ID_HANDLER(ID_DEFAULT_REFRESH, OnRefresh)
	COMMAND_ID_HANDLER(ID_DEFAULT_FIND, OnFind)


	MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)
	NOTIFY_HANDLER(IDC_REGTREE, NM_RCLICK, OnNMRclickRegtree)
	NOTIFY_HANDLER(IDC_DATALIST, NM_RCLICK, OnNMRclickDatalist)
	NOTIFY_HANDLER(IDC_DATALIST, NM_CLICK, OnNMClickDatalist)
	NOTIFY_HANDLER(IDC_DATALIST, LVN_ITEMACTIVATE, OnLvnItemActivateDatalist)
	NOTIFY_HANDLER(IDC_DATALIST, NM_DBLCLK, OnNMDblclkDatalist)
	CHAIN_MSG_MAP(CComCompositeControl<CRegExploreCtl>)
END_MSG_MAP()
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

BEGIN_SINK_MAP(CRegExploreCtl)
	//Make sure the Event Handlers have __stdcall calling convention
END_SINK_MAP()

	STDMETHOD(OnAmbientPropertyChange)(DISPID dispid)
	{
		if (dispid == DISPID_AMBIENT_BACKCOLOR)
		{
			SetBackgroundColorFromAmbient();
			FireViewChange();
		}
		return IOleControlImpl<CRegExploreCtl>::OnAmbientPropertyChange(dispid);
	}
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
	{
		static const IID* arr[] =
		{
			&__uuidof(IRegExploreCtl),
		};

		for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
		{
			if (InlineIsEqualGUID(*arr[i], riid))
				return S_OK;
		}
		return S_FALSE;
	}

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IRegExploreCtl

	enum { IDD = IDD_REGEXPLORECTL };

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}
public:
	STDMETHOD(SetParent)(IUnknown* pUnk);
public:
	STDMETHOD(Navigate)(BSTR Location);

public:
	void SelectItem(CComBSTR bstrItemText);
public:
	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
public:
	LRESULT OnTvnItemexpandingRegtree(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnTvnSelchangedRegtree(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnLvnKeydownDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnLvnEndlabeleditDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
public:
	LRESULT OnMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
public:
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnClickedMouseFrame(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	//ContextMenu handlers
	LRESULT OnListMenuNoSelect_New_Key(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnListMenuNoSelect_New_String(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnListMenuNoSelect_New_Binary(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnListMenuNoSelect_New_DWORD(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnListMenuSelect_Modify(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnListMenuSelect_Delete(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnListMenuSelect_Rename(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnTreeMenu_Expand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnTreeMenu_Delete(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnTreeMenu_Rename(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnTreeMenu_CreateShortcut(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnTreeMenu_CopyKeyName(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnTreeMenuMyComputerSelect_Collapse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnRefresh(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnFind(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
public:
	LRESULT OnLButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
public:
	LRESULT OnNMRclickRegtree(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnNMRclickDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnNMClickDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnLvnItemActivateDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
public:
	LRESULT OnNMDblclkDatalist(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
};

OBJECT_ENTRY_AUTO(__uuidof(RegExploreCtl), CRegExploreCtl)
