//Copyright (c) Microsoft Corporation.  All rights reserved.

#include "stdafx.h"
#include "ResLoader.h"

extern CResLoader g_ResLoader;

//There is a difference in the Win32 API when deleting Registry keys.
//  In Win9X, RegDeleteKey deletes the key and any sub keys. On WinNT,
//  RegDeleteKey deletes the key iff there are no sub keys, and does
//  not recursively delete the keys. This function deletes the key 
//  recursively...
LONG RecursiveDeleteKey(HKEY hKeyRoot, CString lpszSubKey)
{
	HKEY hKeyNew;
	LONG hRes = ERROR_SUCCESS;
	if((hRes = RegOpenKeyEx(hKeyRoot, lpszSubKey, 0, KEY_ALL_ACCESS, &hKeyNew)) == ERROR_SUCCESS)
	{
		int iKey = 0;
		TCHAR szName[MAX_PATH];
		DWORD szNameSize = MAX_PATH;
		while (RegEnumKey(hKeyNew, iKey, szName, szNameSize) == ERROR_SUCCESS)
		{
			hRes = RecursiveDeleteKey(hKeyNew, szName);
			if (hRes != ERROR_SUCCESS)
			{
				RegCloseKey(hKeyNew);
				return hRes;
			}
			szNameSize = MAX_PATH;
		}
	}
	RegCloseKey(hKeyNew);
	hRes = RegDeleteKey(hKeyRoot, lpszSubKey);
	return hRes;
}


void SaveOptions(BOOL fPersist)
{
	HKEY hKeyOptions;
	DWORD dwPersistOptions = (fPersist) ? 1 : 0;
	if(RegOpenKeyEx(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\VisualStudio\\8.0\\AddIns\\RegExplore.Connect\\Options\\Registry Explorer\\General"), 0, KEY_READ, &hKeyOptions) == ERROR_SUCCESS)
	{
		RegSetValueEx(hKeyOptions, _T("PersistShortcuts"), NULL, REG_DWORD, (LPBYTE)&dwPersistOptions, sizeof(DWORD));
		RegCloseKey(hKeyOptions);
	}
}

BOOL LoadOptions()
{
	DWORD dwPersistOptions = 0;
	HKEY hKeyOptions;
	DWORD dwType;
	DWORD dwSizeData = sizeof(DWORD);
	if(RegOpenKeyEx(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\VisualStudio\\8.0\\AddIns\\RegExplore.Connect\\Options\\Registry Explorer\\General"), 0, KEY_READ, &hKeyOptions) == ERROR_SUCCESS)
	{
		if((RegQueryValueEx(hKeyOptions, _T("PersistShortcuts"), NULL, &dwType, (LPBYTE)&dwPersistOptions, &dwSizeData) != ERROR_SUCCESS) || (dwType != REG_DWORD) || (dwSizeData != sizeof(DWORD)))
			dwPersistOptions = 0;
		RegCloseKey(hKeyOptions);
	}
	return (dwPersistOptions) ? TRUE : FALSE;
}

CString GetStringOfValue(BYTE* byteValue, DWORD dwType, DWORD dwSize)
{
	CString pszOutputString;
	switch (dwType)
	{
	case REG_DWORD:
		{
			union
			{
				BYTE b[4];
				DWORD w;
			} temp;
			temp.w = 0;
			for (int i = 0 ; i < 4 ; i++)
				temp.b[i] = byteValue[i];
			pszOutputString.Format("%ld", temp.w);
			break;
		}
	case REG_SZ:
	case REG_EXPAND_SZ:
		{
			pszOutputString = (TCHAR*)byteValue;
			break;
		}
	case REG_BINARY:
		{
			TCHAR szTemp[3];
			if (dwSize == 0)
			{
				pszOutputString = g_ResLoader.TLoadString(IDS_ZEROLENBINARY);
			}
			else
			{
				for (DWORD i = 0 ; i < dwSize ; i++)
				{
					wsprintf(szTemp, _T("%02x "), byteValue[i]);
					pszOutputString.Append(szTemp);
				}
			}
			break;
		}
	case REG_LINK:
	case REG_DWORD_BIG_ENDIAN:
	case REG_MULTI_SZ:
	case REG_NONE:
	case REG_RESOURCE_LIST:
		{
			pszOutputString = g_ResLoader.TLoadString(IDS_UNSUPPTYPE);
		}
	default:
		{
			pszOutputString = g_ResLoader.TLoadString(IDS_UNKTYPE);
		}
	}
	return pszOutputString;
}


CString GetDataTypeName(DWORD dwType)
{
	switch(dwType)
	{
	case REG_SZ:				return CString("REG_SZ");
	case REG_DWORD:				return CString("REG_DWORD");
	case REG_BINARY:			return CString("REG_BINARY");
	case REG_DWORD_BIG_ENDIAN:	return CString("REG_DWORD_BIG_ENDIAN");
	case REG_EXPAND_SZ:			return CString("REG_EXPAND_SZ");
	case REG_LINK:				return CString("REG_LINK");
	case REG_MULTI_SZ:			return CString("REG_MULTI_SZ");
	case REG_NONE:				return CString("REG_NONE");
	case REG_RESOURCE_LIST:		return CString("REG_RESOURCE_LIST");
	default:					return g_ResLoader.TLoadString(IDS_UNKTYPE);
	}
}

CString GetRootNameFromKey(HKEY hKey)
{
	switch ((long)hKey)
	{
	case HKEY_CLASSES_ROOT:		return CString("HKEY_CLASSES_ROOT");
	case HKEY_CURRENT_CONFIG:	return CString("HKEY_CURRENT_CONFIG");
	case HKEY_CURRENT_USER:		return CString("HKEY_CURRENT_USER");
	case HKEY_LOCAL_MACHINE:	return CString("HKEY_LOCAL_MACHINE");
	case HKEY_USERS:			return CString("HKEY_USERS");
	case HKEY_PERFORMANCE_DATA: return CString("HKEY_PERFORMANCE_DATA");
	case HKEY_DYN_DATA:			return CString("HKEY_DYN_DATA");
	default:					return CString("");
	}
}

HKEY GetKeyFromRoot(TCHAR *pszName)
{
	if (_tcscmp(pszName, _T("HKEY_CLASSES_ROOT")) == 0)
		return HKEY_CLASSES_ROOT;
	else if (_tcscmp(pszName, _T("HKEY_CURRENT_CONFIG")) == 0)
		return HKEY_CURRENT_CONFIG;
	else if (_tcscmp(pszName, _T("HKEY_CURRENT_USER")) == 0)
		return HKEY_CURRENT_USER;
	else if (_tcscmp(pszName, _T("HKEY_LOCAL_MACHINE")) == 0)
		return HKEY_LOCAL_MACHINE;
	else if (_tcscmp(pszName, _T("HKEY_USERS")) == 0)
		return HKEY_USERS;
	else if (_tcscmp(pszName, _T("HKEY_PERFORMANCE_DATA")) == 0)
		return HKEY_PERFORMANCE_DATA;
	else if (_tcscmp(pszName, _T("HKEY_DYN_DATA")) == 0)
		return HKEY_DYN_DATA;
	return NULL;
}

void StripKeys(CString pszIn, TCHAR **ppszOutMain, TCHAR **ppszKey)
{
	TCHAR *pszTemp = _tcsrchr(pszIn.GetBuffer(), '\\');
	if (!pszTemp)
	{
		*ppszOutMain = *ppszKey = NULL;
		return;
	}
	*ppszOutMain = new TCHAR[pszIn.GetLength() + 1];
	_tcscpy(*ppszOutMain, pszIn);
	pszTemp = _tcsrchr(*ppszOutMain, '\\');

	TCHAR *pszTemp2 = CharNext(pszTemp);
	*pszTemp = 0;
	*ppszKey = new TCHAR[_tcslen(pszTemp2)+1];
	_tcscpy(*ppszKey, pszTemp2);
}


HPALETTE CreateDIBPalette (LPBITMAPINFO lpbmi, LPINT lpiNumColors) 
{ 
	LPBITMAPINFOHEADER  lpbi;
	LPLOGPALETTE     lpPal;
	HANDLE           hLogPal;
	HPALETTE         hPal = NULL;
	int              i;

	lpbi = (LPBITMAPINFOHEADER)lpbmi;
	if (lpbi->biBitCount <= 8)
		*lpiNumColors = (1 << lpbi->biBitCount);
	else
		*lpiNumColors = 0;  // No palette needed for 24 BPP DIB

	if (lpbi->biClrUsed > 0)
		*lpiNumColors = lpbi->biClrUsed;  // Use biClrUsed

	if (*lpiNumColors)
	{
		hLogPal = GlobalAlloc (GHND, sizeof (LOGPALETTE) + sizeof (PALETTEENTRY) * (*lpiNumColors));
		lpPal = (LPLOGPALETTE) GlobalLock (hLogPal);
		lpPal->palVersion    = 0x300;
		lpPal->palNumEntries = *lpiNumColors;

		for (i = 0;  i < *lpiNumColors;  i++)
		{
			lpPal->palPalEntry[i].peRed   = lpbmi->bmiColors[i].rgbRed;
			lpPal->palPalEntry[i].peGreen = lpbmi->bmiColors[i].rgbGreen;
			lpPal->palPalEntry[i].peBlue  = lpbmi->bmiColors[i].rgbBlue;
			lpPal->palPalEntry[i].peFlags = 0;
		}
		hPal = CreatePalette (lpPal);
		GlobalUnlock (hLogPal);
		GlobalFree   (hLogPal);
	}
	return hPal;
} 

HBITMAP LoadResourceBitmap(HINSTANCE hInstance, TCHAR *pszString, HPALETTE FAR* lphPalette)
{
	HGLOBAL hGlobal;
	HBITMAP hBitmapFinal = NULL;
	LPBITMAPINFOHEADER  lpbi;
	HDC hdc;
	int iNumColors;
	HRSRC hRsrc = FindResource(hInstance, pszString, RT_BITMAP);
	if (hRsrc)
	{
		hGlobal = LoadResource(hInstance, hRsrc);
		lpbi = (LPBITMAPINFOHEADER)LockResource(hGlobal);

		hdc = GetDC(NULL);
		*lphPalette =  CreateDIBPalette ((LPBITMAPINFO)lpbi, &iNumColors);
		if (*lphPalette)
		{
			SelectPalette(hdc,*lphPalette,FALSE);
			RealizePalette(hdc);
		}

		hBitmapFinal = CreateDIBitmap(hdc, (LPBITMAPINFOHEADER)lpbi, (LONG)CBM_INIT, (LPSTR)lpbi + lpbi->biSize + iNumColors * sizeof(RGBQUAD), (LPBITMAPINFO)lpbi, DIB_RGB_COLORS );

		ReleaseDC(NULL,hdc);
		UnlockResource(hGlobal);
		FreeResource(hGlobal);
	}
	return (hBitmapFinal);
} 

#include "addin.h"
EXTERN_C const IID IID_IKeyProperties = __uuidof(IKeyProperties);
EXTERN_C const IID IID_IRegExplorerProperties = __uuidof(IRegExplorerProperties);
EXTERN_C const IID IID_IRegExplorerOptions = __uuidof(IRegExplorerOptions);
EXTERN_C const IID LIBID_RegExploreLib = __uuidof(RegExploreLib);
EXTERN_C const CLSID CLSID_Connect = __uuidof(Connect);
EXTERN_C const CLSID CLSID_RegExploreCtl = __uuidof(RegExploreCtl);
EXTERN_C const CLSID CLSID_RegExplorerOptions = __uuidof(RegExplorerOptions);
