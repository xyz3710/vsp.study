

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Mon Dec 12 12:13:26 2005
 */
/* Compiler settings for .\AddIn.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IRegExploreCtl,0x822DDBB6,0xFC97,0x4734,0x89,0xD7,0x29,0x9A,0xF9,0x2D,0x7D,0xF0);


MIDL_DEFINE_GUID(IID, IID_IRegistryExplorer,0xD2D540DF,0xAB85,0x4B2B,0x97,0x54,0x53,0x6F,0x2F,0x02,0x66,0x02);


MIDL_DEFINE_GUID(IID, IID_IRegExplorerOptions,0xBF0C16A9,0x0FA1,0x4CB4,0x94,0x9D,0x74,0x7A,0x0C,0xF2,0xA3,0x37);


MIDL_DEFINE_GUID(IID, IID_IRegExplorerProperties,0x12E6D6A2,0x1192,0x48FB,0xB8,0xA0,0x58,0xFD,0x95,0xF0,0x58,0x63);


MIDL_DEFINE_GUID(IID, IID_IKeyProperties,0x9E9F0439,0x89AC,0x4093,0xB4,0x84,0xD6,0x5C,0x69,0xD1,0xE6,0x3D);


MIDL_DEFINE_GUID(IID, LIBID_RegExploreLib,0xB620FCE3,0xA05B,0x4BE7,0xA2,0xC2,0x5A,0xB0,0x1D,0xC0,0x51,0x73);


MIDL_DEFINE_GUID(CLSID, CLSID_Connect,0xE588177B,0xDE48,0x464F,0xB7,0x31,0xB9,0xEF,0xDD,0xEC,0xBE,0x07);


MIDL_DEFINE_GUID(CLSID, CLSID_RegExploreCtl,0x98B6C0EB,0x542C,0x4D82,0x83,0xF3,0x01,0x3E,0x66,0xEC,0xE8,0xD1);


MIDL_DEFINE_GUID(CLSID, CLSID_RegistryExplorer,0x7D597871,0xEAE2,0x4046,0x87,0x91,0xBD,0x96,0x55,0x51,0x4A,0x3D);


MIDL_DEFINE_GUID(CLSID, CLSID_RegExplorerOptions,0x72356BE7,0xB4CC,0x4518,0x91,0x24,0x75,0xED,0xC4,0xF8,0xC6,0xA2);


MIDL_DEFINE_GUID(CLSID, CLSID_RegExplorerProperties,0xE38F0506,0x14C2,0x425B,0xBD,0x6A,0xE3,0x97,0xE6,0xF1,0x1F,0x3E);


MIDL_DEFINE_GUID(CLSID, CLSID_KeyProperties,0x1D2EF55C,0xC73B,0x4094,0xA0,0xF1,0x61,0x29,0x97,0xCB,0xBA,0x8B);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



