'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System
Imports Microsoft.VisualStudio.CommandBars
Imports Extensibility
Imports EnvDTE
Imports EnvDTE80
Imports System.IO

Public Class Connect

    Implements IDTExtensibility2
    Implements IDTCommandTarget

    Dim _applicationObject As DTE2
    Dim _addInInstance As AddIn
    Private WithEvents buildEvents As EnvDTE.BuildEvents

    '''<summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
    Public Sub New()

    End Sub

    '''<summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
    '''<param name='application'>Root object of the host application.</param>
    '''<param name='connectMode'>Describes how the Add-in is being loaded.</param>
    '''<param name='addInInst'>Object representing this Add-in.</param>
    '''<remarks></remarks>
    Public Sub OnConnection(ByVal application As Object, ByVal connectMode As ext_ConnectMode, ByVal addInInst As Object, ByRef custom As Array) Implements IDTExtensibility2.OnConnection
        _applicationObject = CType(application, DTE2)
        _addInInstance = CType(addInInst, AddIn)
        buildEvents = _applicationObject.Events.BuildEvents

        If connectMode = ext_ConnectMode.ext_cm_UISetup Then

            Dim commands As Commands2 = CType(_applicationObject.Commands, Commands2)
            Dim toolsMenuName As String
            Try

                'If you would like to move the command to a different menu, change the word "Tools" to the 
                '  English version of the menu. This code will take the culture, append on the name of the menu
                '  then add the command to that menu. You can find a list of all the top-level menus in the file
                '  CommandBar.resx.
                Dim resourceManager As System.Resources.ResourceManager = New System.Resources.ResourceManager("BuildRules.CommandBar", System.Reflection.Assembly.GetExecutingAssembly())

                Dim cultureInfo As System.Globalization.CultureInfo = New System.Globalization.CultureInfo(_applicationObject.LocaleID)
                toolsMenuName = resourceManager.GetString(String.Concat(cultureInfo.TwoLetterISOLanguageName, "Tools"))

            Catch e As Exception
                'We tried to find a localized version of the word Tools, but one was not found.
                '  Default to the en-US word, which may work for the current culture.
                toolsMenuName = "Tools"
            End Try

            'Place the command on the tools menu.
            'Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
            Dim commandBars As CommandBars = CType(_applicationObject.CommandBars, CommandBars)
            Dim menuBarCommandBar As CommandBar = commandBars.Item("MenuBar")

            'Find the Tools command bar on the MenuBar command bar:
            Dim toolsControl As CommandBarControl = menuBarCommandBar.Controls.Item(toolsMenuName)
            Dim toolsPopup As CommandBarPopup = CType(toolsControl, CommandBarPopup)

            Try
                'Add a command to the Commands collection:
                Dim command As Command = commands.AddNamedCommand2(_addInInstance, "BuildRules", "Solution Build Rules", "Executes the command for BuildRules", True, 0, Nothing, CType(vsCommandStatus.vsCommandStatusSupported, Integer) + CType(vsCommandStatus.vsCommandStatusEnabled, Integer), vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton)

                'Find the appropriate command bar on the MenuBar command bar:
                command.AddControl(toolsPopup.CommandBar, 1)
            Catch argumentException As System.ArgumentException
                'If we are here, then the exception is probably because a command with that name
                '  already exists. If so there is no need to recreate the command and we can 
                '  safely ignore the exception.
            End Try

        End If
    End Sub

    '''<summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
    '''<param name='disconnectMode'>Describes how the Add-in is being unloaded.</param>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnDisconnection(ByVal disconnectMode As ext_DisconnectMode, ByRef custom As Array) Implements IDTExtensibility2.OnDisconnection
    End Sub

    '''<summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification that the collection of Add-ins has changed.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnAddInsUpdate(ByRef custom As Array) Implements IDTExtensibility2.OnAddInsUpdate
    End Sub

    '''<summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnStartupComplete(ByRef custom As Array) Implements IDTExtensibility2.OnStartupComplete
    End Sub

    '''<summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnBeginShutdown(ByRef custom As Array) Implements IDTExtensibility2.OnBeginShutdown
    End Sub

    '''<summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
    '''<param name='commandName'>The name of the command to determine state for.</param>
    '''<param name='neededText'>Text that is needed for the command.</param>
    '''<param name='status'>The state of the command in the user interface.</param>
    '''<param name='commandText'>Text requested by the neededText parameter.</param>
    '''<remarks></remarks>
    Public Sub QueryStatus(ByVal commandName As String, ByVal neededText As vsCommandStatusTextWanted, ByRef status As vsCommandStatus, ByRef commandText As Object) Implements IDTCommandTarget.QueryStatus
        If neededText = vsCommandStatusTextWanted.vsCommandStatusTextWantedNone Then
            If commandName = "BuildRules.Connect.BuildRules" Then
                status = CType(vsCommandStatus.vsCommandStatusEnabled + vsCommandStatus.vsCommandStatusSupported, vsCommandStatus)
            Else
                status = vsCommandStatus.vsCommandStatusUnsupported
            End If
        End If
    End Sub

    '''<summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
    '''<param name='commandName'>The name of the command to execute.</param>
    '''<param name='executeOption'>Describes how the command should be run.</param>
    '''<param name='varIn'>Parameters passed from the caller to the command handler.</param>
    '''<param name='varOut'>Parameters passed from the command handler to the caller.</param>
    '''<param name='handled'>Informs the caller if the command was handled or not.</param>
    '''<remarks></remarks>
    Public Sub Exec(ByVal commandName As String, ByVal executeOption As vsCommandExecOption, ByRef varIn As Object, ByRef varOut As Object, ByRef handled As Boolean) Implements IDTCommandTarget.Exec
        handled = False
        If executeOption = vsCommandExecOption.vsCommandExecOptionDoDefault Then
            If commandName = "BuildRules.Connect.BuildRules" Then
                'Create an instance of the WinWrapper class to make VS a parent to the winform:
                Dim rules As New RulesFrm()
                Dim wrapper As New WinWrapper()
                wrapper._applicationObject = _applicationObject
                rules._applicationObject = _applicationObject
                rules.ShowDialog(wrapper)
                handled = True
                Exit Sub
            End If
        End If
    End Sub

    Private Sub buildEvents_OnBuildBegin(ByVal Scope As EnvDTE.vsBuildScope, ByVal Action As EnvDTE.vsBuildAction) Handles buildEvents.OnBuildBegin
        If (Scope = EnvDTE.vsBuildScope.vsBuildScopeSolution) Then
            RunSolutionRules(True)
        End If
    End Sub

    Private Sub buildEvents_OnBuildDone(ByVal Scope As EnvDTE.vsBuildScope, ByVal Action As EnvDTE.vsBuildAction) Handles buildEvents.OnBuildDone
        If (Scope = EnvDTE.vsBuildScope.vsBuildScopeSolution) Then
            RunSolutionRules(False)
        End If
    End Sub

    Private Sub buildEvents_OnBuildProjConfigBegin(ByVal Project As String, ByVal ProjectConfig As String, ByVal Platform As String, ByVal SolutionConfig As String) Handles buildEvents.OnBuildProjConfigBegin
        RunProjectRules(Project, ProjectConfig, Platform, True)
    End Sub

    Private Sub buildEvents_OnBuildProjConfigDone(ByVal Project As String, ByVal ProjectConfig As String, ByVal Platform As String, ByVal SolutionConfig As String, ByVal Success As Boolean) Handles buildEvents.OnBuildProjConfigDone
        RunProjectRules(Project, ProjectConfig, Platform, False)
    End Sub

    Private Sub RunSolutionRules(ByVal pre As Boolean)
        Dim globals As EnvDTE.Globals
        Dim names As Object()
        Dim i As Integer
        Dim prepost As String = "PostBuildRule"

        If (pre = True) Then
            prepost = "PreBuildRule"
        End If

        globals = _applicationObject.Solution.Globals
        If Not (globals Is Nothing) Then
            names = globals.VariableNames
            For i = 0 To names.Length - 1
                If (names(i).ToString().StartsWith(prepost)) Then

                    Dim rules As String()
                    If (globals.VariableValue(names(i)) <> "") Then
                        rules = globals.VariableValue(names(i)).Split("""")
                        Dim outputWindow As EnvDTE.OutputWindow
                        Dim outputWindowBuildPane As EnvDTE.OutputWindowPane
                        outputWindow = CType(_applicationObject.Windows.Item(EnvDTE.Constants.vsWindowKindOutput).Object(), OutputWindow)
                        outputWindowBuildPane = outputWindow.OutputWindowPanes.Item("{1BD8A850-02D1-11D1-BEE7-00A0C913D1F8}")

                        Try
                            Dim p As System.Diagnostics.Process
                            rules(1) = rules(1).TrimStart(" ")

                            'Fix up replacements in the command:
                            rules(2) = rules(2).Replace("$(ConfigurationName)", _applicationObject.Solution.SolutionBuild.ActiveConfiguration.Name)
                            rules(2) = rules(2).Replace("$(SolutionPath)", _applicationObject.Solution.FullName)
                            rules(2) = rules(2).Replace("$(SolutionFolder)", Path.GetDirectoryName(_applicationObject.Solution.FullName))
                            rules(2) = rules(2).Replace("$(Platform)", "")
                            rules(2) = rules(2).Replace("$(ProjectName)", Path.GetFileNameWithoutExtension(_applicationObject.Solution.FullName))

                            If (rules(1).StartsWith("echo")) Then 'If the program name is 'echo' then just display the text
                                outputWindowBuildPane.OutputString(rules(2) + vbLf)
                            Else
                                'Create and run the process. Redirect stdout so that we can display it in the output window
                                p = New System.Diagnostics.Process()
                                p.StartInfo.FileName = rules(1)
                                p.StartInfo.Arguments = rules(2)
                                p.StartInfo.CreateNoWindow = True
                                p.StartInfo.UseShellExecute = False
                                p.StartInfo.RedirectStandardOutput = True
                                p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
                                p.Start()
                                While (p.HasExited = False)
                                    Dim line As String
                                    line = p.StandardOutput.ReadToEnd()
                                    outputWindowBuildPane.OutputString(line + vbLf)
                                End While
                            End If
                        Catch ex As Exception
                            outputWindowBuildPane.OutputString(ex.ToString() + vbLf)
                        End Try
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub RunProjectRules(ByVal project As String, ByVal projectConfig As String, ByVal platform As String, ByVal pre As Boolean)
        Dim proj As EnvDTE.Project
        Dim globals As EnvDTE.Globals
        Dim names As Object()
        Dim i As Integer
        Dim prepost As String = "PostBuildRule"

        If (pre = True) Then
            prepost = "PreBuildRule"
        End If
        proj = _applicationObject.Solution.Projects.Item(project)

        globals = proj.Globals
        If Not (globals Is Nothing) Then
            names = globals.VariableNames
            For i = 0 To names.Length - 1
                If (names(i).ToString().StartsWith(prepost)) Then

                    Dim rules As String()
                    If (globals.VariableValue(names(i)) <> "") Then
                        rules = globals.VariableValue(names(i)).Split("""")
                        Dim outputWindow As EnvDTE.OutputWindow
                        Dim outputWindowBuildPane As EnvDTE.OutputWindowPane
                        outputWindow = CType(_applicationObject.Windows.Item(EnvDTE.Constants.vsWindowKindOutput).Object(), OutputWindow)
                        outputWindowBuildPane = outputWindow.OutputWindowPanes.Item("{1BD8A850-02D1-11D1-BEE7-00A0C913D1F8}")

                        Try
                            Dim p As System.Diagnostics.Process
                            rules(1) = rules(1).TrimStart(" ")

                            'Fix up replacements in the command:
                            projectConfig = projectConfig.Substring(0, projectConfig.Length - 1)  'work around a bug with the configuration name
                            rules(2) = rules(2).Replace("$(SolutionPath)", _applicationObject.Solution.FullName)
                            rules(2) = rules(2).Replace("$(SolutionFolder)", Path.GetDirectoryName(_applicationObject.Solution.FullName))
                            rules(2) = rules(2).Replace("$(Platform)", platform)
                            rules(2) = rules(2).Replace("$(ProjectName)", project)
                            rules(2) = rules(2).Replace("$(ConfigurationName)", projectConfig)

                            If (rules(1).StartsWith("echo")) Then 'If the program name is 'echo' then just display the text
                                outputWindowBuildPane.OutputString(rules(2) + vbLf)
                            Else
                                'Create and run the process. Redirect stdout so that we can display it in the output window
                                p = New System.Diagnostics.Process()
                                p.StartInfo.FileName = rules(1)
                                p.StartInfo.Arguments = rules(2)
                                p.StartInfo.CreateNoWindow = True
                                p.StartInfo.UseShellExecute = False
                                p.StartInfo.RedirectStandardOutput = True
                                p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
                                p.Start()
                                While (p.HasExited = False)
                                    Dim line As String
                                    line = p.StandardOutput.ReadToEnd()
                                    outputWindowBuildPane.OutputString(line + vbLf)
                                End While
                            End If
                        Catch ex As Exception
                            outputWindowBuildPane.OutputString(ex.ToString() + vbLf)
                        End Try
                    End If
                End If
            Next
        End If
    End Sub
End Class
Public Class WinWrapper
    Implements System.Windows.Forms.IWin32Window
    Overridable ReadOnly Property Handle() As System.IntPtr Implements System.Windows.Forms.IWin32Window.Handle
        Get
            Dim iptr As New System.IntPtr(_applicationObject.MainWindow.HWnd)
            Return iptr
        End Get
    End Property

    Public _applicationObject As EnvDTE80.DTE2
End Class