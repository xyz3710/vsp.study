'Copyright (c) Microsoft Corporation.  All rights reserved.

Public Class RulesFrm
    Public _applicationObject As EnvDTE80.DTE2

    Private Sub ResetGlobals(ByVal globals As EnvDTE.Globals, ByVal prebuild As Boolean)
        Dim listViewItem As System.Windows.Forms.ListViewItem
        Dim i As Integer = 0

        ClearGlobalsOfRules(globals, prebuild)

        For Each listViewItem In rulesListView.Items
            If (prebuild = True) Then
                globals.VariableValue("PreBuildRule" + CStr(i)) = listViewItem.Text
                globals.VariablePersists("PreBuildRule" + CStr(i)) = True
                i = i + 1
            Else
                globals.VariableValue("PostBuildRule" + CStr(i)) = listViewItem.Text
                globals.VariablePersists("PostBuildRule" + CStr(i)) = True
                i = i + 1
            End If
        Next
    End Sub

    Private Sub addnewButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addnewButton.Click
        Dim newRule As New NewRuleFrm()
        If (newRule.ShowDialog(Me) = Windows.Forms.DialogResult.OK) Then
            Dim pre As Boolean
            If (projectsTreeView.SelectedNode.Parent.Text = "Pre-build rules") Then
                pre = True
            Else
                pre = False
            End If

            rulesListView.Items.Add("""" + newRule._program + """ " + newRule._commandLine)

            ResetGlobals(GetGlobalsOfSelected(), pre)
        End If
    End Sub

    Private Sub removeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles removeButton.Click
        Dim pre As Boolean

        If (rulesListView.SelectedItems.Count > 0) Then
            rulesListView.SelectedItems.Item(0).Remove()
            If (rulesListView.SelectedItems.Count > 0) Then
                rulesListView.Items.Item(0).Selected = True
            End If
        End If

        If (projectsTreeView.SelectedNode.Parent.Text = "Pre-build rules") Then
            pre = True
        Else
            pre = False
        End If
        ResetGlobals(GetGlobalsOfSelected(), pre)
    End Sub

    Private Sub moveupButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles moveupButton.Click
        Dim pre As Boolean

        If (rulesListView.SelectedItems.Count > 0) Then
            If (rulesListView.SelectedIndices.Item(0) <> 0) Then
                Dim currIndex As Integer
                currIndex = rulesListView.SelectedIndices.Item(0)
                Dim str1 As String
                str1 = rulesListView.Items.Item(currIndex).Text
                rulesListView.Items.Item(currIndex).Text = rulesListView.Items.Item(currIndex - 1).Text
                rulesListView.Items.Item(currIndex - 1).Text = str1
                rulesListView.Items.Item(currIndex - 1).Selected = True
            End If
        End If

        If (projectsTreeView.SelectedNode.Parent.Text = "Pre-build rules") Then
            pre = True
        Else
            pre = False
        End If
        ResetGlobals(GetGlobalsOfSelected(), pre)
    End Sub

    Private Sub movedownButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles movedownButton.Click
        Dim pre As Boolean

        If (rulesListView.SelectedItems.Count > 0) Then
            If (rulesListView.SelectedIndices.Item(0) <> rulesListView.Items.Count - 1) Then
                Dim currIndex As Integer
                currIndex = rulesListView.SelectedIndices.Item(0)
                Dim str1 As String
                str1 = rulesListView.Items.Item(currIndex).Text
                rulesListView.Items.Item(currIndex).Text = rulesListView.Items.Item(currIndex + 1).Text
                rulesListView.Items.Item(currIndex + 1).Text = str1
                rulesListView.Items.Item(currIndex + 1).Selected = True
            End If
        End If

        If (projectsTreeView.SelectedNode.Parent.Text = "Pre-build rules") Then
            pre = True
        Else
            pre = False
        End If
        ResetGlobals(GetGlobalsOfSelected(), pre)
    End Sub

    Private Sub RulesFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim solutionNode As System.Windows.Forms.TreeNode
        Dim projectNode As System.Windows.Forms.TreeNode
        Dim preBuildNode As System.Windows.Forms.TreeNode
        Dim postBuildNode As System.Windows.Forms.TreeNode
        Dim project As EnvDTE.Project

        preBuildNode = projectsTreeView.Nodes.Add("Pre-build rules")
        postBuildNode = projectsTreeView.Nodes.Add("Post-build rules")

        solutionNode = preBuildNode.Nodes.Add("Solution")
        solutionNode.Tag = _applicationObject.Solution

        solutionNode = postBuildNode.Nodes.Add("Solution")
        solutionNode.Tag = _applicationObject.Solution

        For Each project In _applicationObject.Solution.Projects
            Dim globals As EnvDTE.Globals
            Try
                globals = project.Globals
            Catch
                globals = Nothing
            End Try

            If Not (globals Is Nothing) Then
                projectNode = preBuildNode.Nodes.Add(project.Name)
                projectNode.Tag = project
                projectNode = postBuildNode.Nodes.Add(project.Name)
                projectNode.Tag = project
            End If
        Next
    End Sub

    Private Sub ClearGlobalsOfRules(ByVal globals As EnvDTE.Globals, ByVal pre As Boolean)
        Dim names As Object()
        Dim i As Integer
        names = globals.VariableNames
        If Not (names Is Nothing) Then
            For i = 0 To names.Length - 1
                If (pre = True) Then
                    If (names(i).ToString().StartsWith("PreBuildRule")) Then
                        globals.VariablePersists(names(i)) = False
                        globals.VariableValue(names(i)) = ""
                    End If
                Else
                    If (names(i).ToString().StartsWith("PostBuildRule")) Then
                        globals.VariablePersists(names(i)) = False
                        globals.VariableValue(names(i)) = ""
                    End If
                End If
            Next
        End If
    End Sub

    Private Function GetGlobalsOfSelected() As EnvDTE.Globals
        Dim project As EnvDTE.Project
        Dim solution As EnvDTE.Solution
        Try
            project = CType(projectsTreeView.SelectedNode.Tag, EnvDTE.Project)
            Return project.Globals
        Catch
            Try
                solution = CType(projectsTreeView.SelectedNode.Tag, EnvDTE.Solution)
                Return solution.Globals
            Catch
            End Try
        End Try
        Return Nothing
    End Function

    Private Sub projectsTreeView_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles projectsTreeView.AfterSelect
        Dim globals As EnvDTE.Globals
        Dim names As Object()
        Dim i As Integer
        rulesListView.Items.Clear()

        addnewButton.Enabled = False
        moveupButton.Enabled = False
        movedownButton.Enabled = False
        removeButton.Enabled = False

        globals = GetGlobalsOfSelected()

        If Not (globals Is Nothing) Then
            addnewButton.Enabled = True
            names = globals.VariableNames
            If Not (names Is Nothing) Then
                For i = 0 To names.Length - 1
                    If Not (projectsTreeView.SelectedNode.Parent Is Nothing) Then
                        Dim value As String
                        If (projectsTreeView.SelectedNode.Parent.Text = "Pre-build rules") Then
                            If (globals.VariableExists("PreBuildRule" + CStr(i))) Then
                                value = globals.VariableValue("PreBuildRule" + CStr(i))
                                If (value <> "") Then
                                    rulesListView.Items.Add(value)
                                Else
                                    Exit For
                                End If
                            End If
                        ElseIf (projectsTreeView.SelectedNode.Parent.Text = "Post-build rules") Then
                            If (globals.VariableExists("PostBuildRule" + CStr(i))) Then
                                value = globals.VariableValue("PostBuildRule" + CStr(i))
                                If (value <> "") Then
                                    rulesListView.Items.Add(value)
                                Else
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub rulesListView_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rulesListView.SelectedIndexChanged
        If (rulesListView.SelectedItems.Count > 0) Then
            removeButton.Enabled = True
            moveupButton.Enabled = True
            movedownButton.Enabled = True
        Else
            removeButton.Enabled = False
            moveupButton.Enabled = False
            movedownButton.Enabled = False
        End If
    End Sub
End Class