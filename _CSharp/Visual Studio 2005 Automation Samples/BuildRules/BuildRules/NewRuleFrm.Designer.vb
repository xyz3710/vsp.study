'Copyright (c) Microsoft Corporation.  All rights reserved.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewRuleFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.pasteButton = New System.Windows.Forms.Button
        Me.paramsListBox = New System.Windows.Forms.ListBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cancelButton = New System.Windows.Forms.Button
        Me.okButton = New System.Windows.Forms.Button
        Me.programTextBox = New System.Windows.Forms.TextBox
        Me.commandlineTextBox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.browseButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'pasteButton
        '
        Me.pasteButton.Location = New System.Drawing.Point(386, 112)
        Me.pasteButton.Name = "pasteButton"
        Me.pasteButton.Size = New System.Drawing.Size(75, 23)
        Me.pasteButton.TabIndex = 19
        Me.pasteButton.Text = "Paste"
        '
        'paramsListBox
        '
        Me.paramsListBox.Location = New System.Drawing.Point(12, 112)
        Me.paramsListBox.Name = "paramsListBox"
        Me.paramsListBox.Size = New System.Drawing.Size(368, 69)
        Me.paramsListBox.TabIndex = 18
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(9, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(456, 16)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Available replacement parameters:"
        '
        'cancelButton
        '
        Me.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cancelButton.Location = New System.Drawing.Point(385, 187)
        Me.cancelButton.Name = "cancelButton"
        Me.cancelButton.Size = New System.Drawing.Size(75, 23)
        Me.cancelButton.TabIndex = 16
        Me.cancelButton.Text = "Cancel"
        '
        'okButton
        '
        Me.okButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.okButton.Location = New System.Drawing.Point(304, 187)
        Me.okButton.Name = "okButton"
        Me.okButton.Size = New System.Drawing.Size(75, 23)
        Me.okButton.TabIndex = 15
        Me.okButton.Text = "OK"
        '
        'programTextBox
        '
        Me.programTextBox.Location = New System.Drawing.Point(12, 28)
        Me.programTextBox.Name = "programTextBox"
        Me.programTextBox.Size = New System.Drawing.Size(368, 20)
        Me.programTextBox.TabIndex = 14
        '
        'commandlineTextBox
        '
        Me.commandlineTextBox.Location = New System.Drawing.Point(12, 70)
        Me.commandlineTextBox.Name = "commandlineTextBox"
        Me.commandlineTextBox.Size = New System.Drawing.Size(448, 20)
        Me.commandlineTextBox.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(9, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Command Line:"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Program:"
        '
        'browseButton
        '
        Me.browseButton.Location = New System.Drawing.Point(386, 26)
        Me.browseButton.Name = "browseButton"
        Me.browseButton.Size = New System.Drawing.Size(75, 23)
        Me.browseButton.TabIndex = 10
        Me.browseButton.Text = "Browse..."
        '
        'NewRuleFrm
        '
        Me.AcceptButton = Me.okButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(473, 223)
        Me.ControlBox = False
        Me.Controls.Add(Me.pasteButton)
        Me.Controls.Add(Me.paramsListBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cancelButton)
        Me.Controls.Add(Me.okButton)
        Me.Controls.Add(Me.programTextBox)
        Me.Controls.Add(Me.commandlineTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.browseButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "NewRuleFrm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "New Rule"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents pasteButton As System.Windows.Forms.Button
    Friend WithEvents paramsListBox As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cancelButton As System.Windows.Forms.Button
    Friend WithEvents okButton As System.Windows.Forms.Button
    Friend WithEvents programTextBox As System.Windows.Forms.TextBox
    Friend WithEvents commandlineTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents browseButton As System.Windows.Forms.Button
End Class
