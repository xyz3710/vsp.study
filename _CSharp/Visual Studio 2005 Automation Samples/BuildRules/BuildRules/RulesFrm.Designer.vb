'Copyright (c) Microsoft Corporation.  All rights reserved.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RulesFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.okButton = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.movedownButton = New System.Windows.Forms.Button
        Me.moveupButton = New System.Windows.Forms.Button
        Me.removeButton = New System.Windows.Forms.Button
        Me.addnewButton = New System.Windows.Forms.Button
        Me.rulesListView = New System.Windows.Forms.ListView
        Me.Label1 = New System.Windows.Forms.Label
        Me.projectsTreeView = New System.Windows.Forms.TreeView
        Me.SuspendLayout()
        '
        'okButton
        '
        Me.okButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.okButton.Location = New System.Drawing.Point(435, 257)
        Me.okButton.Name = "okButton"
        Me.okButton.Size = New System.Drawing.Size(75, 23)
        Me.okButton.TabIndex = 17
        Me.okButton.Text = "OK"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(154, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Rules to run:"
        '
        'movedownButton
        '
        Me.movedownButton.Location = New System.Drawing.Point(435, 115)
        Me.movedownButton.Name = "movedownButton"
        Me.movedownButton.Size = New System.Drawing.Size(75, 23)
        Me.movedownButton.TabIndex = 15
        Me.movedownButton.Text = "Move Down"
        '
        'moveupButton
        '
        Me.moveupButton.Location = New System.Drawing.Point(435, 86)
        Me.moveupButton.Name = "moveupButton"
        Me.moveupButton.Size = New System.Drawing.Size(75, 23)
        Me.moveupButton.TabIndex = 14
        Me.moveupButton.Text = "Move Up"
        '
        'removeButton
        '
        Me.removeButton.Location = New System.Drawing.Point(435, 57)
        Me.removeButton.Name = "removeButton"
        Me.removeButton.Size = New System.Drawing.Size(75, 23)
        Me.removeButton.TabIndex = 13
        Me.removeButton.Text = "Remove"
        '
        'addnewButton
        '
        Me.addnewButton.Location = New System.Drawing.Point(435, 28)
        Me.addnewButton.Name = "addnewButton"
        Me.addnewButton.Size = New System.Drawing.Size(75, 23)
        Me.addnewButton.TabIndex = 12
        Me.addnewButton.Text = "Add new..."
        '
        'rulesListView
        '
        Me.rulesListView.FullRowSelect = True
        Me.rulesListView.HideSelection = False
        Me.rulesListView.Location = New System.Drawing.Point(157, 28)
        Me.rulesListView.MultiSelect = False
        Me.rulesListView.Name = "rulesListView"
        Me.rulesListView.Size = New System.Drawing.Size(272, 252)
        Me.rulesListView.TabIndex = 11
        Me.rulesListView.UseCompatibleStateImageBehavior = False
        Me.rulesListView.View = System.Windows.Forms.View.List
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 16)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Select a item:"
        '
        'projectsTreeView
        '
        Me.projectsTreeView.HideSelection = False
        Me.projectsTreeView.Location = New System.Drawing.Point(15, 28)
        Me.projectsTreeView.Name = "projectsTreeView"
        Me.projectsTreeView.Size = New System.Drawing.Size(136, 252)
        Me.projectsTreeView.TabIndex = 9
        '
        'RulesFrm
        '
        Me.AcceptButton = Me.okButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 292)
        Me.ControlBox = False
        Me.Controls.Add(Me.okButton)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.movedownButton)
        Me.Controls.Add(Me.moveupButton)
        Me.Controls.Add(Me.removeButton)
        Me.Controls.Add(Me.addnewButton)
        Me.Controls.Add(Me.rulesListView)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.projectsTreeView)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RulesFrm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Pre and Post Build Rules"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents okButton As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents movedownButton As System.Windows.Forms.Button
    Friend WithEvents moveupButton As System.Windows.Forms.Button
    Friend WithEvents removeButton As System.Windows.Forms.Button
    Friend WithEvents addnewButton As System.Windows.Forms.Button
    Friend WithEvents rulesListView As System.Windows.Forms.ListView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents projectsTreeView As System.Windows.Forms.TreeView
End Class
