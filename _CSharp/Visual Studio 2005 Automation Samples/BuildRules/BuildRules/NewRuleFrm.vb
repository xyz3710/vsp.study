'Copyright (c) Microsoft Corporation.  All rights reserved.

Public Class NewRuleFrm

    Public _commandLine As String
    Public _program As String

    Private Sub okButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles okButton.Click
        _program = programTextBox.Text
        _commandLine = commandlineTextBox.Text
    End Sub

    Private Sub browseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles browseButton.Click
        If (OpenFileDialog1.ShowDialog(Me) = Windows.Forms.DialogResult.OK) Then
            programTextBox.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub pasteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pasteButton.Click
        commandlineTextBox.SelectedText = "$(" + paramsListBox.SelectedItem.ToString() + ")"
    End Sub

    Private Sub NewRuleFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        paramsListBox.SelectedIndex = paramsListBox.Items.Add("ConfigurationName")
        paramsListBox.Items.Add("SolutionPath")
        paramsListBox.Items.Add("SolutionFolder")
        paramsListBox.Items.Add("Platform")
        paramsListBox.Items.Add("ProjectName")
    End Sub
End Class