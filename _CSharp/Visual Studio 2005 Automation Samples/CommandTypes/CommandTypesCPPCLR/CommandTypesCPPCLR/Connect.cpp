//Copyright (c) Microsoft Corporation.  All rights reserved.

// This is the main DLL file.

#include "stdafx.h"
#include "Connect.h"

void CommandTypesCPPCLR::Connect::OnAddInsUpdate(System::Array ^%custom)
{
}

void CommandTypesCPPCLR::Connect::OnBeginShutdown(System::Array ^%custom)
{
}

void CommandTypesCPPCLR::Connect::OnConnection(System::Object ^Application, ext_ConnectMode ConnectMode, System::Object ^AddInInst, System::Array ^%custom)
{
	_applicationObject = dynamic_cast<DTE2^>(Application);
	_addInInstance = dynamic_cast<AddIn^>(AddInInst);
	if(ConnectMode == ext_ConnectMode::ext_cm_UISetup)
	{
		array< Object^ >^ contextGUIDs = gcnew array< Object^ >(0);
		Commands2 ^commands = dynamic_cast<Commands2^>(_applicationObject->Commands);
		String ^toolsMenuName;

		try
		{
			//If you would like to move the command to a different menu, change the word "Tools" to the 
			//  English version of the menu. This code will take the culture, append on the name of the menu
			//  then add the command to that menu. You can find a list of all the top-level menus in the file
			//  CommandBar.resx.
			ResourceManager ^resourceManager = gcnew ResourceManager("CommandTypesCPPCLR.CommandBar", Assembly::GetExecutingAssembly());
			CultureInfo ^cultureInfo = gcnew System::Globalization::CultureInfo(_applicationObject->LocaleID);
			toolsMenuName = resourceManager->GetString(String::Concat(cultureInfo->TwoLetterISOLanguageName, "Tools"));
		}
		catch(...)
		{
			//We tried to find a localized version of the word Tools, but one was not found.
			//  Default to the en-US word, which may work for the current culture.
			toolsMenuName = "Tools";
		}

		//Place the command on the tools menu.
		//Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
		_CommandBars ^commandBars = dynamic_cast<CommandBars^>(_applicationObject->CommandBars);
		CommandBar ^menuBarCommandBar = dynamic_cast<CommandBar^>(commandBars["MenuBar"]);

		//Find the Tools command bar on the MenuBar command bar:
		CommandBarControl ^toolsControl = menuBarCommandBar->Controls[toolsMenuName];
		CommandBarPopup ^toolsPopup = dynamic_cast<CommandBarPopup^>(toolsControl);

		//This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
		//  just make sure you also update the QueryStatus/Exec method to include the new command names.
		try
		{
			//Add a command to the Commands collection:
			Command ^command = commands->AddNamedCommand2(_addInInstance, "CommandTypesButton", "CommandTypes", "Executes the command for CommandTypes", false, 1, contextGUIDs, (int)vsCommandStatus::vsCommandStatusSupported + (int)vsCommandStatus::vsCommandStatusEnabled, (int)vsCommandStyle::vsCommandStylePictAndText, vsCommandControlType::vsCommandControlTypeButton);

			//Add a control for the command to the tools menu:
			if ((command) && (toolsPopup))
			{
				command->AddControl(toolsPopup->CommandBar, 1);
			}
		}
		catch(...)
		{
			//If we are here, then the exception is probably because a command with that name
			//  already exists. If so there is no need to recreate the command and we can 
			//  safely ignore the exception.
		}

		//This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
		//  just make sure you also update the QueryStatus/Exec method to include the new command names.
		try
		{
			//Add a command to the Commands collection:
			Command ^command = commands->AddNamedCommand2(_addInInstance, "CommandTypesDropDownCombo", "CommandTypesDropDownCombo", "Executes the command for CommandTypes", false, 1, contextGUIDs, (int)vsCommandStatus::vsCommandStatusSupported + (int)vsCommandStatus::vsCommandStatusEnabled, (int)vsCommandStyle::vsCommandStylePictAndText, vsCommandControlType::vsCommandControlTypeDropDownCombo);

			//Add a control for the command to the tools menu:
			if ((command) && (toolsPopup))
			{
				command->AddControl(toolsPopup->CommandBar, 1);
			}
		}
		catch(...)
		{
			//If we are here, then the exception is probably because a command with that name
			//  already exists. If so there is no need to recreate the command and we can 
			//  safely ignore the exception.
		}

		//This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
		//  just make sure you also update the QueryStatus/Exec method to include the new command names.
		try
		{
			//Add a command to the Commands collection:
			Command ^command = commands->AddNamedCommand2(_addInInstance, "CommandTypesMRUCombo", "CommandTypesMRUCombo", "Executes the command for CommandTypes", false, 1, contextGUIDs, (int)vsCommandStatus::vsCommandStatusSupported + (int)vsCommandStatus::vsCommandStatusEnabled, (int)vsCommandStyle::vsCommandStylePictAndText, vsCommandControlType::vsCommandControlTypeMRUCombo);

			//Add a control for the command to the tools menu:
			if ((command) && (toolsPopup))
			{
				command->AddControl(toolsPopup->CommandBar, 1);
			}
		}
		catch(System::Exception ^ex)
		{
			String ^str = ex->ToString();
			//If we are here, then the exception is probably because a command with that name
			//  already exists. If so there is no need to recreate the command and we can 
			//  safely ignore the exception.
		}

		//This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
		//  just make sure you also update the QueryStatus/Exec method to include the new command names.
		try
		{
			//Add a command to the Commands collection:
			Command ^command = commands->AddNamedCommand2(_addInInstance, "CommandTypesMRUButton", "CommandTypesMRUButton", "Executes the command for CommandTypes", false, 1, contextGUIDs, (int)vsCommandStatus::vsCommandStatusSupported + (int)vsCommandStatus::vsCommandStatusEnabled, (int)vsCommandStyle::vsCommandStylePictAndText, vsCommandControlType::vsCommandControlTypeMRUButton);

			//Add a control for the command to the tools menu:
			if ((command) && (toolsPopup))
			{
				command->AddControl(toolsPopup->CommandBar, 1);
			}
		}
		catch(...)
		{
			//If we are here, then the exception is probably because a command with that name
			//  already exists. If so there is no need to recreate the command and we can 
			//  safely ignore the exception.
		}
	}
}

void CommandTypesCPPCLR::Connect::OnStartupComplete(System::Array ^%custom)
{
}

void CommandTypesCPPCLR::Connect::OnDisconnection(ext_DisconnectMode removeMode, System::Array ^%custom)
{
}



void CommandTypesCPPCLR::Connect::Exec(String ^CmdName, vsCommandExecOption ExecuteOption, Object ^%VariantIn, Object ^%VariantOut, bool %handled)
{
	handled = false;
	if (ExecuteOption == vsCommandExecOption::vsCommandExecOptionDoDefault)
    {
        if (CmdName == "CommandTypesCPPCLR.Connect.CommandTypesButton")
        {
			System::Windows::Forms::MessageBox::Show("CommandTypesButton");
            handled = true;
            return;
        }
        else if (CmdName == "CommandTypesCPPCLR.Connect.CommandTypesDropDownCombo")
        {
            if (VariantIn)
            {
                //We are being told that the user made a selection in the combo, and we need to react to it.
                String ^selectedText = dynamic_cast<String^>(VariantIn);
				System::Windows::Forms::MessageBox::Show("DropDownCombo: " + selectedText);
            }
            else
            {
                //The text of the selected item is being asked for, return this through the VariantOut param.
                VariantOut = "Item1";
            }
            handled = true;
            return;
        }
        else if (CmdName == "CommandTypesCPPCLR.Connect.CommandTypesDropDownCombo_1")
        {
            //We are being asked for the items in the combo box, return them through the VariantOut param.
			VariantOut = (System::Object^)gcnew array<String^> { gcnew String("Item 1"), gcnew String("Item 2"), gcnew String("Item 3"), gcnew String("Item 4") };
            handled = true;
            return;
        }
        else if (CmdName == "CommandTypesCPPCLR.Connect.CommandTypesMRUCombo")
        {
            String ^selectedText = dynamic_cast<String^>(VariantIn);
			System::Windows::Forms::MessageBox::Show("MRUCombo: " + selectedText);
            handled = true;
            return;
        }
        else if (CmdName->StartsWith(mruButtonRootCommandName))
        {
            String ^selectedItem = "";
            if (CmdName == mruButtonRootCommandName)
            {
                selectedItem = MRUButtonItemNames[0];
            }
            else if (CmdName->StartsWith(mruButtonRootCommandName))
            {
				int index = int::Parse(CmdName->Substring(mruButtonRootCommandName->Length + 1));
                selectedItem = MRUButtonItemNames[index];
            }
			System::Windows::Forms::MessageBox::Show(selectedItem);
            handled = true;
            return;
        }
    }
}

void CommandTypesCPPCLR::Connect::QueryStatus(String ^CmdName, vsCommandStatusTextWanted NeededText, vsCommandStatus %StatusOption, Object ^%CommandText)
{
	if (NeededText == vsCommandStatusTextWanted::vsCommandStatusTextWantedNone)
	{
		int index = 0;
		if (CmdName == "CommandTypesCPPCLR.Connect.CommandTypesButton")
		{
			StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
			return;
		}
		else if (CmdName == "CommandTypesCPPCLR:Connect.CommandTypesDropDownCombo")
		{
			StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
			return;
		}
		else if (CmdName == "CommandTypesCPPCLR.Connect.CommandTypesMRUCombo")
		{
			StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
			return;
		}
		else if (CmdName == mruButtonRootCommandName)
		{
			StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
			index = 0;
			return;
		}
		else if (CmdName->StartsWith(mruButtonRootCommandName))
		{
			index = int::Parse(CmdName->Substring(mruButtonRootCommandName->Length + 1));
			if (index > 3)
			{
				StatusOption = vsCommandStatus::vsCommandStatusUnsupported;
				return;
			}
			else
			{
				StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
				CommandText = MRUButtonItemNames[index];
				return;
			}
		}
	}
	else if (NeededText == vsCommandStatusTextWanted::vsCommandStatusTextWantedName)
	{
		if (CmdName == mruButtonRootCommandName)
		{
			CommandText = MRUButtonItemNames[0];
			StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
			return;
		}
		else if (CmdName->StartsWith(mruButtonRootCommandName))
		{
			int index = int::Parse(CmdName->Substring(mruButtonRootCommandName->Length + 1));
			if (index > 3)
			{
				StatusOption = vsCommandStatus::vsCommandStatusUnsupported;
				return;
			}
			else
			{
				StatusOption = (vsCommandStatus)vsCommandStatus::vsCommandStatusSupported | vsCommandStatus::vsCommandStatusEnabled;
				CommandText = MRUButtonItemNames[index];
				return;
			}
		}
	}
}