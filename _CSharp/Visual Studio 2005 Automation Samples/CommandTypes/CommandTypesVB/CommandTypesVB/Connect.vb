'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System
Imports Microsoft.VisualStudio.CommandBars
Imports Extensibility
Imports EnvDTE
Imports EnvDTE80

Public Class Connect

    Implements IDTExtensibility2
    Implements IDTCommandTarget

    Dim _applicationObject As DTE2
    Dim _addInInstance As AddIn

    '''<summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
    Public Sub New()

    End Sub

    '''<summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
    '''<param name='application'>Root object of the host application.</param>
    '''<param name='connectMode'>Describes how the Add-in is being loaded.</param>
    '''<param name='addInInst'>Object representing this Add-in.</param>
    '''<remarks></remarks>
    Public Sub OnConnection(ByVal application As Object, ByVal connectMode As ext_ConnectMode, ByVal addInInst As Object, ByRef custom As Array) Implements IDTExtensibility2.OnConnection
        _applicationObject = CType(application, DTE2)
        _addInInstance = CType(addInInst, AddIn)
        If connectMode = ext_ConnectMode.ext_cm_UISetup Then

            Dim commands As Commands2 = CType(_applicationObject.Commands, Commands2)
            Dim toolsMenuName As String
            Try

                'If you would like to move the command to a different menu, change the word "Tools" to the 
                '  English version of the menu. This code will take the culture, append on the name of the menu
                '  then add the command to that menu. You can find a list of all the top-level menus in the file
                '  CommandBar.resx.
                Dim resourceManager As System.Resources.ResourceManager = New System.Resources.ResourceManager("CommandTypesVB.CommandBar", System.Reflection.Assembly.GetExecutingAssembly())

                Dim cultureInfo As System.Globalization.CultureInfo = New System.Globalization.CultureInfo(_applicationObject.LocaleID)
                toolsMenuName = resourceManager.GetString(String.Concat(cultureInfo.TwoLetterISOLanguageName, "Tools"))

            Catch e As Exception
                'We tried to find a localized version of the word Tools, but one was not found.
                '  Default to the en-US word, which may work for the current culture.
                toolsMenuName = "Tools"
            End Try

            'Place the command on the tools menu.
            'Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
            Dim commandBars As CommandBars = CType(_applicationObject.CommandBars, CommandBars)
            Dim menuBarCommandBar As CommandBar = commandBars.Item("MenuBar")

            'Find the Tools command bar on the MenuBar command bar:
            Dim toolsControl As CommandBarControl = menuBarCommandBar.Controls.Item(toolsMenuName)
            Dim toolsPopup As CommandBarPopup = CType(toolsControl, CommandBarPopup)

            'This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
            '  just make sure you also update the QueryStatus/Exec method to include the new command names.
            Try

                'Add a command to the Commands collection:
                Dim commandObject As Command = commands.AddNamedCommand2(_addInInstance, "CommandTypesButton", "CommandTypes", "Executes the command for CommandTypes", False, 1, Nothing, vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton)

                'Add a control for the command to the tools menu:
                If ((Not (commandObject Is Nothing)) And (Not (toolsPopup Is Nothing))) Then

                    commandObject.AddControl(toolsPopup.CommandBar, 1)
                End If
            Catch
                'If we are here, then the exception is probably because a command with that name
                '  already exists. If so there is no need to recreate the command and we can 
                '  safely ignore the exception.
            End Try

            'This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
            '  just make sure you also update the QueryStatus/Exec method to include the new command names.
            Try
                'Add a command to the Commands collection:
                Dim commandObject As Command = commands.AddNamedCommand2(_addInInstance, "CommandTypesDropDownCombo", "CommandTypesDropDownCombo", "Executes the command for CommandTypes", False, 1, Nothing, vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeDropDownCombo)

                'Add a control for the command to the tools menu:
                If ((Not (commandObject Is Nothing)) And (Not (toolsPopup Is Nothing))) Then

                    commandObject.AddControl(toolsPopup.CommandBar, 1)
                End If
            Catch
                'If we are here, then the exception is probably because a command with that name
                '  already exists. If so there is no need to recreate the command and we can 
                '  safely ignore the exception.
            End Try

            'This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
            '  just make sure you also update the QueryStatus/Exec method to include the new command names.
            Try

                'Add a command to the Commands collection:
                Dim commandObject As Command = commands.AddNamedCommand2(_addInInstance, "CommandTypesMRUCombo", "CommandTypesMRUCombo", "Executes the command for CommandTypes", False, 1, Nothing, vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeMRUCombo)

                'Add a control for the command to the tools menu:
                If ((Not (commandObject Is Nothing)) And (Not (toolsPopup Is Nothing))) Then

                    commandObject.AddControl(toolsPopup.CommandBar, 1)
                End If

            Catch
                'If we are here, then the exception is probably because a command with that name
                '  already exists. If so there is no need to recreate the command and we can 
                '  safely ignore the exception.
            End Try

            'This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
            '  just make sure you also update the QueryStatus/Exec method to include the new command names.
            Try
                'Add a command to the Commands collection:
                Dim commandObject As Command = commands.AddNamedCommand2(_addInInstance, "CommandTypesMRUButton", "CommandTypesMRUButton", "Executes the command for CommandTypes", False, 1, Nothing, vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeMRUButton)

                'Add a control for the command to the tools menu:
                If ((Not (commandObject Is Nothing)) And (Not (toolsPopup Is Nothing))) Then
                    commandObject.AddControl(toolsPopup.CommandBar, 1)
                End If
            Catch
                'If we are here, then the exception is probably because a command with that name
                '  already exists. If so there is no need to recreate the command and we can 
                '  safely ignore the exception.
            End Try

        End If
    End Sub

    '''<summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
    '''<param name='disconnectMode'>Describes how the Add-in is being unloaded.</param>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnDisconnection(ByVal disconnectMode As ext_DisconnectMode, ByRef custom As Array) Implements IDTExtensibility2.OnDisconnection
    End Sub

    '''<summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification that the collection of Add-ins has changed.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnAddInsUpdate(ByRef custom As Array) Implements IDTExtensibility2.OnAddInsUpdate
    End Sub

    '''<summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnStartupComplete(ByRef custom As Array) Implements IDTExtensibility2.OnStartupComplete
    End Sub

    '''<summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnBeginShutdown(ByRef custom As Array) Implements IDTExtensibility2.OnBeginShutdown
    End Sub

    Dim MRUButtonItemNames As String() = New String() {"Button Item 1", "Button Item 2", "Button Item 3", "Button Item 4"}
    Dim mruButtonRootCommandName As String = "CommandTypesVB.Connect.CommandTypesMRUButton"

    '''<summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
    '''<param name='commandName'>The name of the command to determine state for.</param>
    '''<param name='neededText'>Text that is needed for the command.</param>
    '''<param name='status'>The state of the command in the user interface.</param>
    '''<param name='commandText'>Text requested by the neededText parameter.</param>
    '''<remarks></remarks>
    Public Sub QueryStatus(ByVal commandName As String, ByVal neededText As vsCommandStatusTextWanted, ByRef status As vsCommandStatus, ByRef commandText As Object) Implements IDTCommandTarget.QueryStatus
        If (neededText = vsCommandStatusTextWanted.vsCommandStatusTextWantedNone) Then

            Dim index As Integer = 0
            If (commandName = "CommandTypesVB.Connect.CommandTypesButton") Then

                status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                Return

            ElseIf (commandName = "CommandTypesVB.Connect.CommandTypesDropDownCombo") Then

                status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                Return

            ElseIf (commandName = "CommandTypesVB.Connect.CommandTypesMRUCombo") Then

                status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                Return

            ElseIf (commandName = mruButtonRootCommandName) Then

                status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                index = 0
                Return

            ElseIf (commandName.StartsWith(mruButtonRootCommandName)) Then

                index = Integer.Parse(commandName.Substring(mruButtonRootCommandName.Length + 1))
                If (index > 3) Then

                    status = vsCommandStatus.vsCommandStatusUnsupported
                    Return

                Else

                    status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                    commandText = MRUButtonItemNames(index)
                    Return
                End If
            End If

        ElseIf (neededText = vsCommandStatusTextWanted.vsCommandStatusTextWantedName) Then

            If (commandName = mruButtonRootCommandName) Then

                commandText = MRUButtonItemNames(0)
                status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                Return

            ElseIf (commandName.StartsWith(mruButtonRootCommandName)) Then

                Dim index As Integer = Integer.Parse(commandName.Substring(mruButtonRootCommandName.Length + 1))
                If (index > 3) Then

                    status = vsCommandStatus.vsCommandStatusUnsupported
                    Return

                Else

                    status = CType(vsCommandStatus.vsCommandStatusSupported + vsCommandStatus.vsCommandStatusEnabled, vsCommandStatus)
                    commandText = MRUButtonItemNames(index)
                    Return


                End If
            End If
        End If
    End Sub

    '''<summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
    '''<param name='commandName'>The name of the command to execute.</param>
    '''<param name='executeOption'>Describes how the command should be run.</param>
    '''<param name='varIn'>Parameters passed from the caller to the command handler.</param>
    '''<param name='varOut'>Parameters passed from the command handler to the caller.</param>
    '''<param name='handled'>Informs the caller if the command was handled or not.</param>
    '''<remarks></remarks>
    Public Sub Exec(ByVal commandName As String, ByVal executeOption As vsCommandExecOption, ByRef varIn As Object, ByRef varOut As Object, ByRef handled As Boolean) Implements IDTCommandTarget.Exec
        handled = False
        If (executeOption = vsCommandExecOption.vsCommandExecOptionDoDefault) Then

            If (commandName = "CommandTypesVB.Connect.CommandTypesButton") Then

                System.Windows.Forms.MessageBox.Show("CommandTypesButton")
                handled = True
                Return

            ElseIf (commandName = "CommandTypesVB.Connect.CommandTypesDropDownCombo") Then

                If ((Not (varIn Is Nothing))) Then 'TODO: and (varIn is string))

                    'We are being told that the user made a selection in the combo, and we need to react to it.
                    Dim selectedText As String = CType(varIn, String)
                    System.Windows.Forms.MessageBox.Show("DropDownCombo: " + selectedText)

                Else

                    'The text of the selected item is being asked for, return this through the varOut param.
                    varOut = "Item1"
                End If
                handled = True
                Return
            ElseIf (commandName = "CommandTypesVB.Connect.CommandTypesDropDownCombo_1") Then

                'We are being asked for the items in the combo box, return them through the varOut param.
                varOut = New String() {"Item 1", "Item 2", "Item 3", "Item 4"}
                handled = True
                Return

            ElseIf (commandName = "CommandTypesVB.Connect.CommandTypesMRUCombo") Then

                Dim selectedText As String = CType(varIn, String)
                System.Windows.Forms.MessageBox.Show("MRUCombo: " + selectedText)
                handled = True
                Return

            ElseIf (commandName.StartsWith(mruButtonRootCommandName)) Then

                Dim selectedItem As String = ""
                If (commandName = mruButtonRootCommandName) Then

                    selectedItem = MRUButtonItemNames(0)

                ElseIf (commandName.StartsWith(mruButtonRootCommandName)) Then

                    Dim index As Integer = Integer.Parse(commandName.Substring(mruButtonRootCommandName.Length + 1))
                    selectedItem = MRUButtonItemNames(index)
                End If
                System.Windows.Forms.MessageBox.Show(selectedItem)
                handled = True
                Return
            End If
        End If
    End Sub
End Class
