//Copyright (c) Microsoft Corporation.  All rights reserved.

// Connect.cpp : Implementation of CConnect
#include "stdafx.h"
#include "AddIn.h"
#include "Connect.h"

extern CAddInModule _AtlModule;

// CConnect
STDMETHODIMP CConnect::OnConnection(IDispatch *pApplication, ext_ConnectMode ConnectMode, IDispatch *pAddInInst, SAFEARRAY ** /*custom*/ )
{
	HRESULT hr = S_OK;
	pApplication->QueryInterface(__uuidof(DTE2), (LPVOID*)&m_pDTE);
	pAddInInst->QueryInterface(__uuidof(AddIn), (LPVOID*)&m_pAddInInstance);
	if(ConnectMode == 5) //5 == ext_cm_UISetup
	{
		HRESULT hr = S_OK;
		CComPtr<IDispatch> pDisp;
		CComQIPtr<Commands> pCommands;
		CComQIPtr<Commands2> pCommands2;
		CComQIPtr<_CommandBars> pCommandBars;
		CComPtr<CommandBarControl> pCommandBarControl;
		CComPtr<Command> pCreatedCommand;
		CComPtr<CommandBar> pMenuBarCommandBar;
		CComPtr<CommandBarControls> pMenuBarControls;
		CComPtr<CommandBarControl> pToolsCommandBarControl;
		CComQIPtr<CommandBar> pToolsCommandBar;
		CComQIPtr<CommandBarPopup> pToolsPopup;

		IfFailGoCheck(m_pDTE->get_Commands(&pCommands), pCommands);
		pCommands2 = pCommands;
		if(SUCCEEDED(pCommands2->AddNamedCommand2(m_pAddInInstance, CComBSTR("GenerateHTML"), CComBSTR("GenerateHTML"), CComBSTR("Executes the command for GenerateHTML"), VARIANT_TRUE, CComVariant(59), NULL, vsCommandStatusSupported+vsCommandStatusEnabled, vsCommandStylePictAndText, vsCommandControlTypeButton, &pCreatedCommand)) && (pCreatedCommand))
		{
			//Add a button to the tools menu bar.
			IfFailGoCheck(m_pDTE->get_CommandBars(&pDisp), pDisp);
			pCommandBars = pDisp;

			//Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
			IfFailGoCheck(pCommandBars->get_Item(CComVariant(L"MenuBar"), &pMenuBarCommandBar), pMenuBarCommandBar);
			IfFailGoCheck(pMenuBarCommandBar->get_Controls(&pMenuBarControls), pMenuBarControls);

			//Find the Tools command bar on the MenuBar command bar:
			IfFailGoCheck(pMenuBarControls->get_Item(CComVariant(L"Tools"), &pToolsCommandBarControl), pToolsCommandBarControl);
			pToolsPopup = pToolsCommandBarControl;
			IfFailGoCheck(pToolsPopup->get_CommandBar(&pToolsCommandBar), pToolsCommandBar);

			//Add the control:
			pDisp = NULL;
			IfFailGoCheck(pCreatedCommand->AddControl(pToolsCommandBar, 1, &pDisp), pDisp);
		}
		return S_OK;
	}
Error:
	return hr;
}

STDMETHODIMP CConnect::OnDisconnection(ext_DisconnectMode /*RemoveMode*/, SAFEARRAY ** /*custom*/ )
{
	m_pDTE = NULL;
	m_pAddInInstance = NULL;
	return S_OK;
}

STDMETHODIMP CConnect::OnAddInsUpdate (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnStartupComplete (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnBeginShutdown (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::QueryStatus(BSTR bstrCmdName, vsCommandStatusTextWanted NeededText, vsCommandStatus *pStatusOption, VARIANT *pvarCommandText)
{
  if(NeededText == vsCommandStatusTextWantedNone)
	{
	  if(!_wcsicmp(bstrCmdName, L"GenerateHTML.Connect.GenerateHTML"))
	  {
		  *pStatusOption = (vsCommandStatus)(vsCommandStatusEnabled+vsCommandStatusSupported);
	  }
  }
	return S_OK;
}

STDMETHODIMP CConnect::Exec(BSTR bstrCmdName, vsCommandExecOption ExecuteOption, VARIANT * /*pvarVariantIn*/, VARIANT * /*pvarVariantOut*/, VARIANT_BOOL *pvbHandled)
{
	*pvbHandled = VARIANT_FALSE;
	if(ExecuteOption == vsCommandExecOptionDoDefault)
	{
		if(!_wcsicmp(bstrCmdName, L"GenerateHTML.Connect.GenerateHTML"))
		{
			HWND hWndParent;
			long lWndParent;
			CComPtr<IDispatch> pDisp;
			CComPtr<EnvDTE::Document> pDocument;
			CComPtr<EnvDTE::TextSelection> pTextSelection;
			CComQIPtr<EnvDTE::TextDocument> pTextDocument;
			CComPtr<EnvDTE::Window> pMainWindow;
			m_pDTE->get_MainWindow(&pMainWindow);
			pMainWindow->get_HWnd(&lWndParent);
			hWndParent = (HWND)lWndParent;
			m_pDTE->get_ActiveDocument(&pDocument);
			if(pDocument)
			{
				pDocument->Object(CComBSTR(L"TextDocument"), &pDisp);
				pDocument->get_Name(&m_bstrName);
				pTextDocument = pDisp;
				pTextDocument->get_Selection(&pTextSelection);
				pTextSelection->SelectAll();
				pTextSelection->Copy();

				UINT g_CF_RTF = RegisterClipboardFormat("Rich Text Format");
				OpenClipboard(NULL);
				HANDLE hData = GetClipboardData(g_CF_RTF);
				BYTE *pData = (BYTE*)GlobalLock(hData);
				if(!pData)
					MessageBox(hWndParent, "RTF data is not on the clip board", "Error", MB_OK);
				else
				{
	#ifdef WRITETODISK
					ParseRTF((TCHAR*)pData);
	#else
					EnvDTE::vsHTMLTabs CurrentTab;
					CComQIPtr<EnvDTE::HTMLWindow> pHTMLWindow;
					CComPtr<EnvDTE::Window> pNewWindow;
					CComPtr<IDispatch> pDisp;
					CComPtr<IDispatch> pDisp2;
					CComPtr<EnvDTE::Document> pDocument;
					CComPtr<EnvDTE::TextSelection> pSelection;
					CComQIPtr<EnvDTE::TextDocument> pTextDocument;
					CComPtr<EnvDTE::ItemOperations> pItemOperations;
					CComQIPtr<EnvDTE::TextWindow> pTextWindow;
					CComBSTR bstrTemplateName(m_bstrName);
					bstrTemplateName += CComBSTR(L".htm");
					m_pDTE->get_ItemOperations(&pItemOperations);

					pItemOperations->NewFile(CComBSTR(L"General\\HTML Page"), bstrTemplateName, CComBSTR(L"{7651A701-06E5-11D1-8EBD-00A0C90F26EA}"), &pNewWindow);
					pNewWindow->get_Object(&pDisp2);
					pHTMLWindow = pDisp2;
					pHTMLWindow->put_CurrentTab(EnvDTE::vsHTMLTabsSource);
					pHTMLWindow->get_CurrentTabObject(&pDisp);
					pTextWindow = pDisp;
					pTextWindow->get_Selection(&pSelection);
					pSelection->SelectAll();
					pSelection->Delete(1);
					ParseRTF((TCHAR*)pData, pSelection);
	#endif
				}
				CloseClipboard();
			}
			else
			{
				MessageBox(hWndParent, _T("You need to have a text document open and active to use this command"), _T("Generate HTML"), MB_OK|MB_ICONEXCLAMATION);
			}

			*pvbHandled = VARIANT_TRUE;
			return S_OK;
		}
	}
	return S_OK;
}

////////////////////////
struct ColorTable
{
	unsigned char r, g, b;
};

TCHAR szFontName[MAX_PATH];
ColorTable *pColorTable = NULL;

HRESULT ParseFontTable(TCHAR *pszData)
{
	long lCurrPos = 0;
	TCHAR *pszTemp = pszData;
	while(*pszTemp != ' ')
		pszTemp = CharNext(pszTemp);
	pszTemp = CharNext(pszTemp);
	_tcscpy(szFontName, pszTemp);
	while(szFontName[lCurrPos] != ';')
		lCurrPos++;
	szFontName[lCurrPos] = 0;
	return S_OK;
}

HRESULT ParseColorTable(TCHAR *pszData)
{
	unsigned char cCurrentColor = 0;
	long lCountColorEntries = 0;
	TCHAR *pszTemp = pszData;
	TCHAR *pszTemp2;
	while(*pszTemp != ';')
		pszTemp = CharNext(pszTemp);
	pszTemp = CharNext(pszTemp);
	pszTemp2 = pszTemp;
	while(*pszTemp != '}')
	{
		if(*pszTemp == ';')
			lCountColorEntries++;
		pszTemp = CharNext(pszTemp);
	}
	pColorTable = new ColorTable[lCountColorEntries];
	for(long i = 0 ; i < lCountColorEntries ; i++)
	{
		//Read in Red:
		while((*pszTemp2 == ' ') || (*pszTemp2 == '\r') || (*pszTemp2 == '\n'))
			pszTemp2 = CharNext(pszTemp2);

		cCurrentColor = 0;
		pszTemp2 = CharNext(CharNext(CharNext(CharNext(pszTemp2))));
		while(*pszTemp2 != '\\')
		{
			cCurrentColor *= 10;
			cCurrentColor += (*pszTemp2 - '0');
			pszTemp2 = CharNext(pszTemp2);
		}
		pColorTable[i].r = cCurrentColor;

		//Read in Green:
		while((*pszTemp2 == ' ') || (*pszTemp2 == '\r') || (*pszTemp2 == '\n'))
			pszTemp2 = CharNext(pszTemp2);
		cCurrentColor = 0;
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		while(*pszTemp2 != '\\')
		{
			cCurrentColor *= 10;
			cCurrentColor += (*pszTemp2 - '0');
			pszTemp2 = CharNext(pszTemp2);
		}
		pColorTable[i].g = cCurrentColor;

		//Read in Blue:
		while((*pszTemp2 == ' ') || (*pszTemp2 == '\r') || (*pszTemp2 == '\n'))
			pszTemp2 = CharNext(pszTemp2);
		cCurrentColor = 0;
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		pszTemp2 = CharNext(pszTemp2);
		while(*pszTemp2 != ';')
		{
			cCurrentColor *= 10;
			cCurrentColor += (*pszTemp2 - '0');
			pszTemp2 = CharNext(pszTemp2);
		}
		pColorTable[i].b = cCurrentColor;
		pszTemp2 = CharNext(pszTemp2);
	}
	return S_OK;
}

HRESULT CConnect::GetHTMLFromEscape(TCHAR **pszBuffer, TCHAR **pszReturnStartTag)
{
	HRESULT hr = S_OK;
	TCHAR *pszTemp = *pszBuffer;
	if(!memcmp(*pszBuffer, _T("\\fs"), 3))
	{
		*pszReturnStartTag = new TCHAR[100];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		pszTemp = CharNext(pszTemp);
		pszTemp = CharNext(pszTemp);
		pszTemp = CharNext(pszTemp);
		long lSize = atoi(pszTemp);
		if(m_fFontDepth)
			wsprintf(*pszReturnStartTag, _T("</FONT><FONT size=%d>"), lSize/10);
		else
		{
			m_fFontDepth = true;
			wsprintf(*pszReturnStartTag, _T("<FONT size=%d>"), lSize/10);
		}
	}
	else if(!memcmp(*pszBuffer, _T("\\cf"), 3))
	{
		*pszReturnStartTag = new TCHAR[100];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		pszTemp = CharNext(pszTemp);
		pszTemp = CharNext(pszTemp);
		pszTemp = CharNext(pszTemp);
		long lColorIndex = atoi(pszTemp);
		if(lColorIndex == 0)
		{
			if(m_fFontDepth)
				wsprintf(*pszReturnStartTag, _T("</FONT><FONT color=\"#%02X%02X%02X\">"), pColorTable[0].r, pColorTable[0].g, pColorTable[0].b);
			else
			{
				m_fFontDepth = true;
				wsprintf(*pszReturnStartTag, _T("<FONT color=\"#%02X%02X%02X\">"), pColorTable[0].r, pColorTable[0].g, pColorTable[0].b);
			}
		}
		else
		{
			if(m_fFontDepth)
				wsprintf(*pszReturnStartTag, _T("</FONT><FONT color=\"#%02X%02X%02X\">"), pColorTable[lColorIndex-1].r, pColorTable[lColorIndex-1].g, pColorTable[lColorIndex-1].b);
			else
			{
				m_fFontDepth = true;
				wsprintf(*pszReturnStartTag, _T("<FONT color=\"#%02X%02X%02X\">"), pColorTable[lColorIndex-1].r, pColorTable[lColorIndex-1].g, pColorTable[lColorIndex-1].b);
			}
		}
	}
	else if(!memcmp(*pszBuffer, _T("\\par"), 4))
	{
		*pszReturnStartTag = new TCHAR[5];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		_tcscpy(*pszReturnStartTag, _T("<br>"));
	}
	else if(!memcmp(*pszBuffer, _T("\\tab"), 4))
	{
		*pszReturnStartTag = new TCHAR[50];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		_tcscpy(*pszReturnStartTag, _T("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
	}
	else if(!memcmp(*pszBuffer, _T("\\\\"), 2))
	{
		*pszReturnStartTag = new TCHAR[2];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		_tcscpy(*pszReturnStartTag, _T("\\"));
		*pszBuffer = CharNext(*pszBuffer);
		*pszBuffer = CharNext(*pszBuffer);
	}
	else if(!memcmp(*pszBuffer, _T("\\{"), 2))
	{
		*pszReturnStartTag = new TCHAR[2];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		_tcscpy(*pszReturnStartTag, _T("{"));
	}
	else if(!memcmp(*pszBuffer, _T("\\}"), 2))
	{
		*pszReturnStartTag = new TCHAR[2];
		if(!(*pszReturnStartTag))
			return E_OUTOFMEMORY;
		_tcscpy(*pszReturnStartTag, _T("}"));
	}
	return hr;
}

#ifdef WRITETODISK
HRESULT CConnect::ParseRTF2(TCHAR **pszData)
#else
HRESULT CConnect::ParseRTF2(TCHAR **pszData, CComPtr<EnvDTE::TextSelection> pSelection)
#endif
{
Reparse:
	HRESULT hr = S_OK;
	if((!(**pszData)) || (**pszData == '}'))
		return S_OK;
	if((**pszData == '\\') && (*CharNext(*pszData) == '\\'))
	{
#ifdef WRITETODISK
		WriteFile(m_hFile, "\\", strlen("\\"), &m_dwWritten, NULL);
#else
		pSelection->Insert(CComBSTR(L"\\"), 1);
#endif
		*pszData = CharNext(CharNext(*pszData));
		goto Reparse;
	}
	else if((**pszData == '\\'))
	{
		TCHAR *pszStartTag;
		hr = GetHTMLFromEscape(pszData, &pszStartTag);
		if(FAILED(hr))
			return hr;

#ifdef WRITETODISK
		WriteFile(m_hFile, pszStartTag, strlen(pszStartTag), &m_dwWritten, NULL);
#else
		CComBSTR bstrTemp(pszStartTag);
		pSelection->Insert(bstrTemp, 1);
#endif
		delete []pszStartTag;

		*pszData = CharNext(*pszData);
		while((**pszData != '\n') && (**pszData != '\r') && (**pszData != ' ') && (**pszData != '\\') && (**pszData != 0))
		{
			if((**pszData == '}') || (**pszData == '{'))
			{
				*pszData = CharNext(*pszData);
				break;
			}
			*pszData = CharNext(*pszData);
		}

#ifdef WRITETODISK
		HRESULT hr = ParseRTF2(pszData);
#else
		HRESULT hr = ParseRTF2(pszData, pSelection);
#endif
		
		*pszData = CharNext(*pszData);
	}
	else
	{
		while((**pszData != '\\') && (**pszData != '}') && (**pszData != 0))
		{
			if(**pszData == '<')
			{
#ifdef WRITETODISK
				WriteFile(m_hFile, "&lt;", strlen("&lt;"), &m_dwWritten, NULL);
#else
				pSelection->Insert(CComBSTR(L"&lt;"), 1);
#endif
				*pszData = CharNext(*pszData);
			}
			else if(**pszData == '>')
			{
#ifdef WRITETODISK
				WriteFile(m_hFile, "&gt;", strlen("&gt;"), &m_dwWritten, NULL);
#else
				pSelection->Insert(CComBSTR(L"&gt;"), 1);
#endif
				*pszData = CharNext(*pszData);
			}
			else if(**pszData == '&')
			{
#ifdef WRITETODISK
				WriteFile(m_hFile, "&amp;", strlen("&amp;"), &m_dwWritten, NULL);
#else
				pSelection->Insert(CComBSTR(L"&amp;"), 1);
#endif
				*pszData = CharNext(*pszData);
			}
			else if(**pszData == '\\')
			{
				TCHAR *pszStartTag;
				hr = GetHTMLFromEscape(pszData, &pszStartTag);
				if(FAILED(hr))
					return hr;

#ifdef WRITETODISK
				WriteFile(m_hFile, pszStartTag, strlen(pszStartTag), &m_dwWritten, NULL);
#else
				CComBSTR bstrTemp(pszStartTag);
				pSelection->Insert(bstrTemp, 1);
#endif
				delete []pszStartTag;

				*pszData = CharNext(*pszData);
				while((**pszData != '\n') && (**pszData != '\r') && (**pszData != ' ') && (**pszData != '\\') && (**pszData != 0))
				{
					if((**pszData == '}') || (**pszData == '{'))
					{
						*pszData = CharNext(*pszData);
						break;
					}
					*pszData = CharNext(*pszData);
				}
#ifdef WRITETODISK
				HRESULT hr = ParseRTF2(pszData);
#else
				HRESULT hr = ParseRTF2(pszData, pSelection);
#endif
				//pSelection->Insert(bstrTemp, 1);
				*pszData = CharNext(*pszData);
			}
			else
			{
#ifdef WRITETODISK
				WriteFile(m_hFile, *pszData, 1, &m_dwWritten, NULL);
#else
				TCHAR szData[2] = {0, 0};
				szData[0] = **pszData;
				CComBSTR bstrTemp(szData);
				pSelection->Insert(bstrTemp, 1);
#endif
				*pszData = CharNext(*pszData);
			}
		}
		if(**pszData == '}')
			return S_OK;
		else
			goto Reparse;
	}
	return hr;
}

#ifdef WRITETODISK
HRESULT CConnect::ParseRTF(TCHAR *pszData)
#else
HRESULT CConnect::ParseRTF(TCHAR *pszData, CComPtr<EnvDTE::TextSelection> pSelection)
#endif
{
	USES_CONVERSION;
#ifdef WRITETODISK
	CComPtr<EnvDTE::Window> pWindow;
	CComPtr<EnvDTE::ItemOperations> pItemOp;
	TCHAR szTempFile[MAX_PATH];
	GetTempPath(MAX_PATH, szTempFile);
    GetTempFileName(szTempFile, "vsh", 0, szTempFile);
	_tcscat(szTempFile, _T(".htm"));
	m_hFile = CreateFile(szTempFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
#endif

	//We do not do a complete full on parsing of RTF files. The target RTF data is that which is produced by 
	//  Visual Studio 7.1. Any RTF data on the clipboard that was not generated by VS7 will create invalid data (and no error warning will be given).
	TCHAR *pszTemp = pszData;
	TCHAR *pszFontTblStart;
	TCHAR *pszFontTblEnd;
	TCHAR *pszColorTblStart;
	TCHAR *pszColorTblEnd;
	long lTableLength;
	TCHAR *pszFontTbl;
	TCHAR *pszColorTbl;
	HRESULT hr = S_OK;
	CComPtr<EnvDTE::Properties> pProperties;
	CComPtr<EnvDTE::Property> pProperty;
	CComBSTR bstrFontName;
	CComVariant varFontName;
	CComVariant varFontSize;
	TCHAR szFontSize[10];

	//Start by skipping over the first '{' and searching for the next:
	pszTemp = CharNext(pszTemp);
	while (*pszTemp != '{')
		pszTemp = CharNext(pszTemp);

	//Now sitting at the fonttbl entry. Scan until the first '{' of fonttbl
	while (*pszTemp != '{')
		pszTemp = CharNext(pszTemp);

	//Now scan until the closing '}'
	pszFontTblStart = pszTemp;
	while (*pszTemp != '}')
		pszTemp = CharNext(pszTemp);
	pszTemp = CharNext(CharNext(pszTemp));

	pszFontTblEnd = pszTemp;

	lTableLength = ((long)pszFontTblEnd)-((long)pszFontTblStart);
	pszFontTbl = new TCHAR[lTableLength + 1];
	memcpy(pszFontTbl, pszFontTblStart, lTableLength);
	pszFontTbl[lTableLength] = 0;

	//Font table read in, now read the color table:
	pszColorTblStart = pszTemp;

	while (*pszTemp != '}')
		pszTemp = CharNext(pszTemp);
	pszTemp = CharNext(pszTemp);
	pszColorTblEnd = pszTemp;

	lTableLength = ((long)pszColorTblEnd)-((long)pszColorTblStart);
	pszColorTbl = new TCHAR[lTableLength + 1];
	memcpy(pszColorTbl, pszColorTblStart, lTableLength);
	pszColorTbl[lTableLength] = 0;

	ParseFontTable(pszFontTbl);
	ParseColorTable(pszColorTbl);
	m_pDTE->get_Properties(CComBSTR("FontsAndColors"), CComBSTR("TextEditor"), &pProperties);
	pProperties->Item(CComVariant("FontFamily"), &pProperty);
	pProperty->get_Value(&varFontName);
	bstrFontName = varFontName.bstrVal;
	pProperty = NULL;
	pProperties->Item(CComVariant("FontSize"), &pProperty);
	pProperty->get_Value(&varFontSize);
	wsprintf(szFontSize, _T("%d"), varFontSize.lVal);
#ifdef WRITETODISK
	
	WriteFile(m_hFile, "<HTML>\n", strlen("<HTML>\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "<HEAD>\n", strlen("<HEAD>\n"), &m_dwWritten, NULL);	
	WriteFile(m_hFile, "<TITLE>", strlen("<TITLE>"), &m_dwWritten, NULL);
	WriteFile(m_hFile, W2T(m_bstrName), m_bstrName.Length(), &m_dwWritten, NULL);
	WriteFile(m_hFile, "</TITLE>\n", strlen("</TITLE>\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "<STYLE  TYPE=\"text/css\">\n", strlen("<STYLE  TYPE=\"text/css\">\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "BODY{font-family: \"", strlen("BODY{font-family: \""), &m_dwWritten, NULL);
	WriteFile(m_hFile, W2T(bstrFontName), bstrFontName.Length(), &m_dwWritten, NULL);
	WriteFile(m_hFile, "\"; font-size:", strlen("\"; font-size:"), &m_dwWritten, NULL);
	WriteFile(m_hFile, szFontSize, strlen(szFontSize), &m_dwWritten, NULL);
	WriteFile(m_hFile, "px; }\n", strlen("px; }\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "</STYLE>\n", strlen("</STYLE>\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "<META NAME=\"GENERATOR\" Content=\"Microsoft Visual Studio .NET HTML From RTF Generator\">\n", strlen("<META NAME=\"GENERATOR\" Content=\"Microsoft Visual Studio .NET HTML From RTF Generator\">\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "<META HTTP-EQUIV=\"Content-Type\" content=\"text/html\">\n", strlen("<META HTTP-EQUIV=\"Content-Type\" content=\"text/html\">\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "</HEAD>\n", strlen("</HEAD>\n"), &m_dwWritten, NULL);
	WriteFile(m_hFile, "<BODY>\n", strlen("<BODY>\n"), &m_dwWritten, NULL);
	hr = ParseRTF2(&pszTemp);
#else
	pSelection->Insert(CComBSTR(L"<HTML>\n"), 1);
	pSelection->Insert(CComBSTR(L"<HEAD>\n"), 1);
	pSelection->Insert(CComBSTR(L"<TITLE>"), 1);
	pSelection->Insert(m_bstrName, 1);
	pSelection->Insert(CComBSTR(L"</TITLE>\n"), 1);
	pSelection->Insert(CComBSTR(L"<STYLE  TYPE=\"text/css\">\n"), 1);
	pSelection->Insert(CComBSTR(L"BODY{font-family: \""), 1);
	pSelection->Insert(bstrFontName, 1);
	pSelection->Insert(CComBSTR(L"\"; font-size:"), 1);
	pSelection->Insert(CComBSTR(szFontSize), 1);
	pSelection->Insert(CComBSTR(";}\n"), 1);
	pSelection->Insert(CComBSTR(L"</STYLE>\n"), 1);
	pSelection->Insert(CComBSTR(L"<META NAME=\"GENERATOR\" Content=\"Microsoft Visual Studio .NET HTML From RTF Generator\">\n"), 1);
	pSelection->Insert(CComBSTR(L"<META HTTP-EQUIV=\"Content-Type\" content=\"text/html\">\n"), 1);
	pSelection->Insert(CComBSTR(L"</HEAD>\n"), 1);
	pSelection->Insert(CComBSTR(L"<BODY>\n"), 1);
	hr = ParseRTF2(&pszTemp, pSelection);
#endif

	if(FAILED(hr))
		MessageBox(NULL, _T("Error!"), _T("RTF 2 HTML"), MB_ICONEXCLAMATION|MB_OK);
	else
	{
#ifdef WRITETODISK
		if(m_fFontDepth)
			WriteFile(m_hFile, "\n</FONT>\n", strlen("\n</FONT>\n"), &m_dwWritten, NULL);
		WriteFile(m_hFile, "</BODY>\n", strlen("</BODY>\n"), &m_dwWritten, NULL);
		WriteFile(m_hFile, "</HTML>\n", strlen("</HTML>\n"), &m_dwWritten, NULL);
#else
		if(m_fFontDepth)
			pSelection->Insert(CComBSTR(L"\n</FONT>\n"), 1);
		pSelection->Insert(CComBSTR(L"\n</BODY>\n"), 1);
		pSelection->Insert(CComBSTR(L"</HTML>\n"), 1);
#endif
	}

	delete []pszColorTbl;
	delete []pszFontTbl;
#ifdef WRITETODISK
	CloseHandle(m_hFile);
	m_pDTE->get_ItemOperations(&pItemOp);
	pItemOp->OpenFile(CComBSTR(szTempFile), CComBSTR(EnvDTE::vsViewKindPrimary), &pWindow);
	pWindow->put_Visible(VARIANT_TRUE);
#endif
	return S_OK;
}