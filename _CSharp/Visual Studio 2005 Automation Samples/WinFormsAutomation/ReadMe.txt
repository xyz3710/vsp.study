Copyright (c) Microsoft Corporation.  All rights reserved.

This sample demonstrates working with the object model provided by the Windows Forms designer. The Add-in will create a new Windows Application project, and use the object model to build up a form.

Once you load the sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin WinFormsAutomation.Connect
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

When run, the Add-in will create a new Windows Application project, and use the object model to build up a form.

If you wish to run this Add-in again, you will need to delete the folder 'TheProj' from the folder where
projects are created by default.