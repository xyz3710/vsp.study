'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System
Imports Microsoft.VisualStudio.CommandBars
Imports Extensibility
Imports EnvDTE
Imports EnvDTE80
Imports System.Runtime.InteropServices
Imports System.ComponentModel.Design
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Public Class Connect
	
    Implements IDTExtensibility2
	Implements IDTCommandTarget

    Dim _applicationObject As DTE2
    Dim _addInInstance As AddIn

    '''<summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
    Public Sub New()

    End Sub

    '''<summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
    '''<param name='application'>Root object of the host application.</param>
    '''<param name='connectMode'>Describes how the Add-in is being loaded.</param>
    '''<param name='addInInst'>Object representing this Add-in.</param>
    '''<remarks></remarks>
    Public Sub OnConnection(ByVal application As Object, ByVal connectMode As ext_ConnectMode, ByVal addInInst As Object, ByRef custom As Array) Implements IDTExtensibility2.OnConnection
        _applicationObject = CType(application, DTE2)
        _addInInstance = CType(addInInst, AddIn)
		If connectMode = ext_ConnectMode.ext_cm_UISetup Then

        Dim commands As Commands2 = CType(_applicationObject.Commands, Commands2)
        Dim toolsMenuName As String
        Try

            'If you would like to move the command to a different menu, change the word "Tools" to the 
            '  English version of the menu. This code will take the culture, append on the name of the menu
            '  then add the command to that menu. You can find a list of all the top-level menus in the file
            '  CommandBar.resx.
            Dim resourceManager As System.Resources.ResourceManager = New System.Resources.ResourceManager("WinFormsAutomation.CommandBar", System.Reflection.Assembly.GetExecutingAssembly())

            Dim cultureInfo As System.Globalization.CultureInfo = New System.Globalization.CultureInfo(_applicationObject.LocaleID)
            toolsMenuName = resourceManager.GetString(String.Concat(cultureInfo.TwoLetterISOLanguageName, "Tools"))

        Catch e As Exception
            'We tried to find a localized version of the word Tools, but one was not found.
            '  Default to the en-US word, which may work for the current culture.
            toolsMenuName = "Tools"
        End Try

        'Place the command on the tools menu.
        'Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
        Dim commandBars As CommandBars = CType(_applicationObject.CommandBars, CommandBars)
        Dim menuBarCommandBar As CommandBar = commandBars.Item("MenuBar")

        'Find the Tools command bar on the MenuBar command bar:
        Dim toolsControl As CommandBarControl = menuBarCommandBar.Controls.Item(toolsMenuName)
        Dim toolsPopup As CommandBarPopup = CType(toolsControl, CommandBarPopup)

        Try
            'Add a command to the Commands collection:
            Dim command As Command = commands.AddNamedCommand2(_addInInstance, "WinFormsAutomation", "WinFormsAutomation", "Executes the command for WinFormsAutomation", True, 59, Nothing, CType(vsCommandStatus.vsCommandStatusSupported, Integer) + CType(vsCommandStatus.vsCommandStatusEnabled, Integer), vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton)

            'Find the appropriate command bar on the MenuBar command bar:
            command.AddControl(toolsPopup.CommandBar, 1)
        Catch argumentException As System.ArgumentException
            'If we are here, then the exception is probably because a command with that name
            '  already exists. If so there is no need to recreate the command and we can 
            '  safely ignore the exception.
        End Try

		End If
	End Sub

    '''<summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
    '''<param name='disconnectMode'>Describes how the Add-in is being unloaded.</param>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnDisconnection(ByVal disconnectMode As ext_DisconnectMode, ByRef custom As Array) Implements IDTExtensibility2.OnDisconnection
    End Sub

    '''<summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification that the collection of Add-ins has changed.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnAddInsUpdate(ByRef custom As Array) Implements IDTExtensibility2.OnAddInsUpdate
    End Sub

    '''<summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnStartupComplete(ByRef custom As Array) Implements IDTExtensibility2.OnStartupComplete
    End Sub

    '''<summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnBeginShutdown(ByRef custom As Array) Implements IDTExtensibility2.OnBeginShutdown
    End Sub
	
    '''<summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
    '''<param name='commandName'>The name of the command to determine state for.</param>
    '''<param name='neededText'>Text that is needed for the command.</param>
    '''<param name='status'>The state of the command in the user interface.</param>
    '''<param name='commandText'>Text requested by the neededText parameter.</param>
    '''<remarks></remarks>
    Public Sub QueryStatus(ByVal commandName As String, ByVal neededText As vsCommandStatusTextWanted, ByRef status As vsCommandStatus, ByRef commandText As Object) Implements IDTCommandTarget.QueryStatus
        If neededText = vsCommandStatusTextWanted.vsCommandStatusTextWantedNone Then
            If commandName = "WinFormsAutomation.Connect.WinFormsAutomation" Then
                status = CType(vsCommandStatus.vsCommandStatusEnabled + vsCommandStatus.vsCommandStatusSupported, vsCommandStatus)
            Else
                status = vsCommandStatus.vsCommandStatusUnsupported
            End If
        End If
    End Sub

    '''<summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
    '''<param name='commandName'>The name of the command to execute.</param>
    '''<param name='executeOption'>Describes how the command should be run.</param>
    '''<param name='varIn'>Parameters passed from the caller to the command handler.</param>
    '''<param name='varOut'>Parameters passed from the command handler to the caller.</param>
    '''<param name='handled'>Informs the caller if the command was handled or not.</param>
    '''<remarks></remarks>
    Public Sub Exec(ByVal commandName As String, ByVal executeOption As vsCommandExecOption, ByRef varIn As Object, ByRef varOut As Object, ByRef handled As Boolean) Implements IDTCommandTarget.Exec
        handled = False
        If executeOption = vsCommandExecOption.vsCommandExecOptionDoDefault Then
            If commandName = "WinFormsAutomation.Connect.WinFormsAutomation" Then

                '' Winforms automation sample code starts here ...
                '' Create a Windows FOrms project and give the designer focus
                Dim templatePath As String = CType(_applicationObject.Solution, Solution2).GetProjectTemplate("WindowsApplication.zip", "CSharp")
                Dim targetDir As String = CStr(_applicationObject.Properties("Environment", "ProjectsAndSolution").Item("ProjectsLocation").Value) & "\TheProj"

                _applicationObject.Solution.AddFromTemplate(templatePath, targetDir, "TheProj")
                _applicationObject.Windows.Item("Form1.cs [Design]").Activate()

                '' Get IDesignerHost, the root of the forms designer object model
                Dim host As IDesignerHost
                host = CType(_applicationObject.ActiveWindow.Object, IDesignerHost)

                '' Add two buttons, enumerate and print components. 
                Dim button As IComponent
                button = host.CreateComponent(host.GetType("System.Windows.Forms.Button,System.Windows.Forms"))
                Dim button2 As IComponent
                button2 = host.CreateComponent(host.GetType("System.Windows.Forms.Button,System.Windows.Forms"))
                Dim parent As PropertyDescriptor = TypeDescriptor.GetProperties(button)("Parent")
                parent.SetValue(button, host.RootComponent)
                parent.SetValue(button2, host.RootComponent)
                PrintComponents("form with two buttons ...", host.Container.Components)

                '' Then remove one of the components, enumerate and print again.
                host.DestroyComponent(button2)
                PrintComponents("form after removing one button ...", host.Container.Components)

                '' Get the properties of the remaining component, then print their names
                Dim pdc As PropertyDescriptorCollection
                pdc = TypeDescriptor.GetProperties(button)
                PrintProperties(pdc)

                '' Get and set the value of the size proeprty, showing values
                '' before and after.
                Dim pd As PropertyDescriptor
                pd = pdc("Size")
                MsgBox("Default Button size = " + pd.GetValue(button).ToString())
                Dim sz As System.Drawing.Size
                sz = New Size(100, 60)
                pd.SetValue(button, sz)
                MsgBox("custom Button size = " + pd.GetValue(button).ToString())

                '' Select a component on a form
                MsgBox("Selecting button component ...")
                Dim sel As ISelectionService
                sel = CType(host.GetService(Type.GetType("System.ComponentModel.Design.ISelectionService,System")), System.ComponentModel.Design.ISelectionService)
                sel.SetSelectedComponents(New Object() {button})


                Try
                    'Access designer properties
                    MsgBox("Accessing designer grid size ...")
                    Dim opt As IDesignerOptionService
                    opt = CType(host.GetService(Type.GetType("System.ComponentModel.Design.IDesignerOptionService,System")), System.ComponentModel.Design.IDesignerOptionService)
                    Dim siz As Size = CType(opt.GetOptionValue("WindowsFormsDesigner\General", "GridSize"), Size)
                    MsgBox(siz.ToString())
                Catch e As System.Exception
                    MsgBox("failed accessing designer props")
                    MsgBox(e.ToString())
                End Try


                '' Access code file, add a variable
                Try
                    Dim code As FileCodeModel = _applicationObject.ActiveWindow.ProjectItem.FileCodeModel
                    Dim celt As CodeElement
                    Dim cls As CodeClass
                    For Each celt In code.CodeElements
                        '' Look for class at top level in file (happens for VB)
                        If (celt.Kind = vsCMElement.vsCMElementClass) Then
                            cls = CType(celt, CodeClass)
                            If (cls.Name = host.RootComponent.Site.Name) Then
                                cls.AddVariable("Foo", "System.String")
                                Exit For 'stop loop when done
                            End If
                        End If
                        '' If namespace, descend into members and assume find
                        '' form class at top level in the top-level namespace.
                        If (celt.Kind = vsCMElement.vsCMElementNamespace) Then
                            Dim celt2 As CodeElement
                            Dim ns As CodeNamespace = CType(celt, CodeNamespace)
                            For Each celt2 In ns.Members
                                If (celt2.Kind = vsCMElement.vsCMElementClass) Then
                                    cls = CType(celt2, CodeClass)
                                    If (cls.Name = host.RootComponent.Site.Name) Then
                                        cls.AddVariable("Foo", "System.String")
                                        Exit For 'stop loop when done
                                    End If
                                End If
                            Next
                            Exit For 'exit outer loop after searching first namespace
                        End If
                    Next
                Catch e As System.Exception
                    MsgBox("access code file")
                    MsgBox(e.ToString)
                End Try

                'Assign event handler to button click
                Try
                    Dim click As EventDescriptor
                    click = TypeDescriptor.GetEvents(button)("Click")
                    Dim eventSvc As IEventBindingService
                    eventSvc = CType(host.GetService(Type.GetType("System.ComponentModel.Design.IEventBindingService,System")), System.ComponentModel.Design.IEventBindingService)
                    Dim clickProp As PropertyDescriptor
                    clickProp = eventSvc.GetEventProperty(click)
                    clickProp.SetValue(button, "button1_OnClick")
                Catch e As System.Exception
                    MsgBox("assign event handler")
                    MsgBox(e.ToString)
                End Try

                'Hooking component adding event
                Try
                    Dim addSvc As IComponentChangeService
                    addSvc = CType(host.GetService(Type.GetType("System.ComponentModel.Design.IComponentChangeService,System")), System.ComponentModel.Design.IComponentChangeService)
                Catch e As System.Exception
                    MsgBox("hooking comp event")
                    MsgBox(e.ToString)
                End Try

                'Creat a menu on the form
                Try
                    Dim mainMenuComp As IComponent = host.CreateComponent(Type.GetType("System.Windows.Forms.MainMenu,System.Windows.Forms"), "mainMenu1")
                    Dim fileMenuItemComp As IComponent = host.CreateComponent(Type.GetType("System.Windows.Forms.MenuItem,System.Windows.Forms"), "fileMenuItem")
                    Dim menuItem2Comp As IComponent = host.CreateComponent(Type.GetType("System.Windows.Forms.MenuItem,System.Windows.Forms"), "mainItem2")
                    Dim menuItem3Comp As IComponent = host.CreateComponent(Type.GetType("System.Windows.Forms.MenuItem,System.Windows.Forms"), "mainItem3")
                    Dim menuItem4Comp As IComponent = host.CreateComponent(Type.GetType("System.Windows.Forms.MenuItem,System.Windows.Forms"), "mainItem4")

                    Dim mainMenu1 As MainMenu = CType(mainMenuComp, MainMenu)
                    Dim fileMenuItem As MenuItem = CType(fileMenuItemComp, MenuItem)
                    Dim menuItem2 As MenuItem = CType(menuItem2Comp, MenuItem)
                    Dim menuItem3 As MenuItem = CType(menuItem3Comp, MenuItem)
                    Dim menuItem4 As MenuItem = CType(menuItem4Comp, MenuItem)

                    fileMenuItem.Text = "&File"
                    menuItem2.Text = "&Edit"
                    menuItem3.Text = "&Open"
                    menuItem4.Text = "&Save"

                    mainMenu1.MenuItems.Add(fileMenuItem)
                    mainMenu1.MenuItems.Add(menuItem2)

                    fileMenuItem.MenuItems.Add(menuItem3)
                    fileMenuItem.MenuItems.Add(menuItem4)

                Catch e As System.Exception
                    MsgBox("adding menu")
                    MsgBox(e.ToString)
                End Try

                handled = True
                Exit Sub
            End If
        End If
    End Sub

    '''
    ''' The following code is not part of the addin template and provides helper
    ''' functions for the winforms automation sample.
    '''
    Public Sub PrintComponents(ByVal title As String, ByVal Components As ComponentCollection)
        Dim comp As Component
        Dim message As String = ""
        For Each comp In Components
            message = message + Microsoft.VisualBasic.Constants.vbCrLf + comp.ToString()
        Next
        MsgBox(title & Microsoft.VisualBasic.Constants.vbCrLf & message)
    End Sub

    Public Sub PrintProperties(ByVal Properties As PropertyDescriptorCollection)
        Dim message As String = "First ten properties ..." & Microsoft.VisualBasic.Constants.vbCrLf
        Dim i As Integer
        For i = 0 To 9
            message = message & Microsoft.VisualBasic.Constants.vbCrLf + Properties.Item(i).DisplayName
        Next
        MsgBox(message)
    End Sub
End Class
