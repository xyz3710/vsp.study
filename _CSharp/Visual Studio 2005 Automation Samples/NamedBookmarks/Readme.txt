Copyright (c) Microsoft Corporation.  All rights reserved.

The NamedBookmarks Add-in allows the users of the Visual Studio .NET 2005 IDE to save and navigate named bookmarks. 
In the NamedBookmarks dialog, you can manage you bookmarks with the "Drag and Drop" feature of the bookmark treeview. 
The name bookmark information is stored in a XML file "NamedBookmarks.xml" in the same directory of the solution file.  
Therefore all the projects in the same directory share the same named bookmark file.

Samples of the bookmark XML file:
<?xml version="1.0" encoding="utf-8"?>
<solution name="MySolution">
	<folder name="folder1">
		<folder name="subFolder1">
			<bookmark name="bookmark1" file="fileFullPath" line="10"></bookmark>
		</folder>
		<bookmark name="bookmark2" file="fileFullPath" line="1"></bookmark>
	</folder>
	<bookmark name="bookmark3" file="fileFullPath" line="1"></bookmark>
</solution>    

You can use this Add-in to add bookmarks for most types source files, including xml, html, aspx files.

Once you load the sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin NamedBookmarks.Connect
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)