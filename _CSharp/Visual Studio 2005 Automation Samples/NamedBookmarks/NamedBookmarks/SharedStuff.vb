'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports EnvDTE
Imports EnvDTE80
Imports System.Xml
Imports Microsoft.VisualBasic

Module SharedStuff
    Public Const APP_NAME As String = "NamedBookmarks"
    Public Const XML_NODE_BOOKMARK As String = "bookmark"
    Public Const XML_NODE_FOLDER As String = "folder"
    Public Const XML_ATTRIBUTE_FILE As String = "file"
    Public Const XML_ATTRIBUTE_LINE As String = "line"

    Public Const TEXTDOCUMENT As String = "TextDocument"
    Public Function GetXmlObject(ByVal strSolutionFullName As String) As Xml.XmlDocument
        GetXmlObject = New XmlDocument()
        Dim bXmlLoaded As Boolean = False
        If Dir(GetXmlFileName(strSolutionFullName)).Length > 0 Then
            Try
                GetXmlObject.Load(GetXmlFileName(strSolutionFullName))
                bXmlLoaded = True
            Catch ex As Exception
                bXmlLoaded = False
            End Try
        End If
        If bXmlLoaded = False Then GetXmlObject.LoadXml("<?xml version=""1.0"" encoding=""utf-8"" ?><solution />")

        'Samples of the bookmark XML file:
        '<?xml version="1.0" encoding="utf-8"?>
        '<solution name="MySolution">
        '	<folder name="folder1">
        '		<folder name="subFolder1">
        '			<bookmark name="bookmark1" file="fileFullPath" line="10"></bookmark>
        '		</folder>
        '		<bookmark name="bookmark2" file="fileFullPath" line="1"></bookmark>
        '	</folder>
        '	<bookmark name="bookmark3" file="fileFullPath" line="1"></bookmark>
        '</solution>    
    End Function
    Public Function GetSolutionName(ByVal strSolutionFullName As String) As String
        Dim nPos As Integer
        nPos = InStrRev(strSolutionFullName, "\")
        GetSolutionName = strSolutionFullName.Substring(nPos)
        nPos = InStr(GetSolutionName, ".")
        If nPos > 0 Then
            GetSolutionName = GetSolutionName.Substring(0, nPos - 1)
        End If
    End Function
    Public Function GetXmlFileName(ByVal strSolutionFullName As String) As String
        Dim strSolutionPath As String
        Dim strFilename As String = APP_NAME & ".xml"
        Dim nPos As Integer
        nPos = InStrRev(strSolutionFullName, "\")
        strSolutionPath = Left(strSolutionFullName, nPos)
        'The bookmark XML file is under the same directory of the solution file.
        'And the file name is always "NamedBookmark.xml"
        GetXmlFileName = strSolutionPath & strFilename
    End Function
    Public Function SetBookmarkForOpenedDoc(ByRef appObj As DTE2, ByVal strBookmarkName As String, ByVal strFullName As String, ByVal nLine As Integer) As Boolean
        SetBookmarkForOpenedDoc = False
        If appObj.Documents.Count = 0 Then
            Exit Function
        End If
        Dim wTemp As Window = appObj.ActiveWindow
        Dim d As Document
        strFullName = RemoveReadOnly(strFullName)
        For Each d In appObj.Documents
            Try
                If RemoveReadOnly(d.FullName).ToLower = strFullName.ToLower Then
                    d.Activate()
                    Dim nPos As Integer = InStrRev(strFullName, ".")
                    Dim ts As TextSelection
                    Dim ep As EditPoint
                    'Handle markup source files differently:
                    If InStr(strFullName, ".htm") > 0 _
                        Or strFullName.Substring(nPos - 1) = ".aspx" _
                        Or strFullName.Substring(nPos - 1) = ".asmx" _
                        Or InStr(strFullName, ".xsd") > 0 _
                        Or InStr(strFullName, ".xslt") > 0 _
                        Or InStr(strFullName, ".xml") > 0 Then

                        Dim htwin As HTMLWindow = CType(appObj.ActiveWindow.Object, HTMLWindow)
                        htwin.CurrentTab = vsHTMLTabs.vsHTMLTabsSource
                        Dim twin As TextWindow = CType(htwin.CurrentTabObject, TextWindow)
                        ts = twin.Selection
                    Else
                        Dim td As TextDocument = CType(appObj.ActiveDocument.Object, TextDocument)
                        ts = td.Selection
                    End If
                    Try
                        ts.GotoLine(nLine, False)
                        ts.EndOfLine(False)
                        ts.StartOfLine(vsStartOfLineOptions.vsStartOfLineOptionsFirstText, True)
                        ep = ts.ActivePoint.CreateEditPoint()
                        ep.SetBookmark()
                        SetBookmarkForOpenedDoc = True
                    Catch l_err As Exception
                        'An error will be thrown if the bookmark passes the end of the file.
                        MsgBox("Bookmark " & strBookmarkName & " not found.")
                    End Try
                    Exit For
                End If
            Catch ex As Exception
            End Try
        Next
        If Not (wTemp Is Nothing) Then wTemp.Activate()
    End Function

    Public Function OpenBookmarkDocument(ByRef appObj As DTE2, ByVal strBookmarkName As String, ByVal strFullName As String, ByVal nLine As Integer) As Boolean
        appObj.ExecuteCommand("File.OpenFile """ & strFullName & """")
        SetBookmarkForOpenedDoc(appObj, strBookmarkName, strFullName, nLine)
    End Function
    Public Structure BookMarkStruct
        Public file As String
        Public line As Integer
    End Structure
    Public Function RemoveReadOnly(ByVal strPath As String) As String
        Dim nPos As Integer
        nPos = InStr(strPath, " [Read Only]")
        If nPos = 0 Then
            RemoveReadOnly = strPath
            Exit Function
        End If
        RemoveReadOnly = Microsoft.VisualBasic.Left(strPath, nPos - 1)
    End Function
End Module
