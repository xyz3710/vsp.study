'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports EnvDTE
Imports EnvDTE80
Imports System.Environment
Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Windows.Forms

Public Class frmNamedBookMarks
    Private m_nDragLeft As Integer
    Private m_nDragTop As Integer
    Private m_bNewBookmarkAllowed As Boolean
    Private m_bMarkupSourceFile As Boolean
    Private m_ep As EnvDTE.EditPoint
    Private appObject As EnvDTE80.DTE2
    Private m_ndRoot As TreeNode
    Private m_gotoBookmark As Boolean = False


    Public Sub New(ByVal objDTE As EnvDTE80.DTE2)
        InitializeComponent()
        appObject = objDTE
    End Sub

    Private Sub frmNamedBookMarks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strFullName As String = ""

        Me.Left = appObject.MainWindow.Left + 50
        Me.Top = appObject.MainWindow.Top + 50

        Dim nClientLeft As Integer = Me.Width - Me.ClientSize.Width - 4
        Dim nClientTop As Integer = Me.Height - Me.ClientSize.Height - 4
        m_nDragLeft = tv.Left + nClientLeft
        m_nDragTop = tv.Top + nClientTop

        Dim xmlDoc As XmlDocument = SharedStuff.GetXmlObject(appObject.Solution.FullName)

        'Dim xmlNd As XmlNode
        Dim ndTree As System.Windows.Forms.TreeNode
        ndTree = New System.Windows.Forms.TreeNode("Solution '" & SharedStuff.GetSolutionName(appObject.Solution.FullName) & "'", 0, 0)
        m_ndRoot = ndTree
        tv.Nodes.Add(ndTree)
        AddNodesToParent(ndTree, xmlDoc.DocumentElement)
        tv.ExpandAll()
        tv.SelectedNode = ndTree

        m_bNewBookmarkAllowed = True
        m_ep = Nothing
        If appObject.Documents.Count = 0 Then
            m_bNewBookmarkAllowed = False
        Else
            strFullName = appObject.ActiveDocument.FullName
        End If

        If InStr(strFullName, RemoveReadOnly(appObject.ActiveWindow.Caption)) = 0 Then
            m_bNewBookmarkAllowed = False
        End If

        m_bMarkupSourceFile = False
        Dim nPos As Integer = InStrRev(strFullName, ".")
        If strFullName.Length > 0 Then
            If InStr(strFullName, ".html") > 0 _
                Or strFullName.Substring(nPos - 1) = ".aspx" _
                Or strFullName.Substring(nPos - 1) = ".asmx" _
                Or InStr(strFullName, ".xsd") > 0 _
                Or InStr(strFullName, ".xslt") > 0 _
                Or InStr(strFullName, ".xml") > 0 Then
                m_bMarkupSourceFile = True
            End If
        End If
        Dim ts As TextSelection

        If m_bNewBookmarkAllowed Then
            nPos = InStrRev(strFullName, "\")
            lblPath.Text = strFullName.Substring(0, nPos)
            lblFile.Text = strFullName.Substring(nPos)
            Try
                If m_bMarkupSourceFile Then
                    Dim htwin As EnvDTE.HTMLWindow = CType(appObject.ActiveWindow.Object, EnvDTE.HTMLWindow)
                    htwin.CurrentTab = vsHTMLTabs.vsHTMLTabsSource
                    Dim twin As EnvDTE.TextWindow = CType(htwin.CurrentTabObject, TextWindow)
                    ts = twin.Selection
                Else
                    Dim td As EnvDTE.TextDocument = CType(appObject.ActiveDocument.Object, TextDocument)
                    ts = td.Selection
                End If
                m_ep = ts.ActivePoint.CreateEditPoint()
                lblLine.Text = m_ep.Line.ToString()
                txtText.Text = m_ep.GetLines(m_ep.Line, m_ep.Line + 1).TrimStart()
                If txtText.Text.Length > 64 Then txtText.Text = txtText.Text.Substring(0, 64)
            Catch ex As Exception
                m_bNewBookmarkAllowed = False
                txtText.Text = "ERROR: No active text document.  You cannot create new bookmark."
            End Try
        End If

        optBookmark.Checked = m_bNewBookmarkAllowed
        optFolder.Checked = Not m_bNewBookmarkAllowed

        cmdDelete.Enabled = False
        cmdGoto.Enabled = False
    End Sub
    Private Sub AddNodesToParent(ByRef ndTreeParent As System.Windows.Forms.TreeNode, ByRef xmlParent As XmlNode)
        Dim xmlNd As XmlNode
        Dim ndTree As System.Windows.Forms.TreeNode
        Dim stBookmark As BookMarkStruct

        For Each xmlNd In xmlParent.ChildNodes
            If xmlNd.Name = XML_NODE_FOLDER Then
                ndTree = New System.Windows.Forms.TreeNode(xmlNd.Attributes(0).InnerText, 1, 1)
                ndTree.Tag = Nothing
                AddNodesToParent(ndTree, xmlNd)
            Else
                ndTree = New System.Windows.Forms.TreeNode(xmlNd.Attributes(0).InnerText, 3, 3)
                stBookmark = New BookMarkStruct()
                stBookmark.file = xmlNd.Attributes(1).InnerText
                stBookmark.line = Int32.Parse(xmlNd.Attributes(2).InnerText)
                ndTree.Tag = stBookmark
            End If
            ndTreeParent.Nodes.Add(ndTree)
        Next
    End Sub

    Private Sub OpenBookMark()
        If Dir(lblPath.Text & lblFile.Text).Length = 0 Then
            MsgBox("File: " & lblPath.Text & lblFile.Text & " not found.")
            Exit Sub
        End If
        OpenBookmarkDocument(appObject, tv.SelectedNode.Text, lblPath.Text & lblFile.Text, Int32.Parse(lblLine.Text))
        m_gotoBookmark = True
        CloseMyself()
    End Sub

    Private Sub CloseMyself()
        Me.Close()
    End Sub



    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim strFullname As String = appObject.ActiveDocument.FullName
        Dim ndTreeNew As TreeNode
        Dim stBookmark As BookMarkStruct

        If Not (tv.SelectedNode.Tag) Is Nothing Then
            tv.SelectedNode = tv.SelectedNode.Parent
        End If
        If optFolder.Checked Then
            ndTreeNew = New TreeNode("New Folder", 1, 1)
            ndTreeNew.Tag = Nothing
        Else
            If InStr(strFullname, "~") > 0 Then
                MsgBox("The active document is a temp file. Named book marks cannot be placed in it.  Please save the file first.")
                Exit Sub
            End If
            stBookmark = New BookMarkStruct()
            stBookmark.file = strFullname
            stBookmark.line = m_ep.Line()
            ndTreeNew = New TreeNode("New Bookmark", 3, 3)
            ndTreeNew.Tag = stBookmark
        End If
        tv.SelectedNode.Nodes.Add(ndTreeNew)
        tv.SelectedNode = ndTreeNew
        If Not (ndTreeNew.Tag Is Nothing) Then
            m_ep.SetBookmark()
        End If
        ndTreeNew.BeginEdit()

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If MsgBox("Do you want to delete " & tv.SelectedNode.Text & "?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim ndTree As TreeNode = tv.SelectedNode
            ndTree.Remove()
            lblPath.Text = ""
            lblFile.Text = ""
            lblLine.Text = ""
            tv.Refresh()
        End If
    End Sub

    Private Sub cmdGoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGoto.Click
        OpenBookMark()
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        CloseMyself()
    End Sub

    Private Sub tv_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterSelect
        If Not (e.Node.Tag Is Nothing) Then
            Dim stBookmark As BookMarkStruct
            Dim nPos As Integer
            'Dim strFile As String
            Dim bFound As Boolean
            bFound = False
            stBookmark = CType(e.Node.Tag, BookMarkStruct)
            nPos = InStrRev(stBookmark.file, "\")
            lblFile.Text = stBookmark.file.Substring(nPos)
            lblPath.Text = stBookmark.file.Substring(0, nPos)
            lblLine.Text = stBookmark.line.ToString()
            txtText.Text = ""
            Dim doc As EnvDTE.Document
            Dim td As EnvDTE.TextDocument
            Dim ep As EnvDTE.EditPoint
            'Show the line of code of the bookmark:
            For Each doc In appObject.Documents
                If doc.FullName.ToLower = stBookmark.file.ToLower Then
                    td = CType(doc.Object, TextDocument)
                    ep = td.CreateEditPoint(td.StartPoint)
                    Try
                        txtText.Text = ep.GetLines(stBookmark.line, stBookmark.line + 1).Trim()
                    Catch ex As Exception
                        txtText.Text = "ERROR: Line " & lblLine.Text & " not found."
                    End Try
                    bFound = True
                    Exit For
                End If
            Next
            If bFound = False Then
                If Dir(stBookmark.file).Length > 0 Then
                    Dim nFile As Integer
                    Dim i As Integer
                    nFile = FreeFile()
                    FileOpen(nFile, stBookmark.file, OpenMode.Input)
                    Try
                        For i = 1 To stBookmark.line
                            txtText.Text = LineInput(nFile)
                        Next
                    Catch ex As Exception
                        txtText.Text = "ERROR: Line " & lblLine.Text & " not found."
                    End Try
                    FileClose(nFile)
                Else
                    txtText.Text = "ERROR: File not found."
                End If
            End If
            If txtText.Text.Length > 64 Then txtText.Text = txtText.Text.Substring(0, 64)
            cmdGoto.Enabled = True
        Else
            cmdGoto.Enabled = False
        End If
        If e.Node.Parent Is Nothing Then
            cmdDelete.Enabled = False
        Else
            cmdDelete.Enabled = True
        End If
    End Sub

    Private Sub tv_AfterCollapse(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterCollapse
        If e.Node.Parent Is Nothing Then Exit Sub
        e.Node.ImageIndex = 1
        e.Node.SelectedImageIndex = 1
    End Sub

    Private Sub tv_AfterExpand(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterExpand
        If e.Node.Parent Is Nothing Then Exit Sub
        e.Node.ImageIndex = 2
        e.Node.SelectedImageIndex = 2
    End Sub

    Private Sub tv_AfterLabelEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles tv.AfterLabelEdit
        Dim nd As TreeNode
        If e.Node.Parent Is Nothing Then
            MsgBox("Can't change the solution node name")
            e.CancelEdit = True
            Exit Sub
        End If
        Dim strNew As String
        If e.Label Is Nothing Then
            strNew = e.Node.Text
        Else
            strNew = e.Label
        End If

        If strNew.Length = 0 Then
            MsgBox("Blank name not allowed.")
            e.CancelEdit = True
            If e.Node.Text = "New Bookmark" Or e.Node.Text = "New Folder" Then e.Node.BeginEdit()
            Exit Sub
        End If

        For Each nd In tv.SelectedNode.Parent.Nodes
            If Not (nd Is e.Node) And nd.Text.ToLower() = strNew.ToLower() Then
                MsgBox("Duplicated name.")
                e.CancelEdit = True
                If e.Node.Text = "New Bookmark" Or e.Node.Text = "New Folder" Then e.Node.BeginEdit()
                Exit Sub
            End If
        Next
    End Sub

    Private Sub tv_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tv.DoubleClick
        If Not (tv.SelectedNode.Tag Is Nothing) Then
            OpenBookMark()
        End If
    End Sub

    Private Sub tv_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tv.DragDrop
        e.Effect = DragDropEffects.Move
        Dim ndSource As TreeNode = CType(e.Data.GetData("System.Windows.Forms.TreeNode", False), TreeNode)
        Dim ndTarget As TreeNode = tv.GetNodeAt(New System.Drawing.Point(e.X - Me.Left - m_nDragLeft, e.Y - Me.Top - m_nDragTop))
        If Not (ndTarget.Tag Is Nothing) Then
            MsgBox("Can't move to a bookmark.")
            Exit Sub
        End If
        Dim ndTemp As TreeNode
        ndTemp = ndTarget.Parent
        While Not (ndTemp Is Nothing)
            If ndTemp Is ndSource Then
                MsgBox("Can't move the parent node to a child node.")
                Exit Sub
            End If
            ndTemp = ndTemp.Parent
        End While
        If ndSource Is ndTarget Then
            MsgBox("Can't move the selected node to the node itself.")
            Exit Sub
        End If
        For Each ndTemp In ndTarget.Nodes
            If ndTemp.Text.ToLower = ndSource.Text.ToLower Then
                MsgBox("The target folder already has a subfolder/bookmark with duplicated name: " & ndSource.Text)
                Exit Sub
            End If
        Next
        If Not (ndTarget Is Nothing) Then
            ndSource.Remove()
            ndTarget.Nodes.Add(ndSource)
        End If
    End Sub

    Private Sub tv_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tv.DragOver
        e.Effect = DragDropEffects.Move
        Dim ndTarget As TreeNode = tv.GetNodeAt(New System.Drawing.Point(e.X - Me.Left - m_nDragLeft, e.Y - Me.Top - m_nDragTop))
        If Not (ndTarget Is Nothing) Then
            tv.SelectedNode = ndTarget
            If ndTarget.IsExpanded = False Then
                ndTarget.Expand()
            End If
        End If
    End Sub

    Private Sub tv_ItemDrag(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles tv.ItemDrag
        Dim ndSource As TreeNode = CType(e.Item, TreeNode)
        tv.SelectedNode = ndSource
        tv.DoDragDrop(ndSource, DragDropEffects.Move)
    End Sub

    Private Function GetNodePath(ByVal ndTree As TreeNode) As String
        Dim ndTemp As TreeNode
        'Note: we can just use Treenode.Fullpath here, but "->" looks better than "\"
        GetNodePath = ""
        ndTemp = ndTree.Parent
        While Not (ndTemp.Parent Is Nothing)
            GetNodePath = ndTemp.Text & "->" & GetNodePath
            ndTemp = ndTemp.Parent
        End While
        GetNodePath = GetNodePath & ndTree.Text
    End Function

    Private Sub frmNamedBookMarks_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim xmlDoc As XmlDocument = New XmlDocument()
        xmlDoc.LoadXml("<?xml version=""1.0"" encoding=""utf-8""?><solution />")
        'Rebuild the xml when closing the dialog:
        SaveSubFolder(xmlDoc, m_ndRoot, xmlDoc.DocumentElement)
        'Since the folder or the bookmark might have been changed, we have to reload the tasklist items:
        ReloadTaskListItems()
        If m_gotoBookmark Then
            OpenBookmarkDocument(appObject, tv.SelectedNode.Text, lblPath.Text & lblFile.Text, Int32.Parse(lblLine.Text))
        End If
        xmlDoc.Save(SharedStuff.GetXmlFileName(appObject.Solution.FullName))
    End Sub
    Private Sub SaveSubFolder(ByRef xmlDoc As XmlDocument, ByRef ndTreeParent As TreeNode, ByRef ndXmlParent As XmlNode)
        Dim ndTreeChild As TreeNode
        Dim ndXmlChild As XmlNode
        For Each ndTreeChild In ndTreeParent.Nodes
            If ndTreeChild.Tag Is Nothing Then
                ndXmlChild = xmlDoc.CreateNode(XmlNodeType.Element, "folder", "")
                ndXmlChild.Attributes.Append(xmlDoc.CreateAttribute("name"))
                ndXmlChild.Attributes(0).InnerText = ndTreeChild.Text
                SaveSubFolder(xmlDoc, ndTreeChild, ndXmlChild)
            Else
                Dim stBookmark As BookMarkStruct = CType(ndTreeChild.Tag, BookMarkStruct)
                ndXmlChild = xmlDoc.CreateNode(XmlNodeType.Element, "bookmark", "")
                ndXmlChild.Attributes.Append(xmlDoc.CreateAttribute("name"))
                ndXmlChild.Attributes(0).InnerText = ndTreeChild.Text
                ndXmlChild.Attributes.Append(xmlDoc.CreateAttribute("file"))
                ndXmlChild.Attributes(1).InnerText = stBookmark.file
                ndXmlChild.Attributes.Append(xmlDoc.CreateAttribute("line"))
                ndXmlChild.Attributes(2).InnerText = stBookmark.line.ToString()
            End If
            ndXmlParent.AppendChild(ndXmlChild)
        Next
    End Sub
    Private Sub ReloadTaskListItems()
        Dim win As Window = appObject.Windows.Item(EnvDTE.Constants.vsWindowKindTaskList)
        Dim TL As TaskList = CType(win.Object, TaskList)
        Dim ti As TaskItem
        For Each ti In TL.TaskItems
            If ti.Category = APP_NAME Then
                ti.Delete()
            End If
        Next
        ReloadSubFolderTaskItems(m_ndRoot)
    End Sub
    Private Sub ReloadSubFolderTaskItems(ByRef ndTreeParent As TreeNode)
        Dim win As Window = appObject.Windows.Item(EnvDTE.Constants.vsWindowKindTaskList)
        Dim TL As TaskList = CType(win.Object, TaskList)
        Dim ndTreeChild As TreeNode
        For Each ndTreeChild In ndTreeParent.Nodes
            If ndTreeChild.Tag Is Nothing Then
                ReloadSubFolderTaskItems(ndTreeChild)
            Else
                Dim stBookmark As BookMarkStruct
                stBookmark = CType(ndTreeChild.Tag, BookMarkStruct)
                TL.TaskItems.Add(SharedStuff.APP_NAME, _
                    SharedStuff.APP_NAME, _
                    GetNodePath(ndTreeChild), _
                    vsTaskPriority.vsTaskPriorityMedium, _
                    vsTaskIcon.vsTaskIconShortcut, _
                    True, _
                    stBookmark.file, _
                    stBookmark.line, _
                    True, _
                    True)
                SetBookmarkForOpenedDoc(appObject, _
                    GetNodePath(ndTreeChild), _
                    stBookmark.file, _
                    stBookmark.line)
            End If
        Next
    End Sub

    Private Sub optBookmark_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optBookmark.CheckedChanged
        If (m_ep Is Nothing) And optBookmark.Checked Then
            MsgBox("No active source document.")
            optBookmark.Checked = False
            optFolder.Checked = True
        End If
    End Sub

End Class