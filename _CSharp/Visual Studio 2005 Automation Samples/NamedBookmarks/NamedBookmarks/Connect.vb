'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System
Imports Microsoft.VisualStudio.CommandBars
Imports Microsoft.VisualBasic
Imports Extensibility
Imports EnvDTE
Imports EnvDTE80
Imports System.Xml

Public Class Connect
	
    Implements IDTExtensibility2
	Implements IDTCommandTarget

    Dim _applicationObject As DTE2
    Dim _addInInstance As AddIn

    Dim objNbmForm As frmNamedBookMarks

    'In this Add-in, we need to work on two types of events:
    Public WithEvents x_solutionEvents As EnvDTE.SolutionEvents
    Public WithEvents x_taskListEvents As EnvDTE.TaskListEvents

    Private Sub x_taskListEvents_TaskModified(ByVal TaskItem As EnvDTE.TaskItem, ByVal ColumnModified As EnvDTE.vsTaskListColumn) Handles x_taskListEvents.TaskModified
        'Remind the user that the bookmark can be deleted by removing the task item:
        If TaskItem.Category = SharedStuff.APP_NAME And TaskItem.Checked = True Then
            MsgBox("You can remove the bookmark by deleting the task item.")
        End If
    End Sub

    Private Sub x_taskListEvents_TaskNavigated(ByVal TaskItem As EnvDTE.TaskItem, ByRef NavigateHandled As Boolean) Handles x_taskListEvents.TaskNavigated
        If TaskItem.Category <> SharedStuff.APP_NAME Then
            Exit Sub
        End If
        NavigateHandled = False
        If Not System.IO.File.Exists(TaskItem.FileName) Then
            MsgBox("File not found.")
            Exit Sub
        End If
        OpenBookmarkDocument(_applicationObject, TaskItem.Description, TaskItem.FileName, TaskItem.Line)
    End Sub

    Private Sub x_taskListEvents_TaskRemoved(ByVal TaskItem As EnvDTE.TaskItem) Handles x_taskListEvents.TaskRemoved
        If TaskItem.Category <> SharedStuff.APP_NAME Or _applicationObject.Solution.FullName.Length = 0 Then
            Exit Sub
        End If
        Dim strFileName As String = TaskItem.FileName
        Dim nLine As Integer = TaskItem.Line

        Try
            Dim td As TextDocument
            Dim doc As Document
            Dim ep As EditPoint
            For Each doc In _applicationObject.Documents
                If doc.FullName = strFileName Then
                    td = CType(doc.Object, TextDocument)
                    ep = td.CreateEditPoint(td.StartPoint)
                    ep.MoveToLineAndOffset(nLine, 1)
                    ep.ClearBookmark()
                    Exit For
                End If
            Next
        Catch l_err As Exception
            MsgBox(l_err.Message)
        End Try

        'Update the XML file:
        Dim xmlDoc As XmlDocument = SharedStuff.GetXmlObject(_applicationObject.Solution.FullName)
        If DeleteBookmarkInXml(TaskItem.Description, xmlDoc.DocumentElement) = False Then
            'Bookmark not found in xml file
            MsgBox("Can't remove the bookmark because the name has been changed.  Please remove it in the NamedBookmarks dialog.")
        End If
        xmlDoc.Save(SharedStuff.GetXmlFileName(_applicationObject.Solution.FullName))
    End Sub

    Private Function DeleteBookmarkInXml(ByVal strBookmarkName As String, ByRef ndXmlParent As XmlNode, Optional ByVal strParentPath As String = "") As Boolean
        Dim ndXmlChild As XmlNode
        Dim obj As Object
        For Each ndXmlChild In ndXmlParent
            If ndXmlChild.Name = XML_NODE_FOLDER Then
                Dim strChildPath As String
                If strParentPath.Length > 0 Then
                    strChildPath = strParentPath & "->" & ndXmlChild.Attributes(0).InnerText
                Else
                    strChildPath = ndXmlChild.Attributes(0).InnerText
                End If
                'Do this recursively until the bookmark name is matched:
                DeleteBookmarkInXml = DeleteBookmarkInXml(strBookmarkName, ndXmlChild, strChildPath)
                If DeleteBookmarkInXml = True Then Exit Function
            Else
                obj = IIf(strParentPath.Length = 0, ndXmlChild.Attributes(0).InnerText, strParentPath & "->" & ndXmlChild.Attributes(0).InnerText)
                If strBookmarkName = obj.ToString Then
                    ndXmlParent.RemoveChild(ndXmlChild)
                    DeleteBookmarkInXml = True
                    Exit Function
                End If
            End If
        Next
    End Function

    '''<summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
    Public Sub New()

    End Sub

    '''<summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
    '''<param name='application'>Root object of the host application.</param>
    '''<param name='connectMode'>Describes how the Add-in is being loaded.</param>
    '''<param name='addInInst'>Object representing this Add-in.</param>
    '''<remarks></remarks>
    Public Sub OnConnection(ByVal application As Object, ByVal connectMode As ext_ConnectMode, ByVal addInInst As Object, ByRef custom As Array) Implements IDTExtensibility2.OnConnection
        _applicationObject = CType(application, DTE2)
        _addInInstance = CType(addInInst, AddIn)

        Dim commands As Commands2 = CType(_applicationObject.Commands, Commands2)
        Dim toolsMenuName As String

        If connectMode = ext_ConnectMode.ext_cm_UISetup Then

            Try

                'If you would like to move the command to a different menu, change the word "Tools" to the 
                '  English version of the menu. This code will take the culture, append on the name of the menu
                '  then add the command to that menu. You can find a list of all the top-level menus in the file
                '  CommandBar.resx.
                Dim resourceManager As System.Resources.ResourceManager = New System.Resources.ResourceManager("NamedBookmarks.CommandBar", System.Reflection.Assembly.GetExecutingAssembly())

                Dim cultureInfo As System.Globalization.CultureInfo = New System.Globalization.CultureInfo(_applicationObject.LocaleID)
                toolsMenuName = resourceManager.GetString(String.Concat(cultureInfo.TwoLetterISOLanguageName, "Edit"))

            Catch e As Exception
                'We tried to find a localized version of the word Tools, but one was not found.
                '  Default to the en-US word, which may work for the current culture.
                toolsMenuName = "Edit"
            End Try

            'Place the command on the tools menu.
            'Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
            Dim commandBars As CommandBars = CType(_applicationObject.CommandBars, CommandBars)
            Dim menuBarCommandBar As CommandBar = commandBars.Item("MenuBar")

            'Find the Tools command bar on the MenuBar command bar:
            Dim toolsControl As CommandBarControl = menuBarCommandBar.Controls.Item(toolsMenuName)
            Dim toolsPopup As CommandBarPopup = CType(toolsControl, CommandBarPopup)

            Try
                'Add a command to the Commands collection:
                Dim command As Command = commands.AddNamedCommand2(_addInInstance, "NamedBookmarks", "NamedBookmarks", "Executes the command for NamedBookmarks", True, 59, Nothing, CType(vsCommandStatus.vsCommandStatusSupported, Integer) + CType(vsCommandStatus.vsCommandStatusEnabled, Integer), vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton)

                'Find the appropriate command bar on the MenuBar command bar:
                command.AddControl(toolsPopup.CommandBar, 1)
                MsgBox("To make sure the NamedBookmarks Add-in works properly, please check the Startup check box on in the Add-in Manager dialog.")
            Catch argumentException As System.ArgumentException
                'If we are here, then the exception is probably because a command with that name
                '  already exists. If so there is no need to recreate the command and we can 
                '  safely ignore the exception.
            End Try

        End If
	End Sub

    '''<summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
    '''<param name='disconnectMode'>Describes how the Add-in is being unloaded.</param>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnDisconnection(ByVal disconnectMode As ext_DisconnectMode, ByRef custom As Array) Implements IDTExtensibility2.OnDisconnection
    End Sub

    '''<summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification that the collection of Add-ins has changed.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnAddInsUpdate(ByRef custom As Array) Implements IDTExtensibility2.OnAddInsUpdate
    End Sub

    '''<summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnStartupComplete(ByRef custom As Array) Implements IDTExtensibility2.OnStartupComplete
        Dim events As EnvDTE.Events
        events = _applicationObject.Events
        'Enable these events:
        x_solutionEvents = CType(events.SolutionEvents, EnvDTE.SolutionEvents)
        x_taskListEvents = CType(events.TaskListEvents(SharedStuff.APP_NAME), EnvDTE.TaskListEvents)
        'If the solution is open by doubleclicking the solution file, we need to load the bookmarks after the IDE is opened:
        If _applicationObject.Solution.FullName.Length > 0 Then
            LoadBookmarkTaskItems()
        End If
    End Sub

    '''<summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnBeginShutdown(ByRef custom As Array) Implements IDTExtensibility2.OnBeginShutdown
    End Sub
	
    '''<summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
    '''<param name='commandName'>The name of the command to determine state for.</param>
    '''<param name='neededText'>Text that is needed for the command.</param>
    '''<param name='status'>The state of the command in the user interface.</param>
    '''<param name='commandText'>Text requested by the neededText parameter.</param>
    '''<remarks></remarks>
    Public Sub QueryStatus(ByVal commandName As String, ByVal neededText As vsCommandStatusTextWanted, ByRef status As vsCommandStatus, ByRef commandText As Object) Implements IDTCommandTarget.QueryStatus
        If neededText = vsCommandStatusTextWanted.vsCommandStatusTextWantedNone Then
            If commandName = "NamedBookmarks.Connect.NamedBookmarks" Then
                status = CType(vsCommandStatus.vsCommandStatusEnabled + vsCommandStatus.vsCommandStatusSupported, vsCommandStatus)
            Else
                status = vsCommandStatus.vsCommandStatusUnsupported
            End If
        End If
    End Sub

    '''<summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
    '''<param name='commandName'>The name of the command to execute.</param>
    '''<param name='executeOption'>Describes how the command should be run.</param>
    '''<param name='varIn'>Parameters passed from the caller to the command handler.</param>
    '''<param name='varOut'>Parameters passed from the command handler to the caller.</param>
    '''<param name='handled'>Informs the caller if the command was handled or not.</param>
    '''<remarks></remarks>
    Public Sub Exec(ByVal commandName As String, ByVal executeOption As vsCommandExecOption, ByRef varIn As Object, ByRef varOut As Object, ByRef handled As Boolean) Implements IDTCommandTarget.Exec
        If _applicationObject.Solution Is Nothing Then
            MsgBox("There is no opened solution in the IDE.")
        Else
            If _applicationObject.Solution.FullName.Length > 0 Then
                objNbmForm = New frmNamedBookMarks(_applicationObject)
                'Show the NamedBookmark dialog as modal:
                objNbmForm.ShowDialog()
            Else
                MsgBox("There is no saved solution in the IDE.")
            End If
        End If

        handled = False
        If executeOption = vsCommandExecOption.vsCommandExecOptionDoDefault Then
            If commandName = "NamedBookmarks.Connect.NamedBookmarks" Then
                handled = True
                Exit Sub
            End If
        End If
	End Sub

    Private Sub x_solutionEvents_AfterClosing() Handles x_solutionEvents.AfterClosing
        Dim window As Window = _applicationObject.Windows.Item(EnvDTE.Constants.vsWindowKindTaskList)
        Dim tskList As TaskList = CType(window.Object, TaskList)
        Dim tlItem As TaskItem
        'Cleanup task list:
        For Each tlItem In tskList.TaskItems
            If tlItem.Category = SharedStuff.APP_NAME Then
                tlItem.Delete()
            End If
        Next
    End Sub

    Private Sub x_solutionEvents_Opened() Handles x_solutionEvents.Opened
        'Load the bookmarks to the task list if the solution is opened
        If _applicationObject.Solution.FullName.Length > 0 Then
            LoadBookmarkTaskItems()
        End If
    End Sub

    Private Sub LoadBookmarkTaskItems()
        Dim xmlDoc As XmlDocument = SharedStuff.GetXmlObject(_applicationObject.Solution.FullName)
        LoadBookmarkInSubFolder(xmlDoc.DocumentElement)
    End Sub
    Private Sub LoadBookmarkInSubFolder(ByRef ndXmlParent As XmlNode, Optional ByVal strNodePath As String = "")
        Dim win As Window = _applicationObject.Windows.Item(EnvDTE.Constants.vsWindowKindTaskList)
        Dim TL As TaskList = CType(win.Object, TaskList)
        Dim ndXmlChild As XmlNode
        For Each ndXmlChild In ndXmlParent.ChildNodes
            If ndXmlChild.Name = XML_NODE_FOLDER Then
                LoadBookmarkInSubFolder(ndXmlChild, strNodePath & ndXmlChild.Attributes(0).InnerText & "->")
            Else
                TL.TaskItems.Add(SharedStuff.APP_NAME, _
                    SharedStuff.APP_NAME, _
                    strNodePath & ndXmlChild.Attributes(0).InnerText, _
                    vsTaskPriority.vsTaskPriorityMedium, _
                    vsTaskIcon.vsTaskIconShortcut, _
                    True, _
                    ndXmlChild.Attributes(1).InnerText, _
                    ndXmlChild.Attributes(2).InnerText, _
                    True, _
                    True)
            End If
        Next
    End Sub

End Class
