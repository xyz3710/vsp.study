<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNamedBookMarks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNamedBookMarks))
        Me.optBookmark = New System.Windows.Forms.RadioButton
        Me.optFolder = New System.Windows.Forms.RadioButton
        Me.imglst = New System.Windows.Forms.ImageList(Me.components)
        Me.txtText = New System.Windows.Forms.TextBox
        Me.lblLine = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tv = New System.Windows.Forms.TreeView
        Me.lblFile = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmdClose = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdGoto = New System.Windows.Forms.Button
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.lblPath = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'optBookmark
        '
        Me.optBookmark.Location = New System.Drawing.Point(8, 24)
        Me.optBookmark.Name = "optBookmark"
        Me.optBookmark.Size = New System.Drawing.Size(80, 16)
        Me.optBookmark.TabIndex = 0
        Me.optBookmark.Text = "Bookmark"
        '
        'optFolder
        '
        Me.optFolder.Checked = True
        Me.optFolder.Location = New System.Drawing.Point(8, 48)
        Me.optFolder.Name = "optFolder"
        Me.optFolder.Size = New System.Drawing.Size(80, 16)
        Me.optFolder.TabIndex = 1
        Me.optFolder.TabStop = True
        Me.optFolder.Text = "Folder"
        '
        'imglst
        '
        Me.imglst.ImageStream = CType(resources.GetObject("imglst.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imglst.TransparentColor = System.Drawing.Color.Transparent
        Me.imglst.Images.SetKeyName(0, "")
        Me.imglst.Images.SetKeyName(1, "")
        Me.imglst.Images.SetKeyName(2, "")
        Me.imglst.Images.SetKeyName(3, "")
        '
        'txtText
        '
        Me.txtText.Location = New System.Drawing.Point(40, 324)
        Me.txtText.Name = "txtText"
        Me.txtText.Size = New System.Drawing.Size(344, 20)
        Me.txtText.TabIndex = 34
        '
        'lblLine
        '
        Me.lblLine.Location = New System.Drawing.Point(320, 308)
        Me.lblLine.Name = "lblLine"
        Me.lblLine.Size = New System.Drawing.Size(64, 16)
        Me.lblLine.TabIndex = 33
        Me.lblLine.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 324)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 16)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Text:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optFolder)
        Me.GroupBox1.Controls.Add(Me.optBookmark)
        Me.GroupBox1.Location = New System.Drawing.Point(288, 36)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(96, 72)
        Me.GroupBox1.TabIndex = 31
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Type"
        '
        'tv
        '
        Me.tv.AllowDrop = True
        Me.tv.HideSelection = False
        Me.tv.ImageIndex = 0
        Me.tv.ImageList = Me.imglst
        Me.tv.LabelEdit = True
        Me.tv.Location = New System.Drawing.Point(8, 4)
        Me.tv.Name = "tv"
        Me.tv.SelectedImageIndex = 0
        Me.tv.Size = New System.Drawing.Size(272, 264)
        Me.tv.TabIndex = 30
        '
        'lblFile
        '
        Me.lblFile.Location = New System.Drawing.Point(40, 308)
        Me.lblFile.Name = "lblFile"
        Me.lblFile.Size = New System.Drawing.Size(232, 16)
        Me.lblFile.TabIndex = 29
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 308)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "File:"
        '
        'cmdClose
        '
        Me.cmdClose.Location = New System.Drawing.Point(288, 244)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(96, 23)
        Me.cmdClose.TabIndex = 27
        Me.cmdClose.Text = "&Close"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(288, 308)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 16)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Line:"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 276)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 16)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Path:"
        '
        'cmdGoto
        '
        Me.cmdGoto.Location = New System.Drawing.Point(288, 204)
        Me.cmdGoto.Name = "cmdGoto"
        Me.cmdGoto.Size = New System.Drawing.Size(96, 23)
        Me.cmdGoto.TabIndex = 24
        Me.cmdGoto.Text = "&Goto"
        '
        'cmdDelete
        '
        Me.cmdDelete.Location = New System.Drawing.Point(288, 164)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(96, 23)
        Me.cmdDelete.TabIndex = 23
        Me.cmdDelete.Text = "&Delete"
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(288, 4)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(96, 23)
        Me.cmdAdd.TabIndex = 22
        Me.cmdAdd.Text = "&Add"
        '
        'lblPath
        '
        Me.lblPath.Location = New System.Drawing.Point(40, 276)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.Size = New System.Drawing.Size(344, 32)
        Me.lblPath.TabIndex = 21
        '
        'frmNamedBookMarks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(392, 349)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.lblLine)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.tv)
        Me.Controls.Add(Me.lblFile)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdGoto)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.lblPath)
        Me.Name = "frmNamedBookMarks"
        Me.Text = "frmNamedBookMarks"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents optBookmark As System.Windows.Forms.RadioButton
    Friend WithEvents optFolder As System.Windows.Forms.RadioButton
    Friend WithEvents imglst As System.Windows.Forms.ImageList
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents lblLine As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tv As System.Windows.Forms.TreeView
    Friend WithEvents lblFile As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdGoto As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents lblPath As System.Windows.Forms.Label
End Class
