Copyright (c) Microsoft Corporation.  All rights reserved.

This wizard prompts for a directory of JPEG files, adds an HTML page to your project, crunches all of the JPEGs down to thumbnails (by using .NET image classes) and lays them out on the HTML page.

