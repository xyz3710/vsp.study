'Copyright (c) Microsoft Corporation.  All rights reserved.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WizardFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.shownameCheckBox = New System.Windows.Forms.CheckBox
        Me.heightUpDown = New System.Windows.Forms.NumericUpDown
        Me.Label7 = New System.Windows.Forms.Label
        Me.widthUpDown = New System.Windows.Forms.NumericUpDown
        Me.columnsUpDown = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.Label6 = New System.Windows.Forms.Label
        Me.removeButton = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.addButton = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.imagesListView = New System.Windows.Forms.ListView
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.finishButton = New System.Windows.Forms.Button
        Me.cancelBtn = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        CType(Me.heightUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.widthUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.columnsUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'shownameCheckBox
        '
        Me.shownameCheckBox.Location = New System.Drawing.Point(7, 287)
        Me.shownameCheckBox.Name = "shownameCheckBox"
        Me.shownameCheckBox.Size = New System.Drawing.Size(264, 16)
        Me.shownameCheckBox.TabIndex = 31
        Me.shownameCheckBox.Text = "Show the image file name under the thumbnail."
        '
        'heightUpDown
        '
        Me.heightUpDown.Location = New System.Drawing.Point(439, 279)
        Me.heightUpDown.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.heightUpDown.Name = "heightUpDown"
        Me.heightUpDown.Size = New System.Drawing.Size(48, 20)
        Me.heightUpDown.TabIndex = 30
        Me.heightUpDown.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(391, 279)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 23)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Height:"
        '
        'widthUpDown
        '
        Me.widthUpDown.Location = New System.Drawing.Point(439, 255)
        Me.widthUpDown.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.widthUpDown.Name = "widthUpDown"
        Me.widthUpDown.Size = New System.Drawing.Size(48, 20)
        Me.widthUpDown.TabIndex = 28
        Me.widthUpDown.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'columnsUpDown
        '
        Me.columnsUpDown.Location = New System.Drawing.Point(167, 255)
        Me.columnsUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.columnsUpDown.Name = "columnsUpDown"
        Me.columnsUpDown.Size = New System.Drawing.Size(48, 20)
        Me.columnsUpDown.TabIndex = 25
        Me.columnsUpDown.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(7, 255)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(160, 16)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Number of columns of images:"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(295, 255)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 16)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Thumbnail size:"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Multiselect = True
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(391, 255)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 16)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Width:"
        '
        'removeButton
        '
        Me.removeButton.Location = New System.Drawing.Point(415, 87)
        Me.removeButton.Name = "removeButton"
        Me.removeButton.Size = New System.Drawing.Size(75, 23)
        Me.removeButton.TabIndex = 23
        Me.removeButton.Text = "Remove"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(368, 28)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "This wizard will build a HTML page with thumbnails for a set of images that you c" & _
            "hoose."
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(176, 16)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Thumbnail Wizard"
        '
        'addButton
        '
        Me.addButton.Location = New System.Drawing.Point(415, 119)
        Me.addButton.Name = "addButton"
        Me.addButton.Size = New System.Drawing.Size(75, 23)
        Me.addButton.TabIndex = 22
        Me.addButton.Text = "Add"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(7, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Selected Images:"
        '
        'imagesListView
        '
        Me.imagesListView.Location = New System.Drawing.Point(7, 87)
        Me.imagesListView.Name = "imagesListView"
        Me.imagesListView.Size = New System.Drawing.Size(400, 152)
        Me.imagesListView.TabIndex = 20
        Me.imagesListView.UseCompatibleStateImageBehavior = False
        Me.imagesListView.View = System.Windows.Forms.View.Details
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(-1, -1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(528, 64)
        Me.Panel1.TabIndex = 19
        '
        'finishButton
        '
        Me.finishButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.finishButton.Location = New System.Drawing.Point(407, 327)
        Me.finishButton.Name = "finishButton"
        Me.finishButton.Size = New System.Drawing.Size(75, 23)
        Me.finishButton.TabIndex = 18
        Me.finishButton.Text = "Finish"
        '
        'cancelBtn
        '
        Me.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cancelBtn.Location = New System.Drawing.Point(327, 327)
        Me.cancelBtn.Name = "cancelBtn"
        Me.cancelBtn.Size = New System.Drawing.Size(75, 23)
        Me.cancelBtn.TabIndex = 17
        Me.cancelBtn.Text = "Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-9, 303)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 8)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        '
        'WizardFrm
        '
        Me.AcceptButton = Me.finishButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cancelBtn
        Me.ClientSize = New System.Drawing.Size(494, 356)
        Me.ControlBox = False
        Me.Controls.Add(Me.shownameCheckBox)
        Me.Controls.Add(Me.heightUpDown)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.widthUpDown)
        Me.Controls.Add(Me.columnsUpDown)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.removeButton)
        Me.Controls.Add(Me.addButton)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.imagesListView)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.finishButton)
        Me.Controls.Add(Me.cancelBtn)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "WizardFrm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Thumbnail Wizard"
        CType(Me.heightUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.widthUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.columnsUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents shownameCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents heightUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents widthUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents columnsUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents removeButton As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents addButton As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents imagesListView As System.Windows.Forms.ListView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents finishButton As System.Windows.Forms.Button
    Friend WithEvents cancelBtn As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
