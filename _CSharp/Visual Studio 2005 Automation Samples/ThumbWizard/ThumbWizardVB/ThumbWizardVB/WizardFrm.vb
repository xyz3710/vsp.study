'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing

Public Class WizardFrm

    Private Sub addButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addButton.Click
        If (OpenFileDialog1.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim fileName As String
            For Each fileName In OpenFileDialog1.FileNames
                Dim newItem As System.Windows.Forms.ListViewItem
                Dim image As Image
                Try
                    image = System.Drawing.Image.FromFile(fileName)
                    newItem = imagesListView.Items.Add(fileName)
                    newItem.SubItems.Add(image.Width.ToString())
                    newItem.SubItems.Add(image.Height.ToString())
                Catch
                    System.Windows.Forms.MessageBox.Show("Could not load " & fileName & " as an image file", "Thumbnail wizard", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)
                End Try
            Next
        End If
    End Sub

    Private Sub removeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles removeButton.Click
        Dim itm As System.Windows.Forms.ListViewItem
        For Each itm In imagesListView.SelectedItems
            itm.Remove()
        Next
    End Sub

    Private Sub finishButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles finishButton.Click
        Dim fileList As New System.Collections.Specialized.StringCollection
        Dim fullOutputDir As String = Path.Combine(contextParams(3), contextParams(4))
        Dim projectItems As EnvDTE.ProjectItems
        Dim projectItemNewFolder As EnvDTE.ProjectItem
        Dim i As Integer
        projectItems = CType(contextParams(2), EnvDTE.ProjectItems)
        Directory.CreateDirectory(fullOutputDir)
        projectItemNewFolder = projectItems.AddFolder(contextParams(4), EnvDTE.Constants.vsProjectItemKindPhysicalFolder)

        Dim item As System.Windows.Forms.ListViewItem
        For Each item In imagesListView.Items
            Dim image As Image
            Dim thumbImage As Image
            image = System.Drawing.Image.FromFile(item.Text)
            thumbImage = image.GetThumbnailImage(widthUpDown.Value, heightUpDown.Value, Nothing, Nothing)
            Dim path As String = Path.GetPathRoot(item.Text)
            path = Path.Combine(fullOutputDir, Path.GetFileNameWithoutExtension(item.Text) + "_thumbnail.png")
            thumbImage.Save(path, Imaging.ImageFormat.Png)
            image.Save(Path.Combine(fullOutputDir, Path.GetFileName(item.Text)))
            fileList.Add(item.Text)
            projectItemNewFolder.ProjectItems.AddFromFile(path)
        Next

        'Build the html file with references to the images and the thumbnails:
        Dim fs As StreamWriter = Nothing
        Try
            fs = File.CreateText(Path.Combine(fullOutputDir, CType(contextParams(4), String) + ".htm"))
            fs.WriteLine("<HTML>")
            fs.WriteLine("<BODY>")
            fs.WriteLine("<TABLE cellSpacing=""1"" cellPadding=""1"" width=""300"" border=""1"">")
            fs.WriteLine("<TR>")

            For i = 0 To fileList.Count - 1
                Dim currentFile As String
                currentFile = fileList.Item(i)
                If ((i Mod columnsUpDown.Value) = 0) And (i <> 0) Then
                    fs.WriteLine("</TR>")
                    fs.WriteLine("<TR>")
                End If

                fs.WriteLine("<TD>")
                fs.WriteLine("<P align=""center"">")
                fs.WriteLine("<A href=" + Path.GetFileName(currentFile) + ">")
                fs.WriteLine("<IMG src=" + Path.GetFileNameWithoutExtension(currentFile) + "_thumbnail.png" + "/>")
                If (shownameCheckBox.Checked) Then
                    fs.WriteLine("</BR>")
                    fs.WriteLine(Path.GetFileName(currentFile))
                End If
                fs.WriteLine("</A>")
                fs.WriteLine("</P>")
                fs.WriteLine("</TD>")
            Next

            fs.WriteLine("</TR>")
            fs.WriteLine("</TABLE>")
            fs.WriteLine("</BODY>")
            fs.WriteLine("</HTML>")
        Finally
            If (fs IsNot Nothing) Then
                fs.Close()
            End If
        End Try

        projectItemNewFolder.ProjectItems.AddFromFile(Path.Combine(fullOutputDir, contextParams(4) + ".htm")).Open(EnvDTE.Constants.vsViewKindCode).Visible = True
    End Sub

    Private Sub WizardFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        imagesListView.Columns.Add("Image Path", 250, System.Windows.Forms.HorizontalAlignment.Left)
        imagesListView.Columns.Add("Width", 60, System.Windows.Forms.HorizontalAlignment.Left)
        imagesListView.Columns.Add("Height", 60, System.Windows.Forms.HorizontalAlignment.Left)
    End Sub

    Public contextParams() As Object
    Public applicationObject As EnvDTE80.DTE2
End Class