'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System.Runtime.InteropServices

<GuidAttribute("D6E7E999-FFC5-4f57-A3D2-4AA48AA5365A"), ProgIdAttribute("ThumbWizard.WizardVB")> _
Public Class ThumbnailWizard
    Implements EnvDTE.IDTWizard

    Public Sub Execute(ByVal applicationObject As Object, ByVal hwndOwner As Integer, ByRef contextParams() As Object, ByRef customParams() As Object, ByRef wizardResult As EnvDTE.wizardResult) Implements EnvDTE.IDTWizard.Execute
        If (System.String.Compare(contextParams(0), EnvDTE.Constants.vsWizardAddItem, True) = 0) Then
            Dim wrapper As New WinWrapper()
            Dim form As New WizardFrm()
            wrapper.applicationObject = applicationObject
            form.contextParams = contextParams
            form.applicationObject = applicationObject
            form.ShowDialog(wrapper)
        End If
    End Sub
End Class

Public Class WinWrapper
    Implements System.Windows.Forms.IWin32Window
    Overridable ReadOnly Property Handle() As System.IntPtr Implements System.Windows.Forms.IWin32Window.Handle
        Get
            Dim iptr As New System.IntPtr(applicationObject.MainWindow.HWnd)
            Return iptr
        End Get
    End Property

    Public applicationObject As EnvDTE.DTE
End Class