//Copyright (c) Microsoft Corporation.  All rights reserved.

namespace ThumbWizard
{
    partial class WizardFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shownameCheckBox = new System.Windows.Forms.CheckBox();
            this.heightUpDown = new System.Windows.Forms.NumericUpDown();
            this.Label7 = new System.Windows.Forms.Label();
            this.widthUpDown = new System.Windows.Forms.NumericUpDown();
            this.columnsUpDown = new System.Windows.Forms.NumericUpDown();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.OpenFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Label6 = new System.Windows.Forms.Label();
            this.removeButton = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.imagesListView = new System.Windows.Forms.ListView();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.finishButton = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.heightUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnsUpDown)).BeginInit();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // shownameCheckBox
            // 
            this.shownameCheckBox.Location = new System.Drawing.Point(7, 287);
            this.shownameCheckBox.Name = "shownameCheckBox";
            this.shownameCheckBox.Size = new System.Drawing.Size(264, 16);
            this.shownameCheckBox.TabIndex = 31;
            this.shownameCheckBox.Text = "Show the image file name under the thumbnail.";
            // 
            // heightUpDown
            // 
            this.heightUpDown.Location = new System.Drawing.Point(439, 279);
            this.heightUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.heightUpDown.Name = "heightUpDown";
            this.heightUpDown.Size = new System.Drawing.Size(48, 20);
            this.heightUpDown.TabIndex = 30;
            this.heightUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(391, 279);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(48, 23);
            this.Label7.TabIndex = 29;
            this.Label7.Text = "Height:";
            // 
            // widthUpDown
            // 
            this.widthUpDown.Location = new System.Drawing.Point(439, 255);
            this.widthUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.widthUpDown.Name = "widthUpDown";
            this.widthUpDown.Size = new System.Drawing.Size(48, 20);
            this.widthUpDown.TabIndex = 28;
            this.widthUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // columnsUpDown
            // 
            this.columnsUpDown.Location = new System.Drawing.Point(167, 255);
            this.columnsUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.columnsUpDown.Name = "columnsUpDown";
            this.columnsUpDown.Size = new System.Drawing.Size(48, 20);
            this.columnsUpDown.TabIndex = 25;
            this.columnsUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(7, 255);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(160, 16);
            this.Label4.TabIndex = 24;
            this.Label4.Text = "Number of columns of images:";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(295, 255);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(88, 16);
            this.Label5.TabIndex = 26;
            this.Label5.Text = "Thumbnail size:";
            // 
            // OpenFileDialog1
            // 
            this.OpenFileDialog1.Multiselect = true;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(391, 255);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(40, 16);
            this.Label6.TabIndex = 27;
            this.Label6.Text = "Width:";
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(415, 87);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 23;
            this.removeButton.Text = "Remove";
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // Label2
            // 
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(24, 24);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(368, 28);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "This wizard will build a HTML page with thumbnails for a set of images that you c" +
                "hoose.";
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(8, 8);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(176, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Thumbnail Wizard";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(415, 119);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 22;
            this.addButton.Text = "Add";
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(7, 71);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 16);
            this.Label3.TabIndex = 21;
            this.Label3.Text = "Selected Images:";
            // 
            // imagesListView
            // 
            this.imagesListView.Location = new System.Drawing.Point(7, 87);
            this.imagesListView.Name = "imagesListView";
            this.imagesListView.Size = new System.Drawing.Size(400, 152);
            this.imagesListView.TabIndex = 20;
            this.imagesListView.UseCompatibleStateImageBehavior = false;
            this.imagesListView.View = System.Windows.Forms.View.Details;
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.White;
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Location = new System.Drawing.Point(-1, -1);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(528, 64);
            this.Panel1.TabIndex = 19;
            // 
            // finishButton
            // 
            this.finishButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.finishButton.Location = new System.Drawing.Point(407, 327);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(75, 23);
            this.finishButton.TabIndex = 18;
            this.finishButton.Text = "Finish";
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(327, 327);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 17;
            this.cancelBtn.Text = "Cancel";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Location = new System.Drawing.Point(-9, 303);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(520, 8);
            this.GroupBox1.TabIndex = 16;
            this.GroupBox1.TabStop = false;
            // 
            // WizardFrm
            // 
            this.AcceptButton = this.finishButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelBtn;
            this.ClientSize = new System.Drawing.Size(494, 356);
            this.ControlBox = false;
            this.Controls.Add(this.shownameCheckBox);
            this.Controls.Add(this.heightUpDown);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.widthUpDown);
            this.Controls.Add(this.columnsUpDown);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.imagesListView);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.finishButton);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.GroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WizardFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thumbnail Wizard";
            this.Load += new System.EventHandler(this.WizardFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.heightUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnsUpDown)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox shownameCheckBox;
        internal System.Windows.Forms.NumericUpDown heightUpDown;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.NumericUpDown widthUpDown;
        internal System.Windows.Forms.NumericUpDown columnsUpDown;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.OpenFileDialog OpenFileDialog1;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button removeButton;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button addButton;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.ListView imagesListView;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button finishButton;
        internal System.Windows.Forms.Button cancelBtn;
        internal System.Windows.Forms.GroupBox GroupBox1;
    }
}