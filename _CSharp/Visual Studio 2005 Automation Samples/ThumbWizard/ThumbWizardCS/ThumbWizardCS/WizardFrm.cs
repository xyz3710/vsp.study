//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ThumbWizard
{
    public partial class WizardFrm : Form
    {
        public WizardFrm()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (OpenFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                foreach (string fileName in OpenFileDialog1.FileNames)
                {
                    ListViewItem newItem;
                    Image image;
                    try
                    {
                        image = Image.FromFile(fileName);
                        newItem = imagesListView.Items.Add(fileName);
                        newItem.SubItems.Add(image.Width.ToString());
                        newItem.SubItems.Add(image.Height.ToString());
                    }
                    catch
                    {
                        MessageBox.Show(String.Format("Could not load {0} as an image file", fileName), "Thumbnail wizard", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in imagesListView.SelectedItems)
            {
                itm.Remove();
            }
        }

        private void WizardFrm_Load(object sender, EventArgs e)
        {
            imagesListView.Columns.Add("Image Path", 250, HorizontalAlignment.Left);
            imagesListView.Columns.Add("Width", 60, HorizontalAlignment.Left);
            imagesListView.Columns.Add("Height", 60, HorizontalAlignment.Left);
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            List<string> fileList = new List<string>();
            string fullOutputDir = Path.Combine(contextParams[3], contextParams[4]);
            EnvDTE.ProjectItems projectItems;
            EnvDTE.ProjectItem projectItemNewFolder;
            int i;
            projectItems = (EnvDTE.ProjectItems)contextParams[2];
            Directory.CreateDirectory(fullOutputDir);
            projectItemNewFolder = projectItems.AddFolder((string)contextParams[4], EnvDTE.Constants.vsProjectItemKindPhysicalFolder);

            foreach (ListViewItem item in imagesListView.Items)
            {
                Image image = Image.FromFile(item.Text);
                Image thumbImage = image.GetThumbnailImage((int)widthUpDown.Value, (int)heightUpDown.Value, null, IntPtr.Zero);
                string path = Path.Combine(fullOutputDir, Path.GetFileNameWithoutExtension(item.Text));
                path = path + "_thumbnail.png";
                thumbImage.Save(path, System.Drawing.Imaging.ImageFormat.Png);
                image.Save(Path.Combine(fullOutputDir, Path.GetFileName(item.Text)));
                fileList.Add(item.Text);
                projectItemNewFolder.ProjectItems.AddFromFile(path);
            }

            //Build the html file with references to the images and the thumbnails:
            StreamWriter fs = null;
            try
            {
                fs = File.CreateText(Path.Combine(fullOutputDir, (string)contextParams[4] + ".htm"));
                fs.WriteLine("<HTML>");
                fs.WriteLine("<BODY>");
                fs.WriteLine("<TABLE cellSpacing=\"1\" cellPadding=\"1\" width=\"300\" border=\"1\">");
                fs.WriteLine("<TR>");

                i = 0;
                foreach (string file in fileList)
                {
                    string currentFile = fileList[i];
                    if (((i % columnsUpDown.Value) == 0) && (i != 0))
                    {
                        fs.WriteLine("</TR>");
                        fs.WriteLine("<TR>");
                    }

                    fs.WriteLine("<TD>");
                    fs.WriteLine("<P align=\"center\">");
                    fs.WriteLine("<A href=" + Path.GetFileName(currentFile) + ">");
                    fs.WriteLine("<IMG src=" + Path.GetFileNameWithoutExtension(currentFile) + "_thumbnail.png" + "/>");
                    if (shownameCheckBox.Checked)
                    {
                        fs.WriteLine("</BR>");
                        fs.WriteLine(Path.GetFileName(currentFile));
                    }
                    fs.WriteLine("</A>");
                    fs.WriteLine("</P>");
                    fs.WriteLine("</TD>");
                }

                fs.WriteLine("</TR>");
                fs.WriteLine("</TABLE>");
                fs.WriteLine("</BODY>");
                fs.WriteLine("</HTML>");
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }

            projectItemNewFolder.ProjectItems.AddFromFile(Path.Combine(fullOutputDir, contextParams[4] + ".htm")).Open(EnvDTE.Constants.vsViewKindCode).Visible = true;

        }

        internal object[] contextParams;
        internal EnvDTE80.DTE2 applicationObject;
    }
}