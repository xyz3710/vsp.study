//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace ThumbWizard
{
    [GuidAttribute("408A375B-2AFC-49ae-9AC2-FB0A100A20B4"), ProgIdAttribute("ThumbWizard.WizardCS")]
    public class Wizard : EnvDTE.IDTWizard
    {
        #region IDTWizard Members

        public void Execute(object Application, int hwndOwner, ref object[] ContextParams, ref object[] CustomParams, ref EnvDTE.wizardResult retval)
        {
            if (String.Compare((string)ContextParams[0], EnvDTE.Constants.vsWizardAddItem, true) == 0)
            {
                WinWrapper wrapper = new WinWrapper();
                WizardFrm form = new WizardFrm();
                wrapper.applicationObject = (EnvDTE80.DTE2)Application;
                form.contextParams = ContextParams;
                form.applicationObject = (EnvDTE80.DTE2)Application;
                form.ShowDialog(wrapper);
            }
        }

        #endregion
    }

    public class WinWrapper : System.Windows.Forms.IWin32Window
    {
        public EnvDTE80.DTE2 applicationObject;

        #region IWin32Window Members

        public IntPtr Handle
        {
            get
            {
                IntPtr iptr = new IntPtr(applicationObject.MainWindow.HWnd);
                return iptr;
            }
        }

        #endregion
    }
}
