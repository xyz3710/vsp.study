//Copyright (c) Microsoft Corporation.  All rights reserved.

// LoadFilesDlg.cpp : Implementation of CLoadFilesDlg

#include "stdafx.h"
#include "LoadFilesDlg.h"


// CLoadFilesDlg

HKEY GetKeyForReadingScriptFiles()
{
	HKEY hKeyFiles = NULL;
	if(RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\VisualStudio\\8.0\\AddIns\\ScriptHost.Connect\\Scripts"), 0, KEY_READ, &hKeyFiles) == ERROR_SUCCESS)
	{
		return hKeyFiles;
	}
	return NULL;
}

void CreateScriptKey()
{
	HKEY hKey;
	if(RegCreateKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\VisualStudio\\8.0\\AddIns\\ScriptHost.Connect\\Scripts"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_READ, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
	}
}

void SetScriptLoadOption(TCHAR *pszFileName, BOOL load)
{
	HKEY hKeyFiles;
	if(RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\VisualStudio\\8.0\\AddIns\\ScriptHost.Connect\\Scripts"), 0, KEY_READ|KEY_WRITE, &hKeyFiles) == ERROR_SUCCESS)
	{
		RegSetValueEx(hKeyFiles, pszFileName, 0, REG_SZ, (load) ? (LPBYTE)_T("1") : (LPBYTE)_T("0"), sizeof(TCHAR)*2);
		RegCloseKey(hKeyFiles);
	}
}

LRESULT CLoadFilesDlg::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CAxDialogImpl<CLoadFilesDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
	HKEY hKeyFiles;
	LV_COLUMN lvcol;
	lvcol.mask = LVCF_TEXT|LVCF_WIDTH;
	lvcol.cx = 500;
	lvcol.pszText = _T("File");

	CAxDialogImpl<CLoadFilesDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
	ListView_InsertColumn(GetDlgItem(IDC_FILELIST), 1, &lvcol);
	ListView_SetExtendedListViewStyleEx(GetDlgItem(IDC_FILELIST), LVS_EX_CHECKBOXES, LVS_EX_CHECKBOXES);

	if(RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\VisualStudio\\8.0\\AddIns\\ScriptHost.Connect\\Scripts"), 0, KEY_READ, &hKeyFiles) == ERROR_SUCCESS)
	{
		DWORD dwIndex = 0;
		DWORD dwSizeValueName = MAX_PATH;
		TCHAR szValueName[MAX_PATH];
		DWORD dwType;
		TCHAR szData[MAX_PATH];
		DWORD dwSizeData = MAX_PATH;
		while(RegEnumValue(hKeyFiles, dwIndex, szValueName, &dwSizeValueName, 0, &dwType, (LPBYTE)szData, &dwSizeData) == ERROR_SUCCESS)
		{
			LVITEM lvitem;
			lvitem.mask = LVIF_TEXT;
			lvitem.iItem = 0;
			lvitem.iSubItem = 0;
			lvitem.pszText = szValueName;
			int nItem = ListView_InsertItem(GetDlgItem(IDC_FILELIST), &lvitem);
			if(!_tcscmp(szData, _T("1")))
			{
				ListView_SetCheckState(GetDlgItem(IDC_FILELIST), nItem, TRUE);
			}
			else
			{
				ListView_SetCheckState(GetDlgItem(IDC_FILELIST), nItem, FALSE);
			}
			dwIndex++;
			dwSizeValueName = MAX_PATH;
			dwSizeData = MAX_PATH;
		}
		RegCloseKey(hKeyFiles);
	}
	bHandled = TRUE;
	return 1;  // Let the system set the focus
}

LRESULT CLoadFilesDlg::OnBnClickedBrowse(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	TCHAR szFileName[MAX_PATH];
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	TCHAR	szBuf[MAX_PATH];
	
	szBuf[0] = '\0';
	szFileName[0] = 0;
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = m_hWnd;
	ofn.lpstrFilter = _T("Developer Studio 6 Macro Files (*.dsm)\0");
	ofn.lpstrDefExt = _T("dsm");
	ofn.lpstrFileTitle = szBuf;
	ofn.nMaxFileTitle	= MAX_PATH;
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY  | OFN_ALLOWMULTISELECT | OFN_EXPLORER;

	if(GetOpenFileName(&ofn) == IDOK)
	{
		HKEY hKeyFiles;
		CreateScriptKey();
		if(RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\VisualStudio\\8.0\\AddIns\\ScriptHost.Connect\\Scripts"), 0, KEY_READ|KEY_WRITE, &hKeyFiles) == ERROR_SUCCESS)
		{
			if(RegQueryValueEx(hKeyFiles, szFileName, NULL, NULL, NULL, NULL) != ERROR_SUCCESS)
			{
				DWORD dwVal = 1;
				DWORD dwIndex = 0;
				DWORD dwSizeValueName = MAX_PATH;
				TCHAR szValueName[MAX_PATH];
				DWORD dwType;
				TCHAR szData[MAX_PATH];
				DWORD dwSizeData = MAX_PATH;

				RegSetValueEx(hKeyFiles, szFileName, 0, REG_SZ, (LPBYTE)_T("1"), sizeof(TCHAR)*2);
				
				ListView_DeleteAllItems(GetDlgItem(IDC_FILELIST));
				
				while(RegEnumValue(hKeyFiles, dwIndex, szValueName, &dwSizeValueName, 0, &dwType, (LPBYTE)szData, &dwSizeData) == ERROR_SUCCESS)
				{
					LVITEM lvitem;
					lvitem.mask = LVIF_TEXT;
					lvitem.iItem = 0;
					lvitem.iSubItem = 0;
					lvitem.pszText = szValueName;
					int nItem = ListView_InsertItem(GetDlgItem(IDC_FILELIST), &lvitem);
					if(!_tcscmp(szData, _T("1")))
					{
						ListView_SetCheckState(GetDlgItem(IDC_FILELIST), nItem, TRUE);
					}
					else
					{
						ListView_SetCheckState(GetDlgItem(IDC_FILELIST), nItem, FALSE);
					}
					dwIndex++;
					dwSizeValueName = MAX_PATH;
					dwSizeData = MAX_PATH;
				}
			}
			RegCloseKey(hKeyFiles);
		}
	}
	CommDlgExtendedError();
	return 0;
}
