//Copyright (c) Microsoft Corporation.  All rights reserved.

// ScriptDlg.cpp : Implementation of CScriptDlg

#include "stdafx.h"
#include "ScriptDlg.h"
#include "LoadFilesDlg.h"

extern HRESULT LoadEngine();
extern IActiveScript *GetScriptPtr();

//Display the dialog box to ask the user which macro to run.
int ShowScriptDlg(IActiveScript *pScript, CComBSTR &bstrMacroToRun)
{
	CScriptDlg ScriptDlg;
	ScriptDlg.m_pScript = pScript;
	int retVal = ScriptDlg.DoModal();
	if(retVal == IDC_RUN)
	{
		bstrMacroToRun = ScriptDlg.bstrMacroToRun;
	}
	return retVal;
}

// CScriptDlg

//Enumerate the information returned from the ITypeInfo to determine
//  the names of the subs in the macro file. Then add these names to
//  the list of items.
HRESULT CScriptDlg::PopulateNames()
{
	HRESULT hr = S_OK;
	CComPtr<IDispatch> pScriptDispatch;
	CComPtr<IDispatchEx> pScriptDispatchEx;
	CComPtr<ITypeInfo> pTypeInfo;
	UINT uiTypeInfoCount;
	TYPEATTR *pTypeAttr;
	TYPEKIND tkindCur = TKIND_MAX;
	POSITION pos;

	abstrMacroNames.RemoveAll();

	IfFailGo(m_pScript->GetScriptDispatch(NULL, &pScriptDispatch));
	pScriptDispatchEx = pScriptDispatch;
	pScriptDispatch->GetTypeInfo(0, 0, &pTypeInfo);

	pScriptDispatch->GetTypeInfoCount(&uiTypeInfoCount);

	for (UINT uiTypeIndex = 0 ;  uiTypeIndex < uiTypeInfoCount ; uiTypeIndex++)
	{
		CComPtr<ITypeInfo> ptiCur;
		IfFailGo(pTypeInfo->GetTypeAttr(&pTypeAttr));

		for (UINT uiFuncIndex = 0; uiFuncIndex < pTypeAttr->cFuncs; uiFuncIndex++)
		{
			CComBSTR bstrName;
			FUNCDESC *pfuncdesc = NULL;
			IfFailGo(pTypeInfo->GetFuncDesc(uiFuncIndex, &pfuncdesc));
			if((pfuncdesc->invkind == INVOKE_FUNC) && (pfuncdesc->cParams == 0 ) && (pfuncdesc->cParamsOpt == 0))
			{
				pScriptDispatchEx->GetMemberName(pfuncdesc->memid, &bstrName);
				WCHAR *pszTemp = new WCHAR[bstrName.Length() + 1];
				wcscpy(pszTemp, bstrName);
				abstrMacroNames.AddHead(pszTemp);
			}
			pTypeInfo->ReleaseFuncDesc(pfuncdesc);
		}
		pTypeInfo->ReleaseTypeAttr(pTypeAttr);
	}
	pos = abstrMacroNames.GetHeadPosition();
	while(pos)
	{
		USES_CONVERSION;
		TCHAR *pszText = W2T(abstrMacroNames.GetNext(pos));
		SendDlgItemMessage(IDC_SCRIPTSLIST, LB_ADDSTRING, -1, (LPARAM)pszText);
	}
Error:
	return hr;
}

//Find the selected macro, and start running it.
LRESULT CScriptDlg::OnBnClickedRun(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	TCHAR szName[200];	//200 should give us plenty of room for the macro name
	LPARAM sel = SendDlgItemMessage(IDC_SCRIPTSLIST, LB_GETCURSEL);
	SendDlgItemMessage(IDC_SCRIPTSLIST, LB_GETTEXT, (WPARAM)sel, (LPARAM)&szName);
	bstrMacroToRun = szName;
	EndDialog(IDC_RUN);
	return 0;
}

//Add a file to the list of files read and allowed to run.
LRESULT CScriptDlg::OnBnClickedFiles(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CLoadFilesDlg LoadFilesDlg;
	LoadFilesDlg.DoModal();
	SendDlgItemMessage(IDC_SCRIPTSLIST, LB_RESETCONTENT);
	LoadEngine();
	m_pScript = GetScriptPtr();
	PopulateNames();
	return 0;
}