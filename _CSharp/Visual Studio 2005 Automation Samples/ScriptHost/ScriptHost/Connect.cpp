//Copyright (c) Microsoft Corporation.  All rights reserved.

// Connect.cpp : Implementation of CConnect
#include "stdafx.h"
#include "AddIn.h"
#include "Connect.h"

extern CAddInModule _AtlModule;

CConnect* CConnect::pThis = NULL;

HRESULT LoadEngine()
{
	return CConnect::pThis->LoadEngine();
}

IActiveScript *GetScriptPtr()
{
	return CConnect::pThis->GetScriptPtr();
}

// CConnect
STDMETHODIMP CConnect::OnConnection(IDispatch *pApplication, ext_ConnectMode ConnectMode, IDispatch *pAddInInst, SAFEARRAY ** /*custom*/ )
{
	HRESULT hr = S_OK;
	pApplication->QueryInterface(__uuidof(DTE2), (LPVOID*)&m_pDTE);
	pAddInInst->QueryInterface(__uuidof(AddIn), (LPVOID*)&m_pAddInInstance);
	if(ConnectMode == 5) //5 == ext_cm_UISetup
	{
		HRESULT hr = S_OK;
		CComPtr<IDispatch> pDisp;
		CComQIPtr<Commands> pCommands;
		CComQIPtr<Commands2> pCommands2;
		CComQIPtr<_CommandBars> pCommandBars;
		CComPtr<CommandBarControl> pCommandBarControl;
		CComPtr<Command> pCreatedCommand;
		CComPtr<CommandBar> pMenuBarCommandBar;
		CComPtr<CommandBarControls> pMenuBarControls;
		CComPtr<CommandBarControl> pToolsCommandBarControl;
		CComQIPtr<CommandBar> pToolsCommandBar;
		CComQIPtr<CommandBarPopup> pToolsPopup;

		IfFailGoCheck(m_pDTE->get_Commands(&pCommands), pCommands);
		pCommands2 = pCommands;
		if(SUCCEEDED(pCommands2->AddNamedCommand2(m_pAddInInstance, CComBSTR("ScriptHost"), CComBSTR("ScriptHost"), CComBSTR("Executes the command for ScriptHost"), VARIANT_TRUE, CComVariant(59), NULL, vsCommandStatusSupported+vsCommandStatusEnabled, vsCommandStylePictAndText, vsCommandControlTypeButton, &pCreatedCommand)) && (pCreatedCommand))
		{
			//Add a button to the tools menu bar.
			IfFailGoCheck(m_pDTE->get_CommandBars(&pDisp), pDisp);
			pCommandBars = pDisp;

			//Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
			IfFailGoCheck(pCommandBars->get_Item(CComVariant(L"MenuBar"), &pMenuBarCommandBar), pMenuBarCommandBar);
			IfFailGoCheck(pMenuBarCommandBar->get_Controls(&pMenuBarControls), pMenuBarControls);

			//Find the Tools command bar on the MenuBar command bar:
			IfFailGoCheck(pMenuBarControls->get_Item(CComVariant(L"Tools"), &pToolsCommandBarControl), pToolsCommandBarControl);
			pToolsPopup = pToolsCommandBarControl;
			IfFailGoCheck(pToolsPopup->get_CommandBar(&pToolsCommandBar), pToolsCommandBar);

			//Add the control:
			pDisp = NULL;
			IfFailGoCheck(pCreatedCommand->AddControl(pToolsCommandBar, 1, &pDisp), pDisp);
		}
		return S_OK;
	}
Error:
	return hr;
}

STDMETHODIMP CConnect::OnDisconnection(ext_DisconnectMode /*RemoveMode*/, SAFEARRAY ** /*custom*/ )
{
	m_pDTE = NULL;
	m_pAddInInstance = NULL;
	return S_OK;
}

STDMETHODIMP CConnect::OnAddInsUpdate (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnStartupComplete (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnBeginShutdown (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::QueryStatus(BSTR bstrCmdName, vsCommandStatusTextWanted NeededText, vsCommandStatus *pStatusOption, VARIANT *pvarCommandText)
{
	if(NeededText == vsCommandStatusTextWantedNone)
	{
		if(!_wcsicmp(bstrCmdName, L"ScriptHost.Connect.ScriptHost"))
		{
			*pStatusOption = (vsCommandStatus)(vsCommandStatusEnabled+vsCommandStatusSupported);
		}
	}
	return S_OK;
}

int ShowScriptDlg(IActiveScript *pScript, CComBSTR &bstrMacroToRun);

STDMETHODIMP CConnect::Exec(BSTR bstrCmdName, vsCommandExecOption ExecuteOption, VARIANT * /*pvarVariantIn*/, VARIANT * /*pvarVariantOut*/, VARIANT_BOOL *pvbHandled)
{
	*pvbHandled = VARIANT_FALSE;
	if(ExecuteOption == vsCommandExecOptionDoDefault)
	{
		if(!_wcsicmp(bstrCmdName, L"ScriptHost.Connect.ScriptHost"))
		{
			HRESULT hr = S_OK;
			USES_CONVERSION;
			CHAR *pszChar = NULL;
			DISPID currDispid = -1;
			CComVariant val;
			CComBSTR bstrMacroToRun;

			LoadEngine();

			int retVal = ShowScriptDlg(m_pScript, bstrMacroToRun);

			if(retVal == IDC_RUN)
			{
				CComPtr<IDispatch> pScriptDispatch;
				DISPID dispid;
				CComVariant varRetVal;
				DISPPARAMS dispParams = {NULL, NULL, 0, 0}; 
				EXCEPINFO einfo;
				LPOLESTR newstr = bstrMacroToRun;
				IfFailGo(m_pScript->GetScriptDispatch(NULL, &pScriptDispatch));
				IfFailGo(pScriptDispatch->GetIDsOfNames(IID_NULL, &newstr, 1, LOCALE_SYSTEM_DEFAULT, &dispid));
				IfFailGo(pScriptDispatch->Invoke(dispid, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD|DISPATCH_PROPERTYGET, &dispParams, &varRetVal, &einfo, NULL));
			}
Error:
			m_pScript->SetScriptState(SCRIPTSTATE_DISCONNECTED);
			m_pScript->SetScriptState(SCRIPTSTATE_CLOSED);
			m_pScript = NULL;
			m_pParse = NULL;

			*pvbHandled = VARIANT_TRUE;
			return S_OK;
		}
	}
	return S_OK;
}

HRESULT CConnect::LoadEngine()
{
	HKEY hKeyFiles;
	HRESULT hr = S_OK;
	HANDLE hFile;
	DWORD dwSize;
	USES_CONVERSION;
	EXCEPINFO einfo;
	CHAR *pszChar = NULL;
	DWORD dwBytesRead;
	DISPID currDispid = -1;
	CLSID CLSID_VBScript;
	CComVariant val;

	if(m_pScript.p && m_pParse.p)
	{
		m_pScript->SetScriptState(SCRIPTSTATE_DISCONNECTED);
		m_pScript->SetScriptState(SCRIPTSTATE_CLOSED);
		m_pScript = NULL;
		m_pParse = NULL;
	}

	//If you wish to turn this Add-in into a JScript macro engine (or any other script language), change "VBScript" below to the correct moniker.
	IfFailGo(CLSIDFromProgID(L"VBScript", &CLSID_VBScript));
	IfFailGo(CoCreateInstance(CLSID_VBScript, NULL, CLSCTX_INPROC_SERVER, IID_IActiveScript, (LPVOID*)&m_pScript));
	IfFailGo(m_pScript->QueryInterface(IID_IActiveScriptParse, (LPVOID*)&m_pParse));
	IfFailGo(m_pScript->SetScriptSite((IActiveScriptSite*)this));

	IfFailGo(m_pParse->InitNew());
	IfFailGo(m_pScript->AddTypeLib(EnvDTE::LIBID_EnvDTE, 8, 0, 0));
	IfFailGo(m_pScript->AddNamedItem(OLESTR("Application"), SCRIPTITEM_ISVISIBLE | SCRIPTITEM_ISSOURCE | SCRIPTITEM_GLOBALMEMBERS));
	IfFailGo(m_pScript->AddNamedItem(OLESTR("DTE"), SCRIPTITEM_ISVISIBLE | SCRIPTITEM_ISSOURCE | SCRIPTITEM_GLOBALMEMBERS));

	extern HKEY GetKeyForReadingScriptFiles();
	hKeyFiles = GetKeyForReadingScriptFiles();
	if(hKeyFiles != NULL)
	{
		DWORD dwIndex = 0;
		DWORD dwSizeValueName = MAX_PATH;
		DWORD dwType;
		TCHAR szData[MAX_PATH];
		DWORD dwSizeData = MAX_PATH;
		while(RegEnumValue(hKeyFiles, dwIndex, m_szCurrentParseFile, &dwSizeValueName, 0, &dwType, (LPBYTE)szData, &dwSizeData) == ERROR_SUCCESS)
		{
			if(_ttoi(szData) == 1)
			{
				hFile = CreateFile(m_szCurrentParseFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
				dwSize = GetFileSize(hFile, NULL);
				if (dwSize == 0xffffffff)
				{
					IfFailGo(E_FAIL);
				}
				pszChar = new CHAR[dwSize+1];
				memset(pszChar, 0, dwSize);
				ReadFile(hFile, (LPVOID)pszChar, dwSize, &dwBytesRead, NULL);
				CloseHandle(hFile);
				pszChar[dwSize] = 0;

				IfFailGo(m_pParse->ParseScriptText(A2W(pszChar), OLESTR(""), NULL, NULL, 0, 0, SCRIPTTEXT_ISVISIBLE|SCRIPTTEXT_ISPERSISTENT, &val, &einfo));
				if(pszChar)
					delete []pszChar;
			}
			dwIndex++;
			dwSizeValueName = MAX_PATH;
			dwSizeData = MAX_PATH;
		}
		RegCloseKey(hKeyFiles);
	}
	else
	{
		extern void CreateScriptKey();
		//We have no files available, need to create the Key
		CreateScriptKey();
	}
	IfFailGo(m_pScript->SetScriptState(SCRIPTSTATE_STARTED));
	IfFailGo(m_pScript->SetScriptState(SCRIPTSTATE_CONNECTED));
Error:
	return hr;
}


// IActiveScriptSite
STDMETHODIMP CConnect::GetLCID(LCID* plcid)
{
	return E_NOTIMPL;
}

STDMETHODIMP CConnect::GetItemInfo(LPCOLESTR pstrName, DWORD dwReturnMask, IUnknown** ppunkItem, ITypeInfo** ppTypeInfo)
{
	CComPtr<IDispatch> pDisp;
	HRESULT hr = S_OK;
	CComPtr<ITypeLib> pTypeInfoLib;
	CComPtr<ITypeInfo> pClassTypeInfo;
	CComPtr<ITypeInfo> pti;

	CComBSTR bstrItem(pstrName);

	if (dwReturnMask & SCRIPTINFO_IUNKNOWN)
	{
		if (!ppunkItem)
			return E_INVALIDARG;

		// Return our IUnknown...
		hr = m_pDTE->QueryInterface(IID_IUnknown, (LPVOID*)ppunkItem);
		return S_OK;
	}

	if (dwReturnMask & SCRIPTINFO_ITYPEINFO)
	{
		hr = m_pDTE->GetTypeInfo(0, 0, ppTypeInfo);
	}

	return hr;
}

STDMETHODIMP CConnect::GetDocVersionString(BSTR* pstrVersionString)
{
	return E_NOTIMPL;
}

STDMETHODIMP CConnect::OnScriptTerminate(const VARIANT* pvarResult, const EXCEPINFO* pexcepinfo)
{
	return S_OK;
}

STDMETHODIMP CConnect::OnStateChange(SCRIPTSTATE ssScriptState)
{
	return S_OK;
}

extern void SetScriptLoadOption(TCHAR *pszFileName, BOOL load);
STDMETHODIMP CConnect::OnScriptError(IActiveScriptError* pActiveScriptError)
{
	if (pActiveScriptError)
	{
		USES_CONVERSION;
		DWORD dwCookie = 0;
		ULONG ulLineNo = 0;
		LONG  lCharNo = 0;
		EXCEPINFO ExcepInfo;
		CComBSTR SourceLine;
		CComBSTR bstrError;

		if (SUCCEEDED(pActiveScriptError->GetExceptionInfo(&ExcepInfo)))
		{
			if (ExcepInfo.bstrDescription)
			{
				bstrError += ExcepInfo.bstrDescription;
				bstrError += CComBSTR(_T("\r\n"));
			}
		}
		bstrError += CComBSTR(m_szCurrentParseFile);
		bstrError += CComBSTR(_T("\r\n"));
		if (SUCCEEDED(pActiveScriptError->GetSourcePosition(&dwCookie, &ulLineNo, &lCharNo)))
		{
			TCHAR szTemp[500];
			wsprintf(szTemp, _T("Line %ld. Column %ld\r\n"), ulLineNo, lCharNo);
			bstrError += CComBSTR(szTemp);
			bstrError += CComBSTR(_T("\r\n"));
		}
		if (SUCCEEDED(pActiveScriptError->GetSourceLineText(&SourceLine)))
		{
			WCHAR *pszTemp;
			WCHAR *szLine = new WCHAR[SourceLine.Length() + 1];
			wcscpy(szLine, SourceLine);
			pszTemp = szLine;
			while(*pszTemp)	//Get rid of any leading whitespace...
			{
				if((*pszTemp == '\n') || (*pszTemp == '\r') || (*pszTemp == ' ') || (*pszTemp == '\t'))
					pszTemp = CharNextW(pszTemp);
				else
					break;
			}
			bstrError += CComBSTR(pszTemp);
			delete []szLine;
		}

		if (!(!bstrError))
		{
			CComPtr<EnvDTE::Window> pMainWindow;
			HWND hWndParent = NULL;
			if(SUCCEEDED(m_pDTE->get_MainWindow(&pMainWindow)))
			{
				if(FAILED(pMainWindow->get_HWnd((long*)hWndParent)))
				{
					hWndParent = NULL;
				}
			}

			bstrError += CComBSTR(_T("\r\n\r\nDo you wish to try and reload this script the next time?"));
			if(MessageBox(hWndParent, W2T(bstrError), W2T(ExcepInfo.bstrSource), MB_YESNO|MB_ICONEXCLAMATION) == IDNO)
			{
				SetScriptLoadOption(m_szCurrentParseFile, FALSE);
			}
		}
	}
	return S_OK;
}

STDMETHODIMP CConnect::OnEnterScript(void)
{
	return S_OK;
}

STDMETHODIMP CConnect::OnLeaveScript(void)
{
	return S_OK;
}

// IActiveScriptSiteWindow
STDMETHODIMP CConnect::GetWindow(HWND* phWnd)
{
	HRESULT hr = S_OK;
	CComPtr<EnvDTE::Window> pMainWindow;
	IfFailGo(m_pDTE->get_MainWindow(&pMainWindow));
	IfFailGo(pMainWindow->get_HWnd((long*)phWnd));
Error:
	if(FAILED(hr))
		*phWnd = NULL;
	return S_OK;
}

STDMETHODIMP CConnect::EnableModeless(BOOL fEnable)
{
	//Don't want to do this because we don't actually have a window
	return S_OK;
}