//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AddIn.rc
//
#define IDS_PROJNAME                    100
#define IDR_ADDIN                       101
#define IDR_CONNECT                     102
#define IDD_LOADFILESDLG                103
#define IDD_SCRIPTDLG                   104
#define IDC_SCRIPTSLIST                 106
#define IDC_FILES                       107
#define IDC_RUN                         108
#define IDC_FILELIST                    110
#define IDC_BROWSE                      111

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
