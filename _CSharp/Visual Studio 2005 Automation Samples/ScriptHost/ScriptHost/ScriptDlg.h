//Copyright (c) Microsoft Corporation.  All rights reserved.

// ScriptDlg.h : Declaration of the CScriptDlg

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>
#include <atlcoll.h>

// CScriptDlg

class CScriptDlg : 
	public CAxDialogImpl<CScriptDlg>
{
public:
	CScriptDlg()
	{
	}

	~CScriptDlg()
	{
		POSITION pos = abstrMacroNames.GetHeadPosition();
		while(pos)
		{
			WCHAR *pszText = abstrMacroNames.GetNext(pos);
			delete []pszText;
		}
	}

	enum { IDD = IDD_SCRIPTDLG };

BEGIN_MSG_MAP(CScriptDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	COMMAND_HANDLER(IDC_RUN, BN_CLICKED, OnBnClickedRun)
	COMMAND_HANDLER(IDC_FILES, BN_CLICKED, OnBnClickedFiles)
	CHAIN_MSG_MAP(CAxDialogImpl<CScriptDlg>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CAxDialogImpl<CScriptDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		PopulateNames();
		CenterWindow();
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnBnClickedRun(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);

	LRESULT OnBnClickedFiles(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);

public:
	HRESULT PopulateNames();
	CComPtr<IActiveScript> m_pScript;
	CComBSTR bstrMacroToRun;
	CAtlList<WCHAR*> abstrMacroNames;
	//CConnect *m_pConnect;
};


