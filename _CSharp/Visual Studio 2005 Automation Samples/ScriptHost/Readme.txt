Copyright (c) Microsoft Corporation.  All rights reserved.

ScriptHost (written in VC++) hosts the VBScript engine, and allows you to run VBS macros from VC6 in the VS .NET 2005 IDE. 
You do not need to make any syntatic changes to your code except for method/property calls that are not available in VS .NET 2005


Once you load this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin ScriptHost.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)
