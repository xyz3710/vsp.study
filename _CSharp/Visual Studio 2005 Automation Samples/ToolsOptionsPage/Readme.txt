Copyright (c) Microsoft Corporation.  All rights reserved.

This sample shows how to create a custom options page that will be displayed in the Tools->Options dialog and how to interact with that page. It demonstrates the implementation of EnvDTE.IDTToolsOptionsPage interface.

If you load the CS, VB or JS version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin ToolsOptionsPage<language>.Connect (where <language> is CS, VB or JS depending on which version of the sample you loaded)
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)



If you load the CPPCLR version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Linker | General and change the Output File property to <My Documents>\Visual Studio 2005\Addins\$(ProjectName).dll (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)

3. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin ToolsOptionsPageCPPCLR.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

After building the ToolsOptionsPageCPPCLR project, manually copy the ToolsOptionsPageCPPCLR.addin from the ToolsOptionsPageCPPCLR subdirectory to <My Documents>\Visual Studio 2005\Addins




If you load the C++ version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin ToolsOptionsPageCPPATL.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)
