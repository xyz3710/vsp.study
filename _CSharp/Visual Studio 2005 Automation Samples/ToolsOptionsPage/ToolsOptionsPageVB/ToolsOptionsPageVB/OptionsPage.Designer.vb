'Copyright (c) Microsoft Corporation.  All rights reserved.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OptionsPage
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.myPropertyCheckBox = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'myPropertyCheckBox
        '
        Me.myPropertyCheckBox.AutoSize = True
        Me.myPropertyCheckBox.Location = New System.Drawing.Point(3, 3)
        Me.myPropertyCheckBox.Name = "myPropertyCheckBox"
        Me.myPropertyCheckBox.Size = New System.Drawing.Size(82, 17)
        Me.myPropertyCheckBox.TabIndex = 0
        Me.myPropertyCheckBox.Text = "My Property"
        Me.myPropertyCheckBox.UseVisualStyleBackColor = True
        '
        'OptionsPage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.myPropertyCheckBox)
        Me.Name = "OptionsPage"
        Me.Size = New System.Drawing.Size(395, 289)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents myPropertyCheckBox As System.Windows.Forms.CheckBox

End Class
