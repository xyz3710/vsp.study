'Copyright (c) Microsoft Corporation.  All rights reserved.

Public Class OptionsPage
    Implements EnvDTE.IDTToolsOptionsPage

    Shared _propertiesObject As New OptionPageProperties

    Public Shared Function GetValueFromRegistry() As Boolean
        Dim registryKey As Microsoft.Win32.RegistryKey
        Dim registryValue As Integer

        registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\VisualStudio\8.0", False)
        registryValue = CType(registryKey.GetValue("MyPropertyVB", 0), Integer)
        If (registryValue = 0) Then
            Return False
        End If
        Return True
    End Function

    Public Shared Sub SetValueToRegistry(ByVal value As Boolean)
        Dim registryKey As Microsoft.Win32.RegistryKey
        Dim registryValue As Integer

        registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\VisualStudio\8.0", True)
        If (value) Then
            registryValue = 1
        Else
            registryValue = 0
        End If

        registryKey.SetValue("MyPropertyVB", registryValue, Microsoft.Win32.RegistryValueKind.DWord)
    End Sub

    Public Sub GetProperties(ByRef PropertiesObject As Object) Implements EnvDTE.IDTToolsOptionsPage.GetProperties
        'Return an object which is accessed by the method call DTE.Properties(category, subcategory)
        PropertiesObject = _propertiesObject
    End Sub

    Public Sub OnAfterCreated(ByVal DTEObject As EnvDTE.DTE) Implements EnvDTE.IDTToolsOptionsPage.OnAfterCreated
        myPropertyCheckBox.Checked = GetValueFromRegistry()
    End Sub

    Public Sub OnCancel() Implements EnvDTE.IDTToolsOptionsPage.OnCancel

    End Sub

    Public Sub OnHelp() Implements EnvDTE.IDTToolsOptionsPage.OnHelp
        MsgBox("TODO: Display Help")
    End Sub

    Public Sub OnOK() Implements EnvDTE.IDTToolsOptionsPage.OnOK
        SetValueToRegistry(myPropertyCheckBox.Checked)
    End Sub

End Class

<System.Runtime.InteropServices.ComVisible(True)> _
<System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDual)> _
Public Class OptionPageProperties

    Dim _myProperty As Boolean

    Public Property MyProperty() As Boolean
        Get
            Return OptionsPage.GetValueFromRegistry()
        End Get
        Set(ByVal value As Boolean)
            OptionsPage.SetValueToRegistry(value)
        End Set
    End Property

End Class

'Property can be accessed through the object model with code such as the following macro:
'    Sub ToolsOptionsPageProperties()
'        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual Basic").Item("MyProperty").Value)
'        DTE.Properties("My Category", "My Subcategory - Visual Basic").Item("MyProperty").Value = False
'        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual Basic").Item("MyProperty").Value)
'    End Sub