//Copyright (c) Microsoft Corporation.  All rights reserved.
package ToolsOptionsPageJS;

import System.Collections.Generic.*;
import System.ComponentModel.*;
import System.Data.*;
import System.Drawing.*;
import System.Windows.Forms.*;

/**
 * Summary description for OptionsPage.
 */
public class OptionsPage extends System.Windows.Forms.UserControl implements EnvDTE.IDTToolsOptionsPage 
{
	private CheckBox myPropertyCheckBox;

	static OptionPageProperties _propertiesObject = new OptionPageProperties();

	public static boolean GetValueFromRegistry()
        {
            Microsoft.Win32.RegistryKey registryKey;
            int registryValue = 0;

            registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\VisualStudio\\8.0", false);
			registryValue = System.Int32.Parse((registryKey.GetValue("MyPropertyJS", new Integer(0)).ToString()));
            if (registryValue == 0)
                return false;
            return true;
        }

	public static void SetValueToRegistry(boolean value)
        {
            Microsoft.Win32.RegistryKey registryKey;
            int registryValue;

            registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\VisualStudio\\8.0", true);
            if (value)
                registryValue = 1;
            else
                registryValue = 0;

            registryKey.SetValue("MyPropertyJS", new Integer(registryValue), Microsoft.Win32.RegistryValueKind.DWord);
        }

	/**
	 * Required designer variable.
	 */
	private System.ComponentModel.IContainer components;

	public OptionsPage()
	{
		//
		// This call is required by the Windows.Forms Form Designer.
		//
		InitializeComponent();

		//
		// TODO: Add any initialization after the InitializeComponent call
		//
	}

	#region Component Designer generated code
	/**
     * Clean up any resources being used.
     */
	protected void Dispose(boolean disposing)
	{
		if (disposing)
		{
			if (components != null)
			{
				components.Dispose();
			}
		}
		super.Dispose(disposing);
	}

	/**
	 * Required method for Designer support - do not modify 
	 * the contents of this method with the code editor.
	 */
	private void InitializeComponent()
	{
		this.myPropertyCheckBox = new System.Windows.Forms.CheckBox();
		this.SuspendLayout();
		// 
		// myPropertyCheckBox
		// 
		this.myPropertyCheckBox.set_AutoSize(true);
		this.myPropertyCheckBox.set_Location(new System.Drawing.Point(4, 4));
		this.myPropertyCheckBox.set_Name("myPropertyCheckBox");
		this.myPropertyCheckBox.set_Size(new System.Drawing.Size(82, 17));
		this.myPropertyCheckBox.set_TabIndex(0);
		this.myPropertyCheckBox.set_Text("My Property");
		this.myPropertyCheckBox.set_UseVisualStyleBackColor(true);
		// 
		// OptionsPage
		// 
		this.set_AutoScaleDimensions(new System.Drawing.SizeF(6F, 13F));
		this.set_AutoScaleMode(System.Windows.Forms.AutoScaleMode.Font);
		this.get_Controls().Add(this.myPropertyCheckBox);
		this.set_Name("OptionsPage");
		this.set_Size(new System.Drawing.Size(395, 289));
		this.ResumeLayout(false);
		this.PerformLayout();

	}
	#endregion

	#region IDTToolsOptionsPage Members

        public void GetProperties(/**@ref*/ System.Object PropertiesObject)
        {
            //Return an object which is accessed by the method call DTE.Properties(category, subcategory)
            PropertiesObject = _propertiesObject;
        }

        public void OnAfterCreated(EnvDTE.DTE DTEObject)
        {
            myPropertyCheckBox.set_Checked(GetValueFromRegistry());
        }

        public void OnCancel()
        {
        }

        public void OnHelp()
        {
            System.Windows.Forms.MessageBox.Show("TODO: Display Help");
        }

        public void OnOK()
        {
            SetValueToRegistry(myPropertyCheckBox.get_Checked());
        }

        #endregion
}

/**
 * @attribute System.Runtime.InteropServices.ComVisible(true)
 * @attribute System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDual)
 */
public class OptionPageProperties
{
	/** @property */
    public boolean get_MyProperty()
    {
            return OptionsPage.GetValueFromRegistry();
    }

	/** @property */
	public void set_MyProperty(boolean value)
    {
        OptionsPage.SetValueToRegistry(value);
    }
}

//Property can be accessed through the object model with code such as the following macro:
//    Sub ToolsOptionsPageProperties()
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual J#").Item("MyProperty").Value)
//        DTE.Properties("My Category", "My Subcategory - Visual J#").Item("MyProperty").Value = False
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual J#").Item("MyProperty").Value)
//    End Sub