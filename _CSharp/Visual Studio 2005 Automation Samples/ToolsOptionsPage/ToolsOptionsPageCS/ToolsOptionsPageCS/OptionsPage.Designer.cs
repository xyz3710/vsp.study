//Copyright (c) Microsoft Corporation.  All rights reserved.

namespace ToolsOptionsPageCS
{
    partial class OptionsPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myPropertyCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // myPropertyCheckBox
            // 
            this.myPropertyCheckBox.AutoSize = true;
            this.myPropertyCheckBox.Location = new System.Drawing.Point(4, 4);
            this.myPropertyCheckBox.Name = "myPropertyCheckBox";
            this.myPropertyCheckBox.Size = new System.Drawing.Size(82, 17);
            this.myPropertyCheckBox.TabIndex = 0;
            this.myPropertyCheckBox.Text = "My Property";
            this.myPropertyCheckBox.UseVisualStyleBackColor = true;
            // 
            // OptionsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.myPropertyCheckBox);
            this.Name = "OptionsPage";
            this.Size = new System.Drawing.Size(395, 289);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox myPropertyCheckBox;
    }
}
