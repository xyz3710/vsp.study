//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ToolsOptionsPageCS
{
    public partial class OptionsPage : UserControl, EnvDTE.IDTToolsOptionsPage
    {
        static OptionPageProperties _propertiesObject = new OptionPageProperties();

        public OptionsPage()
        {
            InitializeComponent();
        }

        public static bool GetValueFromRegistry()
        {
            Microsoft.Win32.RegistryKey registryKey;
            int registryValue;

            registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\VisualStudio\8.0", false);
            registryValue = (int)registryKey.GetValue("MyPropertyCS", 0);
            if (registryValue == 0)
                return false;
            return true;
        }

        public static void SetValueToRegistry(bool value)
        {
            Microsoft.Win32.RegistryKey registryKey;
            int registryValue;

            registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\VisualStudio\8.0", true);
            if (value)
                registryValue = 1;
            else
                registryValue = 0;

            registryKey.SetValue("MyPropertyCS", registryValue, Microsoft.Win32.RegistryValueKind.DWord);
        }

        #region IDTToolsOptionsPage Members

        public void GetProperties(ref object PropertiesObject)
        {
            //Return an object which is accessed by the method call DTE.Properties(category, subcategory)
            PropertiesObject = _propertiesObject;
        }

        public void OnAfterCreated(EnvDTE.DTE DTEObject)
        {
            myPropertyCheckBox.Checked = GetValueFromRegistry();
        }

        public void OnCancel()
        {
        }

        public void OnHelp()
        {
            System.Windows.Forms.MessageBox.Show("TODO: Display Help");
        }

        public void OnOK()
        {
            SetValueToRegistry(myPropertyCheckBox.Checked);
        }

        #endregion
    }

    [System.Runtime.InteropServices.ComVisible(true)]
    [System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDual)]
    public class OptionPageProperties
    {
        public bool MyProperty
        {
            get
            {
                return OptionsPage.GetValueFromRegistry();
            }
            set
            {
                OptionsPage.SetValueToRegistry(value);
            }
        }
    }
}

//Property can be accessed through the object model with code such as the following macro:
//    Sub ToolsOptionsPageProperties()
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual C#").Item("MyProperty").Value)
//        DTE.Properties("My Category", "My Subcategory - Visual C#").Item("MyProperty").Value = False
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual C#").Item("MyProperty").Value)
//    End Sub