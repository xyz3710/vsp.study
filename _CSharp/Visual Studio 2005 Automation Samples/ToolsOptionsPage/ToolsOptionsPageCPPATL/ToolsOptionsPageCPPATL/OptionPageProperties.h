//Copyright (c) Microsoft Corporation.  All rights reserved.

// OptionPageProperties.h : Declaration of the COptionPageProperties

#pragma once
#include "resource.h"       // main symbols

#include "Addin.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// COptionPageProperties

class ATL_NO_VTABLE COptionPageProperties :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COptionPageProperties, &CLSID_OptionPageProperties>,
	public IDispatchImpl<IOptionPageProperties, &IID_IOptionPageProperties, &LIBID_ToolsOptionsPageCPPATLLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	COptionPageProperties()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_OPTIONPAGEPROPERTIES)


BEGIN_COM_MAP(COptionPageProperties)
	COM_INTERFACE_ENTRY(IOptionPageProperties)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

public:
	STDMETHOD(get_MyProperty)(VARIANT_BOOL* pVal);
public:
	STDMETHOD(put_MyProperty)(VARIANT_BOOL newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(OptionPageProperties), COptionPageProperties)
