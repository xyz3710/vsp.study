//Copyright (c) Microsoft Corporation.  All rights reserved.

// AddIn.cpp : Implementation of DLL Exports.

#include "stdafx.h"
#include "resource.h"
#include "AddIn.h"

CAddInModule _AtlModule;

// {757E0159-D613-40CD-82FB-87BBA6A023EC}
static const IID IID_IOptionsPage = 
{ 0x757E0159, 0xD613, 0x40CD, { 0x82, 0xFB, 0x87, 0xBB, 0xA6, 0xA0, 0x23, 0xEC } };

// {3B858C5B-16A3-4BAC-A290-7C1FA67CCC2A}
static const IID LIBID_ToolsOptionsPageCPPATLLib = 
{ 0x3B858C5B, 0x16A3, 0x4BAC, { 0xA2, 0x90, 0x7C, 0x1F, 0xA6, 0x7C, 0xCC, 0x2A } };

// {BEEAA5B9-4321-44A0-8E09-DA6583F71C0A}
static const CLSID CLSID_OptionsPage = 
{ 0xBEEAA5B9, 0x4321, 0x44A0, { 0x8E, 0x09, 0xDA, 0x65, 0x83, 0xF7, 0x1C, 0x0A } };

// {4FADD0A3-10E0-4712-87F1-55AA80BE74DB}
static const IID IID_IOptionPageProperties =
{ 0x4FADD0A3, 0x10E0, 0x4712, { 0x87, 0xF1, 0x55, 0xAA, 0x80, 0xBE, 0x74, 0xDB } };

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	_AtlModule.SetResourceInstance(hInstance);
	return _AtlModule.DllMain(dwReason, lpReserved); 
}


// Used to determine whether the DLL can be unloaded by OLE
STDAPI DllCanUnloadNow(void)
{
	return _AtlModule.DllCanUnloadNow();
}


// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	return _AtlModule.DllGetClassObject(rclsid, riid, ppv);
}


// DllRegisterServer - Adds entries to the system registry
STDAPI DllRegisterServer(void)
{
	// registers object, typelib and all interfaces in typelib
	HRESULT hr = _AtlModule.DllRegisterServer();
	return hr;
}


// DllUnregisterServer - Removes entries from the system registry
STDAPI DllUnregisterServer(void)
{
	HRESULT hr = _AtlModule.DllUnregisterServer();
	return hr;
}
