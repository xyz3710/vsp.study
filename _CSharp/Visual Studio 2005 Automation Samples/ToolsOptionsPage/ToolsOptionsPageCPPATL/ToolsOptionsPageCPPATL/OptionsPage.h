//Copyright (c) Microsoft Corporation.  All rights reserved.

// OptionsPage.h : Declaration of the COptionsPage
#pragma once
#include "resource.h"       // main symbols
#include <atlctl.h>
#include "Addin.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif


// COptionsPage
class ATL_NO_VTABLE COptionsPage :
	public CComObjectRootEx<CComSingleThreadModel>,
	//public IDispatchImpl<IOptionsPage, &IID_IOptionsPage, &LIBID_ToolsOptionsPageCPPATLLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IPersistStreamInitImpl<COptionsPage>,
	public IOleControlImpl<COptionsPage>,
	public IOleObjectImpl<COptionsPage>,
	public IOleInPlaceActiveObjectImpl<COptionsPage>,
	public IViewObjectExImpl<COptionsPage>,
	public IOleInPlaceObjectWindowlessImpl<COptionsPage>,
	public ISupportErrorInfo,
	public IPersistStorageImpl<COptionsPage>,
	public ISpecifyPropertyPagesImpl<COptionsPage>,
	public IQuickActivateImpl<COptionsPage>,
#ifndef _WIN32_WCE
	public IDataObjectImpl<COptionsPage>,
#endif
	public IProvideClassInfo2Impl<&CLSID_OptionsPage, NULL, &LIBID_ToolsOptionsPageCPPATLLib>,
#ifdef _WIN32_WCE // IObjectSafety is required on Windows CE for the control to be loaded correctly
	public IObjectSafetyImpl<COptionsPage, INTERFACESAFE_FOR_UNTRUSTED_CALLER>,
#endif
	public CComCoClass<COptionsPage, &CLSID_OptionsPage>,
	public CComCompositeControl<COptionsPage>,
	public IDispatchImpl<EnvDTE::IDTToolsOptionsPage, &EnvDTE::IID_IDTToolsOptionsPage, &EnvDTE::LIBID_EnvDTE, 8, 0>
{
public:


	COptionsPage()
	{
		m_bWindowOnly = TRUE;
		CalcExtent(m_sizeExtent);
	}

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_INSIDEOUT |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST
)

DECLARE_REGISTRY_RESOURCEID(IDR_OPTIONSPAGE)


BEGIN_COM_MAP(COptionsPage)
	//COM_INTERFACE_ENTRY(IOptionsPage)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IViewObjectEx)
	COM_INTERFACE_ENTRY(IViewObject2)
	COM_INTERFACE_ENTRY(IViewObject)
	COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceObject)
	COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
	COM_INTERFACE_ENTRY(IOleControl)
	COM_INTERFACE_ENTRY(IOleObject)
	COM_INTERFACE_ENTRY(IPersistStreamInit)
	COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
	COM_INTERFACE_ENTRY(IQuickActivate)
	COM_INTERFACE_ENTRY(IPersistStorage)
	COM_INTERFACE_ENTRY(IDTToolsOptionsPage)
#ifndef _WIN32_WCE
	COM_INTERFACE_ENTRY(IDataObject)
#endif
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
#ifdef _WIN32_WCE // IObjectSafety is required on Windows CE for the control to be loaded correctly
	COM_INTERFACE_ENTRY_IID(IID_IObjectSafety, IObjectSafety)
#endif
END_COM_MAP()

BEGIN_PROP_MAP(COptionsPage)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	// Example entries
	// PROP_ENTRY("Property Description", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()


BEGIN_MSG_MAP(COptionsPage)
	CHAIN_MSG_MAP(CComCompositeControl<COptionsPage>)
END_MSG_MAP()
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

BEGIN_SINK_MAP(COptionsPage)
	//Make sure the Event Handlers have __stdcall calling convention
END_SINK_MAP()

	STDMETHOD(OnAmbientPropertyChange)(DISPID dispid)
	{
		if (dispid == DISPID_AMBIENT_BACKCOLOR)
		{
			SetBackgroundColorFromAmbient();
			FireViewChange();
		}
		return IOleControlImpl<COptionsPage>::OnAmbientPropertyChange(dispid);
	}
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
	{
		static const IID* arr[] =
		{
			&IID_IOptionsPage,
		};

		for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
		{
			if (InlineIsEqualGUID(*arr[i], riid))
				return S_OK;
		}
		return S_FALSE;
	}

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IOptionsPage

	enum { IDD = IDD_OPTIONSPAGE };

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

// IDTToolsOptionsPage
	STDMETHOD(OnAfterCreated)(EnvDTE::_DTE *DTEObject);
	STDMETHOD(GetProperties)(IDispatch **PropertiesObject);
	STDMETHOD(OnOK)();
	STDMETHOD(OnCancel)();
	STDMETHOD(OnHelp)();

	CComPtr<IDispatch> m_pDispOptionPageProperties;
	CComPtr<EnvDTE::_DTE> m_pDTE;
};

OBJECT_ENTRY_AUTO(__uuidof(OptionsPage), COptionsPage)
