//Copyright (c) Microsoft Corporation.  All rights reserved.

// OptionPageProperties.cpp : Implementation of COptionPageProperties

#include "stdafx.h"
#include "OptionPageProperties.h"


// COptionPageProperties

extern bool GetValueFromRegistry();
extern void SetValueToRegistry(bool fValue);

STDMETHODIMP COptionPageProperties::get_MyProperty(VARIANT_BOOL* pVal)
{
	*pVal = GetValueFromRegistry() ? VARIANT_TRUE : VARIANT_FALSE;
	return S_OK;
}

STDMETHODIMP COptionPageProperties::put_MyProperty(VARIANT_BOOL newVal)
{
	SetValueToRegistry((newVal == VARIANT_TRUE) ? true : false);
	return S_OK;
}
