//Copyright (c) Microsoft Corporation.  All rights reserved.

// OptionsPage.cpp : Implementation of COptionsPage
#include "stdafx.h"
#include "OptionsPage.h"
#include "OptionPageProperties.h"

// COptionsPage

//Property can be accessed through the object model with code such as the following macro:
//    Sub ToolsOptionsPageProperties()
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual C++/ATL").Item("MyProperty").Value)
//        DTE.Properties("My Category", "My Subcategory - Visual C++/ATL").Item("MyProperty").Value = False
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual C++/ATL").Item("MyProperty").Value)
//    End Sub

bool GetValueFromRegistry()
{
	DWORD dwValue = 0;
    HKEY hKey = NULL;

	if(RegOpenKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\VisualStudio\\8.0", 0, KEY_READ, &hKey) == ERROR_SUCCESS)
	{
		DWORD dwType = 0;
		DWORD dwSize = sizeof(DWORD);
		RegQueryValueEx(hKey, "MyPropertyCPPATL", NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
		RegCloseKey(hKey);
	}
    
    if (dwValue == 0)
        return false;
    return true;
}

void SetValueToRegistry(bool fValue)
{
	DWORD dwValue = (fValue) ? 1 : 0;
    HKEY hKey = NULL;

	if(RegOpenKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\VisualStudio\\8.0", 0, KEY_READ|KEY_WRITE, &hKey) == ERROR_SUCCESS)
	{
		DWORD dwType = REG_DWORD;
		DWORD dwSize = sizeof(DWORD);
		RegSetValueEx(hKey, "MyPropertyCPPATL", 0, REG_DWORD, (BYTE*)&dwValue, sizeof(DWORD));
		RegCloseKey(hKey);
	}
}

STDMETHODIMP COptionsPage::OnAfterCreated(EnvDTE::_DTE *DTEObject)
{
	m_pDTE = DTEObject;
	bool fValue = GetValueFromRegistry();
	SendDlgItemMessage(IDC_MYPROPERTYCHECK, BM_SETCHECK, (fValue) ? BST_CHECKED : BST_UNCHECKED);
	return S_OK;
}

STDMETHODIMP COptionsPage::GetProperties(IDispatch **PropertiesObject)
{
	if(!m_pDispOptionPageProperties)
	{
		CComObject<COptionPageProperties> *pDispOptionPageProperties;
		CComObject<COptionPageProperties>::CreateInstance(&pDispOptionPageProperties);
		pDispOptionPageProperties->QueryInterface(IID_IDispatch, (LPVOID*)&m_pDispOptionPageProperties);
	}
	m_pDispOptionPageProperties->QueryInterface(IID_IDispatch, (LPVOID*)PropertiesObject);
	return S_OK;
}

STDMETHODIMP COptionsPage::OnOK()
{
	bool fChecked = false;
	if(SendDlgItemMessage(IDC_MYPROPERTYCHECK, BM_GETCHECK, 0, 0) == BST_CHECKED)
		fChecked = true;
	SetValueToRegistry(fChecked);
	return S_OK;
}

STDMETHODIMP COptionsPage::OnCancel()
{
	return S_OK;
}

STDMETHODIMP COptionsPage::OnHelp()
{
	MessageBox("TODO: Display Help", "Tools Options Page C++/ATL", MB_OK);
	return S_OK;
}