//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AddIn.rc
//
#define IDS_PROJNAME                    100
#define IDR_ADDIN                       101
#define IDR_CONNECT                     102
#define IDB_OPTIONSPAGE                 103
#define IDR_OPTIONSPAGE                 104
#define IDD_OPTIONSPAGE                 105
#define IDR_OPTIONPAGEPROPERTIES        106
#define IDC_MYPROPERTYCHECK             201

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         202
#define _APS_NEXT_SYMED_VALUE           107
#endif
#endif
