

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Sun Dec 04 19:07:07 2005
 */
/* Compiler settings for .\AddIn.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __AddIn_h__
#define __AddIn_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IOptionsPage_FWD_DEFINED__
#define __IOptionsPage_FWD_DEFINED__
typedef interface IOptionsPage IOptionsPage;
#endif 	/* __IOptionsPage_FWD_DEFINED__ */


#ifndef __IOptionPageProperties_FWD_DEFINED__
#define __IOptionPageProperties_FWD_DEFINED__
typedef interface IOptionPageProperties IOptionPageProperties;
#endif 	/* __IOptionPageProperties_FWD_DEFINED__ */


#ifndef __Connect_FWD_DEFINED__
#define __Connect_FWD_DEFINED__

#ifdef __cplusplus
typedef class Connect Connect;
#else
typedef struct Connect Connect;
#endif /* __cplusplus */

#endif 	/* __Connect_FWD_DEFINED__ */


#ifndef __OptionsPage_FWD_DEFINED__
#define __OptionsPage_FWD_DEFINED__

#ifdef __cplusplus
typedef class OptionsPage OptionsPage;
#else
typedef struct OptionsPage OptionsPage;
#endif /* __cplusplus */

#endif 	/* __OptionsPage_FWD_DEFINED__ */


#ifndef __OptionPageProperties_FWD_DEFINED__
#define __OptionPageProperties_FWD_DEFINED__

#ifdef __cplusplus
typedef class OptionPageProperties OptionPageProperties;
#else
typedef struct OptionPageProperties OptionPageProperties;
#endif /* __cplusplus */

#endif 	/* __OptionPageProperties_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IOptionsPage_INTERFACE_DEFINED__
#define __IOptionsPage_INTERFACE_DEFINED__

/* interface IOptionsPage */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IOptionsPage;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("757E0159-D613-40CD-82FB-87BBA6A023EC")
    IOptionsPage : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IOptionsPageVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOptionsPage * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOptionsPage * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOptionsPage * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOptionsPage * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOptionsPage * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOptionsPage * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOptionsPage * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IOptionsPageVtbl;

    interface IOptionsPage
    {
        CONST_VTBL struct IOptionsPageVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOptionsPage_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IOptionsPage_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IOptionsPage_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IOptionsPage_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IOptionsPage_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IOptionsPage_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IOptionsPage_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOptionsPage_INTERFACE_DEFINED__ */


#ifndef __IOptionPageProperties_INTERFACE_DEFINED__
#define __IOptionPageProperties_INTERFACE_DEFINED__

/* interface IOptionPageProperties */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IOptionPageProperties;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4FADD0A3-10E0-4712-87F1-55AA80BE74DB")
    IOptionPageProperties : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MyProperty( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MyProperty( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IOptionPagePropertiesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOptionPageProperties * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOptionPageProperties * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOptionPageProperties * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOptionPageProperties * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOptionPageProperties * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOptionPageProperties * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOptionPageProperties * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MyProperty )( 
            IOptionPageProperties * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MyProperty )( 
            IOptionPageProperties * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        END_INTERFACE
    } IOptionPagePropertiesVtbl;

    interface IOptionPageProperties
    {
        CONST_VTBL struct IOptionPagePropertiesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOptionPageProperties_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IOptionPageProperties_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IOptionPageProperties_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IOptionPageProperties_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IOptionPageProperties_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IOptionPageProperties_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IOptionPageProperties_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IOptionPageProperties_get_MyProperty(This,pVal)	\
    (This)->lpVtbl -> get_MyProperty(This,pVal)

#define IOptionPageProperties_put_MyProperty(This,newVal)	\
    (This)->lpVtbl -> put_MyProperty(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IOptionPageProperties_get_MyProperty_Proxy( 
    IOptionPageProperties * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IOptionPageProperties_get_MyProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IOptionPageProperties_put_MyProperty_Proxy( 
    IOptionPageProperties * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IOptionPageProperties_put_MyProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IOptionPageProperties_INTERFACE_DEFINED__ */



#ifndef __ToolsOptionsPageCPPATLLib_LIBRARY_DEFINED__
#define __ToolsOptionsPageCPPATLLib_LIBRARY_DEFINED__

/* library ToolsOptionsPageCPPATLLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_ToolsOptionsPageCPPATLLib;

EXTERN_C const CLSID CLSID_Connect;

#ifdef __cplusplus

class DECLSPEC_UUID("BEEAA5B9-4321-44A0-8E09-DA6583F71C0A")
Connect;
#endif

EXTERN_C const CLSID CLSID_OptionsPage;

#ifdef __cplusplus

class DECLSPEC_UUID("1C1B2FFF-F7E1-4994-A92B-83D11FB11990")
OptionsPage;
#endif

EXTERN_C const CLSID CLSID_OptionPageProperties;

#ifdef __cplusplus

class DECLSPEC_UUID("B33B0DCB-760B-47AA-AAE0-3A2524E0A361")
OptionPageProperties;
#endif
#endif /* __ToolsOptionsPageCPPATLLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


