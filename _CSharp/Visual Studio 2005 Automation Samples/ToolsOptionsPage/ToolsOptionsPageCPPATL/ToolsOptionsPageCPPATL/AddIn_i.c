

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Sun Dec 04 19:07:07 2005
 */
/* Compiler settings for .\AddIn.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IOptionsPage,0x757E0159,0xD613,0x40CD,0x82,0xFB,0x87,0xBB,0xA6,0xA0,0x23,0xEC);


MIDL_DEFINE_GUID(IID, IID_IOptionPageProperties,0x4FADD0A3,0x10E0,0x4712,0x87,0xF1,0x55,0xAA,0x80,0xBE,0x74,0xDB);


MIDL_DEFINE_GUID(IID, LIBID_ToolsOptionsPageCPPATLLib,0x3B858C5B,0x16A3,0x4BAC,0xA2,0x90,0x7C,0x1F,0xA6,0x7C,0xCC,0x2A);


MIDL_DEFINE_GUID(CLSID, CLSID_Connect,0xBEEAA5B9,0x4321,0x44A0,0x8E,0x09,0xDA,0x65,0x83,0xF7,0x1C,0x0A);


MIDL_DEFINE_GUID(CLSID, CLSID_OptionsPage,0x1C1B2FFF,0xF7E1,0x4994,0xA9,0x2B,0x83,0xD1,0x1F,0xB1,0x19,0x90);


MIDL_DEFINE_GUID(CLSID, CLSID_OptionPageProperties,0xB33B0DCB,0x760B,0x47AA,0xAA,0xE0,0x3A,0x25,0x24,0xE0,0xA3,0x61);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



