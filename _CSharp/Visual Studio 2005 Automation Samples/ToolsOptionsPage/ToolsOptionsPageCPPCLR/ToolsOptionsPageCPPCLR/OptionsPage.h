//Copyright (c) Microsoft Corporation.  All rights reserved.

#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace ToolsOptionsPageCPPCLR 
{

	[System::Runtime::InteropServices::ComVisible(true)]
	[System::Runtime::InteropServices::ClassInterface(System::Runtime::InteropServices::ClassInterfaceType::AutoDual)]
	public ref class OptionPageProperties
	{
	public:
		OptionPageProperties()
		{
		}

		property bool MyProperty
		{
			bool get()
			{
				return GetValueFromRegistry();
			}

			void set(bool value)
			{
				SetValueToRegistry(value);
			}
		}

	public:
		static bool GetValueFromRegistry()
		{
			Microsoft::Win32::RegistryKey ^registryKey;
			int registryValue;

			registryKey = Microsoft::Win32::Registry::CurrentUser->OpenSubKey("SOFTWARE\\Microsoft\\VisualStudio\\8.0", false);
			registryValue = (int)registryKey->GetValue("MyPropertyCPPCLR", 0);
			if (registryValue == 0)
				return false;
			return true;
		}

		static void SetValueToRegistry(bool value)
		{
			Microsoft::Win32::RegistryKey ^registryKey;
			int registryValue;

			registryKey = Microsoft::Win32::Registry::CurrentUser->OpenSubKey("SOFTWARE\\Microsoft\\VisualStudio\\8.0", true);
			if (value)
				registryValue = 1;
			else
				registryValue = 0;

			registryKey->SetValue("MyPropertyCPPCLR", registryValue, Microsoft::Win32::RegistryValueKind::DWord);
		}
	};

	/// <summary>
	/// Summary for OptionsPage
	/// </summary>
	public ref class OptionsPage : public System::Windows::Forms::UserControl, public EnvDTE::IDTToolsOptionsPage
	{
		static OptionPageProperties ^_propertiesObject = gcnew OptionPageProperties();

	public:
		OptionsPage(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~OptionsPage()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  myPropertyCheckBox;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->myPropertyCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->SuspendLayout();
			// 
			// myPropertyCheckBox
			// 
			this->myPropertyCheckBox->AutoSize = true;
			this->myPropertyCheckBox->Location = System::Drawing::Point(4, 4);
			this->myPropertyCheckBox->Name = L"myPropertyCheckBox";
			this->myPropertyCheckBox->Size = System::Drawing::Size(82, 17);
			this->myPropertyCheckBox->TabIndex = 0;
			this->myPropertyCheckBox->Text = L"My Property";
			this->myPropertyCheckBox->UseVisualStyleBackColor = true;
			// 
			// OptionsPage
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->myPropertyCheckBox);
			this->Name = L"OptionsPage";
			this->Size = System::Drawing::Size(395, 289);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

#pragma region IDTToolsOptionsPage Members
	public:
		virtual void GetProperties(System::Object ^%PropertiesObject)
		{
			//Return an object which is accessed by the method call DTE.Properties(category, subcategory)
			PropertiesObject = _propertiesObject;
		}

		virtual void OnAfterCreated(EnvDTE::DTE ^DTEObject)
		{
			myPropertyCheckBox->Checked = OptionPageProperties::GetValueFromRegistry();
		}

		virtual void OnCancel()
		{
		}

		virtual void OnHelp()
		{
			System::Windows::Forms::MessageBox::Show("TODO: Display Help");
		}

		virtual void OnOK()
		{
			OptionPageProperties::SetValueToRegistry(myPropertyCheckBox->Checked);
		}

#pragma endregion
	};
}

//Property can be accessed through the object model with code such as the following macro:
//    Sub ToolsOptionsPageProperties()
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual C++/CLR").Item("MyProperty").Value)
//        DTE.Properties("My Category", "My Subcategory - Visual C++/CLR").Item("MyProperty").Value = True
//        MsgBox(DTE.Properties("My Category", "My Subcategory - Visual C++/CLR").Item("MyProperty").Value)
//    End Sub