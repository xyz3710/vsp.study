//Copyright (c) Microsoft Corporation.  All rights reserved.

// This is the main DLL file.

#include "stdafx.h"
#include "Connect.h"

void ToolsOptionsPageCPPCLR::Connect::OnAddInsUpdate(System::Array ^%custom)
{
}

void ToolsOptionsPageCPPCLR::Connect::OnBeginShutdown(System::Array ^%custom)
{
}

void ToolsOptionsPageCPPCLR::Connect::OnConnection(System::Object ^Application, ext_ConnectMode ConnectMode, System::Object ^AddInInst, System::Array ^%custom)
{
    _applicationObject = dynamic_cast<DTE2^>(Application);
    _addInInstance = dynamic_cast<AddIn^>(AddInInst);
    
}

void ToolsOptionsPageCPPCLR::Connect::OnStartupComplete(System::Array ^%custom)
{
}

void ToolsOptionsPageCPPCLR::Connect::OnDisconnection(ext_DisconnectMode removeMode, System::Array ^%custom)
{
}

