Copyright (c) Microsoft Corporation.  All rights reserved.

When loaded the Add-in will create a tool window, then host a UserControl (for C# & VB) or an ActiveX control (C++) inside of that tool window

Instructions for VB, C# and VJ#:
------------------------------------------------------------------------------------------------------------------------
Once you load the managed version of this sample (VB or C#), you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin <language>ToolWindow.Connect (where <language> is either CSharp, VB or VJ depending on which version of the sample you loaded)
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

To run the Add-ins, follow these steps:
1. Load the project in the VSUserControlHost directory and build it. 
2. Load either the C# or VB version of the project.
3. If the reference to the VSUserControlHostLib is broken, you will need to remove it and add it again.  In the 
references dialog box for the ToolWindowAddin project, remove the reference to VSUserControlHostLib by right-clicking on it and selecting 'Remove'.  Then add a reference to the vsusercontrolhostlib.dll located in the ToolWindow directory.
4. In either the CSharpToolWindow directory or VBToolWindow directory (depending on which language you are using), run the reg file 'AddinReg.reg'.
5. Start a new instance of Visual Studio .NET, and in the Add-in manager, load the Add-in you just built.

The VSUserControlHost project is a 'Shim' control which is used to host a .NET UserControl. It creates an instance of 
the .NET runtime, instantiates the UserControl, then parents that control onto the VSUserControlHost. VSUserControlHost is
a general-purpose ATL control, which you may ship as part of your product to 


Instructions for CPPCLR:
------------------------------------------------------------------------------------------------------------------------
If you load the CPPCLR version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Linker | General and change the Output File property to <My Documents>\Visual Studio 2005\Addins\$(ProjectName).dll (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)

3. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin CPPCLRToolWindow.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

After building the CPPCLRToolWindow project, manually copy the CPPCLRToolWindow.addin from the CPPCLRToolWindow subdirectory to <My Documents>\Visual Studio 2005\Addins



Instructions for Visual C++:
------------------------------------------------------------------------------------------------------------------------
If you load the C++ version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin CPPToolWindow.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

