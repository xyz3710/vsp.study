'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System
Imports Microsoft.VisualStudio.CommandBars
Imports Extensibility
Imports EnvDTE
Imports EnvDTE80

Public Class Connect
	
    Implements IDTExtensibility2
	

    Dim _applicationObject As DTE2
    Dim _addInInstance As AddIn
    Dim _windowToolWindow As Window

    '''<summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
    Public Sub New()

    End Sub

    '''<summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
    '''<param name='application'>Root object of the host application.</param>
    '''<param name='connectMode'>Describes how the Add-in is being loaded.</param>
    '''<param name='addInInst'>Object representing this Add-in.</param>
    '''<remarks></remarks>
    Public Sub OnConnection(ByVal application As Object, ByVal connectMode As ext_ConnectMode, ByVal addInInst As Object, ByRef custom As Array) Implements IDTExtensibility2.OnConnection
        _applicationObject = CType(application, DTE2)
        _addInInstance = CType(addInInst, AddIn)

        Dim programmableObject As Object

        'This guid must be unique for each different tool window,
        ' but you may use the same guid for the same tool window.
        'This guid can be used for indexing the windows collection,
        ' for example: applicationObject.Windows.Item(guidstr)
        Dim guidstr As String = "{439bc5e4-6ec1-491f-adc9-fffe36776c2e}"
        Dim windows2 As EnvDTE80.Windows2
        Dim asm As System.Reflection.Assembly
        asm = System.Reflection.Assembly.GetExecutingAssembly()
        windows2 = CType(_applicationObject.Windows, EnvDTE80.Windows2)
        _windowToolWindow = windows2.CreateToolWindow2(_addInInstance, asm.Location, "VBToolWindow.ToolWindowControl", "Visual Basic .NET Tool window", guidstr, programmableObject)

        'Set the picture displayed when the window is tab docked
        _windowToolWindow.SetTabPicture(My.Resources.Resource.ToolWindowBmp.GetHbitmap())

        'When using the hosting control, you must set visible to true before calling HostUserControl,
        ' otherwise the UserControl cannot be hosted properly.
        _windowToolWindow.Visible = True
	End Sub

    '''<summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
    '''<param name='disconnectMode'>Describes how the Add-in is being unloaded.</param>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnDisconnection(ByVal disconnectMode As ext_DisconnectMode, ByRef custom As Array) Implements IDTExtensibility2.OnDisconnection
    End Sub

    '''<summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification that the collection of Add-ins has changed.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnAddInsUpdate(ByRef custom As Array) Implements IDTExtensibility2.OnAddInsUpdate
    End Sub

    '''<summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnStartupComplete(ByRef custom As Array) Implements IDTExtensibility2.OnStartupComplete
    End Sub

    '''<summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnBeginShutdown(ByRef custom As Array) Implements IDTExtensibility2.OnBeginShutdown
    End Sub
	
End Class
