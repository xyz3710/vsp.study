//Copyright (c) Microsoft Corporation.  All rights reserved.

// This is the main DLL file.

#include "stdafx.h"
#include "Connect.h"




void CPPCLRToolWindow::Connect::OnAddInsUpdate(System::Array ^%custom)
{
}

void CPPCLRToolWindow::Connect::OnBeginShutdown(System::Array ^%custom)
{
}

void CPPCLRToolWindow::Connect::OnConnection(System::Object ^Application, ext_ConnectMode ConnectMode, System::Object ^AddInInst, System::Array ^%custom)
{
    _applicationObject = dynamic_cast<DTE2^>(Application);
    _addInInstance = dynamic_cast<AddIn^>(AddInInst);
    
	EnvDTE80::Windows2 ^windows2;
			
    System::Object ^programmableObject;

	//This guid must be unique for each different tool window,
	// but you may use the same guid for the same tool window.
	//This guid can be used for indexing the windows collection,
	// for example: applicationObject.Windows.Item(guidstr)
	String ^guidstr = "{858C3FCD-8B39-4540-A592-F31C1520B174}";
	windows2 = dynamic_cast<EnvDTE80::Windows2^>(_applicationObject->Windows);
    System::Reflection::Assembly ^assembly = System::Reflection::Assembly::GetExecutingAssembly();
    _windowToolWindow = windows2->CreateToolWindow2(_addInInstance, assembly->Location, "CPPCLRToolWindow.ToolWindowControl", "C++/CLR Tool window", guidstr, programmableObject);

	_windowToolWindow->SetTabPicture(ToolWindowBmp()->GetHbitmap());

	//When using the hosting control, you must set visible to true before calling HostUserControl,
	// otherwise the UserControl cannot be hosted properly.
	_windowToolWindow->Visible = true;
}

void CPPCLRToolWindow::Connect::OnStartupComplete(System::Array ^%custom)
{
}

void CPPCLRToolWindow::Connect::OnDisconnection(ext_DisconnectMode removeMode, System::Array ^%custom)
{
}

