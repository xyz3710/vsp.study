//Copyright (c) Microsoft Corporation.  All rights reserved.
package VJToolWindow;

import System.*;
import Microsoft.VisualStudio.CommandBars.*;
import Extensibility.*;
import EnvDTE.*;
import EnvDTE80.*;

/**
/// <summary>The object for implementing an Add-in.</summary>
/// <seealso class='IDTExtensibility2' />
 */
public class Connect implements IDTExtensibility2
{
	/**
	/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
	 */
	public Connect()
	{
	}

	/**
	/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
	/// <param term='application'>Root object of the host application.</param>
	/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
	/// <param term='addInInst'>Object representing this Add-in.</param>
	/// <seealso class='IDTExtensibility2' />
	 */
	public void OnConnection(Object application, ext_ConnectMode connectMode, Object addInInst, /** @ref */ System.Array custom)
	{
		_applicationObject = (DTE2)application;
		_addInInstance = (AddIn)addInInst;

		Object programmableObject = null;

		//This guid must be unique for each different tool window,
		// but you may use the same guid for the same tool window.
		//This guid can be used for indexing the windows collection,
		// for example: applicationObject.Windows.Item(guidstr)
		String guidstr = "{a46d518c-f8b3-423c-8005-3801b5d60781}";
		Windows2 windows2 = (EnvDTE80.Windows2)_applicationObject.get_Windows();
		System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
		_windowToolWindow = windows2.CreateToolWindow2(_addInInstance, asm.get_Location(), "VJToolWindow.ToolWindowControl", "J# Tool window", guidstr, programmableObject);

		//Set the picture displayed when the window is tab docked
		_windowToolWindow.SetTabPicture(Resource.get_ToolWindowBmp().GetHbitmap());

		//When using the hosting control, you must set visible to true before calling HostUserControl,
		// otherwise the UserControl cannot be hosted properly.
		_windowToolWindow.set_Visible(true);
	}

	/**
	/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
	/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />
	 */
	public void OnDisconnection(ext_DisconnectMode disconnectMode, /** @ref */ System.Array custom)
	{
	}

	/**
	/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />	
	 */
	public void OnAddInsUpdate(/** @ref */ System.Array custom)
	{
	}

	/**
	/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />
	 */
	public void OnStartupComplete(/** @ref */ System.Array custom)
	{
	}

	/**
	/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />
	 */
	public void OnBeginShutdown(/** @ref */ System.Array custom)
	{
	}

	private DTE2 _applicationObject;
	private AddIn _addInInstance;
	private Window _windowToolWindow;
}
