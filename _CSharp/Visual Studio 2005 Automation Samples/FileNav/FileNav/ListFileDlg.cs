//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EnvDTE;

namespace FileNav
{
	public partial class ListFileDlg : Form
	{
		public ListFileDlg()
		{
			InitializeComponent();
		}

		EnvDTE80.DTE2 _dte;
		public ListFileDlg(EnvDTE80.DTE2 dte)
		{
			InitializeComponent();
			_manuallySettingSelection = false;
			_dte = dte;
			_fileNames = new System.Collections.Generic.List<ProjectItemInfo>();
			foreach (EnvDTE.Project project in _dte.Solution.Projects)
			{
				WalkProject(project.ProjectItems);
			}
		}

		void WalkProject(ProjectItems projectItems)
		{
			if (projectItems == null)
				return;
			foreach (ProjectItem projectItem in projectItems)
			{
				if (projectItem.Kind == EnvDTE.Constants.vsProjectItemKindPhysicalFile)
				{
					_fileNames.Add(new ProjectItemInfo(projectItems.ContainingProject.UniqueName, projectItem.get_FileNames(0)));
				}
				WalkProject(projectItem.ProjectItems);
			}
		}

		class ProjectItemInfo
		{
			internal ProjectItemInfo(string projectUniqueName, string filePath)
			{
				_projectUniqueName = projectUniqueName;
				_filePath = filePath;
			}
			internal string _projectUniqueName;
			internal string _filePath;
		}

		System.Collections.Generic.List<ProjectItemInfo> _fileNames;
		bool _manuallySettingSelection;
		void FillList()
		{
			listView1.Items.Clear();
			foreach (ProjectItemInfo fileName in _fileNames)
			{
				if ((textBox1.Text.Length == 0) || (System.IO.Path.GetFileName(fileName._filePath).ToLower().StartsWith(textBox1.Text.ToLower())))
				{
					ListViewItem item = listView1.Items.Add(System.IO.Path.GetFileName(fileName._filePath));
					item.SubItems.Add(fileName._projectUniqueName);
					item.Tag = fileName;
				}
			}
			if (listView1.Items.Count > 0)
			{
				_manuallySettingSelection = true;
				listView1.Items[0].Selected = true;
				_manuallySettingSelection = false;
			}
			textBox1.Select();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			FillList();
		}

		private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			
		}

		private void textBox1_KeyUp(object sender, KeyEventArgs e)
		{
			FillList();
		}

		private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
		}

		private void listView1_KeyUp(object sender, KeyEventArgs e)
		{
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (listView1.SelectedItems.Count > 0)
			{
				_dte.ItemOperations.OpenFile(((ProjectItemInfo)listView1.SelectedItems[0].Tag)._filePath, EnvDTE.Constants.vsViewKindPrimary);
				this.Close();
			}
		}

		private void listView1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_manuallySettingSelection == false)
			{
				if (listView1.SelectedItems.Count > 0)
				{
					textBox1.Text = listView1.SelectedItems[0].Text;
				}
			}
		}
	}
}