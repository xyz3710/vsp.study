This directory contains unsupported tools to be used with the Visual Studio Object model.

The following items are currently available:
	ExtBrws.dll - This dll is an Add-in that will work in Visual studio 2005 or the Visual Studio Macros IDE. To use it, run regsvr32 on the dll, then load it from the VS or VBA 
			Add-in Manager dialog. Click the item on the Tools, Extensibility Browse menu item to bring up a dialog that displays all the late bound objects
			available under DTE, DTE.Events, or DTE.Properties.