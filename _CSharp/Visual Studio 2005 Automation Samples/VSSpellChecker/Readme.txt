Copyright (c) Microsoft Corporation.  All rights reserved.

An add-in that does spell-checking using Word components and Windows Forms for the UI.

Once you load the sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin VSSpellChecker.Connect
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

Also, note that for the VB version of this sample, because the VB Code Model does not implement CodeParameter.get_DocComment, and the sample just catches the NotImplementedException without throwing any message, you won�t see anything happening if you run this sample against a VB project.