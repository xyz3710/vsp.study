//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VSSpellChecker
{
	public partial class CheckForm : Form
	{
		public CheckForm()
		{
			InitializeComponent();
			_startPosition = 0;
		}

		int _startPosition;
		public CheckForm(int startPosition, int length, string text, string misspelledWord, List<string> replaceWithWords)
		{
			InitializeComponent();
			foreach (string suggestedWord in replaceWithWords)
			{
				suggestionsListBox.Items.Add(suggestedWord);
			}
			errorTextRichTextBox.Text = text;
			errorTextRichTextBox.Select(startPosition, length);
			errorTextRichTextBox.SelectionColor = System.Drawing.Color.Red;
			errorTextRichTextBox.SelectionFont = new Font(errorTextRichTextBox.SelectionFont, FontStyle.Bold | FontStyle.Italic);
			errorTextRichTextBox.Select(startPosition, 0);
			_startPosition = startPosition + length;
		}

		public int NextStartPosition
		{
			get
			{
				return _startPosition;
			}
		}

		string _replaceWith;
		public string ReplaceWith
		{
			get
			{
				return _replaceWith;
			}
		}

		private void suggestionsListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_replaceWith = suggestionsListBox.Text;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			_replaceWith = customTextBox.Text;
		}

    private void suggestionsListBox_DoubleClick(object sender, EventArgs e)
    {
      string text = (string)((ListBox)sender).SelectedItem;
      if (text != null)
      {
        _replaceWith = text;
        DialogResult = DialogResult.OK;
      }
    }
	}
}