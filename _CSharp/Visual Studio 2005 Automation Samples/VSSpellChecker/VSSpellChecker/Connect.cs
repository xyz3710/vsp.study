//Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.IO;
using System.Xml;
using Microsoft.Office.Interop.Word;

namespace VSSpellChecker
{
    /// <summary>The object for implementing an Add-in.</summary>
    /// <seealso class='IDTExtensibility2' />
    public class Connect : IDTExtensibility2, IDTCommandTarget, System.Windows.Forms.IWin32Window
    {
        /// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
        public Connect()
        {
            _ignoredWords = new System.Collections.Generic.List<string>();
        }

        /// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
        /// <param term='application'>Root object of the host application.</param>
        /// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
        /// <param term='addInInst'>Object representing this Add-in.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
        {
            _applicationObject = (DTE2)application;
            _addInInstance = (EnvDTE.AddIn)addInInst;
            if (connectMode == ext_ConnectMode.ext_cm_UISetup)
            {
                //object []contextGUIDS = new object[] { };
                System.Array contextGUIDS = null;
                Commands2 commands = (Commands2)_applicationObject.Commands;
                string toolsMenuName;

                try
                {
                    //If you would like to move the command to a different menu, change the word "Tools" to the 
                    //  English version of the menu. This code will take the culture, append on the name of the menu
                    //  then add the command to that menu. You can find a list of all the top-level menus in the file
                    //  CommandBar.resx.
                    ResourceManager resourceManager = new ResourceManager("VSSpellChecker.CommandBar", Assembly.GetExecutingAssembly());
                    CultureInfo cultureInfo = new System.Globalization.CultureInfo(_applicationObject.LocaleID);
                    string resourceName = String.Concat(cultureInfo.TwoLetterISOLanguageName, "Tools");
                    toolsMenuName = resourceManager.GetString(resourceName);
                }
                catch
                {
                    //We tried to find a localized version of the word Tools, but one was not found.
                    //  Default to the en-US word, which may work for the current culture.
                    toolsMenuName = "Tools";
                }

                //Place the command on the tools menu.
                //Find the MenuBar command bar, which is the top-level command bar holding all the main menu items:
                Microsoft.VisualStudio.CommandBars.CommandBar menuBarCommandBar = ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)["MenuBar"];

                //Find the Tools command bar on the MenuBar command bar:
                CommandBarControl toolsControl = menuBarCommandBar.Controls[toolsMenuName];
                CommandBarPopup toolsPopup = (CommandBarPopup)toolsControl;

                //This try/catch block can be duplicated if you wish to add multiple commands to be handled by your Add-in,
                //  just make sure you also update the QueryStatus/Exec method to include the new command names.
                try
                {
                    //Add a command to the Commands collection:
                    Command command = commands.AddNamedCommand2(_addInInstance, "VSSpellChecker", "VSSpellChecker", "Executes the command for VSSpellChecker", true, 59, ref contextGUIDS, (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled, (int)vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton);

                    //Add a control for the command to the tools menu:
                    if ((command != null) && (toolsPopup != null))
                    {
                        command.AddControl(toolsPopup.CommandBar, 1);
                    }
                }
                catch (System.ArgumentException)
                {
                    //If we are here, then the exception is probably because a command with that name
                    //  already exists. If so there is no need to recreate the command and we can 
                    //  safely ignore the exception.
                }
            }
        }

        /// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
        /// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
        {
        }

        /// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />		
        public void OnAddInsUpdate(ref Array custom)
        {
        }

        /// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnStartupComplete(ref Array custom)
        {
        }

        /// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnBeginShutdown(ref Array custom)
        {
        }

        /// <summary>Implements the QueryStatus method of the IDTCommandTarget interface. This is called when the command's availability is updated</summary>
        /// <param term='commandName'>The name of the command to determine state for.</param>
        /// <param term='neededText'>Text that is needed for the command.</param>
        /// <param term='status'>The state of the command in the user interface.</param>
        /// <param term='commandText'>Text requested by the neededText parameter.</param>
        /// <seealso class='Exec' />
        public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
        {
            if (neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
            {
                if (commandName == "VSSpellChecker.Connect.VSSpellChecker")
                {
                    status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                    return;
                }
            }
        }

        /// <summary>Implements the Exec method of the IDTCommandTarget interface. This is called when the command is invoked.</summary>
        /// <param term='commandName'>The name of the command to execute.</param>
        /// <param term='executeOption'>Describes how the command should be run.</param>
        /// <param term='varIn'>Parameters passed from the caller to the command handler.</param>
        /// <param term='varOut'>Parameters passed from the command handler to the caller.</param>
        /// <param term='handled'>Informs the caller if the command was handled or not.</param>
        /// <seealso class='Exec' />
        public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
        {
            handled = false;
            if (executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault)
            {
                if (commandName == "VSSpellChecker.Connect.VSSpellChecker")
                {
                    SpellCheckProject();
                    System.Windows.Forms.MessageBox.Show("Spell Check Complete!", "VS Spell Check", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                    handled = true;
                    return;
                }
            }
        }

        ApplicationClass _wordObjectModel;
        private ApplicationClass GetWordObjectModel()
        {
            if (_wordObjectModel != null)
            {
                try
                {
                    string version = _wordObjectModel.Version;
                }
                catch
                {
                    _wordObjectModel = null;
                }
            }
            if (_wordObjectModel == null)
            {
                object unused1 = Type.Missing;
                object unused2 = Type.Missing;
                object unused3 = Type.Missing;
                object unused4 = Type.Missing;
                _wordObjectModel = new ApplicationClass();
                _wordObjectModel.Documents.Add(ref unused1, ref unused2, ref unused3, ref unused4);

            }
            return _wordObjectModel;
        }

        private string GetDocCommentFromElement(CodeElement codeElement)
        {
            string docComment = "";
            if (codeElement.Kind == vsCMElement.vsCMElementClass)
            {
                docComment = ((CodeClass)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementEnum)
            {
                docComment = ((CodeEnum)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementFunction)
            {
                docComment = ((CodeFunction)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementInterface)
            {
                docComment = ((CodeInterface)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementStruct)
            {
                docComment = ((CodeStruct)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementVariable)
            {
                docComment = ((CodeVariable)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementDelegate)
            {
                docComment = ((CodeDelegate)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementParameter)
            {
                docComment = ((CodeParameter)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementProperty)
            {
                docComment = ((CodeProperty)codeElement).DocComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementEvent)
            {
                docComment = ((CodeEvent)codeElement).DocComment;
            }

            //work around a bug in VB where the XML is not well formed:
            if (docComment.StartsWith("<doc>") == false)
            {
                docComment = "<doc>" + docComment + "</doc>";
            }
            return docComment;
        }

        private void SetDocCommentOnElement(CodeElement codeElement, string docComment)
        {
            if (codeElement.ProjectItem.ContainingProject.Kind == VSLangProj.PrjKind.prjKindVBProject)
            {
                //Work around a bug in VB where the XML does not use the top-most <doc> tag:
                if (docComment.StartsWith("<doc>"))
                {
                    docComment = docComment.Substring("<doc>".Length);
                    docComment = docComment.TrimEnd(' ', '\t');
                    docComment = docComment.Substring(0, docComment.Length - "</doc>".Length);
                }
            }
            if (codeElement.Kind == vsCMElement.vsCMElementClass)
            {
                ((CodeClass)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementEnum)
            {
                ((CodeEnum)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementFunction)
            {
                ((CodeFunction)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementInterface)
            {
                ((CodeInterface)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementStruct)
            {
                ((CodeStruct)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementVariable)
            {
                ((CodeVariable)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementDelegate)
            {
                ((CodeDelegate)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementParameter)
            {
                ((CodeParameter)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementProperty)
            {
                ((CodeProperty)codeElement).DocComment = docComment;
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementEvent)
            {
                ((CodeEvent)codeElement).DocComment = docComment;
            }
        }

        private string FindLeadingTrailingWhitespace(string input, out string leading, out string trailing)
        {
            leading = "";
            trailing = "";
            for (int i = 0; i < input.Length; i++)
            {
                if ((input[i] == ' ') || (input[i] == '\t') || (input[i] == '\n') || (input[i] == '\r'))
                {
                    leading = leading + input[i].ToString();
                }
                else
                {
                    break;
                }
            }

            if (leading.Length == input.Length)
            {
                return "";
            }

            for (int i = input.Length - 1; i > -1; i--)
            {
                if ((input[i] == ' ') || (input[i] == '\t') || (input[i] == '\n') || (input[i] == '\r'))
                {
                    trailing = input[i].ToString() + trailing;
                }
                else
                {
                    break;
                }
            }

            string toReturn = input.Substring(leading.Length);
            return toReturn.Substring(0, toReturn.Length - trailing.Length);
        }

        private void WalkCodeElements(EnvDTE.CodeElements codeElements)
        {
            foreach (CodeElement codeElement in codeElements)
            {
                string docComment = GetDocCommentFromElement(codeElement);

                if (string.IsNullOrEmpty(docComment) == false)
                {
                    bool needToSave = false;
                    XmlDocument docCommentXml = new XmlDocument();
                    try //Use a try/catch in case the XML is malformed.
                    {
                        docCommentXml.LoadXml(docComment);
                        XmlNodeList dataNodesSummary = docCommentXml.SelectNodes("/doc/summary");
                        XmlNodeList dataNodesRemarks = docCommentXml.SelectNodes("/doc/remarks");
                        XmlNodeList dataNodesParam = docCommentXml.SelectNodes("/doc/param");
                        foreach (XmlNode dataNodeSummary in dataNodesSummary)
                        {
                            string leadingWhitespace = "";
                            string trailingWhitespace = "";
                            string toSpellcheck = FindLeadingTrailingWhitespace(dataNodeSummary.InnerText, out leadingWhitespace, out trailingWhitespace);
                            string rebuiltString = SpellCheckString(toSpellcheck);

                            //System.Windows.Forms.MessageBox.Show("-->" + rebuiltString + "<--");
                            if (rebuiltString != leadingWhitespace + dataNodeSummary.InnerText + trailingWhitespace)
                            {
                                dataNodeSummary.InnerText = leadingWhitespace + rebuiltString + trailingWhitespace;
                                needToSave = true;
                            }
                        }
                        foreach (XmlNode dataNodeRemarks in dataNodesRemarks)
                        {
                            string leadingWhitespace = "";
                            string trailingWhitespace = "";
                            string rebuiltString = SpellCheckString(dataNodeRemarks.InnerText);
                            if (rebuiltString != leadingWhitespace + dataNodeRemarks.InnerText + trailingWhitespace)
                            {
                                dataNodeRemarks.InnerText = leadingWhitespace + rebuiltString + trailingWhitespace;
                                needToSave = true;
                            }
                        }
                        foreach (XmlNode dataNodeParam in dataNodesParam)
                        {
                            string leadingWhitespace = "";
                            string trailingWhitespace = "";
                            string rebuiltString = SpellCheckString(dataNodeParam.InnerText);
                            if (rebuiltString != leadingWhitespace + dataNodeParam.InnerText + trailingWhitespace)
                            {
                                dataNodeParam.InnerText = leadingWhitespace + rebuiltString + trailingWhitespace;
                                needToSave = true;
                            }
                        }
                        if (needToSave)
                        {
                            SetDocCommentOnElement(codeElement, docCommentXml.OuterXml);
                        }
                    }
                    catch (AbortException ae)
                    {
                        throw;
                    }
                    catch
                    {
                    }
                }

                WalkCodeElements(codeElement.Children);
            }
        }

        void WalkControls(System.Windows.Forms.Control control)
        {
            if (control == null)
                return;
            string replacedString = SpellCheckString(control.Text);
            if (replacedString != control.Text)
            {
                control.Text = replacedString;
            }
            if (control.Controls == null)
                return;
            foreach (System.Windows.Forms.Control subControl in control.Controls)
            {
                WalkControls(subControl);
            }
        }

        private void WalkProjectItems(EnvDTE.ProjectItems projectItems)
        {
            foreach (EnvDTE.ProjectItem projectItem in projectItems)
            {
                string fileName = projectItem.get_FileNames(1);
                //Spell check resource files:
                if (String.Compare(System.IO.Path.GetExtension(fileName), ".resx", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    bool needToSave = false;
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(fileName);
                    XmlNodeList dataNodes = xmlDocument.SelectNodes("/root/data");
                    foreach (XmlNode dataNode in dataNodes)
                    {
                        XmlNode typeNode = dataNode.Attributes.GetNamedItem("type");
                        if (typeNode == null) //strings do not have a type associated with them.
                        {
                            XmlNode valueNode = dataNode.SelectSingleNode("value");
                            string rebuiltString = SpellCheckString(valueNode.InnerText);
                            if (rebuiltString != valueNode.InnerText)
                            {
                                needToSave = true;
                                valueNode.InnerText = rebuiltString;
                            }
                        }
                    }
                    if (needToSave)
                    {
                        try
                        {
                            xmlDocument.Save(fileName);
                        }
                        catch
                        {
                            System.Windows.Forms.MessageBox.Show(this, "The file {0} could not be saved, is it read only?", "Spell checking", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                            //TODO: Error message saying that the file could not be saved.
                        }
                    }
                }

                //Spell check doc comments
                FileCodeModel fileCodeModel = projectItem.FileCodeModel;
                if (fileCodeModel != null)
                {
                    WalkCodeElements(fileCodeModel.CodeElements);
                }

                //TODO: Spell check winforms
                if ((Path.GetExtension(fileName).ToLower() == ".cs") ||
                   (Path.GetExtension(fileName).ToLower() == ".vb") ||
                   (Path.GetExtension(fileName).ToLower() == ".jsl"))
                {
                    System.Windows.Forms.Control control = null;
                    System.ComponentModel.Design.IDesignerHost designerHost = null;
                    EnvDTE.Window window = null;
                    bool wasOpen = false;
                    if (_applicationObject.ItemOperations.IsFileOpen(fileName, EnvDTE.Constants.vsViewKindDesigner))
                    {
                        wasOpen = true;
                    }
                    window = projectItem.Open(EnvDTE.Constants.vsViewKindDesigner);
                    try
                    {
                        designerHost = (System.ComponentModel.Design.IDesignerHost)window.Object;
                        control = (System.Windows.Forms.Control)designerHost.RootComponent;
                        WalkControls(control);
                    }
                    catch (AbortException ae)
                    {
                        throw;
                    }
                    catch
                    {
                        //If the item is not a form, then we end up here and can ignore the exception.
                        if (wasOpen == false)
                            window.Close(vsSaveChanges.vsSaveChangesNo);
                    }
                }

                if (projectItem.ProjectItems != null)
                {
                    WalkProjectItems(projectItem.ProjectItems);
                }
            }
        }

        private string SpellCheckString(string stringToCheck)
        {
            object unused1 = Type.Missing;
            object unused2 = Type.Missing;
            object unused3 = Type.Missing;
            object unused4 = Type.Missing;
            object unused5 = Type.Missing;
            object unused6 = Type.Missing;
            object unused7 = Type.Missing;
            object unused8 = Type.Missing;
            object unused9 = Type.Missing;
            object unused10 = Type.Missing;
            object unused11 = Type.Missing;
            object unused12 = Type.Missing;
            object unused13 = Type.Missing;
            SpellingSuggestions suggestedWords = null;

            string[] values = stringToCheck.Split(' ');

            int currentPosition = 0;
            foreach (string value in values)
            {
                if (_ignoredWords.Contains(value) == true)
                {
                    continue;
                }
                if (GetWordObjectModel().CheckSpelling(value, ref unused1, ref unused2, ref unused3, ref unused4, ref unused5, ref unused6, ref unused7, ref unused8, ref unused9, ref unused10, ref unused11, ref unused12) == false)
                {
                    int index = stringToCheck.IndexOf(value, currentPosition);
                    if (index != -1)
                    {
                        System.Collections.Generic.List<string> suggestedWordList = new System.Collections.Generic.List<string>();
                        suggestedWords = GetWordObjectModel().GetSpellingSuggestions(value, ref unused1, ref unused2, ref unused3, ref unused4, ref unused5, ref unused6, ref unused7, ref unused8, ref unused9, ref unused10, ref unused11, ref unused12, ref unused13);
                        foreach (SpellingSuggestion suggestedWord in suggestedWords)
                        {
                            suggestedWordList.Add(suggestedWord.Name);
                        }

                        CheckForm checkForm = new CheckForm(index, value.Length, stringToCheck, value, suggestedWordList);
                        System.Windows.Forms.DialogResult result = checkForm.ShowDialog();
                        if (result == System.Windows.Forms.DialogResult.Ignore) //The ignore button was pressed
                        {
                            //TODO:
                            currentPosition = index + 1;
                        }
                        else if (result == System.Windows.Forms.DialogResult.Abort) //The stop check button was pressed
                        {
                            throw new AbortException();
                        }
                        else if (result == System.Windows.Forms.DialogResult.No) //The ignore all button was pressed
                        {
                            _ignoredWords.Add(value);
                            currentPosition = index + value.Length + 1;
                        }
                        else if ((result == System.Windows.Forms.DialogResult.OK) || (result == System.Windows.Forms.DialogResult.Retry)) //The replace button or use custom button was pressed
                        {
                            string rebuilt = stringToCheck.Substring(0, index);
                            rebuilt += checkForm.ReplaceWith;
                            rebuilt += stringToCheck.Substring(index + value.Length);
                            stringToCheck = rebuilt;
                            currentPosition = index + checkForm.ReplaceWith.Length + 1;
                        }
                    }
                }
            }
            return stringToCheck;
        }

        private void SpellCheckProject()
        {
            foreach (EnvDTE.Project project in _applicationObject.Solution.Projects)
            {
                try
                {
                    WalkProjectItems(project.ProjectItems);
                }
                catch (AbortException ae)
                {
                    return;
                }
            }
        }

        private DTE2 _applicationObject;
        private EnvDTE.AddIn _addInInstance;
        System.Collections.Generic.List<string> _ignoredWords;

        #region IWin32Window Members

        public IntPtr Handle
        {
            get { return new IntPtr(_applicationObject.MainWindow.HWnd); }
        }

        #endregion
    }
    internal class AbortException : Exception
    {
        internal AbortException()
        {
        }
    }
}