//Copyright (c) Microsoft Corporation.  All rights reserved.

namespace VSSpellChecker
{
	partial class CheckForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.errorTextRichTextBox = new System.Windows.Forms.RichTextBox();
      this.suggestionsListBox = new System.Windows.Forms.ListBox();
      this.ignoreButton = new System.Windows.Forms.Button();
      this.ignoreAllButton = new System.Windows.Forms.Button();
      this.replaceButton = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.customTextBox = new System.Windows.Forms.TextBox();
      this.button2 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // errorTextRichTextBox
      // 
      this.errorTextRichTextBox.Location = new System.Drawing.Point(12, 12);
      this.errorTextRichTextBox.Name = "errorTextRichTextBox";
      this.errorTextRichTextBox.Size = new System.Drawing.Size(426, 156);
      this.errorTextRichTextBox.TabIndex = 0;
      this.errorTextRichTextBox.Text = "";
      // 
      // suggestionsListBox
      // 
      this.suggestionsListBox.FormattingEnabled = true;
      this.suggestionsListBox.Location = new System.Drawing.Point(12, 195);
      this.suggestionsListBox.Name = "suggestionsListBox";
      this.suggestionsListBox.Size = new System.Drawing.Size(345, 108);
      this.suggestionsListBox.TabIndex = 1;
      this.suggestionsListBox.DoubleClick += new System.EventHandler(this.suggestionsListBox_DoubleClick);
      this.suggestionsListBox.SelectedIndexChanged += new System.EventHandler(this.suggestionsListBox_SelectedIndexChanged);
      // 
      // ignoreButton
      // 
      this.ignoreButton.DialogResult = System.Windows.Forms.DialogResult.Ignore;
      this.ignoreButton.Location = new System.Drawing.Point(363, 195);
      this.ignoreButton.Name = "ignoreButton";
      this.ignoreButton.Size = new System.Drawing.Size(75, 23);
      this.ignoreButton.TabIndex = 3;
      this.ignoreButton.Text = "&Ignore";
      // 
      // ignoreAllButton
      // 
      this.ignoreAllButton.DialogResult = System.Windows.Forms.DialogResult.No;
      this.ignoreAllButton.Location = new System.Drawing.Point(363, 224);
      this.ignoreAllButton.Name = "ignoreAllButton";
      this.ignoreAllButton.Size = new System.Drawing.Size(75, 23);
      this.ignoreAllButton.TabIndex = 4;
      this.ignoreAllButton.Text = "Ignore &All";
      // 
      // replaceButton
      // 
      this.replaceButton.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.replaceButton.Location = new System.Drawing.Point(363, 253);
      this.replaceButton.Name = "replaceButton";
      this.replaceButton.Size = new System.Drawing.Size(75, 23);
      this.replaceButton.TabIndex = 5;
      this.replaceButton.Text = "&Replace";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 179);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(85, 13);
      this.label1.TabIndex = 6;
      this.label1.Text = "&Suggested fixes:";
      // 
      // button1
      // 
      this.button1.DialogResult = System.Windows.Forms.DialogResult.Abort;
      this.button1.Location = new System.Drawing.Point(363, 282);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 7;
      this.button1.Text = "Stop Check";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(11, 316);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(92, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "Use custom word:";
      // 
      // customTextBox
      // 
      this.customTextBox.Location = new System.Drawing.Point(106, 313);
      this.customTextBox.Name = "customTextBox";
      this.customTextBox.Size = new System.Drawing.Size(251, 20);
      this.customTextBox.TabIndex = 9;
      // 
      // button2
      // 
      this.button2.DialogResult = System.Windows.Forms.DialogResult.Retry;
      this.button2.Location = new System.Drawing.Point(363, 311);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(75, 23);
      this.button2.TabIndex = 10;
      this.button2.Text = "Use Custom";
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // CheckForm
      // 
      this.AcceptButton = this.replaceButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.ignoreButton;
      this.ClientSize = new System.Drawing.Size(450, 345);
      this.ControlBox = false;
      this.Controls.Add(this.button2);
      this.Controls.Add(this.customTextBox);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.replaceButton);
      this.Controls.Add(this.ignoreAllButton);
      this.Controls.Add(this.ignoreButton);
      this.Controls.Add(this.suggestionsListBox);
      this.Controls.Add(this.errorTextRichTextBox);
      this.Name = "CheckForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Spell check";
      this.ResumeLayout(false);
      this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox errorTextRichTextBox;
		private System.Windows.Forms.ListBox suggestionsListBox;
		private System.Windows.Forms.Button ignoreButton;
		private System.Windows.Forms.Button ignoreAllButton;
		private System.Windows.Forms.Button replaceButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox customTextBox;
		private System.Windows.Forms.Button button2;
	}
}