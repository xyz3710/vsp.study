//Copyright (c) Microsoft Corporation.  All rights reserved.

// Connect.h : Declaration of the CConnect

#pragma once
#include "resource.h"       // main symbols

using namespace AddInDesignerObjects;
using namespace EnvDTE;
using namespace EnvDTE80;

/// <summary>The object for implementing an Add-in.</summary>
/// <seealso class='IDTExtensibility2' />
class ATL_NO_VTABLE CConnect : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CConnect, &CLSID_Connect>,

	public IDispatchImpl<_IDTExtensibility2, &IID__IDTExtensibility2, &LIBID_AddInDesignerObjects, 1, 0>
{
public:
	/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
	CConnect()
	{
	}

	DECLARE_REGISTRY_RESOURCEID(IDR_ADDIN)
	DECLARE_NOT_AGGREGATABLE(CConnect)


	BEGIN_COM_MAP(CConnect)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IDTExtensibility2)
	END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease() 
	{
	}

public:
	//IDTExtensibility2 implementation:

	/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
	/// <param term='application'>Root object of the host application.</param>
	/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
	/// <param term='addInInst'>Object representing this Add-in.</param>
	/// <seealso class='IDTExtensibility2' />
	STDMETHOD(OnConnection)(IDispatch * Application, ext_ConnectMode ConnectMode, IDispatch *AddInInst, SAFEARRAY **custom);

	/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
	/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />
	STDMETHOD(OnDisconnection)(ext_DisconnectMode RemoveMode, SAFEARRAY **custom );

	/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />	
	STDMETHOD(OnAddInsUpdate)(SAFEARRAY **custom );

	/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />
	STDMETHOD(OnStartupComplete)(SAFEARRAY **custom );

	/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
	/// <param term='custom'>Array of parameters that are host application specific.</param>
	/// <seealso class='IDTExtensibility2' />
	STDMETHOD(OnBeginShutdown)(SAFEARRAY **custom );



private:

	class SelectionEventsSink : public IDispEventImpl<1, SelectionEventsSink, &__uuidof(EnvDTE::_dispSelectionEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(SelectionEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSelectionEvents), 1, OnChange)
		END_SINK_MAP()

		void __stdcall OnChange()
		{
			CComPtr<EnvDTE::_DTE> pDTE;
			CComPtr<EnvDTE::SelectedItems> pSelectedItems;
			long lCount, i;
			m_pOutputWindowPane->get_DTE(&pDTE);
			pDTE->get_SelectedItems(&pSelectedItems);
			pSelectedItems->get_Count(&lCount);
			m_pOutputWindowPane->OutputString(CComBSTR(L"SelectionEvents::OnChange\n"));
			for (i = 1 ; i <= lCount ; i++)
			{
				CComBSTR bstrName;
				CComPtr<EnvDTE::SelectedItem> pSelectedItem;
				pSelectedItems->Item(CComVariant(i), &pSelectedItem);
				if(pSelectedItem)
				{
					pSelectedItem->get_Name(&bstrName);
					m_pOutputWindowPane->OutputString(CComBSTR(L"\tSelected item name: "));
					m_pOutputWindowPane->OutputString(bstrName);
					m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
				}
			}
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class FindEventsSink : public IDispEventImpl<1, FindEventsSink, &__uuidof(EnvDTE::_dispFindEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(FindEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispFindEvents), 1, FindDone)
		END_SINK_MAP()

		void FindDone(EnvDTE::vsFindResult Result, VARIANT_BOOL Cancelled)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"FindEvents::FindDone\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class OutputWindowEventsSink : public IDispEventImpl<1, OutputWindowEventsSink, &__uuidof(EnvDTE::_dispOutputWindowEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(OutputWindowEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispOutputWindowEvents), 1, PaneAdded)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispOutputWindowEvents), 2, PaneUpdated)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispOutputWindowEvents), 3, PaneClearing)
		END_SINK_MAP()


		void __stdcall PaneAdded (EnvDTE::OutputWindowPane *pPane)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"OutputWindowEvents::PaneAdded\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tPane name:"));
			pPane->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}


		void __stdcall PaneUpdated (EnvDTE::OutputWindowPane *pPane)
		{
			//Do not want to call this. If we call OutputString, then it causes a PaneUpdated event.
			//This PaneUpdated event causes us to call OutputString again, getting us into an infinite
			//loop.
			//m_pOutputWindowPane->OutputString(CComBSTR(L"OutputWindowEvents::PaneUpdated\n"));
		}


		void __stdcall PaneClearing (EnvDTE::OutputWindowPane *pPane)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"OutputWindowEvents::PaneClearing\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tPane name:"));
			pPane->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class WindowEventsSink : public IDispEventImpl<1, WindowEventsSink, &__uuidof(EnvDTE::_dispWindowEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(WindowEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispWindowEvents), 1, WindowClosing)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispWindowEvents), 2, WindowMoved)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispWindowEvents), 3, WindowActivated)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispWindowEvents), 4, WindowCreated)
		END_SINK_MAP()

		void __stdcall WindowClosing (EnvDTE::Window *Window)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"WindowEvents::WindowClosing\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow caption: "));
			Window->get_Caption(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall WindowMoved (EnvDTE::Window *Window, long Top, long Left, long Width, long Height)
		{
			CComBSTR bstrName;
			TCHAR szLoc[100];
			m_pOutputWindowPane->OutputString(CComBSTR(L"WindowEvents::WindowMoved\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow caption: "));
			Window->get_Caption(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow location: "));
			wsprintf(szLoc, _T("(%d X %d , %d X %d)"), Top, Left, Width, Height);
			m_pOutputWindowPane->OutputString(CComBSTR(szLoc));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall WindowActivated (EnvDTE::Window *GotFocus, EnvDTE::Window *LostFocus)
		{
			CComBSTR bstrName;
			CComBSTR bstrName2;
			m_pOutputWindowPane->OutputString(CComBSTR(L"WindowEvents::WindowActivated\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow activated: "));
			GotFocus->get_Caption(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow losing focus: "));
			LostFocus->get_Caption(&bstrName2);
			m_pOutputWindowPane->OutputString(bstrName2);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall WindowCreated (EnvDTE::Window *Window)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"WindowEvents::WindowCreated\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow caption: "));
			Window->get_Caption(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class TextEditorEventsSink : public IDispEventImpl<1, TextEditorEventsSink, &__uuidof(EnvDTE::_dispTextEditorEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(TextEditorEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTextEditorEvents), 1, LineChanged)
		END_SINK_MAP()

		void __stdcall LineChanged(EnvDTE::TextPoint *StartPoint, EnvDTE::TextPoint *EndPoint, long Hint)
		{
			TCHAR szLoc[100];
			long x1, x2, y1, y2;
			CComBSTR bstrName;
			CComPtr<EnvDTE::TextDocument> pTextDocument;
			CComPtr<EnvDTE::Document> pDocument;
			StartPoint->get_Parent(&pTextDocument);
			pTextDocument->get_Parent(&pDocument);

			m_pOutputWindowPane->OutputString(CComBSTR(L"TextEditorEvents::LineChanged\n"));
			pDocument->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument Name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			StartPoint->get_DisplayColumn(&x1);
			StartPoint->get_Line(&y1);
			EndPoint->get_DisplayColumn(&x2);
			EndPoint->get_Line(&y2);
			wsprintf(szLoc, _T("(%d X %d , %d X %d)"), x1, y1, x2, y2);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tLocation: "));
			m_pOutputWindowPane->OutputString(CComBSTR(szLoc));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			if(Hint & EnvDTE::vsTextChangedMultiLine)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tHint: vsTextChangedMultiLine\n"));
			if(Hint & EnvDTE::vsTextChangedSave)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tHint: vsTextChangedSave\n"));
			if(Hint & EnvDTE::vsTextChangedCaretMoved)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tHint: vsTextChangedCaretMoved\n"));
			if(Hint & EnvDTE::vsTextChangedReplaceAll)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tHint: vsTextChangedReplaceAll\n"));
			if(Hint & EnvDTE::vsTextChangedNewline)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tHint: vsTextChangedNewline\n"));
			if(Hint & EnvDTE::vsTextChangedFindStarting)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tHint: vsTextChangedFindStarting\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class DocumentEventsSink : public IDispEventImpl<1, DocumentEventsSink, &__uuidof(EnvDTE::_dispDocumentEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(DocumentEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDocumentEvents), 1, DocumentSaved)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDocumentEvents), 2, DocumentClosing)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDocumentEvents), 3, DocumentOpening)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDocumentEvents), 4, DocumentOpened)
		END_SINK_MAP()

		void __stdcall DocumentSaved(EnvDTE::Document *Document)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"DocumentEvents::DocumentSaved\n"));
			Document->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall DocumentClosing(EnvDTE::Document *Document)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"DocumentEvents::DocumentClosing\n"));
			Document->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall DocumentOpening(BSTR DocumentPath, VARIANT_BOOL ReadOnly)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"DocumentEvents::DocumentOpening\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument path: "));
			m_pOutputWindowPane->OutputString(DocumentPath);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			if(ReadOnly == VARIANT_TRUE)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument read only: True\n"));
			else
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument read only: False\n"));
		}

		void __stdcall DocumentOpened(EnvDTE::Document *Document)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"DocumentEvents::DocumentOpened\n"));
			Document->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tDocument name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class SolutionEventsSink : public IDispEventImpl<1, SolutionEventsSink, &__uuidof(EnvDTE::_dispSolutionEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(SolutionEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 1, Opened)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 2, BeforeClosing)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 3, AfterClosing)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 4, QueryCloseSolution)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 5, Renamed)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 6, ProjectAdded)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 7, ProjectRemoved)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispSolutionEvents), 8, ProjectRenamed)
		END_SINK_MAP()

		void __stdcall Opened()
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::Opened\n"));
		}

		void __stdcall BeforeClosing()
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::BeforeClosing\n"));
		}

		void __stdcall AfterClosing()
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::AfterClosing\n"));
		}

		void __stdcall QueryCloseSolution(VARIANT_BOOL *fCancel)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::QueryCloseSolution\n"));
		}

		void __stdcall Renamed(BSTR OldName)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::Renamed\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tSolution old name: "));
			m_pOutputWindowPane->OutputString(OldName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall ProjectAdded(EnvDTE::Project *Project)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::ProjectAdded\n"));
			Project->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall ProjectRemoved(EnvDTE::Project *Project)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::ProjectRemoved\n"));
			Project->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall ProjectRenamed(EnvDTE::Project *Project, BSTR OldName)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"SolutionEvents::ProjectRenamed\n"));
			Project->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject name: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject old name: "));
			m_pOutputWindowPane->OutputString(OldName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class BuildEventsSink : public IDispEventImpl<1, BuildEventsSink, &__uuidof(EnvDTE::_dispBuildEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(BuildEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispBuildEvents), 3, OnBuildBegin)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispBuildEvents), 4, OnBuildDone)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispBuildEvents), 5, OnBuildProjConfigBegin)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispBuildEvents), 6, OnBuildProjConfigDone)
		END_SINK_MAP()

		void __stdcall OnBuildBegin(EnvDTE::vsBuildScope Scope, EnvDTE::vsBuildAction Action)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"BuildEvents::OnBuildBegin\n"));
			if(Scope == EnvDTE::vsBuildScopeSolution)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild scope: vsBuildScopeSolution \n"));
			else if(Scope == EnvDTE::vsBuildScopeBatch)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild scope: vsBuildScopeBatch\n"));
			else if(Scope == EnvDTE::vsBuildScopeProject)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild scope: vsBuildScopeProject\n"));

			if(Action == EnvDTE::vsBuildActionBuild)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionBuild\n"));
			else if(Action == EnvDTE::vsBuildActionRebuildAll)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionRebuildAll\n"));
			else if(Action == EnvDTE::vsBuildActionClean)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionClean\n"));
			else if(Action == EnvDTE::vsBuildActionDeploy)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionDeploy\n"));
		}

		void __stdcall OnBuildDone(EnvDTE::vsBuildScope Scope, EnvDTE::vsBuildAction Action)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"BuildEvents::OnBuildDone\n"));
			if(Scope == EnvDTE::vsBuildScopeSolution)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild scope: vsBuildScopeSolution \n"));
			else if(Scope == EnvDTE::vsBuildScopeBatch)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild scope: vsBuildScopeBatch\n"));
			else if(Scope == EnvDTE::vsBuildScopeProject)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild scope: vsBuildScopeProject\n"));

			if(Action == EnvDTE::vsBuildActionBuild)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionBuild\n"));
			else if(Action == EnvDTE::vsBuildActionRebuildAll)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionRebuildAll\n"));
			else if(Action == EnvDTE::vsBuildActionClean)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionClean\n"));
			else if(Action == EnvDTE::vsBuildActionDeploy)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tBuild action: vsBuildActionDeploy\n"));
		}

		void __stdcall OnBuildProjConfigBegin(BSTR Project, BSTR ProjectConfig, BSTR Platform, BSTR SolutionConfig)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"BuildEvents::OnBuildProjConfigBegin\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject name: "));
			m_pOutputWindowPane->OutputString(Project);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject configuration: "));
			m_pOutputWindowPane->OutputString(ProjectConfig);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tPlatform: "));
			m_pOutputWindowPane->OutputString(Platform);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tSolution configurations: "));
			m_pOutputWindowPane->OutputString(SolutionConfig);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall OnBuildProjConfigDone(BSTR Project, BSTR ProjectConfig, BSTR Platform, BSTR SolutionConfig, VARIANT_BOOL Success)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"BuildEvents::OnBuildProjConfigDone\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject name: "));
			m_pOutputWindowPane->OutputString(Project);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tProject configuration: "));
			m_pOutputWindowPane->OutputString(ProjectConfig);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tPlatform: "));
			m_pOutputWindowPane->OutputString(Platform);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			m_pOutputWindowPane->OutputString(CComBSTR(L"\tSolution configurations: "));
			m_pOutputWindowPane->OutputString(SolutionConfig);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));

			if(Success == VARIANT_TRUE)
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tSuccess: True\n"));
			else
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tSuccess: False\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class CommandEventsSink : public IDispEventImpl<1, CommandEventsSink, &__uuidof(EnvDTE::_dispCommandEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(CommandEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispCommandEvents), 1, BeforeExecute)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispCommandEvents), 2, AfterExecute)
		END_SINK_MAP()

		void __stdcall BeforeExecute (BSTR Guid, long ID, VARIANT CustomIn, VARIANT CustomOut, VARIANT_BOOL *CancelDefault)
		{
			CComBSTR bstrName;
			CComPtr<EnvDTE::Commands> pCommands;
			CComPtr<EnvDTE::_DTE> pDTE;
			CComPtr<EnvDTE::Command> pCommand;
			m_pOutputWindowPane->get_DTE(&pDTE);
			pDTE->get_Commands(&pCommands);
			pCommands->Item(CComVariant(Guid), ID, &pCommand);

			m_pOutputWindowPane->OutputString(CComBSTR(L"CommandEvents::BeforeExecute\n"));
			pCommand->get_Name(&bstrName);
			if(bstrName.Length() > 0)
			{
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tCommand name: "));
				m_pOutputWindowPane->OutputString(bstrName);
				m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			}
			else
			{
				TCHAR szID[20];
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tCommand guid: "));
				m_pOutputWindowPane->OutputString(Guid);
				m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
				wsprintf(szID, _T("%d"), ID);
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tCommand ID: "));
				m_pOutputWindowPane->OutputString(CComBSTR(szID));
				m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			}
		}

		void __stdcall AfterExecute (BSTR Guid, long ID, VARIANT CustomIn, VARIANT CustomOut)
		{
			CComBSTR bstrName;
			CComPtr<EnvDTE::Commands> pCommands;
			CComPtr<EnvDTE::_DTE> pDTE;
			CComPtr<EnvDTE::Command> pCommand;
			m_pOutputWindowPane->get_DTE(&pDTE);
			pDTE->get_Commands(&pCommands);
			pCommands->Item(CComVariant(Guid), ID, &pCommand);

			m_pOutputWindowPane->OutputString(CComBSTR(L"CommandEvents::AfterExecute\n"));
			pCommand->get_Name(&bstrName);
			if(bstrName.Length() > 0)
			{
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tCommand name: "));
				m_pOutputWindowPane->OutputString(bstrName);
				m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			}
			else
			{
				TCHAR szID[20];
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tCommand guid: "));
				m_pOutputWindowPane->OutputString(Guid);
				m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
				wsprintf(szID, _T("%d"), ID);
				m_pOutputWindowPane->OutputString(CComBSTR(L"\tCommand ID: "));
				m_pOutputWindowPane->OutputString(CComBSTR(szID));
				m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
			}
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class TaskListEventSink : public IDispEventImpl<1, TaskListEventSink, &__uuidof(EnvDTE::_dispTaskListEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(TaskListEventSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTaskListEvents), 1, OnTaskAdded)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTaskListEvents), 2, OnTaskRemoved)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTaskListEvents), 3, OnTaskModified)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispTaskListEvents), 4, OnTaskNavigated)
		END_SINK_MAP()
	public:

		void __stdcall OnTaskNavigated(EnvDTE::TaskItem *pTaskItem, VARIANT_BOOL *NavigateHandled)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"TaskListEvents::OnTaskNavigated\n"));
		}

		void __stdcall OnTaskRemoved(EnvDTE::TaskItem *pTaskItem)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"TaskListEvents::OnTaskRemoved\n"));
		}

		void __stdcall OnTaskAdded (EnvDTE::TaskItem *TaskItem)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"TaskListEvents::OnTaskAdded\n"));
		}

		void __stdcall OnTaskModified (EnvDTE::TaskItem *TaskItem, EnvDTE::vsTaskListColumn ColumnModified)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"TaskListEvents::OnTaskModified\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class DTEEventsSink : public IDispEventImpl<1, DTEEventsSink, &__uuidof(EnvDTE::_dispDTEEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(DTEEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDTEEvents), 1, OnStartupComplete)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDTEEvents), 2, OnBeginShutdown)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDTEEvents), 3, ModeChanged)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDTEEvents), 4, OnMacrosRuntimeReset)
		END_SINK_MAP()
	public:
		void __stdcall OnStartupComplete()
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"DTEEvents::OnStartupComplete\n"));
		}

		void __stdcall OnBeginShutdown()
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"DTEEvents::OnBeginShutdown\n"));
		}

		void __stdcall ModeChanged(EnvDTE::vsIDEMode LastMode)
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"DTEEvents::ModeChanged\n"));
		}

		void __stdcall OnMacrosRuntimeReset()
		{
			m_pOutputWindowPane->OutputString(CComBSTR(L"DTEEvents::OnMacrosRuntimeReset\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class WindowVisibilityEventSink : public IDispEventImpl<1, WindowVisibilityEventSink, &__uuidof(EnvDTE80::_dispWindowVisibilityEvents), &EnvDTE80::LIBID_EnvDTE80, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(WindowVisibilityEventSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispWindowVisibilityEvents), 1, WindowHiding)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispWindowVisibilityEvents), 2, WindowShowing)
		END_SINK_MAP()
	public:

		void __stdcall WindowHiding(EnvDTE::Window *pWindow)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"WindowVisibilityEvents::WindowHiding\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow hiding: "));
			pWindow->get_Caption(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void __stdcall WindowShowing(EnvDTE::Window *pWindow)
		{
			CComBSTR bstrName;
			m_pOutputWindowPane->OutputString(CComBSTR(L"WindowVisibilityEvents::WindowShowing\n"));
			m_pOutputWindowPane->OutputString(CComBSTR(L"\tWindow showing: "));
			pWindow->get_Caption(&bstrName);
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR(L"\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class TextDocumentKeyPressEventSink : public IDispEventImpl<1, TextDocumentKeyPressEventSink, &__uuidof(EnvDTE80::_dispTextDocumentKeyPressEvents), &EnvDTE80::LIBID_EnvDTE80, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(TextDocumentKeyPressEventSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispTextDocumentKeyPressEvents), 1, BeforeKeyPress)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispTextDocumentKeyPressEvents), 2, AfterKeyPress)
		END_SINK_MAP()
	public:


		void __stdcall BeforeKeyPress(BSTR Keypress, TextSelection *Selection, VARIANT_BOOL InStatementCompletion, VARIANT_BOOL *CancelKeypress)
		{
			*CancelKeypress = VARIANT_FALSE;
			CComBSTR bstrSelectionText;
			Selection->get_Text(&bstrSelectionText);

			m_pOutputWindowPane->OutputString(CComBSTR("TextDocumentKeyPressEvents::BeforeKeyPress\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tKey: "));
			m_pOutputWindowPane->OutputString(CComBSTR(Keypress));
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tSelection: "));
			m_pOutputWindowPane->OutputString(bstrSelectionText);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tInStatementCompletion: "));
			m_pOutputWindowPane->OutputString(CComBSTR(InStatementCompletion));
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall AfterKeyPress(BSTR Keypress, TextSelection *Selection, VARIANT_BOOL InStatementCompletion)
		{
			CComBSTR bstrSelectionText;
			Selection->get_Text(&bstrSelectionText);

			m_pOutputWindowPane->OutputString(CComBSTR("TextDocumentKeyPressEvents::AfterKeyPress\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tKey: "));
			m_pOutputWindowPane->OutputString(Keypress);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tSelection: "));
			m_pOutputWindowPane->OutputString(bstrSelectionText);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tInStatementCompletion: "));
			m_pOutputWindowPane->OutputString(CComBSTR(InStatementCompletion));
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};



	class DebuggerEventsSink : public IDispEventImpl<1, DebuggerEventsSink, &__uuidof(EnvDTE::_dispDebuggerEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(DebuggerEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDebuggerEvents), 1, OnEnterRunMode)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDebuggerEvents), 2, OnEnterDesignMode)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDebuggerEvents), 3, OnEnterBreakMode)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDebuggerEvents), 4, OnExceptionThrown)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDebuggerEvents), 5, OnExceptionNotHandled)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispDebuggerEvents), 6, OnContextChanged)
		END_SINK_MAP()

		void __stdcall OnEnterRunMode(dbgEventReason Reason)
		{
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerEvents::OnEnterRunMode\n"));
		}

		void __stdcall OnEnterDesignMode(dbgEventReason Reason)
		{
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerEvents::OnEnterDesignMode\n"));
		}

		void __stdcall OnEnterBreakMode(dbgEventReason Reason, dbgExecutionAction* ExecutionAction)
		{
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerEvents::OnEnterBreakMode\n"));
		}

		void __stdcall OnExceptionThrown(BSTR ExceptionType, BSTR Name, long Code, BSTR Description, dbgExceptionAction* ExceptionAction)
		{
			*ExceptionAction = EnvDTE::dbgExceptionAction::dbgExceptionActionDefault;
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerEvents::OnExceptionThrown\n"));
		}

		void __stdcall OnExceptionNotHandled(BSTR ExceptionType, BSTR Name, long Code, BSTR Description, dbgExceptionAction* ExceptionAction)
		{
			*ExceptionAction = EnvDTE::dbgExceptionAction::dbgExceptionActionDefault;
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerEvents::OnExceptionNotHandled\n"));
		}

		void __stdcall OnContextChanged(Process* NewProcess, Program* NewProgram, Thread* NewThread, StackFrame* NewStackFrame)
		{
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerEvents::OnContextChanged\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class ProjectItemsEventsSink : public IDispEventImpl<1, ProjectItemsEventsSink, &__uuidof(EnvDTE::_dispProjectItemsEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(ProjectItemsEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispProjectItemsEvents), 1, ItemAdded)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispProjectItemsEvents), 2, ItemRemoved)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispProjectItemsEvents), 4, ItemRenamed)
		END_SINK_MAP()

		void __stdcall ItemAdded(ProjectItem *pProjectItem )
		{
			CComBSTR bstrFileName;
			pProjectItem->get_FileNames(0, &bstrFileName);
			m_pOutputWindowPane->OutputString(CComBSTR("ProjectItemsEvents::ItemAdded\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProject Item: "));
			m_pOutputWindowPane->OutputString(bstrFileName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall ItemRemoved(ProjectItem *pProjectItem )
		{
			CComBSTR bstrFileName;
			pProjectItem->get_FileNames(0, &bstrFileName);
			m_pOutputWindowPane->OutputString(CComBSTR("ProjectItemsEvents::ItemRemoved\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProject Item: "));
			m_pOutputWindowPane->OutputString(bstrFileName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall ItemRenamed(ProjectItem *pProjectItem , BSTR OldName)
		{
			CComBSTR bstrFileName;
			pProjectItem->get_FileNames(0, &bstrFileName);
			m_pOutputWindowPane->OutputString(CComBSTR("ProjectItemsEvents::ItemRenamed\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProject Item: "));
			m_pOutputWindowPane->OutputString(bstrFileName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tOld Name: "));
			m_pOutputWindowPane->OutputString(OldName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class ProjectsEventsSink : public IDispEventImpl<1, ProjectsEventsSink, &__uuidof(EnvDTE::_dispProjectsEvents), &EnvDTE::LIBID_EnvDTE, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(ProjectsEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispProjectsEvents), 1, ItemAdded)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispProjectsEvents), 2, ItemRemoved)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE::_dispProjectsEvents), 4, ItemRenamed)
		END_SINK_MAP()

		void __stdcall ItemAdded(Project *pProject)
		{
			CComBSTR bstrName;
			pProject->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR("ProjectsEvents::ItemAdded\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProject: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall ItemRemoved(Project *pProject)
		{
			CComBSTR bstrName;
			pProject->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR("ProjectsEvents::ItemRemoved\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProject: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall ItemRenamed(Project *pProject, BSTR OldName)
		{
			CComBSTR bstrName;
			pProject->get_Name(&bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR("ProjectsEvents::ItemRenamed\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProject: "));
			m_pOutputWindowPane->OutputString(bstrName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tOld Name: "));
			m_pOutputWindowPane->OutputString(OldName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class CodeModelEventsSink : public IDispEventImpl<1, CodeModelEventsSink, &__uuidof(EnvDTE80::_dispCodeModelEvents), &EnvDTE80::LIBID_EnvDTE80, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(CodeModelEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispCodeModelEvents), 1, ElementAdded)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispCodeModelEvents), 2, ElementChanged)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispCodeModelEvents), 4, ElementDeleted)
		END_SINK_MAP()

		void __stdcall ElementAdded(CodeElement *Element)
		{
			CComBSTR bstrFullName;
			Element->get_FullName(&bstrFullName);
			m_pOutputWindowPane->OutputString(CComBSTR("CodeModelEvents::ElementAdded\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tElement: "));
			m_pOutputWindowPane->OutputString(bstrFullName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall ElementChanged(CodeElement *Element, vsCMChangeKind Change)
		{
			CComBSTR bstrFullName;
			Element->get_FullName(&bstrFullName);

			m_pOutputWindowPane->OutputString(CComBSTR("CodeModelEvents::ElementChanged\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tElement: "));
			m_pOutputWindowPane->OutputString(bstrFullName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tChange: "));
			CComVariant varChange(Change);
			varChange.ChangeType(VT_BSTR);
			m_pOutputWindowPane->OutputString(varChange.bstrVal);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void __stdcall ElementDeleted(IDispatch *Parent, CodeElement *Element)
		{
			CComBSTR bstrFullName;
			Element->get_FullName(&bstrFullName);
			CComQIPtr<CodeElement> pCodeElementParent(Parent);
			CComQIPtr<ProjectItem> pProjectItemParent(Parent);

			m_pOutputWindowPane->OutputString(CComBSTR("CodeModelEvents::ElementDeleted\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tElement: "));
			m_pOutputWindowPane->OutputString(bstrFullName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));

			if(pCodeElementParent)
			{
				CComBSTR bstrElementFullName;
				pCodeElementParent->get_FullName(&bstrElementFullName);
				m_pOutputWindowPane->OutputString(CComBSTR("\tParent: "));
				m_pOutputWindowPane->OutputString(bstrElementFullName);
				m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			}
			else if (pProjectItemParent)
			{
				CComBSTR bstrFileName;
				pProjectItemParent->get_FileNames(0, &bstrFileName);
				m_pOutputWindowPane->OutputString(CComBSTR("\tParent: "));
				m_pOutputWindowPane->OutputString(bstrFileName);
				m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			}
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};


	class DebuggerProcessEventsSink : public IDispEventImpl<1, DebuggerProcessEventsSink, &__uuidof(EnvDTE80::_dispDebuggerProcessEvents), &EnvDTE80::LIBID_EnvDTE80, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(DebuggerProcessEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispDebuggerProcessEvents), 1, OnProcessStateChanged)
		END_SINK_MAP()

		void __stdcall OnProcessStateChanged(Process* NewProcess, dbgProcessState processState)
		{
			CComBSTR bstrProcessName;
			NewProcess->get_Name(&bstrProcessName);
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerProcessEvents::OnProcessStateChanged\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tNew Process: "));
			m_pOutputWindowPane->OutputString(bstrProcessName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProcess State: "));
			CComVariant varProcessState(processState);
			varProcessState.ChangeType(VT_BSTR);
			m_pOutputWindowPane->OutputString(varProcessState.bstrVal);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class DebuggerExpressionEvaluationEventsSink : public IDispEventImpl<1, DebuggerExpressionEvaluationEventsSink, &__uuidof(EnvDTE80::_dispDebuggerExpressionEvaluationEvents), &EnvDTE80::LIBID_EnvDTE80, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(DebuggerExpressionEvaluationEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispDebuggerExpressionEvaluationEvents), 1, OnExpressionEvaluation)
		END_SINK_MAP()

		void __stdcall OnExpressionEvaluation(Process * pProcess, Thread* pThread, dbgExpressionEvaluationState evaluationState)
		{
			CComBSTR bstrProcessName;
			CComBSTR bstrThreadName;
			pProcess->get_Name(&bstrProcessName);
			pThread->get_Name(&bstrThreadName);
			m_pOutputWindowPane->OutputString(CComBSTR("DebuggerExpressionEvaluationEvents, OnExpressionEvaluation\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tProcess: "));
			m_pOutputWindowPane->OutputString(bstrProcessName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tThread: "));
			m_pOutputWindowPane->OutputString(bstrThreadName);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tExpression Evaluation State: "));
			CComVariant varEvaluationState(evaluationState);
			varEvaluationState.ChangeType(VT_BSTR);
			m_pOutputWindowPane->OutputString(varEvaluationState.bstrVal);
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};

	class PublishEventsSink : public IDispEventImpl<1, PublishEventsSink, &__uuidof(EnvDTE80::_dispPublishEvents), &EnvDTE80::LIBID_EnvDTE80, 8, 0>
	{
	public:
		BEGIN_SINK_MAP(PublishEventsSink)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispPublishEvents), 1, OnPublishBegin)
			SINK_ENTRY_EX(1, __uuidof(EnvDTE80::_dispPublishEvents), 2, OnPublishDone)
		END_SINK_MAP()

		void __stdcall OnPublishBegin(VARIANT_BOOL *Continue)
		{
			m_pOutputWindowPane->OutputString(CComBSTR("PublishEvents::OnPublishBegin\n"));
			*Continue = VARIANT_TRUE;
		}

		void __stdcall OnPublishDone(VARIANT_BOOL Success)
		{
			m_pOutputWindowPane->OutputString(CComBSTR("PublishEvents::OnPublishDone\n"));
			m_pOutputWindowPane->OutputString(CComBSTR("\tSuccess: "));
			if(Success == VARIANT_TRUE)
				m_pOutputWindowPane->OutputString(CComBSTR("true"));
			else
				m_pOutputWindowPane->OutputString(CComBSTR("false"));
			m_pOutputWindowPane->OutputString(CComBSTR("\n"));
		}

		void SetOutputWindow(CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane)
		{
			m_pOutputWindowPane = pOutputWindowPane;
		}

		CComPtr<EnvDTE::OutputWindowPane> m_pOutputWindowPane;
	};



	CComPtr<DTE2> m_pDTE;
	CComPtr<AddIn> m_pAddInInstance;
	TaskListEventSink m_TaskListEventSink;
	DTEEventsSink m_DTEEventsSink;
	CommandEventsSink m_CommandEventsSink;
	SelectionEventsSink m_SelectionEventsSink;
	BuildEventsSink m_BuildEventsSink;
	SolutionEventsSink m_SolutionEventsSink;
	DocumentEventsSink m_DocumentEventsSink;
	TextEditorEventsSink m_TextEditorEventsSink;
	WindowEventsSink m_WindowEventsSink;
	OutputWindowEventsSink m_OutputWindowEventsSink;
	FindEventsSink m_FindEventsSink;
	WindowVisibilityEventSink m_WindowVisibilityEventSink;
	TextDocumentKeyPressEventSink m_TextDocumentKeyPressEventSink;
	DebuggerEventsSink m_DebuggerEventsSink;
	CodeModelEventsSink m_CodeModelEventsSink;
	DebuggerProcessEventsSink m_DebuggerProcessEventsSink;
	DebuggerExpressionEvaluationEventsSink m_DebuggerExpressionEvaluationEventsSink;
	PublishEventsSink m_PublishEventsSink;

	ProjectItemsEventsSink m_MiscFilesEventsSink;
	ProjectItemsEventsSink m_SolutionItemsEventsSink;
	ProjectItemsEventsSink m_GlobalProjectItemsEventsSink;
	ProjectsEventsSink m_GlobalProjectsEventsSink;

	CComPtr<EnvDTE::_TaskListEvents> pTaskListEvents;
	CComPtr<EnvDTE::_DTEEvents> pDTEEvents;
	CComPtr<EnvDTE::_CommandEvents> pCommandEvents;
	CComPtr<EnvDTE::_SelectionEvents> pSelectionEvents;
	CComPtr<EnvDTE::_BuildEvents> pBuildEvents;
	CComPtr<EnvDTE::_SolutionEvents> pSolutionEvents;
	CComPtr<EnvDTE::_DocumentEvents> pDocumentEvents;
	CComPtr<EnvDTE::_TextEditorEvents> pTextEditorEvents;
	CComPtr<EnvDTE::_WindowEvents> pWindowEvents;
	CComPtr<EnvDTE::_OutputWindowEvents> pOutputWindowEvents;
	CComPtr<EnvDTE::_FindEvents> pFindEvents;
	CComPtr<EnvDTE80::_WindowVisibilityEvents> pWindowVisibilityEvents;
	CComPtr<EnvDTE80::_TextDocumentKeyPressEvents> pTextDocumentKeyPressEvents;
	CComPtr<EnvDTE::_DebuggerEvents> pDebuggerEvents;
	CComPtr<EnvDTE80::_CodeModelEvents> pCodeModelEvents;
	CComPtr<EnvDTE80::_DebuggerProcessEvents> pDebuggerProcessEvents;
	CComPtr<EnvDTE80::_DebuggerExpressionEvaluationEvents> pDebuggerExpressionEvaluationEvents;
	CComPtr<EnvDTE80::_PublishEvents> pPublishEvents;

	CComPtr<EnvDTE::_ProjectItemsEvents> pMiscFilesEventsSink;
	CComPtr<EnvDTE::_ProjectItemsEvents> pSolutionItemsEventsSink;
	CComPtr<EnvDTE::_ProjectItemsEvents> pGlobalProjectItemsEventsSink;
	CComPtr<EnvDTE::_ProjectsEvents> pGlobalProjectsEventsSink;
};

OBJECT_ENTRY_AUTO(__uuidof(Connect), CConnect)
