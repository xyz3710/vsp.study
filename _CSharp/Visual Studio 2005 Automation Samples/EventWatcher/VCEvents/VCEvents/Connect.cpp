//Copyright (c) Microsoft Corporation.  All rights reserved.

// Connect.cpp : Implementation of CConnect
#include "stdafx.h"
#include "AddIn.h"
#include "Connect.h"

extern CAddInModule _AtlModule;

// CConnect
STDMETHODIMP CConnect::OnConnection(IDispatch *pApplication, ext_ConnectMode ConnectMode, IDispatch *pAddInInst, SAFEARRAY ** /*custom*/ )
{
	HRESULT hr = S_OK;
	CComPtr<EnvDTE::Window> pWindow;
	CComPtr<EnvDTE::Windows> pWindows;
	CComQIPtr<EnvDTE::OutputWindow> pOutputWindow;
	CComPtr<EnvDTE::OutputWindowPane> pOutputWindowPane;
	CComPtr<EnvDTE::OutputWindowPanes> pOutputWindowPanes;
	CComPtr<IDispatch> pDisp;
	CComPtr<EnvDTE::Events> pEvents;
	CComQIPtr<EnvDTE80::Events2> pEvents2;

	pApplication->QueryInterface(__uuidof(DTE2), (LPVOID*)&m_pDTE);
	pAddInInst->QueryInterface(__uuidof(AddIn), (LPVOID*)&m_pAddInInstance);

	IfFailGo(m_pDTE->get_Events(&pEvents));
	IfFailGo(m_pDTE->get_Windows(&pWindows));
	IfFailGo(pWindows->Item(CComVariant(EnvDTE::vsWindowKindOutput), &pWindow));
	IfFailGo(pWindow->get_Object(&pDisp));
	pOutputWindow = pDisp;
	IfFailGo(pOutputWindow->get_OutputWindowPanes(&pOutputWindowPanes));
	IfFailGo(pOutputWindowPanes->Add(CComBSTR(L"DTE Event Information"), &pOutputWindowPane));

	if(SUCCEEDED(pEvents->get_TaskListEvents(CComBSTR(""), (EnvDTE::_TaskListEvents**)&pTaskListEvents)))
	{
		m_TaskListEventSink.SetOutputWindow(pOutputWindowPane);
		m_TaskListEventSink.DispEventAdvise((IUnknown*)pTaskListEvents.p);
	}

	if(SUCCEEDED(pEvents->get_DTEEvents((EnvDTE::_DTEEvents**)&pDTEEvents)))
	{
		m_DTEEventsSink.SetOutputWindow(pOutputWindowPane);
		m_DTEEventsSink.DispEventAdvise((IUnknown*)pDTEEvents.p);
	}

	if(SUCCEEDED(pEvents->get_CommandEvents(CComBSTR(L"{00000000-0000-0000-0000-000000000000}"), 0, (EnvDTE::_CommandEvents**)&pCommandEvents)))
	{
		m_CommandEventsSink.SetOutputWindow(pOutputWindowPane);
		m_CommandEventsSink.DispEventAdvise((IUnknown*)pCommandEvents.p);
	}

	if(SUCCEEDED(pEvents->get_SelectionEvents((EnvDTE::_SelectionEvents**)&pSelectionEvents)))
	{
		m_SelectionEventsSink.SetOutputWindow(pOutputWindowPane);
		m_SelectionEventsSink.DispEventAdvise((IUnknown*)pSelectionEvents.p);
	}

	if(SUCCEEDED(pEvents->get_BuildEvents((EnvDTE::_BuildEvents**)&pBuildEvents)))
	{
		m_BuildEventsSink.SetOutputWindow(pOutputWindowPane);
		m_BuildEventsSink.DispEventAdvise((IUnknown*)pBuildEvents.p);
	}

	if(SUCCEEDED(pEvents->get_SolutionEvents((EnvDTE::_SolutionEvents**)&pSolutionEvents)))
	{
		m_SolutionEventsSink.SetOutputWindow(pOutputWindowPane);
		m_SolutionEventsSink.DispEventAdvise((IUnknown*)pSolutionEvents.p);
	}

	if(SUCCEEDED(pEvents->get_DocumentEvents(NULL, (EnvDTE::_DocumentEvents**)&pDocumentEvents)))
	{
		m_DocumentEventsSink.SetOutputWindow(pOutputWindowPane);
		m_DocumentEventsSink.DispEventAdvise((IUnknown*)pDocumentEvents.p);
	}

	if(SUCCEEDED(pEvents->get_TextEditorEvents(NULL, (EnvDTE::_TextEditorEvents**)&pTextEditorEvents)))
	{
		m_TextEditorEventsSink.SetOutputWindow(pOutputWindowPane);
		m_TextEditorEventsSink.DispEventAdvise((IUnknown*)pTextEditorEvents.p);
	}

	if(SUCCEEDED(pEvents->get_WindowEvents(NULL, (EnvDTE::_WindowEvents**)&pWindowEvents)))
	{
		m_WindowEventsSink.SetOutputWindow(pOutputWindowPane);
		m_WindowEventsSink.DispEventAdvise((IUnknown*)pWindowEvents.p);
	}

	if(SUCCEEDED(pEvents->get_OutputWindowEvents(CComBSTR(L""), (EnvDTE::_OutputWindowEvents**)&pOutputWindowEvents)))
	{
		m_OutputWindowEventsSink.SetOutputWindow(pOutputWindowPane);
		m_OutputWindowEventsSink.DispEventAdvise((IUnknown*)pOutputWindowEvents.p);
	}

	if(SUCCEEDED(pEvents->get_FindEvents((EnvDTE::_FindEvents**)&pFindEvents)))
	{
		m_FindEventsSink.SetOutputWindow(pOutputWindowPane);
		m_FindEventsSink.DispEventAdvise((IUnknown*)pFindEvents.p);
	}

	pEvents2 = pEvents;
	if(SUCCEEDED(pEvents2->get_WindowVisibilityEvents(NULL, (EnvDTE80::_WindowVisibilityEvents**)&pWindowVisibilityEvents)))
	{
		m_WindowVisibilityEventSink.SetOutputWindow(pOutputWindowPane);
		m_WindowVisibilityEventSink.DispEventAdvise((IUnknown*)pWindowVisibilityEvents.p);
	}

	if(SUCCEEDED(pEvents2->get_TextDocumentKeyPressEvents(NULL, (EnvDTE80::_TextDocumentKeyPressEvents**)&pTextDocumentKeyPressEvents)))
	{
		m_TextDocumentKeyPressEventSink.SetOutputWindow(pOutputWindowPane);
		m_TextDocumentKeyPressEventSink.DispEventAdvise((IUnknown*)pTextDocumentKeyPressEvents.p);
	}

	if(SUCCEEDED(pEvents->get_DebuggerEvents((EnvDTE::_DebuggerEvents**)&pDebuggerEvents)))
	{
		m_DebuggerEventsSink.SetOutputWindow(pOutputWindowPane);
		m_DebuggerEventsSink.DispEventAdvise((IUnknown*)pDebuggerEvents.p);
	}

	if(SUCCEEDED(pEvents2->get_CodeModelEvents(NULL, (EnvDTE80::_CodeModelEvents**)&pCodeModelEvents)))
	{
		m_CodeModelEventsSink.SetOutputWindow(pOutputWindowPane);
		m_CodeModelEventsSink.DispEventAdvise((IUnknown*)pCodeModelEvents.p);
	}

	if(SUCCEEDED(pEvents2->get_DebuggerProcessEvents((EnvDTE80::_DebuggerProcessEvents**)&pDebuggerProcessEvents)))
	{
		m_DebuggerProcessEventsSink.SetOutputWindow(pOutputWindowPane);
		m_DebuggerProcessEventsSink.DispEventAdvise((IUnknown*)pDebuggerProcessEvents.p);
	}

	if(SUCCEEDED(pEvents2->get_DebuggerExpressionEvaluationEvents((EnvDTE80::_DebuggerExpressionEvaluationEvents**)&pDebuggerExpressionEvaluationEvents)))
	{
		m_DebuggerExpressionEvaluationEventsSink.SetOutputWindow(pOutputWindowPane);
		m_DebuggerExpressionEvaluationEventsSink.DispEventAdvise((IUnknown*)pDebuggerExpressionEvaluationEvents.p);
	}

	if(SUCCEEDED(pEvents2->get_PublishEvents((EnvDTE80::_PublishEvents**)&pPublishEvents)))
	{
		m_PublishEventsSink.SetOutputWindow(pOutputWindowPane);
		m_PublishEventsSink.DispEventAdvise((IUnknown*)pPublishEvents.p);
	}

	if(SUCCEEDED(pEvents->get_MiscFilesEvents((EnvDTE::_ProjectItemsEvents**)&pMiscFilesEventsSink)))
	{
		m_MiscFilesEventsSink.SetOutputWindow(pOutputWindowPane);
		m_MiscFilesEventsSink.DispEventAdvise((IUnknown*)pMiscFilesEventsSink.p);
	}

	if(SUCCEEDED(pEvents->get_SolutionItemsEvents((EnvDTE::_ProjectItemsEvents**)&pSolutionItemsEventsSink)))
	{
		m_SolutionItemsEventsSink.SetOutputWindow(pOutputWindowPane);
		m_SolutionItemsEventsSink.DispEventAdvise((IUnknown*)pSolutionItemsEventsSink.p);
	}

	if(SUCCEEDED(pEvents2->get_ProjectItemsEvents((EnvDTE::_ProjectItemsEvents**)&pGlobalProjectItemsEventsSink)))
	{
		m_GlobalProjectItemsEventsSink.SetOutputWindow(pOutputWindowPane);
		m_GlobalProjectItemsEventsSink.DispEventAdvise((IUnknown*)pGlobalProjectItemsEventsSink.p);
	}

	if(SUCCEEDED(pEvents2->get_ProjectsEvents((EnvDTE::_ProjectsEvents**)&pGlobalProjectsEventsSink)))
	{
		m_GlobalProjectsEventsSink.SetOutputWindow(pOutputWindowPane);
		m_GlobalProjectsEventsSink.DispEventAdvise((IUnknown*)pGlobalProjectsEventsSink.p);
	}

Error:
	return hr;
}

STDMETHODIMP CConnect::OnDisconnection(ext_DisconnectMode /*RemoveMode*/, SAFEARRAY ** /*custom*/ )
{
	if(pTaskListEvents.p)
		m_TaskListEventSink.DispEventUnadvise((IUnknown*)pTaskListEvents.p);
	if(pDTEEvents.p)
		m_DTEEventsSink.DispEventUnadvise((IUnknown*)pDTEEvents.p);
	if(pCommandEvents.p)
		m_CommandEventsSink.DispEventUnadvise((IUnknown*)pCommandEvents.p);
	if(pSelectionEvents.p)
		m_SelectionEventsSink.DispEventUnadvise((IUnknown*)pSelectionEvents.p);
	if(pBuildEvents.p)
		m_BuildEventsSink.DispEventUnadvise((IUnknown*)pBuildEvents.p);
	if(pSolutionEvents.p)
		m_SolutionEventsSink.DispEventUnadvise((IUnknown*)pSolutionEvents.p);
	if(pDocumentEvents.p)
		m_DocumentEventsSink.DispEventUnadvise((IUnknown*)pDocumentEvents.p);
	if(pTextEditorEvents.p)
		m_TextEditorEventsSink.DispEventUnadvise((IUnknown*)pTextEditorEvents.p);
	if(pWindowEvents.p)
		m_WindowEventsSink.DispEventUnadvise((IUnknown*)pWindowEvents.p);
	if(pOutputWindowEvents.p)
		m_OutputWindowEventsSink.DispEventUnadvise((IUnknown*)pOutputWindowEvents.p);
	if(pFindEvents.p)
		m_FindEventsSink.DispEventUnadvise((IUnknown*)pFindEvents.p);
	if(pTextDocumentKeyPressEvents.p)
		m_TextDocumentKeyPressEventSink.DispEventUnadvise((IUnknown*)pTextDocumentKeyPressEvents.p);
	if(pDebuggerEvents.p)
		m_DebuggerEventsSink.DispEventUnadvise((IUnknown*)pDebuggerEvents.p);
	if(pCodeModelEvents.p)
		m_CodeModelEventsSink.DispEventUnadvise((IUnknown*)pCodeModelEvents.p);
	if(pDebuggerProcessEvents.p)
		m_DebuggerProcessEventsSink.DispEventUnadvise((IUnknown*)pDebuggerProcessEvents.p);
	if(pDebuggerExpressionEvaluationEvents.p)
		m_DebuggerExpressionEvaluationEventsSink.DispEventUnadvise((IUnknown*)pDebuggerExpressionEvaluationEvents.p);
	if(pPublishEvents.p)
		m_PublishEventsSink.DispEventUnadvise((IUnknown*)pPublishEvents.p);

	if(pMiscFilesEventsSink.p)
		m_MiscFilesEventsSink.DispEventUnadvise((IUnknown*)pMiscFilesEventsSink.p);
	if(pSolutionItemsEventsSink.p)
		m_SolutionItemsEventsSink.DispEventUnadvise((IUnknown*)pSolutionItemsEventsSink.p);
	if(pGlobalProjectItemsEventsSink.p)
		m_GlobalProjectItemsEventsSink.DispEventUnadvise((IUnknown*)pGlobalProjectItemsEventsSink.p);
	if(pGlobalProjectsEventsSink.p)
		m_GlobalProjectsEventsSink.DispEventUnadvise((IUnknown*)pGlobalProjectsEventsSink.p);

	m_pDTE = NULL;
	m_pAddInInstance = NULL;
	return S_OK;
}

STDMETHODIMP CConnect::OnAddInsUpdate (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnStartupComplete (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

STDMETHODIMP CConnect::OnBeginShutdown (SAFEARRAY ** /*custom*/ )
{
	return S_OK;
}

