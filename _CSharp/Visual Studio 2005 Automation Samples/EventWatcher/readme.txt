Copyright (c) Microsoft Corporation.  All rights reserved.

This Add-in will connect to all the core shell event objects, and as each event is fired information is placed into the output 
window.

If you load the CS, VB or JS version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click on the project and select Properties

2. Open Build page, and change Output Path to <My Documents>\Visual Studio 2005\Addins (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)
 
3. Open the Debug page, and make the following changes 
i. Set �Start Action� to �Start external program� and enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. In �Start Options� set the �Command line arguments� to /resetaddin <language>Events.Connect (where <language> is CS, VB or JS depending on which version of the sample you loaded)
iii. In �Start Options� set the �Working directory� to the directory  containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)



If you load the CPPCLR version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Linker | General and change the Output File property to <My Documents>\Visual Studio 2005\Addins\$(ProjectName).dll (Note: Instead of <My Documents> subsititute the full path to your "My Documents" folder)

3. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin MCPPEvents.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)

After building the MCPPEvents project, manually copy the MCPPEvents.addin from the MCPPEvents subdirectory to <My Documents>\Visual Studio 2005\Addins




If you load the C++ version of this sample, you need to do the following before running it:
1. In the Solution Explorer, right-click in the project and select Properties

2. Open Debugging and make the following changes
i. In "Command" enter the path to devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe)
ii. Set "Command Arguments" to /resetaddin VCEvents.Connect
iii. In "Working Directory" enter the directory containing devenv.exe (e.g. c:\Program Files\Microsoft Visual Studio 8\Common7\IDE)
