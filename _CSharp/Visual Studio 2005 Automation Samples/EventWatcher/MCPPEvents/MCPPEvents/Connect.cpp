//Copyright (c) Microsoft Corporation.  All rights reserved.

// This is the main DLL file.

#include "stdafx.h"
#include "Connect.h"

void MCPPEvents::Connect::OnAddInsUpdate(System::Array ^%custom)
{
}

void MCPPEvents::Connect::OnBeginShutdown(System::Array ^%custom)
{
}

void MCPPEvents::Connect::OnConnection(System::Object ^Application, ext_ConnectMode ConnectMode, System::Object ^AddInInst, System::Array ^%custom)
{
	_applicationObject = dynamic_cast<DTE2^>(Application);
	_addInInstance = dynamic_cast<AddIn^>(AddInInst);
	Object ^nullObject;

	EnvDTE::Events ^events = _applicationObject->Events;

	_windowsEvents = events->WindowEvents[dynamic_cast<Window^>(nullObject)];
	_textEditorEvents = events->TextEditorEvents[dynamic_cast<TextDocument^>(nullObject)];
	_taskListEvents = events->TaskListEvents[""];
	_solutionEvents = events->SolutionEvents;
	_selectionEvents = events->SelectionEvents;
	_outputWindowEvents = events->OutputWindowEvents[""];
	_findEvents = events->FindEvents;
	_dteEvents = events->DTEEvents;
	_documentEvents = events->DocumentEvents[dynamic_cast<Document^>(nullObject)];
	_debuggerEvents = events->DebuggerEvents;
	_commandEvents = events->CommandEvents["{00000000-0000-0000-0000-000000000000}", 0];
	_buildEvents = events->BuildEvents;
	_miscFilesEvents = events->MiscFilesEvents;
	_solutionItemsEvents = events->SolutionItemsEvents;

	_globalProjectsEvents =  dynamic_cast<Events2^>(events)->ProjectsEvents;
	_globalProjectItemsEvents = dynamic_cast<Events2^>(events)->ProjectItemsEvents;
	_textDocumentKeyPressEvents = dynamic_cast<Events2^>(events)->TextDocumentKeyPressEvents[dynamic_cast<TextDocument^>(nullObject)];
	_codeModelEvents = dynamic_cast<Events2^>(events)->CodeModelEvents[dynamic_cast<CodeElement^>(nullObject)];
	_windowVisibilityEvents = dynamic_cast<Events2^>(events)->WindowVisibilityEvents[dynamic_cast<Window^>(nullObject)];
	_debuggerProcessEvents = dynamic_cast<Events2^>(events)->DebuggerProcessEvents;
	_debuggerExpressionEvaluationEvents = dynamic_cast<Events2^>(events)->DebuggerExpressionEvaluationEvents;
	_publishEvents = dynamic_cast<Events2^>(events)->PublishEvents;

	OutputWindow ^outputWindow = dynamic_cast<OutputWindow^>(_applicationObject->Windows->Item(Constants::vsWindowKindOutput)->Object);
	_outputWindowPane = outputWindow->OutputWindowPanes->Add("DTE Event Information - C++/CLR Event Watcher"); 

	//Connect to each delegate exposed from each object retrieved above
	_windowsEvents->WindowActivated += gcnew _dispWindowEvents_WindowActivatedEventHandler(this, &Connect::WindowActivated);
	_windowsEvents->WindowClosing += gcnew _dispWindowEvents_WindowClosingEventHandler(this, &Connect::WindowClosing);
	_windowsEvents->WindowCreated += gcnew _dispWindowEvents_WindowCreatedEventHandler(this, &Connect::WindowCreated);
	_windowsEvents->WindowMoved += gcnew _dispWindowEvents_WindowMovedEventHandler(this, &Connect::WindowMoved);
	_textEditorEvents->LineChanged += gcnew _dispTextEditorEvents_LineChangedEventHandler(this, &Connect::LineChanged);
	_taskListEvents->TaskAdded += gcnew _dispTaskListEvents_TaskAddedEventHandler(this, &Connect::TaskAdded);
	_taskListEvents->TaskModified += gcnew _dispTaskListEvents_TaskModifiedEventHandler(this, &Connect::TaskModified);
	_taskListEvents->TaskNavigated += gcnew _dispTaskListEvents_TaskNavigatedEventHandler(this, &Connect::TaskNavigated);
	_taskListEvents->TaskRemoved += gcnew _dispTaskListEvents_TaskRemovedEventHandler(this, &Connect::TaskRemoved);
	_solutionEvents->AfterClosing += gcnew _dispSolutionEvents_AfterClosingEventHandler(this, &Connect::AfterClosing);
	_solutionEvents->BeforeClosing += gcnew _dispSolutionEvents_BeforeClosingEventHandler(this, &Connect::BeforeClosing);
	_solutionEvents->Opened += gcnew _dispSolutionEvents_OpenedEventHandler(this, &Connect::Opened);
	_solutionEvents->ProjectAdded += gcnew _dispSolutionEvents_ProjectAddedEventHandler(this, &Connect::ProjectAdded);
	_solutionEvents->ProjectRemoved += gcnew _dispSolutionEvents_ProjectRemovedEventHandler(this, &Connect::ProjectRemoved);
	_solutionEvents->ProjectRenamed += gcnew _dispSolutionEvents_ProjectRenamedEventHandler(this, &Connect::ProjectRenamed);
	_solutionEvents->QueryCloseSolution += gcnew _dispSolutionEvents_QueryCloseSolutionEventHandler(this, &Connect::QueryCloseSolution);
	_solutionEvents->Renamed += gcnew _dispSolutionEvents_RenamedEventHandler(this, &Connect::Renamed);
	_selectionEvents->OnChange += gcnew _dispSelectionEvents_OnChangeEventHandler(this, &Connect::OnChange);
	_outputWindowEvents->PaneAdded += gcnew _dispOutputWindowEvents_PaneAddedEventHandler(this, &Connect::PaneAdded);
	_outputWindowEvents->PaneClearing += gcnew _dispOutputWindowEvents_PaneClearingEventHandler(this, &Connect::PaneClearing);
	_outputWindowEvents->PaneUpdated += gcnew _dispOutputWindowEvents_PaneUpdatedEventHandler(this, &Connect::PaneUpdated);
	_findEvents->FindDone += gcnew _dispFindEvents_FindDoneEventHandler(this, &Connect::FindDone);
	_dteEvents->ModeChanged += gcnew _dispDTEEvents_ModeChangedEventHandler(this, &Connect::ModeChanged);
	_dteEvents->OnBeginShutdown += gcnew _dispDTEEvents_OnBeginShutdownEventHandler(this, &Connect::OnBeginShutdown);
	_dteEvents->OnMacrosRuntimeReset += gcnew _dispDTEEvents_OnMacrosRuntimeResetEventHandler(this, &Connect::OnMacrosRuntimeReset);
	_dteEvents->OnStartupComplete += gcnew _dispDTEEvents_OnStartupCompleteEventHandler(this, &Connect::OnStartupComplete);
	_documentEvents->DocumentClosing += gcnew _dispDocumentEvents_DocumentClosingEventHandler(this, &Connect::DocumentClosing);
	_documentEvents->DocumentOpened += gcnew _dispDocumentEvents_DocumentOpenedEventHandler(this, &Connect::DocumentOpened);
	_documentEvents->DocumentOpening += gcnew _dispDocumentEvents_DocumentOpeningEventHandler(this, &Connect::DocumentOpening);
	_documentEvents->DocumentSaved += gcnew _dispDocumentEvents_DocumentSavedEventHandler(this, &Connect::DocumentSaved);
	_debuggerEvents->OnContextChanged += gcnew _dispDebuggerEvents_OnContextChangedEventHandler(this, &Connect::OnContextChanged);
	_debuggerEvents->OnEnterBreakMode += gcnew _dispDebuggerEvents_OnEnterBreakModeEventHandler(this, &Connect::OnEnterBreakMode);
	_debuggerEvents->OnEnterDesignMode += gcnew _dispDebuggerEvents_OnEnterDesignModeEventHandler(this, &Connect::OnEnterDesignMode);
	_debuggerEvents->OnEnterRunMode += gcnew _dispDebuggerEvents_OnEnterRunModeEventHandler(this, &Connect::OnEnterRunMode);
	_debuggerEvents->OnExceptionNotHandled += gcnew _dispDebuggerEvents_OnExceptionNotHandledEventHandler(this, &Connect::OnExceptionNotHandled);
	_debuggerEvents->OnExceptionThrown += gcnew _dispDebuggerEvents_OnExceptionThrownEventHandler(this, &Connect::OnExceptionThrown);
	_commandEvents->AfterExecute += gcnew _dispCommandEvents_AfterExecuteEventHandler(this, &Connect::AfterExecute);
	_commandEvents->BeforeExecute += gcnew _dispCommandEvents_BeforeExecuteEventHandler(this, &Connect::BeforeExecute);
	_buildEvents->OnBuildBegin += gcnew _dispBuildEvents_OnBuildBeginEventHandler(this, &Connect::OnBuildBegin);
	_buildEvents->OnBuildDone += gcnew _dispBuildEvents_OnBuildDoneEventHandler(this, &Connect::OnBuildDone);
	_buildEvents->OnBuildProjConfigBegin += gcnew _dispBuildEvents_OnBuildProjConfigBeginEventHandler(this, &Connect::OnBuildProjConfigBegin);
	_buildEvents->OnBuildProjConfigDone += gcnew _dispBuildEvents_OnBuildProjConfigDoneEventHandler(this, &Connect::OnBuildProjConfigDone);
	_miscFilesEvents->ItemAdded += gcnew _dispProjectItemsEvents_ItemAddedEventHandler(this, &Connect::MiscFilesEvents_ItemAdded);
	_miscFilesEvents->ItemRemoved += gcnew _dispProjectItemsEvents_ItemRemovedEventHandler(this, &Connect::MiscFilesEvents_ItemRemoved);
	_miscFilesEvents->ItemRenamed += gcnew _dispProjectItemsEvents_ItemRenamedEventHandler(this, &Connect::MiscFilesEvents_ItemRenamed);
	_solutionItemsEvents->ItemAdded += gcnew _dispProjectItemsEvents_ItemAddedEventHandler(this, &Connect::SolutionItemsEvents_ItemAdded);
	_solutionItemsEvents->ItemRemoved += gcnew _dispProjectItemsEvents_ItemRemovedEventHandler(this, &Connect::SolutionItemsEvents_ItemRemoved);
	_solutionItemsEvents->ItemRenamed += gcnew _dispProjectItemsEvents_ItemRenamedEventHandler(this, &Connect::SolutionItemsEvents_ItemRenamed);
	_globalProjectItemsEvents->ItemAdded += gcnew _dispProjectItemsEvents_ItemAddedEventHandler(this, &Connect::GlobalProjectItemsEvents_ItemAdded);
	_globalProjectItemsEvents->ItemRemoved += gcnew _dispProjectItemsEvents_ItemRemovedEventHandler(this, &Connect::GlobalProjectItemsEvents_ItemRemoved);
	_globalProjectItemsEvents->ItemRenamed +=gcnew _dispProjectItemsEvents_ItemRenamedEventHandler(this, &Connect::GlobalProjectItemsEvents_ItemRenamed);
	_globalProjectsEvents->ItemAdded += gcnew _dispProjectsEvents_ItemAddedEventHandler(this, &Connect::GlobalProjectsEvents_ItemAdded);
	_globalProjectsEvents->ItemRemoved+=gcnew _dispProjectsEvents_ItemRemovedEventHandler(this, &Connect::GlobalProjectsEvents_ItemRemoved);
	_globalProjectsEvents->ItemRenamed += gcnew _dispProjectsEvents_ItemRenamedEventHandler(this, &Connect::GlobalProjectsEvents_ItemRenamed);
	_textDocumentKeyPressEvents->AfterKeyPress += gcnew _dispTextDocumentKeyPressEvents_AfterKeyPressEventHandler(this, &Connect::AfterKeyPress);
	_textDocumentKeyPressEvents->BeforeKeyPress += gcnew _dispTextDocumentKeyPressEvents_BeforeKeyPressEventHandler(this, &Connect::BeforeKeyPress);
	_codeModelEvents->ElementAdded += gcnew _dispCodeModelEvents_ElementAddedEventHandler(this, &Connect::ElementAdded);
	_codeModelEvents->ElementChanged+=gcnew _dispCodeModelEvents_ElementChangedEventHandler(this, &Connect::ElementChanged);
	_codeModelEvents->ElementDeleted += gcnew _dispCodeModelEvents_ElementDeletedEventHandler(this, &Connect::ElementDeleted);
	_windowVisibilityEvents->WindowHiding += gcnew _dispWindowVisibilityEvents_WindowHidingEventHandler(this, &Connect::WindowHiding);
	_windowVisibilityEvents->WindowShowing += gcnew _dispWindowVisibilityEvents_WindowShowingEventHandler(this, &Connect::WindowShowing);
	_debuggerExpressionEvaluationEvents->OnExpressionEvaluation += gcnew _dispDebuggerExpressionEvaluationEvents_OnExpressionEvaluationEventHandler(this, &Connect::OnExpressionEvaluation);
	_debuggerProcessEvents->OnProcessStateChanged += gcnew _dispDebuggerProcessEvents_OnProcessStateChangedEventHandler(this, &Connect::OnProcessStateChanged);
	_publishEvents->OnPublishBegin += gcnew _dispPublishEvents_OnPublishBeginEventHandler(this, &Connect::OnPublishBegin);
	_publishEvents->OnPublishDone += gcnew _dispPublishEvents_OnPublishDoneEventHandler(this, &Connect::OnPublishDone);
}

void MCPPEvents::Connect::OnStartupComplete(System::Array ^%custom)
{
}

void MCPPEvents::Connect::OnDisconnection(ext_DisconnectMode removeMode, System::Array ^%custom)
{
	if(_windowsEvents)
	{
		_windowsEvents->WindowActivated -= gcnew _dispWindowEvents_WindowActivatedEventHandler(this, &Connect::WindowActivated);
		_windowsEvents->WindowClosing -= gcnew _dispWindowEvents_WindowClosingEventHandler(this, &Connect::WindowClosing);
		_windowsEvents->WindowCreated -= gcnew _dispWindowEvents_WindowCreatedEventHandler(this, &Connect::WindowCreated);
		_windowsEvents->WindowMoved -= gcnew _dispWindowEvents_WindowMovedEventHandler(this, &Connect::WindowMoved);
	}

	if(_textEditorEvents)
	{
		_textEditorEvents->LineChanged -= gcnew _dispTextEditorEvents_LineChangedEventHandler(this, &Connect::LineChanged);
	}

	if(_taskListEvents)
	{
		_taskListEvents->TaskAdded -= gcnew _dispTaskListEvents_TaskAddedEventHandler(this, &Connect::TaskAdded);
		_taskListEvents->TaskModified -= gcnew _dispTaskListEvents_TaskModifiedEventHandler(this, &Connect::TaskModified);
		_taskListEvents->TaskNavigated -= gcnew _dispTaskListEvents_TaskNavigatedEventHandler(this, &Connect::TaskNavigated);
		_taskListEvents->TaskRemoved -= gcnew _dispTaskListEvents_TaskRemovedEventHandler(this, &Connect::TaskRemoved);
	}

	if(_solutionEvents)
	{
		_solutionEvents->AfterClosing -= gcnew _dispSolutionEvents_AfterClosingEventHandler(this, &Connect::AfterClosing);
		_solutionEvents->BeforeClosing -= gcnew _dispSolutionEvents_BeforeClosingEventHandler(this, &Connect::BeforeClosing);
		_solutionEvents->Opened -= gcnew _dispSolutionEvents_OpenedEventHandler(this, &Connect::Opened);
		_solutionEvents->ProjectAdded -= gcnew _dispSolutionEvents_ProjectAddedEventHandler(this, &Connect::ProjectAdded);
		_solutionEvents->ProjectRemoved -= gcnew _dispSolutionEvents_ProjectRemovedEventHandler(this, &Connect::ProjectRemoved);
		_solutionEvents->ProjectRenamed -= gcnew _dispSolutionEvents_ProjectRenamedEventHandler(this, &Connect::ProjectRenamed);
		_solutionEvents->QueryCloseSolution -= gcnew _dispSolutionEvents_QueryCloseSolutionEventHandler(this, &Connect::QueryCloseSolution);
		_solutionEvents->Renamed -= gcnew _dispSolutionEvents_RenamedEventHandler(this, &Connect::Renamed);
	}


	if(_selectionEvents)
	{
		_selectionEvents->OnChange -= gcnew _dispSelectionEvents_OnChangeEventHandler(this, &Connect::OnChange);
	}

	if(_outputWindowEvents)
	{
		_outputWindowEvents->PaneAdded -= gcnew _dispOutputWindowEvents_PaneAddedEventHandler(this, &Connect::PaneAdded);
		_outputWindowEvents->PaneClearing -= gcnew _dispOutputWindowEvents_PaneClearingEventHandler(this, &Connect::PaneClearing);
		_outputWindowEvents->PaneUpdated -= gcnew _dispOutputWindowEvents_PaneUpdatedEventHandler(this, &Connect::PaneUpdated);
	}

	if(_findEvents)
	{
		_findEvents->FindDone -= gcnew _dispFindEvents_FindDoneEventHandler(this, &Connect::FindDone);
	}

	if(_dteEvents)
	{
		_dteEvents->ModeChanged -= gcnew _dispDTEEvents_ModeChangedEventHandler(this, &Connect::ModeChanged);
		_dteEvents->OnBeginShutdown -= gcnew _dispDTEEvents_OnBeginShutdownEventHandler(this, &Connect::OnBeginShutdown);
		_dteEvents->OnMacrosRuntimeReset -= gcnew _dispDTEEvents_OnMacrosRuntimeResetEventHandler(this, &Connect::OnMacrosRuntimeReset);
		_dteEvents->OnStartupComplete -= gcnew _dispDTEEvents_OnStartupCompleteEventHandler(this, &Connect::OnStartupComplete);
	}

	if(_documentEvents)
	{
		_documentEvents->DocumentClosing -= gcnew _dispDocumentEvents_DocumentClosingEventHandler(this, &Connect::DocumentClosing);
		_documentEvents->DocumentOpened -= gcnew _dispDocumentEvents_DocumentOpenedEventHandler(this, &Connect::DocumentOpened);
		_documentEvents->DocumentOpening -= gcnew _dispDocumentEvents_DocumentOpeningEventHandler(this, &Connect::DocumentOpening);
		_documentEvents->DocumentSaved -= gcnew _dispDocumentEvents_DocumentSavedEventHandler(this, &Connect::DocumentSaved);
	}

	if(_debuggerEvents)
	{
		_debuggerEvents->OnContextChanged -= gcnew _dispDebuggerEvents_OnContextChangedEventHandler(this, &Connect::OnContextChanged);
		_debuggerEvents->OnEnterBreakMode -= gcnew _dispDebuggerEvents_OnEnterBreakModeEventHandler(this, &Connect::OnEnterBreakMode);
		_debuggerEvents->OnEnterDesignMode -= gcnew _dispDebuggerEvents_OnEnterDesignModeEventHandler(this, &Connect::OnEnterDesignMode);
		_debuggerEvents->OnEnterRunMode -= gcnew _dispDebuggerEvents_OnEnterRunModeEventHandler(this, &Connect::OnEnterRunMode);
		_debuggerEvents->OnExceptionNotHandled -= gcnew _dispDebuggerEvents_OnExceptionNotHandledEventHandler(this, &Connect::OnExceptionNotHandled);
		_debuggerEvents->OnExceptionThrown -= gcnew _dispDebuggerEvents_OnExceptionThrownEventHandler(this, &Connect::OnExceptionThrown);
	}

	if(_commandEvents)
	{
		_commandEvents->AfterExecute -= gcnew _dispCommandEvents_AfterExecuteEventHandler(this, &Connect::AfterExecute);
		_commandEvents->BeforeExecute -= gcnew _dispCommandEvents_BeforeExecuteEventHandler(this, &Connect::BeforeExecute);
	}

	if(_buildEvents)
	{
		_buildEvents->OnBuildBegin -= gcnew _dispBuildEvents_OnBuildBeginEventHandler(this, &Connect::OnBuildBegin);
		_buildEvents->OnBuildDone -= gcnew _dispBuildEvents_OnBuildDoneEventHandler(this, &Connect::OnBuildDone);
		_buildEvents->OnBuildProjConfigBegin -= gcnew _dispBuildEvents_OnBuildProjConfigBeginEventHandler(this, &Connect::OnBuildProjConfigBegin);
		_buildEvents->OnBuildProjConfigDone -= gcnew _dispBuildEvents_OnBuildProjConfigDoneEventHandler(this, &Connect::OnBuildProjConfigDone);
	}

	if(_miscFilesEvents)
	{
		_miscFilesEvents->ItemAdded -= gcnew _dispProjectItemsEvents_ItemAddedEventHandler(this, &Connect::MiscFilesEvents_ItemAdded);
		_miscFilesEvents->ItemRemoved -= gcnew _dispProjectItemsEvents_ItemRemovedEventHandler(this, &Connect::MiscFilesEvents_ItemRemoved);
		_miscFilesEvents->ItemRenamed -= gcnew _dispProjectItemsEvents_ItemRenamedEventHandler(this, &Connect::MiscFilesEvents_ItemRenamed);
	}

	if(_solutionItemsEvents)
	{
		_solutionItemsEvents->ItemAdded -= gcnew _dispProjectItemsEvents_ItemAddedEventHandler(this, &Connect::SolutionItemsEvents_ItemAdded);
		_solutionItemsEvents->ItemRemoved -= gcnew _dispProjectItemsEvents_ItemRemovedEventHandler(this, &Connect::SolutionItemsEvents_ItemRemoved);
		_solutionItemsEvents->ItemRenamed -= gcnew _dispProjectItemsEvents_ItemRenamedEventHandler(this, &Connect::SolutionItemsEvents_ItemRenamed);
	}

	if(_globalProjectItemsEvents)
	{
		_globalProjectItemsEvents->ItemAdded -= gcnew _dispProjectItemsEvents_ItemAddedEventHandler(this, &Connect::GlobalProjectItemsEvents_ItemAdded);
		_globalProjectItemsEvents->ItemRemoved -= gcnew _dispProjectItemsEvents_ItemRemovedEventHandler(this, &Connect::GlobalProjectItemsEvents_ItemRemoved);
		_globalProjectItemsEvents->ItemRenamed -=gcnew _dispProjectItemsEvents_ItemRenamedEventHandler(this, &Connect::GlobalProjectItemsEvents_ItemRenamed);
	}

	if(_globalProjectsEvents)
	{
		_globalProjectsEvents->ItemAdded -= gcnew _dispProjectsEvents_ItemAddedEventHandler(this, &Connect::GlobalProjectsEvents_ItemAdded);
		_globalProjectsEvents->ItemRemoved-=gcnew _dispProjectsEvents_ItemRemovedEventHandler(this, &Connect::GlobalProjectsEvents_ItemRemoved);
		_globalProjectsEvents->ItemRenamed -= gcnew _dispProjectsEvents_ItemRenamedEventHandler(this, &Connect::GlobalProjectsEvents_ItemRenamed);
	}

	if(_textDocumentKeyPressEvents)
	{
		_textDocumentKeyPressEvents->AfterKeyPress -= gcnew _dispTextDocumentKeyPressEvents_AfterKeyPressEventHandler(this, &Connect::AfterKeyPress);
		_textDocumentKeyPressEvents->BeforeKeyPress -= gcnew _dispTextDocumentKeyPressEvents_BeforeKeyPressEventHandler(this, &Connect::BeforeKeyPress);
	}

	if(_codeModelEvents)
	{
		_codeModelEvents->ElementAdded -= gcnew _dispCodeModelEvents_ElementAddedEventHandler(this, &Connect::ElementAdded);
		_codeModelEvents->ElementChanged-=gcnew _dispCodeModelEvents_ElementChangedEventHandler(this, &Connect::ElementChanged);
		_codeModelEvents->ElementDeleted -= gcnew _dispCodeModelEvents_ElementDeletedEventHandler(this, &Connect::ElementDeleted);
	}

	if(_windowVisibilityEvents)
	{
	_windowVisibilityEvents->WindowHiding -= gcnew _dispWindowVisibilityEvents_WindowHidingEventHandler(this, &Connect::WindowHiding);
	_windowVisibilityEvents->WindowShowing -= gcnew _dispWindowVisibilityEvents_WindowShowingEventHandler(this, &Connect::WindowShowing);
	}

	if(_debuggerExpressionEvaluationEvents)
	{
	_debuggerExpressionEvaluationEvents->OnExpressionEvaluation -= gcnew _dispDebuggerExpressionEvaluationEvents_OnExpressionEvaluationEventHandler(this, &Connect::OnExpressionEvaluation);
	_debuggerProcessEvents->OnProcessStateChanged -= gcnew _dispDebuggerProcessEvents_OnProcessStateChangedEventHandler(this, &Connect::OnProcessStateChanged);
	}

	if(_publishEvents)
	{
	_publishEvents->OnPublishBegin -= gcnew _dispPublishEvents_OnPublishBeginEventHandler(this, &Connect::OnPublishBegin);
	_publishEvents->OnPublishDone -= gcnew _dispPublishEvents_OnPublishDoneEventHandler(this, &Connect::OnPublishDone);	
	}
}

//WindowEvents
void MCPPEvents::Connect::WindowClosing(Window ^closingWindow)
{
	_outputWindowPane->OutputString("WindowEvents::WindowClosing\n");
	_outputWindowPane->OutputString("\tWindow: ");
	_outputWindowPane->OutputString(closingWindow->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::WindowActivated(Window ^gotFocus, Window ^lostFocus)
{
	_outputWindowPane->OutputString("WindowEvents::WindowActivated\n");
	_outputWindowPane->OutputString("\tWindow receiving focus: ");
	_outputWindowPane->OutputString(gotFocus->default);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tWindow that lost focus: ");
	_outputWindowPane->OutputString(lostFocus->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::WindowCreated(Window ^window)
{
	_outputWindowPane->OutputString("WindowEvents::WindowCreated\n");
	_outputWindowPane->OutputString("\tWindow: ");
	_outputWindowPane->OutputString(window->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::WindowMoved(Window ^window, int top, int left, int width, int height)
{
	_outputWindowPane->OutputString("WindowEvents::WindowMoved\n");
	_outputWindowPane->OutputString("\tWindow: ");
	_outputWindowPane->OutputString(window->default);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tLocation: (");
	_outputWindowPane->OutputString(top.ToString());
	_outputWindowPane->OutputString(" , ");
	_outputWindowPane->OutputString(left.ToString());
	_outputWindowPane->OutputString(" , ");
	_outputWindowPane->OutputString(width.ToString());
	_outputWindowPane->OutputString(" , ");
	_outputWindowPane->OutputString(height.ToString());
	_outputWindowPane->OutputString(")\n");
}

//TextEditorEvents
void MCPPEvents::Connect::LineChanged(TextPoint ^startPoint, TextPoint ^endPoint, int hint)
{
	vsTextChanged textChangedHint = (vsTextChanged)hint;
	_outputWindowPane->OutputString("TextEditorEvents::LineChanged\n");
	_outputWindowPane->OutputString("\tDocument: ");
	_outputWindowPane->OutputString(startPoint->Parent->Parent->default);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tChange hint: ");
	_outputWindowPane->OutputString(hint.ToString());
	_outputWindowPane->OutputString("\n");
}

//TaskListEvents
void MCPPEvents::Connect::TaskAdded(TaskItem ^taskItem)
{
	_outputWindowPane->OutputString("TaskListEvents::TaskAdded\n");
	_outputWindowPane->OutputString("\tTask description: ");
	_outputWindowPane->OutputString(taskItem->Description);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::TaskModified(TaskItem ^taskItem, vsTaskListColumn columnModified)
{
	_outputWindowPane->OutputString("TaskListEvents::TaskModified\n");
	_outputWindowPane->OutputString("\tTask description: ");
	_outputWindowPane->OutputString(taskItem->Description);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::TaskNavigated(TaskItem ^taskItem, bool % navigateHandled)
{
	_outputWindowPane->OutputString("TaskListEvents::TaskNavigated\n");
	_outputWindowPane->OutputString("\tTask description: ");
	_outputWindowPane->OutputString(taskItem->Description);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::TaskRemoved(TaskItem ^taskItem)
{
	_outputWindowPane->OutputString("TaskListEvents::TaskRemoved\n");
	_outputWindowPane->OutputString("\tTask description: ");
	_outputWindowPane->OutputString(taskItem->Description);
	_outputWindowPane->OutputString("\n");
}

//SolutionEvents
void MCPPEvents::Connect::AfterClosing()
{
	_outputWindowPane->OutputString("SolutionEvents::AfterClosing\n");
}

void MCPPEvents::Connect::BeforeClosing()
{
	_outputWindowPane->OutputString("SolutionEvents::BeforeClosing\n");
}

void MCPPEvents::Connect::Opened()
{
	_outputWindowPane->OutputString("SolutionEvents::Opened\n");
}

void MCPPEvents::Connect::ProjectAdded(Project ^project)
{
	_outputWindowPane->OutputString("SolutionEvents::ProjectAdded\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project->UniqueName);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::ProjectRemoved(Project ^project)
{
	_outputWindowPane->OutputString("SolutionEvents::ProjectRemoved\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project->UniqueName);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::ProjectRenamed(Project ^project, String ^oldName)
{
	_outputWindowPane->OutputString("SolutionEvents::ProjectRenamed\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project->UniqueName);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::QueryCloseSolution(bool % cancel)
{
	_outputWindowPane->OutputString("SolutionEvents::QueryCloseSolution\n");
}

void MCPPEvents::Connect::Renamed(String ^oldName)
{
	_outputWindowPane->OutputString("SolutionEvents::Renamed\n");
}

//SelectionEvents
void MCPPEvents::Connect::OnChange()
{
	_outputWindowPane->OutputString("SelectionEvents::OnChange\n");
	int count = _applicationObject->SelectedItems->Count;

	for (int i = 1 ; i <= _applicationObject->SelectedItems->Count ; i++)
	{
		_outputWindowPane->OutputString("Item name: ");
		_outputWindowPane->OutputString(dynamic_cast<Project^>(_applicationObject->SelectedItems->Item(i))->default);
		_outputWindowPane->OutputString("\n");
	}
}

//OutputWindowEvents
void MCPPEvents::Connect::PaneAdded(OutputWindowPane ^pane)
{
	_outputWindowPane->OutputString("OutputWindowEvents::PaneAdded\n");
	_outputWindowPane->OutputString("\tPane: ");
	_outputWindowPane->OutputString(pane->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::PaneClearing(OutputWindowPane ^pane)
{
	_outputWindowPane->OutputString("OutputWindowEvents::PaneClearing\n");
	_outputWindowPane->OutputString("\tPane: ");
	_outputWindowPane->OutputString(pane->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::PaneUpdated(OutputWindowPane ^pane)
{
	//Dont want to do this one, or we will end up in a recursive call:
	//_outputWindowPane->OutputString("OutputWindowEvents::PaneUpdated\n");
	//_outputWindowPane->OutputString("\tPane: " + pane->Name + "\n");
}

//FindEvents
void MCPPEvents::Connect::FindDone(vsFindResult result, bool cancelled)
{
	_outputWindowPane->OutputString("FindEvents::FindDone\n");
}

//DTEEvents
void MCPPEvents::Connect::ModeChanged(vsIDEMode LastMode)
{
	_outputWindowPane->OutputString("DTEEvents::ModeChanged\n");
}

void MCPPEvents::Connect::OnBeginShutdown()
{
	_outputWindowPane->OutputString("DTEEvents::OnBeginShutdown\n");
}

void MCPPEvents::Connect::OnMacrosRuntimeReset()
{
	_outputWindowPane->OutputString("DTEEvents::OnMacrosRuntimeReset\n");
}

void MCPPEvents::Connect::OnStartupComplete()
{
	_outputWindowPane->OutputString("DTEEvents::OnStartupComplete\n");
}

//DocumentEvents
void MCPPEvents::Connect::DocumentClosing(Document ^document)
{
	_outputWindowPane->OutputString("DocumentEvents::DocumentClosing\n");
	_outputWindowPane->OutputString("\tDocument: ");
	_outputWindowPane->OutputString(document->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::DocumentOpened(Document ^document)
{
	_outputWindowPane->OutputString("DocumentEvents::DocumentOpened\n");
	_outputWindowPane->OutputString("\tDocument: ");
	_outputWindowPane->OutputString(document->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::DocumentOpening(String ^documentPath, bool ReadOnly)
{
	_outputWindowPane->OutputString("DocumentEvents::DocumentOpening\n");
	_outputWindowPane->OutputString("\tPath: ");
	_outputWindowPane->OutputString(documentPath);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::DocumentSaved(Document ^document)
{
	_outputWindowPane->OutputString("DocumentEvents::DocumentSaved\n");
	_outputWindowPane->OutputString("\tDocument: ");
	_outputWindowPane->OutputString(document->default);
	_outputWindowPane->OutputString("\n");
}

//DebuggerEvents
void MCPPEvents::Connect::OnContextChanged(Process ^NewProcess, Program ^NewProgram, Thread ^NewThread, StackFrame ^NewStackFrame)
{
	_outputWindowPane->OutputString("DebuggerEvents::OnContextChanged\n");
}

void MCPPEvents::Connect::OnEnterBreakMode(dbgEventReason reason, dbgExecutionAction %executionAction)
{
	executionAction = dbgExecutionAction::dbgExecutionActionDefault;
	_outputWindowPane->OutputString("DebuggerEvents::OnEnterBreakMode\n");
}

void MCPPEvents::Connect::OnEnterDesignMode(dbgEventReason Reason)
{
	_outputWindowPane->OutputString("DebuggerEvents::OnEnterDesignMode\n");
}

void MCPPEvents::Connect::OnEnterRunMode(dbgEventReason Reason)
{
	_outputWindowPane->OutputString("DebuggerEvents::OnEnterRunMode\n");
}

void MCPPEvents::Connect::OnExceptionNotHandled(String ^exceptionType, String ^name, int code, String ^description, dbgExceptionAction %exceptionAction)
{
	exceptionAction = dbgExceptionAction::dbgExceptionActionDefault;
	_outputWindowPane->OutputString("DebuggerEvents::OnExceptionNotHandled\n");
}

void MCPPEvents::Connect::OnExceptionThrown(String ^exceptionType, String ^name, int code, String ^description, dbgExceptionAction %exceptionAction)
{
	exceptionAction = dbgExceptionAction::dbgExceptionActionDefault;
	_outputWindowPane->OutputString("DebuggerEvents::OnExceptionThrown\n");
}

//CommandEvents
void MCPPEvents::Connect::AfterExecute(String ^Guid, int ID, Object ^CustomIn, Object ^CustomOut)
{
	String ^commandName = "";
	try
	{
		commandName = _applicationObject->Commands->Item(Guid, ID)->default;
	}
	catch (System::Exception ^ /*excep*/)
	{
	}
	_outputWindowPane->OutputString("CommandEvents::AfterExecute\n");
	if(commandName != "")
		_outputWindowPane->OutputString("\tCommand name: ");
	_outputWindowPane->OutputString(commandName);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tCommand GUID/ID: ");
	_outputWindowPane->OutputString(Guid);
	_outputWindowPane->OutputString(", ");
	_outputWindowPane->OutputString(ID.ToString());
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::BeforeExecute(String ^Guid, int ID, Object ^CustomIn, Object ^ CustomOut, bool %CancelDefault)
{
	String ^commandName = "";
	try
	{
		commandName = _applicationObject->Commands->Item(Guid, ID)->default;
	}
	catch (System::Exception ^ /*excep*/)
	{
	}
	_outputWindowPane->OutputString("CommandEvents::BeforeExecute\n");
	if(commandName != "")
		_outputWindowPane->OutputString("\tCommand name: ");
	_outputWindowPane->OutputString(commandName);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tCommand GUID/ID: ");
	_outputWindowPane->OutputString(Guid);
	_outputWindowPane->OutputString(", ");
	_outputWindowPane->OutputString(ID.ToString());
	_outputWindowPane->OutputString("\n");
}

//BuildEvents
void MCPPEvents::Connect::OnBuildBegin(vsBuildScope Scope, vsBuildAction Action)
{
	_outputWindowPane->OutputString("BuildEvents::OnBuildBegin\n");
}

void MCPPEvents::Connect::OnBuildDone(vsBuildScope Scope, vsBuildAction Action)
{
	_outputWindowPane->OutputString("BuildEvents::OnBuildDone\n");
}

void MCPPEvents::Connect::OnBuildProjConfigBegin(String ^project, String ^projectConfig, String ^platform, String ^solutionConfig)
{
	_outputWindowPane->OutputString("BuildEvents::OnBuildProjConfigBegin\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tProject Configuration: ");
	_outputWindowPane->OutputString(projectConfig);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tPlatform: ");
	_outputWindowPane->OutputString(platform);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tSolution Configuration: ");
	_outputWindowPane->OutputString(solutionConfig);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::OnBuildProjConfigDone(String ^project, String ^projectConfig, String ^platform, String ^solutionConfig, bool success)
{
	_outputWindowPane->OutputString("BuildEvents::OnBuildProjConfigDone\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tProject Configuration: ");
	_outputWindowPane->OutputString(projectConfig);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tPlatform: ");
	_outputWindowPane->OutputString(platform);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tSolution Configuration: ");
	_outputWindowPane->OutputString(solutionConfig);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tBuild success: ");
	_outputWindowPane->OutputString(success.ToString());
	_outputWindowPane->OutputString("\n");
}

//MiscFilesEvents
void MCPPEvents::Connect::MiscFilesEvents_ItemAdded(ProjectItem ^projectItem)
{
	_outputWindowPane->OutputString("MiscFilesEvents::ItemAdded\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::MiscFilesEvents_ItemRemoved(ProjectItem ^projectItem)
{
	_outputWindowPane->OutputString("MiscFilesEvents::ItemRemoved\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::MiscFilesEvents_ItemRenamed(ProjectItem ^projectItem, String ^OldName)
{
	_outputWindowPane->OutputString("MiscFilesEvents::ItemRenamed\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

//SolutionItemsEvents
void MCPPEvents::Connect::SolutionItemsEvents_ItemAdded(ProjectItem ^projectItem)
{
	_outputWindowPane->OutputString("SolutionItemsEvents::ItemAdded\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::SolutionItemsEvents_ItemRemoved(ProjectItem ^projectItem)
{
	_outputWindowPane->OutputString("SolutionItemsEvents::ItemRemoved\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::SolutionItemsEvents_ItemRenamed(ProjectItem ^projectItem, String ^OldName)
{
	_outputWindowPane->OutputString("SolutionItemsEvents::ItemRenamed\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

//Global ProjectItemsEvents
void MCPPEvents::Connect::GlobalProjectItemsEvents_ItemAdded(ProjectItem ^projectItem)
{
	_outputWindowPane->OutputString("ProjectItemsEvents::ItemAdded\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::GlobalProjectItemsEvents_ItemRemoved(ProjectItem ^projectItem)
{
	_outputWindowPane->OutputString("ProjectItemsEvents::ItemRemoved\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::GlobalProjectItemsEvents_ItemRenamed(ProjectItem ^projectItem, String ^OldName)
{
	_outputWindowPane->OutputString("ProjectItemsEvents::ItemRenamed\n");
	_outputWindowPane->OutputString("\tProject Item: ");
	_outputWindowPane->OutputString(projectItem->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::GlobalProjectsEvents_ItemAdded(Project ^project)
{
	_outputWindowPane->OutputString("ProjectsEvents::ItemAdded\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::GlobalProjectsEvents_ItemRemoved(Project ^project)
{
	_outputWindowPane->OutputString("ProjectsEvents::ItemRemoved\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project->default);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::GlobalProjectsEvents_ItemRenamed(Project ^project, String ^OldName)
{
	_outputWindowPane->OutputString("ProjectsEvents::ItemRenamed\n");
	_outputWindowPane->OutputString("\tProject: ");
	_outputWindowPane->OutputString(project->default);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tOld name: ");
	_outputWindowPane->OutputString(OldName);
	_outputWindowPane->OutputString("\n");
}

//TextDocumentKeyPressEvents
void MCPPEvents::Connect::BeforeKeyPress(String ^Keypress, TextSelection ^Selection, bool InStatementCompletion, bool %CancelKeypress)
{
	CancelKeypress = false;
	_outputWindowPane->OutputString("TextDocumentKeyPressEvents::BeforeKeyPress\n");
	_outputWindowPane->OutputString("\tKeypress: ");
	_outputWindowPane->OutputString(Keypress);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tInStatementCompletion: ");
	_outputWindowPane->OutputString(InStatementCompletion.ToString());
	_outputWindowPane->OutputString("\n");

}

void MCPPEvents::Connect::AfterKeyPress(String ^Keypress, TextSelection ^Selection, bool InStatementCompletion)
{
	_outputWindowPane->OutputString("TextDocumentKeyPressEvents::AfterKeyPress\n");
	_outputWindowPane->OutputString("\tKey: ");
	_outputWindowPane->OutputString(Keypress);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tInStatementCompletion: ");
	_outputWindowPane->OutputString(InStatementCompletion.ToString());
	_outputWindowPane->OutputString("\n");
}

//CodeModelEvents
void MCPPEvents::Connect::ElementAdded(CodeElement ^Element)
{
	_outputWindowPane->OutputString("CodeModelEvents::ElementAdded\n");
	_outputWindowPane->OutputString("\tElement: ");
	_outputWindowPane->OutputString(Element->FullName);
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::ElementChanged(CodeElement ^Element, vsCMChangeKind Change)
{
		_outputWindowPane->OutputString("CodeModelEvents::ElementChanged\n");
	_outputWindowPane->OutputString("\tElement: ");
	_outputWindowPane->OutputString(Element->FullName);
	_outputWindowPane->OutputString("\n");
	_outputWindowPane->OutputString("\tChangeKind: ");
	_outputWindowPane->OutputString(Change.ToString());
	_outputWindowPane->OutputString("\n");
}

void MCPPEvents::Connect::ElementDeleted(Object ^Parent, CodeElement ^Element)
{
	Object ^nullObject;
	CodeElement ^parentCodeElement = dynamic_cast<CodeElement^>(Parent);
	ProjectItem ^parentProjectItem = dynamic_cast<ProjectItem^>(Parent);
	_outputWindowPane->OutputString("CodeModelEvents::ElementAdded\n");
	_outputWindowPane->OutputString("\tElement: ");
	_outputWindowPane->OutputString(Element->FullName);
	_outputWindowPane->OutputString("\n");
	if(parentCodeElement != dynamic_cast<CodeElement^>(nullObject))
	{
		_outputWindowPane->OutputString("\tParent: ");
		_outputWindowPane->OutputString(parentCodeElement->FullName);
	}
	else
	{
		_outputWindowPane->OutputString("\tParent: ");
		_outputWindowPane->OutputString(parentProjectItem->FileNames[0]);
	}
	_outputWindowPane->OutputString("\n");
}

//WindowVisibilityEvents
void MCPPEvents::Connect::WindowHiding(Window ^pWindow)
{
	_outputWindowPane->OutputString("WindowVisibilityEvents, WindowHiding\n");
	_outputWindowPane->OutputString("\tWindow: " + pWindow->default+ "\n");
}

void MCPPEvents::Connect::WindowShowing(Window ^pWindow)
{
	_outputWindowPane->OutputString("WindowVisibilityEvents, WindowShowing\n");
	_outputWindowPane->OutputString("\tWindow: " + pWindow->default+ "\n");
}

//DebuggerProcessEvents
void MCPPEvents::Connect::OnProcessStateChanged(Process ^NewProcess, dbgProcessState processState)
{
	_outputWindowPane->OutputString("DebuggerProcessEvents, OnProcessStateChanged\n");
	_outputWindowPane->OutputString("\tNew Process: " + NewProcess->default + "\n");
	_outputWindowPane->OutputString("\tProcess State: " + processState.ToString() + "\n");
}

//DebuggerExpressionEvaluationEvents
void MCPPEvents::Connect::OnExpressionEvaluation(Process ^pProcess, Thread ^thread, dbgExpressionEvaluationState processState)
{
	_outputWindowPane->OutputString("DebuggerExpressionEvaluationEvents, OnExpressionEvaluation\n");
	_outputWindowPane->OutputString("\tProcess: " + pProcess->default + "\n");
	_outputWindowPane->OutputString("\tThread: " + thread->default + "\n");
	_outputWindowPane->OutputString("\tExpression Evaluation State: " + processState.ToString() + "\n");
}

//PublishEvents
void MCPPEvents::Connect::OnPublishBegin(bool %Continue)
{
	_outputWindowPane->OutputString("PublishEvents, OnPublishBegin\n");
	Continue = true;
}

void MCPPEvents::Connect::OnPublishDone(bool Success)
{
	_outputWindowPane->OutputString("PublishEvents, OnPublishDone\n");
	_outputWindowPane->OutputString("\tSuccess: " + Success.ToString() + "\n");
}