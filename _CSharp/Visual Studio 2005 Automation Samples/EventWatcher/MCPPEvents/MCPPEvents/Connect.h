//Copyright (c) Microsoft Corporation.  All rights reserved.

#pragma once

using namespace System;
using namespace Extensibility;
using namespace EnvDTE;
using namespace EnvDTE80;

namespace MCPPEvents
{
	/// <summary>The object for implementing an Add-in.</summary>
	/// <seealso class='IDTExtensibility2' />
	public ref class Connect : public IDTExtensibility2
	{

	public:
		/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
		Connect()
		{
		}

		/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />	
		virtual void OnAddInsUpdate(System::Array ^%custom);

		/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		virtual void OnBeginShutdown(System::Array ^%custom);

		/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
		/// <param term='application'>Root object of the host application.</param>
		/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
		/// <param term='addInInst'>Object representing this Add-in.</param>
		/// <seealso class='IDTExtensibility2' />
		virtual void OnConnection(System::Object ^Application, ext_ConnectMode ConnectMode, System::Object ^AddInInst, System::Array ^%custom);

		/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		virtual void OnStartupComplete(System::Array ^%custom);

		/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
		/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		virtual void OnDisconnection(ext_DisconnectMode removeMode, System::Array ^%custom);

	private:
		DTE2 ^_applicationObject;
		AddIn ^_addInInstance;

		WindowEvents ^_windowsEvents;
        TextEditorEvents ^_textEditorEvents;
        TaskListEvents ^_taskListEvents;
        SolutionEvents ^_solutionEvents;
        SelectionEvents ^_selectionEvents;
        OutputWindowEvents ^_outputWindowEvents;
        FindEvents ^_findEvents;
        DTEEvents ^_dteEvents;
        DocumentEvents ^_documentEvents;
        DebuggerEvents ^_debuggerEvents;
        CommandEvents ^_commandEvents;
        BuildEvents ^_buildEvents;
        ProjectItemsEvents ^_miscFilesEvents;
        ProjectItemsEvents ^_solutionItemsEvents;
		ProjectItemsEvents ^_globalProjectItemsEvents;
		ProjectsEvents ^_globalProjectsEvents;
		TextDocumentKeyPressEvents ^_textDocumentKeyPressEvents;
		CodeModelEvents ^_codeModelEvents;
		WindowVisibilityEvents ^_windowVisibilityEvents;
		DebuggerProcessEvents ^_debuggerProcessEvents;
		DebuggerExpressionEvaluationEvents ^_debuggerExpressionEvaluationEvents;
		PublishEvents ^_publishEvents;

        OutputWindowPane ^_outputWindowPane;

	public:
        //WindowEvents
        void WindowClosing(Window ^closingWindow);
        void WindowActivated(Window ^gotFocus, Window ^lostFocus);
        void WindowCreated(Window ^window);
        void WindowMoved(Window ^window, int top, int left, int width, int height);

        //TextEditorEvents
        void LineChanged(TextPoint ^startPoint, TextPoint ^endPoint, int hint);

        //TaskListEvents
        void TaskAdded(TaskItem ^taskItem);
        void TaskModified(TaskItem ^taskItem, vsTaskListColumn columnModified);
        void TaskNavigated(TaskItem ^taskItem, bool % navigateHandled);
        void TaskRemoved(TaskItem ^taskItem);

        //SolutionEvents
        void AfterClosing();
        void BeforeClosing();
        void Opened();
        void ProjectAdded(Project ^project);
        void ProjectRemoved(Project ^project);
        void ProjectRenamed(Project ^project, String ^oldName);
        void QueryCloseSolution(bool %cancel);
        void Renamed(String ^oldName);

        //SelectionEvents
        void OnChange();

        //OutputWindowEvents
        void PaneAdded(OutputWindowPane ^pane);
        void PaneClearing(OutputWindowPane ^pane);
        void PaneUpdated(OutputWindowPane ^pane);

        //FindEvents
        void FindDone(vsFindResult result, bool cancelled);

        //DTEEvents
        void ModeChanged(vsIDEMode LastMode);
        void OnBeginShutdown();
        void OnMacrosRuntimeReset();
        void OnStartupComplete();

        //DocumentEvents
        void DocumentClosing(Document ^document);
        void DocumentOpened(Document ^document);
        void DocumentOpening(String ^documentPath, bool ReadOnly);
        void DocumentSaved(Document ^document);

        //DebuggerEvents
        void OnContextChanged(Process ^NewProcess, Program ^NewProgram, Thread ^NewThread, StackFrame ^NewStackFrame);
        void OnEnterBreakMode(dbgEventReason reason, dbgExecutionAction %executionAction);
        void OnEnterDesignMode(dbgEventReason Reason);
        void OnEnterRunMode(dbgEventReason Reason);
        void OnExceptionNotHandled(String ^exceptionType, String ^name, int code, String ^description, dbgExceptionAction %exceptionAction);
        void OnExceptionThrown(String ^exceptionType, String ^name, int code, String ^description, dbgExceptionAction %exceptionAction);

        //CommandEvents
        void AfterExecute(String ^Guid, int ID, Object ^CustomIn, Object ^CustomOut);
        void BeforeExecute(String ^Guid, int ID, Object ^CustomIn, Object ^ CustomOut, bool %CancelDefault);

        //BuildEvents
        void OnBuildBegin(vsBuildScope Scope, vsBuildAction Action);
        void OnBuildDone(vsBuildScope Scope, vsBuildAction Action);
        void OnBuildProjConfigBegin(String ^project, String ^projectConfig, String ^platform, String ^solutionConfig);
        void OnBuildProjConfigDone(String ^project, String ^projectConfig, String ^platform, String ^solutionConfig, bool success);

        //MiscFilesEvents
        void MiscFilesEvents_ItemAdded(ProjectItem ^projectItem);
        void MiscFilesEvents_ItemRemoved(ProjectItem ^projectItem);
        void MiscFilesEvents_ItemRenamed(ProjectItem ^projectItem, String ^OldName);

        //SolutionItemsEvents
        void SolutionItemsEvents_ItemAdded(ProjectItem ^projectItem);
        void SolutionItemsEvents_ItemRemoved(ProjectItem ^projectItem);
        void SolutionItemsEvents_ItemRenamed(ProjectItem ^projectItem, String ^OldName);

		//Global ProjectItemsEvents
		void GlobalProjectItemsEvents_ItemAdded(ProjectItem ^projectItem);
		void GlobalProjectItemsEvents_ItemRemoved(ProjectItem ^projectItem);
		void GlobalProjectItemsEvents_ItemRenamed(ProjectItem ^projectItem, String ^OldName);

		//Global ProjectsEvents
		void GlobalProjectsEvents_ItemAdded(Project ^project);
		void GlobalProjectsEvents_ItemRemoved(Project ^project);
		void GlobalProjectsEvents_ItemRenamed(Project ^project, String ^OldName);

		//TextDocumentKeyPressEvents
		void BeforeKeyPress(String ^Keypress, TextSelection ^Selection, bool InStatementCompletion, bool %CancelKeypress);
		void AfterKeyPress(String ^Keypress, TextSelection ^Selection, bool InStatementCompletion);

		//CodeModelEvents
		void ElementAdded(CodeElement ^Element);
		void ElementChanged(CodeElement ^Element, vsCMChangeKind Change);
		void ElementDeleted(Object ^Parent, CodeElement ^Element);

		//WindowVisibilityEvents
		void WindowHiding(Window ^pWindow);
		void WindowShowing(Window ^pWindow);

		//DebuggerProcessEvents
		void OnProcessStateChanged(Process ^NewProcess, dbgProcessState processState);

		//DebuggerExpressionEvaluationEvents
		void OnExpressionEvaluation(Process ^pProcess, Thread ^thread, dbgExpressionEvaluationState processState);

		//PublishEvents
		void OnPublishBegin(bool %Continue);
		void OnPublishDone(bool Success);
	};
};
