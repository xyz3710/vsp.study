'Copyright (c) Microsoft Corporation.  All rights reserved.

Imports System
Imports Microsoft.VisualStudio.CommandBars
Imports Extensibility
Imports EnvDTE
Imports EnvDTE80

Public Class Connect

    Implements IDTExtensibility2


    Dim _applicationObject As DTE2
    Dim _addInInstance As AddIn

    Public WithEvents _windowsEvents As EnvDTE.WindowEvents
    Public WithEvents _textEditorEvents As EnvDTE.TextEditorEvents
    Public WithEvents _taskListEvents As EnvDTE.TaskListEvents
    Public WithEvents _solutionEvents As EnvDTE.SolutionEvents
    Public WithEvents _selectionEvents As EnvDTE.SelectionEvents
    Public WithEvents _outputWindowEvents As EnvDTE.OutputWindowEvents
    Public WithEvents _findEvents As EnvDTE.FindEvents
    Public WithEvents _dteEvents As EnvDTE.DTEEvents
    Public WithEvents _documentEvents As EnvDTE.DocumentEvents
    Public WithEvents _debuggerEvents As EnvDTE.DebuggerEvents
    Public WithEvents _commandEvents As EnvDTE.CommandEvents
    Public WithEvents _buildEvents As EnvDTE.BuildEvents
    Public WithEvents _miscFilesEvents As EnvDTE.ProjectItemsEvents
    Public WithEvents _solutionItemsEvents As EnvDTE.ProjectItemsEvents

    Public WithEvents _textDocumentKeyPressEvents As EnvDTE80.TextDocumentKeyPressEvents
    Public WithEvents _codeModelEvents As EnvDTE80.CodeModelEvents
    Public WithEvents _windowVisibilityEvents As EnvDTE80.WindowVisibilityEvents
    Public WithEvents _debuggerProcessEvents As EnvDTE80.DebuggerProcessEvents
    Public WithEvents _debuggerExpressionEvaluationEvents As EnvDTE80.DebuggerExpressionEvaluationEvents
    Public WithEvents _publishEvents As EnvDTE80.PublishEvents

    Private _outputWindowPane As OutputWindowPane

    '''<summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
    Public Sub New()

    End Sub

    '''<summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
    '''<param name='application'>Root object of the host application.</param>
    '''<param name='connectMode'>Describes how the Add-in is being loaded.</param>
    '''<param name='addInInst'>Object representing this Add-in.</param>
    '''<remarks></remarks>
    Public Sub OnConnection(ByVal application As Object, ByVal connectMode As ext_ConnectMode, ByVal addInInst As Object, ByRef custom As Array) Implements IDTExtensibility2.OnConnection
        _applicationObject = CType(application, DTE2)
        _addInInstance = CType(addInInst, AddIn)

        Dim events As EnvDTE.Events
        events = _applicationObject.Events
        Dim outputWindow As OutputWindow
        outputWindow = CType(_applicationObject.Windows.Item(Constants.vsWindowKindOutput).Object, EnvDTE.OutputWindow)
        _outputWindowPane = outputWindow.OutputWindowPanes.Add("DTE Event Information")

        'Retrieve the event objects from the automation model
        'VB will automatically connect the method handler since 
        '   the object variable declaration uses the 'WithEvents' handler
        _windowsEvents = CType(events.WindowEvents(Nothing), EnvDTE.WindowEvents)
        _textEditorEvents = CType(events.TextEditorEvents(Nothing), EnvDTE.TextEditorEvents)
        _taskListEvents = CType(events.TaskListEvents(""), EnvDTE.TaskListEvents)
        _solutionEvents = CType(events.SolutionEvents, EnvDTE.SolutionEvents)
        _selectionEvents = CType(events.SelectionEvents, EnvDTE.SelectionEvents)
        _outputWindowEvents = CType(events.OutputWindowEvents(""), EnvDTE.OutputWindowEvents)
        _findEvents = CType(events.FindEvents, EnvDTE.FindEvents)
        _dteEvents = CType(events.DTEEvents, EnvDTE.DTEEvents)
        _documentEvents = CType(events.DocumentEvents(Nothing), EnvDTE.DocumentEvents)
        _debuggerEvents = CType(events.DebuggerEvents, EnvDTE.DebuggerEvents)
        _commandEvents = CType(events.CommandEvents("{00000000-0000-0000-0000-000000000000}", 0), EnvDTE.CommandEvents)
        _buildEvents = CType(events.BuildEvents, EnvDTE.BuildEvents)
        _miscFilesEvents = CType(events.MiscFilesEvents, EnvDTE.ProjectItemsEvents)
        _solutionItemsEvents = CType(events.SolutionItemsEvents, EnvDTE.ProjectItemsEvents)

        _textDocumentKeyPressEvents = CType(CType(events, Events2).TextDocumentKeyPressEvents, EnvDTE80.TextDocumentKeyPressEvents)
        _codeModelEvents = CType(CType(events, Events2).CodeModelEvents, EnvDTE80.CodeModelEvents)
        _windowVisibilityEvents = CType(CType(events, Events2).WindowVisibilityEvents, EnvDTE80.WindowVisibilityEvents)
        _debuggerProcessEvents = CType(CType(events, Events2).DebuggerProcessEvents, EnvDTE80.DebuggerProcessEvents)
        _debuggerExpressionEvaluationEvents = CType(CType(events, Events2).DebuggerExpressionEvaluationEvents, EnvDTE80.DebuggerExpressionEvaluationEvents)
        _publishEvents = CType(CType(events, Events2).PublishEvents, EnvDTE80.PublishEvents)
    End Sub

    '''<summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
    '''<param name='disconnectMode'>Describes how the Add-in is being unloaded.</param>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnDisconnection(ByVal disconnectMode As ext_DisconnectMode, ByRef custom As Array) Implements IDTExtensibility2.OnDisconnection
        _windowsEvents = Nothing
        _textEditorEvents = Nothing
        _taskListEvents = Nothing
        _solutionEvents = Nothing
        _selectionEvents = Nothing
        _outputWindowEvents = Nothing
        _findEvents = Nothing
        _dteEvents = Nothing
        _documentEvents = Nothing
        _debuggerEvents = Nothing
        _commandEvents = Nothing
        _buildEvents = Nothing
        _miscFilesEvents = Nothing
        _solutionItemsEvents = Nothing

        _textDocumentKeyPressEvents = Nothing
        _codeModelEvents = Nothing
        _windowVisibilityEvents = Nothing
        _debuggerProcessEvents = Nothing
        _debuggerExpressionEvaluationEvents = Nothing
        _publishEvents = Nothing
    End Sub

    '''<summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification that the collection of Add-ins has changed.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnAddInsUpdate(ByRef custom As Array) Implements IDTExtensibility2.OnAddInsUpdate
    End Sub

    '''<summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnStartupComplete(ByRef custom As Array) Implements IDTExtensibility2.OnStartupComplete
    End Sub

    '''<summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
    '''<param name='custom'>Array of parameters that are host application specific.</param>
    '''<remarks></remarks>
    Public Sub OnBeginShutdown(ByRef custom As Array) Implements IDTExtensibility2.OnBeginShutdown
    End Sub


    Private Sub windowsEvents_WindowActivated(ByVal GotFocus As EnvDTE.Window, ByVal LostFocus As EnvDTE.Window) Handles _windowsEvents.WindowActivated
        _outputWindowPane.OutputString("WindowEvents::WindowActivated" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Window receiving focus: " & GotFocus.Caption & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Window that lost focus: " & LostFocus.Caption & ControlChars.Lf)
    End Sub

    Private Sub windowsEvents_WindowClosing(ByVal Window As EnvDTE.Window) Handles _windowsEvents.WindowClosing
        _outputWindowPane.OutputString("WindowEvents::WindowClosing" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Window: " & Window.Caption & ControlChars.Lf)
    End Sub

    Private Sub windowsEvents_WindowCreated(ByVal Window As EnvDTE.Window) Handles _windowsEvents.WindowCreated
        _outputWindowPane.OutputString("WindowEvents::WindowCreated" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Window: " & Window.Caption & ControlChars.Lf)
    End Sub

    Private Sub windowsEvents_WindowMoved(ByVal Window As EnvDTE.Window, ByVal Top As Integer, ByVal Left As Integer, ByVal [Width] As Integer, ByVal Height As Integer) Handles _windowsEvents.WindowMoved
        _outputWindowPane.OutputString("WindowEvents::WindowMoved" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Window: " & Window.Caption & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Location: (" & Top.ToString() & " , " & Left.ToString() & " , " & Width.ToString() & " , " & Height.ToString() & ")" & ControlChars.Lf)
    End Sub

    Private Sub textEditorEvents_LineChanged(ByVal StartPoint As EnvDTE.TextPoint, ByVal EndPoint As EnvDTE.TextPoint, ByVal Hint As Integer) Handles _textEditorEvents.LineChanged
        Dim textChangedHint As EnvDTE.vsTextChanged = CType(Hint, EnvDTE.vsTextChanged)
        _outputWindowPane.OutputString("TextEditorEvents::LineChanged" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Document: " & StartPoint.Parent.Parent.Name & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Change hint: " & textChangedHint.ToString() & ControlChars.Lf)
    End Sub

    Private Sub taskListEvents_TaskAdded(ByVal TaskItem As EnvDTE.TaskItem) Handles _taskListEvents.TaskAdded
        _outputWindowPane.OutputString("TaskListEvents::TaskAdded" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Task description: " & TaskItem.Description & ControlChars.Lf)
    End Sub

    Private Sub taskListEvents_TaskModified(ByVal TaskItem As EnvDTE.TaskItem, ByVal ColumnModified As EnvDTE.vsTaskListColumn) Handles _taskListEvents.TaskModified
        _outputWindowPane.OutputString("TaskListEvents::TaskModified" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Task description: " & TaskItem.Description & ControlChars.Lf)
    End Sub

    Private Sub taskListEvents_TaskNavigated(ByVal TaskItem As EnvDTE.TaskItem, ByRef NavigateHandled As Boolean) Handles _taskListEvents.TaskNavigated
        _outputWindowPane.OutputString("TaskListEvents::TaskNavigated" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Task description: " & TaskItem.Description & ControlChars.Lf)
    End Sub

    Private Sub taskListEvents_TaskRemoved(ByVal TaskItem As EnvDTE.TaskItem) Handles _taskListEvents.TaskRemoved
        _outputWindowPane.OutputString("TaskListEvents::TaskRemoved" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Task description: " & TaskItem.Description & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_AfterClosing() Handles _solutionEvents.AfterClosing
        _outputWindowPane.OutputString("SolutionEvents::AfterClosing" & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_BeforeClosing() Handles _solutionEvents.BeforeClosing
        _outputWindowPane.OutputString("SolutionEvents::BeforeClosing" & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_Opened() Handles _solutionEvents.Opened
        _outputWindowPane.OutputString("SolutionEvents::Opened" & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_ProjectAdded(ByVal Project As EnvDTE.Project) Handles _solutionEvents.ProjectAdded
        _outputWindowPane.OutputString("SolutionEvents::ProjectAdded" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project: " & Project.UniqueName & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_ProjectRemoved(ByVal Project As EnvDTE.Project) Handles _solutionEvents.ProjectRemoved
        _outputWindowPane.OutputString("SolutionEvents::ProjectRemoved" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project: " & Project.UniqueName & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_ProjectRenamed(ByVal Project As EnvDTE.Project, ByVal OldName As String) Handles _solutionEvents.ProjectRenamed
        _outputWindowPane.OutputString("SolutionEvents::ProjectRenamed" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project: " & Project.UniqueName & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_QueryCloseSolution(ByRef fCancel As Boolean) Handles _solutionEvents.QueryCloseSolution
        _outputWindowPane.OutputString("SolutionEvents::QueryCloseSolution" & ControlChars.Lf)
    End Sub

    Private Sub solutionEvents_Renamed(ByVal OldName As String) Handles _solutionEvents.Renamed
        _outputWindowPane.OutputString("SolutionEvents::Renamed" & ControlChars.Lf)
    End Sub

    Private Sub selectionEvents_OnChange() Handles _selectionEvents.OnChange
        _outputWindowPane.OutputString("SelectionEvents::OnChange" & ControlChars.Lf)
        Dim count As Integer = _applicationObject.SelectedItems.Count
        Dim i As Integer
        For i = 1 To _applicationObject.SelectedItems.Count
            _outputWindowPane.OutputString("Item name: " & _applicationObject.SelectedItems.Item(i).Name & ControlChars.Lf)
        Next
    End Sub

    Private Sub outputWindowEvents_PaneAdded(ByVal pane As EnvDTE.OutputWindowPane) Handles _outputWindowEvents.PaneAdded
        _outputWindowPane.OutputString("OutputWindowEvents::PaneAdded" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Pane: " & pane.Name & ControlChars.Lf)
    End Sub

    Private Sub outputWindowEvents_PaneClearing(ByVal pane As EnvDTE.OutputWindowPane) Handles _outputWindowEvents.PaneClearing
        _outputWindowPane.OutputString("OutputWindowEvents::PaneClearing" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Pane: " & pane.Name & ControlChars.Lf)
    End Sub

    Private Sub outputWindowEvents_PaneUpdated(ByVal pPane As EnvDTE.OutputWindowPane) Handles _outputWindowEvents.PaneUpdated
        'Dont want to do this one, or we will end up in a recursive call:
        'outputWindowPane.OutputString("OutputWindowEvents::PaneUpdated" & ControlChars.Lf)
        'outputWindowPane.OutputString(ControlChars.Tab & "Pane: " & pane.Name & ControlChars.Lf)
    End Sub

    Private Sub findEvents_FindDone(ByVal Result As EnvDTE.vsFindResult, ByVal Cancelled As Boolean) Handles _findEvents.FindDone
        _outputWindowPane.OutputString("FindEvents::FindDone" & ControlChars.Lf)
    End Sub

    Private Sub dteEvents_ModeChanged(ByVal LastMode As EnvDTE.vsIDEMode) Handles _dteEvents.ModeChanged
        _outputWindowPane.OutputString("DTEEvents::ModeChanged" & ControlChars.Lf)
    End Sub

    Private Sub dteEvents_OnBeginShutdown() Handles _dteEvents.OnBeginShutdown
        _outputWindowPane.OutputString("DTEEvents::OnBeginShutdown" & ControlChars.Lf)
    End Sub

    Private Sub dteEvents_OnMacrosRuntimeReset() Handles _dteEvents.OnMacrosRuntimeReset
        _outputWindowPane.OutputString("DTEEvents::OnMacrosRuntimeReset" & ControlChars.Lf)
    End Sub

    Private Sub dteEvents_OnStartupComplete() Handles _dteEvents.OnStartupComplete
        _outputWindowPane.OutputString("DTEEvents::OnStartupComplete" & ControlChars.Lf)
    End Sub

    Private Sub documentEvents_DocumentClosing(ByVal Document As EnvDTE.Document) Handles _documentEvents.DocumentClosing
        _outputWindowPane.OutputString("DocumentEvents::DocumentClosing" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Document: " & Document.Name & ControlChars.Lf)
    End Sub

    Private Sub documentEvents_DocumentOpened(ByVal Document As EnvDTE.Document) Handles _documentEvents.DocumentOpened
        _outputWindowPane.OutputString("DocumentEvents::DocumentOpened" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Document: " & Document.Name & ControlChars.Lf)
    End Sub

    Private Sub documentEvents_DocumentOpening(ByVal DocumentPath As String, ByVal [ReadOnly] As Boolean) Handles _documentEvents.DocumentOpening
        _outputWindowPane.OutputString("DocumentEvents::DocumentOpening" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Path: " & DocumentPath & ControlChars.Lf)
    End Sub

    Private Sub documentEvents_DocumentSaved(ByVal Document As EnvDTE.Document) Handles _documentEvents.DocumentSaved
        _outputWindowPane.OutputString("DocumentEvents::DocumentSaved" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Document: " & Document.Name & ControlChars.Lf)
    End Sub

    Private Sub debuggerEvents_OnContextChanged(ByVal NewProcess As EnvDTE.Process, ByVal NewProgram As EnvDTE.Program, ByVal NewThread As EnvDTE.Thread, ByVal NewStackFrame As EnvDTE.StackFrame) Handles _debuggerEvents.OnContextChanged
        _outputWindowPane.OutputString("DebuggerEvents::OnContextChanged" & ControlChars.Lf)
    End Sub

    Private Sub debuggerEvents_OnEnterBreakMode(ByVal Reason As EnvDTE.dbgEventReason, ByRef ExecutionAction As EnvDTE.dbgExecutionAction) Handles _debuggerEvents.OnEnterBreakMode
        ExecutionAction = EnvDTE.dbgExecutionAction.dbgExecutionActionDefault
        _outputWindowPane.OutputString("DebuggerEvents::OnEnterBreakMode" & ControlChars.Lf)
    End Sub

    Private Sub debuggerEvents_OnEnterDesignMode(ByVal Reason As EnvDTE.dbgEventReason) Handles _debuggerEvents.OnEnterDesignMode
        _outputWindowPane.OutputString("DebuggerEvents::OnEnterDesignMode" & ControlChars.Lf)
    End Sub

    Private Sub debuggerEvents_OnEnterRunMode(ByVal Reason As EnvDTE.dbgEventReason) Handles _debuggerEvents.OnEnterRunMode
        _outputWindowPane.OutputString("DebuggerEvents::OnEnterRunMode" & ControlChars.Lf)
    End Sub

    Private Sub debuggerEvents_OnExceptionNotHandled(ByVal ExceptionType As String, ByVal [Name] As String, ByVal Code As Integer, ByVal Description As String, ByRef ExceptionAction As EnvDTE.dbgExceptionAction) Handles _debuggerEvents.OnExceptionNotHandled
        ExceptionAction = EnvDTE.dbgExceptionAction.dbgExceptionActionDefault
        _outputWindowPane.OutputString("DebuggerEvents::OnExceptionNotHandled" & ControlChars.Lf)
    End Sub

    Private Sub debuggerEvents_OnExceptionThrown(ByVal ExceptionType As String, ByVal [Name] As String, ByVal Code As Integer, ByVal Description As String, ByRef ExceptionAction As EnvDTE.dbgExceptionAction) Handles _debuggerEvents.OnExceptionThrown
        ExceptionAction = EnvDTE.dbgExceptionAction.dbgExceptionActionDefault
        _outputWindowPane.OutputString("DebuggerEvents::OnExceptionThrown" & ControlChars.Lf)
    End Sub

    Private Sub commandEvents_AfterExecute(ByVal Guid As String, ByVal ID As Integer, ByVal CustomIn As Object, ByVal CustomOut As Object) Handles _commandEvents.AfterExecute
        Dim commandName As String
        Try
            commandName = _applicationObject.Commands.Item(Guid, ID).Name
        Catch excep As System.Exception
        End Try
        _outputWindowPane.OutputString("CommandEvents::AfterExecute" & ControlChars.Lf)
        If (commandName <> "") Then
            _outputWindowPane.OutputString(ControlChars.Tab & "Command name: " & commandName & ControlChars.Lf)
        End If
        _outputWindowPane.OutputString(ControlChars.Tab & "Command GUID/ID: " & Guid & ", " & ID.ToString() & ControlChars.Lf)
    End Sub

    Private Sub commandEvents_BeforeExecute(ByVal Guid As String, ByVal ID As Integer, ByVal CustomIn As Object, ByVal CustomOut As Object, ByRef CancelDefault As Boolean) Handles _commandEvents.BeforeExecute
        Dim commandName As String
        Try
            commandName = _applicationObject.Commands.Item(Guid, ID).Name
        Catch excep As System.Exception
        End Try
        _outputWindowPane.OutputString("CommandEvents::BeforeExecute" & ControlChars.Lf)
        If (commandName <> "") Then
            _outputWindowPane.OutputString(ControlChars.Tab & "Command name: " & commandName & ControlChars.Lf)
        End If
        _outputWindowPane.OutputString(ControlChars.Tab & "Command GUID/ID: " & Guid & ", " & ID.ToString() & ControlChars.Lf)
    End Sub

    Private Sub buildEvents_OnBuildBegin(ByVal Scope As EnvDTE.vsBuildScope, ByVal Action As EnvDTE.vsBuildAction) Handles _buildEvents.OnBuildBegin
        _outputWindowPane.OutputString("BuildEvents::OnBuildBegin" & ControlChars.Lf)
    End Sub

    Private Sub buildEvents_OnBuildDone(ByVal Scope As EnvDTE.vsBuildScope, ByVal Action As EnvDTE.vsBuildAction) Handles _buildEvents.OnBuildDone
        _outputWindowPane.OutputString("BuildEvents::OnBuildDone" & ControlChars.Lf)
    End Sub

    Private Sub buildEvents_OnBuildProjConfigBegin(ByVal Project As String, ByVal ProjectConfig As String, ByVal Platform As String, ByVal SolutionConfig As String) Handles _buildEvents.OnBuildProjConfigBegin
        _outputWindowPane.OutputString("BuildEvents::OnBuildProjConfigBegin" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project: " & Project & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Configuration: " & ProjectConfig & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Platform: " & Platform & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Solution Configuration: " & SolutionConfig & ControlChars.Lf)
    End Sub

    Private Sub buildEvents_OnBuildProjConfigDone(ByVal Project As String, ByVal ProjectConfig As String, ByVal Platform As String, ByVal SolutionConfig As String, ByVal Success As Boolean) Handles _buildEvents.OnBuildProjConfigDone
        _outputWindowPane.OutputString("BuildEvents::OnBuildProjConfigDone" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project: " & Project & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Configuration: " & ProjectConfig & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Platform: " & Platform & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Solution Configuration: " & SolutionConfig & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Build success: " & Success.ToString() & ControlChars.Lf)
    End Sub

    Private Sub miscFilesEvents_ItemAdded(ByVal ProjectItem As EnvDTE.ProjectItem) Handles _miscFilesEvents.ItemAdded
        _outputWindowPane.OutputString("MiscFilesEvents::ItemAdded" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Item: " & ProjectItem.Name & ControlChars.Lf)
    End Sub

    Private Sub miscFilesEvents_ItemRemoved(ByVal ProjectItem As EnvDTE.ProjectItem) Handles _miscFilesEvents.ItemRemoved
        _outputWindowPane.OutputString("MiscFilesEvents::ItemRemoved" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Item: " & ProjectItem.Name & ControlChars.Lf)
    End Sub

    Private Sub miscFilesEvents_ItemRenamed(ByVal ProjectItem As EnvDTE.ProjectItem, ByVal OldName As String) Handles _miscFilesEvents.ItemRenamed
        _outputWindowPane.OutputString("MiscFilesEvents::ItemRenamed" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Item: " & ProjectItem.Name & ControlChars.Lf)
    End Sub

    Private Sub solutionItemsEvents_ItemAdded(ByVal ProjectItem As EnvDTE.ProjectItem) Handles _solutionItemsEvents.ItemAdded
        _outputWindowPane.OutputString("SolutionItemsEvents::ItemAdded" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Item: " & ProjectItem.Name & ControlChars.Lf)
    End Sub

    Private Sub solutionItemsEvents_ItemRemoved(ByVal ProjectItem As EnvDTE.ProjectItem) Handles _solutionItemsEvents.ItemRemoved
        _outputWindowPane.OutputString("SolutionItemsEvents::ItemRemoved" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Item: " & ProjectItem.Name & ControlChars.Lf)
    End Sub

    Private Sub solutionItemsEvents_ItemRenamed(ByVal ProjectItem As EnvDTE.ProjectItem, ByVal OldName As String) Handles _solutionItemsEvents.ItemRenamed
        _outputWindowPane.OutputString("SolutionItemsEvents::ItemRenamed" & ControlChars.Lf)
        _outputWindowPane.OutputString(ControlChars.Tab & "Project Item: " & ProjectItem.Name & ControlChars.Lf)
    End Sub

    Private Sub _textDocumentKeyPressEvents_AfterKeyPress(ByVal Keypress As String, ByVal Selection As EnvDTE.TextSelection, ByVal InStatementCompletion As Boolean) Handles _textDocumentKeyPressEvents.AfterKeyPress
        _outputWindowPane.OutputString("TextDocumentKeyPressEvents, AfterKeyPress\n")
        _outputWindowPane.OutputString("\tKey: " + Keypress + "\n")
        _outputWindowPane.OutputString("\tSelection: " + Selection.Text + "\n")
        _outputWindowPane.OutputString("\tInStatementCompletion: " + InStatementCompletion.ToString() + "\n")
    End Sub

    Private Sub _textDocumentKeyPressEvents_BeforeKeyPress(ByVal Keypress As String, ByVal Selection As EnvDTE.TextSelection, ByVal InStatementCompletion As Boolean, ByRef CancelKeypress As Boolean) Handles _textDocumentKeyPressEvents.BeforeKeyPress
        _outputWindowPane.OutputString("TextDocumentKeyPressEvents, BeforeKeyPress\n")
        _outputWindowPane.OutputString("\tKey: " + Keypress + "\n")
        _outputWindowPane.OutputString("\tSelection: " + Selection.Text + "\n")
        _outputWindowPane.OutputString("\tInStatementCompletion: " + InStatementCompletion.ToString() + "\n")
    End Sub

    Private Sub _codeModelEvents_ElementAdded(ByVal Element As EnvDTE.CodeElement) Handles _codeModelEvents.ElementAdded
        _outputWindowPane.OutputString("CodeModelEvents, ElementAdded\n")
        _outputWindowPane.OutputString("\tElement: " + Element.FullName + "\n")
    End Sub

    Private Sub _codeModelEvents_ElementChanged(ByVal Element As EnvDTE.CodeElement, ByVal Change As EnvDTE80.vsCMChangeKind) Handles _codeModelEvents.ElementChanged
        _outputWindowPane.OutputString("CodeModelEvents, ElementChanged\n")
        _outputWindowPane.OutputString("\tElement: " + Element.FullName + "\n")
        _outputWindowPane.OutputString("\tChange: " + Change.ToString() + "\n")
    End Sub

    Private Sub _codeModelEvents_ElementDeleted(ByVal Parent As Object, ByVal Element As EnvDTE.CodeElement) Handles _codeModelEvents.ElementDeleted
        _outputWindowPane.OutputString("CodeModelEvents, ElementDeleted\n")
        _outputWindowPane.OutputString("\tElement: " + Element.FullName + "\n")
        If TypeOf (Parent) Is CodeElement Then
            _outputWindowPane.OutputString("\tParent: " + CType(Parent, CodeElement).FullName + "\n")
        ElseIf TypeOf (Parent) Is ProjectItem Then
            _outputWindowPane.OutputString("\tParent: " + CType(Parent, ProjectItem).FileNames(0) + "\n")
        End If
    End Sub

    Private Sub _windowVisibilityEvents_WindowHiding(ByVal Window As EnvDTE.Window) Handles _windowVisibilityEvents.WindowHiding
        _outputWindowPane.OutputString("WindowVisibilityEvents, WindowHiding\n")
        _outputWindowPane.OutputString("\tWindow: " + Window.Caption + "\n")
    End Sub

    Private Sub _windowVisibilityEvents_WindowShowing(ByVal Window As EnvDTE.Window) Handles _windowVisibilityEvents.WindowShowing
        _outputWindowPane.OutputString("WindowVisibilityEvents, WindowShowing\n")
        _outputWindowPane.OutputString("\tWindow: " + Window.Caption + "\n")
    End Sub

    Private Sub _debuggerProcessEvents_OnProcessStateChanged(ByVal NewProcess As EnvDTE.Process, ByVal processState As EnvDTE80.dbgProcessState) Handles _debuggerProcessEvents.OnProcessStateChanged
        _outputWindowPane.OutputString("DebuggerProcessEvents, OnProcessStateChanged\n")
        _outputWindowPane.OutputString("\tNew Process: " + NewProcess.Name + "\n")
        _outputWindowPane.OutputString("\tProcess State: " + processState.ToString() + "\n")
    End Sub

    Private Sub _debuggerExpressionEvaluationEvents_OnExpressionEvaluation(ByVal pProcess As EnvDTE.Process, ByVal Thread As EnvDTE.Thread, ByVal evaluationState As EnvDTE80.dbgExpressionEvaluationState) Handles _debuggerExpressionEvaluationEvents.OnExpressionEvaluation
        _outputWindowPane.OutputString("DebuggerExpressionEvaluationEvents, OnExpressionEvaluation\n")
        _outputWindowPane.OutputString("\tProcess: " + pProcess.Name + "\n")
        _outputWindowPane.OutputString("\tThread: " + Thread.Name + "\n")
        _outputWindowPane.OutputString("\tExpression Evaluation State: " + evaluationState.ToString() + "\n")
    End Sub

    Private Sub _publishEvents_OnPublishBegin(ByRef [Continue] As Boolean) Handles _publishEvents.OnPublishBegin
        _outputWindowPane.OutputString("PublishEvents, OnPublishBegin\n")
        [Continue] = True
    End Sub

    Private Sub _publishEvents_OnPublishDone(ByVal Success As Boolean) Handles _publishEvents.OnPublishDone
        _outputWindowPane.OutputString("PublishEvents, OnPublishDone\n")
        _outputWindowPane.OutputString("\tSuccess: " + Success.ToString() + "\n")
    End Sub
End Class
