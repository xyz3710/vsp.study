using System;


// This exception provides the base for
// our CRM system. In large projects
// it would be defined in its own assembly for
// ease of versioning and enhancement.
public class CRMSystemException : System.ApplicationException
{
	private string m_AppSource;

	public CRMSystemException(string message)
		: base(message)
	{
		this.m_AppSource = "SomeCompany CRM System";
	}

	// We only want this method exposed to code
	// in the same assembly. However, we might need to 
	// change the scope if this class were in another
	// assembly.

	internal void LogError()
	{
		System.Diagnostics.EventLog e;
		e = new System.Diagnostics.EventLog("Application");
		e.Source = this.AppSource;
		e.WriteEntry(this.Message, System.Diagnostics.EventLogEntryType.Error);
		e.Dispose();
	}

	// We're exposing a generic 'company' source
	// that children can override if they want.

	public string AppSource
	{
		get
		{
			return m_AppSource;
		}
	}

}
