using System;


// Base exception for our Customer module.
// All Customer exceptions will expose a Customer
// reference via the read only property CustomerInfo.
// Note however in some cases it will be null.

public class CustomerException : CRMSystemException
{

	private string m_AppSource;
	private Customer m_Customer;

	public CustomerException(string message, Customer ReqCustomer)
		: base(message)
	{
		this.m_Customer = ReqCustomer;
		this.m_AppSource = "SomeCompany CRM Customer Module";
	}

	public Customer CustomerInfo
	{
		get
		{
			return this.m_Customer;
		}
	}

	// We wan't exceptions at this level to use
	// our AppSource, not our parents which becomes
	// important when someone calls LogError.

	public string AppSource
	{
		get
		{
			return this.m_AppSource;
		}
	}
}
