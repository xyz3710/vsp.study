using System;


// Custom exceptions don't need to be complicated.
// Note this exeption might be defined in another assembly.
public class EmployeeException : CRMSystemException
{
	public EmployeeException(string message)
		: base(message)
	{
	}
}
