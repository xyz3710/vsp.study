using System;


// Simple exception
public class CustomerNotFoundException : CustomerException
{

	public CustomerNotFoundException(string Message)
		: base(Message, null)
	{
		// We pass null for the Customer
		// since we couldn't find them.
	}
}
