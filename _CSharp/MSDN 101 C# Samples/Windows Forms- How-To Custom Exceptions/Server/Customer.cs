//Copyright (C) 2002 Microsoft Corporation

//All rights reserved.

//THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER 

//EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF 

//MERCHANTIBILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

//Requires the Trial or Release version of Visual Studio .NET Professional (or greater).


using System;

public class Customer
{

	// In real life we'd use property procedures.

	public int Id;
	public string FirstName;
	public string LastName;

	public static Customer EditCustomer(int Id)
	{
		// Pretend we look for the customer in our database
		// We can't find it, so we notify our caller in a way in 
		// which they can't ignore us.
		string strMsg;
		strMsg = string.Format("The customer you requested by Id {0} could not be found.", Id);
		// Create the exception.
		CustomerNotFoundException exp = new CustomerNotFoundException(strMsg);
		// throw it to our caller
		throw exp;

	}

	public static void DeleteCustomer(int Id)
	{
		// Pretend we find the customer in the database
		// but realize we shouldn't delete them, maybe for
		// security reasons.
		Customer c = new Customer();
		c.Id = Id;
		c.FirstName = "Joe";
		c.LastName = "Smith";

		// This could fail if we don't have rights.

		string strUser;
		try 
		{
			strUser = System.Environment.UserDomainName + @"\" + System.Environment.UserName;
		}
		catch
		{
			strUser = "Unavailable";
		}

		string strMsg;
		strMsg = string.Format("The customer you requested {0} {1} could not be deleted. Your account '{2}' does not have permission.", c.FirstName, c.LastName, strUser);
		CustomerNotDeletedException exp = new CustomerNotDeletedException(strMsg, c, strUser);
		exp.LogError();
		throw exp;
	}

}

