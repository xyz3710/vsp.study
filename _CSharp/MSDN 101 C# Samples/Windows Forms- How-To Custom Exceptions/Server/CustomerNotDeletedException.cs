using System;


// This exception exposes a custom user id property
// that is initialized at construction.
public class CustomerNotDeletedException : CustomerException
{

	private string m_UserId;

	public CustomerNotDeletedException(string Message, Customer ReqCustomer, string UserId)
		: base(Message, ReqCustomer)
	{
		this.m_UserId = UserId;
	}

	public string UserId
	{
		get
		{
			return this.m_UserId;
		}
	}

}
