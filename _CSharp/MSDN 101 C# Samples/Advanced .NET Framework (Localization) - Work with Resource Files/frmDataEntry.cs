using System;
using System.Windows.Forms;
using System.Data;
using System.Globalization;

public class frmDataEntry : System.Windows.Forms.Form 
{
    private DataTable dt = new DataTable("Names");

#region " Windows Form Designer generated code "

    public frmDataEntry() 
	{
        //This call is required by the Windows Form Designer.
        InitializeComponent();
        //Add any initialization after the InitializeComponent() call
    }

    //Form overrides dispose to clean up the component list.
    protected override void Dispose(bool disposing) {
        if (disposing) {
            if (components != null) {
                components.Dispose();
            }
        }
        base.Dispose(disposing);
    }

    //Required by the Windows Form Designer
    private System.ComponentModel.IContainer components = null;
    //NOTE: The following procedure is required by the Windows Form Designer
    //It can be modified using the Windows Form Designer.  
    //Do not modify it using the code editor.

    private System.Windows.Forms.GroupBox GroupBox1;

    private System.Windows.Forms.Button cmdSave;

    private System.Windows.Forms.TextBox txtCity;

    private System.Windows.Forms.TextBox txtName;

    private System.Windows.Forms.Label lblCity;

    private System.Windows.Forms.Label lblName;

    private System.Windows.Forms.DataGrid grdPeople;

    private System.Windows.Forms.Label lblWelcome;
	private PictureBox PictureBox1;

	private System.Windows.Forms.Button cmdQuit;

    private void InitializeComponent() {
		System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataEntry));
		this.GroupBox1 = new System.Windows.Forms.GroupBox();
		this.cmdSave = new System.Windows.Forms.Button();
		this.txtCity = new System.Windows.Forms.TextBox();
		this.txtName = new System.Windows.Forms.TextBox();
		this.lblCity = new System.Windows.Forms.Label();
		this.lblName = new System.Windows.Forms.Label();
		this.grdPeople = new System.Windows.Forms.DataGrid();
		this.lblWelcome = new System.Windows.Forms.Label();
		this.cmdQuit = new System.Windows.Forms.Button();
		this.PictureBox1 = new System.Windows.Forms.PictureBox();
		this.GroupBox1.SuspendLayout();
		((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
		((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
		this.SuspendLayout();
		// 
		// GroupBox1
		// 
		this.GroupBox1.Controls.Add(this.cmdSave);
		this.GroupBox1.Controls.Add(this.txtCity);
		this.GroupBox1.Controls.Add(this.txtName);
		this.GroupBox1.Controls.Add(this.lblCity);
		this.GroupBox1.Controls.Add(this.lblName);
		resources.ApplyResources(this.GroupBox1, "GroupBox1");
		this.GroupBox1.Name = "GroupBox1";
		this.GroupBox1.TabStop = false;
		// 
		// cmdSave
		// 
		resources.ApplyResources(this.cmdSave, "cmdSave");
		this.cmdSave.Name = "cmdSave";
		this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
		// 
		// txtCity
		// 
		resources.ApplyResources(this.txtCity, "txtCity");
		this.txtCity.Name = "txtCity";
		// 
		// txtName
		// 
		resources.ApplyResources(this.txtName, "txtName");
		this.txtName.Name = "txtName";
		// 
		// lblCity
		// 
		resources.ApplyResources(this.lblCity, "lblCity");
		this.lblCity.Name = "lblCity";
		// 
		// lblName
		// 
		resources.ApplyResources(this.lblName, "lblName");
		this.lblName.Name = "lblName";
		// 
		// grdPeople
		// 
		this.grdPeople.ColumnHeadersVisible = false;
		this.grdPeople.DataMember = "";
		this.grdPeople.HeaderForeColor = System.Drawing.SystemColors.ControlText;
		resources.ApplyResources(this.grdPeople, "grdPeople");
		this.grdPeople.Name = "grdPeople";
		this.grdPeople.PreferredColumnWidth = 90;
		this.grdPeople.ReadOnly = true;
		this.grdPeople.RowHeadersVisible = false;
		this.grdPeople.TabStop = false;
		// 
		// lblWelcome
		// 
		resources.ApplyResources(this.lblWelcome, "lblWelcome");
		this.lblWelcome.Name = "lblWelcome";
		// 
		// cmdQuit
		// 
		resources.ApplyResources(this.cmdQuit, "cmdQuit");
		this.cmdQuit.Name = "cmdQuit";
		this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
		// 
		// PictureBox1
		// 
		this.PictureBox1.Image = global::HowTo.Properties.Resources.PictureBox1_Image;
		resources.ApplyResources(this.PictureBox1, "PictureBox1");
		this.PictureBox1.Name = "PictureBox1";
		this.PictureBox1.TabStop = false;
		// 
		// frmDataEntry
		// 
		resources.ApplyResources(this, "$this");
		this.Controls.Add(this.PictureBox1);
		this.Controls.Add(this.GroupBox1);
		this.Controls.Add(this.grdPeople);
		this.Controls.Add(this.lblWelcome);
		this.Controls.Add(this.cmdQuit);
		this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
		this.Name = "frmDataEntry";
		this.Load += new System.EventHandler(this.frmDataEntry_Load);
		this.GroupBox1.ResumeLayout(false);
		this.GroupBox1.PerformLayout();
		((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
		((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
		this.ResumeLayout(false);

    }

#endregion

    private void cmdQuit_Click(object sender, System.EventArgs e) 
	{
        this.Close();
    }

    private void cmdSave_Click(object sender, System.EventArgs e) 
	{
        DataRow myDataRow;
        // Creates a new Row from the DataTable
        myDataRow = dt.NewRow();
        // Fills in the data
        myDataRow["Name"] = txtName.Text;
        myDataRow["City"] = txtCity.Text;
        //  Adds the new Row to the people DataTable
        dt.Rows.Add(myDataRow);
    }

    private void frmDataEntry_Load(object sender, System.EventArgs e) 
	{
        // Create the schema for the table of names.
        dt.Columns.Add("Name");
        dt.Columns.Add("City");
        grdPeople.DataSource = dt;
    }
}

