﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;

namespace RedmineTest
{
	class Program
	{
		static void Main(string[] args)
		{
			string host = "http://x10.mine.nu:3001/redmine";
			string apiKey = "353ad75dac7a9585ce66560709ac1f78957b6b82";

			var manager = new RedmineManager(host, apiKey);

			var parameters = new NameValueCollection { { "status_id", "*" } };
			foreach (var issue in manager.GetObjectList<Issue>(parameters))
			{
				Console.WriteLine("#{0}: {1}", issue.Id, issue.Subject);
			}

			//Create a issue.
			/*
			var newIssue = new Issue
			{
				Subject = "Create in REST API",
				Project = new IdentifiableName
				{
					Id = 1,
				},
				Tracker = new IdentifiableName
				{
					Id = 1,
				},
				Status = new IdentifiableName
				{
					Id = 1,
				},
				Priority = new IdentifiableName
				{
					Id = 4,
				},
				Author = new IdentifiableName
				{
					Id = 4,
				},
				Description = "설명 부분",
				StartDate = DateTime.Parse("2013-02-04"),
				EstimatedHours = 1.5f,
			};

			var resultIssue = manager.CreateObject(newIssue);
			string newIssueId = resultIssue.Id;
			*/

			string newIssueId = "470";
			var createdIssue = manager.GetObject<Issue>(newIssueId, null);

			//createdIssue.Status.Id = 2;		// 진행
			//createdIssue.DoneRatio = 1f;

			//manager.UpdateObject<Issue>(newIssueId, createdIssue);
		}
	}
}
