﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.Text;
using System.IO;

namespace JsonEx
{
	class Program
	{
		#region Constants
		private const string FILE_NAME = "data.json";
		private const string FILE_NAME2 = "data2.json";
		#endregion

		static void Main(string[] args)
		{
			Person me = new Person
			{
				Name = "김기원",
				Age = 39,
			};
			Person wife = new Person
			{
				Name = "황태영",
				Age = 38,
			};
			var persons = new List<Person>();
			persons.Add(me);
			persons.Add(wife);

			using (FileStream fs = new FileStream(FILE_NAME, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
			{
				JsonSerializer.SerializeToStream(me, fs);
				fs.Close();
			}

			using (FileStream fs = new FileStream(FILE_NAME, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
			{
				JsonSerializer.SerializeToStream(wife, fs);
				fs.Close();
			}

			using (FileStream fs = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				Person p = JsonSerializer.DeserializeFromStream<Person>(fs);
				Console.WriteLine(p);
				fs.Close();
			}

			var data = JsonSerializer.SerializeToString(persons);

			using (FileStream fs = new FileStream(FILE_NAME2, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
			{
				JsonSerializer.SerializeToStream(data, fs);
				fs.Close();
			}

		}
	}
}
