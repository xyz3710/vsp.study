﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace JsonEx
{
	[DebuggerDisplay("Name:{Name}, Age:{Age}", Name = "Person")]
	public class Person
	{
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}

		#region ToString
		/// <summary>
		/// Person를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("Name:{0}, Age:{1}", Name, Age);
		}
		#endregion
	}
}
