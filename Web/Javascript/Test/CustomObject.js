function myClass(p) {
	this.Comment = "사용자 정의 객체";
	this.v1 = p;		// this. 은 public 변수
	v2 = "텔미";		// this가 없다면 private 변수
}

function myClass_toString() {
	return "v1=" + this.v1;
}

function myClass_setValue(p) {
	this.v2 = p;
}

function myClass_getValue() {
	return this.v2;
}

myClass.prototype.toString = myClass_toString;
myClass.prototype.setValue = myClass_setValue;
myClass.prototype.getValue = myClass_getValue;
myClass.prototype.resetValue = function () {
	this.v1 = null;
	this.v2 = null;
};

var o = new myClass("1");	// create instance

alert(o.Comment);
alert(o.v1);	// 1
alert(o.v2);	// undefined
alert(o);		// v1=1
o.resetValue();
alert(o);		// v1=null
o.setValue("I am Private");
alert(o.getValue());	// I am Private