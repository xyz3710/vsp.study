﻿/*********************************************************************************************************************
:: Purpose	:	처음으로 만들어본 jQuery carousel plugin
:: Creator	:	Kim Ki Won
:: Create	:	2011년 9월 1일 목요일 오후 4:37:39
:: Modifier	:	
:: Update	:	
:: Comment	:	
::*******************************************************************************************************************/
function initiScrollCarousel(icWrapperId, icIndicatorId, icPageWidth, icPageHeight, options) {
/// <summary>
///     icWrapperId 아래 scroller라는 id의 div 태그를 가져야 한다.
/// </summary>
/// <param name="icWrapperId" type="String">
///     Carousel 전체를 감싸고 있는 div wrapper의 id
/// </param>
/// <param name="icIndicatorId" type="jQuery">
///     Carousel 하단의 indicator의 div id
/// </param>
/// <param name="icPageWidth" type="String">
///     Carousel 페이지 폭
/// </param>
/// <param name="icPageHeight" type="String">
///     Indicator를 제외한한 Carousel 페이지 높이
/// </param>
/// <returns type="jQuery" />
	var iScroller;
	var wrapper = $('#' + icWrapperId);
	var indicator = $('#' + icIndicatorId);
	var scroller = wrapper.find('#scroller');
	var indicatorItem = indicator.find('> li');
	var pageCount = indicatorItem.length;
	var that = this;

	that.options = {
		pageBackgroundColor: '#909090',
		overPageBackgroundColor: '#909090',			// 전체 페이지의 배경(스크롤이 오버했을 때 나타나는 배경)
		idcActiveColor: '#000',						// selected indicator's color
		idcActiveBackgroundColor: '#fff',			// selected indicator's background color
		idcColor: '#000',							// indicator's color
		idcBackgroundColor: '#c4c4c4'				// indicator's background color
	};

	/* CSS의 width, height, color 속성등을 변경한다. */
	// 전체 페이지
	wrapper
		.css('width', icPageWidth)
		.css('height', icPageHeight)
		.css('background-color', that.options.overPageBackgroundColor)
		.css({
			'position' : 'relative',
			'margin' : '0',
			'padding' : '0',
			'top' : '0',
			'left' : '0',
			'overflow' : 'hidden'
		});

	// 스크롤 되는 페이지 전체
	scroller.css('width', icPageWidth * pageCount)
		.css('height', icPageHeight)
		.css('background-color', that.options.pageBackgroundColor); // 페이지 배경색

	// 화면에 보여지는 스크롤 페이지
	scroller.find('li')
		.css('width', icPageWidth)
		.css('height', icPageHeight);

	// 화면에 보여지는 스크롤 페이지의 세부 항목
	var marginLeft = Math.round(icPageWidth * 0.08); // page_width의 8%
	var marginTop = Math.round(Math.min(icPageHeight * 0.06, 40)); // page_height의 6%, 또는 Indicator 높이보다 같거나 작게

	scroller.find('ul > li > div')
		.css('width', icPageWidth - marginLeft * 2)
		.css('height', icPageHeight - marginTop)
		.css('margin-left', marginLeft).css('margin-right', marginLeft)
		.css('margin-top', marginTop)
		.css('position', 'relative')
		.css('left', '0');

	// 스크롤 내용의 크기를 페이지 크기에 맞게 조절한다.
	scroller.find('.scrollContent')
		.css('width', icPageWidth - marginLeft * 2)
		.css('height', icPageHeight - marginTop);

	// Indicator의 위치 및 스타일 적용
	var paddingLeft = Math.round((icPageWidth - (18 * pageCount) - (10 * (pageCount - 1))) / 2);
	
	if (paddingLeft < marginLeft / 2)
		alert(icWrapperId + '태그 내의 인디케이터의 개수가 여백 영역을 초과합니다.');

	indicator
		.css('width', icPageWidth - paddingLeft)
		.css('background-color', that.options.pageBackgroundColor) // indicator-bar의 배경색
		.css({
			'padding-top' : '12px',
			'padding-right' : '0px',
			'padding-bottom' : '8px',
			'display' : 'block',
			'float' : 'left',
			'list-style' : 'none',
			'margin' : '0',
			'top' : '0',
			'left' : '0',
			'float' : 'left'
		})
		.css('padding-left', paddingLeft);
	indicatorItem
		.css({
			'display' : 'block',
			'float' : 'left',
			'list-style' : 'none',
			'padding' : '0',
			'margin' : '0',
			/* 1개의 크기는 18px, 마진 10px */
			'font-size' : 'smaller', /* text-height : 16 */
			'cursor' : 'pointer',
			'width' : '12px',
			'height' : '16px',
			'padding-left' : '5px',
			'margin-right' : '10px',
			'overflow' : 'hidden',
			'border-radius' : '7px',
			'-webkit-border-radius' : '7px',
			'-moz-border-radius' : '7px',
			'-o-border-radius' : '7px',
			'background' : '#c4c4c4'
		});
	indicator.find('> li:last-child')
		.css({
			'margin' : '0'
		});


	/* CSS의 width, height, color 속성등을 변경한 뒤 iScroll에 할당해야 #scroller 태그에 translate3d의 화면 크기 값이 정확이 계산된다.. */
	var iScroller = new iScroll(icWrapperId, {
		snap: 'li',
		momentum: false,
		hScrollbar: false,
		vScrollbar: false,
		onScrollEnd: function () {
			indicatorItem.each(function (i, node) {
				if (iScroller.currPageX == i) {
					$(node).addClass('active');
					$(node).css('color', that.options.idcActiveColor)
						.css('background', that.options.idcActiveBackgroundColor);
				}
				else {
					$(node).removeClass('active');
					$(node).css('color', that.options.idcColor)
						.css('background', that.options.idcBackgroundColor);
				}
			});
		}
	});

	// 다른 페이지에서 시작하면 IE와 iPhone에서는 정상 작동 하지 않는다.
	iScroller.scrollToPage(0);

	// indicator의 cursor 모양을 변경하고 클릭 시 페이지를 이동하게 한다.
	indicator.find('li > div').css('cursor', 'pointer').click(function () {
		var index = Number($(this).text() - 1);

		if (iScroller.currPageX != index)
			iScroller.scrollToPage(index);
	});

	return iScroller;
}