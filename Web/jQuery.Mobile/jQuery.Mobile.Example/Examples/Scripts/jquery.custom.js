﻿$(document).bind("mobileinit", function (e) {
	$.mobile.useFastClick = true;
	$.defaultPageTransition = "fade";
	$.mobile.loadingMessage = "로드중입니다...";
	$.mobile.pageLoadErrorMessage = "페이지 로드 에러";
	// anchor 태그의 Ajax 기능을 제거
	// anchor 클릭 시 URL에서 #이 붙어서 iScroll 등이 정상 작동 하지 않아서 개별로 disable 하거나 전체 ajax 처리를 disable 한다.
	// http://jquerymobile.com/test/docs/forms/forms-sample.html
	$.mobile.ajaxEnabled = true;		
});