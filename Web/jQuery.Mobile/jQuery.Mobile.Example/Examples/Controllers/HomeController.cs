﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Example.ViewModels;

namespace Example.Controllers
{
	public class HomeController : Controller
	{
		#region Model 초기화
		private IEnumerable<MoreListViewModel> _models = new MoreListViewModel[] 
		{
			// Book : 100 x 120
			new MoreListViewModel
			{
				ID = 1,
				Title = "Title1",
				Description = "설명 설명 설명 설명 설명 설명 설명 1",
				DownloadCount = 1000,
				Price = 1000,
				TitleIcon = "Content/images/package.jpg",		// 80 x 80
			},
			new MoreListViewModel
			{
				ID = 2,
				Title = "Title2",
				Description = "설명 설명 설명 설명 설명 설명 설명 2",
				DownloadCount = 2,
				Price = 2,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 3,
				Title = "Title3",
				Description = "설명 설명 설명 설명 설명 설명 설명 3",
				DownloadCount = 300,
				Price = 30.5m,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 4,
				Title = "Title4",
				Description = "설명 설명 설명 설명 설명 설명 설명 4",
				DownloadCount = 4,
				Price = 4,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 5,
				Title = "Title5",
				Description = "설명 설명 설명 설명 설명 설명 설명 5",
				DownloadCount = 5,
				Price = 5,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 6,
				Title = "Title6",
				Description = "설명 설명 설명 설명 설명 설명 설명 6",
				DownloadCount = 6,
				Price = 6,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 7,
				Title = "Title7",
				Description = "설명 설명 설명 설명 설명 설명 설명 7",
				DownloadCount = 7,
				Price = 7,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 8,
				Title = "Title8",
				Description = "설명 설명 설명 설명 설명 설명 설명 8",
				DownloadCount = 8,
				Price = 8,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 9,
				Title = "Title9",
				Description = "설명 설명 설명 설명 설명 설명 설명 9",
				DownloadCount = 900,
				Price = 9,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 10,
				Title = "Title10",
				Description = "설명 설명 설명 설명 설명 설명 설명 10",
				DownloadCount = 2000,
				Price = 2000,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 11,
				Title = "Title11",
				Description = "설명 설명 설명 설명 설명 설명 설명 11",
				DownloadCount = 11,
				Price = 11,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 12,
				Title = "Title12",
				Description = "설명 설명 설명 설명 설명 설명 설명 12",
				DownloadCount = 12,
				Price = 12,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 13,
				Title = "Title13",
				Description = "설명 설명 설명 설명 설명 설명 설명 13",
				DownloadCount = 13,
				Price = 13,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 14,
				Title = "Title14",
				Description = "설명 설명 설명 설명 설명 설명 설명 14",
				DownloadCount = 14,
				Price = 14,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 15,
				Title = "Title15",
				Description = "설명 설명 설명 설명 설명 설명 설명 15",
				DownloadCount = 15,
				Price = 15,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 16,
				Title = "Title16",
				Description = "설명 설명 설명 설명 설명 설명 설명 16",
				DownloadCount = 16,
				Price = 16,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 17,
				Title = "Title17",
				Description = "설명 설명 설명 설명 설명 설명 설명 17",
				DownloadCount = 17,
				Price = 17,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 18,
				Title = "Title18",
				Description = "설명 설명 설명 설명 설명 설명 설명 18",
				DownloadCount = 18,
				Price = 18,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 19,
				Title = "Title19",
				Description = "설명 설명 설명 설명 설명 설명 설명 19",
				DownloadCount = 19,
				Price = 19,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 20,
				Title = "Title20",
				Description = "설명 설명 설명 설명 설명 설명 설명 20",
				DownloadCount = 20,
				Price = 20,
				TitleIcon = "Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 21,
				Title = "Title21",
				Description = "설명 설명 설명 설명 설명 설명 설명 21",
				DownloadCount = 21,
				Price = 21,
				TitleIcon = "Content/images/package.jpg",
			},
		};
		#endregion

		//
		// GET: /Home/

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Overview()
		{
			return View();
		}

		public ActionResult UIComposition1()
		{
			return View();
		}

		public ActionResult ListView1()
		{
			return View();
		}

		public ActionResult SplitListView()
		{
			return View();
		}

		private const int _moreListCount = 5;

		public ActionResult MoreListView()
		{
			int totalCount = _models.Count();

			ViewBag.MoreListCount = _moreListCount;
			ViewBag.TotalPages = Math.Ceiling((double)totalCount / (double)_moreListCount);

			return View(_models.PageOf(0, _moreListCount));
		}

		[HttpPost]
		public ActionResult GetMore(int pageNo)
		{
			return Json(_models.PageOf(pageNo, _moreListCount));
		}

		public ActionResult UIComposition2()
		{
			return View();
		}

		public ActionResult FormsAPI()
		{
			return View();
		}

		public ActionResult ComplexScroll()
		{
			return View();
		}

		public ActionResult Carousel()
		{
			return View();
		}
		
		public ActionResult CarouselWithIScroll()
		{
			return View();
		}

		public ActionResult CarouselWithvScroll()
		{
			return View();
		}
	}
}
