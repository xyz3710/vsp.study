﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Linq
{
	public static class LinqExtensions
	{
		/// <summary>
		/// 컬렉션에서 지정된 pageNo의 요소를 itemsPerPage 만큼 구합니다.
		/// </summary>
		/// <typeparam name="TSource">source 요소의 형식입니다.</typeparam>
		/// <param name="source">요소를 반환할 <see cref="System.Collections.Generic.IEnumerable"/>&lt;T&gt;입니다.</param>
		/// <param name="pageNo">값을 구할 페이지 번호입니다.(Index 0 부터 시작합니다.)</param>
		/// <param name="itemsPerPage">페이지당 최대 항목의 개수 입니다.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> PageOf<TSource>(this IEnumerable<TSource> source, int pageNo, int itemsPerPage)
			where TSource : new()
		{
			return source.Skip(pageNo * itemsPerPage).Take(itemsPerPage);
		}
	}
}