﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Example.ViewModels;

namespace SkipTest
{
	public static class LinqExtensions
	{
		/// <summary>
		/// 컬렉션에서 지정된 pageNo의 요소를 itemsPerPage 만큼 구합니다.
		/// </summary>
		/// <typeparam name="TSource">source 요소의 형식입니다.</typeparam>
		/// <param name="source">요소를 반환할 <see cref="System.Collections.Generic.IEnumerable"/>&lt;T&gt;입니다.</param>
		/// <param name="pageNo">값을 구할 페이지 번호입니다.</param>
		/// <param name="itemsPerPage">페이지당 최대 항목의 개수 입니다.</param>
		/// <returns></returns>
		public static IEnumerable<TSource> PageOf<TSource>(this IEnumerable<TSource> source, int pageNo, int itemsPerPage)
			where TSource : new()
		{
			return source.Skip(pageNo * itemsPerPage).Take(itemsPerPage);
		}
	}

	class Program
	{
		#region Model 초기화
		private static IEnumerable<MoreListViewModel> _models = new MoreListViewModel[] 
		{
			new MoreListViewModel
			{
				ID = 1,
				Title = "Title1",
				Description = "설명 설명 설명 설명 설명 설명 설명 1",
				DownloadCount = 1000,
				Price = 1000,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 2,
				Title = "Title2",
				Description = "설명 설명 설명 설명 설명 설명 설명 2",
				DownloadCount = 2,
				Price = 2,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 3,
				Title = "Title3",
				Description = "설명 설명 설명 설명 설명 설명 설명 3",
				DownloadCount = 300,
				Price = 30.5m,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 4,
				Title = "Title4",
				Description = "설명 설명 설명 설명 설명 설명 설명 4",
				DownloadCount = 4,
				Price = 4,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 5,
				Title = "Title5",
				Description = "설명 설명 설명 설명 설명 설명 설명 5",
				DownloadCount = 5,
				Price = 5,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 6,
				Title = "Title6",
				Description = "설명 설명 설명 설명 설명 설명 설명 6",
				DownloadCount = 6,
				Price = 6,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 7,
				Title = "Title7",
				Description = "설명 설명 설명 설명 설명 설명 설명 7",
				DownloadCount = 7,
				Price = 7,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 8,
				Title = "Title8",
				Description = "설명 설명 설명 설명 설명 설명 설명 8",
				DownloadCount = 8,
				Price = 8,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 9,
				Title = "Title9",
				Description = "설명 설명 설명 설명 설명 설명 설명 9",
				DownloadCount = 900,
				Price = 9,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 10,
				Title = "Title10",
				Description = "설명 설명 설명 설명 설명 설명 설명 10",
				DownloadCount = 2000,
				Price = 2000,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 11,
				Title = "Title11",
				Description = "설명 설명 설명 설명 설명 설명 설명 11",
				DownloadCount = 11,
				Price = 11,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 12,
				Title = "Title12",
				Description = "설명 설명 설명 설명 설명 설명 설명 12",
				DownloadCount = 12,
				Price = 12,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 13,
				Title = "Title13",
				Description = "설명 설명 설명 설명 설명 설명 설명 13",
				DownloadCount = 13,
				Price = 13,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 14,
				Title = "Title14",
				Description = "설명 설명 설명 설명 설명 설명 설명 14",
				DownloadCount = 14,
				Price = 14,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 15,
				Title = "Title15",
				Description = "설명 설명 설명 설명 설명 설명 설명 15",
				DownloadCount = 15,
				Price = 15,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 16,
				Title = "Title16",
				Description = "설명 설명 설명 설명 설명 설명 설명 16",
				DownloadCount = 16,
				Price = 16,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 17,
				Title = "Title17",
				Description = "설명 설명 설명 설명 설명 설명 설명 17",
				DownloadCount = 17,
				Price = 17,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 18,
				Title = "Title18",
				Description = "설명 설명 설명 설명 설명 설명 설명 18",
				DownloadCount = 18,
				Price = 18,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 19,
				Title = "Title19",
				Description = "설명 설명 설명 설명 설명 설명 설명 19",
				DownloadCount = 19,
				Price = 19,
				TitleIcon = "/Content/images/package.jpg",
			},
			new MoreListViewModel
			{
				ID = 20,
				Title = "Title20",
				Description = "설명 설명 설명 설명 설명 설명 설명 20",
				DownloadCount = 20,
				Price = 20,
				TitleIcon = "/Content/images/package.jpg",
			},
		};
		#endregion

		static void Main(string[] args)
		{
			for (int i = 0; i < 4; i++)
			{
				foreach (MoreListViewModel item in _models.PageOf(i, 5))
				{
					Console.Write(item.ID + " ");
				}
			}
		}
	}
}
