﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace Example.ViewModels
{
	[DebuggerDisplay("", Name = "{ID}")]
	public class MoreListViewModel
	{
		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public string Description
		{
			get;
			set;
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public int ID
		{
			get;
			set;
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public decimal Price
		{
			get;
			set;
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public string FormattedPrice
		{
			get
			{
				return string.Format("{0:#,##0.##}", Price);
			}
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public string TitleIcon
		{
			get;
			set;
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public int DownloadCount
		{
			get;
			set;
		}

		/// <summary>
		/// PropertyName를 구하거나 설정합니다.
		/// </summary>
		public string FormattedDownloadCount
		{
			get
			{
				return string.Format("{0:#,##0}", DownloadCount);
			}
		}
	}
}