﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalFunction
{
	class Program
	{
		static void Main(string[] args)
		{
			//Test1();
			//Test2();
			Test3();
		}

		private static void Test1()
		{
			var resultSet = LocalFunctionClass.AlphabetSubset('f', 'a');

			Console.WriteLine("iterator created");

			foreach (var thing in resultSet)
			{
				Console.WriteLine($"{thing}, ");
			}
		}

		private static void Test2()
		{
			var resultSet = LocalFunctionClass.AlphabetSubset2('f', 'a');

			Console.WriteLine("iterator created");

			foreach (var thing in resultSet)
			{
				Console.WriteLine($"{thing}, ");
			}
		}

		private static void Test3()
		{
			var resultSet = LocalFunctionClass.AlphabetSubset3('f', 'a');

			Console.WriteLine("iterator created");

			foreach (var thing in resultSet)
			{
				Console.WriteLine($"{thing}, ");
			}
		}
	}
}
