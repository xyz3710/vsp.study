﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalFunction
{
	class LocalFunctionClass
	{
		public static IEnumerable<char> AlphabetSubset(char start, char end)
		{
			if (start < 'a' || start > 'z')
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(start), message: "start must be a letter");
			}

			if (end < 'a' || end > 'z')
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(end), message: "end must be a letter");
			}

			if (end <= start)
			{
				throw new ArgumentException($"{nameof(end)} must be greater than {nameof(start)}");
			}

			for (var c = start; c < end; c++)
			{
				yield return c;
			}
		}

		public static IEnumerable<char> AlphabetSubset2(char start, char end)
		{
			if (start < 'a' || start > 'z')
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(start), message: "start must be a letter");
			}

			if (end < 'a' || end > 'z')
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(end), message: "end must be a letter");
			}

			if (end <= start)
			{
				throw new ArgumentException($"{nameof(end)} must be greater than {nameof(start)}");
			}

			return AlphabetImplementation(start, end);

		}

		private static IEnumerable<char> AlphabetImplementation(char start, char end)
		{
			for (var c = start; c < end; c++)
			{
				yield return c;
			}
		}

		public static IEnumerable<char> AlphabetSubset3(char start, char end)
		{
			if (start < 'a' || start > 'z')
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(start), message: "start must be a letter");
			}

			if (end < 'a' || end > 'z')
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(end), message: "end must be a letter");
			}

			if (end <= start)
			{
				throw new ArgumentException($"{nameof(end)} must be greater than {nameof(start)}");
			}

			return alphabetImplementation();

			// 로컬 함수
			IEnumerable<char> alphabetImplementation()
			{
				for (var c = start; c < end; c++)
				{
					yield return c;
				}
			}
		}

		public Task<string> PerformLongRunningWork(string address, int index, string name)
		{
			if (string.IsNullOrWhiteSpace(address) == true)
			{
				throw new ArgumentException(message: "An address is required", paramName: nameof(address));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException(paramName: nameof(index), message: "The index must be non-negative");
			}

			if (string.IsNullOrWhiteSpace(name) == true)
			{
				throw new ArgumentException(message: "You must supply a name", paramName: nameof(name));
			}

			return Task.FromResult("");
			/*
				return longRunningWorkImplementation();

				async Task<string> longRunningWorkImplementation()_
				{
					var interimResult = await FirstWork(address);
					var secondResult = await SecondStep(index, name);

					return $"The results are {interimResult} and {secondResult}. Enjoy.";
				}
			*/
		}
		private Task FirstWork(string address)
		{
			return Task.FromResult(address);
		}

		private Task SecondStep(int index, string name)
		{
			return Task.FromResult($"{name}: {index}");
		}
	}
}
