﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AsyncAwaitTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			btnRun1.Click += btnRun1_Click;
			btnRun2.Click += btnRun2_Click;
			btnRun3.Click += btnRun3_Click;
		}

		private void btnRun1_Click(object sender, EventArgs e)
		{
			tbMessage.ResetText();
			RunIt();
		}

		private async void RunIt()
		{
			Task<double> task = Task.Factory.StartNew(() => LongCalc(10));
			//task.ConfigureAwait(false);		// 일반 task의 스레드 pool에서 실행 한다.
			await task;		// Task가 끝날때 까지 기다린다.(UI는 unblocking)

			// Task가 깥난 다음 아래 UI 컨트롤의 값 갱신
			// 이 문장은 UI 스레드에서 실행되므로 Invoke를 할 필요가 없다.
			tbMessage.Text = task.Result.ToString();
		}

		private double LongCalc(int r)
		{
			Thread.Sleep(3000);

			return Math.PI * r * r;
		}

		private async void btnRun2_Click(object sender, EventArgs e)
		{
			btnRun2.Enabled = false;
			int times = 10;

			var task1 = Task<int>.Run(() =>
			{
				int result = 0;

				for (int i = 0; i < times; i++)
				{
					result += i;
					Thread.Sleep(500);
				}

				return result;
			});
			int sum = await task1;

			tbMessage.Text = string.Format("Sum = {0}", sum);
			btnRun2.Enabled = true;
		}

		private async void btnRun3_Click(object sender, EventArgs e)
		{
			// Call the method that runs asynchronously.
			string result = await WaitAsynchronouslyAsync();

			// Call the method that runs synchronously.
			//string result = await WaitSynchronously();

			// Display the result.
			tbMessage.Text += string.Format("{0}\r\n", result);
		}

		private async Task<string> WaitAsynchronouslyAsync()
		{
			await Task.Delay(10000);

			return "Finished";
		}

		private async Task<string> WaitSynchronously()
		{
			// Add a using directive for System.Threading.
			Thread.Sleep(10000);

			return "Finished";
		}
	}
}
