﻿namespace AsyncAwaitTest
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRun1 = new System.Windows.Forms.Button();
			this.tbMessage = new System.Windows.Forms.TextBox();
			this.btnRun2 = new System.Windows.Forms.Button();
			this.btnRun3 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnRun1
			// 
			this.btnRun1.Location = new System.Drawing.Point(12, 127);
			this.btnRun1.Name = "btnRun1";
			this.btnRun1.Size = new System.Drawing.Size(299, 23);
			this.btnRun1.TabIndex = 0;
			this.btnRun1.Text = "Await Run(실행 도중 UI는 Nonblocking)";
			this.btnRun1.UseVisualStyleBackColor = true;
			// 
			// tbMessage
			// 
			this.tbMessage.Location = new System.Drawing.Point(12, 12);
			this.tbMessage.Multiline = true;
			this.tbMessage.Name = "tbMessage";
			this.tbMessage.Size = new System.Drawing.Size(299, 109);
			this.tbMessage.TabIndex = 1;
			// 
			// btnRun2
			// 
			this.btnRun2.Location = new System.Drawing.Point(12, 156);
			this.btnRun2.Name = "btnRun2";
			this.btnRun2.Size = new System.Drawing.Size(299, 23);
			this.btnRun2.TabIndex = 0;
			this.btnRun2.Text = "Await Run(실행 도중 UI는 Nonblocking)";
			this.btnRun2.UseVisualStyleBackColor = true;
			// 
			// btnTest03
			// 
			this.btnRun3.Location = new System.Drawing.Point(12, 185);
			this.btnRun3.Name = "btnTest03";
			this.btnRun3.Size = new System.Drawing.Size(299, 23);
			this.btnRun3.TabIndex = 2;
			this.btnRun3.Text = "Async/Sync test";
			this.btnRun3.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1256, 702);
			this.Controls.Add(this.btnRun3);
			this.Controls.Add(this.tbMessage);
			this.Controls.Add(this.btnRun2);
			this.Controls.Add(this.btnRun1);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "await / async Test form";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnRun1;
		private System.Windows.Forms.TextBox tbMessage;
		private System.Windows.Forms.Button btnRun2;
		private System.Windows.Forms.Button btnRun3;
	}
}