﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Test03_Result_Parameter
{
	class Program
	{
		static void Main(string[] args)
		{
			TestCallTReturnType();
			TestCallAsync();
			Console.WriteLine("End all.");
			Console.ReadLine();
		}

		async static Task<int> TaskOfTResult_MethodAsync()
		{
			var today = await Task.FromResult<string>(DateTime.Now.DayOfWeek.ToString());
			int leisureHours = 5;

			if (today.First() == 'S')
			{
				leisureHours = 16;
			}

			return leisureHours;
		}

		private async static void TestCallTReturnType()
		{
			// Calls to TaksOfTResult_MethodAsync
			Task<int> returnedTaskTResult = TaskOfTResult_MethodAsync();
			int intResult = await returnedTaskTResult;
			Console.WriteLine("intResut: {0}", intResult);
			
			// or, in a single statement
			int intResult2 = await TaskOfTResult_MethodAsync();
			Console.WriteLine("intResut2: {0}", intResult2);
		}

		async static Task Task_MethodAsync()
		{
			await Task.Delay(2000);

			Console.WriteLine("Sorry for the delay...");

			// This method has no return statement, so its return type is Task
		}

		private async static void TestCallAsync()
		{
			Task returnedTask = Task_MethodAsync();

			await returnedTask;

			// or, in a single statement
			await Task_MethodAsync();
		}
	}
}
