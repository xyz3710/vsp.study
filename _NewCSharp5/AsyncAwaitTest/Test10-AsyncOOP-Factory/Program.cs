﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// http://blog.stephencleary.com/2013/01/async-oop-2-constructors.html
namespace Test10_AsyncOOP_Factory
{
	class Program
	{
		static void Main(string[] args)
		{
			Test();
		}

		private static async void Test()
		{
			await UseMyClassAsync();
		}

		private static async Task UseMyClassAsync()
		{
			MyClass instance = await MyClass.CreateAsync();
		}
	}
}
