﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test10_AsyncOOP_Factory
{
	public sealed class MyClass
	{
		private MyData _asyncData;
		
		private MyClass()
		{			
		}

		private async Task<MyClass> InitializeAsync()
		{
			_asyncData = await GetDataAsync();

			return this;
		}

		public static Task<MyClass> CreateAsync()
		{
			var ret = new MyClass();

			return ret.InitializeAsync();
		}

		private async Task<MyData> GetDataAsync()
		{
			await Task.Delay(1).ConfigureAwait(false);

			return new MyData();
			//return Task.Run<MyData>(() => new MyData());
		}
	}
}
