﻿//#define CANCEL

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test04_Reentry
{
	public partial class Form1 : Form
	{
		private CancellationTokenSource _cts;
		private Task _pendingWork;
		private char _group;
		public Form1()
		{
			InitializeComponent();

			btnStart.Click += btnStart_Click;
			_group = (char)('A' - 1);
		}

		private async void btnStart_Click(object sender, EventArgs e)
		{
#if CANCEL
			if (_cts != null)
			{
				_cts.Cancel();
			}

			CancellationTokenSource newCTS = new CancellationTokenSource();
			_cts = newCTS;
#else
			_group = (char)(_group + 1);
			SetResult(string.Format("{0}{0}#Starting group {1}", Environment.NewLine, _group));
#endif
			//btnStart.Enabled = false;

			try
			{
#if !CANCEL
				char finishedGroup = await AccessTheWebAsync(_group);
				SetResult(string.Format("{0}{0}#Group {1} is completed.{0}", Environment.NewLine, finishedGroup));
#else
				await AccessTheWebAsync(_cts.Token);
#endif
			}
			catch (OperationCanceledException)
			{
				SetResult("\r\nDownloads canceled.\r\n");
			}
			catch (Exception)
			{
				SetResult(tbResult.Text += "\r\nDownloads failed.\r\n");
			}
			finally
			{
				//btnStart.Enabled = true;
				ScrollToEnd();
			}

#if CANCEL
			if (_cts == newCTS)
			{
				_cts = null;
			}
#endif
		}

		private async Task<char> AccessTheWebAsync(char group)
		{
			HttpClient client = new HttpClient();
			List<string> urlList = SetupUrlList();

			Task<byte[]>[] getContentTasks = urlList.Select(url => client.GetByteArrayAsync(url)).ToArray();

			_pendingWork = FinishOneGroupAsync(urlList, getContentTasks, group);

			SetResult(string.Format("{0}Task assinged for group {1}.Download tasks are active.{0}", Environment.NewLine, group));

			await _pendingWork;

			return group;
		}

		private async Task FinishOneGroupAsync(List<string> urlList, Task<byte[]>[] contentTasks, char group)
		{
			if (_pendingWork != null)
			{
				await _pendingWork;
			}

			int total = 0;

			for (int i = 0; i < contentTasks.Length; i++)
			{
				byte[] content = await contentTasks[i];
				DisplayResults(urlList[i], content, i, group);

				total += content.Length;
			}

			SetResult(string.Format("{0}{0}TOTAL bytes returned: {1,8:#,##0}", Environment.NewLine, total));
		}

		private async Task AccessTheWebAsync(CancellationToken token)
		{
			HttpClient client = new HttpClient();
			List<string> urlList = SetupUrlList();

			var total = 0;
			var position = 0;

			foreach (string url in urlList)
			{
				HttpResponseMessage response = await client.GetAsync(url, token);
				byte[] urlContents = await response.Content.ReadAsByteArrayAsync();

				token.ThrowIfCancellationRequested();

				DisplayResults(url, urlContents, ++position);

				total += urlContents.Length;
			}
		}

		private List<string> SetupUrlList()
		{
			List<string> urls = new List<string> 
			{ 
				"http://msdn.microsoft.com/en-us/library/hh191443.aspx",
				"http://msdn.microsoft.com/en-us/library/aa578028.aspx",
				"http://msdn.microsoft.com/en-us/library/jj155761.aspx",
				"http://msdn.microsoft.com/en-us/library/hh290140.aspx",
				"http://msdn.microsoft.com/en-us/library/hh524395.aspx",
				"http://msdn.microsoft.com/en-us/library/ms404677.aspx",
				"http://msdn.microsoft.com",
				"http://msdn.microsoft.com/en-us/library/ff730837.aspx"
			};

			return urls;
		}
		private void DisplayResults(string url, byte[] urlContents, int position, char group = char.MinValue)
		{
			var displayURL = url.Replace("http://", string.Empty);

			SetResult(string.Format(
								 "{0}. {1,-60} {2,8:#,##0}\r\n",
								 group == char.MinValue ?
									 position.ToString() :
									 string.Format("{0}-{1}", group, position + 1),
								 displayURL,
								 urlContents.Length));
		}

		private void SetResult(string resultMessage)
		{
			tbResult.Text += resultMessage;
			ScrollToEnd();
		}

		private void ScrollToEnd()
		{
			tbResult.SelectionStart = tbResult.Text.Length;
			tbResult.ScrollToCaret();
		}
	}
}
