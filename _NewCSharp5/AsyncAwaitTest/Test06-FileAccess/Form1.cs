﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace Test06_FileAccess
{
	public partial class Form1 : Form
	{
		#region Constants
		private const int BUFFER_SIZE = 4096;
		#endregion

		private string _filePath;
		public Form1()
		{
			InitializeComponent();
			btnStart.Click += btnStart_Click;
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			ProcessWrite();
			Task.Delay(2000);
			ProcessRead();
			Task.Delay(2000);
			ProcessWriteMulti();
		}

		private async void ProcessWrite()
		{
			_filePath = @"temp2.txt";
			string text = "Hello world!!!\r\n";

			WriteTextAsync(_filePath, text);
		}

		private async Task WriteTextAsync(string filePath, string text)
		{
			byte[] encodedText = Encoding.Unicode.GetBytes(text);

			using (FileStream source = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite, bufferSize: BUFFER_SIZE, useAsync: true))
			{
				await source.WriteAsync(encodedText, 0, encodedText.Length);
				await source.FlushAsync();
			}
		}

		private async void ProcessRead()
		{
			if (File.Exists(_filePath) == false)
			{
				Console.WriteLine("File not found: " + _filePath);
			}
			else
			{
				try
				{
					string text = await ReadTextAsync(_filePath);

					SetResult(string.Format("Read: {0}\r\n", text));
					Console.WriteLine(text);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
		}

		private async Task<string> ReadTextAsync(string filePath)
		{
			StringBuilder sb = new StringBuilder();

			using (FileStream source = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: BUFFER_SIZE, useAsync: true))
			{
				byte[] buffer = new byte[BUFFER_SIZE];
				int readCount = 0;

				while ((readCount = await source.ReadAsync(buffer, 0, buffer.Length)) != 0)
				{
					string text = Encoding.Unicode.GetString(buffer, 0, readCount);

					sb.Append(text);
				}
			}

			return sb.ToString();
		}

		private async void ProcessWriteMulti()
		{
			string folder = "TempFolder";
			List<Task> tasks = new List<Task>();
			List<FileStream> sources = new List<FileStream>();

			try
			{
				if (Directory.Exists(folder) == false)
				{
					Directory.CreateDirectory(folder);
				}

				for (int index = 1; index <= 10; index++)
				{
					string text = string.Format("In file {0}\r\n", index);
					string fileName = string.Format("theFile{0:00}.txt", index);
					string filePath = Path.Combine(folder, fileName);
					byte[] encodedText = Encoding.Unicode.GetBytes(text);
					FileStream source = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: BUFFER_SIZE, useAsync: true);

					await source.WriteAsync(encodedText, 0, encodedText.Length);
					await source.FlushAsync();

					sources.Add(source);
				}

				await Task.WhenAll(tasks);
			}
			finally
			{
				foreach (FileStream source in sources)
				{
					source.Close();
				}
			}
		}

		private void SetResult(string resultMessage)
		{
			tbResult.Text += resultMessage;
			ScrollToEnd();
		}

		private void ScrollToEnd()
		{
			tbResult.SelectionStart = tbResult.Text.Length;
			tbResult.ScrollToCaret();
		}
	}
}
