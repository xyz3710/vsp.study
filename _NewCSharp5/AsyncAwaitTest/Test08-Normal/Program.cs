﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// http://www.dotnetperls.com/async
namespace Test08_Normal
{
	class Program
	{
		static void Main(string[] args)
		{
			// Create task and start it.
			// ... Wait for it to complete.
			Task task = new Task(ProcessDataAsync);

			task.Start();
			task.Wait();

			Console.ReadLine();
		}

		private static async void ProcessDataAsync()
		{
			// Start the HAndleFile method.
			Task<int> task = HandleFileAsync(@".\enable1.txt");

			// control returns here before HandleFileAsync returns
			Console.WriteLine("Please wait patiently while I do something important.");
			// Wait for the HandleFile task to complete.
			// ... Display its results.
			int x = await task;
			Console.WriteLine("Count: " + x);
		}

		private static async Task<int> HandleFileAsync(string filePath)
		{
			Console.WriteLine("HandleFile enter");
			int count = 0;

			// Read in the specified file.
			// ... Use async StreamReader method.
			using (StreamReader reader = new StreamReader(filePath))
			{
				string v = await reader.ReadToEndAsync();
				// ... Process the file data somehow.
				count += v.Length;
				// ... A slow-running  computation
				// Dummy code.
				for (int i = 0; i < 10000; i++)
				{
					int x = v.GetHashCode();

					if (x == 0)
					{
						count--;
					}
				}
			}

			Console.WriteLine("HandleFile exit.");

			return count;
		}
	}
}
