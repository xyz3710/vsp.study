﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test11_MVVM_Command
{
	public class AsyncCommand : AsyncCommandBase
	{
		private readonly Func<Task> _command;

		public AsyncCommand(Func<Task> command)
		{
			_command = command;
		}

		public override Task ExecuteAsync(object parameter)
		{
			return _command();
		}

		public override bool CanExecute(object parameter)
		{
			return true;
		}
	}
}
