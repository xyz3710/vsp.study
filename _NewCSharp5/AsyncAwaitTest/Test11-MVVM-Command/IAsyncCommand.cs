﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test11_MVVM_Command
{
	interface IAsyncCommand : ICommand
	{
		Task ExecuteAsync(object parameter);
	}
}
