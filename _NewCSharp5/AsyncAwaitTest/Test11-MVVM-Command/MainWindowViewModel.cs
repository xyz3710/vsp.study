﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test11_MVVM_Command
{
	public sealed class MainWindowViewModel : INotifyPropertyChanged
	{
		// Raises PropertyChanged.
		public string Url
		{
			get;
			set;
		}

		public IAsyncCommand CountUrlBytesCommand
		{
			get;
			private set;
		}

		// Raises PropertyChanged
		public int ByteCount
		{
			get;
			private set;
		}

		
		#region INotifyPropertyChanged 멤버

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
