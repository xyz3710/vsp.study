﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test11_MVVM_Command
{
	interface ICommand
	{
		event EventHandler CanExecuteChanged;
		bool CanExecute(object parameter);
		void Execute(object parameter);
	}
}
