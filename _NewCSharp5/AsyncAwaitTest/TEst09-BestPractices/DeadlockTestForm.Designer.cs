﻿namespace Test09_BestPractices
{
	partial class DeadlockTestForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnDeadlocTest = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnDeadlocTest
			// 
			this.btnDeadlocTest.Location = new System.Drawing.Point(12, 12);
			this.btnDeadlocTest.Name = "btnDeadlocTest";
			this.btnDeadlocTest.Size = new System.Drawing.Size(178, 23);
			this.btnDeadlocTest.TabIndex = 0;
			this.btnDeadlocTest.Text = "Deadlock Test";
			this.btnDeadlocTest.UseVisualStyleBackColor = true;
			// 
			// DeadlockTestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.btnDeadlocTest);
			this.Name = "DeadlockTestForm";
			this.Text = "DeadlockTestForm";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnDeadlocTest;
	}
}