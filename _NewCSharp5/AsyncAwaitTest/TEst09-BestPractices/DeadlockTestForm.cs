﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test09_BestPractices
{
	public partial class DeadlockTestForm : Form
	{
		public DeadlockTestForm()
		{
			InitializeComponent();
			btnDeadlocTest.Click += btnDeadlocTest_Click;
		}

		void btnDeadlocTest_Click(object sender, EventArgs e)
		{
			DeadlockDemo.Test();
			Console.WriteLine("Deadlock passed.");

		}
	}
}
