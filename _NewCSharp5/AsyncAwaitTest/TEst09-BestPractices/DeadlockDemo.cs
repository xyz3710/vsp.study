using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test09_BestPractices
{
	public static class DeadlockDemo
	{
		private static async Task DelayAsync()
		{
			await Task.Delay(1000).ConfigureAwait(false);
		}

		public static void Test()
		{
			// start the delay.
			var delayTask = DelayAsync();

			// wait for the delay to complete.
			delayTask.Wait();
		}
	}
}

