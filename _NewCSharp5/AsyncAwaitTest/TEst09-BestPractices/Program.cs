﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test09_BestPractices
{
	class Program
	{
		static void Main(string[] args)
		{
			Application.Run(new DeadlockTestForm());
		}

		static async Task MyMethodAsync()
		{
			await Task.Delay(1000);

			// ConfigureAwait를 해주면 GUI Deadlock이 발생하지 않는다.
			await Task.Delay(1000).ConfigureAwait(continueOnCapturedContext: false);
		}
	}
}