﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Async 메서드에 디버거 사용: http://msdn.microsoft.com/ko-kr/library/jj155813.aspx
namespace Test07_Debugging
{
	class Program3
	{
		static void Main(string[] args)
		{
			ProcessAsync().Wait();
		}

		private static async Task ProcessAsync()
		{
			var theTask = DoSomethingAsync();
			int z = 0;
			var result = await theTask;
		}

		private static async Task<int> DoSomethingAsync()
		{
			Debug.WriteLine("before");  // Step Out from here
			await Task.Delay(1000);
			Debug.WriteLine("after");
			return 5;
		}
	}
}
