﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Async 메서드에 디버거 사용: http://msdn.microsoft.com/ko-kr/library/jj155813.aspx
namespace Test07_Debugging
{
	class Program1
	{
		static void Main1(string[] args)
		{
			Task theTask = ProcessAsync();
			int x = 0; // set breakpoint
			theTask.Wait();
		}

		private static async Task ProcessAsync()
		{
			var result = await DoSomethingAsync();		// set breakpoint

			int y = 0;		// set breakpoint
		}

		private static async Task<int> DoSomethingAsync()
		{
			await Task.Delay(1000);
			return 5;
		}
	}
}
