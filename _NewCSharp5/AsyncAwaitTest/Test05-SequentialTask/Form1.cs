﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Test05_SequentialTask
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			btnStart.Click += btnStart_Click;
		}

		private async void btnStart_Click(object sender, EventArgs e)
		{
			tbResult.Text = string.Empty;
			//btnStart.Enabled = false;

			//SyndicationClient client = new SyndicationClient();

			var tasks = new List<Task<KeyValuePair<int, int>>>();

			Random random = new Random(1);
			int limit = 10;

			for (int id = 1; id <= 10; id++)
			{
				limit = random.Next(50);

				tasks.Add(ProcessAsync(id, limit));
			}

			// 작업이 완료된 순서 먼저 Total을 display
			while (tasks.Count > 0)
			{
				// Task.WhenAny는 완료된 첫번째 작업을 먼저 반환
				var nextTask = await Task.WhenAny<KeyValuePair<int, int>>(tasks);
				var firstTask = await nextTask;

				DisplayTotal(firstTask);
				tasks.Remove(nextTask);
			}

			// ID 순서별로 Total을 display
			//DisplayTotal(tasks.ToArray());
		}

		private void DisplayTotal(KeyValuePair<int, int> keyValue)
		{
			SetResult(string.Format("{0}ID: {1} Total : {2} ***** {0}{0}", Environment.NewLine, keyValue.Key, keyValue.Value));
		}

		private async void DisplayTotal(Task<KeyValuePair<int, int>>[] tasks)
		{
			foreach (Task<KeyValuePair<int, int>> task in tasks)
			{
				var keyValue = await task;

				DisplayTotal(keyValue);
			}
		}

		private void SetResult(string resultMessage)
		{
			tbResult.Text += resultMessage;
			ScrollToEnd();
		}

		private void ScrollToEnd()
		{
			tbResult.SelectionStart = tbResult.Text.Length;
			tbResult.ScrollToCaret();
		}

		private async Task<KeyValuePair<int, int>> ProcessAsync(int id, int limit)
		{
			int sum = 0;

			for (int i = 1; i <= limit; i++)
			{
				sum += i;
				SetResult(string.Format("ID: {0}, i: {1}, sum: {2}\r\n", id, i, sum));
				await Task.Delay(50);
			}

			return new KeyValuePair<int, int>(id, sum);
		}
	}
}
