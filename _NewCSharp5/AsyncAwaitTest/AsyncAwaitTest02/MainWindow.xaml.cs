﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncAwaitTest02
{
	/// <summary>
	/// MainWindow.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private async void startButton_Click(object sender, RoutedEventArgs e)
		{
			resultsTextBox.Clear();

			//// Disable the button until the operation is complete.
			//startButton.IsEnabled = false;

			//// One-step async call.
			//await SumPageSizesAsync();

			////// Two-step async call.
			////Task sumTask = SumPageSizesAsync();
			////await sumTask;

			//resultsTextBox.Text += "\r\nControl returned to startButton_Click.\r\n";

			//// Reenable the button in case you want to run the operation again.
			//startButton.IsEnabled = true;


			// The display lines in the example lead you through the control shifts.
			resultsTextBox.Text += "ONE:   Entering startButton_Click.\r\n" +
				"           Calling AccessTheWebAsync.\r\n";

			Task<int> getLengthTask = AccessTheWebAsync();

			resultsTextBox.Text += "\r\nFOUR:  Back in startButton_Click.\r\n" +
				"           Task getLengthTask is started.\r\n" +
				"           About to await getLengthTask -- no caller to return to.\r\n";

			int contentLength = await getLengthTask;

			resultsTextBox.Text += "\r\nSIX:   Back in startButton_Click.\r\n" +
				"           Task getLengthTask is finished.\r\n" +
				"           Result from AccessTheWebAsync is stored in contentLength.\r\n" +
				"           About to display contentLength and exit.\r\n";

			resultsTextBox.Text +=
				String.Format("\r\nLength of the downloaded string: {0}.\r\n", contentLength);
		}

		private async Task<int> AccessTheWebAsync()
		{
			resultsTextBox.Text += "\r\nTWO:   Entering AccessTheWebAsync.";

			// Declare an HttpClient object.
			HttpClient client = new HttpClient();

			resultsTextBox.Text += "\r\n           Calling HttpClient.GetStringAsync.\r\n";

			// GetStringAsync returns a Task<string>. 
			Task<string> getStringTask = client.GetStringAsync("http://msdn.microsoft.com");

			resultsTextBox.Text += "\r\nTHREE: Back in AccessTheWebAsync.\r\n" +
				"           Task getStringTask is started.";

			// AccessTheWebAsync can continue to work until getStringTask is awaited.
			resultsTextBox.Text += "\r\n           About to await getStringTask and return a Task<int> to startButton_Click.\r\n";

			// Retrieve the website contents when task is complete.
			string urlContents = await getStringTask;

			resultsTextBox.Text += "\r\nFIVE:  Back in AccessTheWebAsync." +
				"\r\n           Task getStringTask is complete." +
				"\r\n           Processing the return statement." +
				"\r\n           Exiting from AccessTheWebAsync.\r\n";

			return urlContents.Length;
		}
		
		private async Task SumPageSizesAsync()
		{
			// Declare an HttpClient object and increase the buffer size. The
			// default buffer size is 65,536.
			HttpClient client = new HttpClient()
			{
				MaxResponseContentBufferSize = 1000000
			};

			// Make a list of web addresses.
			List<string> urlList = SetUpURLList();

			var total = 0;

			foreach (var url in urlList)
			{
				// GetByteArrayAsync returns a task. At completion, the task
				// produces a byte array.
				byte[] urlContents = await client.GetByteArrayAsync(url);

				// The following two lines can replace the previous assignment statement.
				//Task<byte[]> getContentsTask = client.GetByteArrayAsync(url);
				//byte[] urlContents = await getContentsTask;

				DisplayResults(url, urlContents);

				// Update the total.
				total += urlContents.Length;
			}

			// Display the total count for all of the websites.
			resultsTextBox.Text +=
				string.Format("\r\n\r\nTotal bytes returned:  {0}\r\n", total);
		}


		private List<string> SetUpURLList()
		{
			List<string> urls = new List<string> 
			{ 
				"http://msdn.microsoft.com/library/windows/apps/br211380.aspx",
				"http://msdn.microsoft.com",
				"http://msdn.microsoft.com/en-us/library/hh290136.aspx",
				"http://msdn.microsoft.com/en-us/library/ee256749.aspx",
				"http://msdn.microsoft.com/en-us/library/hh290138.aspx",
				"http://msdn.microsoft.com/en-us/library/hh290140.aspx",
				"http://msdn.microsoft.com/en-us/library/dd470362.aspx",
				"http://msdn.microsoft.com/en-us/library/aa578028.aspx",
				"http://msdn.microsoft.com/en-us/library/ms404677.aspx",
				"http://msdn.microsoft.com/en-us/library/ff730837.aspx"
			};
			return urls;
		}

		private void DisplayResults(string url, byte[] content)
		{
			// Display the length of each website. The string format 
			// is designed to be used with a monospaced font, such as
			// Lucida Console or Global Monospace.
			var bytes = content.Length;
			// Strip off the "http://".
			var displayURL = url.Replace("http://", "");
			resultsTextBox.Text += string.Format("\n{0,-58} {1,8}", displayURL, bytes);
		}
	}
}
