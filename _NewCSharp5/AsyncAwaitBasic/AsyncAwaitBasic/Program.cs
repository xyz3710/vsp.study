﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AsyncAwaitBasic
{
	class Program
	{
		static void Main(string[] args)
		{
			var testProgram = new Program();

			var text = testProgram.TestReadFile(@"AsyncAwaitBasic.exe.config");
			Thread.Sleep(5000);
			Console.WriteLine(text);

			testProgram.Test();

			Console.ReadLine();
		}

		public async Task<string> TestReadFile(string filePath)
		{
			return await ReadFile(filePath);
		}

		public Task<string> ReadFile(string filePath)
		{
			return Task.Factory.StartNew(() =>
			{
				return File.ReadAllText(filePath);
			});
		}

		public async void Test()
		{
			WebClient wc = new WebClient();

			var htmlText = await wc.DownloadStringTaskAsync("http://www.microsoft.com");

			htmlText += await wc.DownloadStringTaskAsync("http://oracle.com");
		}
	}
}
