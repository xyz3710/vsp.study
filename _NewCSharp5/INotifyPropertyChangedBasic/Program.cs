﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace INotifyPropertyChangedBasic
{
	class Program
	{
		static void Main(string[] args)
		{
			SomeClass someClass = new SomeClass
			{
				Value3 = 3,
				Value5 = 5,
			};

			someClass.PropertyChanged += someClass_PropertyChanged;
			someClass.Value3 = 33;
			someClass.Value5 = 55;
		}

		static void someClass_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			Console.WriteLine(e.PropertyName);
		}
	}

	public class SomeClass
	{
		private int _value5;
		private int _value3;

		#region PropertyChanged Event
		/// <summary>
		/// PropertyChanged 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 PropertyChanged EventArgs 입니다.</param>
		public delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs e);

		/// <summary>
		/// PropertyChanged를 하면 발생합니다.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// PropertyChanged 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 PropertyChanged EventArgs 입니다.</param>
		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, e);
			}
		}
		#endregion
		
		/// <summary>
		/// C# 3.0 버전
		/// </summary>
		public int Value3
		{
			get
			{
				return _value3;
			}
			set
			{
				Set(() => Value3, ref _value3, value);
			}
		}
		
		protected void Set<T>(Expression<Func<T>> expression, ref T field, T newValue)
		{
			var body = expression.Body as MemberExpression;
			string propertyName = body.Member.Name;

			field = newValue;

			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		/// <summary>
		/// C# 5.0 버전
		/// </summary>
		public int Value5
		{
			get
			{
				return _value5;
			}
			set
			{
				Set(ref _value5, value);
			}
		}

		private void Set<T>(ref T field, T newValue, [CallerMemberName]string propertyName = null)
		{
			field = newValue;

			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
	}
}
