﻿/**********************************************************************************************************************/
/*	Domain		:	Razor.Helpers.HtmlHelpers
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 5월 12일 목요일 오후 5:08
/*	Purpose		:	Razor 문법에 필요한 Helper 클래스입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RazorHelpers
{
	/// <summary>
	/// Razor 문법에 필요한 Helper 클래스입니다.
	/// </summary>
	public static class HtmlHelpers
	{
		/// <summary>
		/// 특정 길이의 문자열을 잘라서 ... 로 표시합니다.
		/// </summary>
		/// <param name="helper"></param>
		/// <param name="input"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string Truncate(this HtmlHelper helper, string input, int length)
		{
			if (input.Length <= length)
				return input;
			else
				return input.Substring(0, length) + "...";
		}
	}
}