﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicStore.Models;

namespace MusicStore.Controllers
{
	[Authorize]
	public class CheckoutController : Controller
	{
		private const string PROMO_CODE = "Free";
		MusicStoreEntities storeDB = new MusicStoreEntities();

		//
		// GET: /Checkout/AddressAndPayment

		public ActionResult AddressAndPayment()
		{
			return View();
		}

		/// <summary>
		/// POST: /Checkout/AddressAndPayment
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddressAndPayment(FormCollection values)
		{
			Order order = new Order();
			TryUpdateModel(order);

			try
			{
				if (string.Equals(values["PromoCode"], PROMO_CODE, StringComparison.OrdinalIgnoreCase) == false)
					return View(order);
				else
				{
					order.Username = User.Identity.Name;
					order.OrderDate = DateTime.Now;

					// save order
					storeDB.Orders.Add(order);
					storeDB.SaveChanges();

					// process the order
					ShoppingCart cart = ShoppingCart.GetCart(HttpContext);

					cart.CreateOrder(order);

					return RedirectToAction("Complete", new
					{
						id = order.OrderId
					});
				}
			}
			catch
			{
				// invalid - redisplay with errors
				return View(order);
			}
		}

		/// <summary>
		/// GET: /Checkout/Complete
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ActionResult Complete(int id)
		{
			// validate customer owns the order
			bool isValid = storeDB.Orders
				.Any(o => o.OrderId == id && o.Username == User.Identity.Name);

			if (isValid == true)
				return View(id);
			else
				return View("Error");
		}
	}
}
