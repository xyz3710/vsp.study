﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicStore.Models;
using MusicStore.ViewModels;

namespace MusicStore.Controllers
{
	public class ShoppingCartController : Controller
	{
		MusicStoreEntities storeDB = new MusicStoreEntities();

		//
		// GET: /ShoppingCart/

		public ActionResult Index()
		{
			var cart = ShoppingCart.GetCart(HttpContext);
			// set up ViewModel
			var viewModel = new ShoppingCartViewModel
			{
				CartItems = cart.GetCartItems(),
				CartTotal = cart.GetTotal(),
			};

			return View(viewModel);
		}

		/// <summary>
		/// GET: /ShoppingCart/AddToCart/5
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ActionResult AddToCart(int id)
		{
			// 앨범을 찾는다
			Album addedAlbum = storeDB.Albums.Single(allbum => allbum.AlbumId == id);
			ShoppingCart cart = ShoppingCart.GetCart(HttpContext);

			cart.AddToCart(addedAlbum);

			return RedirectToAction("Index");
		}

		/// <summary>
		/// AJAX: /ShoppingCart/RemoveFromCart/5
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult RemoveFromCart(int id)
		{
			ShoppingCart cart = ShoppingCart.GetCart(HttpContext);
			// 삭제 확인을 위한 앨범 이름
			string albumName = storeDB.Carts
				.Single(item => item.RecordId == id)
				.Album.Title;
			int itemCount = cart.RemoveFromCart(id);
			ShoppingCartRemoveViewModel results = new ShoppingCartRemoveViewModel
			{
				Message = Server.HtmlEncode(albumName) + " has been removed from your shopping cart.",
				CartTotal = cart.GetTotal(),
				CartCount = cart.GetCount(),
				ItemCount = itemCount,
				DeleteId = id,
			};

			return Json(results);
		}

		/// <summary>
		/// GET: /ShoppingCart/CartSummary
		/// </summary>
		/// <returns></returns>
		[ChildActionOnly]
		public ActionResult CartSummary()
		{
			ShoppingCart cart = ShoppingCart.GetCart(HttpContext);

			ViewData["CartCount"] = cart.GetCount();

			return PartialView("CartSummary");
		}
	}
}
