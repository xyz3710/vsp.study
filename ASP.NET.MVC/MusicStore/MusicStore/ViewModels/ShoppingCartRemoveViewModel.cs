﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicStore.ViewModels
{
	public class ShoppingCartRemoveViewModel
	{
		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get;
			set;
		}


		/// <summary>
		/// CartTotal를 구하거나 설정합니다.
		/// </summary>
		public decimal CartTotal
		{
			get;
			set;
		}

		/// <summary>
		/// CartCount를 구하거나 설정합니다.
		/// </summary>
		public int CartCount
		{
			get;
			set;
		}

		/// <summary>
		/// ItemCount를 구하거나 설정합니다.
		/// </summary>
		public int ItemCount
		{
			get;
			set;
		}

		/// <summary>
		/// DeleteID를 구하거나 설정합니다.
		/// </summary>
		public int DeleteId
		{
			get;
			set;
		}
	}
}