﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicStore.Models;

namespace MusicStore.ViewModels
{
	public class ShoppingCartViewModel
	{
		/// <summary>
		/// CartItems를 구하거나 설정합니다.
		/// </summary>
		public List<Cart> CartItems
		{
			get;
			set;
		}

		/// <summary>
		/// CartTotal를 구하거나 설정합니다.
		/// </summary>
		public decimal CartTotal
		{
			get;
			set;
		}
	}
}