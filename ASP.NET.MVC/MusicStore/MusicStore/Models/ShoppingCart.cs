﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicStore.Models
{
	public partial class ShoppingCart
	{
		MusicStoreEntities storeDB = new MusicStoreEntities();

		public const string CART_SESSION_KEY = "CartId";

		/// <summary>
		/// ShoppingCartId를 구하거나 설정합니다.
		/// </summary>
		public string ShoppingCartId
		{
			get;
			set;
		}

		public static ShoppingCart GetCart(HttpContextBase context)
		{
			ShoppingCart cart = new ShoppingCart();

			cart.ShoppingCartId = cart.GetCartId(context);

			return cart;
		}

		public static ShoppingCart GetCart(Controller controller)
		{
			return GetCart(controller.HttpContext);
		}

		public void AddToCart(Album album)
		{
			Cart cartItem = storeDB.Carts
				.SingleOrDefault(c => c.CartId == ShoppingCartId && c.AlbumId == album.AlbumId);

			if (cartItem == null)
			{
				cartItem = new Cart
				{
					AlbumId = album.AlbumId,
					CartId = ShoppingCartId,
					Count = 1,
					DateCreated = DateTime.Now,
				};

				storeDB.Carts.Add(cartItem);
			}
			else
				cartItem.Count++;

			storeDB.SaveChanges();
		}

		public int RemoveFromCart(int id)
		{
			Cart cartItem = storeDB.Carts
				.SingleOrDefault(c => c.CartId == ShoppingCartId && c.RecordId == id);
			int itemCount = 0;

			if (cartItem != null)
			{
				if (cartItem.Count > 1)
				{
					cartItem.Count--;
					itemCount = cartItem.Count;
				}
				else
					storeDB.Carts.Remove(cartItem);

				storeDB.SaveChanges();
			}

			return itemCount;
		}

		public void EmptyCart()
		{
			var cartItems = storeDB.Carts.Where(c => c.CartId == ShoppingCartId);

			foreach (Cart cart in cartItems)
				storeDB.Carts.Remove(cart);

			// Save changes
			storeDB.SaveChanges();
		}

		public List<Cart> GetCartItems()
		{
			return storeDB.Carts
				.Where(c => c.CartId == ShoppingCartId).ToList();
		}

		public int GetCount()
		{
			int? count = storeDB.Carts.Where(c => c.CartId == ShoppingCartId)
				.Select(c => (int?)c.Count).Sum();

			return count ?? 0;
		}

		public decimal GetTotal()
		{
			decimal? total = storeDB.Carts
				.Where(c => c.CartId == ShoppingCartId)
				.Select(c => (int?)c.Count * c.Album.Price)
				.Sum();

			return total ?? decimal.Zero;
		}

		public int CreateOrder(Order order)
		{
			decimal orderTotal = 0m;
			List<Cart> cartItems = GetCartItems();

			cartItems.ForEach(item =>
			{
				OrderDetail orderDetail = new OrderDetail
				{
					AlbumId = item.AlbumId,
					OrderId = order.OrderId,
					UnitPrice = item.Album.Price,
					Quantity = item.Count,
				};

				orderTotal += (orderDetail.UnitPrice * orderDetail.Quantity);
				storeDB.OrderDetails.Add(orderDetail);
			});

			order.Total = orderTotal;
			storeDB.SaveChanges();
			EmptyCart();

			return order.OrderId;
		}

		/// <summary>
		/// HttpContextBase로 쿠키에 access
		/// </summary>
		/// <returns></returns>
		public string GetCartId(HttpContextBase context)
		{
			if (context.Session[CART_SESSION_KEY] == null)
			{
				if (string.IsNullOrWhiteSpace(context.User.Identity.Name))
					context.Session[CART_SESSION_KEY] = context.User.Identity.Name;
				else
				{
					Guid tempCartId = Guid.NewGuid();
					// 쿠키로 tempCartId를 클라이언트로 보낸다.
					context.Session[CART_SESSION_KEY] = tempCartId.ToString();
				}
			}

			return context.Session[CART_SESSION_KEY].ToString();
		}

		public void MigrateCart(string userName)
		{
			var shoppingCart = storeDB.Carts.Where(c => c.CartId == ShoppingCartId);

			foreach (Cart cart in shoppingCart)
				cart.CartId = userName;

			storeDB.SaveChanges();
		}
	}
}