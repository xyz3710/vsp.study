﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicStore.Models
{
	public class OrderDetail
	{
		/// <summary>
		/// OrderDetailId를 구하거나 설정합니다.
		/// </summary>
		public int OrderDetailId
		{
			get;
			set;
		}

		/// <summary>
		/// OrderId를 구하거나 설정합니다.
		/// </summary>
		public int OrderId
		{
			get;
			set;
		}

		/// <summary>
		/// AlbumId를 구하거나 설정합니다.
		/// </summary>
		public int AlbumId
		{
			get;
			set;
		}

		/// <summary>
		/// Quantity를 구하거나 설정합니다.
		/// </summary>
		public int Quantity
		{
			get;
			set;
		}

		/// <summary>
		/// UnitPrice를 구하거나 설정합니다.
		/// </summary>
		public decimal UnitPrice
		{
			get;
			set;
		}

		/// <summary>
		/// Album를 구하거나 설정합니다.
		/// </summary>
		public virtual Album Album
		{
			get;
			set;
		}

		/// <summary>
		/// Order를 구하거나 설정합니다.
		/// </summary>
		public virtual Order Order
		{
			get;
			set;
		}
	}
}
