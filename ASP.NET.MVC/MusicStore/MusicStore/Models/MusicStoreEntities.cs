﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicStore.Models
{
	public class MusicStoreEntities : DbContext
	{
		/// <summary>
		/// Albums를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Album> Albums
		{
			get;
			set;
		}

		/// <summary>
		/// Genres를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Genre> Genres
		{
			get;
			set;
		}

		public DbSet<Artist> Artists
		{
			get;
			set;
		}

		/// <summary>
		/// Carts를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Cart> Carts
		{
			get;
			set;
		}

		/// <summary>
		/// Orders를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Order> Orders
		{
			get;
			set;
		}

		/// <summary>
		/// OrderDetails를 구하거나 설정합니다.
		/// </summary>
		public DbSet<OrderDetail> OrderDetails
		{
			get;
			set;
		}
	}
}