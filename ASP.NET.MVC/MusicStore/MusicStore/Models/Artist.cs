﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicStore.Models
{
	public class Artist
	{
		/// <summary>
		/// ArtistId를 구하거나 설정합니다.
		/// </summary>
		public int ArtistId
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}
	}
}