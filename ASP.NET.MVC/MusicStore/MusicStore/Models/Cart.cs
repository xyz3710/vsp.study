﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MusicStore.Models
{
	public class Cart
	{
		/// <summary>
		/// RecordId를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int RecordId
		{
			get;
			set;
		}

		/// <summary>
		/// CartId를 구하거나 설정합니다.
		/// </summary>
		public string CartId
		{
			get;
			set;
		}

		/// <summary>
		/// AlbumId를 구하거나 설정합니다.
		/// </summary>
		public int AlbumId
		{
			get;
			set;
		}

		/// <summary>
		/// Count를 구하거나 설정합니다.
		/// </summary>
		public int Count
		{
			get;
			set;
		}

		/// <summary>
		/// DateCreated를 구하거나 설정합니다.
		/// </summary>
		public DateTime DateCreated
		{
			get;
			set;
		}

		/// <summary>
		/// Album를 구하거나 설정합니다.
		/// </summary>
		public virtual Album Album
		{
			get;
			set;
		}
	}
}