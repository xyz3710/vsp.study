﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MusicStore.Models
{
	[Bind(Exclude="AlbumId")]
	public class Album
	{
		/// <summary>
		/// AlbumId를 구하거나 설정합니다.
		/// </summary>
		[ScaffoldColumn(false)]
		public int AlbumId
		{
			get;
			set;
		}

		/// <summary>
		/// GenreId를 구하거나 설정합니다.
		/// </summary>
		[DisplayName("장르")]
		public int GenreId
		{
			get;
			set;
		}

		/// <summary>
		/// ArtistId를 구하거나 설정합니다.
		/// </summary>
		[DisplayName("아티스트")]
		public int ArtistId
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		[StringLength(160)]
		[Required(ErrorMessage="제목이 필요합니다.")]
		[DisplayName("제목")]
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Price를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="가격이 필요합니다.")]
		[Range(0.01, 100.0, ErrorMessage="가격은 0.01 ~ 100.00 사이의 값을 입력해야만 합니다.")]
		[DisplayName("가격")]
		public decimal Price
		{
			get;
			set;
		}

		/// <summary>
		/// AlbumArtUrl를 구하거나 설정합니다.
		/// </summary>
		[DisplayName("앨범 Art 주소"), StringLength(1024)]
		public string AlbumArtUrl
		{
			get;
			set;
		}

		/// <summary>
		/// Genre를 구하거나 설정합니다.
		/// </summary>
		[DisplayName("장르")]
		public virtual Genre Genre
		{
			get;
			set;
		}

		/// <summary>
		/// Artist를 구하거나 설정합니다.
		/// </summary>
		public virtual Artist Artist
		{
			get;
			set;
		}

		/// <summary>
		/// OrderDetails를 구하거나 설정합니다.
		/// </summary>
		public virtual List<OrderDetail> OrderDetails
		{
			get;
			set;
		}
	}
}