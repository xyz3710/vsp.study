﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace MusicStore.Models
{

	public class ChangePasswordModel
	{
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "현재 비밀번호")]
		public string OldPassword
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "새로운 비밀번호")]
		public string NewPassword
		{
			get;
			set;
		}

		[DataType(DataType.Password)]
		[Display(Name = "새로운 비밀 번호 확인")]
		[Compare("NewPassword", ErrorMessage = "새로운 비밀 번호와 확인 암호가 일치하지 않습니다.")]
		public string ConfirmPassword
		{
			get;
			set;
		}
	}

	public class LogOnModel
	{
		[Required]
		[Display(Name = "사용자 이름")]
		public string UserName
		{
			get;
			set;
		}

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "암호")]
		public string Password
		{
			get;
			set;
		}

		[Display(Name = "Remember me?")]
		public bool RememberMe
		{
			get;
			set;
		}
	}

	public class RegisterModel
	{
		[Required]
		[Display(Name = "사용자 이름")]
		public string UserName
		{
			get;
			set;
		}

		[Required]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "암호")]
		public string Password
		{
			get;
			set;
		}

		[DataType(DataType.Password)]
		[Display(Name = "암호 확인")]
		[Compare("Password", ErrorMessage = "비밀 번호와 확인 암호가 일치하지 않습니다.")]
		public string ConfirmPassword
		{
			get;
			set;
		}
	}
}
