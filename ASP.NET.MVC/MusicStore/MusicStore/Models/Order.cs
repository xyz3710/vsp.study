﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MusicStore.Models
{
	public class Order
	{
		/// <summary>
		/// OrderId를 구하거나 설정합니다.
		/// </summary>
		[ScaffoldColumn(false)]
		public int OrderId
		{
			get;
			set;
		}

		/// <summary>
		/// Username를 구하거나 설정합니다.
		/// </summary>
		[ScaffoldColumn(false)]
		public string Username
		{
			get;
			set;
		}

		/// <summary>
		/// OrderDate를 구하거나 설정합니다.
		/// </summary>
		[ScaffoldColumn(false)]
		public DateTime OrderDate
		{
			get;
			set;
		}

		/// <summary>
		/// LastName를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="성이 필요합니다.")]
		[DisplayName("성")]
		[StringLength(160)]
		public string LastName
		{
			get;
			set;
		}

		/// <summary>
		/// FirstName를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="이름이 필요합니다.")]
		[DisplayName("이름")]
		[StringLength(160)]
		public string FirstName
		{
			get;
			set;
		}

		/// <summary>
		/// Address를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="주소가 필요합니다.")]
		[StringLength(70)]
		[DisplayName("주소")]
		public string Address
		{
			get;
			set;
		}

		/// <summary>
		/// City를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="도시 이름이 필요합니다.")]
		[StringLength(40)]
		[DisplayName("도시")]
		public string City
		{
			get;
			set;
		}

		/// <summary>
		/// State를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="주 이름이 필요합니다.")]
		[StringLength(40)]
		[DisplayName("주/도")]
		public string State
		{
			get;
			set;
		}

		/// <summary>
		/// PostalCode를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="우편번호가 필요합니다.")]
		[StringLength(10)]
		[DisplayName("우편번호")]
		public string PostalCode
		{
			get;
			set;
		}

		/// <summary>
		/// Country를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="나라 이름이 필요합니다.")]
		[StringLength(40)]
		[DisplayName("나라")]
		public string Country
		{
			get;
			set;
		}

		/// <summary>
		/// Phone를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="전화번호가 필요합니다.")]
		[DisplayName("휴대폰")]
		[StringLength(24)]
		public string Phone
		{
			get;
			set;
		}

		/// <summary>
		/// Email를 구하거나 설정합니다.
		/// </summary>
		[Required(ErrorMessage="이메일이 필요합니다.")]
		[DisplayName("Email Address")]
		[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage="이메일이 유효하지 않습니다.")]
		[DataType(DataType.EmailAddress)]
		public string Email
		{
			get;
			set;
		}

		/// <summary>
		/// Total를 구하거나 설정합니다.
		/// </summary>
		[ScaffoldColumn(false)]
		public decimal Total
		{
			get;
			set;
		}

		/// <summary>
		/// OrderDetail를 구하거나 설정합니다.
		/// </summary>
		public List<OrderDetail> OrderDetails
		{
			get;
			set;
		}
	}
}