﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicStore.Models
{
	public class Genre
	{
		/// <summary>
		/// GenreId를 구하거나 설정합니다.
		/// </summary>
		public int GenreId
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Description를 구하거나 설정합니다.
		/// </summary>
		public string Description
		{
			get;
			set;
		}

		/// <summary>
		/// Albums를 구하거나 설정합니다.
		/// </summary>
		public List<Album> Albums
		{
			get;
			set;
		}
	}
}