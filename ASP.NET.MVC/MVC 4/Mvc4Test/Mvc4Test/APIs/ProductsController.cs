﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mvc4Test.Models;

namespace Mvc4Test.APIs
{
	public class ProductsController : ApiController
	{
		StoreDbContext db = new StoreDbContext();

		// GET /api/products
		/* Fiddler: Composer, 
		 *	GET, 
		 *	Request Headers section: 
		 *		Accept: application/json  or
		 *		Accept: application/xml
		 *	
		 * http://localhost:64082/api/products?$top=2&$skip=2&$orderby=id 
		 * IQueryable로 하면 OData 처리시 이런 형태로 호출이 되어야 하는데 안된다 ...
		 * // http://forums.asp.net/t/1826213.aspx/1?Support+for+OData+URL+Parameters+in+Web+API+
		 * // http://blogs.msdn.com/b/alexj/archive/2012/08/15/odata-support-in-asp-net-web-api.aspx
		 * => QueryableAttribute를 붙여서 해결할 수 있다.
		 */
		[Queryable()]
		public IQueryable<Product> Get()
		{
			return db.Products;
		}

		// Get /api/products/2
		public Product Get(int id)
		{
			return db.Products.Single(x => x.Id == id);
		}

		// POST /api/products
		/* Fiddler: Composer, 
		 *	POST, 
		 *	Request Headers section: 
		 *		Content-Type: application/json
		 *	Request Body section:
		 *		{
		 *			"Name":"Rocks!",
		 *			"Price":35.5
		 *		}
		 */
		/*
		public Product Post(Product product)
		{
			db.Products.Add(product);
			db.SaveChanges();

			return product;
		}
		*/

		public HttpResponseMessage Post(Product product)
		{
			db.Products.Add(product);
			db.SaveChanges();

			var result = Request.CreateResponse<Product>(HttpStatusCode.Created, product);
			result.Headers.Location = new Uri(Request.RequestUri, "/api/products/" + product.Id.ToString());

			return result;
		}
	}
}