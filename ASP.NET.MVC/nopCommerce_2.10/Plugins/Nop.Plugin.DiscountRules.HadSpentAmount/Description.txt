﻿Group: Discount requirements
FriendlyName: Customer had spent x.xx amount
SystemName: DiscountRequirement.HadSpentAmount
Version: 1.00
SupportedVersions: 2.00, 2.10
Author: nopCommerce team
DisplayOrder: 1
FileName: Nop.Plugin.DiscountRules.HadSpentAmount.dll