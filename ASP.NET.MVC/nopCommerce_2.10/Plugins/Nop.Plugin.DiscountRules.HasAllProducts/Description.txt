﻿Group: Discount requirements
FriendlyName: Customer has all of these product variants in the cart
SystemName: DiscountRequirement.HasAllProducts
Version: 1.00
SupportedVersions: 2.00, 2.10
Author: nopCommerce team
DisplayOrder: 1
FileName: Nop.Plugin.DiscountRules.HasAllProducts.dll