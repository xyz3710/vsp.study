﻿Group: Discount requirements
FriendlyName: Customer had previously purchased one of these product variants
SystemName: DiscountRequirement.PurchasedOneProduct
Version: 1.00
SupportedVersions: 2.00, 2.10
Author: nopCommerce team
DisplayOrder: 1
FileName: Nop.Plugin.DiscountRules.PurchasedOneProduct.dll