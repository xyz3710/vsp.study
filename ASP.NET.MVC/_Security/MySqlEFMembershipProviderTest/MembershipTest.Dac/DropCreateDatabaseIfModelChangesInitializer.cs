﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace MembershipTest.Dac
{
	public class DropCreateDatabaseIfModelChangesInitializer : DropCreateMySqlDatabaseIfModelChanges<MembershipTestDbContext>
	{
		protected override void Seed(MembershipTestDbContext db)
		{
			db.Users.Add(new User
			{
				Name = "Test1",
			});
			db.Users.Add(new User
			{
				Name = "Test2",
			});
			db.Users.Add(new User
			{
				Name = "Test3",
			});

			db.Roles.Add(new Role
			{
				Name = "RoleTest1",
			});
			db.Roles.Add(new Role
			{
				Name = "RoleTest2",
			});
			db.Roles.Add(new Role
			{
				Name = "RoleTest3",
			});
		}
	}
}
