/**********************************************************************************************************************/
/*	Domain		:	MySql.Data.MySqlClient.DropCreateMySqlDatabaseAlways`1
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Wednesday, April 10, 2013 10:23 AM
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.Entity;
using System.Configuration;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;

namespace MySql.Data.MySqlClient
{
	/// <summary>
	/// 컨텍스트가 응용 프로그램 도메인에서 처음 사용될 때 항상 데이터베이스를 다시 만들고 
	/// 선택적으로 데이터베이스를 데이터로 다시 시드할 <see cref="T:MySql.Data.MySqlClient.MySqlDatabaseInitializer`1" />의 구현입니다. 
	/// 데이터베이스를 시드하려면 파생 클래스를 만들고 Seed 메서드를 재정의합니다. 
	/// </summary>
	/// <typeparam name="TContext">컨텍스트의 형식입니다.</typeparam>
	public class DropCreateMySqlDatabaseAlways<TContext> : MySqlDatabaseInitializer<TContext>
		where TContext : DbContext
	{
		/// <summary>
		/// 전략을 실행하여 지정된 컨텍스트에 대한 데이터베이스를 초기화합니다.
		/// </summary>
		/// <param name="context">컨텍스트입니다.</param>
		public override void InitializeDatabase(TContext context)
		{
			context.Database.Delete();
			CreateMySqlDatabase(context);
		}
	}
}
