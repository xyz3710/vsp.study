using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.Entity;
using System.Configuration;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;

namespace MySql.Data.MySqlClient
{
	/// <summary>
	/// 데이터베이스가 만들어진 후 모델이 변경된 경우에만 데이터베이스를 삭제하고, 다시 만들고, 
	/// 선택적으로 데이터베이스를 데이터로 다시 시드할 <see cref="T:MySql.Data.MySqlClient.MySqlDatabaseInitializer`1" />의 구현입니다. 
	/// 이 작업은 저장소 모델이 만들어질 때 저장소 모델의 해시를 데이터베이스에 작성한 다음 현재 모델에서 생성된 해시와 비교하여 수행됩니다. 
	/// 데이터베이스를 시드하려면 파생 클래스를 만들고 Seed 메서드를 재정의합니다. 
	/// </summary>
	/// <typeparam name="TContext">컨텍스트의 형식입니다.</typeparam>
	public class DropCreateMySqlDatabaseIfModelChanges<TContext> : MySqlDatabaseInitializer<TContext>
		where TContext : DbContext
	{
		/// <summary>
		/// 전략을 실행하여 지정된 컨텍스트에 대한 데이터베이스를 초기화합니다.
		/// </summary>
		/// <param name="context">컨텍스트입니다.</param>
		public override void InitializeDatabase(TContext context)
		{
			bool needsNewDb = false;

			if (context.Database.Exists() == true)
			{
				if (context.Database.CompatibleWithModel(false) == false)
				{
					context.Database.Delete();
					needsNewDb = true;
				}
			}
			else
				needsNewDb = true;

			if (needsNewDb == true)
				CreateMySqlDatabase(context);
		}
	}
}
