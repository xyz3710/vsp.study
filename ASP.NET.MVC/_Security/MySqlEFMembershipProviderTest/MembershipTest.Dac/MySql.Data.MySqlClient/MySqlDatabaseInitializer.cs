/**********************************************************************************************************************/
/*	Domain		:	MySql.Data.MySqlClient.MySqlDatabaseInitializer`1
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Thursday, April 04, 2013 11:23 AM
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://www.nsilverbullet.net/2012/11/07/6-steps-to-get-entity-framework-5-working-with-mysql-5-5/
 *					#429 일감 첫번째 덧글에 의해 아래 사이트에서 찾아서 fix.
 *					http://brice-lambson.blogspot.kr/2012/05/using-entity-framework-code-first-with.html
 *					https://gist.github.com/3061139
 *					http://blog.oneunicorn.com/2012/02/27/code-first-migrations-making-__migrationhistory-not-a-system-table/
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MySql.Data.MySqlClient
{
	/// <summary>
	/// 컨텍스트가 응용 프로그램 도메인에서 처음 사용될 때 항상 데이터베이스를 다시 만들고 
	/// 선택적으로 데이터베이스를 데이터로 다시 시드할 <see cref="T:System.Data.Entity.IDatabaseInitializer`1" />의 구현입니다. 
	/// 데이터베이스를 시드하려면 파생 클래스를 만들고 Seed 메서드를 재정의합니다. 
	/// </summary>
	/// <typeparam name="TContext">컨텍스트의 형식입니다.</typeparam>
	public abstract class MySqlDatabaseInitializer<TContext> : IDatabaseInitializer<TContext>
		where TContext : DbContext
	{
		/// <summary>
		/// 전략을 실행하여 지정된 컨텍스트에 대한 데이터베이스를 초기화합니다.
		/// </summary>
		/// <param name="context">컨텍스트입니다.</param>
		public abstract void InitializeDatabase(TContext context);

		/*
		/// <summary>
		/// 전략을 실행하여 지정된 컨텍스트에 대한 데이터베이스를 초기화합니다.
		/// </summary>
		/// <param name="context">컨텍스트입니다.</param>
		public void InitializeDatabase(TContext context)
		{
			if (context.Database.Exists() == false)
				CreateMySqlDatabase(context);
			//else
			//{
			//	if (!context.Database.CompatibleWithModel(false))
			//	{
			//		throw new InvalidOperationException("The model has changed!");
			//	}
			//}
		}
		*/

		/// <summary>
		/// 데이터베이스를 추가 합니다.
		/// </summary>
		/// <param name="context">컨텍스트입니다.</param>
		protected void CreateMySqlDatabase(TContext context)
		{
			try
			{
				// Create as much of the database as we can             
				context.Database.Create();
				// No exception? Don't need a workaround
				Seed(context);
				context.SaveChanges();

				return;
			}
			catch (MySqlException ex)
			{
				/*
				if (ex.Number == 1044)
				{
					// Access denied for user 'User'@'%' to database 'database'
					// 오류로 database가 생성이 안되는 경우
					using (var connection = ((MySqlConnection)context.Database.Connection).Clone())
					{
						using (var command = connection.CreateCommand())
						{
							string userId = string.Empty;
							string password = string.Empty;

							foreach (string item in connection.ConnectionString.SplitDelimiter(";"))
							{
								var parse = item.SplitDelimiter("=").ToArray();

								if (parse.Count() == 2)
								{
									if (String.Compare(parse[0], "User Id", true) == 0)
										userId = parse[1];

									if (String.Compare(parse[0], "password", true) == 0)
										password = parse[1];
								}
								if (userId != string.Empty && password != string.Empty)
									break;
							}
							
							//command.CommandText = string.Format("grant all privileges on {0}.* to '{1}'@'%' identified by '{2}' with grant option;",
							//	connection.Database, userId, password);
							command.CommandText = string.Format("create database {0} character set utf8;", connection.Database);

							try
							{
								connection.Open();
								command.ExecuteNonQuery();
							}
							catch (Exception exDb)
							{
								throw exDb;
							}
							finally
							{
								connection.Close();
							}
						}
					}
				}
				*/

				if (ex.Number != 1064)		// Ignore the parse exception
					throw;
			}

			// Manually create the metadata table         
			using (var connection = ((MySqlConnection)context.Database.Connection).Clone())
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText =
		@"
CREATE TABLE __MigrationHistory (
	MigrationId mediumtext,
	CreatedOn datetime,
	Model mediumblob,
	ProductVersion mediumtext);

ALTER TABLE __MigrationHistory
ADD PRIMARY KEY (MigrationId(255));

INSERT INTO __MigrationHistory (
	MigrationId,
	CreatedOn,
	Model,
	ProductVersion)
VALUES (
	'InitialCreate',
	@CreatedOn,
	@Model,
	@ProductVersion);
";
					command.Parameters.AddWithValue("@Model", GetModel(context));
					command.Parameters.AddWithValue("@ProductVersion", GetProductVersion());
					command.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
					connection.Open();
					command.ExecuteNonQuery();
					connection.Close();
				}
			}

			Seed(context);
			context.SaveChanges();
		}

		private byte[] GetModel(TContext context)
		{
			using (var memoryStream = new MemoryStream())
			{
				using (var gzipStream = new System.IO.Compression.GZipStream(memoryStream, System.IO.Compression.CompressionMode.Compress))
				using (var xmlWriter = System.Xml.XmlWriter.Create(gzipStream, new System.Xml.XmlWriterSettings
				{
					Indent = true
				}))
				{
					EdmxWriter.WriteEdmx(context, xmlWriter);
				}
				return memoryStream.ToArray();
			}
		}

		private string GetProductVersion()
		{
			return typeof(DbContext).Assembly
				.GetCustomAttributes(false)
				.OfType<System.Reflection.AssemblyInformationalVersionAttribute>()
				.Single()
				.InformationalVersion;
		}

		/// <summary>
		/// 재정의되면 시드를 위해 컨텍스트에 데이터를 추가합니다.  
		/// 기본 구현은 아무 작업도 수행하지 않습니다. 
		/// </summary>
		/// <param name="context">시드할 컨텍스트입니다.</param>
		protected virtual void Seed(TContext context)
		{
		}
	}
}

