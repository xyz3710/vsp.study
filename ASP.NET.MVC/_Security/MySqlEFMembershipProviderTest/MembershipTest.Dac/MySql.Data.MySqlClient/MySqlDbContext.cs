﻿/**********************************************************************************************************************/
/*	Domain		:	MySql.Data.MySqlClient.MySqlDbContext
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Wednesday, April 10, 2013 10:28 AM
/*	Purpose		:	MySql db에 접근하는 DbContext를 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;

namespace MySql.Data.MySqlClient
{
	/// <summary>
	/// MySql db에 접근하는 DbContext를 제공합니다.
	/// </summary>
	public class MySqlDbContext : DbContext
	{
		/// <summary>
		/// MySqlDbContext 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="nameOrConnectionString">The name or connection string.</param>
		protected MySqlDbContext(string nameOrConnectionString)
			: base(nameOrConnectionString)
		{
			ConnectionString = nameOrConnectionString;
			Database.DefaultConnectionFactory = new MySqlConnectionFactory(ConnectionString);
		}

		/// <summary>
		/// 새로운 MySqlDbContext 인스턴스를 생성 합니다.
		/// </summary>
		/// <param name="database">The database</param>
		/// <param name="userId">The user id</param>
		/// <param name="password">The password</param>
		/// <param name="server">The server</param>
		/// <param name="port">The port</param>
		protected MySqlDbContext(
			string database,
			string userId,
			string password,
			string server = "localhost",
			uint port = 3306)
			: this(GetMySqlConnectionString(database, userId, password, server, port))
		{
		}

		/// <summary>
		/// MySql 연결 문자열을 구합니다.
		/// </summary>
		/// <param name="database">The database</param>
		/// <param name="userId">The user id</param>
		/// <param name="password">The password</param>
		/// <param name="server">The server</param>
		/// <param name="port">The port</param>
		/// <returns>연결 문자열</returns>
		public static string GetMySqlConnectionString(
			string database,
			string userId,
			string password,
			string server = "localhost",
			uint port = 3306)
		{
			var connectionStringBuilder = new MySqlConnectionStringBuilder
			{
				Server = server,
				Port = port,
				Database = database,
				UserID = userId,
				Password = password,
				PersistSecurityInfo = true,
			};

			return connectionStringBuilder.ConnectionString;
		}

		/// <summary>
		/// 생성된 연결 문자열을 구합니다.
		/// </summary>
		public string ConnectionString
		{
			get;
			protected set;
		}
	}
}
