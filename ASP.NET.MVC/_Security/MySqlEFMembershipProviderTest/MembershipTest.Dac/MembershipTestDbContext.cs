﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data.Entity;

namespace MembershipTest.Dac
{
	public class MembershipTestDbContext : MySqlDbContext
	{
		public MembershipTestDbContext()
			: base("MembershipTestDbContext")
		{
			//Database.SetInitializer<MembershipTestDbContext>(new DropCreateDatabaseIfModelChangesInitializer());	// Model이 변경되었으면 기존 모델을 Drop 시키고 새로 생성한다.
		}

		public static MembershipTestDbContext CreateContext()
		{
			return new MembershipTestDbContext();
		}

		public DbSet<User> Users
		{
			get;
			set;
		}

		public DbSet<Role> Roles
		{
			get;
			set;
		}
	}
}
