﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MembershipTest.Dac
{
	class Program
	{
		private static void Main()
		{
			var db = MembershipTestDbContext.CreateContext();
			var count = db.Users.Count();

			Console.WriteLine("user count: {0}", count);
		}
	}
}
