﻿http://stackoverflow.com/questions/2038993/asp-net-membership-role-providers-for-mysql/13247593#13247593

* MySQL Connector Net 6.5.x 이상 dll 추가
* 속성에서 로컬 복사 true : 그래야 Membership sector 오류가 발생하지 않는다.
	MySql.Data.dll
	MySql.Data.Entity.dll
	MySql.Web.dll

* system.web 에 아래 구문 추가

<membership defaultProvider="MySqlMembershipProvider">
	<providers>
		<clear />
		<add name="MySqlMembershipProvider"
				type="MySql.Web.Security.MySQLMembershipProvider, MySql.Web"
				autogenerateschema="true"
				connectionStringName="LMContext"
				enablePasswordRetrieval="false"
				enablePasswordReset="true"
				requiresQuestionAndAnswer="false"
				requiresUniqueEmail="false"
				passwordFormat="Hashed"
				maxInvalidPasswordAttempts="5"
				minRequiredPasswordLength="6"
				minRequiredNonalphanumericCharacters="0"
				passwordAttemptWindow="10"
				passwordStrengthRegularExpression=""
				applicationName="/" />
	</providers>
</membership>

<roleManager enabled="true" defaultProvider="MySqlRoleProvider">
	<providers>
		<clear />
		<add connectionStringName="LMContext"
			applicationName="/"
			name="MySqlRoleProvider"
			type="MySql.Web.Security.MySQLRoleProvider, MySql.Web, Version=6.6.5.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d"
			autogenerateschema="true"/>
	</providers>
</roleManager>

<profile>
	<providers>
		<clear/>
		<add type="MySql.Web.Security.MySqlProfileProvider, MySql.Web, Version=6.6.5.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d"
		name="MySqlProfileProvider"
		applicationName="/"
		connectionStringName="LMContext"
		autogenerateschema="true"/>
	</providers>
</profile>



mysql --user=root -p --host=x10.mine.nu --port=3306

create database MembershipTest character set utf8;

-- database를 생성하지 못하므로 all privilege 권한을 줘야 한다.
grant all privileges on MembershipTest.* to 'dev'@'%' identified by 'theProgrammer100' with grant option;

-- grant all privileges 
flush privileges;

grant all privileges on *.* to 'root'@'%' with grant option;

