﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace SimpleMembershipTest
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new
				{
					id = RouteParameter.Optional
				}
			);

			// Convert model property name style into camelcase
			// http://www.imaso.co.kr/?doc=bbs/gnuboard.php&bo_table=article&wr_id=42101
			var json = config.Formatters.JsonFormatter;
			json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

			config.EnableQuerySupport();
		}
	}
}