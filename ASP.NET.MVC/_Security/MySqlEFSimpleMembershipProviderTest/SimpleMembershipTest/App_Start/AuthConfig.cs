﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using SimpleMembershipTest.Models;

namespace SimpleMembershipTest
{
	public static class AuthConfig
	{
		public static void RegisterAuth()
		{
			// To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
			// you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

			// https://manage.dev.live.com/Applications/ApiSettings/00000000400ED3E9?sr=0
			// localhost 테스트 안됨
			OAuthWebSecurity.RegisterMicrosoftClient(
				clientId: "00000000400ED3E9",
				clientSecret: "prOckIlsluD1Jp6ogivnxkDfyo3eHJGq",
				displayName: "Microsoft는 localhost 테스트 안됨");

			// https://dev.twitter.com/apps/4198114/show
			// localhost 테스트 됨
			OAuthWebSecurity.RegisterTwitterClient(
				consumerKey: "Gwo399U3zCvDwfbtTUTbeg",
				consumerSecret: "QwTZlAViQh7l8NfpZ0ckpftlZlMKGYy7YcW4Q3eWZ8");

			// https://developers.facebook.com/apps/297951627001345/summary?save=1
			// localhost 테스트 됨
			OAuthWebSecurity.RegisterFacebookClient(
				appId: "297951627001345",
				appSecret: "ebf82c594c9b7257781b76726d5ada36");

			// https://www.linkedin.com/secure/developer
			OAuthWebSecurity.RegisterLinkedInClient(
				consumerKey: "trxj6j982cvk",
				consumerSecret: "dKB7bQQ4O6x2kYnc"
			);

			OAuthWebSecurity.RegisterGoogleClient();
			OAuthWebSecurity.RegisterYahooClient();
		}
	}
}
