﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Web.Security;
using System.Security.Cryptography;

namespace SimpleMembership01.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Message = "ASP.NET MVC 시작";

			return View();
		}

		public ActionResult About()
		{
			string newPassword = "a1234";
			string hashPassword = Crypto.HashPassword(newPassword);
			string token = GenerateToken();

			//MailSender.Send(to: "xyz37@naver.com",
			//subject: "이메일 테스트",
			//body: "이메일"
			//);
			return View();
		}

		public string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
		{
			return string.Empty;
		}

		internal static string GenerateToken()
		{
			byte[] data = new byte[0x10];

			using (RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
			{
				provider.GetBytes(data);

				return Convert.ToBase64String(data);
			}
		}
	}
}
