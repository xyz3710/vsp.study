﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleMembership01
{
	public static class TypeExtensions
	{
		/// <summary>
		/// 시스템에서 약속한 delimiter(:)를 이용하여 split 시킵니다.
		/// </summary>
		/// <typeparam name="T">string, int 타입만 가능합니다.</typeparam>
		/// <param name="value">구별할 값</param>
		/// <returns></returns>
		public static IEnumerable<T> SplitDelimiter<T>(this string value, string delimiter = ";")
		{
			if (string.IsNullOrEmpty(value) == true)
				return new T[0];

			string[] splitValues = value.Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

			if (typeof(T) == typeof(string))
				return splitValues.Cast<T>();
			else if (typeof(T) == typeof(int))
			{
				int oneValue = 0;
				List<int> result = new List<int>();

				foreach (string stringValue in splitValues)
				{
					if (int.TryParse(stringValue, out oneValue) == true)
						result.Add(oneValue);
				}

				return result.Cast<T>();
			}

			return new T[0];
		}

	}
}