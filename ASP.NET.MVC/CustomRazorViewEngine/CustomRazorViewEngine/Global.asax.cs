﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CustomRazorViewEngine
{
	// 참고: IIS6 또는 IIS7 클래식 모드를 사용하도록 설정하는 지침을 보려면 
	// http://go.microsoft.com/?LinkId=9394801을 방문하십시오.

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Default", // 경로 이름
				"{controller}/{action}/{id}", // 매개 변수가 있는 URL
				new
				{
					controller = "Home",
					action = "Index",
					id = UrlParameter.Optional
				} // 매개 변수 기본값
			);

		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);

			// Custom ViewEngine을 등록합니다.
			RegisterCustomViewEnginges();
		}

		private void RegisterCustomViewEnginges()
		{
			ViewEngines.Engines.Clear();
			// NOTICE: ASP.NET을 사용하지 않을 경우 아래 주석을 제거 한다.
			// by KIMKIWON\xyz37(김기원) in 2011년 8월 24일 수요일 오전 1:51
			ViewEngines.Engines.Add(new WebFormViewEngine());			
			ViewEngines.Engines.Add(new CustomRazorView());
		}
	}
}