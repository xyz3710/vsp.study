﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomRazorViewEngine.Customed.Framework
{
	public enum LoginRouteTypes
	{
		/// <summary>
		/// Hybrid용
		/// </summary>
		NativeApp,
		/// <summary>
		/// MobileWeb
		/// </summary>
		MobileWeb,
		/// <summary>
		/// Web
		/// </summary>
		Web,
	}
}
