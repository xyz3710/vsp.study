﻿/**********************************************************************************************************************/
/*	Domain		:	ustomRazorViewEngineCustomView
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2011년 8월 14일 일요일 오후 2:12
/*	Purpose		:	RazorViewEngine의 customizing 한 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2011년 8월 16일 화요일 오후 3:23
/*	Changes		:	51degrees foundation (http://51degrees.codeplex.com/)을 적용하여 Mobile 접근을 검사하도록 수정
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2011년 9월 20일 화요일 오후 4:43
/*	Changes		:	#205 관련하여 ViewLocationFormatHelper.ReplaceViewLocation method 수정
 *					CreatePartialView, CreateView override method 추가
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	RazorViewEngine의 ViewLocationFormats, PartialViewLocationFormats, MasterLocationFormats 경로를 변경한다.
 *					Global.asax.cs에서 WebFormViewEngine도 등록할 경우 WebForm 에서도 동일한 작업을 해줘야 한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 16일 화요일 오후 2:19
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using CustomRazorViewEngine.Customed.Framework;

namespace CustomRazorViewEngine
{
	/// <summary>
	/// RazorViewEngine의 customizing 한 기능을 제공합니다.
	/// </summary>
	public class CustomRazorView : RazorViewEngine
	{
		#region Constructors
		/// <summary>
		/// CustomRazorViewEngine class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public CustomRazorView()
		{
			FileExtensions = new[]
			{
				"cshtml"
			};
		}

		#endregion

		/// <summary>
		/// RedirectView의 접미사를 구합니다.
		/// </summary>
		public string RedirectViewPostfix
		{
			get;
			private set;
		}

		private bool CheckIsMobileDevice(ControllerContext controllerContext)
		{
			HttpContextBase httpContext = controllerContext.HttpContext;
			bool isMobileDevice = (bool)(httpContext.Session[GlobalConstants.IS_MOBILE_DEVICE_SESSION_KEY] ?? false);

			System.Diagnostics.Debug.WriteLine(string.Format("SessionKey: {0}", httpContext.Session.SessionID));
			System.Diagnostics.Debug.WriteLine(string.Format("IsMobileDevice in ViewEngine: {0}", httpContext.Request.Browser[GlobalConstants.FIFTYONE_IS_WIRELESS_DEVICE_KEY]));

			if (httpContext.Session.IsNewSession == true)
			{
				HttpRequestBase request = httpContext.Request;

				isMobileDevice = Convert.ToBoolean(request.Browser[GlobalConstants.FIFTYONE_IS_WIRELESS_DEVICE_KEY]);
				//IsMobile =  request.Browser[GlobalConstants.FIFTYONE_IS_WIRELESS_DEVICE_KEY];
				//IsTablet =  request.Browser[GlobalConstants.FIFTYONE_IS_TABLET_KEY];
				//BrandName = request.Browser[GlobalConstants.FIFTYONE_BRAND_NAME_KEY];
				//ModelName = request.Browser[GlobalConstants.FIFTYONE_MODEL_NAME_KEY];
				//Useragent = request.UserAgent;
				httpContext.Session.Add(GlobalConstants.IS_MOBILE_DEVICE_SESSION_KEY, isMobileDevice);
			}

			return isMobileDevice;
		}

		private void SetViewLocationPostfix(ControllerContext controllerContext)
		{
			// NOTICE: 이부분은 별도로 구현해야 합니다.
			// by KIMKIWON\xyz37(김기원) in 2011년 10월 4일 화요일 오전 11:46
			LoginRouteTypes currentRouteType = LoginRouteTypes.Web;
			string viewLocationPostfix = string.Empty;
			const string HYBRID = ".Hybrid";
			const string MOBILE = ".Mobile";

			// NOTICE: 현재 모바일의 접근은 51degrees foundation (http://51degrees.codeplex.com/)을 사용했는데 별도의 루틴을 구현하면 됩니다.
			// by KIMKIWON\xyz37(김기원) in 2011년 10월 4일 화요일 오전 11:55
			if (CheckIsMobileDevice(controllerContext) == true)
				currentRouteType = LoginRouteTypes.MobileWeb;

			// NOTICE: Hybrid는 NativeApp + MobileWeb 형태이고, Mobile은 순수한 Mobile Browser를 통해서 접근했을 경우이다.
			// 이후 Hybrid와 Mobile을 하나로 통합 후 정리한다.
			// by KIMKIWON\xyz37(김기원) in 2011년 8월 24일 수요일 오전 10:25
			switch (currentRouteType)
			{
				case LoginRouteTypes.NativeApp:
					viewLocationPostfix = HYBRID;

					break;
				case LoginRouteTypes.MobileWeb:
					viewLocationPostfix = MOBILE;

					break;
				default:
					viewLocationPostfix = string.Empty;

					break;
			}

			RedirectViewPostfix = viewLocationPostfix;
		}

		protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
		{
			SetViewLocationPostfix(controllerContext);

			return base.CreatePartialView(controllerContext, partialPath.ReplaceViewLocation(RedirectViewPostfix));
		}

		protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
		{
			SetViewLocationPostfix(controllerContext);

			return base.CreateView(controllerContext, viewPath.ReplaceViewLocation(RedirectViewPostfix), masterPath);
		}

		public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
		{
			PartialViewLocationFormats = PartialViewLocationFormats.ReplaceViewLocation(RedirectViewPostfix);
			AreaPartialViewLocationFormats = AreaPartialViewLocationFormats.ReplaceViewLocation(RedirectViewPostfix);

			return base.FindPartialView(controllerContext, partialViewName, useCache);
		}

		public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
		{
			ViewLocationFormats = ViewLocationFormats.ReplaceViewLocation(RedirectViewPostfix);
			MasterLocationFormats = MasterLocationFormats.ReplaceViewLocation(RedirectViewPostfix);
			AreaViewLocationFormats = AreaViewLocationFormats.ReplaceViewLocation(RedirectViewPostfix);
			AreaMasterLocationFormats = AreaMasterLocationFormats.ReplaceViewLocation(RedirectViewPostfix);

			return base.FindView(controllerContext, viewName, masterName, useCache);
		}
	}

	internal static class ViewLocationFormatHelper
	{
		private static Regex _postfixRegex = new Regex(@"(/Views)(\.?\w*)/", RegexOptions.Compiled | RegexOptions.Singleline);

		/// <summary>
		/// ViewEngine의 기본 View 파일의 경로를 특정 접미사로 대체합니다.
		/// </summary>
		/// <param name="originalViewLocation">원본 경로</param>
		/// <param name="oldLocationPostfix">추가할 접미사(.Mobile, .Hybrid 처럼 . 으로 시작하고 구성해야 한다.)</param>
		/// <returns></returns>
		public static string ReplaceViewLocation(this string originalViewLocation, string oldLocationPostfix = "")
		{
			string viewLocationFormats = _postfixRegex.Replace(originalViewLocation, string.Format("$1{0}/", oldLocationPostfix));

			return viewLocationFormats;
		}

		/// <summary>
		/// ViewEngine의 기본 View 파일의 경로를 특정 접미사로 대체합니다.
		/// </summary>
		/// <param name="originalViewLocations">원본 경로</param>
		/// <param name="oldLocationPostfix">추가할 접미사(.Mobile, .Hybrid 처럼 . 으로 시작하고 구성해야 한다.)</param>
		/// <returns></returns>
		public static string[] ReplaceViewLocation(this string[] originalViewLocations, string oldLocationPostfix = "")
		{
			string[] viewLocationFormats = new string[originalViewLocations.Length];

			for (int i = 0; i < originalViewLocations.Length; i++)
				viewLocationFormats[i] = originalViewLocations[i].ReplaceViewLocation(oldLocationPostfix);

			return viewLocationFormats;
		}
	}
}