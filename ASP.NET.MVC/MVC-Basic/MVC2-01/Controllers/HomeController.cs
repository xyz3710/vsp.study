﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC2_01.Controllers
{
	[HandleError]
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewData["Message"] = "ASP.NET MVC 시작";

			return View();
		}

		public ActionResult About()
		{
			return View();
		}
	}
}
