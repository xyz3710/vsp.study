﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Luxury.SmartAudio.Website.Controllers;

namespace MVC3_ErrorHandling
{
	// 참고: IIS6 또는 IIS7 클래식 모드를 사용하도록 설정하는 지침을 보려면 
	// http://go.microsoft.com/?LinkId=9394801을 방문하십시오.

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			//filters.Add(new HandleErrorAttribute2()); //must be before HandleErrorAttribute
			filters.Add(new HandleErrorAttribute());
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Default", // 경로 이름
				"{controller}/{action}/{id}", // 매개 변수가 있는 URL
				new
				{
					controller = "Home",
					action = "Index",
					id = UrlParameter.Optional
				} // 매개 변수 기본값
			);

		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
		}

		protected void Application_Error(object sender, EventArgs e)
		{
			Exception exception = Server.GetLastError();

			Response.Clear();

			HttpException httpException = exception as HttpException;
			RouteData routeData = new RouteData();
			routeData.Values.Add("controller", "Error");

			if (httpException != null)
			{
				switch (httpException.GetHttpCode())
				{
					case 404: // Page not found.
						routeData.Values.Add("action", "HttpError404");

						break;
					case 500:	// Server error.
						routeData.Values.Add("action", "HttpError500");

						break;
					default:
						routeData.Values.Add("action", "Index");

						break;
				}
			}
			else
				routeData.Values.Add("action", "Index");

			Server.ClearError();

			IController errorController = new ErrorController();
			string controller = Convert.ToString(Request.RequestContext.RouteData.Values["controller"]);
			string action = Convert.ToString(Request.RequestContext.RouteData.Values["action"]);
			HandleErrorInfo handleErrorInfo = new HandleErrorInfo(exception, controller, action);

			routeData.Values.Add("handleErrorInfo", handleErrorInfo);

			errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
		}

		protected void Application_Error2(object sender, EventArgs e)
		{
			var error = Server.GetLastError();
			var code = (error is HttpException) ? (error as HttpException).GetHttpCode() : 500;

			if (code != 404)
			{
				// Generate email with error details and send to administrator
			}

			Response.Clear();
			Server.ClearError();

			string path = Request.Path;
			Context.RewritePath(string.Format("~/Error/Http{0}", code), false);
			IHttpHandler httpHandler = new MvcHttpHandler();
			httpHandler.ProcessRequest(Context);
			Context.RewritePath(path, false);
		}

	}

	public class HandleErrorAttribute2 : IExceptionFilter
	{
		public void OnException(ExceptionContext context)
		{
			Exception ex = context.Exception;
			if (!(ex is HttpException)) //ignore "file not found"
			{
				//Log error here
			}
		}
	}

}