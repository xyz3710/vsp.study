﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;

namespace MVC3_ErrorHandling.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Message = "ASP.NET MVC 시작";

			return View();
		}

		public ActionResult About()
		{
			return View();
		}

		public ActionResult ExceptionThrow()
		{
			throw new NotImplementedException();
		}
	}
}
