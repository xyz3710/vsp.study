﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Text;

namespace Luxury.SmartAudio.Website.Controllers
{
	[HandleError]
	[OutputCache(Location = OutputCacheLocation.None)]
	public class ErrorController : Controller
	{
		public ActionResult Index()
		{
			HandleErrorInfo handleErrorInfo = RouteData.Values["handleErrorInfo"] as HandleErrorInfo;
			StringBuilder message = new StringBuilder();

			message.AppendLine("죄송합니다.");
			message.AppendLine("");
			message.AppendLine("요청을 처리하는 동안 오류가 발생했습니다.");
			message.AppendLine("빠른 시일 내에 해결하도록 하겠습니다.");
			message.AppendLine("");
			message.AppendLine("오류 내용은 담당자에게 메일로 발송되었습니다.");

			ViewBag.Message = message.ToString();

			return View(handleErrorInfo);
		}

		public ActionResult HttpError500()
		{
			HandleErrorInfo handleErrorInfo = RouteData.Values["handleErrorInfo"] as HandleErrorInfo;
			StringBuilder message = new StringBuilder();

			message.AppendLine("죄송합니다.");
			message.AppendLine("");
			message.AppendLine("서버에서 알 수 없는 오류가 발생하였습니다.");
			message.AppendLine("빠른 시일 내에 해결하도록 하겠습니다.");
			message.AppendLine("");
			message.AppendLine("오류 내용은 담당자에게 메일로 발송되었습니다.2");

			ViewBag.Message = message.ToString();

			return View("Index", handleErrorInfo);
		}

		public ActionResult HttpError404()
		{
			HandleErrorInfo handleErrorInfo = RouteData.Values["handleErrorInfo"] as HandleErrorInfo;

			return View(handleErrorInfo);
		}
	}
}
