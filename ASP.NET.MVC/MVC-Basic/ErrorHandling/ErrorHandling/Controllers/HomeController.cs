﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ErrorHandlingWithMvc.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData["Message"] = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }


        public ActionResult GenerateDivideByZeroException()
        {
            int x = 0;
            int y = 0;
            int errorHere = x/y;

            return View("Index");
        }


        public ActionResult ServerError500()
        {
            // Go to view to see the exception.

            return View();
        }


    }
}
