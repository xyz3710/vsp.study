﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml;
using MVC3_CSS_01.Models;

namespace MVC3_CSS_01.Controllers
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/

		public ActionResult Index()
		{
			XElement xElement = XElement.Load("http://xyz37.blog.me/rss");

			var query = xElement.Descendants("item")
				.Select(item => new BlogItem
				{
					Category = item.Element("category").Value,
					Title = item.Element("title").Value,
					Description = item.Element("description").Value,
					Link= item.Element("link").Value,
				});

			ViewBag.Rss = query.ToList();

			return View();
		}

	}
}
