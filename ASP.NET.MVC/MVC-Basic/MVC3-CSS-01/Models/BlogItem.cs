﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC3_CSS_01.Models
{
	public class BlogItem
	{
		/// <summary>
		/// Category를 구하거나 설정합니다.
		/// </summary>
		public string Category
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Description를 구하거나 설정합니다.
		/// </summary>
		public string Description
		{
			get;
			set;
		}

		/// <summary>
		/// Link를 구하거나 설정합니다.
		/// </summary>
		public string Link
		{
			get;
			set;
		}
	}
}