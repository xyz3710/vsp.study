﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVC3_02.Models
{
	public class MusicTrack
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		public int ID
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Url를 구하거나 설정합니다.
		/// </summary>
		public string Url
		{
			get;
			set;
		}

		/// <summary>
		/// EpisodeId를 구하거나 설정합니다.
		/// </summary>
		public int EpisodeId
		{
			get;
			set;
		}

		/// <summary>
		/// Episode를 구하거나 설정합니다.
		/// </summary>
		public Episode Episode
		{
			get;
			set;
		}
	}
}
