﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVC3_02.Models
{
	public class Episode
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		public int ID
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		[Required]
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// PublishedAt를 구하거나 설정합니다.
		/// </summary>
		public DateTime? PublishedAt
		{
			get;
			set;
		}

		/// <summary>
		/// Summary를 구하거나 설정합니다.
		/// </summary>
		public string Summary
		{
			get;
			set;
		}

		/// <summary>
		/// LeadImage를 구하거나 설정합니다.
		/// </summary>
		public string LeadImage
		{
			get;
			set;
		}

		/// <summary>
		/// ShortUrl를 구하거나 설정합니다.
		/// </summary>
		public string ShortUrl
		{
			get;
			set;
		}

		/// <summary>
		/// MusicTracks를 구하거나 설정합니다.
		/// </summary>
		public ICollection<MusicTrack> MusicTracks
		{
			get;
			set;
		}
	}
}