﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3_02.Models;

namespace MVC3_02.Controllers
{ 
    public class EpisodeController : Controller
    {
        private PodCastContext db = new PodCastContext();

        //
        // GET: /Episode/

        public ViewResult Index()
        {
            return View(db.Episode.ToList());
        }

        //
        // GET: /Episode/Details/5

        public ViewResult Details(int id)
        {
            Episode episode = db.Episode.Find(id);
            return View(episode);
        }

        //
        // GET: /Episode/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Episode/Create

        [HttpPost]
        public ActionResult Create(Episode episode)
        {
            if (ModelState.IsValid)
            {
                db.Episode.Add(episode);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(episode);
        }
        
        //
        // GET: /Episode/Edit/5
 
        public ActionResult Edit(int id)
        {
            Episode episode = db.Episode.Find(id);
            return View(episode);
        }

        //
        // POST: /Episode/Edit/5

        [HttpPost]
        public ActionResult Edit(Episode episode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(episode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(episode);
        }

        //
        // GET: /Episode/Delete/5
 
        public ActionResult Delete(int id)
        {
            Episode episode = db.Episode.Find(id);
            return View(episode);
        }

        //
        // POST: /Episode/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Episode episode = db.Episode.Find(id);
            db.Episode.Remove(episode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}