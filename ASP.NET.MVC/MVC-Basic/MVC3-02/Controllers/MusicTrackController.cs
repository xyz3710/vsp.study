﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3_02.Models;

namespace MVC3_02.Controllers
{ 
    public class MusicTrackController : Controller
    {
        private PodCastContext db = new PodCastContext();

        //
        // GET: /MusicTrack/

        public ViewResult Index()
        {
            var musictrack = db.MusicTrack.Include(m => m.Episode);
            return View(musictrack.ToList());
        }

        //
        // GET: /MusicTrack/Details/5

        public ViewResult Details(int id)
        {
            MusicTrack musictrack = db.MusicTrack.Find(id);
            return View(musictrack);
        }

        //
        // GET: /MusicTrack/Create

        public ActionResult Create()
        {
            ViewBag.EpisodeId = new SelectList(db.Episode, "ID", "Title");
            return View();
        } 

        //
        // POST: /MusicTrack/Create

        [HttpPost]
        public ActionResult Create(MusicTrack musictrack)
        {
            if (ModelState.IsValid)
            {
                db.MusicTrack.Add(musictrack);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.EpisodeId = new SelectList(db.Episode, "ID", "Title", musictrack.EpisodeId);
            return View(musictrack);
        }
        
        //
        // GET: /MusicTrack/Edit/5
 
        public ActionResult Edit(int id)
        {
            MusicTrack musictrack = db.MusicTrack.Find(id);
            ViewBag.EpisodeId = new SelectList(db.Episode, "ID", "Title", musictrack.EpisodeId);
            return View(musictrack);
        }

        //
        // POST: /MusicTrack/Edit/5

        [HttpPost]
        public ActionResult Edit(MusicTrack musictrack)
        {
            if (ModelState.IsValid)
            {
                db.Entry(musictrack).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EpisodeId = new SelectList(db.Episode, "ID", "Title", musictrack.EpisodeId);
            return View(musictrack);
        }

        //
        // GET: /MusicTrack/Delete/5
 
        public ActionResult Delete(int id)
        {
            MusicTrack musictrack = db.MusicTrack.Find(id);
            return View(musictrack);
        }

        //
        // POST: /MusicTrack/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            MusicTrack musictrack = db.MusicTrack.Find(id);
            db.MusicTrack.Remove(musictrack);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}