﻿using System.Web.Mvc;

namespace Mvc3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
