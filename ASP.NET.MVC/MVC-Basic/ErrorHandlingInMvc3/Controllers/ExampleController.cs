﻿using System;
using System.Web.Mvc;

namespace Mvc3.Controllers
{
    public class ExampleController : Controller
    {
        public ActionResult Exception()
        {
            throw new ArgumentNullException();
        }

        public ActionResult Db()
        {
            // Inherits from DbException
            throw new MyDbException();
        }
    }
}
