﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using MVC3_01.Models;

namespace MVC3_01.Controllers
{
	public class HomeController : Controller
	{
		// public으로 선언된 method는 모두 action으로 간주
		// NonAction으로 하면 public이어도 action이 안됨
		// action은 View를 준비하기 이전 단계

		private List<Person> _persons = new List<Person>()
        					{
        						new Person("김기원", 38, "군산"),
        						new Person("황태영", 37, "전주"),
        						new Person("김영재", 4, "전주"),
        						new Person("김수민", 3, "전주"),
        					};

		//
		// GET: /Home/

		public ActionResult Index()
		{
			ViewData["FromViewData"] = "Hello, ASP.NET MVC 3";
			ViewBag.ViewBag = "Hello, ASP.NET MVC 3 ViewBag";

			ViewBag.Persons = _persons;
			
			return View(_persons);
		}

		public string Index2()
		{
			return "Hello, ASP.NET MVC3 .. !";
		}

		[NonAction]
		public string Test1()
		{
			return "<h1>Test1 Page</h1>";
		}

		[ActionName("Test3")]
		public string Test2()
		{
			return "<h1>Test2 Page</h1>";
		}

		public ActionResult TypedViewIndex()
		{
			return View("TypedViewIndex", _persons);
		}

		public string Exam1(int id)
		{
			return "This is sample page with ID : " + id;
		}

		public string Exam2(int id, string alpha, string beta)
		{
			StringBuilder value = new StringBuilder();

			value.Append("Type URL /Home/Exam2/123?alpha=KiWon&beta=Kim<br/>");
			value.AppendFormat("This is sample page with ID : {0}\r\nAlpha : {1},\tBeta : {2}", id, alpha, beta);

			return value.ToString();
		}

		[HttpGet]
		public ActionResult PostTestGet()
		{
			return View();
		}

		/*
		[HttpPost]
		public string PostTest(Person person)
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendLine("<h1>");
			sb.AppendLine(person.Name);
			sb.AppendLine(person.Age.ToString());
			sb.AppendLine(person.Region);
			sb.AppendLine("</h1>");

			return sb.ToString();
		}
		*/

		[HttpPost]
		public ActionResult PostTestGet(Person person)
		{
			return View("PostTestPost", person);
		}

		public JsonResult GetJsonPerson()
		{
			return Json(_persons, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetJsonPersonIndex()
		{
			return View();
		}

		public JsonResult GetJsonPerson2()
		{
			return Json(_persons);
		}


		public ActionResult GetJsonPersonIndex2()
		{
			return View();
		}
	}
}