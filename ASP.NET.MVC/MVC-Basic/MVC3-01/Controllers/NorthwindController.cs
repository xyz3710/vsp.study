﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3_01.Models;

namespace MVC3_01.Controllers
{
	public class NorthwindController : Controller
	{
		//
		// GET: /Northwind/

		public ActionResult Index()
		{
			NorthwindDataContext dbContext = new NorthwindDataContext();
			List<Products> products = dbContext.Products.ToList();

			List<int?> categoryIds = dbContext.Products.GroupBy(p => p.CategoryID, p => new
			{
				Key = p.CategoryID,
			}).Select(c => c.Key).ToList();
			List<int?> supplierIds = dbContext.Products.GroupBy(p => p.SupplierID, p => new
			{
				Key = p.CategoryID,
			}).Select(c => c.Key).ToList();

			Dictionary<string, List<int?>> dic = new Dictionary<string, List<int?>>();

			dic.Add("Category", categoryIds);
			dic.Add("Supplier", supplierIds);

			ViewBag.Dic = dic;
									
			return View(products);
		}

		//
		// GET: /Northwind/Details/5

		public ActionResult Details(int id)
		{
			NorthwindDataContext dbContext = new NorthwindDataContext();
			Products product = dbContext.Products.Single(p => p.ProductID == id);

			return View(product);
		}

		//
		// GET: /Northwind/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Northwind/Create

		[HttpPost]
		public ActionResult Create(Products collection)
		{
			try
			{
				// TODO: Add insert logic here
				NorthwindDataContext dbContext = new NorthwindDataContext();

				dbContext.Products.InsertOnSubmit(collection);
				dbContext.SubmitChanges();

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		//
		// GET: /Northwind/Edit/5

		public ActionResult Edit(int id)
		{
			NorthwindDataContext dbContext = new NorthwindDataContext();
			Products newProduct = dbContext.Products.Single(p => p.ProductID == id);

			return View(newProduct);
		}

		//
		// POST: /Northwind/Edit/5

		[HttpPost]
		public ActionResult Edit(int id, Products product)
		{
			try
			{
				// TODO: Add update logic here
				NorthwindDataContext dbContext = new NorthwindDataContext();
				Products newProduct = dbContext.Products.Single(p => p.ProductID == id);

				newProduct.ProductID = product.ProductID;
				newProduct.ProductName = product.ProductName;
				newProduct.QuantityPerUnit = product.QuantityPerUnit;
				newProduct.ReorderLevel= product.ReorderLevel;
				newProduct.SupplierID = product.SupplierID;
				newProduct.UnitPrice = product.UnitPrice;
				newProduct.UnitsInStock = product.UnitsInStock;
				newProduct.UnitsOnOrder = product.UnitsOnOrder;
				newProduct.CategoryID = product.CategoryID;
				newProduct.Discontinued = product.Discontinued;

				dbContext.SubmitChanges();

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		//
		// GET: /Northwind/Delete/5

		public ActionResult Delete(int id)
		{
			NorthwindDataContext dbContext = new NorthwindDataContext();
			Products newProduct = dbContext.Products.Single(p => p.ProductID == id);

			dbContext.Products.DeleteOnSubmit(newProduct);
			dbContext.SubmitChanges();

			return RedirectToAction("Index");
		}

		//
		// POST: /Northwind/Delete/5

		[HttpPost]
		public ActionResult Delete(int id, Products collection)
		{
			try
			{
				// TODO: Add delete logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		public PartialViewResult MenuBar()
		{
			NorthwindDataContext dbContext = new NorthwindDataContext();
			List<int?> categoryIds = dbContext.Products.GroupBy(p => p.CategoryID, p => new
			{
				Key = p.CategoryID,
			}).Select(c => c.Key).ToList();
			List<int?> supplierIds = dbContext.Products.GroupBy(p => p.SupplierID, p => new
			{
				Key = p.CategoryID,
			}).Select(c => c.Key).ToList();

			Dictionary<string, List<int?>> dic = new Dictionary<string, List<int?>>();

			dic.Add("Category", categoryIds);
			dic.Add("Supplier", supplierIds);

			ViewBag.Dic = dic;

			return PartialView();
		}
	}
}
