using System;

namespace MVC3_01.Models
{
	public class Person
	{
		#region Constructors
		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person(string name, int age, string region)
		{
			Name = name;
			Age = age;
			Region = region;
		}

		#region Constructors
		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person()
		{

		}
		#endregion
		#endregion

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}

		/// <summary>
		/// Region를 구하거나 설정합니다.
		/// </summary>
		public string Region
		{
			get;
			set;
		}
	}
}
