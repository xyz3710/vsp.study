﻿using System.Collections.Generic;
using System.Web.Helpers;
using System.Web.Mvc;
using MvcApplication1.Models;
using System.Linq;

namespace MvcApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly List<FavouriteGivenName> _mostPopular;
        public HomeController()
        {
            _mostPopular = new List<FavouriteGivenName>()
            {
                new FavouriteGivenName() {Id = 1, Name = "Jack", Age = 30},
                new FavouriteGivenName() {Id = 2, Name = "Riley", Age = 40},
                new FavouriteGivenName() {Id = 3, Name = "William", Age = 17},
                new FavouriteGivenName() {Id = 4, Name = "Oliver", Age = 56},
                new FavouriteGivenName() {Id = 5, Name = "Lachlan", Age = 25},
                new FavouriteGivenName() {Id = 6, Name = "Thomas", Age = 75},
                new FavouriteGivenName() {Id = 7, Name = "Joshua", Age = 93},
                new FavouriteGivenName() {Id = 8, Name = "James", Age = 15},
                new FavouriteGivenName() {Id = 9, Name = "Liam", Age = 73},
                new FavouriteGivenName() {Id = 10, Name = "Max", Age = 63}
            };
        }

        public ActionResult Index(int? page)
        {
            return View(_mostPopular.ToList());
        }

        public ActionResult EfficientWay()
        {
            return View();
        }

        [HttpGet]
        public JsonResult EfficientPaging(int? page)
        {
            int skip = page.HasValue ? page.Value - 1 : 0;
            var data = _mostPopular.OrderBy(o => o.Id).Skip(skip * 5).Take(5).ToList();
            var grid = new WebGrid(data);
            var htmlString = grid.GetHtml(tableStyle: "webGrid",
                                          headerStyle: "header",
                                          alternatingRowStyle: "alt",
                                          htmlAttributes: new { id = "DataTable" });
            return Json(new
                            {
                                Data = htmlString.ToHtmlString(),
                                Count = _mostPopular.Count() / 5
                            }, JsonRequestBehavior.AllowGet);
        }
    }
}
