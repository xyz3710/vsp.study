﻿namespace MvcApplication1.Models
{
    public class FavouriteGivenName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}