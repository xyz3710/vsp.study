﻿/**********************************************************************************************************************/
/*	Domain		:	Razor_Helper.MyHelpers.MyHelpers
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 6월 21일 화요일 오후 7:49
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	Web.config의 system.web/pages/namespaces에 "Razor_Helper.MyHelpers"를 추가하면 
 *					cshtml 파일 내에서 @using Razor_Helper.MyHelpers 를 하는 일을 줄일 수 있다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Razor_Helper.MyHelpers
{
	public static class MyHelpers
	{
		public static MvcHtmlString Image(this HtmlHelper helper, string src, string altText)
		{
			var builder = new TagBuilder("img");

			builder.MergeAttribute("src", src);
			builder.MergeAttribute("alt", altText);

			return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
		}
	}
}