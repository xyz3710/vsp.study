﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.IO;

namespace Razor_Helper.Controllers
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/

		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// GET: /Home/ResizeImage
		/// </summary>
		/// <returns></returns>
		public ActionResult ResizeImage()
		{
			return View();
		}

		/// <summary>
		/// POST: /Home/ResizeImage
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult ResizeImage(string enctype)
		{
			const string root = @"~/WebImage/";
			WebImage photo = WebImage.GetImageFromRequest();
			string newFileName = string.Empty;
			string imagePath = string.Empty;
			string imageThumbPath = string.Empty;

			if (photo != null)
			{
				newFileName = string.Format("{0}_{1}", Guid.NewGuid().ToString(), Path.GetFileName(photo.FileName));
				imagePath = Path.Combine("images", newFileName);
				photo.Save(Path.Combine(root, imagePath));
				imageThumbPath = Path.Combine(@"images\thumbs\" + newFileName);
				photo.Resize(60, 60, true, true);
				photo.Save(Path.Combine(root, imageThumbPath));
			}

			ViewBag.ImagePath = imagePath;
			ViewBag.ImageThumbPath = imageThumbPath;

			return RedirectToAction("ResizeImage");
		}

		public ActionResult AddTextWatermark()
		{
			return View();
		}

		public ActionResult WebCache()
		{
			return View();
		}

		public ActionResult Recaptcha()
		{
			return View();
		}

		public ActionResult LiveMailTest()
		{
			return View();
		}

		public ActionResult GMailTest()
		{
			return View();
		}
	}
}
