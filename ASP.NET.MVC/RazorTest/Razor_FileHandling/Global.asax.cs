﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MvcCodeRouting;

namespace Razor_FileHandling
{
	// 참고: IIS6 또는 IIS7 클래식 모드를 사용하도록 설정하는 지침을 보려면 
	// http://go.microsoft.com/?LinkId=9394801을 방문하십시오.

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Default", // 경로 이름
				"{controller}/{action}/{id}", // 매개 변수가 있는 URL
				new
				{
					controller = "Home",
					action = "Index",
					id = UrlParameter.Optional
				} // 매개 변수 기본값
			);

			// http://mvccoderouting.codeplex.com/ 에 의해 변경
			// 아래와 같이 하면 [HttpPost] 속성이 있는 action도 Get으로 처리한다.
			//routes.MapCodeRoutes(typeof(Razor_FileHandling.Controllers.HomeController).Namespace);

			/*
			// ~/routes.axd로 접속하면 아래와 같은 코드가 나온다
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.MapRoute(null, "{action}", new
			{
				controller = @"Home",
				action = @"Index"
			}, new
			{
				action = @"Index|FileWrite|FileWriteAction",
				__mvccoderouting = new MvcCodeRouting.CodeRoutingConstraint()
			}, new[] { "Razor_FileHandling.Controllers" }); 
			*/
		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
		}
	}
}