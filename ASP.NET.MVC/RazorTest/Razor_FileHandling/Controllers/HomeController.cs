﻿/**********************************************************************************************************************/
/*	Domain		:	Razor_FileHandling.Controllers.HomeController
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 6월 1일 수요일 오후 5:00
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	FileUpload를 사용하려면 Library Package Manager에 ASP.NET Web Helpers(Web Helper로 검색)를 설치해야 한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;

namespace Razor_FileHandling.Controllers
{
	public class HomeController : Controller
	{
		#region Constants
		private const string TARGET_FILE_NAME = "data.txt";
		private const string WORKDATA_FOLDER = "WorkData";
		private const string INDEX_VIEW_NAME = "Index";
		#endregion

		//
		// GET: /Home/

		public ActionResult Index()
		{
			ViewBag.WorkRootPath = WorkDataFolder;

			return View(Directory.GetFiles(WorkDataFolder));
		}

		private string WorkDataFolder
		{
			get
			{
				string workRootPath = Path.Combine(Request.PhysicalApplicationPath, WORKDATA_FOLDER);

				if (Directory.Exists(workRootPath) == false)
					Directory.CreateDirectory(workRootPath);

				return workRootPath;
			}
		}

		private string TargetPath
		{
			get
			{
				return Path.Combine(WorkDataFolder, TARGET_FILE_NAME);
			}
		}

		/// <summary>
		/// GET: /Home/FileWrite
		/// </summary>
		/// <returns></returns>
		public ActionResult FileWrite()
		{
			return View();
		}

		//
		// POST: /Home/FileWriteAction
		[HttpPost]
		public ActionResult FileWriteAction()
		{
			string result = string.Empty;
			string firstName = Server.HtmlEncode(Request["FirstName"]);
			string lastName = Server.HtmlEncode(Request["LastName"]);
			string email = Server.HtmlEncode(Request["Email"]);

			// 문자열 연결
			string userData = string.Format("{0}, {1}, {2}\r\n", firstName, lastName, email);
			// 경로와 이름 설정
			string targetPath = TargetPath;

			// 파일에 쓰기
			try
			{
				System.IO.File.WriteAllText(targetPath, userData, Encoding.UTF8);

				result = targetPath + " 로 저장되었습니다.";
			}
			catch (Exception ex)
			{
				result = string.Format("쓰기 오류가 발생했습니다.\r\n{0}", ex.Message);
			}

			ViewBag.Result = result;

			return View("FileWrite");
		}

		/// <summary>
		/// GET: /Home/FileAppend
		/// </summary>
		/// <returns></returns>
		public ActionResult FileAppend()
		{
			return View();
		}

		/// <summary>
		/// POST: /Home/FileAppendAction
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult FileAppendAction()
		{
			string result = string.Empty;
			string firstName = Server.HtmlEncode(Request["FirstName"]);
			string lastName = Server.HtmlEncode(Request["LastName"]);
			string email = Server.HtmlEncode(Request["Email"]);

			// 문자열 연결
			string userData = string.Format("{0}, {1}, {2}\r\n", firstName, lastName, email);
			// 경로와 이름 설정
			string targetPath = TargetPath;

			// 파일에 쓰기
			try
			{
				System.IO.File.AppendAllText(targetPath, userData, Encoding.UTF8);

				result = targetPath + " 에 추가 되었습니다.";
			}
			catch (Exception ex)
			{
				result = string.Format("쓰기 오류가 발생했습니다.\r\n{0}", ex.Message);
			}

			ViewBag.Result = result;

			return View("FileAppend");
		}

		/// <summary>
		/// GET: /Home/FileRead
		/// </summary>
		/// <returns></returns>
		public ActionResult FileRead()
		{
			string result = string.Empty;
			string[] contents = null;
			string targetPath = TargetPath;

			try
			{
				if (System.IO.File.Exists(targetPath) == true)
				{
					contents = System.IO.File.ReadAllLines(targetPath);

					if (contents == null)
						result = "파일이 비었습니다.";
				}
				else
					result = "파일이 존재하지 않습니다.";
			}
			catch (Exception ex)
			{
				result = string.Format("읽기 오류가 발생했습니다.\r\n{0}", ex.Message);
			}

			ViewBag.Contents = contents;

			return View();
		}

		/// <summary>
		/// GET: /Home/FileDelete/D%3a%5cVSP%5cStudy%5cASP.NET.MVC%5cRazorTest%5cRazor_FileHandling%5cWorkData%5cDesign_20wx5mkt.1wb.winprf 
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public ActionResult FileDelete(string filePath)
		{
			bool result = false;


			if (System.IO.File.Exists(filePath) == true)
			{
				try
				{
					System.IO.File.Delete(filePath);
					result = true;
				}
				catch (Exception ex)
				{
					ViewBag.ErrorMessage = ex.Message;
				}
			}

			ViewBag.Result = result;

			return RedirectToAction(INDEX_VIEW_NAME);
		}

		/// <summary>
		/// POST: /Home/FileUpload
		/// </summary>
		/// <param name="fileUpload"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult FileUpload(IEnumerable<HttpPostedFileBase> fileUpload)
		{
			// fileUpload 또는 Request.Files에 업로드한 개체가 할당되지 않는다.
			string fileName = string.Empty;
			string savedFileName = string.Empty;

			if (fileUpload != null)
			{
				foreach (HttpPostedFileBase uploadFile in fileUpload)
				{
					fileName = Path.GetFileName(uploadFile.FileName);
					savedFileName = Path.Combine(WorkDataFolder, fileName);
					uploadFile.SaveAs(savedFileName);
				}

				ViewBag.UploadMessage = string.Format("파일 업로드 완료, 전체 업로드 파일 : {0}", Request.Files.Count);
			}
			else
				ViewBag.UploadMessage = "파일 업로드 실패";

			return RedirectToAction(INDEX_VIEW_NAME);
		}

		/// <summary>
		/// POST: /Home/FileUpload
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult FileUpload_1()
		{
			string fileName = string.Empty;
			string savedFileName = string.Empty;

			if (Request.Files != null)
			{
				foreach (HttpPostedFileBase uploadFile in Request.Files)
				{
					fileName = Path.GetFileName(uploadFile.FileName);
					savedFileName = Path.Combine(WorkDataFolder, fileName);
					uploadFile.SaveAs(savedFileName);
				}

				ViewBag.UploadMessage = string.Format("파일 업로드 완료, 전체 업로드 파일 : {0}", Request.Files.Count);
			}
			else
				ViewBag.UploadMessage = "파일 업로드 실패";

			return RedirectToAction(INDEX_VIEW_NAME);
		}

		public ActionResult FileUpload2()
		{
			return View();
		}

		/// <summary>
		/// POST: /Home/FileUpload2
		/// </summary>
		/// <param name="fileUpload"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult FileUpload2(IEnumerable<HttpPostedFileBase> fileUpload)
		{
			string fileName = string.Empty;
			string savedFileName = string.Empty;

			if (fileUpload != null)
			{
				foreach (HttpPostedFileBase uploadFile in fileUpload)
				{
					fileName = Path.GetFileName(uploadFile.FileName);
					savedFileName = Path.Combine(WorkDataFolder, fileName);
					uploadFile.SaveAs(savedFileName);
				}

				ViewBag.UploadMessage = string.Format("파일 업로드 완료, 전체 업로드 파일 : {0}", Request.Files.Count);
			}
			else
				ViewBag.UploadMessage = "파일 업로드 실패";

			return RedirectToAction(INDEX_VIEW_NAME);
		}

		public ActionResult FileUploadSample()
		{
			return View();
		}

		public ActionResult Upload()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Upload(IEnumerable<HttpPostedFileBase> fileUpload)
		{
			if (fileUpload != null)
			{
				foreach (var file in fileUpload)
				{
					if (file != null && file.ContentLength > 0)
					{
						var fileName = Path.GetFileName(file.FileName);
						var path = Path.Combine(WorkDataFolder, fileName);
						file.SaveAs(path);
					}
				}
			}

			return RedirectToAction("Index");
		}
	}
}
