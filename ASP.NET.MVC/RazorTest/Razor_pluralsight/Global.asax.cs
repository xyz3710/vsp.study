﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Razor_pluralsight.Infrastructure;
using System.Data.Entity;
using Razor_pluralsight.Models;
using System.Data.Common;

namespace Razor_pluralsight
{
	// 참고: IIS6 또는 IIS7 클래식 모드를 사용하도록 설정하는 지침을 보려면 
	// http://go.microsoft.com/?LinkId=9394801을 방문하십시오.

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
			filters.Add(new LogAttribute());
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			// 라우터의 순서가 중요하다 Cuisine route를 Default 아래로 내리면 인식하지 못한다.
			routes.MapRoute(
				"Cuisine",
				"Cuisine/{name}",
				new
				{
					controller = "Cuisine",
					action = "Search",
					name = UrlParameter.Optional,
				}
			);

			routes.MapRoute(
				"Default", // 경로 이름
				"{controller}/{action}/{id}", // 매개 변수가 있는 URL
				new
				{
					controller = "Home",
					action = "Index",
					id = UrlParameter.Optional
				} // 매개 변수 기본값
			);

		}

		protected void Application_Start()
		{
			Database.SetInitializer(new OdeToFoodDbInitializer());

			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
		}
	}

	public class OdeToFoodDbInitializer : DropCreateDatabaseIfModelChanges<OdeToFoodDb>
	{
		protected override void Seed(OdeToFoodDb context)
		{
			base.Seed(context);

			context.Reviews.Add(new Review
			{
				Body = "A fantistic  culinary pleasure.",
				DiningDate = DateTime.Parse("2011-06-20"),
				ID = 1,
				Rating = 9,
				Restaurant = new Restaurant
				{
					Id = 1,
					Name = "Mannequin Pis",
					ChefsName = "Martine",
					Address = new Address
					{
						City = "Chonju",
						State = "ChollaBukdo",
						Country = "Korea",
					}
				}
			});

			context.Reviews.Add(new Review
			{
				Body = "Only eat here if you must...",
				DiningDate = DateTime.Parse("2011-06-21"),
				ID = 2,
				Rating = 3,
				Restaurant = new Restaurant
				{
					Id = 2,
					Name = "동물병원",
					ChefsName = "최욱",
					Address = new Address
					{
						City = "대전",
						State = "충북",
						Country = "Korea",
					}
				}
			});

			context.Reviews.Add(new Review
			{
				Body = "I'm lucky to still be alive.'",
				DiningDate = DateTime.Parse("2011-06-22"),
				ID = 3,
				Rating = 3,
				Restaurant = new Restaurant
				{
					Id = 3,
					Name = "베네치아",
					ChefsName = "김종현",
					Address = new Address
					{
						City = "전주",
						State = "전북",
						Country = "Korea",
					}
				}
			});

			context.Reviews.Add(new Review
			{
				Body = "I'm lucky to still be alive.'",
				DiningDate = DateTime.Parse("2011-06-22"),
				ID = 4,
				Rating = 9,
				Restaurant = new Restaurant
				{
					Id = 4,
					Name = "메차쿠차",
					ChefsName = "정상호",
					Address = new Address
					{
						City = "전주",
						State = "전북",
						Country = "Korea",																
					}
				}
			});

			context.Reviews.Add(new Review
			{
				Body = "돈까스가 맛나다.",
				DiningDate = DateTime.Parse("2011-06-22"),
				ID = 4,
				Rating = 9,
				Restaurant = context.Restaurants.Find(4),
			});

			context.SaveChanges();
		}
	}
}