﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Razor_pluralsight.Models;
using System.Threading;

namespace Razor_pluralsight.Controllers
{
	public class HomeController : Controller
	{
		OdeToFoodDb _db = new OdeToFoodDb();

		//[RequireHttps]
		public ActionResult Index()
		{
			// RouteData의 key는 Global.asax의 routes.MapRoute의 Url 데이터를 나타낸다.
			ViewBag.Message = string.Format(
								  "{0}::{1} {2}",
								  RouteData.Values["controller"],
								  RouteData.Values["action"],
								  RouteData.Values["id"]);

			return View();
		}

		[OutputCache(CacheProfile="About", VaryByParam="*")]
		public ActionResult About(string firstName, string lastName)
		{
			// OutputCache의 profile key로 About으로 지정하고 web.config system.web 하위에서 설정할 수 있다.
			ViewBag.FirstName = firstName;
			ViewBag.LastName = lastName;

			return View();
		}

		public PartialViewResult LastestReview()
		{
			Random random = new Random(1);
			Thread.Sleep(100);
			var review = _db.Reviews.FindTheLastest(random.Next(1, 3)).Single();

			return PartialView("_Review", review);
		}

		public PartialViewResult Search(string q)
		{
			var restaurants = _db.Restaurants
				.Where(r => r.Name.Contains(q) || string.IsNullOrEmpty(q) == true);

			return PartialView("_RestaurantSearchResults", restaurants);
		}

		public ActionResult QuickSearch(string term)
		{
			var restaurants = _db.Restaurants
				//.Where(r => r.Name.Contains(term) == true)
				.Select(r => new
				{
					label = r.Name
				});

			return Json(restaurants, JsonRequestBehavior.AllowGet);
		}
	}
}
