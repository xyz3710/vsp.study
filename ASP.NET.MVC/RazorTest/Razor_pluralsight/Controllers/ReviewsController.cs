﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Razor_pluralsight.Models;
using System.ComponentModel.DataAnnotations;

namespace Razor_pluralsight.Controllers
{
	public class ReviewsController : Controller
	{
		private OdeToFoodDb db = new OdeToFoodDb();

		//
		// GET: /Reviews/

		public ViewResult Index()
		{
			return View(db.Reviews.OrderByDescending(r => r.ID));
		}

		//
		// GET: /Reviews/Details/5

		public ViewResult Details(int id)
		{
			Review review = db.Reviews.Find(id);
			return View(review);
		}

		//
		// GET: /Reviews/Create
		public ActionResult Create()
		{
			return View(new Review());
		}

		//
		// POST: /Reviews/Create

		[HttpPost]
		public ActionResult Create(Review newReview, int restaurantId = 0)
		{
			try
			{
				if (restaurantId == 0)
					db.Reviews.Add(newReview);
				else
				{
					var restaurant = db.Restaurants.Find(restaurantId);

					restaurant.Reviews.Add(newReview);
				}

				db.SaveChanges();

				return RedirectToAction("Details", "Restaurant", new
				{
					restaurantId
				});
			}
			catch
			{
				return View();
			}
		}


		//
		// GET: /Reviews/Edit/5

		public ActionResult Edit(int id)
		{
			Review review = db.Reviews.Find(id);
			return View(review);
		}

		//
		// POST: /Reviews/Edit/5

		[HttpPost]
		public ActionResult Edit(Review review)
		{
			if (ModelState.IsValid)
			{
				db.Entry(review).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(review);
		}

		//
		// GET: /Reviews/Delete/5

		public ActionResult Delete(int id)
		{
			Review review = db.Reviews.Find(id);
			return View(review);
		}

		//
		// POST: /Reviews/Delete/5

		[HttpPost, ActionName("Delete")]
		public ActionResult DeleteConfirmed(int id)
		{
			Review review = db.Reviews.Find(id);
			db.Reviews.Remove(review);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
	}
}