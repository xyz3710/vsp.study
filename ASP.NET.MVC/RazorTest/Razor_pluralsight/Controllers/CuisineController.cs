﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Razor_pluralsight.Infrastructure;

namespace Razor_Common.Controllers
{
	//[Authorize]
	public class CuisineController : Controller
	{
		//
		// GET: /Cuisine/
		// GET: /Cuisine?name=german
		//[Authorize]
		[Log]
		public ActionResult Search(string name = "*")
		{
			// TODO: Web.config의 <system.web> section에 <customErrors mode="On"/> 를 추가하고 테스트를 해본다.
			// by KIMKIWON\xyz37(김기원) in 2011년 6월 21일 화요일 오후 2:58
			if (name == "error")
				throw new Exception("ooops!!");

			if (name == "*")
			{
				// Redirection To Action
				//return RedirectToAction("Index", "Home", new
				//{
				//	id = 3
				//});

				// Redirection To Route
				return RedirectToRoute("Cuisine", new
				{
					name = "german"
				});

				// Redirection To File
				//return File(Server.MapPath("~/Content/Site.css"), "text/css");

				// Redirection To JSON
				//return Json(new
				//{
				//	cuisineName = name
				//}, JsonRequestBehavior.AllowGet);
			}

			// parameter로 전달해도 되고 RouteData에서 가져와도 된다.
			//name = Convert.ToString(RouteData.Values["name"]);
			//name = Server.HtmlEncode(name);

			return Content(string.Format("You have reached Cuisine controller with [{0}].<br/>Exception test로 url에 Cuisine/error 를 넘겨본다.", name));
		}
	}
}
