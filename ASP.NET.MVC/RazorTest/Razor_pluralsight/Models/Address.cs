﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Razor_pluralsight.Models
{
	public class Address
	{
		/// <summary>
		/// State를 구하거나 설정합니다.
		/// </summary>
		public string State
		{
			get;
			set;
		}

		/// <summary>
		/// City를 구하거나 설정합니다.
		/// </summary>
		public string City
		{
			get;
			set;
		}

		/// <summary>
		/// Street를 구하거나 설정합니다.
		/// </summary>
		public string Street
		{
			get;
			set;
		}

		/// <summary>
		/// Country를 구하거나 설정합니다.
		/// </summary>
		public string Country
		{
			get;
			set;
		}
	}
}
