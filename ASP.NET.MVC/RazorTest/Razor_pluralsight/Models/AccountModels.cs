﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace Razor_pluralsight.Models
{

	public class ChangePasswordModel
	{
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "현재 암호")]
		public string OldPassword
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, ErrorMessage = "{0}은(는) {2}자 이상이어야 합니다.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "새 암호")]
		public string NewPassword
		{
			get;
			set;
		}

		[DataType(DataType.Password)]
		[Display(Name = "새 암호 확인")]
		[Compare("NewPassword", ErrorMessage = "새 암호와 확인 암호가 일치하지 않습니다.")]
		public string ConfirmPassword
		{
			get;
			set;
		}
	}

	public class LogOnModel
	{
		[Required]
		[Display(Name = "사용자 이름")]
		public string UserName
		{
			get;
			set;
		}

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "암호")]
		public string Password
		{
			get;
			set;
		}

		[Display(Name = "사용자 이름 및 암호 저장")]
		public bool RememberMe
		{
			get;
			set;
		}
	}

	public class RegisterModel
	{
		[Required]
		[Display(Name = "사용자 이름")]
		public string UserName
		{
			get;
			set;
		}

		[Required]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "전자 메일 주소")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, ErrorMessage = "{0}은(는) {2}자 이상이어야 합니다.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "암호")]
		public string Password
		{
			get;
			set;
		}

		[DataType(DataType.Password)]
		[Display(Name = "암호 확인")]
		[Compare("Password", ErrorMessage = "암호와 확인 암호가 일치하지 않습니다.")]
		public string ConfirmPassword
		{
			get;
			set;
		}
	}

	public interface IFormsAuthenticationService
	{
		void SignIn(string userName, bool createPersistentCookie);
		void SignOut();
	}

	public class FormsAuthenticationService : IFormsAuthenticationService
	{
		public void SignIn(string userName, bool createPersistentCookie)
		{
			if (String.IsNullOrEmpty(userName))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "userName");

			FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
		}

		public void SignOut()
		{
			FormsAuthentication.SignOut();
		}
	}

	public interface IMembershipService
	{
		int MinPasswordLength
		{
			get;
		}

		bool ValidateUser(string userName, string password);
		MembershipCreateStatus CreateUser(string userName, string password, string email);
		bool ChangePassword(string userName, string oldPassword, string newPassword);
	}

	public class AccountMembershipService : IMembershipService	
	{
		private readonly MembershipProvider _provider;

		public AccountMembershipService()
			: this(null)
		{
		}

		public AccountMembershipService(MembershipProvider provider)
		{
			_provider = provider ?? Membership.Provider;
		}

		public int MinPasswordLength
		{
			get
			{
				return _provider.MinRequiredPasswordLength;
			}
		}

		public bool ValidateUser(string userName, string password)
		{
			if (String.IsNullOrEmpty(userName))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "userName");
			if (String.IsNullOrEmpty(password))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "password");

			return _provider.ValidateUser(userName, password);
		}

		public MembershipCreateStatus CreateUser(string userName, string password, string email)
		{
			if (String.IsNullOrEmpty(userName))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "userName");
			if (String.IsNullOrEmpty(password))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "password");
			if (String.IsNullOrEmpty(email))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "email");

			MembershipCreateStatus status;
			_provider.CreateUser(userName, password, email, null, null, true, null, out status);
			return status;
		}

		public bool ChangePassword(string userName, string oldPassword, string newPassword)
		{
			if (String.IsNullOrEmpty(userName))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "userName");
			if (String.IsNullOrEmpty(oldPassword))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "oldPassword");
			if (String.IsNullOrEmpty(newPassword))
				throw new ArgumentException("값은 null이거나 비어 있을 수 없습니다.", "newPassword");

			// 기본 ChangePassword()는 특정 실패 시나리오에서 false를 반환하지 않고
			// 예외를 throw합니다.
			try
			{
				MembershipUser currentUser = _provider.GetUser(userName, true /* userIsOnline */);
				return currentUser.ChangePassword(oldPassword, newPassword);
			}
			catch (ArgumentException)
			{
				return false;
			}
			catch (MembershipPasswordException)
			{
				return false;
			}
		}
	}
}
