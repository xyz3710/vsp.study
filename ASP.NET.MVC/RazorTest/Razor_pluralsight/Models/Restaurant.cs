using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace Razor_pluralsight.Models
{
	public class Restaurant
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// ChefsName를 구하거나 설정합니다.
		/// </summary>
		public string ChefsName
		{
			get;
			set;
		}

		/// <summary>
		/// Address를 구하거나 설정합니다.
		/// </summary>
		public Address Address
		{
			get;
			set;
		}

		public virtual ICollection<Review> Reviews
		{
			get;
			set;
		}
	}
}

