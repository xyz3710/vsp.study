﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;

namespace Razor_pluralsight.Models
{
	public class Review : IValidatableObject
	{
		#region Constructors
		/// <summary>
		/// Review class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Review()
		{
			DiningDate = DateTime.Now;
			Rating = 1;
		}
		#endregion

		[Key]
		public virtual int ID
		{
			get;
			set;
		}

		/// <summary>
		/// Rating를 구하거나 설정합니다.
		/// </summary>
		[Range(1, 10)]
		[DisplayFormat(DataFormatString="{0:#,##0}", ApplyFormatInEditMode=true)]
		[Required(ErrorMessageResourceType=(typeof(Razor_pluralsight.Views.Home.HomeResource)), ErrorMessageResourceName="Greeting")]
		public virtual int Rating
		{
			get;
			set;
		}

		[Required()]
		[DataType(DataType.MultilineText)]
		[MinLength(3)]
		[AllowHtml]
		public virtual string Body
		{
			get;
			set;
		}

		/// <summary>
		/// DiningDate를 구하거나 설정합니다.
		/// </summary>
		[DisplayName("식사일자")]
		[DataType(DataType.DateTime)]
		[DisplayFormat(DataFormatString="{0:yyyy-MM-dd}", ApplyFormatInEditMode=true)]
		public virtual DateTime DiningDate
		{
			get;
			set;
		}

		public virtual Restaurant Restaurant
		{
			get;
			set;
		}

		#region IValidatableObject 멤버

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			// 아래 field가 없으면 Html.ValidationSummary(true) 영역에 나타나는데
			// field를 넣으면 Range, Required와 같이 입력 컨트롤 옆에 나타난다.
			var field = new[] { "DiningDate" };

			if (DiningDate > DateTime.Now)
				yield return new ValidationResult("식사일자는 미래 날짜가 될 수 없습니다.", field);

			if (DiningDate < DateTime.Now.AddYears(-1))
				yield return new ValidationResult("식사일자는 1년 전일 수 없습니다.", field);
		}

		#endregion
	}
}