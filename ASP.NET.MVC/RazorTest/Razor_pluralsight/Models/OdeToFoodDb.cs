﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Common;
using System.Configuration;

namespace Razor_pluralsight.Models
{
	public class OdeToFoodDb : DbContext
	{
		public virtual DbSet<Review> Reviews
		{
			get;
			set;
		}

		public virtual DbSet<Restaurant> Restaurants
		{
			get;
			set;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Restaurant>()
				.Property(r => r.Id)
				.HasColumnName("restId");
						
			base.OnModelCreating(modelBuilder);
		}
	}
}