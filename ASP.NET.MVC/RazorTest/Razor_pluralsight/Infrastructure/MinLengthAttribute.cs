﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Razor_pluralsight.Infrastructure
{
	[AttributeUsage(AttributeTargets.Property)]
	public class MinLengthAttribute : ValidationAttribute
	{
		#region Constructors
		/// <summary>
		/// MinLengthAttribute class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public MinLengthAttribute(int minCharacters)
		{
			MinCharacters = minCharacters;
		}

		/// <summary>
		/// MinLengthAttribute class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public MinLengthAttribute()
		{
			
		}
		#endregion
		public int MinCharacters
		{
			get;
			set;
		}

		public override bool IsValid(object value)
		{
			string valueString = value as string;

			return (valueString != null && valueString.Length > MinCharacters);
		}
	}
}