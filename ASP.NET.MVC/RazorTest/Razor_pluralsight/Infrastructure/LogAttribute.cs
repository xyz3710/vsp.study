﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;

namespace Razor_pluralsight.Infrastructure
{
	public class LogAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			Debug.WriteLine(string.Format("Controller: {0}", filterContext.RouteData.Values["controller"])
				, "LogAttribute.OnActionExecuting");

			base.OnActionExecuting(filterContext);
		}

		public override void OnActionExecuted(ActionExecutedContext filterContext)
		{
			Debug.WriteLine(filterContext.Exception, "LogAttribute.OnActionExecuted");

			base.OnActionExecuted(filterContext);
		}

		public override void OnResultExecuting(ResultExecutingContext filterContext)
		{
			Debug.WriteLine(filterContext.Controller, "LogAttribute.OnResultExecuting");

			base.OnResultExecuting(filterContext);
		}

		public override void OnResultExecuted(ResultExecutedContext filterContext)
		{
			Debug.WriteLine(filterContext.Exception, "LogAttribute.OnResultExecuted");

			base.OnResultExecuted(filterContext);
		}
	}
}