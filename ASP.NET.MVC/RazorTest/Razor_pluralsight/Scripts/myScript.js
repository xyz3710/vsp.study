﻿/// <reference path="jquery-1.6.1-vsdoc.js" />
/// <reference path="jquery-ui-1.8.13.js" />

$(document).ready(function () {

	// Home/Index.cshtml의 input type의 class="autocomplete" 로 지정 했을 경우
	//	$(".autocomplete").autocomplete();

	// Home/Index.cshtml의 input type에 data-autocomplete를 지정했을 경우
	$(":input[data-autocomplete]").each(function () {
		$(this).autocomplete({ source: $(this).attr("data-autocomplete") });
	});

	$(":input[date-datepicker]").datepicker({ dateformat: "yyyy/mm/dd" });

})