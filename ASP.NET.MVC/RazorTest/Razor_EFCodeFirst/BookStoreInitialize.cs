﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Razor_EFCodeFirst.Models;

namespace Razor_EFCodeFirst
{
	public class BookStoreInitialize : DropCreateDatabaseIfModelChanges<BookStore>
	{
		protected override void Seed(BookStore context)
		{
			base.Seed(context);

			Person author1 = new Person
			{
				Name = "김기원",
				SSN = "kkw",
			};

			Person author2 = new Person
			{
				Name = "황태영",
				SSN = "hty",
			};

			Person author3 = new Person
			{
				Name = "김영재",
				SSN = "kyj",
			};

			Person author4 = new Person
			{
				Name = "김수민",
				SSN = "ksm",
			};

			context.Persons.Add(author1);
			context.Persons.Add(author2);
			context.Persons.Add(author3);
			context.Persons.Add(author4);

			BookType BookTypeNovel = new BookType
			{
				Name = "Novel"
			};
			BookType BookTypeCatoon = new BookType
			{
				Name = "Catoon"
			};
			BookType BookTypeFX = new BookType
			{
				Name = "FX"
			};
			context.BookType.Add(BookTypeNovel);
			context.BookType.Add(BookTypeCatoon);
			context.BookType.Add(BookTypeFX);

			Book book1 = new Book
						{
							Author = author1,
							AuthorId = author1.SSN,
							BookType = BookTypeNovel,
							ISBN = "a123456",
							Title = "소설보다 쉬운 C#",							
						};
			context.Books.Add(book1);
			Book book2 = new Book
						{
							Author = author1,
							AuthorId = author1.SSN,
							BookType = BookTypeNovel,
							ISBN = "a123457",
							Title = "알기 쉬운 Redmine",
						};
			context.Books.Add(book2);
			Book book3 = new Book
						{
							Author = author1,
							AuthorId = author1.SSN,
							BookType = BookTypeNovel,
							ISBN = "a123458",
							Title = "Ruby on Rails",
						};
			context.Books.Add(book3);
			Book book4 = new Book
						{
							Author = author2,
							AuthorId = author2.SSN,
							BookType = BookTypeNovel,
							ISBN = "a123459",
							Title = "육아 일기",
						};
			context.Books.Add(book4);
			Book book5 = new Book
						{
							Author = author3,
							AuthorId = author3.SSN,
							BookType = BookTypeCatoon,
							ISBN = "a123460",
							Title = "블럭 쌓기",
						};
			context.Books.Add(book5);
			Book book6 = new Book
						{
							Author = author4,
							AuthorId = author4.SSN,
							BookType = BookTypeCatoon,
							ISBN = "a123461",
							Title = "엄마에게 떼 쓰기",
						};
			context.Books.Add(book6);

			context.Reviews.Add(new Review
			{
				Comment = "C# 쉽다",
				Subject = book1,
			});
			context.Reviews.Add(new Review
			{
				Comment = "C# Good",
				Subject = book1,
			});
			context.Reviews.Add(new Review
			{
				Comment = "C# Awesome",
				Subject = book1,
			});
			context.Reviews.Add(new Review
			{
				Comment = "C# Excellent",
				Subject = book1,
			});
			context.Reviews.Add(new Review
			{
				Comment = "쉬운 Redmine",
				Subject = book2,
			});
			context.Reviews.Add(new Review
			{
				Comment = "쉬운 Ruby",
				Subject = book3,
			});
			context.Reviews.Add(new Review
			{
				Comment = "잘 키우시네요 good",
				Subject = book4,
			});
			context.Reviews.Add(new Review
			{
				Comment = "잘 키우시네요 excellent",
				Subject = book4,
			});
			context.Reviews.Add(new Review
			{
				Comment = "잘 키우시네요 groovy",
				Subject = book4,
			});
			context.Reviews.Add(new Review
			{
				Comment = "cool!!",
				Subject = book5,
			});
			context.Reviews.Add(new Review
			{
				Comment = "good",
				Subject = book5,
			});
			context.Reviews.Add(new Review
			{
				Comment = "Wow",
				Subject = book6,
			});
			context.Reviews.Add(new Review
			{
				Comment = "Fantastic",
				Subject = book6,
			});
		}
	}
}
