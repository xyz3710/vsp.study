﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Razor_EFCodeFirst.Models;
using PagedList;
using Trirand.Web.Mvc;

namespace Razor_EFCodeFirst.Controllers
{
	public class BookController : Controller
	{
		private BookStore db = new BookStore();

		//
		// GET: /Book/

		public ActionResult Index()
		{
			// Get the model (setup) of the grid defined in the /Models folder.
			var gridModel = new OrdersJqGridModel();
			var ordersGrid = gridModel.OrdersGrid;

			// customize the default Orders grid model with custom settings
			// NOTE: you need to call this method in the action that fetches the data as well,
			// so that the models match
			SetUpGrid(ordersGrid);

			// Pass the custmomized grid model to the View
			return View(gridModel);
		}

		private void SetUpGrid(JQGrid ordersGrid)
		{
			//// Customize/change some of the default settings for this model
			//// ID is a mandatory field. Must by unique if you have several grids on one page.
			//ordersGrid.ID = "OrdersGrid";
			//// Setting the DataUrl to an action (method) in the controller is required.
			//// This action will return the data needed by the grid
			//ordersGrid.DataUrl = Url.Action("SearchGridDataRequested");
			//ordersGrid.EditUrl = Url.Action("EditRows");
			//// show the search toolbar
			//ordersGrid.ToolBarSettings.ShowSearchToolBar = true;
			//ordersGrid.Columns.Find(c => c.DataField == "OrderID").Searchable = false;
			//ordersGrid.Columns.Find(c => c.DataField == "OrderDate").Searchable = false;

			//SetUpCustomerIDSearchDropDown(ordersGrid);
			//SetUpFreightSearchDropDown(ordersGrid);
			//SetShipNameSearchDropDown(ordersGrid);

			//ordersGrid.ToolBarSettings.ShowEditButton = true;
			//ordersGrid.ToolBarSettings.ShowAddButton = true;
			//ordersGrid.ToolBarSettings.ShowDeleteButton = true;
			//ordersGrid.EditDialogSettings.CloseAfterEditing = true;
			//ordersGrid.AddDialogSettings.CloseAfterAdding = true;

			//// setup the dropdown values for the CustomerID editing dropdown
			//SetUpCustomerIDEditDropDown(ordersGrid);
		}

		public ActionResult GetBooks()
		{
			var books = db.Books;//.Include(b => b.Author).Include(b => b.BookType);

			return Json(books);
		}

		public ViewResult Index2(string sortColumn, int? page = 1)
		{
			var books = db.Books.Select(b => b);//.Include(b => b.Author).Include(b => b.BookType);
			ViewBag.TitleSortParam = string.IsNullOrEmpty(sortColumn) == true ? "Title desc" : string.Empty;
			ViewBag.AuthorSortParam = string.IsNullOrEmpty(sortColumn) || sortColumn == "Author" ? "Author desc" : "Author";

			switch (sortColumn)
			{
				case "Title desc":
					books = books.OrderByDescending(b => b.Title);

					break;
				case "Author":
					books = books.OrderBy(b => b.Author.SSN);

					break;
				case "Author desc":
					books = books.OrderByDescending(b => b.Author.SSN);

					break;
				default:
					books = books.OrderBy(b => b.Title);

					break;
			}

			int pageSize = 2;
			int pageIndex = (page ?? 1) - 1;

			return View(books.ToPagedList(pageIndex, pageSize));
		}

		//
		// GET: /Book/Details/5

		public ViewResult Details(string id)
		{
			Book book = db.Books.Find(id);
			return View(book);
		}

		//
		// GET: /Book/Create

		public ActionResult Create()
		{
			ViewBag.AuthorId = new SelectList(db.Persons, "SSN", "Name");
			ViewBag.BookTypeId = new SelectList(db.BookType, "Id", "Name");
			return View();
		}

		//
		// POST: /Book/Create

		[HttpPost]
		public ActionResult Create(Book book)
		{
			if (ModelState.IsValid)
			{
				db.Books.Add(book);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			ViewBag.AuthorId = new SelectList(db.Persons, "SSN", "Name", book.AuthorId);
			ViewBag.BookTypeId = new SelectList(db.BookType, "Id", "Name", book.BookTypeId);
			return View(book);
		}

		//
		// GET: /Book/Edit/5

		public ActionResult Edit(string id)
		{
			Book book = db.Books.Find(id);
			// ViewBag 항목으로 Key를 던지면 View의 DropDown에서 자동으로 인식한다.
			ViewBag.AuthorId = new SelectList(db.Persons, "SSN", "Name", book.AuthorId);
			ViewBag.BookTypeId = new SelectList(db.BookType, "Id", "Name", book.BookTypeId);

			return View(book);
		}

		//
		// POST: /Book/Edit/5

		[HttpPost]
		public ActionResult Edit(Book book)
		{
			if (ModelState.IsValid)
			{
				db.Entry(book).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.AuthorId = new SelectList(db.Persons, "SSN", "Name", book.AuthorId);
			ViewBag.BookTypeId = new SelectList(db.BookType, "Id", "Name", book.BookTypeId);

			return View(book);
		}

		//
		// GET: /Book/Delete/5

		public ActionResult Delete(string id)
		{
			Book book = db.Books.Find(id);
			return View(book);
		}

		//
		// POST: /Book/Delete/5

		[HttpPost, ActionName("Delete")]
		public ActionResult DeleteConfirmed(string id)
		{
			Book book = db.Books.Find(id);
			db.Books.Remove(book);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
	}
}