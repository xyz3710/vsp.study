﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Razor_EFCodeFirst.Models
{
	public class Book
	{
		/// <summary>
		/// ISBN를 구하거나 설정합니다.
		/// </summary>
		[Key]
		[ScaffoldColumn(true)]
		[Display(Name="ISBN")]
		[Column(Order=0)]
		[Required()]
		public string ISBN
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="제목")]
		[ScaffoldColumn(true)]
		[Column(Order=1)]
		[Required()]
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// AuthorId를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="저자 ")]
		[Required()]
		public string AuthorId
		{
			get;
			set;
		}

		/// <summary>
		/// Author를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="저자")]
		[ForeignKey("AuthorId")]
		public virtual Person Author
		{
			get;
			set;
		}

		/// <summary>
		/// BookTypeId를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="책 종류")]
		[Required()]
		public int BookTypeId
		{
			get;
			set;
		}

		/// <summary>
		/// BookType를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="책 종류")]
		[ForeignKey("BookTypeId")]
		public virtual BookType BookType
		{
			get;
			set;
		}

		/// <summary>
		/// Reviews를 구하거나 설정합니다.
		/// </summary>
		public virtual ICollection<Review> Reviews
		{
			get;
			set;
		}

	}
}