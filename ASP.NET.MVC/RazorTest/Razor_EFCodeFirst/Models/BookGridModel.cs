﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trirand.Web.Mvc;
using System.Web.UI.WebControls;

namespace Razor_EFCodeFirst.Models
{
	public class BookGridViewModel
	{
		public BookGridViewModel()
		{
			BookGrid = new JQGrid
			{
				Columns = new List<JQGridColumn>()
								 {
									 new JQGridColumn { DataField = "ISBN", 
														// always set PrimaryKey for Add,Edit,Delete operations
														// if not set, the first column will be assumed as primary key														
														PrimaryKey = true,
														Editable = false,
														Width = 100 },
									 new JQGridColumn { DataField = "Title", 
														HeaderText = "제목",
														Editable = true,
														Width = 100, },
									 new JQGridColumn { DataField = "AuthorId",
														HeaderText = "저자",
														Editable = true,
														Width = 100 },
									 new JQGridColumn { DataField = "BookTypeId", 
														HeaderText = "책종류",
														Editable = true,
														Width = 75 },
								 },
				Width = Unit.Pixel(800),
			};

			BookGrid.ToolBarSettings.ShowRefreshButton = true;
			//BookGrid.PagerSettings.PageSize = 3;
			BookGrid.ToolBarSettings.ShowSearchToolBar = true;
			BookGrid.ToolBarSettings.ShowSearchButton = true;
			BookGrid.ToolBarSettings.ShowEditButton = true;
			BookGrid.ToolBarSettings.ShowAddButton = true;
			BookGrid.ToolBarSettings.ShowDeleteButton = true;
			BookGrid.EditDialogSettings.CloseAfterEditing = true;
			BookGrid.AddDialogSettings.CloseAfterAdding = true;
			BookGrid.SearchDialogSettings.Modal = false;
			BookGrid.MultiSelect = true;
		}

		public JQGrid BookGrid
		{
			get;
			set;
		}
	}
}