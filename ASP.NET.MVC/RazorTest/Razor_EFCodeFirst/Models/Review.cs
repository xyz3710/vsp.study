﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Razor_EFCodeFirst.Models
{
	public class Review
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// ISBN를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="책 ISBN")]
		public string ISBN
		{
			get;
			set;
		}

		/// <summary>
		/// Comment를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="서평")]
		public string Comment
		{
			get;
			set;
		}

		/// <summary>
		/// Subject를 구하거나 설정합니다.
		/// </summary>
		[ForeignKey("ISBN")]
		[Display(Name="책 제목")]
		public virtual Book Subject
		{
			get;
			set;
		}

	}
}