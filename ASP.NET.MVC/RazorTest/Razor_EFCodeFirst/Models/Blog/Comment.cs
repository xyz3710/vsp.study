﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Razor_EFCodeFirst.Models
{
	public class Comment
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// Remark를 구하거나 설정합니다.
		/// </summary>
		public string Remark
		{
			get;
			set;
		}
        
        
	}
}
