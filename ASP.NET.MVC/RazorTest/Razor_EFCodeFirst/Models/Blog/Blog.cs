﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Razor_EFCodeFirst.Models
{
	public class Blog
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// BloggerName를 구하거나 설정합니다.
		/// </summary>
		public string BloggerName
		{
			get;
			set;
		}

		[InverseProperty("CreatedBy")]
		public List<Post> PostsWritten
		{
			get;
			set;
		}
		[InverseProperty("UpdatedBy")]
		public List<Post> PostsUpdated
		{
			get;
			set;
		}

		/// <summary>
		/// Post를 구하거나 설정합니다.
		/// </summary>
		public virtual ICollection<Post> Post
		{
			get;
			set;
		}
		
	}
}