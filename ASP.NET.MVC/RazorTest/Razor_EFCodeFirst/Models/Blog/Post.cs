﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Razor_EFCodeFirst.Models
{
	public class Post
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// DateCreated를 구하거나 설정합니다.
		/// </summary>
		public DateTime DateCreated
		{
			get;
			set;
		}

		/// <summary>
		/// Content를 구하거나 설정합니다.
		/// </summary>
		public string Content
		{
			get;
			set;
		}

		/// <summary>
		/// BlogId를 구하거나 설정합니다.
		/// </summary>
		public int BlogId
		{
			get;
			set;
		}

		/// <summary>
		/// BoardTypes를 구하거나 설정합니다.
		/// </summary>
		public BoardTypes BoardTypes
		{
			get;
			set;
		}

		public Person CreatedBy
		{
			get;
			set;
		}
		public Person UpdatedBy
		{
			get;
			set;
		}

		/// <summary>
		/// Blog를 구하거나 설정합니다.
		/// </summary>
		[ForeignKey("BlogId")]
		public virtual ICollection<Blog> Blog
		{
			get;
			set;
		}

		/// <summary>
		/// Comments를 구하거나 설정합니다.
		/// </summary>
		public ICollection<Comment> Comments
		{
			get;
			set;
		}

	}
}
