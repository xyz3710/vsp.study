﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Razor_EFCodeFirst.Views;

namespace Razor_EFCodeFirst.Models
{
	public class Person
	{
		/// <summary>
		/// SSN를 구하거나 설정합니다.
		/// </summary>
		[Key]
		[ScaffoldColumn(true)]
		[Display(ResourceType=typeof(Columns), Name="SSN")]
		public string SSN
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		[Display(ResourceType=typeof(Columns), Name="Name")]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		[Display(ResourceType=typeof(Columns), Name="Age")]
		public int Age
		{
			get;
			set;
		}

		/// <summary>
		/// Height를 구하거나 설정합니다.
		/// </summary>
		[Column(TypeName="Decimal")]
		public decimal Height
		{
			get;
			set;
		}

		/// <summary>
		/// Job를 구하거나 설정합니다.
		/// </summary>
		[EnumDataType(typeof(Job))]
		public Job Job
		{
			get;
			set;
		}
				
		/// <summary>
		/// Books를 구하거나 설정합니다.
		/// </summary>
		public ICollection<Book> Books
		{
			get;
			set;
		}

	}
}