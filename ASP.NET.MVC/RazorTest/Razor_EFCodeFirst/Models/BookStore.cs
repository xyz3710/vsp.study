﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Razor_EFCodeFirst.Models
{
	public class BookStore : DbContext
	{
		public DbSet<Book> Books
		{
			get;
			set;
		}

		/// <summary>
		/// Person를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Person> Persons
		{
			get;
			set;
		}

		/// <summary>
		/// BookReview를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Review> Reviews
		{
			get;
			set;
		}

		/// <summary>
		/// BookType를 구하거나 설정합니다.
		/// </summary>
		public DbSet<BookType> BookType
		{
			get;
			set;
		}

		/// <summary>
		/// Blog를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Blog> Blogs
		{
			get;
			set;
		}

		/// <summary>
		/// Post를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Post> Posts
		{
			get;
			set;
		}
		

		/// <summary>
		/// BookView를 구하거나 설정합니다.
		/// </summary>
		public ICollection<BookView> BookViews
		{
			get
			{
				//var bookView = Books.Include(b => b.Author)
				return null;
			}
		}
	}
}