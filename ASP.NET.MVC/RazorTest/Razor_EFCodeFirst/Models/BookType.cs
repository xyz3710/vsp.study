﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Razor_EFCodeFirst.Models
{
	[Table("BookType")]
	public class BookType
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// Types를 구하거나 설정합니다.
		/// </summary>
		[Display(Name="책 종류")]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Books를 구하거나 설정합니다.
		/// </summary>
		public virtual ICollection<Book> Books
		{
			get;
			set;
		}
	}
}
