﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Razor_EFCodeFirst.Models
{
	public class BookView : Book
	{
		/// <summary>
		/// AuthorName를 구하거나 설정합니다.
		/// </summary>
		[NotMapped]
		public string AuthorName
		{
			get
			{
				return Author.Name;
			}
		}

		/// <summary>
		/// BookTypeName를 구하거나 설정합니다.
		/// </summary>
		[NotMapped]
		public string BookTypeName
		{
			get
			{
				return BookType.Name;
			}
		}

	}
}