;(function($){
/**
 * jqGrid Korean Translation
 * Ki Won  Kim xyz37@naver.com
 * http://xyz37.blog.me
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = {
	defaults : {
		recordtext: "({0} - {1}) / {2}",
		emptyrecords: "데이터가 없습니다.",
		loadtext: "로드중...",
		pgtext : "{0} / {1}"
	},
	search : {
		caption: "검색...",
		Find: "찾기",
		Reset: "초기화",
		odata : ['＝', '≠', '＜', '≤','＞','≥', 'begins with','does not begin with','is in','is not in','ends with','does not end with','contains','does not contain'],
		groupOps: [	{ op: "AND", text: "all" },	{ op: "OR",  text: "any" }	],
		matchText: " 일치",
		rulesText: " 규칙"
	},
	edit : {
		addCaption: "레코드 추가",
		editCaption: "레코드 편집",
		bSubmit: "전송",
		bCancel: "취소",
		bClose: "닫기",
		saveData: "데이터가 변경되었습니다! 저장하시겠습니까?",
		bYes : "예",
		bNo : "아니요",
		bExit : "취소",
		msg: {
			required:"Field is required",
			number:"Please, enter valid number",
			minValue:"value must be greater than or equal to ",
			maxValue:"value must be less than or equal to",
			email: "is not a valid e-mail",
			integer: "Please, enter valid integer value",
			date: "Please, enter valid date value",
			url: "is not a valid URL. Prefix required ('http://' or 'https://')",
			nodefined : " is not defined!",
			novalue : " return value is required!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
			
		}
	},
	view : {
		caption: "레코드 보기",
		bClose: "닫기"
	},
	del : {
		caption: "삭제",
		msg: "선택한 레코드를 삭제하시겠습니까?",
		bSubmit: "삭제",
		bCancel: "취소"
	},
	nav : {
		edittext: "",
		edittitle: "선택된 행 편집",
		addtext:"",
		addtitle: "새로운 행 추가",
		deltext: "",
		deltitle: "선택된 행 삭제",
		searchtext: "",
		searchtitle: "레코드 검색",
		refreshtext: "",
		refreshtitle: "그리드 다시 로드",
		alertcap: "경고",
		alerttext: "행을 선택하세요.",
		viewtext: "",
		viewtitle: "선택된 행 보기"
	},
	col : {
		caption: "컬럼 선택",
		bSubmit: "확인",
		bCancel: "취소"
	},
	errors : {
		errcap : "에러",
		nourl : "Url이 설정되지 않음",
		norecords: "처리하기 위한 레코드가 없음",
		model : "Length of colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: ",", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"일", "월", "화", "수", "목", "금", "토",
				"일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"
			],
			monthNames: [
				"1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월",
				"1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"
			],
			AmPm : ["오전","오후","오전","오후"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n/j/Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
};
})(jQuery);
