﻿/**********************************************************************************************************************/
/*	Domain		:	Luxury.SmartAudio.Website.Custom.CustomViewEngine
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2011년 8월 14일 일요일 오후 2:12
/*	Purpose		:	RazorViewEngine의 customizing 한 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2011년 8월 16일 화요일 오후 3:23
/*	Changes		:	51degrees foundation (http://51degrees.codeplex.com/)을 적용하여 Mobile 접근을 검사하도록 수정
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	RazorViewEngine의 ViewLocationFormats, PartialViewLocationFormats, MasterLocationFormats 경로를 변경한다.
 *					Global.asax.cs에서 WebFormViewEngine도 등록할 경우 WebForm 에서도 동일한 작업을 해줘야 한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 8월 16일 화요일 오후 2:19
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Luxury.SmartAudio.Website
{
	/// <summary>
	/// RazorViewEngine의 customizing 한 기능을 제공합니다.
	/// </summary>
	public class CustomRazorViewEngine : RazorViewEngine
	{
		public const string IS_MOBILE_DEVICE_SESSION_KEY = "IsMobileDevice";

		#region Constructors
		/// <summary>
		/// CustomRazorViewEngine class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public CustomRazorViewEngine()
		{
			FileExtensions = new[] {
				"cshtml"
			};
			RedirectViewPostFix = ".Mobile";
		}
		#endregion

		/// <summary>
		/// RedirectViewPostFix를 구하거나 설정합니다.
		/// </summary>
		public string RedirectViewPostFix
		{
			get;
			set;
		}

		private bool CheckIsMobileDevice(ControllerContext controllerContext)
		{
			bool isMobileDevice = (bool)(controllerContext.HttpContext.Session[IS_MOBILE_DEVICE_SESSION_KEY] ?? false);

			if (controllerContext.HttpContext.Session.IsNewSession == true)
			{
				HttpRequestBase request = controllerContext.HttpContext.Request;

				isMobileDevice = Convert.ToBoolean(request.Browser["is_wireless_device"]);
				//IsMobile =  request.Browser["is_wireless_device"];
				//IsTablet =  request.Browser["is_tablet"];
				//BrandName = request.Browser["brand_name"];
				//ModelName = request.Browser["model_name"];
				//Useragent = request.UserAgent;

				controllerContext.HttpContext.Session.Add(IS_MOBILE_DEVICE_SESSION_KEY, isMobileDevice);
			}

			//#if DEBUG
			//			isMobileDevice = true;
			//#endif
			return isMobileDevice;
		}

		public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
		{
			bool isMobileDevice = CheckIsMobileDevice(controllerContext);

			if (isMobileDevice == true)
			{
				PartialViewLocationFormats = PartialViewLocationFormats.ReplaceViewLocation(RedirectViewPostFix);
				AreaPartialViewLocationFormats = AreaPartialViewLocationFormats.ReplaceViewLocation(RedirectViewPostFix);
			}

			return base.FindPartialView(controllerContext, partialViewName, useCache);
		}

		public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
		{
			bool isMobileDevice = CheckIsMobileDevice(controllerContext);

			if (isMobileDevice == true)
			{
				ViewLocationFormats = ViewLocationFormats.ReplaceViewLocation(RedirectViewPostFix);
				MasterLocationFormats = MasterLocationFormats.ReplaceViewLocation(RedirectViewPostFix);
				AreaViewLocationFormats = AreaViewLocationFormats.ReplaceViewLocation(RedirectViewPostFix);
				AreaMasterLocationFormats = AreaMasterLocationFormats.ReplaceViewLocation(RedirectViewPostFix);
			}

			return base.FindView(controllerContext, viewName, masterName, useCache);
		}
	}

	internal static class ViewLocationFormatHelper
	{
		/// <summary>
		/// ViewEngine의 기본 View 파일의 경로를 특정 접미사로 대체합니다.
		/// </summary>
		/// <param name="originalViewLocations">원본 경로</param>
		/// <param name="oldLocationPostfix">추가할 접미사</param>
		/// <returns></returns>
		public static string[] ReplaceViewLocation(this string[] originalViewLocations, string oldLocationPostfix = ".Mobile")
		{
			string[] viewLocationFormats = new string[originalViewLocations.Length];
			string newPostFix = string.IsNullOrEmpty(oldLocationPostfix) == true ? string.Empty : oldLocationPostfix;

			for (int i = 0; i < originalViewLocations.Length; i++)
				viewLocationFormats[i] = originalViewLocations[i].Replace("/Views/", String.Format("/Views{0}/", newPostFix));

			return viewLocationFormats;
		}
	}
}