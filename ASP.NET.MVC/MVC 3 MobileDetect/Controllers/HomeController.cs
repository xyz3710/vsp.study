﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc3MobileDetection.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            //Many more capabilities are available to be queried. 
            //See http://51degrees.mobi/Support/Documentation/Foundation/UserGuide.aspx for more info.
            ViewBag.IsMobile = Request.Browser["is_wireless_device"];
            ViewBag.IsTablet = Request.Browser["is_tablet"];
            ViewBag.BrandName = Request.Browser["brand_name"];
            ViewBag.ModelName = Request.Browser["model_name"];
            ViewBag.Useragent = Request.UserAgent;

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
