using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scaffolding.Models;

namespace Scaffolding.Controllers
{   
    public class BooksController : Controller
    {
        private LibraryContainer context = new LibraryContainer();

        //
        // GET: /Books/

        public ViewResult Index()
        {
            return View(context.Book.ToList());
        }

        //
        // GET: /Books/Details/5

        public ViewResult Details(int id)
        {
            Book book = context.Book.Single(x => x.Id == id);
            return View(book);
        }

        //
        // GET: /Books/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Books/Create

        [HttpPost]
        public ActionResult Create(Book book)
        {
            if (ModelState.IsValid)
            {
                context.Book.Add(book);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(book);
        }
        
        //
        // GET: /Books/Edit/5
 
        public ActionResult Edit(int id)
        {
            Book book = context.Book.Single(x => x.Id == id);
            return View(book);
        }

        //
        // POST: /Books/Edit/5

        [HttpPost]
        public ActionResult Edit(Book book)
        {
            if (ModelState.IsValid)
            {
                context.Entry(book).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(book);
        }

        //
        // GET: /Books/Delete/5
 
        public ActionResult Delete(int id)
        {
            Book book = context.Book.Single(x => x.Id == id);
            return View(book);
        }

        //
        // POST: /Books/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = context.Book.Single(x => x.Id == id);
            context.Book.Remove(book);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}