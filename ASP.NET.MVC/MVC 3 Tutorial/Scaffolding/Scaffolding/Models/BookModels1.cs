﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Scaffolding.Models
{
	public class Book
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// ISBN를 구하거나 설정합니다.
		/// </summary>
		[Required, MaxLength(20)]
		public string ISBN
		{
			get;
			set;
		}

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		[Required, MaxLength(100)]
		public string Title
		{
			get;
			set;
		}
	}
	/*

		public class Library : DbContext
		{
			public DbSet<Book> Books
			{
				get;
				set;
			}
		}

		public class LibraryInitializer : DropCreateDatabaseIfModelChanges<Library>
		{
			protected override void Seed(Library context)
			{
				var books = new List<Book>
				{
					new Book { ISBN = "123456789-0", Title = "Best Book in the world." },
					new Book { ISBN = "987654321-0", Title = "Worst  Book in the world." },
				};

				books.ForEach(book => context.Books.Add(book));

				context.SaveChanges();
			}
		}
	*/
}