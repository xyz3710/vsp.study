﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Exam01
{
	public class Person
	{
		#region Constructors
		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person(string name, int age, string province)
		{
			Name = name;
			Age = age;
			Province = province;
		}

		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person()
		{
			
		}
		#endregion
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}

		/// <summary>
		/// Province를 구하거나 설정합니다.
		/// </summary>
		public string Province
		{
			get;
			set;
		}
	}

	public partial class DataBinding : System.Web.UI.Page
	{
		private List<string> _items = new List<string>()
		{
			"개미",
			"곰",
			"고양이",
			"개",
			"코끼리",
			"파리",
			"고릴라",
		};
		private List<Person> _people = new List<Person>()
		{
			new Person("김기원", 10038, "전북 전주"),
			new Person("황태영", 37, "전북 전주"),
			new Person("김영재", 5, "전북 전주"),
			new Person("김수민", 3, "전북 전주"),
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			txtName.Text = "김기원";
			txtAge.Text = "38";
			txtProvince.Text = "전북 전주";

			if (IsPostBack == false)
			{
				RadioButtonList1.DataSource = _items;
				lvPeople.DataSource = _people;
			}

			// # 으로 바인딩된 컨트롤 및 기타 컨트롤을 바인딩 해준다.
			Page.DataBind();
		}
	}
}