﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Exam01
{
	public partial class List01 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			HtmlForm form = Controls.Cast<Control>().Single(c => c is HtmlForm) as HtmlForm;

			// ListControl
			foreach (ListControl listControl in form.Controls.Cast<Control>().Where(c => c is ListControl && !(c is BulletedList)))
			{
				listControl.AutoPostBack = true;
				listControl.SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
			}
		}

		void listControl_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (sender is CheckBoxList)
			{
				CheckBoxList checkBoxList = sender as CheckBoxList;

				string checkedList = checkBoxList.Items.Cast<ListItem>()
					.Where(i => i.Selected == true)
					.Aggregate<ListItem, string>(
						string.Empty,
						(acc, next) =>
						{
							string nextValue = next.Text;

							if (acc == string.Empty)
								return nextValue;

							return string.Format("{0}{1}{2}", acc, ", ", nextValue);
						});

				lblList.Text = string.Format("{0} : {1}", checkBoxList.ID, checkedList);
			}
			else
			{
				ListControl listControl = sender as ListControl;

				lblList.Text = string.Format("{0} : {1}", listControl.GetType(), listControl.SelectedValue);
			}
		}
	}
}