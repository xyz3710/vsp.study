﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Exam01
{
	public partial class PostBack : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("Page load 됨", "PostBack.Page_Load");

			if (IsPostBack == true)
				System.Diagnostics.Debug.WriteLine("IsPostBack is true. !!", "PostBack.Page_Load");
		}
	}
}