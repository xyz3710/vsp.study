﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Exam01
{
	public partial class Exam01 : System.Web.UI.Page
	{
		public string TitleString = "ASP.NET Lesson 1";
		public string BodyString = "Hello ASP.NET !!!";

		protected void Page_Load(object sender, EventArgs e)
		{
			HtmlForm form = Controls.Cast<Control>().Single(c => c is HtmlForm) as HtmlForm;

			// CheckBox and RadioButton
			foreach (CheckBox checkBox in form.Controls.Cast<Control>().Where(c => c is CheckBox))
			{
				checkBox.AutoPostBack = true;
				checkBox.Text = checkBox.ID;
				checkBox.CheckedChanged += new EventHandler(checkBox_CheckedChanged);

				if (checkBox is RadioButton)
					(checkBox as RadioButton).GroupName = "Radio";
			}
		}

		void checkBox_CheckedChanged(object sender, EventArgs e)
		{
			if (sender is RadioButton)
			{
				RadioButton radioButton = sender as RadioButton;
				lblWhatChecked.Text = string.Format("[{0}] clicked that from [{1}] type.", radioButton.ID, radioButton.GetType().ToString());
			}
			else
			{
				HtmlForm form = Controls.Cast<Control>().Single(c => c is HtmlForm) as HtmlForm;
				string checkedIds = form.Controls.Cast<Control>()
					.Where(c => c is CheckBox && !(c is RadioButton) && (c as CheckBox).Checked == true)
					.Cast<CheckBox>()
					.Aggregate<CheckBox, string>(
						string.Empty,
						(acc, next) =>
						{
							if (acc == string.Empty)
								return next.ID;

							return string.Format("{0}{1}{2}", acc, ", ", next.ID);
						});

				CheckBox checkBox = sender as CheckBox;

				lblWhatChecked.Text = string.Format("[{0}] clicked those from [{1}] type.", checkedIds, checkBox.GetType().ToString());
			}
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			lblTime.Text = DateTime.Now.ToString();
		}

		protected void btnGetImage_Click(object sender, EventArgs e)
		{
			ImagePanel.ImageUrl = string.Format("Images/{0}.ico", txtImageUrl.Text);
		}
	}
}