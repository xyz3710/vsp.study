﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List01.aspx.cs" Inherits="Exam01.List01" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<a href="Default.aspx">처음으로</a>
	<form id="form1" runat="server">
		<h2>
			Bulleted list</h2>
		<asp:BulletedList ID="BulletedList1" runat="server" DisplayMode="HyperLink">
			<asp:ListItem Value="http://xyz37.blog.me" Selected="True">김기원</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/gain26">황태영</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius37">김영재</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius26">김수민</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/xyz3710" Enabled="false">김기원2</asp:ListItem>
		</asp:BulletedList>
		<hr />
		<h2>
			List</h2>
		<asp:DropDownList ID="DropDownList1" runat="server">
			<asp:ListItem Value="http://xyz37.blog.me" Selected="True">김기원</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/gain26">황태영</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius37">김영재</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius26">김수민</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/xyz3710" Enabled="false">김기원2</asp:ListItem>
		</asp:DropDownList>
		<br />
		<asp:CheckBoxList ID="CheckBoxList1" runat="server">
			<asp:ListItem Value="http://xyz37.blog.me" Selected="True">김기원</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/gain26">황태영</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius37">김영재</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius26">김수민</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/xyz3710" Enabled="false">김기원2</asp:ListItem>
		</asp:CheckBoxList>
		<br />
		<asp:RadioButtonList ID="RadioButtonList1" runat="server">
			<asp:ListItem Value="http://xyz37.blog.me" Selected="True">김기원</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/gain26">황태영</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius37">김영재</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius26">김수민</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/xyz3710" Enabled="false">김기원2</asp:ListItem>
		</asp:RadioButtonList>
		<br />
		<asp:ListBox ID="ListBox1" runat="server">
			<asp:ListItem Value="http://xyz37.blog.me" Selected="True">김기원</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/gain26">황태영</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius37">김영재</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/genius26">김수민</asp:ListItem>
			<asp:ListItem Value="http://facebook.com/xyz3710" Enabled="false">김기원2</asp:ListItem>
		</asp:ListBox>
		<br />
		<asp:Label ID="lblList" runat="server" Text="ListLabel"></asp:Label>
	</form>
</body>
</html>
