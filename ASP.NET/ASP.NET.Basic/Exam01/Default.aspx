﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Exam01.Exam01" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>
		<% =TitleString %></title>
</head>
<body>
	<form id="form1" runat="server">
		<header>
			<% =TitleString %></header>
		<hgroup>
			<h1>
				<% =TitleString %></h1>
		</hgroup>
		<hr />
		<p>
			<% =BodyString %></p>
		<asp:Label ID="lblTime" runat="server" Text="Label"></asp:Label>
		<hr />
		<asp:Button ID="Button1" runat="server" Text="Click It!!" 
			onclick="Button1_Click" />
			<hr />
		<asp:Image ID="ImagePanel" runat="server" /><br />
		ImageName(1 to 10) : <asp:TextBox ID="txtImageUrl" runat="server"></asp:TextBox> 
		<asp:Button ID="btnGetImage"
			runat="server" Text="Get it" onclick="btnGetImage_Click" />
			<hr />
			<h2>Radio & CheckBox those properties are Page_Load event</h2>
		<table style="width: 100%;border:1px">
			<tr>
				<td>
					<asp:CheckBox ID="CheckBox1" runat="server" />
				</td>
				<td>
					<asp:CheckBox ID="CheckBox2" runat="server" />
				</td>
				<td>
					<asp:CheckBox ID="CheckBox3" runat="server" />
				</td>
			</tr>
			<tr>
				<td>
					<asp:RadioButton ID="RadioButton1" runat="server" />
				</td>
				<td>
					<asp:RadioButton ID="RadioButton2" runat="server" />
				</td>
				<td>
					<asp:RadioButton ID="RadioButton3" runat="server" />
				</td>
			</tr>
		</table>
		<asp:Label ID="lblWhatChecked" runat="server" Text="Label"></asp:Label>
		<hr />
		<a href="List01.aspx">List Control로</a>
		<br />
		<a href="DataBinding.aspx">DataBinding으로</a>
		<br />
		<a href="PostBack.aspx">PostBack으로</a>
		<br />
		<a href="Slave.aspx">Master 하위 폼(slave)으로</a>
	</form>
</body>
</html>
