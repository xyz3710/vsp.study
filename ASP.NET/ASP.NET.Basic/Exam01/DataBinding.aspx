﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataBinding.aspx.cs" Inherits="Exam01.DataBinding" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<a href="Default.aspx">처음으로</a>
	<form id="form1" runat="server">
		<header>Binding Basic</header>
		<fieldset>
			<strong>이름 : </strong>
			<asp:TextBox ID="txtName" runat="server" /><br />
			<strong>나이 : </strong>
			<asp:TextBox ID="txtAge" runat="server" /><br />
			<strong>지역 : </strong>
			<asp:TextBox ID="txtProvince" runat="server" /><br />
			<asp:Button ID="btnBinding" runat="server" Text="Binding" />
		</fieldset>
		<hgroup>
			<h2>
				<asp:Label ID="Label1" runat="server" Text="<%#txtName.Text %>"></asp:Label><br />
				<asp:Label ID="Label2" runat="server" Text="<%#txtAge.Text %>"></asp:Label><br />
				<asp:Label ID="Label3" runat="server" Text="<%#txtProvince.Text %>"></asp:Label><br />
			</h2>
		</hgroup>
		<hr />
		<asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true">
		</asp:RadioButtonList>
		<asp:Label ID="Label4" runat="server" Text="<% #RadioButtonList1.SelectedValue %>"></asp:Label>
		<hr />
		<asp:ListView ID="lvPeople" runat="server">
			<ItemTemplate>
			<%#DataBinder.Eval(Container.DataItem, "Name") %> <br />
			<%#DataBinder.Eval(Container.DataItem, "Age", "{0,-9:N0}") %> <br />
			<%#DataBinder.Eval(Container.DataItem, "Province") %> <br />
			<hr />
			</ItemTemplate>
		</asp:ListView>
	</form>
</body>
</html>
