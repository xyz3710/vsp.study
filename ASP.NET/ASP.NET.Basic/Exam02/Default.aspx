﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Exam02.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			RequiredFieldValidator :
			<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
			<br />
			<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<p style='color:red'>RequiredFieldValidator Error.</p>"
				ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
			<hr />
			RangeValidator(1~10) :
			<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
			<br />
			<asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="<p style='color:red'>RangeValidator Error input from 1 to 10.</p>"
				ControlToValidate="TextBox2" MinimumValue="1" MaximumValue="10"></asp:RangeValidator>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="<p style='color:red'>Please input int value from 1 to 10.</p>"
				ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
			<hr />
			RegularExpressionValidator(email) :
			<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
			<br />
			<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<p style='color:red'>Please valid email.</p>"
				ControlToValidate="TextBox3" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
			<hr />
			CompareValidator :
			<asp:TextBox ID="txtControlToCompare" runat="server"></asp:TextBox>
			<asp:TextBox ID="txtControlToValidate" runat="server"></asp:TextBox>
			<br />
			<asp:CompareValidator ID="RangeValidator2" runat="server" ErrorMessage="<p style='color:red'>Value are not equal.</p>"
				ControlToCompare="txtControlToCompare" ControlToValidate="txtControlToValidate"></asp:CompareValidator>
			<hr />
			<h2>
				회원가입</h2>
			<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Account Validation"
				ValidationGroup="ValidateAccount" />
			이름 :
			<asp:TextBox ID="txtName" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator"
				ControlToValidate="txtName" ValidationGroup="ValidateAccount"></asp:RequiredFieldValidator>
			<br />
			ID :
			<asp:TextBox ID="txtID" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
				ControlToValidate="txtID"></asp:RequiredFieldValidator>
			<br />
			비밀번호 :
			<asp:TextBox ID="txtPw" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator"
				ControlToValidate="txtPw"></asp:RequiredFieldValidator>
			<br />
			비밀번호 확인 :
			<asp:TextBox ID="txtPwConfirm" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="RequiredFieldValidator"
				ControlToValidate="txtPwConfirm"></asp:RequiredFieldValidator>
			<asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator"
				ControlToCompare="txtPw" ControlToValidate="txtPwConfirm"></asp:CompareValidator>
			<br />
			email:
			<asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="RequiredFieldValidator"
				ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
			<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator"
				ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
				ControlToValidate="txtEmail"></asp:RegularExpressionValidator>
			<br />
			<asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
		</div>
	</form>
</body>
</html>
