﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ValidationExample.aspx.cs" Inherits="ValidationExample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script type="text/javascript1.5">
    function validateDate(oSrc, args) {
        var iDay, iMonth, iYear;
        var arrValues;
        arrValues = args.Value.split("/");
        iMonth = arrValues[0];
        iDay = arrValues[1];
        iYear = arrValues[2];

        var testDate = new Date(iYear, iMonth - 1, iDay);
        if ((testDate.getDate() != iDay) ||
      (testDate.getMonth() != iMonth - 1) ||
      (testDate.getFullYear() != iYear)) {
            args.IsValid = false;
            return;
        }

        return true;
       }
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h1>
            Submit a Reservation</h1>
        Email <asp:TextBox ID="textEmail" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="textEmail" Display="Dynamic" 
            ErrorMessage="Email address is required" ValidationGroup="AllValidators">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="textEmail" Display="Dynamic" 
            ErrorMessage="E-mail addresses must be in the format of name@domain.xyz." 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
            ValidationGroup="AllValidators">Invalid format!</asp:RegularExpressionValidator>
        <br />
        Number in Party
        <asp:TextBox ID="textNumberInParty" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="textNumberInParty" 
            ErrorMessage="Please indicate how many people are in your party" 
            ValidationGroup="AllValidators">*</asp:RequiredFieldValidator>
        <asp:RangeValidator ID="RangeValidator1" runat="server" 
            ControlToValidate="textNumberInParty" 
            ErrorMessage="Enter a number between 1 and 20 for the number of people in your party" 
            MaximumValue="20" MinimumValue="1" Type="Integer" 
            ValidationGroup="AllValidators">Enter a number between 1 and 20.</asp:RangeValidator>
        <br />
        Preferred Date
        <asp:TextBox ID="textPreferredDate" runat="server"></asp:TextBox>
        <asp:CustomValidator ID="CustomValidator1" runat="server" 
            ControlToValidate="textPreferredDate" Display="Dynamic" 
            ErrorMessage="Enter a date in the format m/d/yyyy." 
            onservervalidate="CustomValidator1_ServerValidate" 
            ValidationGroup="AllValidators" ClientValidationFunction="validateDate">Invalid date format (requires m/d/yyyy).</asp:CustomValidator>
        <br />
        <asp:CheckBox ID="checkPhoneConfirmation" runat="server" AutoPostBack="True" 
            oncheckedchanged="checkPhoneConfirmation_CheckedChanged" 
            Text="Confirm reservation by telephone" />
        <br />
        Phone Number 
        <asp:TextBox ID="textPhoneNumber" runat="server" Enabled="False"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validatorRequiredPhoneNumber" runat="server" 
            ControlToValidate="textPhoneNumber" Display="Dynamic" 
            ErrorMessage="You must provide a phone number.">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="validatorRegExPhoneNumber" runat="server" 
            ControlToValidate="textPhoneNumber" Display="Dynamic" 
            ErrorMessage="Phone number format is invalid." 
            ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">Invalid format.</asp:RegularExpressionValidator>
        <br />
        <asp:Button ID="buttonSubmit" runat="server" Text="Submit Request" 
            onclick="buttonSubmit_Click" ValidationGroup="AllValidators" />
        <br />
        <asp:Label id="labelMessage" runat="server"></asp:Label>
    
        <br />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            ShowMessageBox="True" ValidationGroup="AllValidators" />
    
        <br />
 
    </div>
    </form>
</body>
</html>
