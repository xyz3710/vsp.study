﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ValidationExample : System.Web.UI.Page
{
	protected System.Web.UI.WebControls.Label labelMessage;
	protected System.Web.UI.WebControls.TextBox textPhoneNumber;
	protected System.Web.UI.WebControls.CheckBox checkPhoneConfirmation;
	protected System.Web.UI.WebControls.RequiredFieldValidator validatorRequiredPhoneNumber;
	protected System.Web.UI.WebControls.RegularExpressionValidator validatorRegExPhoneNumber;

	protected void Page_Load(object sender, EventArgs e)
	{
		labelMessage.Text = "";
	}
	protected void buttonSubmit_Click(object sender, EventArgs e)
	{
		if (Page.IsValid)
		{
			labelMessage.Text = "Your reservation has been processed.";
		}
		else
		{
			labelMessage.Text = "Page is not valid.";
		}

	}

	protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
	{
		try
		{
			DateTime.ParseExact(args.Value, "d",
				System.Globalization.DateTimeFormatInfo.InvariantInfo);
			args.IsValid = true;
		}
		catch
		{
			args.IsValid = false;
		}

	}
	protected void checkPhoneConfirmation_CheckedChanged(object sender, EventArgs e)
	{
		if (checkPhoneConfirmation.Checked)
		{
			textPhoneNumber.Enabled = true;
			validatorRequiredPhoneNumber.ValidationGroup =
                "AllValidators";
			validatorRegExPhoneNumber.ValidationGroup = "AllValidators";
		}
		else
		{
			textPhoneNumber.Enabled = false;
			validatorRequiredPhoneNumber.ValidationGroup = "";
			validatorRegExPhoneNumber.ValidationGroup = "";
		}

	}
}