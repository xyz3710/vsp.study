using System;
using System.Collections.Generic;
using System.Text;

namespace AnonymousMethod04_Generic
{
	class SomeClass1<T>
	{
		delegate void SomeDelegate(T t);

		public void InvokeMethod(T t)
		{
			SomeDelegate del = delegate(T item)
			{
				Console.WriteLine("Datatype : {0}, item : {1}", typeof(T), item);
			};

			del(t);
		}
	}

	class SomeClass2
	{
		delegate void SomeDelegate<T>(T t);

		public void InvokeMethod(int data)
		{
			SomeDelegate<int> del = delegate(int number)
			{
				Console.WriteLine(number.ToString());
			};

			del(data);
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			SomeClass1<string> someClassString = new SomeClass1<string>();

			someClassString.InvokeMethod("Hi all");


			SomeClass1<int> someClassInt = new SomeClass1<int>();

			someClassInt.InvokeMethod(1000);


			SomeClass2 someClass2 = new SomeClass2();

			someClass2.InvokeMethod(999);

			Console.WriteLine(IsInRole("tester"));

			Console.ReadLine();
		}

		static bool IsInRole(string role)
		{
			string[] roles = getRoles();

			Predicate<string> exists = delegate(string roleToMatch)
			{
				return roleToMatch == role;
			};

			return Array.Exists(roles, exists);
		}

		private static string[] getRoles()
		{
			return new string[] { "master", "tester", "developer" };
		}
	}
}
