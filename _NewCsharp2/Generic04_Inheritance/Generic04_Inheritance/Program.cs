﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Generic04_Casting
{

	#region Generic Inheritance
	
	public class BaseClass<T>
	{
		public void Print(T t)
		{
			Console.WriteLine("BaseClass\t{0}", t);
		}
	}

	public class SubClass : BaseClass<int>
	{

	}

	public class SubClassGeneric<K> : BaseClass<K>
	{

	}

	#endregion

	#region Generic New
	public class NewBase<T> where T : new()
	{
		public T GetT()
		{
			return new T();
		}
	}

	public class NewSubClass<T> : NewBase<T> where T : new()
	{

	} 
	#endregion
	
	#region Generic Virtual
	public class VirtualBaseClass<T>
	{
		public virtual T GetT()
		{
			return default(T);
		}
	}

	public class VirtualSubClass : VirtualBaseClass<string>
	{
		public override string GetT()
		{
			if (base.GetT() == null)
				return "Empty string";

			return string.Empty;
		}
	}

	public class VirtualSubClassGeneric<T> : VirtualBaseClass<T>
	{
		public override T GetT()
		{
			return base.GetT();
		}
	} 
	#endregion

	#region Generic Abstract1
	public interface ISomeInterface<T>
	{
		T SomeMethod(T t);
	}

	public abstract class AbstractBaseClass<T>
	{
		public abstract T SomeMethod(T t);
	}
  
	public class AbstractSubClass<T> : AbstractBaseClass<T>
	{
		public override T SomeMethod(T t)
		{
			return default(T);
		}
	}
	#endregion

	#region Generic Abstract2
	public abstract class BaseCalculator<T>
	{
		public abstract T Add(T arg1, T arg2);
		public abstract T Subtract(T arg1, T arg2);
		public abstract T Divide(T arg1, T arg2);
		public abstract T Multiply(T arg1, T arg2);
	}

	public class MyCalculator : BaseCalculator<int>
	{
		public override int Add(int arg1, int arg2)
		{
			return arg1 + arg2;
		}

		public override int Subtract(int arg1, int arg2)
		{
			return arg1 - arg2;
		}

		public override int Divide(int arg1, int arg2)
		{
			return arg1 / arg2;
		}

		public override int Multiply(int arg1, int arg2)
		{
			return arg1 * arg2;
		}
	}  
	#endregion

	#region Generic Interface
	public interface ICalculator<T>
	{
		T Add(T arg1, T arg2);
		T Subtract(T arg1, T arg2);
		T Divide(T arg1, T arg2);
		T Multiply(T arg1, T arg2);
	}

	public class MyICalculator : ICalculator<int>
	{
		#region ICalculator<int> Members

		public int Add(int arg1, int arg2)
		{
			return arg1 + arg2;
		}

		public int Subtract(int arg1, int arg2)
		{
			return arg1 - arg2;
		}

		public int Divide(int arg1, int arg2)
		{
			return arg1 / arg2;
		}

		public int Multiply(int arg1, int arg2)
		{
			return arg1 * arg2;
		}

		#endregion
	}

	public class MyICalculator2<T> : ICalculator<T>
	{

		#region ICalculator<T> Members

		public T Add(T arg1, T arg2)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public T Subtract(T arg1, T arg2)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public T Divide(T arg1, T arg2)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public T Multiply(T arg1, T arg2)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
  
	#endregion


	class Program
	{
		static void Main(string[] args)
		{
			SubClass subClass = new SubClass();

			subClass.Print(10);

			
			SubClassGeneric<string> subClassGeneric = new SubClassGeneric<string>();

			subClassGeneric.Print("string");


			NewSubClass<ArrayList> newSubClass = new NewSubClass<ArrayList>();

			Console.WriteLine(newSubClass.GetT().ToString());


			VirtualSubClass virtualSubClass = new VirtualSubClass();

			Console.WriteLine(virtualSubClass.GetT().ToString());


			VirtualSubClassGeneric<string> virtualSubClassGeneric = new VirtualSubClassGeneric<string>();

			Console.WriteLine(virtualSubClassGeneric.GetT());


			Console.ReadLine();
		}
	}
  
}
