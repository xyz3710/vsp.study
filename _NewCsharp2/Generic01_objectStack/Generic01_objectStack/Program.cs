﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGeneric01_objectStack
{
	public class Stack
	{
		private readonly int mSize;
		private int mStackPointer = 0;
		private object[] mItem;

		public Stack() : this(100)
		{
		}
	
		public Stack(int size)
		{
			mSize = size;
			mItem = new object[mSize];
		}

		public void Push(object item)
		{
			if (mStackPointer >= mSize)
				throw new StackOverflowException();

			mItem[mStackPointer++] = item;
		}

		public object Pop()
		{
			if (mStackPointer-- >= 0)
			{
				return mItem[mStackPointer];
			}
			else
			{
				mStackPointer = 0;
				
				throw new InvalidOperationException("Cannot pop an empty stack");
			}
		}
	}
  
	class Program
	{
		static void Main(string[] args)
		{
			Stack stack = new Stack();

			stack.Push("1");

			string value = (string)stack.Pop();

			Console.WriteLine(value);

			Console.ReadLine();			
		}
	}
}
