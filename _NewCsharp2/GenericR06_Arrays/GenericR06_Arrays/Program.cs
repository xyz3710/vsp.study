﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR06_Arrays
{
	class Program
	{
		static void Main(string[] args)
		{
			int[] arr = { 0, 1, 2, 3, 4 };
			List<int> list = new List<int>();

			for (int x = 5; x < 10; x++)
			{
				list.Add(x);
			}

			ProcessItems<int>(arr);
			ProcessItems<int>(list);

			Console.ReadLine();
		}

		private static void ProcessItems<T>(IList<T> coll)
		{
			foreach (T item in coll)
			{
				Console.Write(item.ToString() + " ");
			}

			Console.WriteLine();
		}
	}
}
