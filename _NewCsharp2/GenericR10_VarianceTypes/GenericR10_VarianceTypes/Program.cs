﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace GenericR10_VarianceTypes
{
	static class VarianceWorkaround
	{
		// Simple workaround for single method
		// Variance in one directon only
		public static void Add<S, D>(List<S> source, List<D> destination)
			where S : D
		{
			foreach (S sourceElement in source)
			{
				destination.Add(sourceElement);
			}
		}

		// Workaround for interface
		// Variance in one direction only so type expressions are natural
		public static IEnumerable<D> Convert<S, D>(IEnumerable<S> source)
			where S : D
		{
			return new EnumerableWrapper<S, D>(source);
		}

		private class EnumerableWrapper<S, D> : IEnumerable<D>
			where S : D
		{
			private IEnumerable<S> source;

			#region Constructor

			/// <summary>
			/// Constructor
			/// </summary>
			public EnumerableWrapper(IEnumerable<S> source)
			{
				this.source = source;
			}

			#endregion
		
			#region IEnumerable<D> Members

			public IEnumerator<D> GetEnumerator()
			{
				return new EnumeratorWrapper(this.source.GetEnumerator());
			}

			#endregion

			#region IEnumerable Members

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			#endregion

			private class EnumeratorWrapper : IEnumerator<D>
			{
				private IEnumerator<S> source;

				#region Constructor

				/// <summary>
				/// Constructor
				/// </summary>
				public EnumeratorWrapper(IEnumerator<S> source)
				{
					this.source = source;
				}

				#endregion
		
				#region IEnumerator<D> Members

				public D Current
				{
					get
					{
						return this.source.Current;
					}
				}

				#endregion

				#region IDisposable Members

				public void Dispose()
				{
					this.source.Dispose();
				}

				#endregion

				#region IEnumerator Members

				object IEnumerator.Current
				{
					get
					{
						return this.source.Current;
					}
				}

				public bool MoveNext()
				{
					return this.source.MoveNext();
				}

				public void Reset()
				{
					this.source.Reset();
				}

				#endregion
			}

			// Workaround for interface
			// Variance in both directons, causes issues
			// similar to existing array variance
			public static ICollection<D> Convert<S, D>(ICollection<S> source)
				where S : D
			{
				return new CollectionWrapper<S, D>(source);
			}

			private class CollectionWrapper<S, D> : EnumerableWrapper<S, D>, ICollection<D>
				where S : D
			{
				private ICollection<S> source;

				#region Constructor

				/// <summary>
				/// Constructor
				/// </summary>
				public CollectionWrapper(ICollection<S> source) : base(source)
				{
				}

				#endregion
		
				#region ICollection<D> Members

				// Variance going the wrong way ...
				// ... can yield exceptions at runtime
				public void Add(D item)
				{
					if (item is S)
					{
						this.source.Add((S)item);
					}
					else
					{
						throw new Exception(@"Type mismatch exceptino, due to type hole intruduced by variance");
					}
				}

				public void Clear()
				{
					this.source.Clear();
				}

				// variance going the wrong way ...
				// ... but the semantics of the method yields reasonalbe semantics
				public bool Contains(D item)
				{
					if (item is S)
					{
						return this.source.Contains((S)item);
					}
					else
					{
						return false;
					}
				}

				// variance going the right way...
				public void CopyTo(D[] array, int arrayIndex)
				{
					foreach (S src in this.source)
					{
						array[arrayIndex++] = src;
					}
				}

				public int Count
				{
					get
					{
						return this.source.Count;
					}
				}

				public bool IsReadOnly
				{
					get
					{
						return this.source.IsReadOnly;
					}
				}

				// VarianceWorkaround going the wrong way ...
				// ... but the semantics of the method yields reasonable semantics
				public bool Remove(D item)
				{
					if (item is S)
					{
						return this.source.Remove((S)item);
					}
					else
					{
						return false;
					}
				}

				#endregion
			}

			public static IList<D> Convert<S, D>(IList<S> source)
				where S : D
			{
				return new ListWrapper<S, D>(source);
			}

			// Workaround for interface
			// Variance in both dirctions, causes issues similar to existing array variance
			private class ListWrapper<S, D> : CollectionWrapper<S, D>, IList<D>
				where S : D
			{
				private IList<S> source;

				#region Constructor

				/// <summary>
				/// Constructor
				/// </summary>
				public ListWrapper(IList<S> source) : base(source)
				{
					this.source = source;
				}

				#endregion
		
				#region IList<D> Members

				public int IndexOf(D item)
				{
					if (item is S)
					{
						return this.source.IndexOf((S)item);
					}
					else
					{
						return -1;
					}
				}

				// variance the wrong way...
				// ... can throw exceptions at runtime
				public void Insert(int index, D item)
				{
					if (item is S)
					{
						this.source.Insert(index, (S)item);
					}
					else
					{
						throw new Exception("Invalid type exception");
					}
				}

				public void RemoveAt(int index)
				{
					this.source.RemoveAt(index);
				}

				public D this[int index]
				{
					get
					{
						return this.source[index];
					}
					set
					{
						if (value is S)
						{
							this.source[index] = (S)value;
						}
						else
						{
							throw new Exception("Invalid type exception.");
						}
					}
				}

				#endregion
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			List<int> ints = new List<int>();

			ints.Add(1);
			ints.Add(10);
			ints.Add(42);

			List<object> objects = new List<object>();

			// doesn't compile ints is not a IEnumerable<object>
			//objects.AddRange(ints);

			// does compile
			VarianceWorkaround.Add<int, object>(ints, objects);

			// would like to do this, but can't ...
			// ... ints is not an IEnumerable<object>
			//PrintObjects(ints);

			PrintObjects(VarianceWorkaround.Convert<int, object>(ints));

			// this work fine
			AddToObjects(objects);
			//AddToObjects(VarianceWorkaround.Convert<int, object>(ints));

			Console.ReadLine();
		}

		private static void AddToObjects(IList<object> objects)
		{
			// this will fail if the collection provided is a wrapped collection
			objects.Add(new object());
		}

		private static void PrintObjects(IEnumerable<object> objects)
		{
			foreach (object o in objects)
			{
				Console.WriteLine(o);
			}
		}

		private static void ArraryExample()
		{
			object[] objects = new string[10];

			// no problem, adding a string to a string[]
			objects[0] = "hello";

			// runtime exception, adding an object to a string[]
			objects[1] = new object();
		}
	}
}
