﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR01_Introduction
{
	using GenList=GenericList<int>;

	public class GenericList<T>
	{
		private class Node
		{
			private T data;
			private Node next;

			#region Constructor

			/// <summary>
			/// Constructor
			/// </summary>
			public Node(T t)
			{
				next = null;
				data = t;
			}
			#endregion	

			public Node Next
			{
				get
				{
					return next;
				}
				set
				{
					next = value;
				}
			}

			public T Data
			{
				get
				{
					return data;
				}
				set
				{
					data = value;
				}
			}	
		}

		private Node head;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public GenericList()
		{
			head = null;
		}
		#endregion	

		public void AddHead(T t)
		{
			Node n = new Node(t);

			//n.Next = head;
			//head = n;

			head.Next = n;
		}

		public IEnumerator<T> GetEnumerator()
		{
			Node current = head;

			while (current != null)
			{
				yield return current.Data;

				current = current.Next;
			}
	
		}
	}  
  
	class Program
	{
		static void Main(string[] args)
		{
			GenList list = new GenList();

			for (int i = 0; i < 10; i++)
			{
				list.AddHead(i);
			}

			foreach (int x in list)
			{
				Console.Write(x + " ");
			}

			Console.WriteLine("\nDone.");

			Console.ReadLine();
		}
	}
}
