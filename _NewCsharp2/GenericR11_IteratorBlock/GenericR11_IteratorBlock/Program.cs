﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR11_IteratorBlock
{
	public class Stack<T> : IEnumerable<T>
	{
		private T[] values = new T[100];
		private int top = 0;

		/// <summary>
		/// Pushes the specified t.
		/// </summary>
		/// <param name="t">The t.</param>
		public void Push(T t)
		{
			values[top++] = t;
		}

		/// <summary>
		/// Pops this instance.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<T> Pop()
		{
			yield return values[--top];
		}

		public IEnumerable<T> TopToBottom
		{
			get
			{
				// Since we implement IEnumerable<T>
				// and the default iteration is top to botton,
				// just return the object
				return this;
			}
		}

		// Iterate from bottom to top.
		public IEnumerable<T> BottomToTop
		{
			get
			{
				for (int i = 0; i < top; i++)
				{
					yield return values[i];
				}
			
			}
		}

		// A parameterized iterator that return n items from the top.
		public IEnumerable<T> TopN(int n)
		{
			// in this example we return less than N if necessary
			int j = (n >= top) ? 0 : top - n;

			for (int i = top; --i >= j; )
			{
				yield return values[i];
			}			
		}

		#region IEnumerable<T> Members

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = top; --i >= 0;)
			{
				yield return values[i];
			}			
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion
	}

	class Program
	{
		static void Main(string[] args)
		{
			Stack<int> s = new Stack<int>();

			for (int i = 0; i < 10; i++)
			{
				s.Push(i);
			}
			
			// Prints : 9 8 7 6 5 4 3 2 1 0
			foreach (int n in s)
			{
				Console.Write("{0} ", n);
			}
			Console.WriteLine();

			// Prints : 9 8 7 6 5 4 3 2 1 0
			// Foreach legal since s.TopToBottom returns IEnumerable<int>
			foreach (int n in s.TopToBottom)
			{
				Console.Write("{0} ", n);
			}
			Console.WriteLine();

			// Prints : 0 1 2 3 4 5 6 7 8 9
			// Foreach legal since s.BottomToTop returns IEnumerable<int>
			foreach (int n in s.BottomToTop)
			{
				Console.Write("{0} ", n);
			}
			Console.WriteLine();

			// Prints : 9 8 7 6 5 4 3
			// Foreach legal since s.TopN returns IEnumerable<int>
			foreach (int n in s.TopN(7))
			{
				Console.Write("{0} ", n);
			}
			Console.WriteLine();

			foreach (int n in s.Pop())
			{
				Console.Write("{0} ", n);
			}
			Console.WriteLine();

			Console.ReadLine();
		}
	}
}
