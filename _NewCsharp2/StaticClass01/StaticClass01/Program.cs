﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StaticClass01
{
	static class StaticType
	{
		public static void Foo()
		{
			Console.WriteLine("Static method foo...");
		}

		// 다음과 같은 Instance 생성자 선언은 컴파일 오류다
		//public StaticType()
		//{
		//}

		// 역시 Instance method 선언 역시 컴파일 오류다
		//public void InstanceMethod()
		//{
		//}

		// 반면 static constructor는 사용할 수 있다.
		static StaticType()
		{
			Console.WriteLine("Static constructor is valid...");
		}
	}
  
	class Program
	{
		static void Main(string[] args)
		{
			StaticType.Foo();

			Console.ReadLine();
		}
	}
}
