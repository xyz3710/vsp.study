using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AnonymousMethod02
{
	/// <summary>
	/// Use normal method call
	/// </summary>
	class SomeClass1
	{
		delegate void SomeDelegate();

		public void InvokeMethod()
		{
			SomeDelegate del = new SomeDelegate(SomeMethod);

			del();
			del();
		}

		void SomeMethod()
		{
			Console.WriteLine("Hello1");
		}
	}

	/// <summary>
	/// Use anonymous method
	/// </summary>
	class SomeClass2
	{
		delegate void SomeDelegate();

		public void InvokeMethod()
		{
			SomeDelegate del = delegate()
			{
				Console.WriteLine("Hello2");
			};

			del();
			del();
		}
	}

	class MyClass
	{
		public void LaunchThread()
		{
			// normal Thread call
			Thread workerThread1 = new Thread(new ThreadStart(print));

			workerThread1.Start();

			
			// anonymous Thread call
			Thread workerThread2 = new Thread(delegate()
			{
				Console.WriteLine("Hello in thread2");
			});

			workerThread2.Start();
		}

		private void print()
		{
			Console.WriteLine("Hello in thread1");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			SomeClass1 normalMethod = new SomeClass1();

			normalMethod.InvokeMethod();


			SomeClass2 anonymousMethod = new SomeClass2();

			anonymousMethod.InvokeMethod();


			MyClass myClass = new MyClass();

			myClass.LaunchThread();


			Console.ReadLine();
		}
	}
}
