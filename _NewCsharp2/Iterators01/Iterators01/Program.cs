﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestIterators
{
	public class EvenList
	{
		public static IEnumerable EvenNumber(int number, int exponent)
		{
			int current = number;
			while (current <= exponent)
			{
				if (current % 2 == 0)
					yield return current;
				current++;
			}
		}

		public static IEnumerable OddNumber(int number, int exponent)
		{
			int current = number;

			while (current <= exponent)
			{
				if (current % 2 == 1)
					yield return current;

				current++;
			}
		}

		static void Main()
		{
			// 1부터 10까지의 홀수만 출력한다.
			foreach (int i in OddNumber(1, 10))
				Console.Write("{0} ", i);

			Console.WriteLine();

			// 1부터 10까지의 짝수만 출력한다.
			foreach (int i in EvenNumber(1, 10))
				Console.Write("{0} ", i);

			Console.WriteLine();


			List<int> list = new List<int>(10);

			for (int i = 0; i < 10; i++)
			{
				list.Add(i);
			}

			foreach (int i in list)
			{
				Console.Write("{0} ", i);
			}
			Console.WriteLine("\nAfter Add");

			Console.WriteLine("Before Remove");
			foreach (int i in list)
			{
				Console.Write("{0} ", list.Remove());
			}
			Console.WriteLine("\nAfter Remove");

			foreach (int i in list)
			{
				Console.Write("{0} ", i);
			}

			Console.Read();
		}
	}

	class List<T> : IEnumerable<T>
	{
		private T[] elements;
		private int count;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public List(int index)
		{
			this.count = 0;
			this.elements = new T[index];
		}

		#endregion

		/// <summary>
		/// Gets or sets the count.
		/// </summary>
		/// <value>The count.</value>
		public int Count
		{
			get
			{
				return count;
			}
			set
			{
				count = value;
			}
		}

		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		public void Add(T item)
		{
			if (elements.Length > Count)
				this.elements[Count++] = item;
			else
				throw new IndexOutOfRangeException("Index out of range.");
		}

		/// <summary>
		/// Removes this instance.
		/// </summary>
		/// <returns></returns>
		public T Remove()
		{
			--count;

			if (Count >= 0)
			{
				T ret = elements[count];

				elements[count] = default(T);

				return ret;
			}
			else
				return default(T);
		}

		#region IEnumerable<T> Members

		public IEnumerator<T> GetEnumerator()
		{
			foreach (T element in elements)
			{
				yield return element;
			}
		}

		#endregion

		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion
	}
}
