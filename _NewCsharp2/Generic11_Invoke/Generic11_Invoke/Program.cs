﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Generic11_Invoke
{
	class Program
	{
		static void Main(string[] args)
		{
			LinkedList<int, string> list = new LinkedList<int, string>();

			Type type = list.GetType();

			Console.WriteLine(list.ToString());

			MethodInfo methodInfo = type.GetMethod("AddHead");

			list.AddHead(0, "Zero");
			Console.WriteLine("\t\tprint 1");
			list.Print();

			object[] argMember = {
				new object[]{1, "One"},
				new object[]{2, "Two"},
				new object[]{3, "Three"}
			};

			foreach (object[] o in argMember)
			{
				methodInfo.Invoke(list, o);
			}

			Console.WriteLine("\t\tprint 2");
			list.Print();

			Console.ReadLine();
		}
	}
}
