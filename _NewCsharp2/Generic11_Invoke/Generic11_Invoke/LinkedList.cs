using System;
using System.Collections.Generic;
using System.Text;

namespace Generic11_Invoke
{
	public class Node<K, T>
	{
		private K key;
		private T item;
		private Node<K, T> next;

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Node&lt;K, T&gt;"/> class.
		/// </summary>
		public Node()
		{
			key = default(K);
			item = default(T);
			this.next = null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Node&lt;K, T&gt;"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="item">The item.</param>
		/// <param name="next">The next.</param>
		public Node(K key, T item, Node<K, T> next)
		{
			this.key = key;
			this.item = item;
			this.next = next;
		}

		#endregion

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		public K Key
		{
			get
			{
				return key;
			}
			set
			{
				key = value;
			}
		}

		/// <summary>
		/// Gets or sets the item.
		/// </summary>
		/// <value>The item.</value>
		public T Item
		{
			get
			{
				return item;
			}
			set
			{
				item = value;
			}
		}

		/// <summary>
		/// Gets or sets the next.
		/// </summary>
		/// <value>The next.</value>
		public Node<K, T> Next
		{
			get
			{
				return next;
			}
			set
			{
				next = value;
			}
		}		
	}

	public class LinkedList<T, U>
	{
		private Node<T, U> head;

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="T:LinkedList&lt;T, U&gt;"/> class.
		/// </summary>
		public LinkedList()
		{
			this.head = new Node<T,U>();
		}

		#endregion

		/// <summary>
		/// Gets the head.
		/// </summary>
		/// <value>The head.</value>
		public Node<T, U> Head
		{
			get
			{
				return this.head;
			}
		}

		/// <summary>
		/// Adds the head.
		/// </summary>
		/// <param name="node">The node.</param>
		public void AddHead(T key, U item)
		{
			this.head.Next = new Node<T,U>(key, item, this.head.Next);
		}

		/// <summary>
		/// Prints this instance.
		/// </summary>
		public void Print()
		{
			Node<T, U> current = head;
			
			while (current.Next != null)
			{
				current = current.Next;

				Console.WriteLine("Key\t: {0}, Item\t: {1}",
					current.Key, current.Item);
			}	
		}
	}
}
