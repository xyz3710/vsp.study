﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR07_Delegates
{
	public class Stack<T>
	{
		T[] items;
		int index;

		public delegate void StackDelegate(T[] items);
	}
  
	class Program
	{
		public delegate void Del<T>(T item);

		public static void Notify(int i)
		{
			Console.WriteLine(i);
		}

		static void Main(string[] args)
		{
			Del<int> m1 = new Del<int>(Notify);

			m1(10);


			Del<int> m2 = Notify;

			m2(20);


			Console.ReadLine();
		}
	}
}
