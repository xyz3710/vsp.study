using System;
using System.Collections.Generic;
using System.Text;

namespace InlineWarning
{
	// Disable 'field never used' warning
#pragma warning disable 168
	class Program
	{
		static void Main(string[] args)
		{
			int mNumber;

			Console.ReadLine();
		}
	}
#pragma warning restore 168
}
