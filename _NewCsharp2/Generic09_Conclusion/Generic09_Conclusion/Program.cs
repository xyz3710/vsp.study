﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Generic09_Conclusion
{
	#region Using the generic methods of System.Array

	public class GenericArray
	{
		public static void GetPrime()
		{
			int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
			Action<int> trace = delegate(int number)
			{
				Console.Write(number + " ");
			};

			Predicate<int> isPrime = delegate(int number)
			{
				switch (number)
				{
					case 1:
					case 2:
					case 3:
					case 5:
					case 7:
					case 11:
					case 13:
					case 17:
					case 19:
						return true;

					default:
						return false;
				}
			};

			Console.WriteLine("\nOriginal Numbers");
			Array.ForEach(numbers, trace);
			int[] primes = Array.FindAll(numbers, isPrime);

			Console.WriteLine("\nPrime Numbers");
			Array.ForEach(primes, trace);
		}
	}

	#endregion
	
	class Program
	{
		static void Main(string[] args)
		{
			#region Using the generic methods of System.Array

			GenericArray.GetPrime(); 

			#endregion

			#region Collection

			Console.WriteLine("\nCollection");
			
			Queue<int> queue = new Queue<int>();

			for (int i = 0; i < 5; i++)
			{
				queue.Enqueue(i); 
			}

			Action<int> trace = delegate(int number)
			{
				Console.Write(number + " ");
			};

			Array.ForEach(queue.ToArray(), trace);

			#endregion
	

			Console.ReadLine();
		}
	}
}
