﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic06_Delegates
{
	public delegate void GenericOutsideDelegate<K>(K k);

	public class MyClass<T>
	{
		public delegate void GenericDelegate1(T t);

		public delegate void GenericDelegate2<X>(T t, X x);

		public void Del1Method(T t)
		{
			Console.WriteLine("Del1Method : {0}", t);
		}

		public void Del2Method<Y>(T t, Y y)
		{
			Console.WriteLine("Del2Method : {0}, {1}", t, y);
		}
	}
  

	public delegate void GenericEventHandler<S, A>(S sender, A args);

	public class MyPublisher
	{
		public event EventHandler<EventArgs> GenericEvent;
		public event GenericEventHandler<MyPublisher, EventArgs> MyEvent;

		public void FireGenericEvent()
		{
			GenericEvent(this, EventArgs.Empty);
		}

		public void FireEvent()
		{
			MyEvent(this, EventArgs.Empty);
		}
	}

	public class MySubscriber<A>		// Optional : can be a specific type
	{
		public void SomeMethod(MyPublisher sender, A args)
		{
			Console.WriteLine("MySubscriber's SomeMethod");
		}
	}
  

	class Program
	{
		static void Main(string[] args)
		{
			MyClass<int> obj1 = new MyClass<int>();
			MyClass<int>.GenericDelegate1 del1;

			MyClass<string> obj2 = new MyClass<string>();
			MyClass<string>.GenericDelegate2<int> del2;

			del1 = new MyClass<int>.GenericDelegate1(obj1.Del1Method);
			del2 = new MyClass<string>.GenericDelegate2<int>(obj2.Del2Method);

			del1(100);
			del2("Test string", 999);

			GenericOutsideDelegate<int> ousideDelegate;

			ousideDelegate = new GenericOutsideDelegate<int>(obj1.Del1Method);

			ousideDelegate(133);


			MyPublisher publisher = new MyPublisher();
			MySubscriber<EventArgs> subscriber = new MySubscriber<EventArgs>();
			publisher.MyEvent += subscriber.SomeMethod;
			publisher.FireEvent();

			publisher.GenericEvent += new EventHandler<EventArgs>(publisher_GenericEvent);
			publisher.FireGenericEvent();
			
			Console.ReadLine();
		}

		static void publisher_GenericEvent(object sender, EventArgs e)
		{
			Console.WriteLine("Generic Event");
		}
	}
}
