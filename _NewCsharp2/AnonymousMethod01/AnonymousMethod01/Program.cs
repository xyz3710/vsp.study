﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnonymousMethod01
{
	delegate void SomeDelegate1();
	delegate void SomeDelegate2(string arg);
	delegate void NormalDelegate(string title);
	
	class Program
	{
		static void Main(string[] args)
		{
			// 익명 메소드를 통해 delegate 설정
			SomeDelegate1 dele1 = delegate()
			{
				Console.WriteLine("Hello Delegate world");
			};

			SomeDelegate2 dele2 = delegate(string msg)
			{
				Console.WriteLine(msg);
			};

			NormalDelegate normalDelegate = new NormalDelegate(show);

			normalDelegate("Normal Delegate");


			dele1();
			dele2("Hello anonymous method...");
		}

		private static void show(string p)
		{
			Console.WriteLine(p);
		}
	}
}
