﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR05_Methods
{
	public class Sample<T>
	{
		public void Swap(ref T lhs, ref T rhs)
		{
			T temp;

			temp = lhs;
			lhs = rhs;
			rhs = temp;
		}

		// CS0693 warning
		// 'T' has the same name as the type parameter from outer type 'T'
		public void Test1<T>()
		{
		}

		// no waring
		public void Test2<U>()
		{
		}
	}

	class Program
	{
		static void Swap<T>(ref T lhs, ref T rhs)
		{
			T temp;

			temp = lhs;
			lhs = rhs;
			rhs = temp;
		}

		static void SwapIfGreater<T>(ref T lhs, ref T rhs)
			where T : IComparable<T>
		{
			T temp;

			if (lhs.CompareTo(rhs) > 0)
			{
				temp = lhs;
				lhs = rhs;
				rhs = temp;
			}
		}

		static void Main(string[] args)
		{
			int a = 1;
			int b = 2;

			Swap<int>(ref a, ref b);

			Console.WriteLine(a + ", " + b);

			Swap(ref a, ref b);

			Console.WriteLine(a + ", " + b);

			SwapIfGreater(ref a, ref b);

			Console.WriteLine(a + ", " + b);

			Console.ReadLine();
		}
	}
}
