﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR02_Constraint01
{
	public class Employee
	{
		private string name;
		private int id;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public Employee(string name, int id)
		{
			this.name = name;
			this.id = id;
		}

		#endregion
		
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}

		public int ID
		{
			get
			{
				return id;
			}
			set
			{
				id = value;
			}
		}
	}

	public class GenericList<T> where T : Employee
	{
		private class Node
		{
			private Node next;
			private T data;

			#region Constructor

			/// <summary>
			/// Constructor
			/// </summary>
			public Node(T t)
			{
				next = null;
				data = t;
			}

			#endregion		

			public Node Next
			{
				get
				{
					return next;
				}
				set
				{
					next = value;
				}
			}

			public T Data
			{
				get
				{
					return data;
				}
				set
				{
					data = value;
				}
			}

		}

		private Node head;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public GenericList()
		{
			head = null;
		}

		#endregion

		public void AddHead(T t)
		{
			Node n = new Node(t);

			n.Next = head;

			head = n;			
		}

		public IEnumerator<T> GetEnumerator()
		{
			Node current = head;

			while (current != null)
			{
				yield return current.Data;

				current = current.Next;
			}			
		}

		public T FindFirstOccurence(string name)
		{
			Node current = head;
			T t = default(T);

			while (current != null)
			{
				// The constraint enables access to the Name property.
				if (current.Data.Name == name)
				{
					t = current.Data;

					break;
				}
				else
				{
					current = current.Next;
				}
			}

			return t;
		}
	}
  
  
	class Program
	{
		static void Main(string[] args)
		{
			GenericList<Employee> list = new GenericList<Employee>();

			Employee emplyee1 = new Employee("김기원", 33);
			Employee emplyee2 = new Employee("황태영", 32);
			Employee emplyee3 = new Employee("김은미", 28);

			list.AddHead(emplyee1);
			list.AddHead(emplyee2);
			list.AddHead(emplyee3);

			foreach (Employee e in list)
			{
				Console.WriteLine("ID : {0}\tName : {1}", e.ID, e.Name);
			}

			string name = "김은미";

			Console.WriteLine("Find {0}'s ID => {1}", name, list.FindFirstOccurence(name).ID);
			Console.WriteLine("Done");

			Console.ReadLine();
		}
	}
}
