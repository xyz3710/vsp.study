﻿//Copyright (C) Microsoft Corporation. All rights reserved.

using System;
using System.Collections.Generic;
using System.Text;

namespace AnonymousDelegate_Sample
{

    // 대리자 메서드를 정의합니다.
    delegate decimal CalculateBonus(decimal sales);

    // Employee 형식을 정의합니다.
    class Employee
    {
        public string name;
        public decimal sales;
        public decimal bonus;
        public CalculateBonus calculation_algorithm;
    }

    class Program
    {

        // 이 클래스는 계산을 수행하는 대리자 두 개를 정의합니다.
        // 첫 번째 대리자는 명명된 메서드이고, 두 번째 대리자는 익명의 대리자입니다.

        // 이 메서드는 명명된 메서드입니다.
        // 이 메서드는 가능한 보너스 계산 알고리즘 구현을 정의합니다.

        static decimal CalculateStandardBonus(decimal sales)
        {
            return sales / 10;
        }

        static void Main(string[] args)
        {

            // 보너스 계산에 사용되는 값입니다.
            // 참고: 이 지역 변수는 "캡처된 외부 변수"가 됩니다.
            decimal multiplier = 2;

            // 이 대리자는 명명된 메서드로 정의됩니다.
            CalculateBonus standard_bonus = new CalculateBonus(CalculateStandardBonus);

            // 이 대리자는 익명이므로 명명된 메서드가 없습니다.
            // 이 대리자는 다른 보너스 계산 알고리즘을 정의합니다.
            CalculateBonus enhanced_bonus = delegate(decimal sales) { return multiplier * sales / 10; };

            // Employee 개체를 몇 개 선언합니다.
            Employee[] staff = new Employee[5];

            // Employee 배열을 채웁니다.
            for (int i = 0; i < 5; i++)
                staff[i] = new Employee();

            // Employee에 초기 값을 할당합니다.
            staff[0].name = "Mr Apple";
            staff[0].sales = 100;
            staff[0].calculation_algorithm = standard_bonus;

            staff[1].name = "Ms Banana";
            staff[1].sales = 200;
            staff[1].calculation_algorithm = standard_bonus;

            staff[2].name = "Mr Cherry";
            staff[2].sales = 300;
            staff[2].calculation_algorithm = standard_bonus;

            staff[3].name = "Mr Date";
            staff[3].sales = 100;
            staff[3].calculation_algorithm = enhanced_bonus;

            staff[4].name = "Ms Elderberry";
            staff[4].sales = 250;
            staff[4].calculation_algorithm = enhanced_bonus;

            // 모든 Employee의 보너스를 계산합니다.
            foreach (Employee person in staff)
                PerformBonusCalculation(person);

            // 모든 Employee의 세부 정보를 표시합니다.
            foreach (Employee person in staff)
                DisplayPersonDetails(person);


        }

        public static void PerformBonusCalculation(Employee person)
        {

            // 이 메서드는 person 개체에 저장된 대리자를 사용하여
            // 계산을 수행합니다.
            // 참고: 이 메서드는 multiplier 지역 변수가 이 메서드 범위 밖에 있지만
            // 이 변수를 인식합니다.
            // multipler 변수는 "캡처된 외부 변수"입니다.
            person.bonus = person.calculation_algorithm(person.sales);
        }

        public static void DisplayPersonDetails(Employee person)
        {
            Console.WriteLine(person.name);
            Console.WriteLine(person.bonus);
            Console.WriteLine("---------------");
        }
    }
}


