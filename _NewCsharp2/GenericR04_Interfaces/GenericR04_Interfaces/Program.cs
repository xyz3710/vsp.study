﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR04_Interfaces
{
	public class GenericList<T> : IEnumerable<T>
	{
		// Nested class is also generic on T
		protected class Node
		{
			private Node next;
			private T data;

			#region Constructor

			/// <summary>
			/// Constructor
			/// </summary>
			public Node(T t)
			{
				next = null;
				data = t;
			}

			#endregion
		
			public Node Next
			{
				get
				{
					return next;
				}
				set
				{
					next = value;
				}
			}

			public T Data
			{
				get
				{
					return data;
				}
				set
				{
					data = value;
				}
			}

		}

		protected Node head;
		protected Node current;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public GenericList()
		{
			head = null;
			current = null;
		}

		#endregion

		public void AddHead(T t)	// T as method parameter type
		{
			Node n = new Node(t);

			n.Next = head;
			head = n;			
		}

		// Implementation of the iterator
		public System.Collections.Generic.IEnumerator<T> GetEnumerator()
		{
			Node current = head;

			while (current != null)
			{
				yield return current.Data;

				current = current.Next;
			}	
		}

		// IEnumerable<T> inherits from IEnumerable, therfore this class
		// must implement both the generic and non-generic versions of 
		// GetEnumerator. In most cases, the non-generic method can
		// simply call the generic method.
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}

	public class SorttedList<T> : GenericList<T> where T : IComparable<T>
	{
		// A simple, unoptimized sort algorithm that
		// orders list elements from lowest to height:
		public void BubbleSort()
		{
			if (head == null || head.Next == null)
			{
				return;
			}

			bool swapped;

			do
			{
				Node previous = null;
				Node current = head;
				swapped = false;

				while (current.Next != null)
				{
					// Because we need to call this method, the SortedList
					// class is constrained on IEnumerable<T>
					if (current.Data.CompareTo(current.Next.Data) > 0)
					{
						Node tmp = current.Next;
						current.Next = current.Next.Next;
						tmp.Next = current;

						if (previous == null)
						{
							head = tmp;
						}
						else
						{
							previous.Next= tmp;
						}

						previous = tmp;
						swapped = true;
					}
					else
					{
						previous = current;
						current = current.Next;
					}
				}
			} while (swapped == true);	
		}
	}
  
	// A simple class that implements IComparable<T> using itself as the
	// type argument. This is a common design pattern in objects that
	// are stored in generic lists.
	public class Person : IComparable<Person>
	{
		private string name;
		private int age;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public Person(string name, int age)
		{
			this.name = name;
			this.age = age;
		}

		#endregion
		
		// This will cause list elements to be sorted on age values.
		public int CompareTo(Person p)
		{
			return age - p.age;
		}

		public override string ToString()
		{
			return name + ":" + age.ToString();
		}

		// Must implement Equals.
		public bool Equals(Person p)
		{
			return (this.age == p.age);
		}
	}
  
  
	class Program
	{
		static void Main(string[] args)
		{
			// Declare and instantiate a new generic SortedList class.
			// Person is the type argument.
			SorttedList<Person> list = new SorttedList<Person>();

			// create name and age values to initialize Person objects.
			string[] names = new string[]
			{
				"Fransciose",
				"Bill",
				"Li",
				"Sandra",
				"Alok",
				"Hiroyki",
				"Maria Alessandro",
				"Raul"
			};
			int[] ages = new int[] { 45, 19, 28, 23, 18, 9, 108, 72, 30, 35 };

			// Populate the list.
			for (int i = 0; i < names.Length; i++)
			{
				list.AddHead(new Person(names[i], ages[i]));
			}

			Console.WriteLine("==== Before sorted ====");
			// print out unsorted list.
			printOut(list);

			// sort the list.
			list.BubbleSort();

			Console.WriteLine("\n==== After sorted ====");
			// print out sorted list.
			printOut(list);

			Console.ReadLine();
		}

		private static void printOut(SorttedList<Person> list)
		{
			foreach (Person p in list)
			{
				Console.WriteLine(p.ToString());
			}
		}
	}
}
