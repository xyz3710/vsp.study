﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic05_Methods
{
	public class MyClass<T>
	{
		public void MyMethods<X>(X x)
		{
			Console.WriteLine(x);
		}
	}

	public class MyClass2
	{
		public void MyMethod<PARAM>(PARAM param)		// class 전체 Generic이 아니어도 가능
		{
			Console.WriteLine(param);
		}

		public T SomeMethod<T>(T t) where T : IComparable<T>
		{
			Console.WriteLine(t);
			return t;
		}
	}

	
	public class MyStaticClass<T>
	{
		public static T SomeMethod(T t)
		{
			return t;
		}

		public static T GetSome<X>(T t, X x)
		{
			Console.WriteLine();
			Console.WriteLine(x);

			return t;
		}
	}

  
	class Program
	{
		static void Main(string[] args)
		{
			MyClass2 obj = new MyClass2();

			obj.MyMethod<string>("Test");
			obj.MyMethod(3);
			obj.SomeMethod<string>("test");


			int num = MyStaticClass<int>.SomeMethod(10);
			
			Console.WriteLine(num);


			num = MyStaticClass<int>.GetSome<string>(100, "백");

			Console.WriteLine(num);

			Console.ReadLine();
		}
	}
}
