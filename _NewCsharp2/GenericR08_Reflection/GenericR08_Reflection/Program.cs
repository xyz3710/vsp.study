﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR08_Reflection
{
	public class GenericReflection<T>
	{
		private T item;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public GenericReflection()
		{
			item = default(T);
		}

		#endregion
		
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public GenericReflection(T item)
		{
			this.item = item;
		}

		#endregion

		public T Item
		{
			get
			{
				return item;
			}
			set
			{
				item = value;
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			GenericReflection<int> gReflect = new GenericReflection<int>();

			Type type = gReflect.GetType();

			Console.WriteLine("IsGenericType\t\t\t{0}", type.IsGenericType);
			Console.WriteLine("GetGenericTypeDefinition\t{0}", type.GetGenericTypeDefinition());
			
			//Type[] types = type.GetGenericParameterConstraints();
			
			//foreach (Type t in types)
			//{
			//    Console.WriteLine("GetGenericParameterConstraints\t{0}", t);
			//}		
			
			Console.WriteLine("ConstrainsGenericParameters\t{0}", type.ContainsGenericParameters);
			//Console.WriteLine("GenericParameterAttributes\t{0}", type.GenericParameterAttributes);
			Console.WriteLine("IsGenericParameter\t\t{0}", type.IsGenericParameter);
			if (type.IsGenericParameter == true)
				Console.WriteLine("GenericParameterPosition\t{0}", type.GenericParameterPosition);

			Console.ReadLine();
		}
	}
}
