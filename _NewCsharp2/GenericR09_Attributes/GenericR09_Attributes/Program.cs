﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR08_Attributes
{
	class CustomAttributes : System.Attribute
	{
		public System.Object Info;
	}

	public class GenericClass1<T>
	{
	}

	public class GenericClass2<T, U>
	{
	}

	public class GenericClass3<T, U, V>
	{
	}

	[CustomAttributes(Info = typeof(GenericClass1<>))]
	public class ClassA
	{
	}

	[CustomAttributes(Info = typeof(GenericClass2<,>))]
	public class ClassB
	{		
	}
  
	// Compile error
	//[CustomAttributes(Info = typeof(GenericClass3<int, double, string>))]
	//public class ClassC()
	//{
	//}

	class Program
	{
		static void Main(string[] args)
		{
		}
	}
}
