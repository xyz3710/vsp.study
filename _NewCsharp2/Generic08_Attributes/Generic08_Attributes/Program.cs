﻿using System;
using System.Collections.Generic;
using System.Text;
using LinkedList;

namespace Generic08_Attributes
{
	[AttributeUsage(AttributeTargets.GenericParameter)]
	public class SomeAttribute : Attribute
	{
		void SomeMethod<T>(T t)
		{
		}

		LinkedList<int, string> mList = new LinkedList<int, string>();
	}

	
	//public class SomeAttribute2<T> : Attribute		// comile error : 
	//                                                //	A generic type cannot derrive from 'Attribute' 
	//                                                //	because it is an attribute class
	//{

	//}

	class Program
	{
		static void Main(string[] args)
		{
		}
	}
}
