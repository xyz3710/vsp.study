using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalNamespace
{
	namespace MyApp
	{
		namespace System
		{
			class MyClass
			{
				public void MyMethod()
				{
					// does not work
					//System.Diagnostics.Trace.WriteLine("Does not work!");
				}
			}	
		}
	}

	namespace MyNamespace
	{
		namespace System
		{
			class MyClass
			{
				public void MyMethod()
				{
					global::System.Diagnostics.Trace.WriteLine("In works!");
				}
			}	
		}	
	}

	class Program
	{
		static void Main(string[] args)
		{
			MyNamespace.System.MyClass myclass = new GlobalNamespace.MyNamespace.System.MyClass();

			myclass.MyMethod();

			Console.ReadLine();
		}
	}
}
