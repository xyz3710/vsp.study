﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGeneric02_stack
{
	public class Stack<T>
	{
		private readonly int mSize;
		private int mStackPointer = 0;
		private T[] mItem;

		public Stack()
			: this(100)
		{
		}

		public Stack(int size)
		{
			mSize = size;
			mItem = new T[mSize];
		}

		public void Push(T item)
		{
			if (mStackPointer >= mSize)
				throw new StackOverflowException();

			mItem[mStackPointer++] = item;
		}

		public T Pop()
		{
			if (--mStackPointer >= 0)
			{
				return mItem[mStackPointer];
			}
			else
			{
				mStackPointer = 0;

				return default(T);
			}
		}

		public int Count
		{
			get
			{
				return mStackPointer;
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Stack<string> stack = new Stack<string>();
			string[] data = new string[]{
				"1",
				"two test",
				"three test",
				"end"};

			for (int i = 0; i < data.Length; i++)
			{
				stack.Push(data[i]);

				Console.WriteLine("Pushed => {0}", data[i]);
			}

			Console.WriteLine();

			int count = stack.Count;

			for (int i = 0; i < count; i++)
			{
				Console.WriteLine("Popped => {0}", stack.Pop());
			}

			stack.Pop();

			Console.ReadLine();
		}
	}
}
