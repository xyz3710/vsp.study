using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DelegateInference01
{
	class SomeClass1
	{
		delegate void SomeDelegate();

		public void InvokeMethod()
		{
			SomeDelegate del = new SomeDelegate(SomeMethod1);

			del();
		}

		void SomeMethod1()
		{
			Console.WriteLine("SomeMethod1");
		}
	}

	class SomeClass2
	{
		delegate void SomeDelegate(string buffer);

		public void InvokeMethod(string buffer)
		{
			SomeDelegate del = SomeMethod2;

			del(buffer);
		}

		void SomeMethod2(string buffer)
		{
			Console.WriteLine("SomeMethod2\t{0}", buffer);
		}
	}

	class MyClass
	{
		void ThreadMethod()
		{
			for (int i = 0; i < 5; i++)
			{
				Console.WriteLine("in threadMethod");
				
				Thread.Sleep(1000);
			}			
		}

		public void LaunchThread()
		{
			new Thread(ThreadMethod).Start();
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			SomeClass1 someClass1 = new SomeClass1();

			someClass1.InvokeMethod();


			SomeClass2 someClass2 = new SomeClass2();

			someClass2.InvokeMethod("Hi All");


			MyClass myClass = new MyClass();

			myClass.LaunchThread();

			Console.ReadLine();
		}
	}
}
