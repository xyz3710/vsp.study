﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nullable
{
	class Program
	{
		static void Main(string[] args)
		{
			System.Nullable<int> a = null;

			if (a == null)
			{
				Console.WriteLine("a is null");
			}
			else
			{
				Console.WriteLine("a is NOT null");
			}

			a = 999;

			Console.WriteLine("a = {0}", a);

			// 위의 Nullable type을 간략화 함
			int? b = null;

			if (b == null)
			{
				Console.WriteLine("b is null");
			}
			else
			{
				Console.WriteLine("b is NOT null");
			}

			b = 1000;
			Console.WriteLine("b = {0}", b);
		}
	}
}
