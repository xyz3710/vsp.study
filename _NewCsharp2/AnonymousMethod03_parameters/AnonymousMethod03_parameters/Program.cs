using System;
using System.Collections.Generic;
using System.Text;

namespace AnonymousMEthod03_parameters
{
	class SomeClass1
	{
		delegate void SomeDelegate(string str);

		public void InvokeMethod()
		{
			SomeDelegate del = delegate(string str)
			{
				Console.WriteLine(str);
			};

			del("Hi!~~");
		}
	}

	class SomeClass2
	{
		delegate void SomeDelegate(string str);

		public void InvokeMethod()
		{
			SomeDelegate del = delegate
			{
				Console.WriteLine("Hello");
			};

			del("Parameter is ignored");
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			SomeClass1 someClass1 = new SomeClass1();

			someClass1.InvokeMethod();


			SomeClass2 someClass2 = new SomeClass2();

			someClass2.InvokeMethod();

			Console.ReadLine();
		}
	}
}
