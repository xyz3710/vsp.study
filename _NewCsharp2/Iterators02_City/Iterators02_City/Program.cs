using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace Iterators02_City
{
	/// <summary>
	/// Generic Iterator class
	/// </summary>
	public class CityCollectionGeneric : IEnumerable<string>
	{
		private string[] mCities = { "New York", "Paris", "London" };

		IEnumerator<string> IEnumerable<string>.GetEnumerator()
		{
			for (int i = 0; i < mCities.Length; i++)
			{
				yield return mCities[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<string>)this).GetEnumerator();
		}

		public IEnumerable<string> Reverse
		{
			get
			{
				for (int i = mCities.Length - 1; i >= 0; i--)
				{
					yield return mCities[i];
				}
			}
		}
	}

	public class CityCollectionNoneGeneric : IEnumerable
	{
		private string[] mCities = { "New York", "Paris", "London" };

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			for (int i = 0; i < mCities.Length; i++)
			{
				yield return mCities[i];
			}
		}

		#endregion
	}

	public class Test
	{
		[Obsolete("Use NewTT instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void TT(int index, string item)
		{
			NewTT(index, item);
		}
		public static void NewTT(int index, string item)
		{
			Console.WriteLine(index + item);
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			// normal iterators test
			Console.WriteLine("\tNormal Iterator test");

			string[] cities = { "New York", "Paris", "London" };

			foreach (string city in cities)
			{
				Console.WriteLine(city);
			}

			// generic iterator
			Console.WriteLine("\n\tGeneric<string> Iterator test");
			CityCollectionGeneric cityCollection = new CityCollectionGeneric();

			foreach (string city in cityCollection)
			{
				Console.WriteLine(city);
			}
			
			Console.WriteLine("\t\tReverse");
			foreach (string city in cityCollection.Reverse)
			{
				Console.WriteLine(city);
			}

			// none-generic iterator
			Console.WriteLine("\n\tNone Generic Iterator test");
			CityCollectionNoneGeneric cityCollectionNone = new CityCollectionNoneGeneric();

			foreach (string city in cityCollectionNone)
			{
				Console.WriteLine(city);
			}


			Console.WriteLine("\n\rBinary Tree");
			

			Console.ReadLine();
		}
	}
}
