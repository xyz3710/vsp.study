﻿using System;
using System.Collections.Generic;
using System.Text;


namespace LinkedList
{
	using List=LinkedList<int, string>;

	public class Node<K, T> 
		where K : IComparable<K>
		//where T : new()
/*
 * new() Keyword를 Generic에서 사용할 때는 사용하고자 하는 실제 변수(여기서는 string)에서 
 * public 형의 인자없는 생성자가 있어야 한다.
 * 아니면 Compiler Error CS0310 에러 발생
 * The type 'typename' must have a public parameterless constructor in order to use it as parameter 'parameter' 
 * in the generic type or method 'generic'
 * 
 */
	{
		public K Key;
		public T Item;

		public Node<K, T> NextNode;

		public Node()
		{
			Key = default(K);
			//Item = new T();
			Item = default(T);
			NextNode = null;
		}

		public Node(K key, T item, Node<K, T> nextNode)
		{
			this.Key = key;
			this.Item = item;
			this.NextNode = nextNode;
		}
	}

	public class LinkedList<K, T> 
		where K : IComparable<K>
		//where T : new()
	{
		private Node<K, T> mHead;

		public LinkedList()
		{
			mHead = new Node<K,T>();
		}

		public Node<K, T> NodeHead
		{
			get
			{
				return mHead;
			}
		}

		public void AddHead(K key, T item)
		{
			Node<K, T> newNode = new Node<K, T>(key, item, mHead.NextNode);

			mHead.NextNode = newNode;
		}

		public void Print(Node<K, T> head)
		{
			Node<K, T> current = head;

			Console.WriteLine("====== Print ======");
			
			while (current.NextNode != null)
			{
				current = current.NextNode;
				
				Console.WriteLine("Key : {0},\tItem : {1}", current.Key, current.Item);
			}

			Console.WriteLine("====== Print ======");
		}

		private T find(K key)
		{
			Node<K, T> current = mHead;

			while (current.NextNode != null)
			{
				if (current.Key.CompareTo(key) == 0)
					break;
				else
					current = current.NextNode;
			}

			return current.Item;
		}

		public T this[K key]
		{
			get
			{
				return find(key);
			}
		}

		public static LinkedList<K, T> operator+(LinkedList<K, T> lhs, LinkedList<K, T> rhs)
		{
			return concatenate(lhs, rhs);
		}

		private static LinkedList<K, T> concatenate(LinkedList<K, T> list1, LinkedList<K, T> list2)
		{
			LinkedList<K, T> newList = new LinkedList<K, T>();
			Node<K, T> current = list1.mHead;

			while (current !=null)
			{
				newList.AddHead(current.Key, current.Item);

				current = current.NextNode;
			}

			return newList;
		}
	
	}	
}
