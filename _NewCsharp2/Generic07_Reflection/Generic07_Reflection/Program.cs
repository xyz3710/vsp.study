﻿using System;
using System.Collections.Generic;
using System.Text;
using LinkedList;
using System.Diagnostics;
using System.Reflection;

namespace Generic07_Reflection
{
	class MyClass<T>
	{
		public void SomeMethod(T t)
		{
			Type type = typeof(T);

			Debug.Assert(type == t.GetType());
		}
	}


	class Program
	{
		static void Main()
		{
			LinkedList<int, string> list = new LinkedList<int, string>();

			Type type1 = typeof(LinkedList<int, string>);
			Type type2 = list.GetType();

			Debug.Assert(type1 == type2);

			Console.WriteLine(type1);
			Console.WriteLine(type2);

			Console.WriteLine("{0}", type1 == type2);


			Type unbound1Type = typeof(MyClass<>);

			Console.WriteLine(unbound1Type.ToString());		
			// output : Generic07_Reflection.MyClass`1[T]


			Type unbound2Type = typeof(LinkedList<,>);

			Console.WriteLine(unbound2Type.ToString());
			// output : LinkedList.LinkedList`2[K,T]

			// 여기에서 '1, '2는 unbound한 Generic type의 갯수이다.


			#region Using Type for generic reflectoin
			Type boundedType = list.GetType();
			Console.WriteLine(boundedType.ToString());
			// output : LinkedList.LinkedList`2[System.Int32,System.String]

			Type[] parameters = boundedType.GetGenericArguments();

			Debug.Assert(parameters.Length == 2);
			Debug.Assert(parameters[0] == typeof(int));
			Debug.Assert(parameters[1] == typeof(string));

			
			Type unboundedType = boundedType.GetGenericTypeDefinition();

			Debug.Assert(unboundedType == typeof(LinkedList<,>));
			Console.WriteLine(unboundedType.ToString());
			// output : LinkedList.LinkedList`2[K,T]
			#endregion


			#region MethodInfo
			Type type = list.GetType();
			MethodInfo methodInfo = type.GetMethod("AddHead");
			object[] args = { 1, "AAA" };

			Console.WriteLine("Before Invoke");
			list.Print(list.NodeHead);
			
			methodInfo.Invoke(list, args);
			
			Console.WriteLine("After Invoke");
			list.Print(list.NodeHead);
			#endregion


			Console.WriteLine("end");


			Console.ReadLine();
		}
	}
}
