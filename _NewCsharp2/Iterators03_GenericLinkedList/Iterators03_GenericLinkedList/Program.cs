using System;
using System.Collections.Generic;
using System.Text;

namespace Interators03_GenericLinkedList
{
	class Node<K, T>
	{
		public K Key;
		public T Item;

		public Node<K, T> NextNode;
	}

	public class LinkedList<K, T> : IEnumerable<T>
	{
		Node<K, T> _head;
		
		public LinkedList()
		{
			_head = new Node<K, T>();
		}

		public void AddHead(K key, T item)
		{
			Node<K, T> node = new Node<K, T>();

			node.Key = key;
			node.Item = item;
			node.NextNode = _head.NextNode;

			_head.NextNode = node;
		}

		#region IEnumerable<T> Members

		public IEnumerator<T> GetEnumerator()
		{
			Node<K, T> current = _head;

			while (current != null)
			{
				yield return current.Item;

				current = current.NextNode;
			}
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<T>)this).GetEnumerator();
		}

		#endregion
	}

	class Program
	{
		static void Main(string[] args)
		{
			LinkedList<int, string> linkedList = new LinkedList<int, string>();

			for (int i = 0; i < 5; i++)
			{
				linkedList.AddHead(i, "Item" + i.ToString());
			}

			foreach (string str in linkedList)
			{
				Console.WriteLine(str);
			}

			Console.ReadLine();
		}
	}
}
