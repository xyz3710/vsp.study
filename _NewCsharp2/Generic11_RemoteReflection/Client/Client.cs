﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic11_RemoteReflection
{
	public static class ClientApp
	{
		public static void Main(string[] args)
		{
			ISharedInterface<string> strObj = 
				(ISharedInterface<string>)Activator.GetObject(typeof(ISharedInterface<string>),
				"ipc://test/stringRemoteObject.rem");

			Console.WriteLine("String Object responds: " 
				+ strObj.DoSomething("Hi Server"));

			//ISharedInterface<int> intRemoteObject =
			//    (ISharedInterface<int>)Activator.GetObject(typeof(ISharedInterface<>),
			//    "ipc://test/genericRemtoeObject.rem");

			//Console.WriteLine("Int Object responds: "
			//    + intRemoteObject.DoSomething(80));
		}
	}
}
