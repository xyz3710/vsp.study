﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Remoting.Channels;

namespace Generic11_RemoteReflection
{
	// Remote server
	internal class GenericServer<T> : MarshalByRefObject, ISharedInterface<T>
		where T : MarshalByRefObject
	{
		#region ISharedInterface<T> Members

		T ISharedInterface<T>.DoSomething(T t)
		{
			Console.WriteLine(t);

			return t;
		}

		#endregion
	}
  
	// Remote host service
	class HostService
	{
		static void Main(string[] args)
		{
			RemotingConfiguration.RegisterWellKnownServiceType(typeof(GenericServer<>),
				"StringRemoteObject.rem", WellKnownObjectMode.SingleCall);

			//RemotingConfiguration.RegisterWellKnownServiceType(typeof(ImpementatoinClass<>),
			//    "GenericRemoteObject.rem", WellKnownObjectMode.SingleCall);

			IpcChannel channel = new IpcChannel("test");
			ChannelServices.RegisterChannel(channel);

			Console.WriteLine("Press enter to exit");
			Console.ReadLine();
		}
	}
}
