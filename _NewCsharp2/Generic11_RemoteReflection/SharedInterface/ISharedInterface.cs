﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic11_RemoteReflection
{
	public interface ISharedInterface<T>
	{
		T DoSomething(T t);
	}
}
