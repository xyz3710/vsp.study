﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessorAccessibility01
{
	class MyClass1
	{
		private string prop;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public MyClass1()
		{
			this.prop = "MyClass1";
		}

		#endregion

		public string Prop
		{
			get
			{
				return prop;
			}

			protected set		// get/set의 접근 제한자를 달리할 수 있다.
			{
				prop = value;
			}
		}
	}

	class MyClass2 : MyClass1
	{
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public MyClass2()
		{
			base.Prop = "MyClass2";
		}

		#endregion
		
	}

	class MyClass3
	{
		private string prop;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public MyClass3()
		{
			this.prop = "MyClass3";
		}

		#endregion

		public virtual string Prop
		{
			get
			{
				return prop;
			}

			protected set		// get/set의 접근 제한자를 달리할 수 있다.
			{
				prop = value;
			}
		}
	}
  
	class MyClass4 : MyClass3
	{
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public MyClass4()
		{
			
		}

		#endregion

		public override string Prop
		{
			get
			{
				return base.Prop;
			}
			// override 하는 경우 다음과 같이 base class와 동일하게 접근 제한자를 명시해야한다.
			protected set
			{
				base.Prop = value;
			}
		}
	}
  

	class Program
	{
		static void Main(string[] args)
		{
			MyClass1 myclass1 = new MyClass1();

			Console.WriteLine(myclass1.Prop);
			//myclass1.Prop = "Test1";		// error
			Console.WriteLine(myclass1.Prop);

			
			MyClass2 myclass2 = new MyClass2();

			Console.WriteLine(myclass2.Prop);
			//myclass2.Prop = "Test2";		// error
			Console.WriteLine(myclass2.Prop);

			Console.ReadLine();
		}
	}
}
