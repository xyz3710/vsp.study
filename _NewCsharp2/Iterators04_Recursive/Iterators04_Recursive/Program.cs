using System;
using System.Collections.Generic;
using System.Text;

namespace Iterator04_Recursive
{
	class Node<T> 
		where T : IComparable<T>
	{
		public Node<T> LeftNode;
		public Node<T> RightNode;
		public T Item;
	}

	public class BinaryTree<T>
		where T : IComparable<T>
	{
		private Node<T> _root;

		public void Add(params T[] items)
		{
			Array.ForEach(items, Add);
		}

		public void Add(T item)
		{
			add(item, _root);
		}

		private void add(T item, Node<T> rootNode)
		{
			if (_root == null)
			{
				_root = new Node<T>();

				_root.Item = item;

				return;
			}

			if (rootNode.Item.CompareTo(item) > 0)
			{
				// insert left node
				Node<T> leftNode = new Node<T>();

				leftNode.Item = item;

				if (rootNode.LeftNode == null)
					rootNode.LeftNode = leftNode;
				else
					add(item, rootNode.LeftNode);
			}
			else if (rootNode.Item.CompareTo(item) < 0)
			{
				// insert right node
				Node<T> rightNode = new Node<T>();

				rightNode.Item = item;

				if (rootNode.RightNode == null)
					rootNode.RightNode = rightNode;
				else
					add(item, rootNode.RightNode);
			}
		}

		public IEnumerable<T> InOrder
		{
			get
			{
				return ScanInOrder(_root);
			}
		}

		IEnumerable<T> ScanInOrder(Node<T> _root)
		{
			if (_root.LeftNode != null)
			{
				foreach (T item in ScanInOrder(_root.LeftNode))
				{
					yield return item;
				}
			}

			yield return _root.Item;

			if (_root.RightNode != null)
			{
				foreach (T item in ScanInOrder(_root.RightNode))
				{
					yield return item;
				}
			}
		}
	}
	
	class Program
	{
		private static void Main(string[] args)
		{
			BinaryTree<int> tree = new BinaryTree<int>();

			tree.Add(9, 8, 0, 4, 6, 2, 7, 5, 3, 1);

			foreach (int number in tree.InOrder)
			{
				Console.WriteLine(number);
			}


			Console.ReadLine();
		}
	}
}
