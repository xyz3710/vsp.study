﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Remoting;

namespace Generic10_Serialization
{
	#region Client-side serialization of a generic type
	
	[Serializable]
	public class MyClass<T>
	{

	}

	#endregion

	#region Generic and Remoting

	public class MyServer<T> : MarshalByRefObject 
		where T : MarshalByRefObject
	{

	}
  

	#endregion
	
	class Program
	{
		static void Main(string[] args)
		{
			#region Client-side serialization of a generic type

			MyClass<int> obj1 = new MyClass<int>();

			IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

			Stream stream = new FileStream(@"D:\VSP\Test\obj.bin", FileMode.Create, FileAccess.ReadWrite);

			using (stream)
			{
				formatter.Serialize(stream, obj1);

				stream.Seek(0, SeekOrigin.Begin);

				MyClass<int> obj2;

				obj2 = (MyClass<int>)formatter.Deserialize(stream);
			}

			#endregion

			#region Generic and Remoting

			Type serverType = typeof(MyServer<int>);
			string url = @"http://www.daum.net";

			RemotingConfiguration.RegisterActivatedServiceType(serverType);

			MyServer<int> obj;
			//obj = new MyServer<int>();
			obj = (MyServer<int>)Activator.CreateInstance(serverType);

			// use obj
			
			Microsoft.VisualBasic.Interaction.AppActivate("Untitled - Notepad");
			#endregion
	
		}
	}
}
