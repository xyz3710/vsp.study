﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericR03_Classes
{
	public class BaseNode
	{
	}

	public class BaseNodeGeneric<T>
	{
	}

	// concrete type
	class NodeConcrete<T> : BaseNode
	{
	}

	// closed constructred type
	class NodeClosed<T> : BaseNodeGeneric<int>
	{
	}

	// open constructed type
	class NodeOpen<T> : BaseNodeGeneric<T>
	{
	}

	// no error
	class Node1 : BaseNodeGeneric<int>
	{
	}
  
	// generate an error
	//class Node2 : BaseNodeGeneric<T>
	//{
	//}

	// generate an error
	//class Node3 : T
	//{
	//}



	class BaseNodeMultiple<T, U>
	{
	}

	// no error
	class Node4<T> : BaseNodeMultiple<T, int>
	{
	}

	// no error
	class Node5<T, U> : BaseNodeMultiple<T, U>
	{
	}

	// generate an error
	//class Node6<T> : BaseNodeMultiple<T, U>
	//{
	//}



	class NodeItem<T> where T : IComparable<T>, new()
	{
	}

	class SpecialNodeItem<T> : NodeItem<T> where T : IComparable<T>, new()
	{
	}


	class SuperKeyType<K, V, U>
		where U : IComparable<U>
		where V : new()
	{
	}

	
	class Program
	{
		static void Main(string[] args)
		{

			Console.ReadLine();
		}
	}
}
