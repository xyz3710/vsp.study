﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Gpio;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// 빈 페이지 항목 템플릿은 http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 에 문서화되어 있습니다.

namespace App1
{
	/// <summary>
	/// 자체에서 사용하거나 프레임 내에서 탐색할 수 있는 빈 페이지입니다.
	/// </summary>
	public sealed partial class MainPage : Page
	{
		private const int LED_PIN = 12;

		private GpioPin _pin;
		GpioPinValue _pinValue;

		public MainPage()
		{

			this.InitializeComponent();
			ClickMe.Click += (sender, e) =>
			{
				var now = DateTime.Now;

				HelloMessage.Text = $"Hello Windows 10 IoT Core! {now.ToString()}";
			};
			btnLEDControl.Click += btnLEDControl_Click;
			tbGpioStatus.Text = string.Empty;
			InitGPIO();
		}

		private GpioPinValue PinValue
		{
			get
			{
				return _pinValue;
			}
			set
			{
				if (_pinValue == value)
				{
					return;
				}

				switch (_pinValue)
				{
					case GpioPinValue.High:
						_pinValue = GpioPinValue.Low;
						tbGpioStatus.Text = "LED 끄기";

						break;
					case GpioPinValue.Low:
						_pinValue = GpioPinValue.High;
						tbGpioStatus.Text = "LED 켜기";

						break;
				}

				_pin.Write(_pinValue);
			}
		}
		private void InitGPIO()
		{
			var gpio = GpioController.GetDefault();

			if (gpio == null)
			{
				tbGpioStatus.Text = "GPIO 디바이스가 없습니다.";
				_pin = null;

				return;
			}

			_pin = gpio.OpenPin(LED_PIN);
			_pin.Write(GpioPinValue.High);
			_pin.SetDriveMode(GpioPinDriveMode.Output);

			tbGpioStatus.Text = $"GPIO {LED_PIN}번 핀이 정상 초기화 되었습니다.";
		}

		private void btnLEDControl_Click(object sender, RoutedEventArgs e)
		{
			switch (_pinValue)
			{
				case GpioPinValue.High:
					_pinValue = GpioPinValue.Low;
					btnLEDControl.Content = "LED 끄기";

					break;
				case GpioPinValue.Low:
					_pinValue = GpioPinValue.High;
					btnLEDControl.Content = "LED 켜기";

					break;
			}

			_pin.Write(_pinValue);
		}
	}
}
