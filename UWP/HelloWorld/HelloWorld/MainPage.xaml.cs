﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Gpio;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// 빈 페이지 항목 템플릿은 http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 에 문서화되어 있습니다.

namespace HelloWorld
{
	/// <summary>
	/// 자체에서 사용하거나 프레임 내에서 탐색할 수 있는 빈 페이지입니다.
	/// </summary>
	public sealed partial class MainPage : Page
	{
		#region Constants
		private const int LED_PIN = 18;
		#endregion

		private GpioPin _pin;

		public MainPage()
		{
			this.InitializeComponent();
			InitGPIO();
		}

		private void InitGPIO()
		{
			var gpio = GpioController.GetDefault();

			if (gpio == null)
			{
				tbMessage.Text = "이 장치에는 GPIO controller 가 없습니다.";

				return;
			}

			_pin = gpio.OpenPin(LED_PIN);

			if (_pin == null)
			{
				tbMessage.Text = $"핀 {LED_PIN}번을 초기화하는데 오류가 발생했습니다.";

				return;
			}

			_pin.Write(GpioPinValue.Low);
			_pin.SetDriveMode(GpioPinDriveMode.Output);
			tbMessage.Text = $"GPIO 핀 {LED_PIN}번이 정상적으로 초기화 되었습니다.";

		}

		private void btnOn_Click(object sender, RoutedEventArgs e)
		{
			tbMessage.Text = "Say hello";
			_pin.Write(GpioPinValue.High);
		}

		private void btnOff_Click(object sender, RoutedEventArgs e)
		{
			tbMessage.Text = "Good Bye";
			_pin.Write(GpioPinValue.Low);
		}
	}
}
