using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Diagnostics;

namespace BlogTest
{
	public class BlogContext : DbContext
	{
		public BlogContext()
		{
			Database.SetInitializer<BlogContext>(new DatabaseInitializer());
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			// one to many
			//modelBuilder.Entity<Comment>().HasRequired<Post>(x => x.Post)
			//	.WithMany(c => c.Comments).HasForeignKey(x => x.PostId);
			
		}

		/// <summary>
		/// Post를 구하거나 설정합니다.
		/// </summary>
		/// <value>Post를 반환합니다.</value>
		public DbSet<Post> Posts
		{
			get;
			set;
		}

		/// <summary>
		/// Blog를 구하거나 설정합니다.
		/// </summary>
		/// <value>Blog를 반환합니다.</value>
		public DbSet<Blog> Blogs
		{
			get;
			set;
		}

		/// <summary>
		/// Comment를 구하거나 설정합니다.
		/// </summary>
		/// <value>Comment를 반환합니다.</value>
		public DbSet<Comment> Comments
		{
			get;
			set;
		}
	}
}

