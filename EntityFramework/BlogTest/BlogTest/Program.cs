﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.Extensions;

namespace BlogTest
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var db = new BlogContext())
			{
				db.Blogs.Add(new Blog
				{
					Name = "first",
					Posts = new List<Post>
					{
						new Post{Title = "post1", Content="content1",},
						new Post{Title = "post2", Content="content2",},
					},
					Items = new List<Item> { new Item { Value = 1 }, new Item { Value = 2 }, new Item { Value = 4 }, },
				});

				var blogs = new List<Blog>
				{
					new Blog
					{
						Name = "second",
						Posts = new List<Post>
						{
							new Post{Title = "post3", Content="content1",},
							new Post{Title = "post4", Content="content2",},
						},
						Items = new List<Item> { new Item { Value = 1 }, new Item { Value = 2 }, new Item { Value = 4 }, },
					},
					new Blog
					{
						Name = "third",
						Posts = new List<Post>
						{
							new Post{Title = "post5", Content="content1",},
							new Post{Title = "post6", Content="content2",},
						},
						Items = new List<Item> { new Item { Value = 1 }, new Item { Value = 2 }, new Item { Value = 4 }, },
					}
				};
				db.BulkInsert<Blog>(blogs);
				var result = db.SaveChanges();
				Console.WriteLine("Result: {0}", result);
			}

			using (var db = new BlogContext())
			{
				var blog = db.Blogs.ToList();
				var post = db.Posts.ToList();
			}

			//Test2();
		}

		private static void Test2()
		{
			using (var db = new BlogContext())
			{
				Blog testBlog = new Blog
								{
									Name = "Test",
								};
				db.Blogs.Add(testBlog);
				db.SaveChanges();

				Post firstPost = new Post
								{
									BlogId = testBlog.Id,
									Title = "첫 글",
									//Comments = firstPostComment,
								};
				db.Posts.Add(firstPost);
				db.SaveChanges();

				List<Comment> firstPostComment = new List<Comment>
								{
									new Comment{PostId=firstPost.Id, Note = "댓글1", Rate = new List<int>{ 7, 8, 9 }},
									new Comment{PostId=firstPost.Id, Note = "댓글2", Rate = new List<int>{ 10, 30 }},
								};

				//firstPost.Comments.Add(new Comment
				//{
				//	PostId = firstPost.Id,
				//	Note = "댓글1",
				//	Rate = new List<int> { 7, 8, 9 }
				//});
				//firstPost.Comments.Add(new Comment
				//{
				//	PostId = firstPost.Id,
				//	Note = "댓글2",
				//	Rate = new List<int> { 10, 30 }
				//});
				db.SaveChanges();

				List<Post> posts = new List<Post>
								{
									new Post
									{
										//Blog = testBlog,
										Title = "두번째 글",
										//Comments = new List<Comment>{
										//new Comment{Note = "댓글1", Rate = new List<int>{ 1, 2, 3 }},
										//new Comment{Note = "댓글2", Rate = new List<int>{ 1, 3 }},
										//new Comment{Note = "댓글3", Rate = new List<int>{ 3 }},
										//},
									},
									new Post
									{
										//Blog = testBlog,
										Title = "세번째 글",
										//Comments = new List<Comment>{
										//new Comment{Note = "댓글1", Rate = new List<int>{ 4, 5, 6 }},
										//new Comment{Note = "댓글2", Rate = new List<int>{ 1, 3 }},
										//},
									},
									new Post
									{
										//Blog = testBlog,
										Title = "네번째 글",
										//Comments = new List<Comment>{
										//new Comment{Note = "댓글1", Rate = new List<int>{ 3, 2, 1 }},
										//new Comment{Note = "댓글2", Rate = new List<int>{ 3 }},
										//},
									},
								};
				var option = new BulkInsertOptions
				{
					SqlBulkCopyOptions = System.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints
				};
				db.BulkInsert<Post>(posts, option);

				db.SaveChanges();

				var post = db.Posts.First();
				var comment = db.Comments.ToList();


				var bulkPosts = db.Posts.ToList();
			}

			using (var db = new BlogContext())
			{
				var post1 = db.Posts.First();
				//Console.WriteLine("{0}, {1}.", post1.Title, post1.Comments);
			}
		}

	}
}
