﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Diagnostics;

namespace BlogTest
{
	[DebuggerDisplay("Id:{Id}, Name:{Name}", Name = "Blog")]
	public class Blog
	{
		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Items를 구하거나 설정합니다.
		/// </summary>
		/// <value>Items를 반환합니다.</value>
		public virtual List<Item> Items
		{
			get;
			set;
		}
		
		public virtual List<Post> Posts
		{
			get;
			set;
		}
	}

	public class Item
	{
		public int Id
		{
			get;
			set;
		}

		public int Value
		{
			get;
			set;
		}

		public int BlogId
		{
			get;
			set;
		}

		public virtual Blog Blog
		{
			get;
			set;
		}
	}

	[DebuggerDisplay("Id:{Id}, Title:{Title}, BlogId:{BlogId}, Blog:{Blog}", Name = "Post")]
	public class Post
	{
		public int Id
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public string Content
		{
			get;
			set;
		}

		public int BlogId
		{
			get;
			set;
		}

		public virtual Blog Blog
		{
			get;
			set;
		}
	}

	[DebuggerDisplay("Id:{Id}, Note:{Note}", Name = "Comment")]
	public class Comment
	{
		public int Id
		{
			get;
			set;
		}

		public string Note
		{
			get;
			set;
		}

		public int PostId
		{
			get;
			set;
		}
		
		public virtual Post Post
		{
			get;
			set;
		}

		public virtual List<int> Rate
		{
			get;
			set;
		}
		
	}
}