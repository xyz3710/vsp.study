﻿#define PERFORMANCE_TEST
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbConnect.Models;

namespace DbConnect
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var db = new APQRContext())
			{
				var dtLong = db.GetDataTable("UP_QA_GetYearPrint",
						new ParamHelper("DT_Gubun", "B"),
						new ParamHelper("DT_From", "2012-01-01"),
						new ParamHelper("DT_To", "2012-12-16"),
						new ParamHelper("itemCode", "2090201"),
						new ParamHelper("procCode", "BP0"),
						new ParamHelper("years", "2012"));
				var dtShort = db.GetDataSet("UP_QA_GetYearPrint",
						new ParamHelper("DT_Gubun", "B"),
						new ParamHelper("DT_From", "2012-01-01"),
						new ParamHelper("DT_To", "2012-12-16"),
						new ParamHelper("itemCode", "0933040"),
						new ParamHelper("procCode", "AP0"),
						new ParamHelper("years", "2012"));
				var dtLong2 = db.PrepareQuery("UP_QA_GetYearPrint")
						.SetParameter("DT_Gubun", "B")
						.SetParameter("DT_From", "2012-01-01")
						.SetParameter("DT_To", "2012-12-16")
						.SetParameter("itemCode", "2090201")
						.SetParameter("procCode", "BP0")
						.SetParameter("years", "2012")
						.GetDataSet();
				var dtShort2 = db.PrepareQuery("UP_QA_GetYearPrint")
						.SetParameter("DT_Gubun", "B")
						.SetParameter("DT_From", "2012-01-01")
						.SetParameter("DT_To", "2012-12-16")
						.SetParameter("itemCode", "0933040")
						.SetParameter("procCode", "AP0")
						.SetParameter("years", "2012")
						.Translate<YearPrint>(db);
			}
		}
	}
}
