﻿/**********************************************************************************************************************/
/*	Domain		:	System.Data.Entity.DbContextExtensions
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	Wednesday, December 04, 2013 1:59 PM
/*	Purpose		:	System.Data.Entity.DbContext의 Stored Procedure 관련 확장 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Data;

namespace System.Data.Entity
{
	/// <summary>
	/// <seealso cref="System.Data.Entity.DbContext"/>의 Stored Procedure 관련 확장 기능을 제공합니다.
	/// </summary>
	public static class DbContextExtensions
	{
		/// <summary>
		/// Stored Procedure를 사용하는 <seealso cref="System.Data.Command.DbCommand"/>를 구합니다.
		/// </summary>
		/// <param name="dbContext">DbContext</param>
		/// <param name="procedureName">DbCommand에서 사용할 Stored Procedure 이름</param>
		/// <param name="timeOut">명령을 실행할 때까지 대기하는 시간(초)</param>
		/// <returns><seealso cref="System.Data.Command.DbCommand"/>를 반환합니다.</returns>
		public static DbCommand PrepareQuery(this DbContext dbContext, string procedureName, int timeOut = 120)
		{
			var command = dbContext.Database.Connection.CreateCommand();

			command.CommandType = System.Data.CommandType.StoredProcedure;
			command.CommandText = procedureName;
			command.CommandTimeout = timeOut;

			return command;
		}

		/// <summary>
		/// DbCommand에 Parameter를 추가 합니다.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <param name="value">The value.</param>
		/// <returns>DbCommand.</returns>
		public static DbCommand SetParameter(this DbCommand command, string parameterName, dynamic value)
		{
			SqlParameter parameter = new SqlParameter(parameterName, value);

			command.Parameters.Add(parameter);

			return command;
		}

		/// <summary>
		/// Stored Procedure를 이용한 <seealso cref="System.Data.Command.DbCommand" />에서 DataTable을 구합니다.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="tableName">Name of the table.</param>
		/// <returns>tableName의 이름을 가진 <seealso cref="System.Data.DataTable" />을 반환 합니다.</returns>
		/// <exception cref="System.Data.DataException"></exception>
		public static DataTable GetDataTable(this DbCommand command, string tableName = null)
		{
			var resultTable = new DataTable(tableName ?? command.CommandText);
			DbDataReader dataReader = null;

			command.Connection.Open();

			try
			{
				dataReader = command.ExecuteReader(CommandBehavior.Default);
				resultTable.Load(dataReader);
			}
			catch (Exception ex)
			{
				throw new DataException(ex.Message, ex.InnerException);
			}
			finally
			{
				if (dataReader != null)
					dataReader.Close();

				command.Connection.Close();
			}

			return resultTable;
		}

		/// <summary>
		/// Stored Procedure를 이용한 <seealso cref="System.Data.Command.DbCommand" />에서 DataTable을 구합니다.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="datasetName">Name of the table.</param>
		/// <returns>tableName의 이름을 가진 <seealso cref="System.Data.DataTable" />을 반환 합니다.</returns>
		/// <exception cref="System.Data.DataException"></exception>
		public static DataSet GetDataSet(this DbCommand command, string datasetName = null)
		{
			var resultSet = new DataSet(datasetName ?? command.CommandText);
			DbDataReader dataReader = null;

			command.Connection.Open();

			try
			{
				IDataAdapter adapter = new SqlDataAdapter(command as SqlCommand);

				adapter.Fill(resultSet);
			}
			catch (Exception ex)
			{
				throw new DataException(ex.Message, ex.InnerException);
			}
			finally
			{
				if (dataReader != null)
					dataReader.Close();

				command.Connection.Close();
			}

			return resultSet;
		}

		/// <summary>
		/// Stored Procedure를 이용한 <seealso cref="System.Data.Command.DbCommand" />에서 Entity을 구합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="command">The command.</param>
		/// <param name="dbContext">Entity를 구할 DbContext</param>
		/// <returns>tableName의 이름을 가진 <seealso cref="System.Data.DataTable" />을 반환 합니다.</returns>
		/// <exception cref="System.Data.DataException"></exception>
		public static List<T> Translate<T>(this DbCommand command, DbContext dbContext)
			where T : class
		{
			List<T> result = new List<T>();

			command.Connection.Open();

			try
			{
				var context = (dbContext as IObjectContextAdapter).ObjectContext;

				result = context.Translate<T>(command.ExecuteReader(CommandBehavior.Default)).ToList();
			}
			catch (Exception ex)
			{
				throw new DataException(ex.Message, ex.InnerException);
			}
			finally
			{
				command.Connection.Close();
			}

			return result;
		}

		/// <summary>
		/// Stored Procedure를 통해서 DataTable을 구합니다.
		/// </summary>
		/// <param name="db">The db.</param>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>procedureName의 이름을 가진 <seealso cref="System.Data.DataTable"/>을 반환 합니다.</returns>
		/// <exception cref="System.Data.DataException"></exception>
		public static DataTable GetDataTable(this DbContext db, string procedureName, params ParamHelper[] parameters)
		{
			var resultTable = new DataTable(procedureName);
			DbDataReader dataReader = null;

			try
			{
				db.Database.Connection.Open();

				var command = db.Database.Connection.CreateCommand();

				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = procedureName;

				if (parameters != null && parameters.Length > 0)
					command.Parameters.AddRange(parameters.Select(x => new SqlParameter(x.ParameterName, x.Value)).ToArray());

				dataReader = command.ExecuteReader(CommandBehavior.Default);
				resultTable.Load(dataReader);
			}
			catch (Exception ex)
			{
				throw new DataException(ex.Message, ex.InnerException);
			}
			finally
			{
				if (dataReader != null)
					dataReader.Close();

				db.Database.Connection.Close();
			}

			return resultTable;
		}

		/// <summary>
		/// Stored Procedure를 통해서 DataSet을 구합니다.
		/// </summary>
		/// <param name="db">The db.</param>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>procedureName의 이름을 가진 <seealso cref="System.Data.DataSet"/>을 반환 합니다.</returns>
		/// <exception cref="System.Data.DataException"></exception>
		public static DataSet GetDataSet(this DbContext db, string procedureName, params ParamHelper[] parameters)
		{
			var resultSet = new DataSet(procedureName);

			try
			{
				db.Database.Connection.Open();

				var command = db.Database.Connection.CreateCommand();

				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = procedureName;

				if (parameters != null && parameters.Length > 0)
					command.Parameters.AddRange(parameters.Select(x => new SqlParameter(x.ParameterName, x.Value)).ToArray());

				IDataAdapter adapter = new SqlDataAdapter(command as SqlCommand);

				adapter.Fill(resultSet);
			}
			catch (Exception ex)
			{
				throw new DataException(ex.Message, ex.InnerException);
			}
			finally
			{
				db.Database.Connection.Close();
			}

			return resultSet;
		}
	}

	/// <summary>
	/// <seealso cref="System.Data.Entity.DbContextExtensions"/> 에서 사용되는 Parameter 입력용 클래스 입니다.
	/// </summary>
	public class ParamHelper
	{
		/// <summary>
		/// SqlParameter
		/// </summary>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <param name="value">The value.</param>
		public ParamHelper(string parameterName, dynamic value)
		{
			ParameterName = parameterName;
			Value = value;
		}

		/// <summary>
		/// 파라미터 이름을 구하거나 설정합니다.
		/// </summary>
		public string ParameterName
		{
			get;
			set;
		}

		/// <summary>
		/// 파라미터 값를 구하거나 설정합니다.
		/// </summary>
		public dynamic Value
		{
			get;
			set;
		}
	}
}
