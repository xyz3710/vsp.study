using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DbConnect.Models
{
	public partial class APQRContext : DbContext
	{
		static APQRContext()
		{
			Database.SetInitializer<APQRContext>(null);
		}

		public APQRContext()
			: base("Name=APQRContext")
		{
		}

		//public DbSet<YearPrint> YearPrints
		//{
		//	get;
		//	set;
		//}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//modelBuilder.Configurations.Add(new T_QA_YearPrintMap());
		}
	}
}
