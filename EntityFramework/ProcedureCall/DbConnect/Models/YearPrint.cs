﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DbConnect.Models
{
	[DebuggerDisplay("Num:{Num}, Seq:{Seq}, ReqstDT:{ReqstDT}, BatchDT:{BatchDT}, TestNo:{TestNo}, BatchNo:{BatchNo}, ItemCode:{ItemCode}, ItemName:{ItemName}, ProcCode:{ProcCode}, ProcName:{ProcName}", Name = "YearPrint")]
	public class YearPrint
	{
		public int Num
		{
			get;
			set;
		}
		public int Seq
		{
			get;
			set;
		}

		public string ReqstDT
		{
			get;
			set;
		}

		public string BatchDT
		{
			get;
			set;
		}

		public string TestNo
		{
			get;
			set;
		}

		public string BatchNo
		{
			get;
			set;
		}

		public string ItemCode
		{
			get;
			set;
		}

		public string ItemName
		{
			get;
			set;
		}

		public string ProcCode
		{
			get;
			set;
		}

		public string ProcName
		{
			get;
			set;
		}

		public string TS01
		{
			get;
			set;
		}

		public string TS02
		{
			get;
			set;
		}

		public string TS03
		{
			get;
			set;
		}

		public string TS04
		{
			get;
			set;
		}

		public string TS05
		{
			get;
			set;
		}

		public string TS06
		{
			get;
			set;
		}

		public string TS07
		{
			get;
			set;
		}

		public string TS08
		{
			get;
			set;
		}

		public string TS09
		{
			get;
			set;
		}

		public string TS10
		{
			get;
			set;
		}

		public string TS11
		{
			get;
			set;
		}

		public string TS12
		{
			get;
			set;
		}

		public string TS13
		{
			get;
			set;
		}

		public string TS14
		{
			get;
			set;
		}

		public string TS15
		{
			get;
			set;
		}

		public string TS16
		{
			get;
			set;
		}

		public string TS17
		{
			get;
			set;
		}

		public string TS18
		{
			get;
			set;
		}

		public string TS19
		{
			get;
			set;
		}

		public string TS20
		{
			get;
			set;
		}

		public string TS21
		{
			get;
			set;
		}

		public string TS22
		{
			get;
			set;
		}

		public string TS23
		{
			get;
			set;
		}

		public string TS24
		{
			get;
			set;
		}

		public string TS25
		{
			get;
			set;
		}

		public string TS26
		{
			get;
			set;
		}

		public string TS27
		{
			get;
			set;
		}

		public string TS28
		{
			get;
			set;
		}

		public string TS29
		{
			get;
			set;
		}

		public string TS30
		{
			get;
			set;
		}

		public string TS31
		{
			get;
			set;
		}

		public string TS32
		{
			get;
			set;
		}

		public string TS33
		{
			get;
			set;
		}

		public string TS34
		{
			get;
			set;
		}

		public string TS35
		{
			get;
			set;
		}

		public string TS36
		{
			get;
			set;
		}

		public string TS37
		{
			get;
			set;
		}

		public string TS38
		{
			get;
			set;
		}

		public string TS39
		{
			get;
			set;
		}

		public string TS40
		{
			get;
			set;
		}

		public string TS41
		{
			get;
			set;
		}

		public string TS42
		{
			get;
			set;
		}

		public string TS43
		{
			get;
			set;
		}

		public string TS44
		{
			get;
			set;
		}

		public string TS45
		{
			get;
			set;
		}

		public string TS46
		{
			get;
			set;
		}

		public string TS47
		{
			get;
			set;
		}

		public string TS48
		{
			get;
			set;
		}

		public string TS49
		{
			get;
			set;
		}

		public string TS50
		{
			get;
			set;
		}

		public string TS51
		{
			get;
			set;
		}

		public string TS52
		{
			get;
			set;
		}

		public string TS53
		{
			get;
			set;
		}

		public string TS54
		{
			get;
			set;
		}

		public string TS55
		{
			get;
			set;
		}

		public string TS56
		{
			get;
			set;
		}

		public string TS57
		{
			get;
			set;
		}

		public string TS58
		{
			get;
			set;
		}

		public string TS59
		{
			get;
			set;
		}

		public string TS60
		{
			get;
			set;
		}

		public string TS61
		{
			get;
			set;
		}

		public string TS62
		{
			get;
			set;
		}

		public string TS63
		{
			get;
			set;
		}

		public string TS64
		{
			get;
			set;
		}

		public string TS65
		{
			get;
			set;
		}

		public string TS66
		{
			get;
			set;
		}

		public string TS67
		{
			get;
			set;
		}

		public string TS68
		{
			get;
			set;
		}

		public string TS69
		{
			get;
			set;
		}

		public string TS70
		{
			get;
			set;
		}

		public string TS71
		{
			get;
			set;
		}

		public string TS72
		{
			get;
			set;
		}

		public string TS73
		{
			get;
			set;
		}

		public string TS74
		{
			get;
			set;
		}

		public string TS75
		{
			get;
			set;
		}

		public string TS76
		{
			get;
			set;
		}

		public string TS77
		{
			get;
			set;
		}

		public string TS78
		{
			get;
			set;
		}

		public string TS79
		{
			get;
			set;
		}

		public string TS80
		{
			get;
			set;
		}

		public string TS81
		{
			get;
			set;
		}

		public string TS82
		{
			get;
			set;
		}

		public string TS83
		{
			get;
			set;
		}

		public string TS84
		{
			get;
			set;
		}

		public string TS85
		{
			get;
			set;
		}

		public string TS86
		{
			get;
			set;
		}

		public string TS87
		{
			get;
			set;
		}

		public string TS88
		{
			get;
			set;
		}

		public string TS89
		{
			get;
			set;
		}

		public string TS90
		{
			get;
			set;
		}

		public string TS91
		{
			get;
			set;
		}

		public string TS92
		{
			get;
			set;
		}

		public string TS93
		{
			get;
			set;
		}

		public string TS94
		{
			get;
			set;
		}

		public string TS95
		{
			get;
			set;
		}

		public string TS96
		{
			get;
			set;
		}

		public string TS97
		{
			get;
			set;
		}

		public string TS98
		{
			get;
			set;
		}

		public string TS99
		{
			get;
			set;
		}

		public string TS100
		{
			get;
			set;
		}

		public string TS101
		{
			get;
			set;
		}

		public string TS102
		{
			get;
			set;
		}

		public string TS103
		{
			get;
			set;
		}

		public string TS104
		{
			get;
			set;
		}

		public string TS105
		{
			get;
			set;
		}

		public string TS106
		{
			get;
			set;
		}

		public string TS107
		{
			get;
			set;
		}

		public string TS108
		{
			get;
			set;
		}

		public string TS109
		{
			get;
			set;
		}

		public string TS110
		{
			get;
			set;
		}

		public string TS111
		{
			get;
			set;
		}

		public string TS112
		{
			get;
			set;
		}

		public string TS113
		{
			get;
			set;
		}

		public string TS114
		{
			get;
			set;
		}

		public string TS115
		{
			get;
			set;
		}

		public string TS116
		{
			get;
			set;
		}

		public string TS117
		{
			get;
			set;
		}

		public string TS118
		{
			get;
			set;
		}

		public string TS119
		{
			get;
			set;
		}

		public string TS120
		{
			get;
			set;
		}

		public string TS121
		{
			get;
			set;
		}

		public string TS122
		{
			get;
			set;
		}

		public string TS123
		{
			get;
			set;
		}

		public string TS124
		{
			get;
			set;
		}

		public string TS125
		{
			get;
			set;
		}

		public string TS126
		{
			get;
			set;
		}

		public string TS127
		{
			get;
			set;
		}

		public string TS128
		{
			get;
			set;
		}

		public string TS129
		{
			get;
			set;
		}

		public string TS130
		{
			get;
			set;
		}

		public string TS131
		{
			get;
			set;
		}

		public string TS132
		{
			get;
			set;
		}

		public string TS133
		{
			get;
			set;
		}

		public string TS134
		{
			get;
			set;
		}

		public string TS135
		{
			get;
			set;
		}

		public string TS136
		{
			get;
			set;
		}

		public string TS137
		{
			get;
			set;
		}

		public string TS138
		{
			get;
			set;
		}

		public string TS139
		{
			get;
			set;
		}

		public string TS140
		{
			get;
			set;
		}

		public string TS141
		{
			get;
			set;
		}

		public string TS142
		{
			get;
			set;
		}

		public string TS143
		{
			get;
			set;
		}

		public string TS144
		{
			get;
			set;
		}

		public string TS145
		{
			get;
			set;
		}

		public string TS146
		{
			get;
			set;
		}

		public string TS147
		{
			get;
			set;
		}

		public string TS148
		{
			get;
			set;
		}

		public string TS149
		{
			get;
			set;
		}

		public string TS150
		{
			get;
			set;
		}
	}
}
