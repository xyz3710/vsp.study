﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbConnect.Models
{
	[Table("T_QA_YearPrint")]
	public class T_QA_YearPrint
	{
		[Key, Column("Years", TypeName = "char", Order = 1), StringLength(4)]
		public string Years
		{
			get;
			set;
		}

		[Key, Column("num", Order = 2)]
		public int num
		{
			get;
			set;
		}

		[Key, Column("seq", Order = 3)]
		public int seq
		{
			get;
			set;
		}

		[Column("reqstDT", TypeName = "varchar"), StringLength(10)]
		public string reqstDT
		{
			get;
			set;
		}

		[Column("batchDT", TypeName = "varchar"), StringLength(10)]
		public string batchDT
		{
			get;
			set;
		}

		[Column("testNo", TypeName = "varchar"), StringLength(15)]
		public string testNo
		{
			get;
			set;
		}

		[Column("batchNo", TypeName = "varchar"), StringLength(50)]
		public string batchNo
		{
			get;
			set;
		}

		[Column("itemCode", TypeName = "char"), StringLength(15)]
		public string itemCode
		{
			get;
			set;
		}

		[Column("procCode", TypeName = "char"), StringLength(3)]
		public string procCode
		{
			get;
			set;
		}

		[Column("TS01", TypeName = "varchar"), StringLength(256)]
		public string TS01
		{
			get;
			set;
		}

		[Column("TS02", TypeName = "varchar"), StringLength(256)]
		public string TS02
		{
			get;
			set;
		}

		[Column("TS03", TypeName = "varchar"), StringLength(256)]
		public string TS03
		{
			get;
			set;
		}

		[Column("TS04", TypeName = "varchar"), StringLength(256)]
		public string TS04
		{
			get;
			set;
		}

		[Column("TS05", TypeName = "varchar"), StringLength(256)]
		public string TS05
		{
			get;
			set;
		}

		[Column("TS06", TypeName = "varchar"), StringLength(256)]
		public string TS06
		{
			get;
			set;
		}

		[Column("TS07", TypeName = "varchar"), StringLength(256)]
		public string TS07
		{
			get;
			set;
		}

		[Column("TS08", TypeName = "varchar"), StringLength(256)]
		public string TS08
		{
			get;
			set;
		}

		[Column("TS09", TypeName = "varchar"), StringLength(256)]
		public string TS09
		{
			get;
			set;
		}

		[Column("TS10", TypeName = "varchar"), StringLength(256)]
		public string TS10
		{
			get;
			set;
		}

		[Column("TS11", TypeName = "varchar"), StringLength(256)]
		public string TS11
		{
			get;
			set;
		}

		[Column("TS12", TypeName = "varchar"), StringLength(256)]
		public string TS12
		{
			get;
			set;
		}

		[Column("TS13", TypeName = "varchar"), StringLength(256)]
		public string TS13
		{
			get;
			set;
		}

		[Column("TS14", TypeName = "varchar"), StringLength(256)]
		public string TS14
		{
			get;
			set;
		}

		[Column("TS15", TypeName = "varchar"), StringLength(256)]
		public string TS15
		{
			get;
			set;
		}

		[Column("TS16", TypeName = "varchar"), StringLength(256)]
		public string TS16
		{
			get;
			set;
		}

		[Column("TS17", TypeName = "varchar"), StringLength(256)]
		public string TS17
		{
			get;
			set;
		}

		[Column("TS18", TypeName = "varchar"), StringLength(256)]
		public string TS18
		{
			get;
			set;
		}

		[Column("TS19", TypeName = "varchar"), StringLength(256)]
		public string TS19
		{
			get;
			set;
		}

		[Column("TS20", TypeName = "varchar"), StringLength(256)]
		public string TS20
		{
			get;
			set;
		}

		[Column("TS21", TypeName = "varchar"), StringLength(256)]
		public string TS21
		{
			get;
			set;
		}

		[Column("TS22", TypeName = "varchar"), StringLength(256)]
		public string TS22
		{
			get;
			set;
		}

		[Column("TS23", TypeName = "varchar"), StringLength(256)]
		public string TS23
		{
			get;
			set;
		}

		[Column("TS24", TypeName = "varchar"), StringLength(256)]
		public string TS24
		{
			get;
			set;
		}

		[Column("TS25", TypeName = "varchar"), StringLength(256)]
		public string TS25
		{
			get;
			set;
		}

		[Column("TS26", TypeName = "varchar"), StringLength(256)]
		public string TS26
		{
			get;
			set;
		}

		[Column("TS27", TypeName = "varchar"), StringLength(256)]
		public string TS27
		{
			get;
			set;
		}

		[Column("TS28", TypeName = "varchar"), StringLength(256)]
		public string TS28
		{
			get;
			set;
		}

		[Column("TS29", TypeName = "varchar"), StringLength(256)]
		public string TS29
		{
			get;
			set;
		}

		[Column("TS30", TypeName = "varchar"), StringLength(256)]
		public string TS30
		{
			get;
			set;
		}

		[Column("TS31", TypeName = "varchar"), StringLength(256)]
		public string TS31
		{
			get;
			set;
		}

		[Column("TS32", TypeName = "varchar"), StringLength(256)]
		public string TS32
		{
			get;
			set;
		}

		[Column("TS33", TypeName = "varchar"), StringLength(256)]
		public string TS33
		{
			get;
			set;
		}

		[Column("TS34", TypeName = "varchar"), StringLength(256)]
		public string TS34
		{
			get;
			set;
		}

		[Column("TS35", TypeName = "varchar"), StringLength(256)]
		public string TS35
		{
			get;
			set;
		}

		[Column("TS36", TypeName = "varchar"), StringLength(256)]
		public string TS36
		{
			get;
			set;
		}

		[Column("TS37", TypeName = "varchar"), StringLength(256)]
		public string TS37
		{
			get;
			set;
		}

		[Column("TS38", TypeName = "varchar"), StringLength(256)]
		public string TS38
		{
			get;
			set;
		}

		[Column("TS39", TypeName = "varchar"), StringLength(256)]
		public string TS39
		{
			get;
			set;
		}

		[Column("TS40", TypeName = "varchar"), StringLength(256)]
		public string TS40
		{
			get;
			set;
		}

		[Column("TS41", TypeName = "varchar"), StringLength(256)]
		public string TS41
		{
			get;
			set;
		}

		[Column("TS42", TypeName = "varchar"), StringLength(256)]
		public string TS42
		{
			get;
			set;
		}

		[Column("TS43", TypeName = "varchar"), StringLength(256)]
		public string TS43
		{
			get;
			set;
		}

		[Column("TS44", TypeName = "varchar"), StringLength(256)]
		public string TS44
		{
			get;
			set;
		}

		[Column("TS45", TypeName = "varchar"), StringLength(256)]
		public string TS45
		{
			get;
			set;
		}

		[Column("TS46", TypeName = "varchar"), StringLength(256)]
		public string TS46
		{
			get;
			set;
		}

		[Column("TS47", TypeName = "varchar"), StringLength(256)]
		public string TS47
		{
			get;
			set;
		}

		[Column("TS48", TypeName = "varchar"), StringLength(256)]
		public string TS48
		{
			get;
			set;
		}

		[Column("TS49", TypeName = "varchar"), StringLength(256)]
		public string TS49
		{
			get;
			set;
		}

		[Column("TS50", TypeName = "varchar"), StringLength(256)]
		public string TS50
		{
			get;
			set;
		}

		[Column("TS51", TypeName = "varchar"), StringLength(256)]
		public string TS51
		{
			get;
			set;
		}

		[Column("TS52", TypeName = "varchar"), StringLength(256)]
		public string TS52
		{
			get;
			set;
		}

		[Column("TS53", TypeName = "varchar"), StringLength(256)]
		public string TS53
		{
			get;
			set;
		}

		[Column("TS54", TypeName = "varchar"), StringLength(256)]
		public string TS54
		{
			get;
			set;
		}

		[Column("TS55", TypeName = "varchar"), StringLength(256)]
		public string TS55
		{
			get;
			set;
		}

		[Column("TS56", TypeName = "varchar"), StringLength(256)]
		public string TS56
		{
			get;
			set;
		}

		[Column("TS57", TypeName = "varchar"), StringLength(256)]
		public string TS57
		{
			get;
			set;
		}

		[Column("TS58", TypeName = "varchar"), StringLength(256)]
		public string TS58
		{
			get;
			set;
		}

		[Column("TS59", TypeName = "varchar"), StringLength(256)]
		public string TS59
		{
			get;
			set;
		}

		[Column("TS60", TypeName = "varchar"), StringLength(256)]
		public string TS60
		{
			get;
			set;
		}

		[Column("TS61", TypeName = "varchar"), StringLength(256)]
		public string TS61
		{
			get;
			set;
		}

		[Column("TS62", TypeName = "varchar"), StringLength(256)]
		public string TS62
		{
			get;
			set;
		}

		[Column("TS63", TypeName = "varchar"), StringLength(256)]
		public string TS63
		{
			get;
			set;
		}

		[Column("TS64", TypeName = "varchar"), StringLength(256)]
		public string TS64
		{
			get;
			set;
		}

		[Column("TS65", TypeName = "varchar"), StringLength(256)]
		public string TS65
		{
			get;
			set;
		}

		[Column("TS66", TypeName = "varchar"), StringLength(256)]
		public string TS66
		{
			get;
			set;
		}

		[Column("TS67", TypeName = "varchar"), StringLength(256)]
		public string TS67
		{
			get;
			set;
		}

		[Column("TS68", TypeName = "varchar"), StringLength(256)]
		public string TS68
		{
			get;
			set;
		}

		[Column("TS69", TypeName = "varchar"), StringLength(256)]
		public string TS69
		{
			get;
			set;
		}

		[Column("TS70", TypeName = "varchar"), StringLength(256)]
		public string TS70
		{
			get;
			set;
		}

		[Column("TS71", TypeName = "varchar"), StringLength(256)]
		public string TS71
		{
			get;
			set;
		}

		[Column("TS72", TypeName = "varchar"), StringLength(256)]
		public string TS72
		{
			get;
			set;
		}

		[Column("TS73", TypeName = "varchar"), StringLength(256)]
		public string TS73
		{
			get;
			set;
		}

		[Column("TS74", TypeName = "varchar"), StringLength(256)]
		public string TS74
		{
			get;
			set;
		}

		[Column("TS75", TypeName = "varchar"), StringLength(256)]
		public string TS75
		{
			get;
			set;
		}

		[Column("TS76", TypeName = "varchar"), StringLength(256)]
		public string TS76
		{
			get;
			set;
		}

		[Column("TS77", TypeName = "varchar"), StringLength(256)]
		public string TS77
		{
			get;
			set;
		}

		[Column("TS78", TypeName = "varchar"), StringLength(256)]
		public string TS78
		{
			get;
			set;
		}

		[Column("TS79", TypeName = "varchar"), StringLength(256)]
		public string TS79
		{
			get;
			set;
		}

		[Column("TS80", TypeName = "varchar"), StringLength(256)]
		public string TS80
		{
			get;
			set;
		}

		[Column("TS81", TypeName = "varchar"), StringLength(256)]
		public string TS81
		{
			get;
			set;
		}

		[Column("TS82", TypeName = "varchar"), StringLength(256)]
		public string TS82
		{
			get;
			set;
		}

		[Column("TS83", TypeName = "varchar"), StringLength(256)]
		public string TS83
		{
			get;
			set;
		}

		[Column("TS84", TypeName = "varchar"), StringLength(256)]
		public string TS84
		{
			get;
			set;
		}

		[Column("TS85", TypeName = "varchar"), StringLength(256)]
		public string TS85
		{
			get;
			set;
		}

		[Column("TS86", TypeName = "varchar"), StringLength(256)]
		public string TS86
		{
			get;
			set;
		}

		[Column("TS87", TypeName = "varchar"), StringLength(256)]
		public string TS87
		{
			get;
			set;
		}

		[Column("TS88", TypeName = "varchar"), StringLength(256)]
		public string TS88
		{
			get;
			set;
		}

		[Column("TS89", TypeName = "varchar"), StringLength(256)]
		public string TS89
		{
			get;
			set;
		}

		[Column("TS90", TypeName = "varchar"), StringLength(256)]
		public string TS90
		{
			get;
			set;
		}

		[Column("TS91", TypeName = "varchar"), StringLength(256)]
		public string TS91
		{
			get;
			set;
		}

		[Column("TS92", TypeName = "varchar"), StringLength(256)]
		public string TS92
		{
			get;
			set;
		}

		[Column("TS93", TypeName = "varchar"), StringLength(256)]
		public string TS93
		{
			get;
			set;
		}

		[Column("TS94", TypeName = "varchar"), StringLength(256)]
		public string TS94
		{
			get;
			set;
		}

		[Column("TS95", TypeName = "varchar"), StringLength(256)]
		public string TS95
		{
			get;
			set;
		}

		[Column("TS96", TypeName = "varchar"), StringLength(256)]
		public string TS96
		{
			get;
			set;
		}

		[Column("TS97", TypeName = "varchar"), StringLength(256)]
		public string TS97
		{
			get;
			set;
		}

		[Column("TS98", TypeName = "varchar"), StringLength(256)]
		public string TS98
		{
			get;
			set;
		}

		[Column("TS99", TypeName = "varchar"), StringLength(256)]
		public string TS99
		{
			get;
			set;
		}

		[Column("TS100", TypeName = "varchar"), StringLength(256)]
		public string TS100
		{
			get;
			set;
		}

		[Column("TS101", TypeName = "varchar"), StringLength(256)]
		public string TS101
		{
			get;
			set;
		}

		[Column("TS102", TypeName = "varchar"), StringLength(256)]
		public string TS102
		{
			get;
			set;
		}

		[Column("TS103", TypeName = "varchar"), StringLength(256)]
		public string TS103
		{
			get;
			set;
		}

		[Column("TS104", TypeName = "varchar"), StringLength(256)]
		public string TS104
		{
			get;
			set;
		}

		[Column("TS105", TypeName = "varchar"), StringLength(256)]
		public string TS105
		{
			get;
			set;
		}

		[Column("TS106", TypeName = "varchar"), StringLength(256)]
		public string TS106
		{
			get;
			set;
		}

		[Column("TS107", TypeName = "varchar"), StringLength(256)]
		public string TS107
		{
			get;
			set;
		}

		[Column("TS108", TypeName = "varchar"), StringLength(256)]
		public string TS108
		{
			get;
			set;
		}

		[Column("TS109", TypeName = "varchar"), StringLength(256)]
		public string TS109
		{
			get;
			set;
		}

		[Column("TS110", TypeName = "varchar"), StringLength(256)]
		public string TS110
		{
			get;
			set;
		}

		[Column("TS111", TypeName = "varchar"), StringLength(256)]
		public string TS111
		{
			get;
			set;
		}

		[Column("TS112", TypeName = "varchar"), StringLength(256)]
		public string TS112
		{
			get;
			set;
		}

		[Column("TS113", TypeName = "varchar"), StringLength(256)]
		public string TS113
		{
			get;
			set;
		}

		[Column("TS114", TypeName = "varchar"), StringLength(256)]
		public string TS114
		{
			get;
			set;
		}

		[Column("TS115", TypeName = "varchar"), StringLength(256)]
		public string TS115
		{
			get;
			set;
		}

		[Column("TS116", TypeName = "varchar"), StringLength(256)]
		public string TS116
		{
			get;
			set;
		}

		[Column("TS117", TypeName = "varchar"), StringLength(256)]
		public string TS117
		{
			get;
			set;
		}

		[Column("TS118", TypeName = "varchar"), StringLength(256)]
		public string TS118
		{
			get;
			set;
		}

		[Column("TS119", TypeName = "varchar"), StringLength(256)]
		public string TS119
		{
			get;
			set;
		}

		[Column("TS120", TypeName = "varchar"), StringLength(256)]
		public string TS120
		{
			get;
			set;
		}

		[Column("TS121", TypeName = "varchar"), StringLength(256)]
		public string TS121
		{
			get;
			set;
		}

		[Column("TS122", TypeName = "varchar"), StringLength(256)]
		public string TS122
		{
			get;
			set;
		}

		[Column("TS123", TypeName = "varchar"), StringLength(256)]
		public string TS123
		{
			get;
			set;
		}

		[Column("TS124", TypeName = "varchar"), StringLength(256)]
		public string TS124
		{
			get;
			set;
		}

		[Column("TS125", TypeName = "varchar"), StringLength(256)]
		public string TS125
		{
			get;
			set;
		}

		[Column("TS126", TypeName = "varchar"), StringLength(256)]
		public string TS126
		{
			get;
			set;
		}

		[Column("TS127", TypeName = "varchar"), StringLength(256)]
		public string TS127
		{
			get;
			set;
		}

		[Column("TS128", TypeName = "varchar"), StringLength(256)]
		public string TS128
		{
			get;
			set;
		}

		[Column("TS129", TypeName = "varchar"), StringLength(256)]
		public string TS129
		{
			get;
			set;
		}

		[Column("TS130", TypeName = "varchar"), StringLength(256)]
		public string TS130
		{
			get;
			set;
		}

		[Column("TS131", TypeName = "varchar"), StringLength(256)]
		public string TS131
		{
			get;
			set;
		}

		[Column("TS132", TypeName = "varchar"), StringLength(256)]
		public string TS132
		{
			get;
			set;
		}

		[Column("TS133", TypeName = "varchar"), StringLength(256)]
		public string TS133
		{
			get;
			set;
		}

		[Column("TS134", TypeName = "varchar"), StringLength(256)]
		public string TS134
		{
			get;
			set;
		}

		[Column("TS135", TypeName = "varchar"), StringLength(256)]
		public string TS135
		{
			get;
			set;
		}

		[Column("TS136", TypeName = "varchar"), StringLength(256)]
		public string TS136
		{
			get;
			set;
		}

		[Column("TS137", TypeName = "varchar"), StringLength(256)]
		public string TS137
		{
			get;
			set;
		}

		[Column("TS138", TypeName = "varchar"), StringLength(256)]
		public string TS138
		{
			get;
			set;
		}

		[Column("TS139", TypeName = "varchar"), StringLength(256)]
		public string TS139
		{
			get;
			set;
		}

		[Column("TS140", TypeName = "varchar"), StringLength(256)]
		public string TS140
		{
			get;
			set;
		}

		[Column("TS141", TypeName = "varchar"), StringLength(256)]
		public string TS141
		{
			get;
			set;
		}

		[Column("TS142", TypeName = "varchar"), StringLength(256)]
		public string TS142
		{
			get;
			set;
		}

		[Column("TS143", TypeName = "varchar"), StringLength(256)]
		public string TS143
		{
			get;
			set;
		}

		[Column("TS144", TypeName = "varchar"), StringLength(256)]
		public string TS144
		{
			get;
			set;
		}

		[Column("TS145", TypeName = "varchar"), StringLength(256)]
		public string TS145
		{
			get;
			set;
		}

		[Column("TS146", TypeName = "varchar"), StringLength(256)]
		public string TS146
		{
			get;
			set;
		}

		[Column("TS147", TypeName = "varchar"), StringLength(256)]
		public string TS147
		{
			get;
			set;
		}

		[Column("TS148", TypeName = "varchar"), StringLength(256)]
		public string TS148
		{
			get;
			set;
		}

		[Column("TS149", TypeName = "varchar"), StringLength(256)]
		public string TS149
		{
			get;
			set;
		}

		[Column("TS150", TypeName = "varchar"), StringLength(256)]
		public string TS150
		{
			get;
			set;
		}
	}
}
