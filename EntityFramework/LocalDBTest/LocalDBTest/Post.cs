using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDBTest
{
	public class Post
	{
		public int PostId
		{
			get;
			set;
		}
		public string Title
		{
			get;
			set;
		}
		public string Content
		{
			get;
			set;
		}

		public int BlogId
		{
			get;
			set;
		}
		public virtual Blog Blog
		{
			get;
			set;
		}
	}
}

