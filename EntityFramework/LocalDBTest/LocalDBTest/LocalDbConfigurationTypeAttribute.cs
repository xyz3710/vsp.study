﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.Entity
{
	/// <summary>
	/// Class LocalDbConfigurationTypeAttribute.
	/// </summary>
	public class LocalDbConfigurationTypeAttribute : DbConfigurationTypeAttribute
	{
		/// <summary>
		/// Initializes a new instance of the LocalDbConfigurationTypeAttribute class.
		/// </summary>
		public LocalDbConfigurationTypeAttribute(string localDbVersion, string databaseName, string dataFileName)
			: base(typeof(LocalDbConfiguration))
		{
			LocalDbVersion = localDbVersion;
			DatabaseName = databaseName;
			DataFileName = dataFileName;
		}

		/// <summary>
		/// LocalDbVersion를 구하거나 설정합니다.(v11.0 형식)
		/// </summary>
		/// <value>LocalDbVersion을 반환합니다.</value>
		public string LocalDbVersion
		{
			get;
			set;
		}

		/// <summary>
		/// DatabaseName를 구하거나 설정합니다.
		/// </summary>
		/// <value>DatabaseName을 반환합니다.</value>
		public string DatabaseName
		{
			get;
			set;
		}

		/// <summary>
		/// DataFileName를 구하거나 설정합니다.(확장자 .mdf 포함)
		/// </summary>
		/// <remarks>|TargetDir|을 포함할 경우 프로젝트 output 경로로 변경됩니다.</remarks>
		/// <value>DataFileName을 반환합니다.</value>
		public string DataFileName
		{
			get;
			set;
		}
	}
}
