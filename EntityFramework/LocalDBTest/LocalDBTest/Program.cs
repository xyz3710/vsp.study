﻿// ****************************************************************************************************************** //
//	Domain		:	LocalDBTest.Program
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 3월 10일 월요일 오후 4:22
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	db file을 attach할 수 없다고 할 경우 아래와 같이 해결 한다.
//					Cannot attach the file ‘{0}' as database '{1}'
//					http://www.dotnetexamples.com/2013/05/cannot-attach-mdf-file-as-database.html
//						sqllocaldb.exe stop v11.0
//						sqllocaldb.exe delete v11.0
//						
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="program.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDBTest
{
	class Program
	{
		static void Main(string[] args)
		{
			//DbConfiguration.SetConfiguration(new BlogginDbConfiguration());
			// 아래와 같이 사용할 경우에는 DbConfigurationType을 상속 받은 클래스가 해당 어셈블리 내에서 1개만 존재해야함
			// 그렇지 않을 경우 LocalDbConfiguration를 상속 받은 클래스를
			// DbContext 개체에 DbConfigurationType(typeof(~~~DbConfiguration))] 선언해야 한다.
			DbConfiguration.SetConfiguration(new LocalDbConfiguration("Blog", @"|TargetDir|\Data\BlogDb", "v11.0"));

			using (var db = new BloggingContext())
			{
				// Display all Blogs from the database 
				DisplayBlogs(db);

				// Create and save a new Blog 
				Console.Write("Enter a name for a new Blog: ");
				var name = Console.ReadLine();

				var blog = new Blog
				{
					Name = name
				};
				db.Blogs.Add(blog);
				db.SaveChanges();

				DisplayBlogs(db);

				Console.WriteLine("Press any key to exit...");
				Console.ReadKey();
			}
		}
		private static void DisplayBlogs(BloggingContext db)
		{
			var query = from b in db.Blogs
						orderby b.Name
						select b;

			Console.WriteLine("All blogs in the database:");
			foreach (var item in query)
			{
				Console.WriteLine(item.Name);
			}
		}
	}
}
