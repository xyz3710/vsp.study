// ****************************************************************************************************************** //
//	Domain		:	
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 3월 10일 월요일 오후 12:27
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://msdn.microsoft.com/en-us/data/jj680699
//					http://stackoverflow.com/questions/20354083/ef6-and-multiple-configurations-sql-server-and-sql-server-compact
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="BloggingContext.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LocalDBTest
{
	//public class BlogginDbConfiguration : DbConfiguration
	//{
	//	public BlogginDbConfiguration()
	//	{
	//		string mdfFilename = "Blog.mdf";
	//		string outputFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
	//		string attachDbFilename = Path.Combine(outputFolder, mdfFilename);
	//		string connectionString = string.Format(@"Data Source=(LocalDB)\v11.0;Initial Catalog=Blog;AttachDbFilename=""{0}"";Integrated Security=True", attachDbFilename);

	//		SetDefaultConnectionFactory(new LocalDbConnectionFactory("v11.0", connectionString));
	//	}
	//}

	//[DbConfigurationType(typeof(BlogginDbConfiguration))]
	public class BloggingContext : DbContext
	{
		/// <summary>
		/// Initializes a new instance of the BloggingContext class.
		/// </summary>
		public BloggingContext()
		//: base(GetLocalDbConnection("Blogging", "Blog.mdf"), true)
		{
			Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BloggingContext>());
		}

		/// <summary>
		/// Gets the LocalDb database connection.
		/// </summary>
		/// <param name="databaseName">Name of the database.</param>
		/// <param name="databaseFileName">Name of the database file. With extension(.mdf) and local execution path</param>
		/// <param name="dataSource">The data source.</param>
		/// <returns>DbConnection.</returns>
		public static DbConnection GetLocalDbConnection(string databaseName, string databaseFileName, string dataSource = "v11.0")
		{
			if (databaseFileName.Contains(".mdf") == false)
			{
				databaseFileName = databaseFileName + ".mdf";
			}

			string outputFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
			string attachDbFilename = Path.Combine(outputFolder, databaseName);
			string connectionString = string.Format(@"Initial Catalog={0};AttachDbFilename=""{1}"";Integrated Security=True", databaseName, attachDbFilename);
			var factory = new LocalDbConnectionFactory(dataSource, connectionString);

			return factory.CreateConnection(databaseName + "Context");

			//var sqlConnStringBuilder = new SqlConnectionStringBuilder();
			//sqlConnStringBuilder.DataSource = "(localdb)\v11.0";
			//sqlConnStringBuilder.IntegratedSecurity = true;
			//sqlConnStringBuilder.MultipleActiveResultSets = true;

			//var sqlConnFact = new SqlConnectionFactory(sqlConnStringBuilder.ConnectionString);
			//var sqlConn = sqlConnFact.CreateConnection(dbName);
			//return sqlConn;
		}
		public static string ConnectionString
		{
			get
			{
				string mdfFilename = "Blog.mdf";
				string outputFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				string attachDbFilename = Path.Combine(outputFolder, mdfFilename);
				string connectionString = string.Format(@"Data Source=(LocalDB)\v11.0;Initial Catalog=Blog;AttachDbFilename=""{0}"";Integrated Security=True", attachDbFilename);

				SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

				//builder.IntegratedSecurity = true;
				//builder.InitialCatalog = "Blog";
				//builder.AttachDBFilename = Path.Combine(@"E:\BlogDb.mdf");

				return builder.ConnectionString;
			}
		}

		public DbSet<Blog> Blogs
		{
			get;
			set;
		}
		public DbSet<Post> Posts
		{
			get;
			set;
		}
	}
}

