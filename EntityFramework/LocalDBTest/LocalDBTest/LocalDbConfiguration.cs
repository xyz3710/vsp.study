// ****************************************************************************************************************** //
//	Domain		:	System.Data.Entity.LocalDbConfiguration
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 3월 10일 월요일 오후 4:22
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	db file을 attach할 수 없다고 할 경우 아래와 같이 해결 한다.
//						Cannot attach the file ‘{0}' as database '{1}'
//						http://www.dotnetexamples.com/2013/05/cannot-attach-mdf-file-as-database.html
//							sqllocaldb.exe stop v11.0
//							sqllocaldb.exe delete v11.0
//					DbConfiguration.SetConfiguration(new LocalDbConfiguration("Blog", @"|TargetDir|\Data\BlogDb", "v11.0"));
//						위와 같이 사용할 경우에는 DbConfigurationType을 상속 받은 클래스가 해당 어셈블리 내에서 1개만 존재해야함
//						그렇지 않을 경우 LocalDbConfiguration를 상속 받은 클래스를
//						DbContext 개체에 DbConfigurationType(typeof(~~~DbConfiguration))] 선언해야 한다.
//					
//
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="LocalDbConfiguration.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace System.Data.Entity
{
	/// <summary>
	/// <seealso cref="DbContext"/>를 상속받은 DbContext 개체에 LocalDb를 사용하는 <seealso cref="DbConfiguration"/> 의 기능을 구현합니다.
	/// </summary>
	public class LocalDbConfiguration : DbConfiguration
	{
		#region Constants
		private const string TARGET_DIR_TOKEN = "|TargetDir|";
		private const string DATA_FILE_EXTENSION = ".mdf";
		#endregion

		/// <summary>
		/// Initializes a new instance of the LocalDbConfiguration class.
		/// </summary>
		/// <param name="databaseName">database 이름.</param>
		/// <param name="dataFileName">DataFileName를 구하거나 설정합니다.(확장자 .mdf 포함, |TargetDir|을 포함할 경우 프로젝트 output 경로로 변경)</param>
		/// <param name="localDbVersion">LocalDb version.</param>
		public LocalDbConfiguration(string databaseName, string dataFileName = "", string localDbVersion = "v11.0")
		{
			localDbVersion = CheckLocalDbVersion(localDbVersion);
			dataFileName = CheckDataFileExtension(dataFileName);

			string connectionString = string.Empty;

			if (string.IsNullOrEmpty(dataFileName) == true)
			{
				connectionString = string.Format(
									   @"Data Source=(LocalDB)\{0};Initial Catalog={1};Integrated Security=True",
									   localDbVersion,
									   databaseName);
			}
			else
			{
				if (dataFileName.Contains(TARGET_DIR_TOKEN) == true)
				{
					string outputFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
					dataFileName = dataFileName.Replace(TARGET_DIR_TOKEN, outputFolder);
					string dataFilePath = Path.GetDirectoryName(dataFileName);

					if (Directory.Exists(dataFilePath) == false)
					{
						Directory.CreateDirectory(dataFilePath);
					}
				}

				connectionString = string.Format(
									   @"Data Source=(LocalDB)\{0};Initial Catalog={1};Integrated Security=True;AttachDbFilename=""{2}""",
									   localDbVersion,
									   databaseName,
									   dataFileName);
			}

			SetDefaultConnectionFactory(new LocalDbConnectionFactory(localDbVersion, connectionString));
			LocalDbVersion = localDbVersion;
			DatabaseName = databaseName;
			DataFileName = dataFileName;
		}

		/// <summary>
		/// LocalDbVersion를 구하거나 설정합니다.(v11.0 형식)
		/// </summary>
		/// <value>LocalDbVersion을 반환합니다.</value>
		public string LocalDbVersion
		{
			get;
			set;
		}

		/// <summary>
		/// DatabaseName를 구하거나 설정합니다.
		/// </summary>
		/// <value>DatabaseName을 반환합니다.</value>
		public string DatabaseName
		{
			get;
			set;
		}

		/// <summary>
		/// DataFileName를 구하거나 설정합니다.(확장자 .mdf 포함)
		/// </summary>
		/// <remarks>|TargetDir|을 포함할 경우 프로젝트 output 경로로 변경됩니다.</remarks>
		/// <value>DataFileName을 반환합니다.</value>
		public string DataFileName
		{
			get;
			set;
		}

		private string CheckLocalDbVersion(string localDbVersion)
		{
			string result = "v11.0";

			if (string.IsNullOrEmpty(localDbVersion) == false)
			{
				if (localDbVersion.Contains("v") == false ||
					localDbVersion.Contains(".") == false)
				{
					throw new ArgumentException("LocalDb version must contains 'v', or '.' like v11.0.");
				}
			}

			return result;
		}

		private string CheckDataFileExtension(string dataFileName)
		{
			string result = string.Empty;

			if (dataFileName.Contains(DATA_FILE_EXTENSION) == false)
			{
				result = string.Format("{0}{1}", dataFileName, DATA_FILE_EXTENSION);
			}

			return result;
		}
	}
}

