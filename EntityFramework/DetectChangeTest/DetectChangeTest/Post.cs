using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DetectChangeTest
{
	public class Post
	{
		public int Id
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public string Content
		{
			get;
			set;
		}

		public int BlodId
		{
			get;
			set;
		}

		public virtual Blog Blog
		{
			get;
			set;
		}
	}
}
