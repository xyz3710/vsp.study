﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DetectChangeTest
{
	public class BlogContext : DbContext
	{
		/// <summary>
		/// Blog를 구하거나 설정합니다.
		/// </summary>
		/// <value>Blog를 반환합니다.</value>
		public DbSet<Blog> Blogs
		{
			get;
			set;
		}

		/// <summary>
		/// Post를 구하거나 설정합니다.
		/// </summary>
		/// <value>Post를 반환합니다.</value>
		public DbSet<Post> Posts
		{
			get;
			set;
		}
	}
}
