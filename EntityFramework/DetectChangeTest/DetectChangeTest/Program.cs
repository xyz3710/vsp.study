﻿#define PERFORMANCE_TEST

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DetectChangeTest
{
	class Program
	{
		static void Main(string[] args)
		{
			#region PERFORMANCE TEST START 104220
#if PERFORMANCE_TEST
			DateTime performanceTestStart104220 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart104220), "Program.Main");
#endif
			#endregion
			using (var db = new BlogContext())
			{
				#region PERFORMANCE TEST STOP 104220
#if PERFORMANCE_TEST
				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart104220), "Program.Main");
#endif
				#endregion
				//Blog firstBlog = new Blog
				//{
				//	Title = "첫 블로그",
				//};
				//db.Blogs.Add(firstBlog);
				//db.SaveChanges();

				int blogId = 1;//				firstBlog.Id;
				#region PERFORMANCE TEST START 104256
#if PERFORMANCE_TEST
				DateTime performanceTestStart104256 = DateTime.Now;
				Console.WriteLine(string.Format("Start\t{0}", performanceTestStart104256), "Program.Main");
#endif
				#endregion
				var posts = GetBulkPosts(blogId);
				#region PERFORMANCE TEST STOP 104256
#if PERFORMANCE_TEST
				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart104256), "Program.Main");
#endif
				#endregion

				db.Configuration.AutoDetectChangesEnabled = false;
				db.Configuration.ProxyCreationEnabled = false;
				db.Configuration.ValidateOnSaveEnabled = false;
				Console.WriteLine("Before Count: {0:#,##0}", db.Posts.Count());

				#region PERFORMANCE TEST START 104358
#if PERFORMANCE_TEST
				DateTime performanceTestStart104358 = DateTime.Now;
				Console.WriteLine(string.Format("Start\t{0}", performanceTestStart104358), "Program.Main");
#endif
				#endregion
				//db.Posts.AddRange(posts);
				posts.ForEach(post => db.Entry(post).State = System.Data.Entity.EntityState.Added);
				#region PERFORMANCE TEST STOP 104358
#if PERFORMANCE_TEST
				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart104358), "Program.Main");
#endif
				#endregion

				
				#region PERFORMANCE TEST START 172551
#if PERFORMANCE_TEST
				DateTime performanceTestStart172551 = DateTime.Now;
				Console.WriteLine(string.Format("Start\t{0}", performanceTestStart172551), "Program.Main");
#endif
				#endregion
				db.SaveChanges();
				#region PERFORMANCE TEST STOP 172551
#if PERFORMANCE_TEST
				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart172551), "Program.Main");
#endif
				Console.WriteLine("After  Count: {0:#,##0}", db.Posts.Select(x => x.Id).Count());
				#endregion
			}
		}

		private static List<Post> GetBulkPosts(int blogId)
		{
			const int dummyCount = 1000;

			return Enumerable.Range(1, dummyCount).Select(x => new Post
			{
				Title = string.Format("{0:00000}: {1}", x, DateTime.Now.ToString("yyyy-MM-dd hh:ss:mm:fff")),
				Content = string.Format("{0:00000} 번째 content", x),
			})
			.ToList();
		}
	}
}
