﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdfCodeFirstSample;

namespace EFFunctionTest
{
	public class Person
	{
		public int Id
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}
		public DateTime? BirthDate
		{
			get;
			set;
		}
	}

	public class TownContext : DbContext
	{
		public DbSet<Person> People
		{
			get;
			set;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Add(new GetAgeStoreFunctionInjectionConvention());
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			using (var db = new TownContext())
			{
				db.People.Add(new Person
				{
					Name = "Alfreds Futterkiste",
					BirthDate = new DateTime(1980, 1, 1)
				});
				db.People.Add(new Person
				{
					Name = "Alfreds Futterkiste",
					BirthDate = new DateTime(1970, 1, 1)
				});
				db.People.Add(new Person
				{
					Name = "Alfreds Futterkiste",
					BirthDate = new DateTime(1974, 1, 1)
				});
				db.SaveChanges();

				foreach (Person person in db.People)
				{
					Console.WriteLine("{0}: {1}", person.Name, Fun.GetAge(person.BirthDate));
				}
			}

		}
	}
}
