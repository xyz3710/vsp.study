using System.Data.Entity;
using TransactionScope01.Models;

namespace TransactionScope01
{
	public class SampleDataIfNewDatabaseCreated : DropCreateDatabaseIfModelChanges<ProductContext>
	{
		protected override void Seed(ProductContext context)
		{
			base.Seed(context);

			context.Categories.Add(new Category
			{
				Name = "Foods"
			});
			context.Categories.Add(new Category
			{
				Name = "Vegetables"
			});
			context.PriceTypes.Add(new PriceType
			{
				Name = "정가"
			});
			context.PriceTypes.Add(new PriceType
			{
				Name = "세일가"
			});
			context.Products.Add(new Product
			{
				Name = "빵",
				CategoryId = 1,
				PriceTypeId = 1
			});
			context.Products.Add(new Product
			{
				Name = "우유",
				CategoryId = 1,
				PriceTypeId = 2
			});
			context.Products.Add(new Product
			{
				Name = "상추",
				CategoryId = 2,
				PriceTypeId = 1
			});
			context.Products.Add(new Product
			{
				Name = "토마토",
				CategoryId = 2,
				PriceTypeId = 1
			});
			context.Images.Add(new Image
			{
				ImageUrl = "Images/01",
			});
			context.Images.Add(new Image
			{
				ImageUrl = "Images/02",
			});
			context.Product_Images.Add(new Product_Image
			{
				ProductId = 1,
				ImageId = 2
			});
			context.Product_Images.Add(new Product_Image
			{
				ProductId = 2,
				ImageId = 1
			});
			context.Product_Images.Add(new Product_Image
			{
				ProductId = 3,
				ImageId = 2
			});
			context.Product_Images.Add(new Product_Image
			{
				ProductId = 4,
				ImageId = 1
			});
		}
	}
}

