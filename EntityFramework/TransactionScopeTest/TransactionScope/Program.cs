﻿/**********************************************************************************************************************/
/*	Domain		:	TransactionScope.Program
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2011년 10월 8일 토요일 오전 8:37
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://blogs.msdn.com/b/adonet/archive/2011/03/15/ef-4-1-code-first-walkthrough.aspx 참고
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using TransactionScope01.Models;
using System.Data.Entity.Infrastructure;

namespace TransactionScope01
{
	class Program
	{
		static void Main(string[] args)
		{
			//if (Database.Exists("ProductContext") == true)
			//	Database.Delete("ProductContext");

			Database.SetInitializer<ProductContext>(new SampleDataIfNewDatabaseCreated());

			try
			{
				using (var db = new ProductContext())
				{
					if (db.Products.Count(p => p.Name == "호박") == 0)
					{
						db.Products.Add(new Product
						{
							CategoryId = 2,
							PriceTypeId = 2,
							Name = "호박"
						});

						int recordsAffected = db.SaveChanges();
					}

					// Query for all Food products using LINQ 
					var allFoods = db.Products.OrderBy(p => p.CategoryId).ThenBy(p => p.Name).ToList();

					Console.WriteLine("All foods in database:");

					foreach (var item in allFoods)
					{
						Console.WriteLine("{0} - \tC:{1}\tPT:{2}", item.Name, item.Category.Name, item.PriceType.Name);
					}
				}
			}
			catch (DbUpdateException ex)
			{
				Console.WriteLine(ex.Message);
			}
			catch (ModelValidationException ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
