﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TransactionScope01.Models
{
	public class Image
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int ID
		{
			get;
			set;
		}

		/// <summary>
		/// ImageUrl를 구하거나 설정합니다.
		/// </summary>
		public string ImageUrl
		{
			get;
			set;
		}

		public virtual Product_Image Product_Image
		{
			get;
			set;
		}
	}
}
