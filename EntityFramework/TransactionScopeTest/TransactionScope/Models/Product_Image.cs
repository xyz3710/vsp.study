using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TransactionScope01.Models
{
	public class Product_Image
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int ID
		{
			get;
			set;
		}
		/// <summary>
		/// ProductId를 구하거나 설정합니다.
		/// </summary>
		public int ProductId
		{
			get;
			set;
		}

		public int ImageId
		{
			get;
			set;
		}

		public virtual Product Product
		{
			get;
			set;
		}

		public virtual ICollection<Image> Images
		{
			get;
			set;
		}
	}
}

