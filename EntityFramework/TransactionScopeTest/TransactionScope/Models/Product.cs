using System.ComponentModel.DataAnnotations;
namespace TransactionScope01.Models
{
	public class Product
	{
		[Key]
		public int ID
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int CategoryId
		{
			get;
			set;
		}

		public int PriceTypeId
		{
			get;
			set;
		}

		[ForeignKey("CategoryId")]
		public virtual Category Category
		{
			get;
			set;
		}

		[ForeignKey("PriceTypeId")]
		public virtual PriceType PriceType
		{
			get;
			set;
		}
	}
}

