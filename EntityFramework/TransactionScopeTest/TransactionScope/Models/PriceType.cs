using System.ComponentModel.DataAnnotations;
namespace TransactionScope01.Models
{
	public class PriceType
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		[Key]
		public int ID
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}
	}
}

