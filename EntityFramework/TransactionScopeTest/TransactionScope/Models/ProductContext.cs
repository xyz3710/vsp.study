using System.Data.Entity;

namespace TransactionScope01.Models
{
	public class ProductContext : DbContext
	{
		public DbSet<Category> Categories
		{
			get;
			set;
		}

		public DbSet<Product> Products
		{
			get;
			set;
		}

		public DbSet<PriceType> PriceTypes
		{
			get;
			set;
		}

		public DbSet<Image> Images
		{
			get;
			set;
		}

		public DbSet<Product_Image> Product_Images
		{
			get;
			set;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}

