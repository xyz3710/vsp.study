﻿using GS.IPO.Dac.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.IPO.Dac.Entities
{
	/// <summary>
	/// 체결 정보
	/// </summary>
	/// <seealso cref="GS.IPO.Dac.Entities.Base.EntityBase" />
	[System.Diagnostics.DebuggerDisplay("RegDate:{RegDate}, SendExchangeOffice:{SendExchangeOffice}, ArrivalExchangeOffice:{ArrivalExchangeOffice}, BagSeq:{BagSeq}, RegId:{RegId}, MailWeight:{MailWeight}, FinalBag:{FinalBag}, DealState:{DealState}", Name = "ContractInfo")]
	[Table("PTIPT0024")]
	public class ContractInfo
	{
		/// <summary>
		/// 발송 등록 일자(yyyyMMdd)를 구하거나 설정합니다.
		/// </summary>
		/// <value>RegDate를 반환합니다.</value>
		[Column("SENDREGYMD", TypeName = "varchar"), MaxLength(8)]
		[Required]
		public string RegDate { get; set; }

		/// <summary>
		/// 발송 교환국 우체국 코드(SENDEXCHGPOCD)를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>SELIPO로 하드 코딩</remarks>
		/// <value>SendExchangeOffice를 반환합니다.</value>
		[Column("SENDEXCHGPOCD", TypeName = "varchar"), MaxLength(6)]
		[Required]
		public string SendExchangeOffice { get; set; }

		/// <summary>
		/// 도착 교환국 우체국 코드(ARRIVEXCHGPOCD)를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>SELIPO로 하드 코딩</remarks>
		/// <value>ArrivalExchangeOffice를 반환합니다.</value>
		[Column("ARRIVEXCHGPOCD", TypeName = "varchar"), MaxLength(6)]
		[Required]
		public string ArrivalExchangeOffice { get; set; }

		/// <summary>
		/// 자루 번호를 구하거나 설정합니다.
		/// </summary>
		/// <value>BagSeq를 반환합니다.</value>
		[Column("SACKSEQ", TypeName = "char"), MaxLength(4)]
		[Required]
		public string BagSeq { get; set; }

		/// <summary>
		/// 등기Id를 구하거나 설정합니다.
		/// </summary>
		/// <value>Id를 반환합니다.</vaue>
		[Key]
		[Column("MAILNO", TypeName = "varchar"), MaxLength(13)]
		public string RegId { get; set; }

		/// <summary>
		/// 우편물 중량를 구하거나 설정합니다.
		/// </summary>
		/// <value>MailWeight를 반환합니다.</value>
		[Column("MAILWGHT")]
		[Required]
		public int MailWeight { get; set; }

		/// <summary>
		/// 파이널 백 여부를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>Final bag일 경우에만 : F 하드 코딩</remarks>
		[Column("EVNTFLEX1", TypeName = "varchar"), MaxLength(1)]
		public string FinalBag { get; set; }

		/// <summary>
		/// IF_ID를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>IF-LPAPND-050-P02I 하드 코딩</remarks>
		/// <value>IF_ID를 반환합니다.</value>
		[Column("IF_ID", TypeName = "varchar"), MaxLength(20)]
		public string IF_ID { get; set; }

		/// <summary>
		/// INIT_TIME를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS')</remarks>
		[Column("INIT_TIME", TypeName = "varchar"), MaxLength(14)]
		public string INIT_TIME { get; set; }

		/// <summary>
		/// 체결 여부를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>'N' 하드 코딩</remarks>
		[Column("DEAL_STATE", TypeName = "char"), MaxLength(1)]
		public string DealState { get; set; }

		/// <summary>
		/// 업부 구분를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>'1' 하드 코딩</remarks>
		/// <value>WorkGBN를 반환합니다.</value>
		[Column("WRK_GBN", TypeName = "char"), MaxLength(1)]
		public string WorkGBN { get; set; }

	}
}
