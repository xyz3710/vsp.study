﻿using GS.IPO.Dac.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OracleWithoutClientEntityFramework
{
	public class SimContext : DbContext
	{
		/// <summary>
		/// ContractInfo를 구하거나 설정합니다.
		/// </summary>
		/// <value>ContractInfo를 반환합니다.</value>
		public DbSet<ContractInfo> ContractInfoes { get; set; }


	}
}
