﻿// ****************************************************************************************************************** //
//	Domain		:	
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2017년 12월 11일 월요일 18:05
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	affectedRow 파라미터를 이용한 stored procedure는 아래 참고
/*
PROMPT CREATE OR REPLACE PROCEDURE sp_insert440
CREATE OR REPLACE PROCEDURE sp_insert440
(
	startTXNID 	IN NUMBER
	,startDT 	IN VARCHAR2
	,endDT 		IN VARCHAR2
	,affectedRow OUT NUMBER
)

IS
	vSQLERRM	VARCHAR2(255);
BEGIN

affectedRow := 0;

-- step 1.
INSERT INTO ssm_pcgut0440
(
	tx_id
)
SELECT
	NULL AS TX_ID
	FROM statView s
;

affectedRow := SQL%ROWCOUNT;
DBMS_OUTPUT.PUT_LINE('Insert ssm_pcgut0440: ' || SQL%ROWCOUNT);

COMMIT;

EXCEPTION
--	WHEN NO_DATA_FOUND THEN
--		RETURN 0;
	WHEN OTHERS THEN
		affectedRow := -1;

		ROLLBACK;

		vSQLERRM := SUBSTR(SQLERRM, 10, 100);
DBMS_OUTPUT.PUT_LINE('ERROR: ORA' || SQLCODE || vSQLERRM);
END;

 */
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="OracleDbHelper.cs" company="(주)가치소프트">
//		Copyright (c) 2017. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;

namespace OracleWithoutClient
{
	/// <summary>
	/// Class DbHelper.
	/// </summary>
	public class OracleDbHelper
	{
		/// <summary>
		/// Connection info
		/// </summary>
		[System.Diagnostics.DebuggerDisplay("Host:{Host}, Port:{Port}, TNS:{TNS}, UserId:{UserId}, Password:{Password}", Name = "ConnectionInfo")]
		public class ConnectionInfo
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="ConnectionInfo"/> class.
			/// </summary>
			/// <param name="host">The host.</param>
			/// <param name="port">The port.</param>
			/// <param name="tns">The TNS.</param>
			/// <param name="userId">The user identifier.</param>
			/// <param name="password">The password.</param>
			public ConnectionInfo(string host, string port, string tns, string userId, string password)
			{
				Host = host;
				Port = port;
				TNS = tns;
				UserId = userId;
				Password = password;
			}

			/// <summary>
			/// Gets the host.
			/// </summary>
			public string Host { get; private set; }

			/// <summary>
			/// Gets the port.
			/// </summary>
			public string Port { get; private set; }

			/// <summary>
			/// Gets the TNS.
			/// </summary>
			public string TNS { get; private set; }

			/// <summary>
			/// Gets the user identifier.
			/// </summary>
			public string UserId { get; private set; }
			/// <summary>
			/// Gets the password.
			/// </summary>
			public string Password { get; private set; }
		}

		#region Constants
		private const string OUT_PARAM_NAME = "affectedRow";
		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="OracleDbHelper" /> class.
		/// </summary>
		/// <param name="connectionInfo">The connection information.</param>
		public OracleDbHelper(ConnectionInfo connectionInfo)
			: this(connectionInfo.Host, connectionInfo.Port, connectionInfo.TNS, connectionInfo.UserId, connectionInfo.Password)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="OracleDbHelper"/> class.
		/// </summary>
		/// <param name="host">The host.</param>
		/// <param name="port">The port.</param>
		/// <param name="tns">The tns.</param>
		/// <param name="userId">The user identifier.</param>
		/// <param name="password">The password.</param>
		public OracleDbHelper(string host,
			string port,
			string tns,
			string userId,
			string password)
		{
			Host = host;
			Port = port;
			TNS = tns;
			UserId = userId;
			Password = password;
			ConnectionString = GetConnectionString(host, port, tns, userId, password);
		}

		/// <summary>
		/// Gets the host.
		/// </summary>
		public string Host { get; private set; }

		/// <summary>
		/// Gets the port.
		/// </summary>
		public string Port { get; private set; }

		/// <summary>
		/// Gets the TNS.
		/// </summary>
		public string TNS { get; private set; }

		/// <summary>
		/// Gets the user identifier.
		/// </summary>
		public string UserId { get; private set; }
		/// <summary>
		/// Gets the password.
		/// </summary>
		public string Password { get; private set; }

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		public string ConnectionString { get; private set; }

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		/// <param name="host">The host.</param>
		/// <param name="port">The port.</param>
		/// <param name="tns">The TNS.</param>
		/// <param name="userId">The user identifier.</param>
		/// <param name="password">The password.</param>
		/// <returns></returns>
		public static string GetConnectionString(string host,
			string port,
			string tns,
			string userId,
			string password)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("Data Source=(DESCRIPTION=");
			sb.AppendFormat("(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})))", host, port);
			sb.AppendFormat("(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={0})));", tns);
			sb.AppendFormat("User Id={0};Password={1}", userId, password);

			return sb.ToString();
		}

		/// <summary>
		/// Gets the connection string.
		/// </summary>
		/// <param name="connectionInfo">The connection information.</param>
		/// <returns></returns>
		public static string GetConnectionString(ConnectionInfo connectionInfo)
		{
			return GetConnectionString(connectionInfo.Host, connectionInfo.Port, connectionInfo.TNS, connectionInfo.UserId, connectionInfo.Password);
		}

		private void InvokeCommand(Action<OracleCommand> action)
		{
			using (var connection = new OracleConnection(ConnectionString))
			{
				using (var command = new OracleCommand())
				{
					try
					{
						if (connection.State != ConnectionState.Open)
						{
							connection.Open();
						}

						command.Connection = connection;
						action(command);
					}
					finally
					{
						if (command != null)
						{
							command.Dispose();
						}

						if (connection != null)
						{
							connection.Close();
						}
					}
				}
			}
		}

		/// <summary>
		/// Invokes the command.
		/// </summary>
		/// <param name="connectionInfo">The connection information.</param>
		/// <param name="action">The action.</param>
		public static void InvokeCommand(ConnectionInfo connectionInfo, Action<OracleCommand> action)
		{
			var connectionString = GetConnectionString(connectionInfo);

			using (var connection = new OracleConnection(connectionString))
			{
				using (var command = new OracleCommand())
				{
					try
					{
						if (connection.State != ConnectionState.Open)
						{
							connection.Open();
						}

						command.Connection = connection;
						action(command);
					}
					finally
					{
						if (command != null)
						{
							command.Dispose();
						}

						if (connection != null)
						{
							connection.Close();
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets the data table.
		/// </summary>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public DataTable GetDataTable(string commandText)
		{
			DataTable table = new DataTable();

			InvokeCommand(command =>
			{
				command.CommandText = commandText;
				command.CommandType = CommandType.Text;

				OracleDataReader reader = command.ExecuteReader();

				if (reader != null)
				{
					table.Load(reader);
				}
			});

			return table;
		}

		/// <summary>
		/// Gets the data table by stored procedure.
		/// </summary>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>
		/// DataTable.
		/// </returns>
		public DataTable GetDataTable(string procedureName, params OracleParameter[] parameters)
		{
			DataTable table = new DataTable();

			InvokeCommand(command =>
			{
				command.CommandText = procedureName;
				command.CommandType = CommandType.Text;

				if (parameters != null)
				{
					command.Parameters.AddRange(parameters);
				}

				OracleDataReader reader = command.ExecuteReader();

				if (reader != null)
				{
					table.Load(reader);
				}
			});

			return table;
		}

		/// <summary>
		/// Gets the data table by stored procedure.
		/// </summary>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>
		/// DataTable.
		/// </returns>
		public DataTable GetDataTable(string procedureName, IDictionary<string, dynamic> parameters)
		{
			var param = new OracleParameter[0];

			if (parameters == null)
			{
				return GetDataTable(procedureName, param);
			}

			param = parameters.ToDictionary().Select(x => new OracleParameter
			{
				ParameterName = x.Key,
				Value = x.Value,
			}).ToArray();

			return GetDataTable(procedureName, param);
		}

		/// <summary>
		/// Gets the scalar.
		/// </summary>
		/// <param name="commandText">The command text.</param>
		/// <returns>
		/// dynamic.
		/// </returns>
		public object GetScalar(string commandText)
		{
			object result = null;

			InvokeCommand(command =>
			{
				command.CommandText = commandText;
				command.CommandType = CommandType.Text;

				result = command.ExecuteScalar();
			});

			return result;
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandText">The command text.</param>
		/// <returns>
		/// System.Int32.
		/// </returns>
		public int ExecuteNonQuery(string commandText)
		{
			int result = -1;

			InvokeCommand(command =>
			{
				command.CommandText = commandText;
				command.CommandType = CommandType.Text;

				result = command.ExecuteNonQuery();
			});

			return result;
		}

		/// <summary>
		/// Executes the non query by stored procedure.
		/// </summary>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>
		/// Insert, Update 성공시 -1을 Create Table 성공시 0 을 반환 합니다.
		/// </returns>
		public int ExecuteNonQuery(
			string procedureName,
			params OracleParameter[] parameters)
		{
			int result = 0;

			InvokeCommand(command =>
			{
				command.CommandText = procedureName;
				command.CommandType = CommandType.StoredProcedure;

				if (parameters != null)
				{
					command.Parameters.AddRange(parameters);
				}

				result = command.ExecuteNonQuery();
			});

			return result;
		}

		/// <summary>
		/// Executes the non query by stored procedure.
		/// </summary>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>
		/// Insert, Update 성공시 -1을 Create Table 성공시 0 을 반환 합니다.
		/// </returns>
		public int ExecuteNonQuery(
			string procedureName,
			object parameters)
		{
			var param = new OracleParameter[0];

			if (parameters == null)
			{
				return ExecuteNonQuery(procedureName, param);
			}

			param = parameters.ToDictionary().Select(x => new OracleParameter
			{
				ParameterName = x.Key,
				Value = x.Value,
			}).ToArray();

			return ExecuteNonQuery(procedureName, param);
		}

		/// <summary>
		/// Executes the non query by stored procedure.
		/// </summary>
		/// <param name="procedureName">Name of the procedure.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns>
		/// Insert, Update 성공시 -1을 Create Table 성공시 0 을 반환 합니다.
		/// </returns>
		/// <exception cref="ArgumentNullException">parameters</exception>
		public int ExecuteStoredProcedure(
			string procedureName,
			object parameters)
		{
			if (parameters == null)
			{
				throw new ArgumentNullException("parameters");
			}

			var affectedRow = 0;
			var param = parameters.ToDictionary().Select(x => new OracleParameter
			{
				ParameterName = x.Key,
				Value = x.Value,
			}).ToList();

			param.Add(new OracleParameter(OUT_PARAM_NAME, OracleDbType.Int32, affectedRow, ParameterDirection.Output));

			InvokeCommand(command =>
			{
				command.CommandText = procedureName;
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddRange(param.ToArray());

				command.ExecuteNonQuery();
				affectedRow = Convert.ToInt32(command.Parameters[param.Count - 1].Value.ToString());
			});

			return affectedRow;
		}

		/// <summary>
		/// Executes the transaction.
		/// </summary>
		/// <param name="commands">The commands.</param>
		/// <returns> affected rows</returns>
		public int ExecuteTransaction(IEnumerable<string> commands)
		{
			var affected = 0;

			using (var connection = new OracleConnection(ConnectionString))
			{
				if (connection.State != ConnectionState.Open)
				{
					connection.Open();
				}
				using (var command = connection.CreateCommand())
				{
					OracleTransaction transaction = null;

					try
					{

						transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);
						command.Transaction = transaction;

						foreach (string cmd in commands)
						{
							command.CommandText = cmd;
							command.ExecuteNonQuery();
							affected++;
						}

						transaction.Commit();
					}
					catch (Exception ex)
					{
						if (transaction != null)
						{
							transaction.Rollback();
						}

						throw ex;
					}
					finally
					{
						if (transaction != null)
						{
							transaction.Dispose();
						}

						if (command != null)
						{
							command.Dispose();
						}

						if (connection != null)
						{
							connection.Close();
						}
					}
				}
			}

			return affected;
		}
	}
}
