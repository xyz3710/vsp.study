﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace AsyncDemo
{
	class Program
	{
		static void Main(string[] args)
		{
			var task = PerformDatabaseOperations();

			Console.WriteLine();
			Console.WriteLine("Quote of the day");
			Console.WriteLine(" Don't worry about the world coming to an end today... ");
			Console.WriteLine(" It's already tomorrow in Australia.");

			task.Wait();

			Console.WriteLine();
			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		public static async Task PerformDatabaseOperations()
		{
			using (var db = new BloggingContext())
			{
				// Create a new blog and save it 
				db.Blogs.Add(new Blog
				{
					Name = "Test Blog #" + (db.Blogs.Count() + 1)
				});
				Console.WriteLine("Calling SaveChanges.");
				await db.SaveChangesAsync();
				Console.WriteLine("SaveChanges completed.");

				// Query for all blogs ordered by name 
				Console.WriteLine("Executing query.");
				var blogs = await (from b in db.Blogs
								   orderby b.Name
								   select b).ToListAsync();

				// Write all blogs out to Console 
				Console.WriteLine();
				Console.WriteLine("All blogs:");
				foreach (var blog in blogs)
				{
					Console.WriteLine(" " + blog.Name);
				}
			}
		}
	}
}
