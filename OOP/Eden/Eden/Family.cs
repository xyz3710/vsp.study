using System;
using System.Collections.Generic;
using System.Text;

namespace Eden
{
	public class Family
	{
		private Male _father;
		private Female _mother;
		private PersonList _children;
		private int _day;

		public Family(Male father, Female mother)
		{
			_father = father;
			_mother = mother;
			_children = new PersonList();
			_day = 1;
		}

		~Family()
		{
			_children = null;
		}

		public void LiveFromHandToMouth()
		{
			// 동이튼다
			_day++;
			
			// 식구들이 아침식사를 한다
			eat(Food.Banana);

			// 식구들이 일을 본다
			urinate();

			// 식구들이 점심 식사를 한다
			eat(Food.Meat);

			// 식구들이 저녁 식사를 한다
			eat(Food.Apple);

			if (_day % 365 == 0)
			{
				// 1년을 살면 아기가 태어난다
				Person baby = _father.BearBaby(_mother, _day);

				_children.AddTail(baby);
			}
			else
			{
				// 부부가 그걸 한다.
				_father.DoingX(_mother);
			}
		}

		public void Print()
		{
			Console.WriteLine("\n       <<  가  족  상  태  의  값  >>");
			Console.WriteLine("+--------+------+--------+--------+-----------+");
			Console.WriteLine("| 이  름 | 성별 | 몸무게 |   힘   |  행복도   |");
			Console.WriteLine("+--------+------+--------+--------+-----------+");
			
			_father.Print();
			_mother.Print();

			foreach (Person child in _children)
			{
				child.Print();
			}

			Console.WriteLine("+--------+------+--------+--------+-----------+");
		}


		private void eat(Food food)
		{
			// 식구들이 함께 식사를 한다
			_father.Eat(food);
			_mother.Eat(Food.Apple);	// 이브는 항상 사과만 먹는다

			foreach (Person child in _children)
			{
				child.Eat(food);
			}
		}
		
		private void urinate()
		{
			// 식구들이 나란히 일은 본다
			_father.Urinate(Amount.Big);
			_mother.Urinate(Amount.Small);

			foreach (Person child in _children)
			{
				if (child.WhatIsYourClass == "남자")
				{
					// 아들은 항상 큰 일을 본다.
					child.Urinate(Amount.Big);
				}
				else if (child.WhatIsYourClass == "여자")
				{
					// 딸들은 항상 작은 일을 본다.
					child.Urinate(Amount.Small);
				}
			}
		}
	}
}