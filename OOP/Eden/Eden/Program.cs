using System;
using System.Collections.Generic;
using System.Text;

namespace Eden
{
	class Program
	{
		private const int SOME_DURATION = 10;
	
		static void Main(string[] args)
		{
			// 옛날 옛적 지오디가 아담을 창조했다.
			Male adam = new Male("아  담");

			// 아담은 한 동안 밥 먹고 화장실 가는 일만을 반복했다.
			for (int clock = 0; clock < SOME_DURATION; clock++)
			{
				adam.Eat(Food.Banana);		// 아침에는 바나나를 먹고
				adam.Urinate(Amount.Big);	// 화장실에서 큰걸 해결하고
				adam.Eat(Food.Meat);		// 점심으로 불고기를 먹은 후
				adam.Eat(Food.Apple);		// 저녁에는 사과를 먹었다.
			}

			/* 매일 밥 먹고 화장실 가는 일만 반복하던 그는 매우 심심하다고 느꼈다
			 * 그래서 아담은 지오디에게 "여자"라고 분류되는 동반다를 만들어 달라고 부탁했다
			 * 다행히 지오디는 그 부탁을 들어 주었고 이브를 창조했다 */

			Female eve = new Female("이  브");

			// 아담과 이브는 밥 먹고 화장실 가는 일을 반복했다.
			for (int clock = 0; clock < SOME_DURATION; clock++)
			{
				adam.Eat(Food.Banana);		// 아침에는 바나나를 먹고
				eve.Eat(Food.Apple);		// 아침에는 바나나를 먹고
				adam.Urinate(Amount.Big);	// 화장실에서 큰걸 해결하고
				eve.Urinate(Amount.Small);	// 화장실에서 작은걸 해결하고
				adam.Eat(Food.Meat);		// 점심으로 불고기를 먹은 후
				eve.Eat(Food.Apple);		// 점심으로 불고기를 먹은 후
				adam.Eat(Food.Apple);		// 저녁에는 사과를 먹었다.
				eve.Eat(Food.Apple);		// 저녁에는 사과를 먹었다.			
			}

			/* 함께 식사하고 화장실 가는 일을 반복하며 생활하던 아담과 이브는
			 * 좀 더 즐거운 시간을 보내기 위한 놀이를 생각한다.
			 * 그 놀이의 이름을 "그걸 한다"인데 하루 일과가 끝난 후에 하기에 적당한 놀이였다.
			 * 그래서 그 둘은 매일 밤에 그걸 하기로 했다 */
			// 아담과 이브는 밥 먹고 화장실 가는 일을 반복했다.
			for (int clock = 0; clock < SOME_DURATION; clock++)
			{
				adam.Eat(Food.Banana);		// 아침에는 바나나를 먹고
				eve.Eat(Food.Apple);		// 아침에는 바나나를 먹고
				adam.Urinate(Amount.Big);	// 화장실에서 큰걸 해결하고
				eve.Urinate(Amount.Small);	// 화장실에서 작은걸 해결하고
				adam.Eat(Food.Meat);		// 점심으로 불고기를 먹은 후
				eve.Eat(Food.Apple);		// 점심으로 불고기를 먹은 후
				adam.Eat(Food.Apple);		// 저녁에는 사과를 먹었다.
				eve.Eat(Food.Apple);		// 저녁에는 사과를 먹었다.			

				// 해지고 난 후 한밤중에
				adam.DoingX(eve);			// 그걸 한다.
			}

			/* 정확한 이유는 모르겠지만 지오디는 이브가 자꾸 사과만 먹는 것 때문에 화가 났다
			 * 그래서 지오디는 이들에게 놀이를 하기 위한 의무를 지우기로 결정했다.
			 * 그걸 하기 위한 놀이의 의무의 명칭은 "결혼한다"는 것이며 그 의무의 부담은
			 * 결혼하는 사람들은 가족을 구성해야 하고 그걸 365회 할 때 마다 
			 * 아들이나 딸이 새로 태어나야 한다는 것이다.
			 * 아담과 이브는 이 의무를 따르기로 결정하고 둘이 결혼하여 새 가족을 구성하였다 */
			Family family = adam.Marry(eve);

			// 아담과 이브는 가족을 형성한 후 아이들을 낳으면서 그럭저럭 살아간다
			for (int clock = 0; clock < SOME_DURATION * 200; clock++)
			{
				family.LiveFromHandToMouth();
			}

			// 자, 가족 구성원의 상태 값(성별, 몸무게, 힘, 행복도)등을 알아보자
			family.Print();

			Console.ReadLine();
		}
	}
}
