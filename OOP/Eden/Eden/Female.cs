using System;
using System.Collections.Generic;
using System.Text;

namespace Eden
{
	public class Female : Person
	{
		private const int INCREASE_HAPPINESS = 50;
		
		private double _happiness;

		public Female() : base()
		{
			_happiness = 0.0;
		}
	
		public Female(string name) : base(name)
		{
			_happiness = 0.0;
		}

		/// <summary>
		/// 먹으면 몸무게가 늘고 행복감은 준다.
		/// </summary>
		/// <param name="food"></param>
		public override void Eat(Food food)
		{
			base.Eat(food);

			_happiness -= (int)food;
		}

		/// <summary>
		/// 일을 보면 몸무게는 줄고 행복감은 는다.
		/// </summary>
		/// <param name="amount"></param>
		public override void Urinate(Amount amount)
		{
			base.Urinate(amount);

			_happiness += (int)amount * INCREASE_HAPPINESS;
		}

		/// <summary>
		/// 출력한다.
		/// </summary>
		public override void Print()
		{
			base.Print();

			Console.WriteLine("|        | {0,9:F} |", _happiness);
		}

		/// <summary>
		/// 그걸 할때마다 몸무게는 빠지고 행복감은 는다
		/// </summary>
		/// <param name="spouse"></param>
		public override void DoingX(Person spouse)
		{
			base.DoingX(this);

			// 그걸할 때 배우자쪽의 상태 변화를 야기시킨다.
			if (spouse != null)
			{
				spouse.DoingX(null);
			}
		}

		/// <summary>
		/// 아기의 이름을 정한다.
		/// </summary>
		public override void PleaseMakeBabyName()
		{
			Console.Write("안녕하세요 지오디!! 제 (딸)이름 좀 정해주세요 : ");
			_name = Console.ReadLine();
		}

		/// <summary>
		/// 성별을 구한다.
		/// </summary>
		/// <returns></returns>
		public override string WhatIsYourClass
		{
			get
			{
				return "여자";
			}
		}


		private void increaseHappiness(int howMuch)
		{
			_happiness += howMuch * 0.15 + 0.23;
		}
	}
}
