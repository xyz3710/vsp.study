using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Eden
{
	public class PersonList : IEnumerable
	{
		private LinkedList<Person> _personList;
		private LinkedListNode<Person> _current;

		public PersonList()
		{
			_personList = new LinkedList<Person>();
		}

		~PersonList()
		{
			_personList = null;
		}

		public void AddTail(Person person)
		{
			_personList.AddLast(person);
		}

		public void AddHead(Person person)
		{
			_personList.AddFirst(person);
		}

		public Person GetFirst()
		{
			_current = _personList.First;

			return _current.Value;
		}

		public Person GetNext()
		{
			if (_current == null)
			{
				_current= _personList.First;
			}

			_current = _current.Next;

			return _current.Value;
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			LinkedListNode<Person> current = _personList.First;

			for (int i = 0; i < _personList.Count; i++)
			{
				yield return current.Value;

				current = current.Next;
			}
		}

		#endregion
	}
}
