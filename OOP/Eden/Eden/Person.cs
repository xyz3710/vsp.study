using System;
using System.Collections.Generic;
using System.Text;

namespace Eden
{
	public class Person
	{
		protected const int DOING_X_WEIGHT = 10;
		
		private int _weight;
		protected string _name;

		public Person()
		{
			_weight = 10;
			_name = null;
		}
	
		public Person(string name)
		{
			_weight = 100;
			_name = name;
		}

		~Person()
		{
			if (_name != null)
			{
				Console.WriteLine("{0}({1}) was die", this.GetType().Name, _name);
				_name = null;
			}
			else
			{
				Console.WriteLine("{0}({1}) was die", this.GetType().Name, "No name");
			}
		}

		/// <summary>
		/// 먹은만큼 몸무게가 는다.
		/// </summary>
		/// <param name="food"></param>
		public virtual void Eat(Food food)
		{
			_weight += (int)food;
		}

		/// <summary>
		/// 일보는 만큼 몸무게가 빠진다.
		/// </summary>
		/// <param name="amount"></param>
		public virtual void Urinate(Amount amount)
		{
			_weight -= (int)amount;
		}

		/// <summary>
		/// 출력한다.
		/// </summary>
		public virtual void Print()
		{
			Console.Write("| {0,2} | {1,2} | {2,6:D} ", _name, WhatIsYourClass, _weight);		
		}

		/// <summary>
		/// 그걸 할때마다 몸무게가 빠진다.
		/// </summary>
		/// <param name="spouse"></param>
		public virtual void DoingX(Person spouse)
		{
			_weight -= DOING_X_WEIGHT;
		}

		public virtual void PleaseMakeBabyName()
		{			
		}

		/// <summary>
		/// 성별을 구한다.
		/// </summary>
		/// <returns></returns>
		public virtual string WhatIsYourClass
		{
			get
			{
				return "사람";
			}			
		}
	}
}
