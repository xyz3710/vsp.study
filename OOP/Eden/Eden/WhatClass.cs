using System;
using System.Collections.Generic;
using System.Text;

namespace Eden
{
	public enum WhatClass
	{
		/// <summary>
		/// 사람
		/// </summary>
		Person,
		/// <summary>
		/// 남자
		/// </summary>
		Male,
		/// <summary>
		/// 여자
		/// </summary>
		Female,
	}
}
