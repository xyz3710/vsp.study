using System;
using System.Collections.Generic;
using System.Text;

namespace Eden
{
	public class Male : Person
	{
		private int _xPower;

		public Male() : base()
		{
			_xPower = 0;
		}
	
		public Male(string name) : base(name)
		{
			_xPower = 0;
		}

		/// <summary>
		/// 새로운 가정을 구성한다.
		/// </summary>
		/// <param name="bride"></param>
		/// <returns></returns>
		public Family Marry(Female bride)
		{
			return new Family(this, bride);
		}

		/// <summary>
		/// 아기를 만든다.
		/// </summary>
		/// <param name="spouse"></param>
		/// <param name="day"></param>
		/// <returns></returns>
		public Person BearBaby(Female spouse, int day)
		{
			// 일단 그것을 한다
			DoingX(spouse);

			Person baby;

			if (day % 2 == 0)
			{
				// 날짜가 짝수인 경우에는 아들
				baby = new Male();
			}
			else
			{
				// 날짜가 홀수인 경우에는 딸
				baby = new Female();
			}

			baby.PleaseMakeBabyName();

			return baby;
		}

		/// <summary>
		/// 먹으면 몸무게가 늘고 힘도 솟는다.
		/// </summary>
		/// <param name="food"></param>
		public override void Eat(Food food)
		{
			base.Eat(food);

			increaseXPower((int)food);
		}

		/// <summary>
		/// 일을 보면 몸무게는 줄고 힘은 솟는다.
		/// </summary>
		/// <param name="amount"></param>
		public override void Urinate(Amount amount)
		{
			base.Urinate(amount);

			increaseXPower((int)amount);
		}

		/// <summary>
		/// 현재 힘을 출력한다.
		/// </summary>
		public override void Print()
		{
			base.Print();

			Console.WriteLine("| {0,6:D} |           |", _xPower);
		}

		/// <summary>
		/// 그걸 할때마다 몸무게도 빠지고 힘도 빠진다.
		/// </summary>
		/// <param name="spouse"></param>
		public override void DoingX(Person spouse)
		{
			base.DoingX(this);

			_xPower -= DOING_X_WEIGHT;

			// 그걸할 때 배우자쪽의 상태 변화를 야기시킨다.
			if (spouse != null)
			{
				spouse.DoingX(null);
			}
		}

		/// <summary>
		/// 아기의 이름을 정한다.
		/// </summary>
		public override void PleaseMakeBabyName()
		{
			Console.Write("안녕하세요 지오디!! 제 (아들)이름 좀 정해주세요 : ");
			_name = Console.ReadLine();
		}

		/// <summary>
		/// 성별을 구한다.
		/// </summary>
		/// <returns></returns>
		public override string WhatIsYourClass
		{
			get
			{
				return "남자";
			}
		}


		private void increaseXPower(int howMuch)
		{
			_xPower += howMuch / 10;
		}
	}
}
