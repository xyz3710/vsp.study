using System;
using System.Collections.Generic;
using System.Text;

namespace Association01_1to1
{
	class Program
	{
		static void Main(string[] args)
		{
			Man husband = new Man();

			husband.Name = "Kim Ki Won";

			Woman wife = new Woman();

			wife.Name = "Hwang Tae Young";

			husband.Marry(wife);
			wife.Marry(husband);


		}
	}
}
