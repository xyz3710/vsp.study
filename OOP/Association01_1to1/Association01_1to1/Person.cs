using System;
using System.Collections.Generic;
using System.Text;

namespace Association01_1to1
{
	public class Person : IDisposable
	{
		private string _name;

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			_name = null;
		}

		#endregion
	}
}
