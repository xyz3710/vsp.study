using System;
using System.Collections.Generic;
using System.Text;

namespace Association01_1to1
{
	public class Man : Person
	{
		private Woman _wife;

		public Man()
		{
			_wife = null;
		}

		~Man()
		{
			if (_wife != null)
			{
				_wife.Husband = null;
				_wife = null;
			}			
		}

		public Woman Wife
		{
			get
			{
				return _wife;
			}
			set
			{
				_wife = value;
			}
		}

		public void Marry(Woman wife)
		{
			if (_wife == null)
			{
				_wife = wife;
				_wife.Husband = this;
			}			
		}

		public void Divorce()
		{
			if (_wife.Husband != null)
			{
				_wife.Husband = null;
				_wife = null;
			}			
		}
	}
}
