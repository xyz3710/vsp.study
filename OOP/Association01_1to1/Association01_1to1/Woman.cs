using System;
using System.Collections.Generic;
using System.Text;

namespace Association01_1to1
{
	public class Woman : Person
	{
		private Man _husband;

		public Woman()
		{
			_husband = null;
		}

		~Woman()
		{
			if (_husband.Wife != null)
			{
				_husband.Wife = null;
				_husband = null;
			}
		}

		public Man Husband
		{
			get
			{
				return _husband;
			}
			set
			{
				_husband = value;
			}
		}

		public void Marry(Man husband)
		{
			if (_husband == null)
			{
				_husband = husband;
				_husband.Wife = this;
			}			
		}

		public void Divorce()
		{
			if (_husband.Wife != null)
			{
				_husband.Wife = null;
				_husband = null;
			}			
		}
	}
}
