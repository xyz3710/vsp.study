using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Association02_1toK
{
	public class Hotel
	{
		private const int ROOMCAPACITY = 100;
		private int _roomNo;
		private int _availableRoom;
		private Array _rooms;
		private Guest _customer;

		public Hotel()
		{
			_rooms = new Guest[ROOMCAPACITY];

			_roomNo = 0;
			_availableRoom = 100;
		}

		public void CheckIn(Guest customer)
		{
			_customer = customer;
			
			int roomNo = PreferredRoom(_customer);

			_rooms[roomNo] = _customer;

			_availableRoom--;
		}

		public void CheckOut(int roomNo)
		{
			_rooms[roomNo] = null;

			_availableRoom++;
		}

		public int PreferredRoom(Guest customer)
		{
			int roomNo = _roomNo++;

			return roomNo;
		}

		public int RoomNo
		{
			get
			{
				return _roomNo;
			}
			set
			{
				_roomNo = value;
			}
		}

		public Guest Guest
		{
			get
			{
				throw new System.NotImplementedException();
			}
			set
			{
			}
		}
	}
}
