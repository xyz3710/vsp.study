using System;
using System.Collections.Generic;
using System.Text;

namespace Association04_TupleClass
{
	public class MarryInfoTuple
	{
		private Man _husband;
		private Woman _wife;
		private DateTime _when;
		private string _where;

		public Man Husband
		{
			get
			{
				return _husband;
			}
			set
			{
				_husband = value;
			}
		}

		public Woman Wife
		{
			get
			{
				return _wife;
			}
			set
			{
				_wife = value;
			}
		}

		public DateTime MyProperty
		{
			get
			{
				return _when;
			}
			set
			{
				_when = value;
			}
		}

		public string Where
		{
			get
			{
				return _where;
			}
			set
			{
				_where = value;
			}
		}
	}
}
