using System;
using System.Collections.Generic;
using System.Text;

namespace Association04_TupleClass
{
	public class Person : IDisposable
	{
		private string _name;

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			_name = null;
		}

		#endregion
	}
}
