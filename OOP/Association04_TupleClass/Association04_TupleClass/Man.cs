using System;
using System.Collections.Generic;
using System.Text;

namespace Association04_TupleClass
{
	public class Man : Person
	{
		private Woman _wife;
		private MarryInfoTuple _marryInfo;

		public Man()
		{
			_wife = null;
			_marryInfo = null;
		}

		~Man()
		{
			if (_marryInfo != null)
			if (_wife != null)
			{
				_wife.Husband = null;
				_wife = null;
			}			
		}

		public Woman Wife
		{
			get
			{
				return _wife;
			}
			set
			{
				_wife = value;
			}
		}

		public MarryInfoTuple MarryInfoTuple
		{
			get
			{
				throw new System.NotImplementedException();
			}
			set
			{
			}
		}

		public void Marry(Woman wife)
		{
			if (_wife == null)
			{
				_wife = wife;
				_wife.Husband = this;
			}			
		}

		public void Divorce()
		{
			if (_wife.Husband != null)
			{
				_wife.Husband = null;
				_wife = null;
			}			
		}
	}
}
