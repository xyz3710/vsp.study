﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace OOP01
{
	/// <summary>
	/// <remarks>Here is an example of a bulleted list:
	/// <list type="bullet">
	/// <item>
	/// <description><seealso cref="Box"/><seealso href="http://daum.net"/>Item 1.</description>
	/// </item>
	/// <item>
	/// <description><seealso cref="Point"/><see href="http://naver.com"/>Item 2.</description>
	/// </item>
	/// </list>
	/// </remarks>
	/// <example> This sample shows how to call the GetZero method.
	/// <code>
	///   class MyClass 
	///   {
	///      public static int Main() 
	///      {
	///         return GetZero();
	///      }
	///   }
	/// </code>
	/// </example>
	/// </summary>
	public class Programs
	{
		static void Main(string[] args)
		{
			System.Drawing.Graphics gContext = null;

			Point point = new Point(System.Drawing.Color.Blue, 10, 10);
			
			point.Draw(gContext);
			point.Move(gContext, 10, 10);

			point.Show();
			Console.WriteLine("Color {0}\tx : {1}\ty : {2}", point.Color, point.X, point.Y);

			Console.WriteLine();
			
			
			Line line = new Line(System.Drawing.Color.Red, 20, 20, 200, 200);

			line.Draw(gContext);
			line.Move(gContext, 20, 20);
			line.Show();
			Console.WriteLine("Color {0}\tx1 : {1}\ty1 : {2}\tx2 : {3}\ty2 : {4}", 
				line.Color, line.X1, line.Y1, line.X2, line.Y2);

			Console.WriteLine();

			
			Box box = new Box(System.Drawing.Color.Yellow, 30, 30, 300, 300);

			box.FillFlag = true;
			box.Draw(gContext);
			box.Move(gContext, 30, 30);
			box.Show();
			Console.WriteLine("Color {0}\tx1 : {1}\ty1 : {2}\tx2 : {3}\ty2 : {4}",
				box.Color, box.X1, box.Y1, box.X2, box.Y2);

			Console.WriteLine();


			Point pt = (Point)point.Copy();

			Console.WriteLine("Coppied class of point \nColor {0}\tx : {1}\ty : {2}",
				pt.Color, pt.X, pt.Y);


			#region 생성자를 동적으로 호출하기

			//Console.WriteLine("생성자를 동적으로 호출하기");
			//Type[] paramTypes = new Type[] 
			//{ 
			//    typeof(System.Drawing.Color),
			//    typeof(int),
			//    typeof(int)
			//};

			//ConstructorInfo ctorPoint = point.GetType().GetConstructor(paramTypes);

			//if (ctorPoint != null)
			//{
			//    object[] ctorParams = new object[]
			//    {
			//        point.Color,
			//        point.X,
			//        point.Y
			//    };

			//    Point pt = (Point)ctorPoint.Invoke(ctorParams);

			//    pt.Draw(gContext);
			//    pt.Move(gContext, 100, 100);
			//} 

			#endregion
	

			Console.ReadLine();
		}
	}
}
