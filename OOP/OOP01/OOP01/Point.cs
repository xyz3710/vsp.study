using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace OOP01
{
	/// <summary>
	/// Point that derived with <see cref="OnePointFigure"/>
	/// </summary>
	public class Point : OnePointFigure
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Point"/> class.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="x">The x.</param>
		/// <param name="y">The y.</param>
		public Point(Color color, int x, int y) : base(color, x, y)
		{
		}

		/// <summary>
		/// Draws the specified g context.
		/// </summary>
		/// <param name="gContext">The g context.</param>
		public override void Draw(Graphics gContext)
		{
			//base.Draw(gContext);
			Console.WriteLine("{0} class's {1}", this.GetType().Name, "override Draw");
		}
	}
}
