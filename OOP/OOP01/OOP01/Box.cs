using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace OOP01
{
	/// <summary>
	/// Box that derived with <see cref="TwoPointFigure"/>
	/// </summary>
	public class Box : TwoPointFigure
	{
		private bool fillFlag;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Box"/> class.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="x1">The x1.</param>
		/// <param name="y1">The y1.</param>
		/// <param name="x2">The x2.</param>
		/// <param name="y2">The y2.</param>
		public Box(Color color, int x1, int y1, int x2, int y2)
			: base(color, x1, y1, x2, y2)
		{
		}

		/// <summary>
		/// Gets or sets a value indicating whether [fill flag].
		/// </summary>
		/// <value><c>true</c> if [fill flag]; otherwise, <c>false</c>.</value>
		public bool FillFlag
		{
			get
			{
				return fillFlag;
			}
			set
			{
				fillFlag = value;
			}
		}
	}
}
