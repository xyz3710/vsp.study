using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;

namespace OOP01
{
	/// <summary>
	/// Base Figure class
	/// </summary>
	public abstract class Figure : IFigure
	{
		private Color color;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Figure"/> class.
		/// </summary>
		/// <param name="color">The color.</param>
		public Figure(Color color)
		{
			this.color = color;
		}

		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		/// <value>The color.</value>
		public Color Color
		{
			get
			{
				Console.WriteLine("{0} class's Get color : {1}", this.GetType().Name, color.ToString());

				return color;
			}
			set
			{
				color = value;

				Console.WriteLine("{0} class's Set color : {1}", this.GetType().Name, color.ToString());
			}
		}

		/// <summary>
		/// Draws the specified g context.
		/// </summary>
		/// <param name="gContext">The g context.</param>
		public virtual void Draw(Graphics gContext)
		{
			Console.WriteLine("{0} class's {1}", this.GetType().Name, "Draw");
		}

		/// <summary>
		/// Moves the specified g context.
		/// </summary>
		/// <param name="gContext">The g context.</param>
		/// <param name="dx">The dx.</param>
		/// <param name="dy">The dy.</param>
		public virtual void Move(Graphics gContext, int dx, int dy)
		{
			Console.WriteLine("{0} class's {1} to\tx : {2}\ty : {3}", this.GetType().Name, "Move", dx, dy);
		}

		/// <summary>
		/// Shows this instance.
		/// </summary>
		public virtual void Show()
		{
			Console.WriteLine("{0} class's {1}", this.GetType().Name, "Show");
		}

		/// <summary>
		/// Copies this instance.
		/// </summary>
		/// <returns></returns>
		public abstract object Copy();
	}

	/// <summary>
	/// Figure with one point that derived with <see cref="Figure"/>
	/// </summary>
	public abstract class OnePointFigure : Figure
	{
		private int x;
		private int y;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:OnePointFigure"/> class.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="x">The x.</param>
		/// <param name="y">The y.</param>
		public OnePointFigure(Color color, int x, int y) : base(color)
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>
		/// Gets or sets the X.
		/// </summary>
		/// <value>The X.</value>
		public int X
		{
			get
			{
				return x;
			}
			set
			{
				x = value;
			}
		}

		/// <summary>
		/// Gets or sets the Y.
		/// </summary>
		/// <value>The Y.</value>
		public int Y
		{
			get
			{
				return y;
			}
			set
			{
				y = value;
			}
		}

		/// <summary>
		/// Copies this instance.
		/// </summary>
		/// <returns></returns>
		public override object Copy()
		{
			Type[] ctorParamTypes = new Type[] 
		    { 
		        typeof(System.Drawing.Color),
		        typeof(int),
		        typeof(int)
		    };

			ConstructorInfo ctorInfo = this.GetType().GetConstructor(ctorParamTypes);

			if (ctorInfo != null)
			{
				return ctorInfo.Invoke(new object[] { Color, x, y });
			}

			return null;
		}
	}

	/// <summary>
	/// Figure with two points that derived with <see cref="Figure"/>
	/// </summary>
	public abstract class TwoPointFigure : Figure
	{
		private int x1;
		private int y1;
		private int x2;
		private int y2;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:TwoPointFigure"/> class.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="x1">The x1.</param>
		/// <param name="y1">The y1.</param>
		/// <param name="x2">The x2.</param>
		/// <param name="y2">The y2.</param>
		public TwoPointFigure(Color color, int x1, int y1, int x2, int y2)
			: base(color)
		{
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}

		/// <summary>
		/// Gets or sets the x1.
		/// </summary>
		/// <value>The x1.</value>
		public int X1
		{
			get
			{
				return x1;
			}
			set
			{
				x1 = value;
			}
		}

		/// <summary>
		/// Gets or sets the y1.
		/// </summary>
		/// <value>The y1.</value>
		public int Y1
		{
			get
			{
				return y1;
			}
			set
			{
				y1 = value;
			}
		}

		/// <summary>
		/// Gets or sets the x2.
		/// </summary>
		/// <value>The x2.</value>
		public int X2
		{
			get
			{
				return x2;
			}
			set
			{
				x2 = value;
			}
		}

		/// <summary>
		/// Gets or sets the y2.
		/// </summary>
		/// <value>The y2.</value>
		public int Y2
		{
			get
			{
				return y2;
			}
			set
			{
				y2 = value;
			}
		}

		/// <summary>
		/// Copies this instance.
		/// </summary>
		/// <returns></returns>
		public override object Copy()
		{
			Type[] ctorParamTypes = new Type[] 
			{ 
				typeof(System.Drawing.Color),
				typeof(int),
				typeof(int),
				typeof(int),
				typeof(int)
			};

			ConstructorInfo ctorInfo = this.GetType().GetConstructor(ctorParamTypes);

			if (ctorInfo != null)
			{
				return ctorInfo.Invoke(new object[] { Color, x1, y1, x2, y2 });
			}

			return null;
		}
	}
}
