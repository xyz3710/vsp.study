﻿using System;
namespace OOP01
{
	/// <summary>
	/// Interface for Figure
	/// </summary>
	interface IFigure
	{
		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		/// <value>The color.</value>
		System.Drawing.Color Color
		{
			get;
			set;
		}

		/// <summary>
		/// Copies this instance.
		/// </summary>
		/// <returns></returns>
		object Copy();
		
		/// <summary>
		/// Draws the specified g context.
		/// </summary>
		/// <param name="gContext">The g context.</param>
		void Draw(System.Drawing.Graphics gContext);
		
		/// <summary>
		/// Moves the specified g context.
		/// </summary>
		/// <param name="gContext">The g context.</param>
		/// <param name="dx">The dx.</param>
		/// <param name="dy">The dy.</param>
		void Move(System.Drawing.Graphics gContext, int dx, int dy);
		
		/// <summary>
		/// Shows this instance.
		/// </summary>
		void Show();
	}
}
