using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace OOP01
{
	/// <summary>
	/// Line that derived with <see cref="TwoPointFigure"/>
	/// </summary>
	public class Line : TwoPointFigure
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Line"/> class.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="x1">The x1.</param>
		/// <param name="y1">The y1.</param>
		/// <param name="x2">The x2.</param>
		/// <param name="y2">The y2.</param>
		public Line(Color color, int x1, int y1, int x2, int y2) 
			: base(color, x1, y1, x2, y2)
		{
		}
	}
}
