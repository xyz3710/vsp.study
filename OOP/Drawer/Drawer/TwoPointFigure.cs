using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class TwoPointFigure : Figure
	{
		public TwoPointFigure() 
			: base()
		{
		}

		public TwoPointFigure(Graphics dc, Color penColor, Point startPoint, Point endPoint)
			: this(dc, penColor, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y)
		{
		}

		public TwoPointFigure(Graphics dc, Color penColor, int x1, int y1, int x2, int y2)
			: base(dc, penColor)
		{
			_gdi.Location = new Point(x1, y1);
			_gdi.Location2 = new Point(x2, y2);
		}

		public override void Marquee(Point endPoint)
		{
			base.Marquee(endPoint);
		}

		public override Figure Copy()
		{
			return base.Copy();
		}

		public override void Draw(GDI gdi)
		{
			base.Draw(gdi);
		}

		public override void Move(GDI gdi, int x, int y)
		{
			base.Move(gdi, x, y);
		}

		public override void Remove()
		{
			base.Remove();
		}
	}
}
