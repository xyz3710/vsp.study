using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class Square : TwoPointFilledFigure
	{
		public Square(Graphics dc, Color penColor, Point startPoint, Point endPoint)
			: this(dc, penColor, Color.Empty, startPoint, endPoint)
		{
		}
		
		public Square(Graphics dc, Color penColor, Color fillColor, Point startPoint, Point endPoint)
			: this(dc, penColor, fillColor, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y)
		{
		}
		
		public Square(Graphics dc, Color penColor, int x1, int y1, int x2, int y2)
			: this(dc, penColor, Color.Empty, x1, y1, x2, y2)
		{
		}

		public Square(Graphics dc, Color penColor, Color fillColor, int x1, int y1, int x2, int y2)
			: base(dc, penColor, fillColor, x1, y1, x2, y2)
		{
		}

		public override Figure Copy()
		{
			return new Square(_gdi.DC, _gdi.PenColor, _gdi.FillColor, _gdi.Location, _gdi.Location2);
		}

		public override void Draw(GDI gdi)
		{
			//gdi.FillColor = Color.LightBlue;

			SolidBrush brush = new SolidBrush(gdi.FillColor);

			gdi.DC.FillRectangle(brush, gdi.Rectangle);


			Pen pen = new Pen(Color.Black);
			pen.Alignment = System.Drawing.Drawing2D.PenAlignment.Outset;

			gdi.DC.DrawRectangle(pen, gdi.Rectangle);
			
			base.Draw(gdi);
		}
	}
}
