using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class GDI
	{
		private Graphics _dc;
		private Color _penColor;
		private Color _fillColor;
		private Point _location;
        private Point _location2;
		private Rectangle _rectangle;

		public GDI(Graphics dc, Color penColor)
			: this(dc, penColor, Color.Empty, Point.Empty, Point.Empty)
		{
		}

		public GDI(Graphics dc, Color penColor, Color fillColor)
			: this(dc, penColor, fillColor, Point.Empty, Point.Empty)
		{
		}

		public GDI(Graphics dc, Color penColor, Point location)
			: this(dc, penColor, Color.Empty, location, Point.Empty)
		{
		}

		private GDI(Graphics dc, Color penColor, Color fillColor, Point location, Point location2)
		{
			_dc = dc;
			_penColor = penColor;
			_fillColor = fillColor;
			_location = location;
			_location2 = location2;
		}

		public Graphics DC
		{
			get
			{
				return _dc;
			}
			set
			{
				_dc = value;
			}
		}

		public Color PenColor
		{
			get
			{
				return _penColor;
			}
			set
			{
				_penColor = value;
			}
		}

		public Color FillColor
		{
			get
			{
				return _fillColor;
			}
			set
			{
				_fillColor = value;
			}
		}

		public Point Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

		public Point Location2
		{
			get
			{
				return _location2;
			}
			set
			{
				_location2 = value;
			}
		}

		public Rectangle Rectangle
		{
			get
			{
				_rectangle = new Rectangle(
					new Point(
						_location.X < _location2.X ? _location.X : _location2.X,
						_location.Y < _location2.Y ? _location.Y : _location2.Y),
					new Size(
						Math.Abs(_location.X - _location2.X),
						Math.Abs(_location.Y - _location2.Y))
				);					

				return _rectangle;
			}
			set
			{
				_rectangle = value;
			}
		}
	}
}
