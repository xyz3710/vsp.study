using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Drawer
{
	public partial class frmDrawer : Form
	{
		private Color[] _colors = {
			Color.Black,
			Color.Blue,
			Color.Green,
			Color.SkyBlue,
			Color.Red,
			Color.Purple,
			Color.Yellow,
			Color.White,
			Color.Gray,
			Color.LightBlue,
			Color.LightGreen,
			Color.LightSkyBlue,
			Color.IndianRed,
			Color.LightPink,
			Color.LightYellow,
			Color.WhiteSmoke,
		};
		private Shape _shape;
		private Figure _curFigure;
		private Color _curPenColor;
		private Color _curFillColor;
		private Graphics _curDC;
		private FigureList _figureList;
		private bool _trace;
		private Point _startPoint;
        private Point _endPoint;
		private bool _isPointer;

		public frmDrawer()
		{
			InitializeComponent();

			_curPenColor = Color.Black;
			_curFillColor = Color.White;
			_figureList = new FigureList();
			_trace = false;
			_startPoint = new Point();
			_endPoint = new Point();
		}

		private void frmDrawer_Load(object sender, EventArgs e)
		{
			for (int i = 0; i < (int)Shape.ShapeCount; i++)
			{
				string menuName = ((Shape)i).ToString();

				ToolStripMenuItem tsi 
					= new ToolStripMenuItem(menuName, null, new EventHandler(cmMain_OnClick));
				
				tsi.Name = "cmsMain" + menuName;
				tsi.CheckOnClick = true;

				cmMain.Items.Add(tsi);
			}

			for (int i = 0; i < _colors.Length; i++)
			{
				tsmFPenColor.DropDownItems.Add(
					_colors[i].Name, null, new EventHandler(cmLineColor_OnClick));

				tsmFFillColor.DropDownItems.Add(
					_colors[i].Name, null, new EventHandler(cmFillColor_OnClick));
				
				tsmNFPenColor.DropDownItems.Add(
					_colors[i].Name, null, new EventHandler(cmLineColor_OnClick));
			}

			_isPointer = true;

			_curDC = this.CreateGraphics();

			#region 자동 Dropdown color list 추가
			// 자동 Dropdown color list 추가
			//Type typeColor = typeof(Color);

			//MemberInfo[] colors = typeColor.GetProperties();

			//for (int i = 0; i < 141; i++)
			//{
			//    tsmFPenColor.DropDownItems.Add(colors[i].Name, null, new EventHandler(cmLineColor_OnClick));
			//} 
			#endregion
		}

		private void cmMain_OnClick(object sender, EventArgs e)
		{
			try
			{
				_shape = (Shape)Enum.Parse(typeof(Shape), sender.ToString());
				_isPointer = false;
			}
			catch (Exception)
			{
				_isPointer = true;
			}
				
			this.Text = "Drawer : " + _shape.ToString();

			for (int i = 0; i < cmMain.Items.Count; i++)
			{
				if (cmMain.Items[i] is ToolStripMenuItem)
				{
					((ToolStripMenuItem)cmMain.Items[i]).Checked = false;
				}
			}

			((ToolStripMenuItem)sender).Checked = true;
		}

		private void cmLineColor_OnClick(object sender, EventArgs e)
		{
			//_curPenColor = 
			
			Console.WriteLine("전경색 {0}", sender);
		}

		private void cmFillColor_OnClick(object sender, EventArgs e)
		{
			//_curFillColor = Color.White;

			Console.WriteLine("채움색 {0}", sender);
		}

		// TODO: 라인을 선택해서 첫번째 점을 찍고 팝업으로 점을 선택한 뒤 두번째 점을 찍으면
		// 선이 이어 지지 않으나 다시 라인을 선택하고 첫번째 점을 찍으면 전에 직은 좌표와 이어진다
		private void frmDrawer_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Left)
				return;

			if (_isPointer == true)
			{
				Figure selectedFigure = _figureList.InRegion(e.Location);
				
				if (selectedFigure != null)
				{
					_curFigure = selectedFigure.Copy();

					Console.WriteLine(_curFigure.ToString() + " was selected");
					
					//_figureList.
					_curFigure.Move(_curFigure.Gdi, 0, 0);
					_figureList.Replace(selectedFigure, _curFigure);
				}

				return;
			}

			Console.WriteLine("Down");

			switch (_shape)
			{
				case Shape.Point:
					_curFigure = new Dot(_curDC, _curPenColor, e.X, e.Y);

					_figureList.Add(_curFigure);
					_curFigure.Draw(_curFigure.Gdi);

					break;
				case Shape.Line:
					if (_trace == false)
					{
						// Line의 시작점을 알리기 위한 임시 Dot
						// MouseUp에서 삭제한다
						_curFigure = new Dot(_curDC, _curPenColor, e.X, e.Y);

						_curFigure.Draw(_curFigure.Gdi);
						
						_trace = true;
						_startPoint = e.Location;
					}

					break;
				case Shape.Retangle:
				case Shape.Circle:
					if (_trace == false)
					{
						// Line의 시작점을 알리기 위한 임시 Dot
						// MouseUp에서 삭제한다
						_curFigure = new Dot(_curDC, _curPenColor, e.X, e.Y);

						_curFigure.Draw(_curFigure.Gdi);

						_trace = true;
						_startPoint = e.Location;
					}

					break;
				case Shape.TV:

					break;
				default:
					break;
			}

			Console.WriteLine("{0}\tstart\t{1}, {2}", _trace, _startPoint.X, _startPoint.Y);
		}

		private void frmDrawer_MouseMove(object sender, MouseEventArgs e)
		{
			Console.WriteLine("Move");
			if (_trace == true)
			{
			    _curFigure.Marquee(e.Location);
			}			
		}

		private void frmDrawer_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Left)
				return;

			_endPoint = e.Location;

			switch (_shape)
			{
				case Shape.Point:

					break;
				case Shape.Line:
					if (_trace == true &&
						_startPoint.Equals(e.Location) == false)
					{
						// Line의 시작점을 나타내는 임시 Dot 객체를 삭제한다
						_curFigure.Remove();
						_trace = false;

						_curFigure = new Line(_curDC, _curPenColor, _startPoint, _endPoint);
						
						_figureList.Add(_curFigure);
						_curFigure.Draw(_curFigure.Gdi); 
					}

					break;
				case Shape.Retangle:
					if (_trace == true &&
						_startPoint.Equals(e.Location) == false)
					{
						// Line의 시작점을 나타내는 임시 Dot 객체를 삭제한다
						_curFigure.Remove();
						_trace = false;

						_curFigure = new Square(_curDC, _curPenColor, _startPoint, _endPoint);

						_figureList.Add(_curFigure);
						_curFigure.Draw(_curFigure.Gdi);
					}

					break;
				case Shape.Circle:
					if (_trace == true &&
						_startPoint.Equals(e.Location) == false)
					{
						// Line의 시작점을 나타내는 임시 Dot 객체를 삭제한다
						_curFigure.Remove();
						_trace = false;

						_curFigure = new Ellipse(_curDC, _curPenColor, _startPoint, _endPoint);

						_figureList.Add(_curFigure);
						_curFigure.Draw(_curFigure.Gdi);
					}

					break;
				case Shape.TV:

					break;
				default:
					break;
			}

			Console.WriteLine("{0}\tstart\t{1}, {2}\tend : {3}, {4}",
				_trace, _startPoint.X, _startPoint.Y, e.X, e.Y);
		}

		private void frmDrawer_Paint(object sender, PaintEventArgs e)
		{
			foreach (Figure figure in _figureList)
			{
				figure.Draw(figure.Gdi);
			}
		}

		private void frmDrawer_MouseClick(object sender, MouseEventArgs e)
		{
			if (_isPointer == true)
			{

			}
		}
	}
}