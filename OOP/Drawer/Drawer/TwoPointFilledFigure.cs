using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class TwoPointFilledFigure : TwoPointFigure
	{
		public TwoPointFilledFigure()
			: base()
		{
		}

		public TwoPointFilledFigure(Graphics dc, Color penColor, Color fillColor, Point startPoint, Point endPoint)
			: this(dc, penColor, fillColor, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y)
		{
		}

		public TwoPointFilledFigure(Graphics dc, Color penColor, Color fillColor, int x1, int y1, int x2, int y2)
			: base(dc, penColor, x1, y1, x2, y2)
		{
			_gdi.FillColor = fillColor;
		}

		public override Figure Copy()
		{
			return base.Copy();
		}

		public override void Draw(GDI gdi)
		{
			base.Draw(gdi);
		}

		public override void Move(GDI gdi, int x, int y)
		{
			base.Move(gdi, x, y);
		}

		public override void Remove()
		{
			base.Remove();
		}
	}
}
