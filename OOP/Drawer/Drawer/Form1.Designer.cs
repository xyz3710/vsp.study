namespace Drawer
{
	partial class frmDrawer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cmNFilled = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsmNFCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmNFDelete = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.tsmNFPenColor = new System.Windows.Forms.ToolStripMenuItem();
			this.cmFilled = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsmFCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmFDelete = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.tsmFPenColor = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmFFillColor = new System.Windows.Forms.ToolStripMenuItem();
			this.cmMain = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.pointerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
			this.cmNFilled.SuspendLayout();
			this.cmFilled.SuspendLayout();
			this.cmMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// cmNFilled
			// 
			this.cmNFilled.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmNFCopy,
            this.tsmNFDelete,
            this.toolStripMenuItem1,
            this.tsmNFPenColor});
			this.cmNFilled.Name = "cmNFilled";
			this.cmNFilled.ShowImageMargin = false;
			this.cmNFilled.Size = new System.Drawing.Size(108, 76);
			// 
			// tsmNFCopy
			// 
			this.tsmNFCopy.Name = "tsmNFCopy";
			this.tsmNFCopy.Size = new System.Drawing.Size(107, 22);
			this.tsmNFCopy.Text = "Copy";
			// 
			// tsmNFDelete
			// 
			this.tsmNFDelete.Name = "tsmNFDelete";
			this.tsmNFDelete.Size = new System.Drawing.Size(107, 22);
			this.tsmNFDelete.Text = "Delete";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(104, 6);
			// 
			// tsmNFPenColor
			// 
			this.tsmNFPenColor.Name = "tsmNFPenColor";
			this.tsmNFPenColor.Size = new System.Drawing.Size(107, 22);
			this.tsmNFPenColor.Text = "Line color";
			// 
			// cmFilled
			// 
			this.cmFilled.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFCopy,
            this.tsmFDelete,
            this.toolStripMenuItem2,
            this.tsmFPenColor,
            this.tsmFFillColor});
			this.cmFilled.Name = "cmFilled";
			this.cmFilled.ShowImageMargin = false;
			this.cmFilled.Size = new System.Drawing.Size(108, 98);
			// 
			// tsmFCopy
			// 
			this.tsmFCopy.Name = "tsmFCopy";
			this.tsmFCopy.Size = new System.Drawing.Size(107, 22);
			this.tsmFCopy.Text = "Copy";
			// 
			// tsmFDelete
			// 
			this.tsmFDelete.Name = "tsmFDelete";
			this.tsmFDelete.Size = new System.Drawing.Size(107, 22);
			this.tsmFDelete.Text = "Delete";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(104, 6);
			// 
			// tsmFPenColor
			// 
			this.tsmFPenColor.Name = "tsmFPenColor";
			this.tsmFPenColor.Size = new System.Drawing.Size(107, 22);
			this.tsmFPenColor.Text = "Line color";
			// 
			// tsmFFillColor
			// 
			this.tsmFFillColor.Name = "tsmFFillColor";
			this.tsmFFillColor.Size = new System.Drawing.Size(107, 22);
			this.tsmFFillColor.Text = "Fill color";
			// 
			// cmMain
			// 
			this.cmMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pointerToolStripMenuItem,
            this.toolStripMenuItem3});
			this.cmMain.Name = "cmMain";
			this.cmMain.Size = new System.Drawing.Size(153, 54);
			// 
			// pointerToolStripMenuItem
			// 
			this.pointerToolStripMenuItem.Checked = true;
			this.pointerToolStripMenuItem.CheckOnClick = true;
			this.pointerToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.pointerToolStripMenuItem.Name = "pointerToolStripMenuItem";
			this.pointerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.pointerToolStripMenuItem.Text = "Pointer";
			this.pointerToolStripMenuItem.Click += new System.EventHandler(this.cmMain_OnClick);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(149, 6);
			// 
			// frmDrawer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(792, 573);
			this.ContextMenuStrip = this.cmMain;
			this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "frmDrawer";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Drawer";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmDrawer_Paint);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmDrawer_MouseClick);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmDrawer_MouseUp);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDrawer_MouseMove);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDrawer_MouseDown);
			this.Load += new System.EventHandler(this.frmDrawer_Load);
			this.cmNFilled.ResumeLayout(false);
			this.cmFilled.ResumeLayout(false);
			this.cmMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ContextMenuStrip cmNFilled;
		private System.Windows.Forms.ToolStripMenuItem tsmNFCopy;
		private System.Windows.Forms.ToolStripMenuItem tsmNFDelete;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem tsmNFPenColor;
		private System.Windows.Forms.ContextMenuStrip cmFilled;
		private System.Windows.Forms.ToolStripMenuItem tsmFCopy;
		private System.Windows.Forms.ToolStripMenuItem tsmFDelete;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem tsmFPenColor;
		private System.Windows.Forms.ToolStripMenuItem tsmFFillColor;
		private System.Windows.Forms.ContextMenuStrip cmMain;
		private System.Windows.Forms.ToolStripMenuItem pointerToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;

	}
}

