using System;
using System.Collections.Generic;
using System.Text;

namespace Drawer
{
	public enum Shape : int
	{
		Point = 0,
		Line,
		Retangle,
		Circle,
		TV,
		ShapeCount
	}
}
