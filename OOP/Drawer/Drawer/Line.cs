using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class Line : TwoPointFigure
	{
		private int _width;

		public Line(Graphics dc, Color penColor, Point startPoint, Point endPoint)
			: this(dc, penColor, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y)
		{
		}
		
		public Line(Graphics dc, Color penColor, int x1, int y1, int x2, int y2)
			: base(dc, penColor, x1, y1, x2, y2)
		{
		}
	
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		public override void Marquee(Point endPoint)
		{
			base.Marquee(endPoint);
		}	
	
		public override Figure Copy()
		{
			return new Line(_gdi.DC, _gdi.PenColor, _gdi.Location, _gdi.Location2);
		}

		public override void Draw(GDI gdi)
		{
			Pen pen = new Pen(gdi.PenColor, _width);

			gdi.DC.DrawLine(pen, 
				gdi.Location.X, gdi.Location.Y,
				gdi.Location2.X, gdi.Location2.Y);
		}

		public override void Move(GDI gdi, int x, int y)
		{
			base.Move(gdi, x, y);
		}

		public override void Remove()
		{
			base.Remove();
		}
	}
}
