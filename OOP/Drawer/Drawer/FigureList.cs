using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;

namespace Drawer
{
	public class FigureList : IEnumerable
	{
		private Hashtable _figureHash;

		public FigureList()
		{
			_figureHash = new Hashtable();
		}

		public void Add(Figure figure)
		{
			_figureHash.Add(figure.GetHashCode(), figure);
		}

		public void Remove(object hashKey)
		{
			_figureHash.Remove(hashKey);
		}

		public void Replace(Figure oldFigure, Figure newFigure)
		{
			_figureHash.Remove(oldFigure.GetHashCode());
			
			Add(newFigure);
		}

		public Figure InRegion(Point location)
		{
			Rectangle newRegion = new Rectangle(location, new Size());

			foreach (Figure figure in this)
			{
				if (figure.Gdi.Rectangle.IntersectsWith(newRegion) == true)
				{
					return figure;
				}
			}

			return null;
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			foreach (DictionaryEntry de in _figureHash)
			{
				yield return de.Value;
			}
		}

		#endregion
	}
}
