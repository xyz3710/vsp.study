using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class OnePointFigure : Figure
	{
		public OnePointFigure() 
			: base()
		{
		}

		public OnePointFigure(Graphics dc, Color penColor, int x, int y)
			: base(dc, penColor)
		{
			_gdi.Location = new Point(x, y);
			_gdi.Location2 = new Point(x + 1, y + 1);
		}

		public override Figure Copy()
		{
			return base.Copy();
		}

		public override void Draw(GDI gdi)
		{
			Pen pen = new Pen(gdi.PenColor, 1);

			gdi.DC.DrawRectangle(pen, gdi.Rectangle);

			base.Draw(gdi);
		}

		public override void Move(GDI gdi, int x, int y)
		{
			base.Move(gdi, x, y);
		}

		public override void Remove()
		{
			base.Remove();
		}
	}
}
