using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Drawer
{
	public class Figure : Drawer.IFigure, IDisposable
	{
		protected Color BOARD_COLOR = Color.White;
		protected GDI _gdi;

		public Figure() 
			: this(Graphics.FromImage(new Bitmap(1, 1)), Color.Empty)
		{
		}

		public Figure(Graphics dc, Color penColor)
		{
			_gdi = new GDI(dc, penColor);
		}

		public GDI Gdi
		{
			get
			{
				return _gdi;
			}
			set
			{
				_gdi = value;
			}
		}

		public virtual Figure Copy()
		{
			return new Figure(_gdi.DC, _gdi.PenColor);
		}

		public virtual void Marquee(Point endPoint)
		{
			_gdi.Location2 = endPoint;

			SolidBrush dashBrush = new SolidBrush(Color.Silver);
			Pen pen = new Pen(dashBrush);

			pen.Alignment = System.Drawing.Drawing2D.PenAlignment.Center;

			_gdi.DC.DrawRectangle(pen, _gdi.Rectangle);
		}

		public virtual void Draw(GDI gdi)
		{			
		}

		public virtual void Move(GDI gdi, int x, int y)
		{
			GDI tmpGdi = gdi;
			
			tmpGdi.PenColor = Color.Gold;
			tmpGdi.FillColor = Color.Yellow;

			Draw(tmpGdi);
			

			int offsetX = gdi.Location.X - x;
			int offsetY = gdi.Location.Y - y;
			int x2 = gdi.Location2.X - offsetX;
			int y2 = gdi.Location2.Y - offsetY;
						
			gdi.Location = new Point(x, y);
			gdi.Location2 = new Point(x2, y2);

			Draw(gdi);
		}

		public virtual void Remove()
		{
			Dispose();
		}

		#region IDisposable Members

		~Figure()
		{
			Finalize(false);
		}

		public void Dispose()
		{
			Finalize(true);
			GC.SuppressFinalize(this);
		}

		protected void Finalize(bool disposing)
		{
			if (disposing == true)
			{
				// Free other state (managed objects).
				this._gdi = null;
			}

			// Free your own state (unmanaged objects).
			// Set large fields to null.
		}

		#endregion
	}
}
