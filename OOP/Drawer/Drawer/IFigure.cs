﻿using System;

namespace Drawer
{
	interface IFigure
	{
		GDI Gdi
		{
			get;
			set;
		}
		Figure Copy();
		void Draw(GDI gdi);
		void Move(GDI gdi, int x, int y);
		void Remove();
	}
}
