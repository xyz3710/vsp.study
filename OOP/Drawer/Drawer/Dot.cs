using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Drawer
{
	public class Dot : OnePointFigure
	{
		private int _width;

		public Dot(Graphics dc, Color penColor, int x, int y)
			: this(dc, penColor, x, y, 1)
		{
		}

		public Dot(Graphics dc, Color penColor, int x, int y, int width)
			: base(dc, penColor, x, y)
		{
			_width = width;
		}
	
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		public override Figure Copy()
		{
			return new Dot(_gdi.DC, _gdi.PenColor, _gdi.Location.X, _gdi.Location.Y);
		}

		public override void Draw(GDI gdi)
		{
			base.Draw(gdi);
		}

		public override void Move(GDI gdi, int x, int y)
		{
			base.Move(gdi, x, y);
		}

		public override void Remove()
		{
			_gdi.PenColor = BOARD_COLOR;
			Draw(_gdi);

			base.Remove();
		}
	}
}
