﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LambdaExpression;

namespace LinqQueryableInFX
{
	class Program
	{
		static void Main(string[] args)
		{
			"Linq Queryable test in .Net Framework".PrintBar();
			LinqQueryableTest linqQueryableTest = new LinqQueryableTest();
			linqQueryableTest.TestAll();

			"Group join test".PrintBar();
			GroupJoinTest.GroupJoinEx1();

			"Aggregate test".PrintBar();
			List<string> items = new string[] { "a", "b", "c", "d", "e" }.ToList();
			string aggregateItem = items.Aggregate((aggregated, next) => String.Format("{0}{1}{2}", aggregated, ", ", next));
			Console.WriteLine(string.Format("aggregate item : {0}.", aggregateItem));

			MinusTest.Test();
		}


	}
}
