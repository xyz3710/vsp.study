﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LambdaExpression;

namespace LinqQueryableInFX
{
	public class LinqQueryableTest
	{
		/// <summary>
		/// LinqQueryableTest class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public LinqQueryableTest()
		{
			InitCustomersRelation();
		}

		public List<Customer> Customers
		{
			get;
			set;
		}

		public List<SubCustomer> SubCustomers
		{
			get;
			set;
		}

		private void InitCustomersRelation()
		{
			Customer ibm = new Customer
			{
				Name = "IBM",
				Code = 1
			};
			Customer apple = new Customer
			{
				Name = "Apple",
				Code = 2
			};
			Customer hp = new Customer
			{
				Name = "HP",
				Code = 5
			};
			Customer samsung = new Customer
			{
				Name = "Samsung",
				Code = 3
			};
			Customer dell = new Customer
			{
				Name = "Dell",
				Code = 4
			};
			Customer hunjoo = new Customer
			{
				Name = "HyunJoo",
				Code = 6
			};
			Customers = new List<Customer>(new Customer[] {
				ibm,
				apple,
				hp,
				samsung,
				dell,
				hunjoo,
			});
			SubCustomers = new List<SubCustomer>(new SubCustomer[] {
				new SubCustomer{ Name = "Sub Hp", Code = 1001, Parent = hp, Reliable = true },
				new SubCustomer{ Name = "Sub Samsung", Code = 1002, Parent = samsung, Reliable = true },
				new SubCustomer{ Name = "Sub Ssamsong", Code = 1003, Parent = samsung, Reliable = false },
				new SubCustomer{ Name = "Sub Dell", Code = 1004, Parent = dell, Reliable = true },
				new SubCustomer{ Name = "Sub IBM", Code = 1005, Parent = ibm, Reliable = true },
				new SubCustomer{ Name = "Sub apple", Code = 1006, Parent = apple, Reliable = true },
				new SubCustomer{ Name = "iPhone", Code = 1007, Parent = apple, Reliable = true },
			});
		}

		private IOrderedEnumerable<Customer> OrderByName()
		{
			return Customers.OrderBy<Customer, string>(c => c.Name);
		}

		private IOrderedEnumerable<Customer> OrderByCodeDesc()
		{
			return Customers.OrderByDescending(c => c.Code);
		}
		
		private void PrintCustomers(IEnumerable<Customer> customers)
		{
			foreach (Customer customer in customers)
				Console.WriteLine(customer);
		}

		public void AggregateBody()
		{
			"Aggregate".PrintBar();
			
			string sentence = "the quick brown fox jumps over the lazy dog";
			Console.WriteLine("Original sentence : {0}", sentence);

			// Split the string into individual words.
			string[] words = sentence.Split(' ');
			Console.WriteLine("Split words");

			Console.WriteLine("Aggregate splited words");
			// Prepend each word to the beginning of the new sentence to reverse the word order.
			string reversed = words.Aggregate((workingSentence, next) => next + " " + workingSentence);

			Console.WriteLine(reversed);

			"Aggregate two types".PrintBar();

			"Aggregate three types".PrintBar();
			string[] fruits = { "apple", "mango", "orange", "passionfruit", "grape" };

			// Determine whether any string in the array is longer than "banana".
			string longestName = fruits.Aggregate("banana",
									(longest, next) => next.Length > longest.Length ? next : longest,
				// Return the final result as an upper case string.
									fruit => fruit.ToUpper());
			Console.WriteLine("The fruit with the longest name is {0}.", longestName);

			string splitter = fruits.Aggregate("",
				(aggregate, next) =>
				{
					Console.WriteLine("a:{0}\tn:{1}", aggregate, next);

					if (aggregate == string.Empty)
						return next;

					return aggregate + "," + next;
				},
				result => result);

			Console.WriteLine(splitter);
		}

		public void AllBody()
		{
			bool all = Customers.All(customer => customer.Code < 10);
			("customer code < 10 All result : {0}" + all.ToString()).PrintBar();
		}

		public void AverageBody()
		{
			double avgCode = Customers.Average(c => c.Code);
			Console.WriteLine("Average is {0} in Customers", avgCode);
		}

		public void BinarySearchBody()
		{
			"Binary Search".PrintBar();
			Customer searchItem = new Customer
			{
				Name = "Dell"
			};
			int index = Customers.BinarySearch(searchItem);
			
			Console.WriteLine("Binary Search {0}, found index : {1}, result : {2}", searchItem, index, Customers[index]);
		}

		public void ConcatBody()
		{
			"Concat".PrintBar();

			List<Student> students = Student.GetStudents();
			List<Teacher> teachers = Teacher.GetTeachers();
			string whereCity = "전주";
			var peopleInSeoul =
					(from s in students
					 where s.City == whereCity
					 select String.Format("{0} {1} <= {2}", s.Last, s.First, s.Id))
				 .Concat(from t in teachers
						 where t.City == whereCity
						 select String.Format("{0} {1}, Id: {2}", t.Last, t.First, t.Id));

			Console.WriteLine(String.Format("다음 학생들과 선생님이 {0}에 삽니다.", whereCity));

			foreach (var person in peopleInSeoul)
				Console.WriteLine(person);
		}

		public void ConvertAllBody()
		{
			"ConvertAll Customer to Customer2".PrintBar();
			List<SubCustomer> customers2 = Customers.ConvertAll<SubCustomer>(delegate(Customer c)
			{
				return new SubCustomer
				{
					Code = c.Code,
					Name = c.Name,
					Reliable = true,
				};
			});
			foreach (SubCustomer customer2 in customers2)
				Console.WriteLine(customer2);
		}

		public void CountBody()
		{
			"Count with odd".PrintBar();
			Console.WriteLine(Customers.Count<Customer>(c => c.Code % 2 == 1));
		}

		public void DefaultIfEmptyBody()
		{
			"Default if empty".PrintBar();
			foreach (Customer customer in Customers.DefaultIfEmpty(new Customer
					{
						Name = "Empty",
						Code = 999
					}))
				Console.WriteLine(customer);

			Console.WriteLine(Customers.DefaultIfEmpty(new Customer
			{
				Name = "Empty",
				Code = 999
			}).ElementAtOrDefault(8));
		}

		public void DistinctBody()
		{
			"Distinct".PrintBar();
			PrintCustomers(Customers.Distinct());
		}

		public void FirstBody()
		{
			"Code가 짝수인 첫번째 Element".PrintBar();
			Console.WriteLine(Customers.First(c => c.Code % 2 == 0));
		}

		public void GroupByBody()
		{
			"Group by".PrintBar();
			IEnumerable<IGrouping<bool, Customer>> customersGroupBy = Customers.GroupBy(c => c.Code % 2 == 0);
			foreach (IGrouping<bool, Customer> iGroupingBoolCustomer in customersGroupBy)
			{
				if (iGroupingBoolCustomer.Key == true)
					Console.WriteLine("\tCode is even");
				else
					Console.WriteLine("\tCode as odd");

				PrintCustomers(iGroupingBoolCustomer.ToList());
			}
		}

		public void GroupJoinBody()
		{
			"Group join".PrintBar();
			var groupJoinQuery = Customers.GroupJoin(
				SubCustomers,
				customer => customer,
				subCustomer => subCustomer.Parent,
				(customer, subCustomerCollection) => new
				{
					TopName = customer.Name,
					TopCode = customer.Code,
					SubCustomers = subCustomerCollection
				});

			foreach (var item in groupJoinQuery)
			{
				Console.WriteLine("Parent : {0}, {1}", item.TopName, item.TopCode);

				foreach (SubCustomer sub in item.SubCustomers)
					Console.WriteLine("\t{0}", sub);
			}
		}

		private void IntersecBody()
		{
			"Inersect".PrintBar();
			var intersecQuery = Customers.Intersect(new Customer[] { Customers[4], Customers[1] });
			
			foreach (Customer item in (intersecQuery as IEnumerable<Customer>))
				Console.WriteLine(item);
		}

		public void JoinBody()
		{
			"Join".PrintBar();
			var joinQuery = Customers.Join(
				SubCustomers,
				c => c,
				sc => sc.Parent,
				(customer, subCustomer) => new
				{
					ParentName = customer.Name,
					ParentCode = customer.Code,
					SubCustomer = subCustomer,
				});

			foreach (var item in joinQuery)
				Console.WriteLine("Parent : {0}, {1}\r\n\tSubCustomer : {2}", item.ParentName, item.ParentCode, item.SubCustomer);
		}

		public void LastBody()
		{
			"Last".PrintBar();
			Console.WriteLine(Customers.Last());
			Console.WriteLine(Enumerable.Last<Customer>(Customers));
			Console.WriteLine(Customers.Last(c => c.Code < 3));
		}

		public void MinMaxBody()
		{
			"Max & Min".PrintBar();
			Console.WriteLine("Max : {0}", Customers.Max());
			Console.WriteLine("Max : {0}", Customers.Max(c =>
			{
				Console.Write("{0}:{1}\t", c.Name, c.Name.Length);

				return c.Name.Length;
			}));
			Console.WriteLine("Min : {0}", Customers.Min());
			Console.WriteLine("Min Code : {0}", Customers.Min(c => c.Code));
		}

		public void ReverseBody()
		{
			"Original".PrintBar();
			PrintCustomers(Customers);
			"Reverse".PrintBar();
			Customers.Reverse();
			PrintCustomers(Customers);
			Customers.Reverse();
		}

		/// <summary>
		/// Convert Customer to Customer2 with Select Extension
		/// </summary>
		public void ConvertWithSelectBody()
		{
			Console.WriteLine("Convert Customer to Customer2 with Select Extension");
			IEnumerable<SubCustomer> subCustomers = Customers.Select<Customer, SubCustomer>(c => new SubCustomer
			{
				Name=c.Name,
				Code = c.Code,
				Reliable = true,
				Parent = null,
			});

			foreach (SubCustomer subCustomer in subCustomers)
				Console.WriteLine(subCustomer);
		}

		public void SelectForNewClassBody()
		{
			"Select for new class to var".PrintBar();
			var selectQuery = Customers.Select(c => new
			{
				Kind = c.Code % 2 == 0 ? "Major" : "Minor",
				Name = c.Name,
				Code = c.Code,
			});

			foreach (var item in selectQuery)
				Console.WriteLine("Kind : {0}, Name : {1}, Code : {2}", item.Kind, item.Name, item.Code);
		}

		public void SkipBody()
		{
			"Skip 3 index".PrintBar();
			PrintCustomers(Customers.Skip(3));

			"Skip code is even".PrintBar();
			List<Customer> customers = Customers.Select(c => c).ToList();

			customers.AddRange(SubCustomers.Select(sc => sc.ToCustomer()));
			PrintCustomers(customers.SkipWhile(c => c.Code < 100));
		}

		public void SumBody()
		{
			"Code Sum".PrintBar();
			Console.WriteLine(Customers.Sum(c => c.Code));
		}

		public void TakeBody()
		{
			"Take inner 3 items".PrintBar();
			PrintCustomers(Customers.Take(3));

			"Take Code inner 4 items".PrintBar();
			PrintCustomers(Customers.TakeWhile(c => c.Code <= 4));
		}

		public void ToDictionaryBody()
		{
			"Code key to ToDictionary".PrintBar();
			foreach (int intItem in Customers.ToDictionary(c => c.Code).Keys)
				Console.WriteLine("Key : {0}", intItem);
		}

		public void UnionBody()
		{
			"Union SubCustomers to Customers".PrintBar();
			List<Customer> unionCustomers = SubCustomers.ConvertAll<Customer>(delegate(SubCustomer sc)
			{
				return new Customer
				{
					Name = sc.Name,
					Code = sc.Code,
				};
			});
			PrintCustomers(unionCustomers);
			Console.WriteLine("\tAfter Union and order by Name");
			PrintCustomers(Customers.Union(unionCustomers).OrderBy(c => c.Name));
		}

		public void OrderByBody()
		{
			"Order by name".PrintBar();
			PrintCustomers(OrderByName());

			"Order by code descending".PrintBar();
			PrintCustomers(OrderByCodeDesc());
		}

		public void TestAll()
		{
			"Original data".PrintBar();
			PrintCustomers(Customers);

			AggregateBody();
			AllBody();
			AverageBody();
			BinarySearchBody();
			ConcatBody();
			ConvertAllBody();
			CountBody();
			DefaultIfEmptyBody();
			DistinctBody();
			FirstBody();
			GroupByBody();
			GroupJoinBody();
			IntersecBody();
			JoinBody();
			LastBody();
			MinMaxBody();
			ReverseBody();
			ConvertWithSelectBody();
			SelectForNewClassBody();
			SkipBody();
			SumBody();
			TakeBody();
			ToDictionaryBody();
			UnionBody();
			OrderByBody();
		}
	}
}
