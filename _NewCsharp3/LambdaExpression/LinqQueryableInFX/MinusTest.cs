﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LambdaExpression;
using System.Linq.Expressions;
using System.Diagnostics;
using System.Collections;

namespace LinqQueryableInFX
{
	public static class MinusTest
	{
		#region Minus
		/// <summary>
		/// outer 컬렉션에 없는 inner 컬렉션의 항목만을 구합니다.
		/// </summary>
		/// <typeparam name="T">null이 가능한 구조체 타입입니다.</typeparam>
		/// <param name="inner">기준이 되는 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 컬렉션입니다.</param>
		/// <returns>inner 컬렉션과 작거나 같은 값이 나옵니다.</returns>
		public static IEnumerable<T> Minus<T>(
			this IEnumerable<T> inner,
			IEnumerable<T> outer)
			where T : struct
		{
			return from i in inner
				   from o in
					   (from o in outer
						where o.Equals(i)
						select (T?)o
					   ).DefaultIfEmpty()
				   where o == null
				   select i;
		}

		/// <summary>
		/// outer 컬렉션에 없는 inner 컬렉션의 항목만을 구합니다.
		/// </summary>
		/// <typeparam name="T">클래스 타입입니다.</typeparam>
		/// <typeparam name="TKey">키가되는 타입입니다.</typeparam>
		/// <param name="inner">기준이 되는 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 컬렉션입니다.</param>
		/// <param name="compareKeyExpression">T 타입에서 비교하려는 key의 표현식입니다.</param>
		/// <returns>inner 컬렉션과 작거나 같은 값이 나옵니다.</returns>
		public static IEnumerable<T> Minus<T, TKey>(
			this IEnumerable<T> inner,
			IEnumerable<T> outer,
			Expression<Func<T, TKey>> compareKeyExpression)
			where T : class
		{
			var compiledCondition = compareKeyExpression.Compile();
			return from i in inner
				   from o in
					   (from o in outer
						where compiledCondition(o).Equals(compiledCondition(i))
						select o
					   ).DefaultIfEmpty()
				   where o == null
				   select i;
		}

		/// <summary>
		/// outer 컬렉션에 없는 inner 컬렉션의 항목만을 구합니다.
		/// </summary>
		/// <param name="inner">기준이 되는 문자열 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 문자열 컬렉션입니다.</param>
		/// <returns>inner 컬렉션과 작거나 같은 값이 나옵니다.</returns>
		public static IEnumerable<string> Minus(
			this IEnumerable<string> inner,
			IEnumerable<string> outer)
		{
			return inner.Minus(outer, x => x);
		}
		#endregion

		#region GetInsertUpdate<T, TKey>
		/// <summary>
		/// inner 컬렉션과 outer 컬렉션을 비교하여 같은 항목은 updateCollection에 outer에 없는 항목은 insertCollection에 저장합니다.
		/// </summary>
		/// <typeparam name="T">null이 가능한 구조체 타입입니다.</typeparam>
		/// <param name="inner">기준이 되는 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 컬렉션입니다.</param>
		/// <param name="insertCollection">추가할 항목을 반환합니다.</param>
		/// <param name="updateCollection">수정할 항목을 반환합니다.</param>
		public static void GetInsertUpdate<T>(
			this IEnumerable<T> inner,
			IEnumerable<T> outer,
			out IEnumerable<T> insertCollection,
			out IEnumerable<T> updateCollection)
			where T : struct
		{
			insertCollection = outer.Minus(inner);
			updateCollection = inner.Intersect(outer);
		}

		/// <summary>
		/// inner 컬렉션과 outer 컬렉션을 비교하여 같은 항목은 updateCollection에 outer에 없는 항목은 insertCollection에 저장합니다.
		/// </summary>
		/// <typeparam name="T">클래스 타입입니다.</typeparam>
		/// <typeparam name="TKey">키가 되는 타입입니다.</typeparam>
		/// <param name="inner">기준이 되는 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 컬렉션입니다.</param>
		/// <param name="compareKeyExpression">T 타입에서 비교하려는 key의 표현식입니다.</param>
		/// <param name="insertCollection">추가할 항목을 반환합니다.</param>
		/// <param name="updateCollection">수정할 항목을 반환합니다.</param>
		public static void GetInsertUpdate<T, TKey>(
			this IEnumerable<T> inner,
			IEnumerable<T> outer,
			Expression<Func<T, TKey>> compareKeyExpression,
			out IEnumerable<T> insertCollection,
			out IEnumerable<T> updateCollection)
			where T : class
		{
			insertCollection = outer.Minus(inner, compareKeyExpression);

			// 아래와 같이 하면 아래와 같은 오류가 발생한다.
			// An IQueryable that returns a self-referencing Constant expression is not supported.
			// 자신을 참조하는 Constant 식을 반환하는 IQueryable은 사용할 수 없습니다.
			//updateCollection = inner.AsQueryable().
			//	Join(outer.AsQueryable(), compareKeyExpression, compareKeyExpression, (x, y) => x);			
			var compiledCondition = compareKeyExpression.Compile();

			updateCollection = from i in inner
							   join o in outer on compiledCondition(i) equals compiledCondition(o)
							   select o;
		}

		/// <summary>
		/// inner 컬렉션과 outer 컬렉션을 비교하여 같은 항목은 updateCollection에 outer에 없는 항목은 insertCollection에 저장합니다.
		/// </summary>
		/// <param name="inner">기준이 되는 문자열 컬렉션입니다.</param>
		/// <param name="outer">비교하려는 문자열 컬렉션입니다.</param>
		/// <param name="insertCollection">추가할 문자열 항목을 반환합니다.</param>
		/// <param name="updateCollection">수정할 문자열 항목을 반환합니다.</param>
		public static void GetInsertUpdate(
			this IEnumerable<string> inner,
			IEnumerable<string> outer,
			out IEnumerable<string> insertCollection,
			out IEnumerable<string> updateCollection)
		{
			insertCollection = outer.Minus(inner);
			updateCollection = inner.Intersect(outer);
		}
		#endregion

		internal static void Test()
		{
			// arr1에 있으면 arr2 Update, arr1에 없으면 arr2 Insert 기능을 위해 
			"두 배열간 minus 테스트".PrintBar();
			var arr1 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			var arr2 = new int[] { 2, 4, 5, 6, 8, 10, 11, 12, 13 };
			//arr1.Intersect
			// Update: 2, 4, 6, 8, 10
			// Insert: 11, 12, 13
			var minusResult1 = from a1 in arr1
							   from a2 in
								   (from a2 in arr2
									where a2 == a1
									select (int?)a2
									).DefaultIfEmpty()
							   where a2 == null
							   select a1;

			Console.WriteLine(string.Format("minus1 테스트 결과 : {0}",
				minusResult1.Aggregate<int, string>(
											string.Empty,
											(acc, next) =>
											{
												string nextValue = next.ToString();

												if (acc == string.Empty)
													return nextValue;

												return string.Format("{0}{1}{2}", acc, ", ", nextValue);
											})));

			var minusResult2 = arr1.Minus(arr2);
			Console.WriteLine(string.Format("minus2 테스트 결과 : {0}",
				minusResult2.Aggregate<int, string>(
											string.Empty,
											(acc, next) =>
											{
												string nextValue = next.ToString();

												if (acc == string.Empty)
													return nextValue;

												return string.Format("{0}{1}{2}", acc, ", ", nextValue);
											})));

			var p1 = Man.GetSampleData1();
			var p2 = Man.GetSampleData2();
			var p3 = new Man[] { };
			var s1 = new string[] { "a", "b", "c", "d", "e", "f", "g", "h" };
			var s2 = new string[] { "b", "d", "f", "h", "i" };
			var minusResult3 = p1.Minus(p2, x => x.Id);
			var minusResult4 = s1.Minus(s2);
			var minusResult5 = p2.Minus(p1, x => x.Id);
			var union = p1.Union(p2, new Man()).ToList();
			var aa1 = new int[] { 1, 2, 3, 4, 5 };
			var aa2 = new int[] { 2, 3, 6 };

			IEnumerable<Man> insert1 = new List<Man>();
			IEnumerable<Man> update1 = new List<Man>();
			p1.GetInsertUpdate(p2, x => x.Id, out insert1, out update1);

			IEnumerable<string> insert2 = new List<string>();
			IEnumerable<string> update2 = new List<string>();
			s1.GetInsertUpdate(s2, out insert2, out update2);

			IEnumerable<int> insert3 = new List<int>();
			IEnumerable<int> update3 = new List<int>();
			aa1.GetInsertUpdate(aa2, out insert3, out update3);
		}
	}

	[DebuggerDisplay("Id:{Id}, Name:{Name}", Name = "Man")]
	public class Man : IEqualityComparer<Man>
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		public static IEnumerable<Man> GetSampleData1()
		{
			return new List<Man>
			{
				new Man{ Id = 1, Name = "Man1" },
				new Man{ Id = 2, Name = "Man2" },
				new Man{ Id = 4, Name = "Man4" },
				new Man{ Id = 5, Name = "Man5" },
				new Man{ Id = 6, Name = "Man6" },
				new Man{ Id = 7, Name = "Man7" },
				new Man{ Id = 8, Name = "Man8" },
				new Man{ Id = 9, Name = "Man9" },
				new Man{ Id = 10, Name = "Man10" },
			};
		}

		public static IEnumerable<Man> GetSampleData2()
		{
			return new List<Man>
			{
				new Man{ Id = 2, Name = "Man2" },
				new Man{ Id = 3, Name = "Man3" },
				new Man{ Id = 4, Name = "Man4" },
				new Man{ Id = 5, Name = "Man5" },
				new Man{ Id = 6, Name = "Man6" },
				new Man{ Id = 8, Name = "Man8" },
				new Man{ Id = 10, Name = "Man10" },
				new Man{ Id = 11, Name = "Man11" },
				new Man{ Id = 12, Name = "Man12" },
				new Man{ Id = 13, Name = "Man13" },
				new Man{ Id = 14, Name = "Man14" },
			};
		}

		#region IEqualityComparer<Man> 멤버

		public bool Equals(Man x, Man y)
		{
			return x.Id == y.Id;
		}

		public int GetHashCode(Man man)
		{
			//Check whether the object is null
			if (Object.ReferenceEquals(man, null))
				return 0;

			//Get hash code for the Name field if it is not null.
			int hashmanName = man.Name == null ? 0 : man.Name.GetHashCode();

			//Get hash code for the Code field.
			int hashmanCode = man.Id.GetHashCode();

			//Calculate the hash code for the man.
			return hashmanName ^ hashmanCode;
		}

		#endregion
	}
}
