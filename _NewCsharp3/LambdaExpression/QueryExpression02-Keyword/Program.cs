﻿using System;
using System.Collections.Generic;
using System.Text;
using LambdaExpression;
using System.Linq;
using System.Data.Linq;

namespace QueryExpression02_Keyword
{
	class Program
	{
		public static List<Student> Students
		{
			get
			{
				return Student.GetStudents();
			}
		}

		public static List<Teacher> Teachers
		{
			get
			{
				return Teacher.GetTeachers();
			}
		}

		static void Main(string[] args)
		{
			var studentGroup =
				from s in Students
				group s by s.Last[0];

			foreach (var student in studentGroup)
			{
				Console.WriteLine(student.Key);

				foreach (Student s in student)
					Console.WriteLine("{0}   {1}", s.Last, s.First);
			}


			LetTest();
		}

		private static void LetTest()
		{
			"let keyword 1 모음이 포함된 단어만...".PrintBar();
			string[] strings = 
			{
				"A penny saved is a penny earned.",
				"The early bird catches the worm.",
				"The pen is mightier than the sword." 
			};

			// Split the sentence into an array of words
			// and select those whose first letter is a vowel.
			var earlyBirdQuery =
				from sentence in strings
				let words = sentence.Split(' ')
				from word in words
				let w = word.ToLower()
				where w[0] == 'a' || w[0] == 'e'
					|| w[0] == 'i' || w[0] == 'o'
					|| w[0] == 'u'
				select word;

			// Execute the query.
			foreach (var v in earlyBirdQuery)
				Console.WriteLine("\"{0}\" starts with a vowel", v);

			"let keyword 2 중간값 계산의 성능 향상".PrintBar();

			var studentsLet =
				from s in Students
				let totalScore = s.Scores.Aggregate((sum, score) => sum += score)
				where (totalScore / s.Scores.Count) < s.Scores[0]
				select String.Format("{0}\t{1}\t합계: {2:000}, 평균: {3}", s.Last, s.First, totalScore, (totalScore / (double)s.Scores.Count));

			foreach (string item in studentsLet)
				Console.WriteLine(item);
		}
	}
}
