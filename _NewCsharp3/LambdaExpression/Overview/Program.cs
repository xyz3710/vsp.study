﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using LambdaExpression;

namespace LambdaExpression01
{
	class Program
	{
		static void Main(string[] args)
		{
			"Lambda Expression declare test".PrintBar();
			DeclareTest.TestMethod();

			"Lambda Expression conversions test".PrintBar();
			ConversionsTest.TestMethod();

			"Lambda Expression type inference test".PrintBar();
			TypeInferenceTest.TestMethod();
		}
	}
}
