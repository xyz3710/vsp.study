﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LambdaExpression01
{
	class ConversionsTest
	{
		/// <summary>
		/// Lambda Expression Conversions test method
		/// </summary>
		public static void TestMethod()
		{
			Func<int, int> f1 = x => x + 1;
			Func<int, double> f2 = x => x + 0.5d;
			Func<double, int> f3 = x => (int)(x + 1);

			Console.WriteLine("f1(1) : {0}", f1(1));
			Console.WriteLine("f2(1) : {0}", f2(1));
			Console.WriteLine("f3(1.5) : {0}", f3(1.5));
		}
	}
}
