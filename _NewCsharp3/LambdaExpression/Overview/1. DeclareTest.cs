﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LambdaExpression01
{
	class DeclareTest
	{
		/// <summary>
		/// Lambda Expression Declare test method
		/// </summary>
		public static void TestMethod()
		{
			// Implicitly typed, expression body
			Func<int, int> f1 = x => x + 1;
			// Implicityly typed, statement body
			Func<int, int> f2 = x =>
			{
				return x + 1;
			};
			// Explicitly typed, expression body
			Func<int, int> f3 = (int x) => x + 1;
			// Explicitly typed, statement body
			Func<int, int> f4 = (int x) =>
			{
				return x + 1;
			};
			// Multiple parameters
			Func<int, int, int> f5 = (x, y) => x * y;
			// No parameters
			Func<int> f6 = () =>
			{
				Console.WriteLine("blank");

				return 0;
			};

			Console.WriteLine("f1(1) : {0}", f1(1));
			Console.WriteLine("f2(1) : {0}", f2(1));
			Console.WriteLine("f3(1) : {0}", f3(1));
			Console.WriteLine("f4(1) : {0}", f4(1));
			Console.WriteLine("f5(2, 3) : {0}", f5(2, 3));
			Console.WriteLine("f6(1) : {0}", f6());
		}
	}
}
