﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LambdaExpression01
{
	class TypeInferenceTest
	{
		/// <summary>
		/// Lambda Expression Type inference test method
		/// </summary>
		public static void TestMethod()
		{
			double seconds = F(
                                 "1:15:30", 
                                 s => TimeSpan.Parse(s), 
                                 t => t.TotalSeconds);

			Console.WriteLine("seconds : {0}", seconds);
		}

		/// <summary>
		/// X의 값을 f1의 input argument로 입력 후, f1의 return Y를 f2의 input argument Y로 입력 후 Z의 return으로 구합니다.
		/// </summary>
		/// <typeparam name="X"></typeparam>
		/// <typeparam name="Y"></typeparam>
		/// <typeparam name="Z"></typeparam>
		/// <param name="value"></param>
		/// <param name="f1"></param>
		/// <param name="f2"></param>
		/// <returns></returns>
		public static Z F<X, Y, Z>(X value, Func<X, Y> f1, Func<Y, Z> f2)
		{
			return f2(f1(value));
		}        
	}
}
