﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LambdaExpression;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace QuerExpression01
{
	class Program
	{
		static void Main(string[] args)
		{
			QueryExpressionTest querExpressionTest = new QueryExpressionTest();

			querExpressionTest.TestMethod();

			// other test
			string text = "Test1:10,Test2:2,Test3:3";
			string[] items = text.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
			string CODE = "Code";
            string QTYVALUE = "QtyValue";
            Regex regex = new Regex(String.Format(@"(?<{0}>[\w\d]*)\:(?<{1}>[\d]*)[,]?", CODE, QTYVALUE), RegexOptions.Compiled);

			var result = regex.Matches(text)
				.Cast<Match>()
				.Select(m => new ReworkItem
				{
					CodeId = m.Groups[CODE].Value,
					QtyValue = Convert.ToInt32(m.Groups[QTYVALUE].Value),
				});
			/*
			string result2 = items.Aggregate((acc, next) => string.Format("{0}, {1}", acc, next));
			*/
			text =result.Aggregate<ReworkItem, string>(
				string.Empty,
				(aggregate, next) =>
				{
					if (aggregate == string.Empty)
						return next.ToString();

					return string.Format("{0}{1}{2}", aggregate.ToString(), ",", next.ToString());
				});

			Console.WriteLine(text);
		}

		private string ConvertItemBySeperator<T>(IList<T> items)
		{
			if (items.Count == 0)
				return string.Empty;

			return items.Aggregate<T, string>(
				string.Empty,
				(aggregate, next) =>
				{
					if (aggregate == string.Empty)
						return next.ToString();

					return String.Format("{0}{1}{2}", aggregate, ",", next);
				});
		}
	}

	public struct ReworkItem
	{
		/// <summary>
		/// CodeId를 구하거나 설정합니다.
		/// </summary>
		public string CodeId
		{
			get;
			set;
		}

		/// <summary>
		/// QtyValue를 구하거나 설정합니다.
		/// </summary>
		public int QtyValue
		{
			get;
			set;
		}

		#region ToString
		/// <summary>
		/// 를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("{0}:{1}",
					CodeId, QtyValue);
		}
		#endregion		
	}
}
