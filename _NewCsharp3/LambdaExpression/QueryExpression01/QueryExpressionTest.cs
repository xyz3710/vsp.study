﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LambdaExpression;

namespace QuerExpression01
{
	class QueryExpressionTest
	{
		/// <summary>
		/// QuerExpressionTest 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public QueryExpressionTest()
		{
			InitCustomersRelation();
		}

		private void InitCustomersRelation()
		{
			Customer ibm = new Customer
			{
				Name = "IBM",
				Code = 1
			};
			Customer apple = new Customer
			{
				Name = "Apple",
				Code = 2
			};
			Customer hp = new Customer
			{
				Name = "HP",
				Code = 5
			};
			Customer samsung = new Customer
			{
				Name = "Samsung",
				Code = 3
			};
			Customer dell = new Customer
			{
				Name = "Dell",
				Code = 4
			};
			Customer hunjoo = new Customer
			{
				Name = "HyunJoo",
				Code = 6
			};
			Customers = new List<Customer>(new Customer[] {
				ibm,
				apple,
				hp,
				samsung,
				dell,
				hunjoo,
			});
			SubCustomers = new List<SubCustomer>(new SubCustomer[] {
				new SubCustomer{ Name = "Sub Hp", Code = 1001, Parent = hp, Reliable = true },
				new SubCustomer{ Name = "Sub Samsung", Code = 1002, Parent = samsung, Reliable = true },
				new SubCustomer{ Name = "Sub Ssamsong", Code = 1003, Parent = samsung, Reliable = false },
				new SubCustomer{ Name = "Sub Dell", Code = 1004, Parent = dell, Reliable = true },
				new SubCustomer{ Name = "Sub IBM", Code = 1005, Parent = ibm, Reliable = true },
				new SubCustomer{ Name = "Sub apple", Code = 1006, Parent = apple, Reliable = true },
				new SubCustomer{ Name = "iPhone", Code = 1007, Parent = apple, Reliable = true },
			});
		}
		
		public List<Customer> Customers
		{
			get;
			set;
		}

		public List<SubCustomer> SubCustomers
		{
			get;
			set;
		}

		public Dictionary<string, Customer> CustomersDic
		{
			get
			{
				return Customers.ToDictionary(c => c.Name);
			}
		}
		
		private void PrintCustomers(IEnumerable<Customer> customers)
		{
			foreach (Customer customer in customers)
				Console.WriteLine(customer);
		}

		public void TestMethod()
		{
			"from, where, select clause".PrintBar();
			var result1 = 
				from c in Customers
				where c.Name.ToLower().Contains("a")
				select c;
			
			PrintCustomers(result1);

			"group by clause".PrintBar();
			var result2 = 
				from c in Customers
				group c.Code by c.Name;

			foreach (IGrouping<string, int> item in result2)
				Console.WriteLine("Key : {0}, Value : {1}", item.Key, CustomersDic[item.Key]);
			
			Dictionary<string, IGrouping<string, int>> result2ToDictionary = result2.ToDictionary(r => r.Key);
			
			foreach (string key in result2ToDictionary.Keys)
				Console.WriteLine("Key : {0}, Value : {1}", key, result2ToDictionary);
			
		}
	}
}
