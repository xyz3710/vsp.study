﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace ExpressionParameter
{
	static class Program
	{
		private static IQueryable<Repository> GetDataSource()
		{
			return new List<Repository>(new Repository[]
											{
												new Repository
												{
													ID = 1,
													Name = "김기원",
													Age = 38,
												},
												new Repository
												{
													ID = 2,
													Name = "황태영",
													Age = 37,
												},
											}).AsQueryable();
		}

		static void Main(string[] args)
		{
			var dataSource = GetDataSource();

			dataSource.Where(x => x.ID == 1);

			IList<SelectListItem> selectListItems = Extensions<Repository>.GetSelectListItems(
							dataSource,
							x => x.Name,
							x => x.ID,
							x => x.ID
						);
		}

	}

	static class Extensions<TModel>
	{
		public static IList<SelectListItem> GetSelectListItems(
			IQueryable<TModel> modelSource,
			Expression<Func<TModel, string>> textExpression,
			Expression<Func<TModel, int>> valueExpression,
			Expression<Func<TModel, int>> selectExpression,
			int selectedId = 0)
		{
			var itemList = new List<SelectListItem>();
			var compiledText = textExpression.Compile();
			var compiledValue = valueExpression.Compile();
			var compiledSelect = selectExpression.Compile();

			// Member 이름을 구할 때
			string memberName = ExpressionHelper.GetExpressionText(textExpression);
			MemberExpression toMemberExpression = ToMemberExpression(textExpression);
			// Member의 type을 구할 때
			Type type = toMemberExpression.Type;

			var model = modelSource
				.Select(x => new SelectListItem
				{
					Text = compiledText(x),
					Value = Convert.ToString(compiledValue(x)),
					Selected = compiledSelect(x) == selectedId,
				});

			itemList = model.ToList();

			return itemList;
		}

		public static MemberExpression ToMemberExpression(LambdaExpression expression)
		{
			MemberExpression memberExpression = expression.Body as MemberExpression;

			if (memberExpression == null)
			{
				UnaryExpression unaryExpression = expression.Body as UnaryExpression;

				if (unaryExpression != null)
				{
					memberExpression = unaryExpression.Operand as MemberExpression;
				}
			}

			return memberExpression;
		}
	}
}
