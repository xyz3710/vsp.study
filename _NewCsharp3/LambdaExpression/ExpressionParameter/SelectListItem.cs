﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionParameter
{
	public class SelectListItem
	{
		/// <summary>
		/// Text를 구하거나 설정합니다.
		/// </summary>
		public string Text
		{
			get;
			set;
		}

		/// <summary>
		/// Value를 구하거나 설정합니다.
		/// </summary>
		public string Value
		{
			get;
			set;
		}

		/// <summary>
		/// Selected를 구하거나 설정합니다.
		/// </summary>
		public bool Selected
		{
			get;
			set;
		}
	}
}
