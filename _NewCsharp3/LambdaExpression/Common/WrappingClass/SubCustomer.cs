using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LambdaExpression
{
	public class SubCustomer
	{
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Code를 구하거나 설정합니다.
		/// </summary>
		public int Code
		{
			get;
			set;
		}
		/// <summary>
		/// Parent를 구하거나 설정합니다.
		/// </summary>
		public Customer Parent
		{
			get;
			set;
		}

		/// <summary>
		/// Reliable를 구하거나 설정합니다.
		/// </summary>
		public bool Reliable
		{
			get;
			set;
		}

		#region ToString
		/// <summary>
		/// Customer2를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("Name:{0}, Code:{1}, ParentCode:{2}, Reliable:{3}",
					Name, Code, Parent, Reliable);
		}
		#endregion

		public Customer ToCustomer()
		{
			return new Customer
			{
				Name = Name,
				Code = Code,
			};
		}
	}
}
