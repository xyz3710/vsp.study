﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LambdaExpression
{
	public class Pet
	{
		public string Name
		{
			get;
			set;
		}

		public Person Owner
		{
			get;
			set;
		}
	}
}
