﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace LambdaExpression
{
	[DebuggerDisplay("First:{First}, Last:{Last}, Id:{Id}, City:{City}, Subject:{Subject}", Name="Teacher")]
	public class Teacher : SchoolPerson
	{		
		/// <summary>
		/// 과목를 구하거나 설정합니다.
		/// </summary>
		public string Subject
		{
			get;
			set;
		}

		public static List<Teacher> GetTeachers()
		{
			return new List<Teacher>
			{
				new Teacher{
					First = "선생1",
					Last = "최",
					Id = 945,
					City = "서울",
				},
				new Teacher{
					First = "선생2",
					Last = "홍",
					Id = 956,
					City = "부산",
				},
				new Teacher{
					First = "선생1",
					Last = "심",
					Id = 972,
					City = "대구",
				},
				new Teacher{
					First = "선생9",
					Last = "최",
					Id = 948,
					City = "전주",
				},
				new Teacher{
					First = "일한",
					Last = "김",
					Id = 987,
					City = "전주",
				},
			};
		}
	}
}
