﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace LambdaExpression
{
	[DebuggerDisplay("First:{First}, Last:{Last}, Id:{Id}, City:{City}", Name="SchoolPerson")]
	public class SchoolPerson
	{
		/// <summary>
		/// First를 구하거나 설정합니다.
		/// </summary>
		public string First
		{
			get;
			set;
		}
		
		/// <summary>
		/// Last를 구하거나 설정합니다.
		/// </summary>
		public string Last
		{
			get;
			set;
		}

		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// City를 구하거나 설정합니다.
		/// </summary>
		public string City
		{
			get;
			set;
		}
	}
}
