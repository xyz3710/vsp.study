﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace LambdaExpression
{
	[DebuggerDisplay("First:{First}, Last:{Last}, Id:{Id}, City:{City}, Street:{Street}, Scores:{Scores}", Name="Student")]
	public class Student : SchoolPerson
	{
		/// <summary>
		/// Street를 구하거나 설정합니다.
		/// </summary>
		public string Street
		{
			get;
			set;
		}

		/// <summary>
		/// Scores를 구하거나 설정합니다.
		/// </summary>
		public List<int> Scores
		{
			get;
			set;
		}

		public static List<Student> GetStudents()
		{
			return new List<Student>
			{
				new Student{
					First = "학생1",
					Last = "김",
					Id = 111,
					Street = "123 Main Street",
					City = "서울",
					Scores = new List<int>{ 97, 92, 81, 60 }
				},
				new Student{
					First = "학생2",
					Last = "이",
					Id = 112,
					Street = "124 Main Street",
					City = "부산",
					Scores = new List<int>{ 75, 84, 91, 39 }
				},
				new Student{
					First = "학생3",
					Last = "박",
					Id = 113,
					Street = "125 Main Street",
					City = "대구",
					Scores = new List<int>{ 88, 94, 65, 91 }
				},
				new Student{
					First = "기원",
					Last = "김",
					Id = 110,
					Street = "전주 인후동",
					City = "전주",
					Scores = new List<int>{ 100, 100, 100, 99 }
				},
			};
		}
	}
}
