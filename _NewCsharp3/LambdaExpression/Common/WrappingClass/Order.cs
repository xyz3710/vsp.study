using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace LambdaExpression
{
    [DebuggerDisplay("OrderId:{OrderId}, OrderDate:{OrderDate}, OrderAmount:{OrderAmount}", Name = "Order")]
    public class Order
    {
        /// <summary>
        /// OrderId를 구하거나 설정합니다.
        /// </summary>
        public string OrderId
        {
            get;
            set;
        }
        
        /// <summary>
        /// OrderDate를 구하거나 설정합니다.
        /// </summary>
        public DateTime OrderDate
        {
            get;
            set;
        }

        /// <summary>
        /// OrderAmount를 구하거나 설정합니다.
        /// </summary>
        public int OrderAmount
        {
            get;
            set;
        }

        #region ToString
        /// <summary>
        /// Order를 나타내는 String을 반환합니다. 
        /// </summary>
        /// <returns>전체 Property의 값</returns>
        public override string ToString()
        {
            return string.Format("OrderId:{0}, OrderDate:{1}, OrderAmount:{2}",
                    OrderId, OrderDate, OrderAmount);
        }
        #endregion
        
        public static Order RandomOrder()
        {
            Random id = new Random();
            Random amount = new Random(8);
            Random year = new Random();
            Random month = new Random();
            Random day = new Random(7);

            Thread.Sleep(10);

            return new Order
            {
                OrderId = string.Format("OR{0:0}", id.Next(1, 5)),
                OrderAmount = amount.Next(1000, 18000),
                OrderDate = DateTime.Parse(string.Format("{0}-{1}-{2}", year.Next(1995, 2010), month.Next(1, 12), day.Next(1, 31))),
            };
        }
    }
}

