using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace LambdaExpression
{
    [DebuggerDisplay("Name:{Name}, Code:{Code}, City:{City}, OrderDate:{OrderDate}", Name = "Customer")]
	public class Customer : IComparable
	{
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Code를 구하거나 설정합니다.
		/// </summary>
		public int Code
		{
			get;
			set;
		}

        /// <summary>
        /// City를 구하거나 설정합니다.
        /// </summary>
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// OrderDate를 구하거나 설정합니다.
        /// </summary>
        public Order Order
        {
            get;
            set;
        }
        
        #region ToString
        /// <summary>
        /// Customer를 나타내는 String을 반환합니다. 
        /// </summary>
        /// <returns>전체 Property의 값</returns>
        public override string ToString()
        {
            return string.Format("Name:{0}\tCode:{1}\tCity:{2}\tOrderDate:{3}",
                    Name, Code, City, Order);
        }
        #endregion

		#region IComparable 멤버

		public int CompareTo(object obj)
		{
			Customer customer = obj as Customer;

			if (customer != null)
				return customer.Name.CompareTo(Name);

			return -1;
		}

		#endregion
	}
}
