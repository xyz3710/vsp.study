﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace LambdaExpression
{
	public static class StringExtensions
	{
		/// <summary>
		/// 80개의 ******* bar를 변환 합니다.
		/// </summary>
		public static string Bar(this string text)
		{
			int totalWidth = (80 - text.Length - 2) / 2;

			return Bar(text, totalWidth);
		}

        /// <summary>
		/// 지정한 개수의 ******* bar를 변환 합니다.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="totalWidth"></param>
		public static string Bar(this string text, int totalWidth)
		{
			return string.Format(
                       "{0} {1} {2}", 
                       string.Empty.PadLeft(totalWidth, '*'), 
                       text, 
                       string.Empty.PadRight(totalWidth, '*'));
		}

		/// <summary>
		/// text 좌우에 ****** bar를 출력합니다.
		/// </summary>
		/// <param name="text"></param>
		public static void PrintBar(this string text)
		{
			Console.WriteLine(text.Bar());
		}

		public static IQueryable<TResult> Select<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
		{
			return source.Provider.CreateQuery<TResult>(Expression.Call(
                                                            null, 
                                                            ((MethodInfo)MethodBase.GetCurrentMethod()).MakeGenericMethod(new Type[] { typeof(TSource), typeof(TResult) }), 
                                                            new Expression[] { source.Expression, Expression.Quote(selector) }));
		}

	}
}
