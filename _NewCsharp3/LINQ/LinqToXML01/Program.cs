﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using System.Xml;

namespace LinqToXML01
{
	class Program
	{
		static void Main(string[] args)
		{
			List<Student> students = Student.GetStudents();
			List<Teacher> teachers = Teacher.GetTeachers();
			// create XML
			var studentToXML = 
				new XElement("Root",
					from s in students
					let x = ScoreAggregate(s.Scores)
					select new XElement("student",
							new XElement("이름", s.First),
							new XElement("성", s.Last),
							new XElement("점수", x)
						)	// end "student
				);

			Console.WriteLine(studentToXML);

			XmlWriterSettings xws = new XmlWriterSettings()
			{				
				Indent = true,
				IndentChars = "\t",
				ConformanceLevel = ConformanceLevel.Document,
				Encoding = Encoding.UTF8,
			};

			using (XmlWriter xw = XmlWriter.Create("Students0.xml", xws))
			{
				XContainer xContainer = studentToXML as XContainer;
				
				xContainer.WriteTo(xw);
			}
			
			/*
			using (XmlWriter xw = XmlWriter.Create("Students1.xml", xws))
			{
				studentToXML.WriteTo(xw);
				//xw.WriteEndElement();				// Error		
			}
			using (XmlWriter xw = XmlWriter.Create("Students2.xml", xws))
			{
				XDocument xDocument = new XDocument(
					new XDeclaration("1.0", "UTF-8", "yes"),
					studentToXML);

				xDocument.WriteTo(xw);
			}
			*/
		}

		private static string ScoreAggregate(IEnumerable<int> scores)
		{
			return scores.Aggregate(string.Empty,
			(accStr, score) =>
			{
				if (accStr == string.Empty)
					return score.ToString();

				return String.Format("{0}, {1}", accStr, score);
			});
		}
	}
}
