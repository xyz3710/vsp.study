﻿//#define PRINT_RSS

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace LinqToXML03
{
	class Program
	{
		static void Main(string[] args)
		{
			XElement rss = XElement.Load("http://xyz37.blog.me/rss");
			var rssItem = from item in rss.Descendants("channel").Descendants("item")
						  select new
						  {
							  Author = item.Element("author").Value,
							  Category = item.Element("category").Value,
							  Title = item.Element("title").Value,
							  RootLink = item.Ancestors("channel").Descendants("link").ElementAt(0).Value,
						  };

#if PRINT_RSS
			foreach (var item in rssItem)
			{
				Console.WriteLine(item.RootLink);
				Console.WriteLine(item.Author);
				Console.WriteLine(item.Category);
				Console.WriteLine(item.Title);
				Console.WriteLine(string.Empty.PadRight(70, '*'));
			}
#endif
			XElement weather = XElement.Load("http://www.kma.go.kr/XML/weather/sfc_web_map.xml");
			var weatherLocal = from item in weather.Descendants(AddNamespace(weather, "local"))
							   orderby item.Value
							   let date = item.Ancestors(AddNamespace(weather, "weather")).ElementAt(0)
							   select new
							   {
								   Local = item.Value,
								   Stn_id = item.Attribute("stn_id").Value,
								   Icon = item.Attribute("icon").Value,
								   Desc = item.Attribute("desc").Value,
								   Date = DateTime.Parse(string.Format(
															 "{0}-{1}-{2} {3}:00:00",
															 date.Attribute("year").Value,
															 date.Attribute("month").Value,
															 date.Attribute("day").Value,
															 date.Attribute("hour").Value)),
							   };

			foreach (var item in weatherLocal)
			{
				Console.WriteLine(item.Date);
				Console.WriteLine(item.Local);
				Console.WriteLine(item.Stn_id);
				Console.WriteLine(item.Icon);
				Console.WriteLine(item.Desc);
				Console.WriteLine(string.Empty.PadRight(70, '*'));
			}
		}
		private static string AddNamespace(XElement weather, string name)
		{
			return string.Format("{{{0}}}{1}", weather.Name.Namespace.NamespaceName, name);
		}
	}
}
