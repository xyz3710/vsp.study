namespace SqlServerQueryVisualizer {
    partial class QueryVisualizerForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.txtExpression = new System.Windows.Forms.TextBox();
			this.txtSql = new System.Windows.Forms.TextBox();
			this.btnQuery = new System.Windows.Forms.Button();
			this.chkUseOriginal = new System.Windows.Forms.CheckBox();
			this.radioQuery1 = new System.Windows.Forms.RadioButton();
			this.radioQuery2 = new System.Windows.Forms.RadioButton();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.btnClose = new System.Windows.Forms.Button();
			this.scTop = new System.Windows.Forms.SplitContainer();
			this.scMain = new System.Windows.Forms.SplitContainer();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.tlpMain.SuspendLayout();
			this.scTop.Panel1.SuspendLayout();
			this.scTop.Panel2.SuspendLayout();
			this.scTop.SuspendLayout();
			this.scMain.Panel1.SuspendLayout();
			this.scMain.Panel2.SuspendLayout();
			this.scMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// txtExpression
			// 
			this.txtExpression.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtExpression.Location = new System.Drawing.Point(0, 0);
			this.txtExpression.Multiline = true;
			this.txtExpression.Name = "txtExpression";
			this.txtExpression.ReadOnly = true;
			this.txtExpression.Size = new System.Drawing.Size(1004, 86);
			this.txtExpression.TabIndex = 0;
			// 
			// txtSql
			// 
			this.txtSql.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtSql.Location = new System.Drawing.Point(0, 0);
			this.txtSql.Multiline = true;
			this.txtSql.Name = "txtSql";
			this.txtSql.Size = new System.Drawing.Size(1004, 238);
			this.txtSql.TabIndex = 1;
			// 
			// btnQuery
			// 
			this.btnQuery.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnQuery.Location = new System.Drawing.Point(767, 3);
			this.btnQuery.Name = "btnQuery";
			this.btnQuery.Size = new System.Drawing.Size(114, 27);
			this.btnQuery.TabIndex = 3;
			this.btnQuery.Text = "&Execute";
			this.btnQuery.UseVisualStyleBackColor = true;
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			// 
			// chkUseOriginal
			// 
			this.chkUseOriginal.AutoSize = true;
			this.chkUseOriginal.Dock = System.Windows.Forms.DockStyle.Fill;
			this.chkUseOriginal.Location = new System.Drawing.Point(15, 3);
			this.chkUseOriginal.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
			this.chkUseOriginal.Name = "chkUseOriginal";
			this.chkUseOriginal.Size = new System.Drawing.Size(132, 27);
			this.chkUseOriginal.TabIndex = 4;
			this.chkUseOriginal.Text = "&Original query";
			this.chkUseOriginal.UseVisualStyleBackColor = true;
			this.chkUseOriginal.CheckedChanged += new System.EventHandler(this.chkIncludeParams_CheckedChanged);
			// 
			// radioQuery1
			// 
			this.radioQuery1.AutoSize = true;
			this.radioQuery1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.radioQuery1.Location = new System.Drawing.Point(165, 3);
			this.radioQuery1.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
			this.radioQuery1.Name = "radioQuery1";
			this.radioQuery1.Size = new System.Drawing.Size(102, 27);
			this.radioQuery1.TabIndex = 5;
			this.radioQuery1.TabStop = true;
			this.radioQuery1.Text = "Query &1";
			this.radioQuery1.UseVisualStyleBackColor = true;
			this.radioQuery1.Visible = false;
			this.radioQuery1.CheckedChanged += new System.EventHandler(this.radioQuery1_CheckedChanged);
			// 
			// radioQuery2
			// 
			this.radioQuery2.AutoSize = true;
			this.radioQuery2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.radioQuery2.Location = new System.Drawing.Point(285, 3);
			this.radioQuery2.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
			this.radioQuery2.Name = "radioQuery2";
			this.radioQuery2.Size = new System.Drawing.Size(102, 27);
			this.radioQuery2.TabIndex = 6;
			this.radioQuery2.TabStop = true;
			this.radioQuery2.Text = "Query &2";
			this.radioQuery2.UseVisualStyleBackColor = true;
			this.radioQuery2.Visible = false;
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 6;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.Controls.Add(this.btnClose, 5, 0);
			this.tlpMain.Controls.Add(this.chkUseOriginal, 0, 0);
			this.tlpMain.Controls.Add(this.radioQuery1, 1, 0);
			this.tlpMain.Controls.Add(this.btnQuery, 4, 0);
			this.tlpMain.Controls.Add(this.radioQuery2, 2, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tlpMain.Location = new System.Drawing.Point(0, 328);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(1004, 33);
			this.tlpMain.TabIndex = 7;
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnClose.Location = new System.Drawing.Point(887, 3);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(114, 27);
			this.btnClose.TabIndex = 7;
			this.btnClose.Text = "&Close";
			this.btnClose.UseVisualStyleBackColor = true;
			// 
			// scTop
			// 
			this.scTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scTop.Location = new System.Drawing.Point(0, 0);
			this.scTop.Name = "scTop";
			this.scTop.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// scTop.Panel1
			// 
			this.scTop.Panel1.Controls.Add(this.txtExpression);
			this.scTop.Panel1MinSize = 86;
			// 
			// scTop.Panel2
			// 
			this.scTop.Panel2.Controls.Add(this.txtSql);
			this.scTop.Size = new System.Drawing.Size(1004, 328);
			this.scTop.SplitterDistance = 86;
			this.scTop.TabIndex = 8;
			// 
			// scMain
			// 
			this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scMain.Location = new System.Drawing.Point(0, 0);
			this.scMain.Name = "scMain";
			this.scMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// scMain.Panel1
			// 
			this.scMain.Panel1.Controls.Add(this.scTop);
			this.scMain.Panel1.Controls.Add(this.tlpMain);
			// 
			// scMain.Panel2
			// 
			this.scMain.Panel2.Controls.Add(this.dataGridView1);
			this.scMain.Size = new System.Drawing.Size(1008, 730);
			this.scMain.SplitterDistance = 365;
			this.scMain.TabIndex = 9;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToOrderColumns = true;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(0, 0);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowTemplate.Height = 23;
			this.dataGridView1.Size = new System.Drawing.Size(1004, 357);
			this.dataGridView1.TabIndex = 1;
			// 
			// QueryVisualizerForm
			// 
			this.AcceptButton = this.btnQuery;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(1008, 730);
			this.Controls.Add(this.scMain);
			this.Font = new System.Drawing.Font("���� ����", 9F);
			this.Name = "QueryVisualizerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SQL Server Query Visualizer";
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.scTop.Panel1.ResumeLayout(false);
			this.scTop.Panel1.PerformLayout();
			this.scTop.Panel2.ResumeLayout(false);
			this.scTop.Panel2.PerformLayout();
			this.scTop.ResumeLayout(false);
			this.scMain.Panel1.ResumeLayout(false);
			this.scMain.Panel2.ResumeLayout(false);
			this.scMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtExpression;
        private System.Windows.Forms.TextBox txtSql;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.CheckBox chkUseOriginal;
        private System.Windows.Forms.RadioButton radioQuery1;
        private System.Windows.Forms.RadioButton radioQuery2;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.SplitContainer scTop;
		private System.Windows.Forms.SplitContainer scMain;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button btnClose;
    }
}