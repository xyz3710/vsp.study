﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LinqToObject01
{
	class Program
	{
		static void Main(string[] args)
		{
			string tempPath = Environment.GetEnvironmentVariable("Temp");
			DirectoryInfo dirInfo = new DirectoryInfo(tempPath);

			var query =
				from file in dirInfo.GetFiles("*.*", SearchOption.AllDirectories)
				where file.Length > 50
				orderby file.Length descending
				select file;

			foreach (FileInfo file in query)
				Console.WriteLine("{0} [{1:#,##0} bytes]", file.Name, file.Length);
		}
	}
}
