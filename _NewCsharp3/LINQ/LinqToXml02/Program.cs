﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Data.Linq;

namespace LinqToXml02
{
	class Program
	{
		static void Main(string[] args)
		{
			const string SAMPLE_XML = "customer.xml";
			GenerateCustomXml(SAMPLE_XML);
			GetDescendantCustomer(SAMPLE_XML);

			const string PURCHASE_XML = "purchase.xml";
            GeneratePurchaseXml(PURCHASE_XML);
			GetDescendantPurchase(PURCHASE_XML);
		}

		private static void GeneratePurchaseXml(string PURCHASE_XML)
		{
			XElement shippingAddress = GetAddressElement("Shipping", "홍길동", "서울", "111-222");
			XElement billingAddress = GetAddressElement("Billing", "홍길순", "부산", "222-333");
			XElement note = GetDeliveryNotes("부재시 경비실에 맡겨주세요");
			XElement items = GetItems(
							GetItem("872-AA", "청소기", 1, 10000),
							GetItem("926-AA", "모니터", 2, 40000)
						);

			XDocument purchase = AddPurchaseOrder(
                                     "11111", 
                                     DateTime.Now.ToShortDateString(), 
                                     shippingAddress, 
                                     billingAddress, 
                                     note, 
                                     items);

			purchase.Save(PURCHASE_XML);
		}

		private static XDocument AddPurchaseOrder(string orderNumber, string orderDate, params XElement[] contents)
		{
			return new XDocument(
					new XDeclaration("1.0", "UTF-8", "yes"),
					new XElement("PurchaseOrder",
						new XAttribute("orderNumber", orderNumber),
						new XAttribute("orderDate", orderDate),
						contents
				)
			);
		}

		private static XElement GetAddressElement(string type, string name, string city, string zipCode)
		{
			return new XElement("Address",
					new XAttribute("Type", type),
					new XElement("Name", name),
					new XElement("City", city),
					new XElement("Zip", zipCode)
				);
		}

		private static XElement GetDeliveryNotes(string notes)
		{
			return new XElement("DeliveryNotes", notes);
		}

		private static XElement GetItem(string partNumber, string productNumber, int quantity, int price)
		{
			return new XElement("Item",
					new XAttribute("PartNumber", partNumber),
					new XElement("ProductNumber", productNumber),
					new XElement("Quantity", quantity),
					new XElement("Price", price)
				);
		}

		private static XElement GetItems(params XElement[] item)
		{
			return new XElement("Imtes",
						item
				);
		}

		private static void GetDescendantPurchase(string PURCHASE_XML)
		{
			XElement purchaseOrder = XElement.Load(PURCHASE_XML);

			// Address type이 Billing인것만 조회
			IEnumerable<XElement> address = 
				from item in purchaseOrder.Descendants("Address")
				where item.Attribute("Type").Value == "Billing"
				select item;

			foreach (XElement item in address)
				Console.WriteLine(item);
		}

		private static void GenerateCustomXml(string fileName)
		{
			XDocument customer = 
				new XDocument(
					new XDeclaration("1.0", "UTF-8", "yes"),
					new XElement("customer",
						new XAttribute("id", "C01"),
						new XElement("firstName", "기원"),
						new XElement("lastName", "김"),
						new XElement("addresses",
							new XElement("address", new XAttribute("type", "email"), "xyz37@naver.com"),
							new XElement("address", new XAttribute("type", "url"), "http://blog.naver.com/xyz37"),
							new XElement("address", new XAttribute("type", "home"), "전주")
						)
					)
				);

			customer.Save(fileName);
		}

		private static void GetDescendantCustomer(string fileName)
		{
			XDocument customer = XDocument.Load(fileName);
			IEnumerable<XElement> addressesElements = customer.Descendants("addresses").Elements();

			foreach (XElement element in addressesElements)
				Console.WriteLine(element);

			Console.WriteLine();

			foreach (XElement element in addressesElements)
				Console.WriteLine(string.Format("type : {0}, value : {1}", element.Attribute("type").Value, element.Value));
		}
	}
}
