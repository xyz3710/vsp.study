USE [tempdb];
CREATE TABLE [dbo].[TestTable](
	[Col1] [nchar](10) NULL,
	[Col2] [nchar](10) NULL,
	[Col3] [nchar](10) NULL
) ON [PRIMARY]
GO

insert into [TestTable] values ('A1', 'B1', 'C1');
insert into [TestTable] values ('A2', 'B2', 'C2');
insert into [TestTable] values ('A3', 'B3', 'C3');
insert into [TestTable] values ('A4', 'B4', 'C4');
insert into [TestTable] values ('A5', 'B5', 'C5');
insert into [TestTable] values ('A6', 'B6', 'C6');
insert into [TestTable] values ('A7', 'B7', 'C7');
insert into [TestTable] values ('A8', 'B8', 'C8');
insert into [TestTable] values ('A9', 'B9', 'C9');
insert into [TestTable] values ('A10', 'B10', 'C10');
insert into [TestTable] values ('A11', 'B11', 'C11');
insert into [TestTable] values ('A12', 'B12', 'C12');
insert into [TestTable] values ('A13', 'B13', 'C13');
insert into [TestTable] values ('A14', 'B14', 'C14');
insert into [TestTable] values ('A15', 'B15', 'C15');
insert into [TestTable] values ('A16', 'B16', 'C16');
insert into [TestTable] values ('A17', 'B17', 'C17');
insert into [TestTable] values ('A18', 'B18', 'C18');
insert into [TestTable] values ('A19', 'B19', 'C19');
insert into [TestTable] values ('A20', 'B20', 'C20');