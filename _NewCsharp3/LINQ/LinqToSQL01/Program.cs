﻿/**********************************************************************************************************************/
/*	Domain		:	LinqToSQL01.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 5월 6일 목요일 오전 10:13
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://thankee.tistory.com/73 참고
 *					1. TestTable.sql를 해당 데이터 베이스에서 생성
 *					2. LINQ to SQL class 추가
 *					3. 해당 테이블 생성(테이블.dbml)
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Data.Linq;

namespace LinqToSQL01
{
	class Program
	{
		static void Main(string[] args)
		{
			// Connect sql
			string connectionString = String.Format(
                                          "Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}", 
                                          @"ISETDA-KUNSAN\SQLEXPRESS,1433",
										  "tempdb", 
                                          "tester",
										  "1234");
			DataContext db = new DataContext(connectionString);
			Table<TestTable> testTable = db.GetTable<TestTable>();

			var query =
				from c in testTable
				where c.Col1.StartsWith("A1") == true
				select string.Format("{0},\t{1},\t{2}", c.Col1.Trim(), c.Col2.Trim(), c.Col3.Trim())
				;

			foreach (var row in query)
				Console.WriteLine(row);
		}
	}
}
