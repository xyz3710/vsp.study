﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     이 코드는 도구를 사용하여 생성되었습니다.
//     런타임 버전:4.0.30319.225
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LinqToSQL01
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="tempdb")]
	public partial class TestTableDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region 확장성 메서드 정의
    partial void OnCreated();
    #endregion
		
		public TestTableDataContext() : 
				base(global::LinqToSQL01.Properties.Settings.Default.tempdbConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public TestTableDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TestTableDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TestTableDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TestTableDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<TestTable> TestTable
		{
			get
			{
				return this.GetTable<TestTable>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TestTable")]
	public partial class TestTable
	{
		
		private string _Col1;
		
		private string _Col2;
		
		private string _Col3;
		
		public TestTable()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Col1", DbType="NChar(10)")]
		public string Col1
		{
			get
			{
				return this._Col1;
			}
			set
			{
				if ((this._Col1 != value))
				{
					this._Col1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Col2", DbType="NChar(10)")]
		public string Col2
		{
			get
			{
				return this._Col2;
			}
			set
			{
				if ((this._Col2 != value))
				{
					this._Col2 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Col3", DbType="NChar(10)")]
		public string Col3
		{
			get
			{
				return this._Col3;
			}
			set
			{
				if ((this._Col3 != value))
				{
					this._Col3 = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
