﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;

namespace LinqToDataSet01
{
	class Program
	{
		#region Constants
		private const string TABLE_NAME = "Table1";
		#endregion

		static void Main(string[] args)
		{
			DataSet dataSet = new DataSet();

			dataSet.Locale = CultureInfo.InvariantCulture;

			try
			{
				string connectionString = String.Format(
							  @"Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2}",
							  @"ISETDA-KUNSAN\SQLEXPRESS",
							  "TestDB",
							  @"anonymous");
				SqlDataAdapter da = new SqlDataAdapter("select * from table1", connectionString);

				da.TableMappings.Add("Table", TABLE_NAME);
				da.Fill(dataSet);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Sql Exception occured: ", ex.Message);
			}

			DataTable testTable = dataSet.Tables[TABLE_NAME];

			if (testTable == null)
			{
				Console.WriteLine("Unable get table info");

				return;
			}

			IEnumerable<DataRow> query = 
				from c in testTable.AsEnumerable()
				where Convert.ToString(c["Col1"]).StartsWith("Test1") == true
				select c;

			foreach (DataRow dataRow in query)
				Console.WriteLine(dataRow.Field<string>("Col1"));
		}
	}
}
