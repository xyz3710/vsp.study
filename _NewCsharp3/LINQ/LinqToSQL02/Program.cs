﻿/**********************************************************************************************************************/
/*	Domain		:	LinqToSQL02.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 5월 10일 월요일 오전 11:26
/*	Purpose		:	LINQ to SQL 테스트(http://thankee.tistory.com/73 참고)
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;

namespace LinqToSQL02
{
	class Program
	{
		private static NorthWindDataContext _db;

		static void Main(string[] args)
		{
			_db = new NorthWindDataContext();

			// DeletePost();
			#region SELECT
			// Select
			var chocoladeProducts = from p in _db.Products
									where p.ProductID == 48
									select p;
			#endregion

			#region UPDATE
			// Update
			foreach (Products chocoladeProduct in chocoladeProducts)
			{
				chocoladeProduct.ProductName = "초콜렛1";
				chocoladeProduct.Discontinued = true;
			}
			#endregion

			#region DELETE
			//_db.Products.DeleteAllOnSubmit(chocoladeProduct);
			#endregion

			#region INSERT
			/*
			// Insert
			Products newProducts = new Products()
			{
				ProductID = 78,
				CategoryID = 3,
				Discontinued = false,
				ProductName = "신제품",
				QuantityPerUnit = "Kg",
				ReorderLevel = 30,
				SupplierID = 29,
				UnitPrice = 99.99m,
				UnitsInStock = 111,
				UnitsOnOrder = 0
			};

			_db.Products.InsertOnSubmit(newProducts);
			*/
			#endregion

			#region New Primary Key/Foreign Key
			/*
			// New Primary Key/Foreign Key
			var product = from p in _db.Products
						  where p.Categories.CategoryName == "Beverages"
						  select p;

			// 외래키에 기존의 Category 할당
			foreach (Products item in product)
				item.Categories = _db.Categories.Single(c => c.CategoryName == "Seafood");

			// 외래키에 새로운 Category 할당
			Categories softwareCategory = new Categories()
			{
				CategoryName = "소프트웨어",
			};

			foreach (Products item in product)
				item.Categories = softwareCategory;
			*/
			#endregion

			// NorthWind.cs의 Products partial 클레스에서 유효성 검사를 한다.
			_db.SubmitChanges();
			
			ShowProduct();
		}

		private static void ShowProduct()
		{
			var products = _db.Products;

			foreach (Products pro in products)
				Console.WriteLine("{0:000}, {1}, {2}", pro.ProductID, pro.ProductName, pro.Suppliers.CompanyName);

			Console.WriteLine("All {0:#,##0} ea products", products.Count());
			Console.WriteLine("=".PadLeft(60, '='));
		}

		private static void DeletePost(string postId)
		{
			ShowPost();
			var lastPost = from p in _db.Posts
						   where p.Id.ToString() == postId
						   select p;

			_db.Posts.DeleteAllOnSubmit(lastPost);
			ShowPost();
		}

		private static void ShowPost()
		{
			var Posts = _db.Posts;

			foreach (Posts pro in Posts)
				Console.WriteLine("{0}, {1}, {2}", pro.Id, pro.Title, pro.Body);

			Console.WriteLine("All {0:#,##0} ea Posts", Posts.Count());
			Console.WriteLine("=".PadLeft(60, '='));
		}
	}
}
