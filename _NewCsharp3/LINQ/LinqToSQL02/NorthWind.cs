using System.Diagnostics;
using System;
namespace LinqToSQL02
{
	partial class Categories
	{
	}
	/// <summary>
	/// 제품 정보
	/// </summary>
	[DebuggerDisplay("ProductID:{ProductID}, ProductName:{ProductName}, SupplierID:{SupplierID}, CategoryID:{CategoryID}, QuantityPerUnit:{QuantityPerUnit}, UnitPrice:{UnitPrice}, UnitsInStock:{UnitsInStock}, UnitsOnOrder:{UnitsOnOrder}, ReorderLevel:{ReorderLevel}, Discontinued:{Discontinued}, Order_Details:{Order_Details}, Categories:{Categories}, Suppliers:{Suppliers}, ProductID:{ProductID}, ProductName:{ProductName}, SupplierID:{SupplierID}, CategoryID:{CategoryID}, QuantityPerUnit:{QuantityPerUnit}, UnitPrice:{UnitPrice}, UnitsInStock:{UnitsInStock}, UnitsOnOrder:{UnitsOnOrder}, ReorderLevel:{ReorderLevel}, Discontinued:{Discontinued}, Order_Details:{Order_Details}, Categories:{Categories}, Suppliers:{Suppliers}", Name="Products")]
	public partial class Products
	{
		/// <summary>
		/// 특정 Property 유효성 검사
		/// </summary>
		/// <param name="value"></param>
		partial void OnDiscontinuedChanging(bool value)
		{
			Console.WriteLine("Discontinued 속성에서 검사했습니다. : {0} 입니다.", Discontinued);
		}

		partial void OnValidate(System.Data.Linq.ChangeAction action)
		{
			switch (action)
			{
				case System.Data.Linq.ChangeAction.Delete:
					

					break;
				case System.Data.Linq.ChangeAction.Insert:
					

					break;
				case System.Data.Linq.ChangeAction.Update:
					Console.WriteLine("전체 속성에서 검사했습니다.(Discontinued) : {0} 입니다.", Discontinued);	

					break;
			}
		}
	}
		
	partial class NorthWindDataContext
	{
		partial void UpdateProducts(Products instance)
		{
			Console.WriteLine("products 가 수정 되었습니다.\r\n{0}", instance.ToString());
		}
	}
}
