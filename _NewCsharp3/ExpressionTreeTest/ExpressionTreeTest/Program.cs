﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTreeTest
{
	class Program
	{
		static void Main(string[] args)
		{
			ExpressionTreeWithLambda();
			ExpressionExecute();
			ExpressionModify();
		}

		private static void ExpressionTreeWithLambda()
		{
			Console.WriteLine("\n\tLambda/API를 통한 식 트리 만들기");
			Expression<Func<int, bool>> lambda1 = num => num < 5;

			Console.WriteLine(lambda1);


			ParameterExpression numParam = Expression.Parameter(typeof(int), "num");
			ConstantExpression five = Expression.Constant(5, typeof(int));
			BinaryExpression numLessThenFive = Expression.LessThan(numParam, five);
			Expression<Func<int, bool>> lambda2 = Expression.Lambda<Func<int, bool>>(numLessThenFive, numParam);

			Console.WriteLine(lambda2);

			ParameterExpression value = Expression.Parameter(typeof(int), "value");
			ParameterExpression result = Expression.Parameter(typeof(int), "result");
			LabelTarget label = Expression.Label(typeof(int));
			BlockExpression block = Expression.Block(
				new[] { result },
				Expression.Assign(result, Expression.Constant(1)),
					Expression.Loop(
						Expression.IfThenElse(
				// Condition: value > 1
							Expression.GreaterThan(value, Expression.Constant(1)),
				// If true: result *= value --
							Expression.MultiplyAssign(result,
								Expression.PostDecrementAssign(value)),
				// If false, exist the loop and go to the label.
							Expression.Break(label, result)
						),
				// Label to jump to.
					label
				)
			);

			Console.WriteLine(block);

			// Compile and execute and expression tree.
			Expression<Func<int, int>> lambda = Expression.Lambda<Func<int, int>>(block, value);

			Console.WriteLine(lambda);
			int factorial = lambda.Compile()(5);

			Console.WriteLine(factorial);
		}

		private static void ExpressionModify()
		{
			Console.WriteLine("\n\t표현식 수정하기 && => ||");
			Expression<Func<string, bool>> expr = name => name.Length > 10 && name.StartsWith("G");
			Console.WriteLine(expr);

			AndAlsoModifier treeModifier = new AndAlsoModifier();
			Expression modifiedExpr = treeModifier.Modify(expr);

			Console.WriteLine(modifiedExpr);
		}

		private static void ExpressionExecute()
		{
			Console.WriteLine("\n\t표현식 실행하기");
			BinaryExpression be = Expression.Power(Expression.Constant(2d), Expression.Constant(3d));
			Expression<Func<double>> le = Expression.Lambda<Func<double>>(be);
			Func<double> compiledExpression = le.Compile();
			double result = compiledExpression();

			Console.WriteLine("{0} = {1}", be, result);
		}

	}

	public class AndAlsoModifier : ExpressionVisitor
	{
		public Expression Modify(Expression expression)
		{
			return Visit(expression);
		}

		protected override Expression VisitBinary(BinaryExpression node)
		{
			if (node.NodeType == ExpressionType.AndAlso)
			{
				Expression left = Visit(node.Left);
				Expression right = Visit(node.Right);

				return Expression.MakeBinary(ExpressionType.OrElse, left, right, node.IsLiftedToNull, node.Method);
			}

			return base.VisitBinary(node);
		}
	}
}
