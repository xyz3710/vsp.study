﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtensionMethods
{
	public static class E
	{
		/// <summary>
		/// Int type in F method, E class
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="i"></param>
		public static void F(this object obj, int i)
		{
			Console.WriteLine("Int type in F method, E class");
		}

		/// <summary>
		/// String type in F method, E class
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="s"></param>
		public static void F(this object obj, string s)
		{
			Console.WriteLine("String type in F method, E class");
		}
	}

	public class A
	{
		/// <summary>
		/// A class constructor
		/// </summary>
		public A()
		{
			Console.WriteLine("A class constructor.");
		}
	}

	public class B
	{
		/// <summary>
		/// Int type in F method, B class
		/// </summary>
		/// <param name="i"></param>
		public void F(int i)
		{
			Console.WriteLine("Int type in F method, B class");
		}
	}

	public class C
	{
		/// <summary>
		/// object type F method, C class
		/// </summary>
		/// <param name="obj"></param>
		public void F(object obj)
		{
			Console.WriteLine("object type F method, C class");
		}
	}		
}
