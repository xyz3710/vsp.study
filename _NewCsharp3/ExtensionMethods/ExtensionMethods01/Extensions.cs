﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtensionMethods
{
	static class Extensions
	{
		public static int ToInt32(this string stringItem)
		{
			int result = 0;

			int.TryParse(stringItem, out result);

			return result;
		}

		public static T[] Slice<T>(this T[] source, int index, int count)
		{
			if (index <0 || count < 0 || source.Length - index < count)
            throw new ArgumentException();

			T[] result = new T[count];
			Array.Copy(source, index, result, 0, count);

			return result;
		}
	}
}
