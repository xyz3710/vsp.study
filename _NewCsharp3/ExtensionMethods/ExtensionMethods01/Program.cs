﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtensionMethods
{
	class Program
	{
		static void Main(string[] args)
		{
			string item = "1234";

			int i = item.ToInt32();
			int j = Extensions.ToInt32(item);

			Console.WriteLine("i:{0}, j:{1}", i, j);

			int[] digits = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			int[] digitsSliced = digits.Slice(4, 3);

			foreach (int intItem in digitsSliced)
				Console.WriteLine(intItem);
		}

		private static void Test(A a, B b, C c)
		{
			a.F(1);
			a.F("hello");
			b.F(2);
			b.F("hello");
			c.F(1);
			c.F("hello");
		}
	}
}
