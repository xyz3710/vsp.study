// Assembly StyleCop.CSharp.Rules, Version 4.7.1000.0

[assembly: System.Reflection.AssemblyVersion("4.7.1000.0")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyTitle("StyleCop C# Analyzers")]
[assembly: System.Reflection.AssemblyDescription("Source code analyzers for use with StyleCop")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("")]
[assembly: System.Reflection.AssemblyProduct("StyleCop")]
[assembly: System.Reflection.AssemblyCopyright("MS-PL")]
[assembly: System.Runtime.InteropServices.Guid("B2C1C895-B8DD-4A2B-8671-77F9E2E8C511")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: System.CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Resources.NeutralResourcesLanguage("en-US")]
[assembly: System.Reflection.AssemblyFileVersion("4.7.48.0")]

