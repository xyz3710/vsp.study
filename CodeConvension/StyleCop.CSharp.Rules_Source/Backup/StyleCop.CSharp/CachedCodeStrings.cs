namespace StyleCop.CSharp
{
    using System;
    using System.Globalization;

    public static class CachedCodeStrings
    {
        private static string classText;
        private static CultureInfo culture;
        private static string exampleHeaderSummaryForDestructor;
        private static string exampleHeaderSummaryForInstanceConstructor;
        private static string exampleHeaderSummaryForPrivateInstanceConstructor;
        private static string exampleHeaderSummaryForStaticConstructor;
        private static string headerSummaryForBooleanGetAccessor;
        private static string headerSummaryForBooleanGetAndSetAccessor;
        private static string headerSummaryForBooleanSetAccessor;
        private static string headerSummaryForDestructor;
        private static string headerSummaryForGetAccessor;
        private static string headerSummaryForGetAndSetAccessor;
        private static string headerSummaryForInstanceConstructor;
        private static string headerSummaryForPrivateInstanceConstructor;
        private static string headerSummaryForSetAccessor;
        private static string headerSummaryForStaticConstructor;
        private static string parameterNotUsed;
        private static string structText;

        private static void ClearCachedStrings()
        {
            headerSummaryForBooleanGetAccessor = null;
            headerSummaryForBooleanGetAndSetAccessor = null;
            headerSummaryForBooleanSetAccessor = null;
            headerSummaryForGetAccessor = null;
            headerSummaryForGetAndSetAccessor = null;
            headerSummaryForSetAccessor = null;
            headerSummaryForStaticConstructor = null;
            exampleHeaderSummaryForStaticConstructor = null;
            headerSummaryForPrivateInstanceConstructor = null;
            exampleHeaderSummaryForPrivateInstanceConstructor = null;
            headerSummaryForInstanceConstructor = null;
            exampleHeaderSummaryForInstanceConstructor = null;
            headerSummaryForDestructor = null;
            exampleHeaderSummaryForDestructor = null;
            parameterNotUsed = null;
            classText = null;
            structText = null;
        }

        public static string ClassText
        {
            get
            {
                if (classText == null)
                {
                    classText = CodeStrings.Class;
                }
                return classText;
            }
        }

        public static CultureInfo Culture
        {
            get
            {
                return culture;
            }
            set
            {
                if (value == null)
                {
                    value = new CultureInfo("en-US");
                }
                if ((culture == null) || (culture.EnglishName != value.EnglishName))
                {
                    ClearCachedStrings();
                    culture = value;
                    CodeStrings.Culture = value;
                }
            }
        }

        public static string ExampleHeaderSummaryForDestructor
        {
            get
            {
                if (exampleHeaderSummaryForDestructor == null)
                {
                    exampleHeaderSummaryForDestructor = CodeStrings.ExampleHeaderSummaryForDestructor;
                }
                return exampleHeaderSummaryForDestructor;
            }
        }

        public static string ExampleHeaderSummaryForInstanceConstructor
        {
            get
            {
                if (exampleHeaderSummaryForInstanceConstructor == null)
                {
                    exampleHeaderSummaryForInstanceConstructor = CodeStrings.ExampleHeaderSummaryForInstanceConstructor;
                }
                return exampleHeaderSummaryForInstanceConstructor;
            }
        }

        public static string ExampleHeaderSummaryForPrivateInstanceConstructor
        {
            get
            {
                if (exampleHeaderSummaryForPrivateInstanceConstructor == null)
                {
                    exampleHeaderSummaryForPrivateInstanceConstructor = CodeStrings.ExampleHeaderSummaryForPrivateInstanceConstructor;
                }
                return exampleHeaderSummaryForPrivateInstanceConstructor;
            }
        }

        public static string ExampleHeaderSummaryForStaticConstructor
        {
            get
            {
                if (exampleHeaderSummaryForStaticConstructor == null)
                {
                    exampleHeaderSummaryForStaticConstructor = CodeStrings.ExampleHeaderSummaryForStaticConstructor;
                }
                return exampleHeaderSummaryForStaticConstructor;
            }
        }

        public static string HeaderSummaryForBooleanGetAccessor
        {
            get
            {
                if (headerSummaryForBooleanGetAccessor == null)
                {
                    headerSummaryForBooleanGetAccessor = CodeStrings.HeaderSummaryForBooleanGetAccessor;
                }
                return headerSummaryForBooleanGetAccessor;
            }
        }

        public static string HeaderSummaryForBooleanGetAndSetAccessor
        {
            get
            {
                if (headerSummaryForBooleanGetAndSetAccessor == null)
                {
                    headerSummaryForBooleanGetAndSetAccessor = CodeStrings.HeaderSummaryForBooleanGetAndSetAccessor;
                }
                return headerSummaryForBooleanGetAndSetAccessor;
            }
        }

        public static string HeaderSummaryForBooleanSetAccessor
        {
            get
            {
                if (headerSummaryForBooleanSetAccessor == null)
                {
                    headerSummaryForBooleanSetAccessor = CodeStrings.HeaderSummaryForBooleanSetAccessor;
                }
                return headerSummaryForBooleanSetAccessor;
            }
        }

        public static string HeaderSummaryForDestructor
        {
            get
            {
                if (headerSummaryForDestructor == null)
                {
                    headerSummaryForDestructor = CodeStrings.HeaderSummaryForDestructor;
                }
                return headerSummaryForDestructor;
            }
        }

        public static string HeaderSummaryForGetAccessor
        {
            get
            {
                if (headerSummaryForGetAccessor == null)
                {
                    headerSummaryForGetAccessor = CodeStrings.HeaderSummaryForGetAccessor;
                }
                return headerSummaryForGetAccessor;
            }
        }

        public static string HeaderSummaryForGetAndSetAccessor
        {
            get
            {
                if (headerSummaryForGetAndSetAccessor == null)
                {
                    headerSummaryForGetAndSetAccessor = CodeStrings.HeaderSummaryForGetAndSetAccessor;
                }
                return headerSummaryForGetAndSetAccessor;
            }
        }

        public static string HeaderSummaryForInstanceConstructor
        {
            get
            {
                if (headerSummaryForInstanceConstructor == null)
                {
                    headerSummaryForInstanceConstructor = CodeStrings.HeaderSummaryForInstanceConstructor;
                }
                return headerSummaryForInstanceConstructor;
            }
        }

        public static string HeaderSummaryForPrivateInstanceConstructor
        {
            get
            {
                if (headerSummaryForPrivateInstanceConstructor == null)
                {
                    headerSummaryForPrivateInstanceConstructor = CodeStrings.HeaderSummaryForPrivateInstanceConstructor;
                }
                return headerSummaryForPrivateInstanceConstructor;
            }
        }

        public static string HeaderSummaryForSetAccessor
        {
            get
            {
                if (headerSummaryForSetAccessor == null)
                {
                    headerSummaryForSetAccessor = CodeStrings.HeaderSummaryForSetAccessor;
                }
                return headerSummaryForSetAccessor;
            }
        }

        public static string HeaderSummaryForStaticConstructor
        {
            get
            {
                if (headerSummaryForStaticConstructor == null)
                {
                    headerSummaryForStaticConstructor = CodeStrings.HeaderSummaryForStaticConstructor;
                }
                return headerSummaryForStaticConstructor;
            }
        }

        public static string ParameterNotUsed
        {
            get
            {
                if (parameterNotUsed == null)
                {
                    parameterNotUsed = CodeStrings.ParameterNotUsed;
                }
                return parameterNotUsed;
            }
        }

        public static string StructText
        {
            get
            {
                if (structText == null)
                {
                    structText = CodeStrings.Struct;
                }
                return structText;
            }
        }
    }
}

