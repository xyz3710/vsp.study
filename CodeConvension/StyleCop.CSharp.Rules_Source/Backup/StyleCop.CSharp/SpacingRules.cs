namespace StyleCop.CSharp
{
    using StyleCop;
    using System;

    [SourceAnalyzer(typeof(CsParser))]
    public class SpacingRules : SourceAnalyzer
    {
        public override void AnalyzeDocument(CodeDocument document)
        {
            Param.RequireNotNull(document, "document");
            CsDocument document2 = (CsDocument) document;
            if ((document2.RootElement != null) && !document2.RootElement.Generated)
            {
                this.CheckSpacing(document2.Tokens, false, null);
            }
        }

        private void CheckAttributeTokenCloseBracket(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if (((previous != null) && (previous.Value.CsTokenType == CsTokenType.WhiteSpace)) && !this.IsTokenFirstNonWhitespaceTokenOnLine(tokens, tokenNode))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingAttributeBracketsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckAttributeTokenOpenBracket(Node<CsToken> tokenNode)
        {
            Node<CsToken> next = tokenNode.Next;
            if ((next != null) && (next.Value.CsTokenType == CsTokenType.WhiteSpace))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningAttributeBracketsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckCloseCurlyBracket(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if (((previous != null) && (previous.Value.CsTokenType != CsTokenType.WhiteSpace)) && (previous.Value.CsTokenType != CsTokenType.EndOfLine))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingCurlyBracketsMustBeSpacedCorrectly, new object[0]);
            }
            Node<CsToken> next = tokenNode.Next;
            if (next != null)
            {
                CsTokenType csTokenType = next.Value.CsTokenType;
                if ((((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && ((csTokenType != CsTokenType.CloseParenthesis) && !IsTokenADot(next.Value))) && (((csTokenType != CsTokenType.Semicolon) && (csTokenType != CsTokenType.OpenSquareBracket)) && (csTokenType != CsTokenType.Comma)))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingCurlyBracketsMustBeSpacedCorrectly, new object[0]);
                }
                if (csTokenType == CsTokenType.WhiteSpace)
                {
                    foreach (CsToken token in tokens.ForwardIterator(tokenNode.Next.Next))
                    {
                        CsTokenType type2 = token.CsTokenType;
                        if (((type2 == CsTokenType.CloseParenthesis) || (type2 == CsTokenType.Semicolon)) || ((type2 == CsTokenType.Comma) || IsTokenADot(token)))
                        {
                            base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingCurlyBracketsMustBeSpacedCorrectly, new object[0]);
                            continue;
                        }
                        if (type2 != CsTokenType.WhiteSpace)
                        {
                            return;
                        }
                    }
                }
            }
        }

        private void CheckCloseParen(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if ((previous != null) && ((previous.Value.CsTokenType == CsTokenType.WhiteSpace) || ((previous.Value.CsTokenType == CsTokenType.EndOfLine) && (previous.Previous.Value.CsTokenType != CsTokenType.SingleLineComment))))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), previous.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingParenthesisMustBeSpacedCorrectly, new object[0]);
            }
            Node<CsToken> next = tokenNode.Next;
            if (next == null)
            {
                return;
            }
            CsTokenType csTokenType = next.Value.CsTokenType;
            CsTokenType type2 = (next.Next == null) ? CsTokenType.Other : next.Next.Value.CsTokenType;
            if (tokenNode.Value.Parent is CastExpression)
            {
                if (csTokenType == CsTokenType.WhiteSpace)
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), next.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingParenthesisMustBeSpacedCorrectly, new object[0]);
                    return;
                }
                return;
            }
            if ((csTokenType != CsTokenType.LabelColon) && (type2 != CsTokenType.LabelColon))
            {
                if (csTokenType == CsTokenType.WhiteSpace)
                {
                    foreach (CsToken token2 in tokens.ForwardIterator(tokenNode.Next.Next))
                    {
                        if (IsAllowedAfterClosingParenthesis(token2))
                        {
                            base.AddViolation(tokenNode.Value.FindParentElement(), next.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingParenthesisMustBeSpacedCorrectly, new object[0]);
                        }
                        else if (token2.CsTokenType != CsTokenType.WhiteSpace)
                        {
                            break;
                        }
                    }
                }
                else if ((next.Value.CsTokenType != CsTokenType.EndOfLine) && !IsAllowedAfterClosingParenthesis(next.Value))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), next.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingParenthesisMustBeSpacedCorrectly, new object[0]);
                }
                return;
            }
            bool flag = false;
            foreach (CsToken token in tokens.ReverseIterator(tokenNode.Previous))
            {
                switch (token.CsTokenType)
                {
                    case CsTokenType.EndOfLine:
                        goto Label_013E;

                    case CsTokenType.Case:
                        flag = true;
                        goto Label_013E;
                }
            }
        Label_013E:
            if ((flag && (csTokenType == CsTokenType.WhiteSpace)) || (!flag && (csTokenType != CsTokenType.WhiteSpace)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), next.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingParenthesisMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckCloseSquareBracket(MasterList<CsToken> tokens, Node<CsToken> tokenNode, Node<CsToken> parentTokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if ((previous != null) && ((previous.Value.CsTokenType == CsTokenType.WhiteSpace) || (previous.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingSquareBracketsMustBeSpacedCorrectly, new object[0]);
            }
            Node<CsToken> node2 = tokenNode.Next ?? parentTokenNode.Next;
            if (node2 != null)
            {
                CsTokenType csTokenType = node2.Value.CsTokenType;
                if (((((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && ((csTokenType != CsTokenType.CloseParenthesis) && (csTokenType != CsTokenType.OpenParenthesis))) && (((csTokenType != CsTokenType.CloseSquareBracket) && (csTokenType != CsTokenType.OpenSquareBracket)) && ((csTokenType != CsTokenType.Semicolon) && (csTokenType != CsTokenType.Comma)))) && (((csTokenType != CsTokenType.CloseGenericBracket) && (node2.Value.Text != "++")) && ((node2.Value.Text != "--") && !node2.Value.Text.StartsWith(".", StringComparison.Ordinal))))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingSquareBracketsMustBeSpacedCorrectly, new object[0]);
                }
                if (csTokenType == CsTokenType.WhiteSpace)
                {
                    foreach (CsToken token in tokens.ForwardIterator(node2.Next))
                    {
                        CsTokenType type2 = token.CsTokenType;
                        switch (type2)
                        {
                            case CsTokenType.CloseParenthesis:
                            case CsTokenType.OpenParenthesis:
                            case CsTokenType.CloseSquareBracket:
                            case CsTokenType.OpenSquareBracket:
                            case CsTokenType.Semicolon:
                            case CsTokenType.Comma:
                            {
                                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingSquareBracketsMustBeSpacedCorrectly, new object[0]);
                                continue;
                            }
                        }
                        if (type2 != CsTokenType.WhiteSpace)
                        {
                            return;
                        }
                    }
                }
            }
        }

        private void CheckGenericSpacing(MasterList<CsToken> tokens, Node<CsToken> genericTokenNode)
        {
            GenericType type = genericTokenNode.Value as GenericType;
            if (type.ChildTokens.Count > 0)
            {
                for (Node<CsToken> node = type.ChildTokens.First; node != null; node = node.Next)
                {
                    OperatorSymbol symbol;
                    if (base.Cancel)
                    {
                        return;
                    }
                    if (node.Value.CsTokenClass == CsTokenClass.GenericType)
                    {
                        this.CheckGenericSpacing(tokens, node);
                    }
                    if (!node.Value.Generated)
                    {
                        switch (node.Value.CsTokenType)
                        {
                            case CsTokenType.Other:
                            case CsTokenType.EndOfLine:
                            case CsTokenType.Out:
                            case CsTokenType.OpenCurlyBracket:
                            case CsTokenType.CloseCurlyBracket:
                            case CsTokenType.BaseColon:
                            case CsTokenType.WhereColon:
                            case CsTokenType.AttributeColon:
                            case CsTokenType.LabelColon:
                            case CsTokenType.In:
                                goto Label_017B;

                            case CsTokenType.WhiteSpace:
                                this.CheckWhitespace(node);
                                goto Label_017B;

                            case CsTokenType.PreprocessorDirective:
                                this.CheckPreprocessorSpacing(node.Value);
                                goto Label_017B;

                            case CsTokenType.OpenParenthesis:
                                this.CheckOpenParen(type.ChildTokens, node);
                                goto Label_017B;

                            case CsTokenType.CloseParenthesis:
                                this.CheckCloseParen(type.ChildTokens, node);
                                goto Label_017B;

                            case CsTokenType.OpenSquareBracket:
                                this.CheckOpenSquareBracket(node);
                                goto Label_017B;

                            case CsTokenType.CloseSquareBracket:
                                this.CheckCloseSquareBracket(type.ChildTokens, node, genericTokenNode);
                                goto Label_017B;

                            case CsTokenType.OpenGenericBracket:
                                this.CheckGenericTokenOpenBracket(node);
                                goto Label_017B;

                            case CsTokenType.CloseGenericBracket:
                                this.CheckGenericTokenCloseBracket(node, genericTokenNode);
                                goto Label_017B;

                            case CsTokenType.OperatorSymbol:
                                goto Label_014E;

                            case CsTokenType.Comma:
                                this.CheckSemicolonAndComma(tokens, node);
                                goto Label_017B;
                        }
                    }
                    goto Label_017B;
                Label_014E:
                    symbol = node.Value as OperatorSymbol;
                    if ((symbol.SymbolType == OperatorType.MemberAccess) || (symbol.SymbolType == OperatorType.QualifiedAlias))
                    {
                        this.CheckMemberAccessSymbol(type.ChildTokens, node);
                    }
                Label_017B:;
                }
            }
        }

        private void CheckGenericTokenCloseBracket(Node<CsToken> closeBracketTokenNode, Node<CsToken> genericTokenNode)
        {
            Node<CsToken> previous = closeBracketTokenNode.Previous;
            if ((previous != null) && ((previous.Value.CsTokenType == CsTokenType.WhiteSpace) || (previous.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(closeBracketTokenNode.Value.FindParentElement(), closeBracketTokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingGenericBracketsMustBeSpacedCorrectly, new object[0]);
            }
            bool flag = false;
            Node<CsToken> next = genericTokenNode.Next;
            if (next == null)
            {
                flag = true;
            }
            else
            {
                CsTokenType csTokenType = next.Value.CsTokenType;
                if (((((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && ((csTokenType != CsTokenType.OpenParenthesis) && (csTokenType != CsTokenType.CloseParenthesis))) && (((csTokenType != CsTokenType.CloseGenericBracket) && (csTokenType != CsTokenType.NullableTypeSymbol)) && ((csTokenType != CsTokenType.OperatorSymbol) && (csTokenType != CsTokenType.OpenSquareBracket)))) && ((csTokenType != CsTokenType.Comma) && (csTokenType != CsTokenType.Semicolon)))
                {
                    flag = true;
                }
                if (csTokenType == CsTokenType.WhiteSpace)
                {
                    Node<CsToken> node3 = next.Next;
                    if ((node3 == null) || (node3.Value.CsTokenType == CsTokenType.OpenParenthesis))
                    {
                        flag = true;
                    }
                }
            }
            if (flag)
            {
                base.AddViolation(closeBracketTokenNode.Value.FindParentElement(), closeBracketTokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ClosingGenericBracketsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckGenericTokenOpenBracket(Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if ((previous != null) && ((previous.Value.CsTokenType == CsTokenType.WhiteSpace) || (previous.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningGenericBracketsMustBeSpacedCorrectly, new object[0]);
            }
            Node<CsToken> next = tokenNode.Next;
            if ((next != null) && ((next.Value.CsTokenType == CsTokenType.WhiteSpace) || (next.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningGenericBracketsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckIncrementDecrement(Node<CsToken> tokenNode)
        {
            bool flag = false;
            bool flag2 = false;
            Node<CsToken> previous = tokenNode.Previous;
            if (previous == null)
            {
                flag = true;
            }
            else
            {
                switch (previous.Value.CsTokenType)
                {
                    case CsTokenType.WhiteSpace:
                    case CsTokenType.EndOfLine:
                    case CsTokenType.SingleLineComment:
                    case CsTokenType.MultiLineComment:
                        flag = true;
                        break;
                }
            }
            Node<CsToken> next = tokenNode.Next;
            if (next == null)
            {
                flag2 = true;
            }
            else
            {
                switch (next.Value.CsTokenType)
                {
                    case CsTokenType.WhiteSpace:
                    case CsTokenType.EndOfLine:
                    case CsTokenType.SingleLineComment:
                    case CsTokenType.MultiLineComment:
                        flag2 = true;
                        break;
                }
            }
            bool flag3 = false;
            if (!flag && !flag2)
            {
                if ((previous.Value.CsTokenType == CsTokenType.OpenSquareBracket) || (previous.Value.CsTokenType == CsTokenType.OpenParenthesis))
                {
                    return;
                }
                switch (next.Value.CsTokenType)
                {
                    case CsTokenType.CloseSquareBracket:
                    case CsTokenType.CloseParenthesis:
                    case CsTokenType.Comma:
                    case CsTokenType.Semicolon:
                        return;
                }
                flag3 = true;
            }
            else if (flag && flag2)
            {
                flag3 = true;
            }
            else if (flag)
            {
                switch (next.Value.CsTokenType)
                {
                    case CsTokenType.CloseSquareBracket:
                    case CsTokenType.CloseParenthesis:
                    case CsTokenType.Comma:
                    case CsTokenType.Semicolon:
                        flag3 = true;
                        break;
                }
            }
            else if ((previous.Value.CsTokenType == CsTokenType.OpenSquareBracket) || (previous.Value.CsTokenType == CsTokenType.OpenParenthesis))
            {
                flag3 = true;
            }
            if (flag3)
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.IncrementDecrementSymbolsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckKeywordWithoutSpace(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> next = tokenNode.Next;
            if ((next != null) && ((next.Value.CsTokenType == CsTokenType.WhiteSpace) || (next.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                foreach (CsToken token in tokens.ForwardIterator(next.Next))
                {
                    if (token.CsTokenType == CsTokenType.OpenParenthesis)
                    {
                        base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.KeywordsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
                        break;
                    }
                    if ((token.CsTokenType != CsTokenType.WhiteSpace) && (token.CsTokenType != CsTokenType.EndOfLine))
                    {
                        break;
                    }
                }
            }
        }

        private void CheckKeywordWithSpace(Node<CsToken> tokenNode)
        {
            Node<CsToken> next = tokenNode.Next;
            if ((next == null) || (((next.Value.CsTokenType != CsTokenType.WhiteSpace) && (next.Value.CsTokenType != CsTokenType.EndOfLine)) && ((next.Value.CsTokenType != CsTokenType.Semicolon) && (next.Value.CsTokenType != CsTokenType.AttributeColon))))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.KeywordsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
            }
        }

        private void CheckLabelColon(Node<CsToken> tokenNode)
        {
            Node<CsToken> next = tokenNode.Next;
            if (next != null)
            {
                CsTokenType csTokenType = next.Value.CsTokenType;
                if ((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ColonsMustBeSpacedCorrectly, new object[0]);
                }
            }
            Node<CsToken> previous = tokenNode.Previous;
            if (previous != null)
            {
                switch (previous.Value.CsTokenType)
                {
                    case CsTokenType.WhiteSpace:
                    case CsTokenType.EndOfLine:
                    case CsTokenType.SingleLineComment:
                    case CsTokenType.MultiLineComment:
                        base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.ColonsMustBeSpacedCorrectly, new object[0]);
                        break;
                }
            }
        }

        private void CheckMemberAccessSymbol(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if (previous != null)
            {
                CsTokenType csTokenType = previous.Value.CsTokenType;
                if ((((csTokenType == CsTokenType.WhiteSpace) || (csTokenType == CsTokenType.EndOfLine)) || ((csTokenType == CsTokenType.SingleLineComment) || (csTokenType == CsTokenType.MultiLineComment))) && !this.IsTokenFirstNonWhitespaceTokenOnLine(tokens, tokenNode))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.MemberAccessSymbolsMustBeSpacedCorrectly, new object[0]);
                }
            }
            Node<CsToken> next = tokenNode.Next;
            if (next != null)
            {
                switch (next.Value.CsTokenType)
                {
                    case CsTokenType.WhiteSpace:
                    case CsTokenType.EndOfLine:
                    case CsTokenType.SingleLineComment:
                    case CsTokenType.MultiLineComment:
                        if (previous != null)
                        {
                            foreach (CsToken token in tokens.ReverseIterator(previous))
                            {
                                CsTokenType type3 = token.CsTokenType;
                                if (type3 == CsTokenType.Operator)
                                {
                                    return;
                                }
                                if (((type3 != CsTokenType.WhiteSpace) && (type3 != CsTokenType.EndOfLine)) && ((type3 != CsTokenType.SingleLineComment) && (type3 != CsTokenType.MultiLineComment)))
                                {
                                    break;
                                }
                            }
                        }
                        base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.MemberAccessSymbolsMustBeSpacedCorrectly, new object[0]);
                        break;
                }
            }
        }

        private void CheckNewKeywordSpacing(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> next = tokenNode.Next;
            if (next != null)
            {
                if ((next.Value.CsTokenType == CsTokenType.WhiteSpace) || (next.Value.CsTokenType == CsTokenType.EndOfLine))
                {
                    foreach (CsToken token in tokens.ForwardIterator(next.Next))
                    {
                        if (token.CsTokenType == CsTokenType.OpenSquareBracket)
                        {
                            base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.CodeMustNotContainSpaceAfterNewKeywordInImplicitlyTypedArrayAllocation, new object[0]);
                            break;
                        }
                        if ((token.CsTokenType != CsTokenType.WhiteSpace) && (token.CsTokenType != CsTokenType.EndOfLine))
                        {
                            break;
                        }
                    }
                }
                else if (next.Value.CsTokenType != CsTokenType.OpenSquareBracket)
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.KeywordsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
                }
            }
        }

        private void CheckNullableTypeSymbol(Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if ((previous != null) && ((previous.Value.CsTokenType == CsTokenType.WhiteSpace) || (previous.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.NullableTypeSymbolsMustNotBePrecededBySpace, new object[0]);
            }
        }

        private void CheckOpenCurlyBracket(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if (previous != null)
            {
                CsTokenType csTokenType = previous.Value.CsTokenType;
                if (((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && (csTokenType != CsTokenType.OpenParenthesis))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningCurlyBracketsMustBeSpacedCorrectly, new object[0]);
                }
                if (csTokenType == CsTokenType.WhiteSpace)
                {
                    foreach (CsToken token in tokens.ReverseIterator(previous))
                    {
                        CsTokenType type2 = token.CsTokenType;
                        if (type2 == CsTokenType.OpenParenthesis)
                        {
                            base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningCurlyBracketsMustBeSpacedCorrectly, new object[0]);
                        }
                        else if (type2 != CsTokenType.WhiteSpace)
                        {
                            break;
                        }
                    }
                }
            }
            Node<CsToken> next = tokenNode.Next;
            if (((next != null) && (next.Value.CsTokenType != CsTokenType.WhiteSpace)) && (next.Value.CsTokenType != CsTokenType.EndOfLine))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningCurlyBracketsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckOpenParen(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            bool flag = false;
            bool flag2 = false;
            Node<CsToken> previous = tokenNode.Previous;
            if ((previous != null) && (previous.Value.CsTokenType == CsTokenType.WhiteSpace))
            {
                foreach (CsToken token in tokens.ReverseIterator(previous))
                {
                    CsTokenType csTokenType = token.CsTokenType;
                    if (csTokenType != CsTokenType.WhiteSpace)
                    {
                        if (csTokenType == CsTokenType.EndOfLine)
                        {
                            flag = true;
                            break;
                        }
                        if (((((((csTokenType == CsTokenType.Case) || (csTokenType == CsTokenType.Catch)) || ((csTokenType == CsTokenType.CloseSquareBracket) || (csTokenType == CsTokenType.Comma))) || (((csTokenType == CsTokenType.Equals) || (csTokenType == CsTokenType.Fixed)) || ((csTokenType == CsTokenType.For) || (csTokenType == CsTokenType.Foreach)))) || ((((csTokenType == CsTokenType.From) || (csTokenType == CsTokenType.Group)) || ((csTokenType == CsTokenType.If) || (csTokenType == CsTokenType.In))) || (((csTokenType == CsTokenType.Into) || (csTokenType == CsTokenType.Join)) || ((csTokenType == CsTokenType.Let) || (csTokenType == CsTokenType.Lock))))) || (((((csTokenType == CsTokenType.MultiLineComment) || (csTokenType == CsTokenType.Number)) || ((csTokenType == CsTokenType.OperatorSymbol) || (csTokenType == CsTokenType.OpenCurlyBracket))) || (((csTokenType == CsTokenType.OrderBy) || (csTokenType == CsTokenType.Return)) || ((csTokenType == CsTokenType.Select) || (csTokenType == CsTokenType.Semicolon)))) || ((((csTokenType == CsTokenType.Switch) || (csTokenType == CsTokenType.Throw)) || ((csTokenType == CsTokenType.Using) || (csTokenType == CsTokenType.Where))) || (((csTokenType == CsTokenType.While) || (csTokenType == CsTokenType.WhileDo)) || ((csTokenType == CsTokenType.Yield) || (csTokenType == CsTokenType.LabelColon)))))) || ((csTokenType == CsTokenType.Async) || (csTokenType == CsTokenType.By)))
                        {
                            break;
                        }
                        base.AddViolation(tokenNode.Value.FindParentElement(), previous.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningParenthesisMustBeSpacedCorrectly, new object[0]);
                    }
                }
            }
            Node<CsToken> next = tokenNode.Next;
            if ((next != null) && ((next.Value.CsTokenType == CsTokenType.WhiteSpace) || (next.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                foreach (CsToken token2 in tokens.ForwardIterator(next))
                {
                    CsTokenType type2 = token2.CsTokenType;
                    if (type2 == CsTokenType.EndOfLine)
                    {
                        flag2 = true;
                        break;
                    }
                    if (((type2 != CsTokenType.WhiteSpace) && (type2 != CsTokenType.SingleLineComment)) && (type2 != CsTokenType.MultiLineComment))
                    {
                        base.AddViolation(tokenNode.Value.FindParentElement(), next.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningParenthesisMustBeSpacedCorrectly, new object[0]);
                        break;
                    }
                }
            }
            if (flag && flag2)
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningParenthesisMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckOpenSquareBracket(Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if ((previous != null) && ((previous.Value.CsTokenType == CsTokenType.WhiteSpace) || (previous.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningSquareBracketsMustBeSpacedCorrectly, new object[0]);
            }
            Node<CsToken> next = tokenNode.Next;
            if ((next != null) && ((next.Value.CsTokenType == CsTokenType.WhiteSpace) || (next.Value.CsTokenType == CsTokenType.EndOfLine)))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningSquareBracketsMustBeSpacedCorrectly, new object[0]);
            }
        }

        private void CheckOperatorKeyword(Node<CsToken> tokenNode)
        {
            Node<CsToken> next = tokenNode.Next;
            if ((next != null) && (next.Value.CsTokenType != CsTokenType.WhiteSpace))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.OperatorKeywordMustBeFollowedBySpace, new object[0]);
            }
        }

        private void CheckPositiveOrNegativeSign(Node<CsToken> tokenNode, bool positiveToken)
        {
            bool flag = false;
            Node<CsToken> previous = tokenNode.Previous;
            if (previous == null)
            {
                flag = true;
            }
            else
            {
                CsTokenType csTokenType = previous.Value.CsTokenType;
                if ((((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && ((csTokenType != CsTokenType.CloseParenthesis) && (csTokenType != CsTokenType.OpenParenthesis))) && (csTokenType != CsTokenType.OpenSquareBracket))
                {
                    flag = true;
                }
                else if ((csTokenType == CsTokenType.WhiteSpace) || (csTokenType == CsTokenType.EndOfLine))
                {
                    Node<CsToken> node2 = previous.Previous;
                    if (node2 != null)
                    {
                        switch (node2.Value.CsTokenType)
                        {
                            case CsTokenType.OpenParenthesis:
                            case CsTokenType.OpenSquareBracket:
                                flag = true;
                                break;
                        }
                    }
                }
            }
            if (flag)
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, positiveToken ? ((System.Enum) StyleCop.CSharp.Rules.PositiveSignsMustBeSpacedCorrectly) : ((System.Enum) StyleCop.CSharp.Rules.NegativeSignsMustBeSpacedCorrectly), new object[0]);
            }
            Node<CsToken> next = tokenNode.Next;
            if (next != null)
            {
                switch (next.Value.CsTokenType)
                {
                    case CsTokenType.WhiteSpace:
                    case CsTokenType.EndOfLine:
                    case CsTokenType.SingleLineComment:
                    case CsTokenType.MultiLineComment:
                        base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, positiveToken ? ((System.Enum) StyleCop.CSharp.Rules.PositiveSignsMustBeSpacedCorrectly) : ((System.Enum) StyleCop.CSharp.Rules.NegativeSignsMustBeSpacedCorrectly), new object[0]);
                        break;
                }
            }
        }

        private void CheckPreprocessorSpacing(CsToken preprocessor)
        {
            if (((preprocessor.Text.Length > 1) && (preprocessor.Text[0] == '#')) && ((preprocessor.Text[1] == ' ') || (preprocessor.Text[1] == '\t')))
            {
                base.AddViolation(preprocessor.FindParentElement(), preprocessor.Location, (System.Enum) StyleCop.CSharp.Rules.PreprocessorKeywordsMustNotBePrecededBySpace, new object[0]);
            }
        }

        private void CheckSemicolonAndComma(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            bool flag = false;
            if (tokenNode.Value.Text == ",")
            {
                flag = true;
            }
            string[] strArray = new string[] { "[", "<" };
            string[] strArray2 = new string[] { "]", ">" };
            if (!flag)
            {
                strArray = new string[] { "(" };
                strArray2 = new string[] { ")" };
            }
            bool flag2 = true;
            bool flag3 = true;
            bool flag4 = false;
            Node<CsToken> previous = tokenNode.Previous;
            if (previous != null)
            {
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (previous.Value.Text == strArray[i])
                    {
                        flag4 = true;
                        break;
                    }
                }
                if (!flag4)
                {
                    if (previous.Value.Text == tokenNode.Value.Text)
                    {
                        flag4 = true;
                    }
                    else
                    {
                        flag2 = false;
                    }
                }
            }
            if (!flag4)
            {
                flag2 = false;
            }
            foreach (CsToken token in tokens.ReverseIterator(tokenNode.Previous))
            {
                if (token.CsTokenType == CsTokenType.LabelColon)
                {
                    flag2 = true;
                    break;
                }
                if ((token.CsTokenType != CsTokenType.WhiteSpace) && (token.CsTokenType != CsTokenType.EndOfLine))
                {
                    break;
                }
            }
            flag4 = false;
            previous = tokenNode.Next;
            if (previous != null)
            {
                for (int j = 0; j < strArray2.Length; j++)
                {
                    if (previous.Value.Text == strArray2[j])
                    {
                        flag4 = true;
                        break;
                    }
                }
                if (!flag4)
                {
                    if (previous.Value.Text == tokenNode.Value.Text)
                    {
                        flag4 = true;
                    }
                    else
                    {
                        flag3 = false;
                    }
                }
            }
            if (!flag4)
            {
                flag3 = false;
            }
            if (!flag2)
            {
                Node<CsToken> node2 = tokenNode.Previous;
                if ((node2 != null) && ((node2.Value.CsTokenType == CsTokenType.WhiteSpace) || (node2.Value.CsTokenType == CsTokenType.EndOfLine)))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, flag ? ((System.Enum) StyleCop.CSharp.Rules.CommasMustBeSpacedCorrectly) : ((System.Enum) StyleCop.CSharp.Rules.SemicolonsMustBeSpacedCorrectly), new object[0]);
                }
            }
            if (!flag3)
            {
                Node<CsToken> next = tokenNode.Next;
                if (((next != null) && (next.Value.CsTokenType != CsTokenType.WhiteSpace)) && ((next.Value.CsTokenType != CsTokenType.EndOfLine) && (next.Value.CsTokenType != CsTokenType.CloseParenthesis)))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, flag ? ((System.Enum) StyleCop.CSharp.Rules.CommasMustBeSpacedCorrectly) : ((System.Enum) StyleCop.CSharp.Rules.SemicolonsMustBeSpacedCorrectly), new object[0]);
                }
            }
        }

        private void CheckSingleLineComment(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            if (tokenNode.Value.Text.Length > 2)
            {
                bool flag = false;
                string text = tokenNode.Value.Text;
                if ((((text[2] != ' ') && (text[2] != '\t')) && ((text[2] != '/') && (text[2] != '\\'))) && (((text[1] != '\n') && (text[1] != '\r')) && (((text.Length < 4) || (text[2] != '-')) || (text[3] != '-'))))
                {
                    flag = true;
                }
                else if (((text.Length > 3) && ((text[3] == ' ') || (text[3] == '\t'))) && (text[2] != '\\'))
                {
                    bool flag2 = true;
                    int num = 0;
                    foreach (CsToken token in tokens.ReverseIterator(tokenNode.Previous))
                    {
                        if (token.CsTokenType == CsTokenType.EndOfLine)
                        {
                            if (++num != 2)
                            {
                                continue;
                            }
                            break;
                        }
                        if (token.CsTokenType == CsTokenType.SingleLineComment)
                        {
                            flag2 = false;
                            break;
                        }
                        if (token.CsTokenType != CsTokenType.WhiteSpace)
                        {
                            break;
                        }
                    }
                    if (flag2)
                    {
                        flag = true;
                    }
                    if (tokenNode.Value.Parent is FileHeader)
                    {
                        flag = false;
                    }
                }
                if (flag)
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.SingleLineCommentsMustBeginWithSingleSpace, new object[0]);
                }
            }
        }

        private void CheckSpacing(MasterList<CsToken> tokens, bool type, Node<CsToken> parentTokenNode)
        {
            if (tokens.Count > 0)
            {
                for (Node<CsToken> node = tokens.First; node != null; node = node.Next)
                {
                    OperatorSymbol symbol;
                    CsTokenClass class2;
                    if (base.Cancel)
                    {
                        return;
                    }
                    if (node.Value.Generated)
                    {
                        goto Label_0492;
                    }
                    switch (node.Value.CsTokenType)
                    {
                        case CsTokenType.OpenParenthesis:
                            this.CheckOpenParen(tokens, node);
                            goto Label_043B;

                        case CsTokenType.CloseParenthesis:
                            this.CheckCloseParen(tokens, node);
                            goto Label_043B;

                        case CsTokenType.OpenCurlyBracket:
                            this.CheckOpenCurlyBracket(tokens, node);
                            goto Label_043B;

                        case CsTokenType.CloseCurlyBracket:
                            this.CheckCloseCurlyBracket(tokens, node);
                            goto Label_043B;

                        case CsTokenType.OpenSquareBracket:
                            this.CheckOpenSquareBracket(node);
                            goto Label_043B;

                        case CsTokenType.CloseSquareBracket:
                            this.CheckCloseSquareBracket(tokens, node, parentTokenNode);
                            goto Label_043B;

                        case CsTokenType.OperatorSymbol:
                            symbol = node.Value as OperatorSymbol;
                            switch (symbol.Category)
                            {
                                case OperatorCategory.Relational:
                                case OperatorCategory.Logical:
                                case OperatorCategory.Assignment:
                                case OperatorCategory.Arithmetic:
                                case OperatorCategory.Shift:
                                case OperatorCategory.Conditional:
                                case OperatorCategory.Lambda:
                                    goto Label_03F6;

                                case OperatorCategory.IncrementDecrement:
                                    goto Label_0400;

                                case OperatorCategory.Unary:
                                    goto Label_0409;
                            }
                            goto Label_043B;

                        case CsTokenType.BaseColon:
                        case CsTokenType.WhereColon:
                            this.CheckSymbol(tokens, node);
                            goto Label_043B;

                        case CsTokenType.AttributeColon:
                        case CsTokenType.LabelColon:
                            this.CheckLabelColon(node);
                            goto Label_043B;

                        case CsTokenType.Comma:
                        case CsTokenType.Semicolon:
                            this.CheckSemicolonAndComma(tokens, node);
                            goto Label_043B;

                        case CsTokenType.NullableTypeSymbol:
                            this.CheckNullableTypeSymbol(node);
                            goto Label_043B;

                        case CsTokenType.By:
                        case CsTokenType.Catch:
                        case CsTokenType.Equals:
                        case CsTokenType.Fixed:
                        case CsTokenType.For:
                        case CsTokenType.Foreach:
                        case CsTokenType.From:
                        case CsTokenType.Group:
                        case CsTokenType.If:
                        case CsTokenType.In:
                        case CsTokenType.Into:
                        case CsTokenType.Join:
                        case CsTokenType.Let:
                        case CsTokenType.Lock:
                        case CsTokenType.On:
                        case CsTokenType.OrderBy:
                        case CsTokenType.Return:
                        case CsTokenType.Select:
                        case CsTokenType.Stackalloc:
                        case CsTokenType.Switch:
                        case CsTokenType.Throw:
                        case CsTokenType.Using:
                        case CsTokenType.Where:
                        case CsTokenType.While:
                        case CsTokenType.WhileDo:
                        case CsTokenType.Yield:
                        case CsTokenType.Async:
                            this.CheckKeywordWithSpace(node);
                            goto Label_043B;

                        case CsTokenType.Checked:
                        case CsTokenType.DefaultValue:
                        case CsTokenType.Sizeof:
                        case CsTokenType.Typeof:
                        case CsTokenType.Unchecked:
                            this.CheckKeywordWithoutSpace(tokens, node);
                            goto Label_043B;

                        case CsTokenType.New:
                            this.CheckNewKeywordSpacing(tokens, node);
                            goto Label_043B;

                        case CsTokenType.Operator:
                            this.CheckOperatorKeyword(node);
                            goto Label_043B;

                        case CsTokenType.WhiteSpace:
                            this.CheckWhitespace(node);
                            goto Label_043B;

                        case CsTokenType.SingleLineComment:
                            this.CheckTabsInComment(node.Value);
                            this.CheckSingleLineComment(tokens, node);
                            goto Label_043B;

                        case CsTokenType.MultiLineComment:
                            this.CheckTabsInComment(node.Value);
                            goto Label_043B;

                        case CsTokenType.PreprocessorDirective:
                            this.CheckPreprocessorSpacing(node.Value as Preprocessor);
                            goto Label_043B;

                        case CsTokenType.Attribute:
                        {
                            StyleCop.CSharp.Attribute attribute = node.Value as StyleCop.CSharp.Attribute;
                            this.CheckSpacing(attribute.ChildTokens, false, node);
                            goto Label_043B;
                        }
                        case CsTokenType.OpenAttributeBracket:
                            this.CheckAttributeTokenOpenBracket(node);
                            goto Label_043B;

                        case CsTokenType.CloseAttributeBracket:
                            this.CheckAttributeTokenCloseBracket(tokens, node);
                            goto Label_043B;

                        case CsTokenType.XmlHeader:
                        {
                            XmlHeader header = (XmlHeader) node.Value;
                            this.CheckXmlHeaderComment(header);
                            for (Node<CsToken> node2 = header.ChildTokens.First; node2 != null; node2 = node2.Next)
                            {
                                this.CheckTabsInComment(node2.Value);
                            }
                            goto Label_043B;
                        }
                        default:
                            goto Label_043B;
                    }
                    switch (symbol.SymbolType)
                    {
                        case OperatorType.Dereference:
                        case OperatorType.AddressOf:
                            this.CheckUnsafeAccessSymbols(node, type, parentTokenNode);
                            goto Label_043B;

                        case OperatorType.Pointer:
                        case OperatorType.MemberAccess:
                        case OperatorType.QualifiedAlias:
                            this.CheckMemberAccessSymbol(tokens, node);
                            goto Label_043B;

                        default:
                            goto Label_043B;
                    }
                Label_03F6:
                    this.CheckSymbol(tokens, node);
                    goto Label_043B;
                Label_0400:
                    this.CheckIncrementDecrement(node);
                    goto Label_043B;
                Label_0409:
                    if (symbol.SymbolType == OperatorType.Negative)
                    {
                        this.CheckPositiveOrNegativeSign(node, false);
                    }
                    else if (symbol.SymbolType == OperatorType.Positive)
                    {
                        this.CheckPositiveOrNegativeSign(node, true);
                    }
                    else
                    {
                        this.CheckUnarySymbol(tokens, node);
                    }
                Label_043B:
                    class2 = node.Value.CsTokenClass;
                    if (class2 != CsTokenClass.GenericType)
                    {
                        if (class2 == CsTokenClass.Type)
                        {
                            goto Label_047A;
                        }
                        if (class2 == CsTokenClass.ConstructorConstraint)
                        {
                            this.CheckSpacing(((ConstructorConstraint) node.Value).ChildTokens, false, node);
                        }
                        goto Label_0492;
                    }
                    this.CheckGenericSpacing(tokens, node);
                Label_047A:
                    this.CheckSpacing(((TypeToken) node.Value).ChildTokens, true, node);
                Label_0492:;
                }
            }
        }

        private void CheckSymbol(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if (((previous != null) && (previous.Value.CsTokenType != CsTokenType.WhiteSpace)) && (previous.Value.CsTokenType != CsTokenType.EndOfLine))
            {
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.SymbolsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
            }
            Node<CsToken> next = tokenNode.Next;
            if (((next != null) && (next.Value.CsTokenType != CsTokenType.WhiteSpace)) && (next.Value.CsTokenType != CsTokenType.EndOfLine))
            {
                if (previous != null)
                {
                    foreach (CsToken token in tokens.ReverseIterator(previous))
                    {
                        if (token.CsTokenType == CsTokenType.Operator)
                        {
                            return;
                        }
                        if (((token.CsTokenType != CsTokenType.WhiteSpace) && (token.CsTokenType != CsTokenType.EndOfLine)) && (((token.CsTokenType != CsTokenType.SingleLineComment) && (token.CsTokenType != CsTokenType.MultiLineComment)) && (token.CsTokenType != CsTokenType.PreprocessorDirective)))
                        {
                            break;
                        }
                    }
                }
                base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.LineNumber, (System.Enum) StyleCop.CSharp.Rules.SymbolsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
            }
        }

        private void CheckTabsInComment(CsToken comment)
        {
            int num = 0;
            for (int i = 0; i < comment.Text.Length; i++)
            {
                if (comment.Text[i] == '\t')
                {
                    base.AddViolation(comment.FindParentElement(), comment.Location, (System.Enum) StyleCop.CSharp.Rules.TabsMustNotBeUsed, new object[0]);
                }
                else if (comment.Text[i] == '\n')
                {
                    num++;
                }
            }
        }

        private void CheckUnarySymbol(MasterList<CsToken> tokens, Node<CsToken> tokenNode)
        {
            Node<CsToken> previous = tokenNode.Previous;
            if (previous != null)
            {
                CsTokenType csTokenType = previous.Value.CsTokenType;
                if ((((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && ((csTokenType != CsTokenType.OpenParenthesis) && (csTokenType != CsTokenType.OpenSquareBracket))) && (csTokenType != CsTokenType.CloseParenthesis))
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.SymbolsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
                }
                if ((csTokenType == CsTokenType.WhiteSpace) || (csTokenType == CsTokenType.EndOfLine))
                {
                    foreach (CsToken token in tokens.ReverseIterator(previous))
                    {
                        if ((token.CsTokenType == CsTokenType.OpenParenthesis) || (token.CsTokenType == CsTokenType.OpenSquareBracket))
                        {
                            base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.SymbolsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
                        }
                        else if (token.CsTokenType == CsTokenType.WhiteSpace)
                        {
                            continue;
                        }
                        if (((token.CsTokenType != CsTokenType.OpenParenthesis) && (token.CsTokenType != CsTokenType.OpenSquareBracket)) && (token.CsTokenType != CsTokenType.WhiteSpace))
                        {
                            break;
                        }
                    }
                }
            }
            Node<CsToken> next = tokenNode.Next;
            if (next != null)
            {
                switch (next.Value.CsTokenType)
                {
                    case CsTokenType.WhiteSpace:
                    case CsTokenType.EndOfLine:
                    case CsTokenType.SingleLineComment:
                    case CsTokenType.MultiLineComment:
                        base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.SymbolsMustBeSpacedCorrectly, new object[] { tokenNode.Value.Text });
                        break;
                }
            }
        }

        private void CheckUnsafeAccessSymbols(Node<CsToken> tokenNode, bool type, Node<CsToken> parentTokenNode)
        {
            if (type)
            {
                bool flag = false;
                Node<CsToken> node = tokenNode.Next ?? parentTokenNode.Next;
                if (node == null)
                {
                    flag = true;
                }
                else
                {
                    CsTokenType csTokenType = node.Value.CsTokenType;
                    if ((((csTokenType != CsTokenType.WhiteSpace) && (csTokenType != CsTokenType.EndOfLine)) && ((csTokenType != CsTokenType.OpenParenthesis) && (csTokenType != CsTokenType.OpenSquareBracket))) && ((csTokenType != CsTokenType.CloseParenthesis) && (csTokenType != tokenNode.Value.CsTokenType)))
                    {
                        flag = true;
                    }
                }
                if (flag)
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.DereferenceAndAccessOfSymbolsMustBeSpacedCorrectly, new object[0]);
                }
                Node<CsToken> previous = tokenNode.Previous;
                if (previous != null)
                {
                    switch (previous.Value.CsTokenType)
                    {
                        case CsTokenType.WhiteSpace:
                        case CsTokenType.EndOfLine:
                        case CsTokenType.SingleLineComment:
                        case CsTokenType.MultiLineComment:
                            base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.DereferenceAndAccessOfSymbolsMustBeSpacedCorrectly, new object[0]);
                            return;
                    }
                }
            }
            else
            {
                bool flag2 = false;
                Node<CsToken> node3 = tokenNode.Previous;
                if (node3 == null)
                {
                    flag2 = true;
                }
                else
                {
                    CsTokenType type4 = node3.Value.CsTokenType;
                    if ((((type4 != CsTokenType.WhiteSpace) && (type4 != CsTokenType.EndOfLine)) && ((type4 != CsTokenType.OpenParenthesis) && (type4 != CsTokenType.OpenSquareBracket))) && ((type4 != CsTokenType.CloseParenthesis) && (type4 != tokenNode.Value.CsTokenType)))
                    {
                        flag2 = true;
                    }
                }
                if (flag2)
                {
                    base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.DereferenceAndAccessOfSymbolsMustBeSpacedCorrectly, new object[0]);
                }
                Node<CsToken> next = tokenNode.Next;
                if (next != null)
                {
                    switch (next.Value.CsTokenType)
                    {
                        case CsTokenType.WhiteSpace:
                        case CsTokenType.EndOfLine:
                        case CsTokenType.SingleLineComment:
                        case CsTokenType.MultiLineComment:
                            base.AddViolation(tokenNode.Value.FindParentElement(), tokenNode.Value.Location, (System.Enum) StyleCop.CSharp.Rules.DereferenceAndAccessOfSymbolsMustBeSpacedCorrectly, new object[0]);
                            break;
                    }
                }
            }
        }

        private void CheckWhitespace(Node<CsToken> tokenNode)
        {
            Whitespace whitespace = (Whitespace) tokenNode.Value;
            if (whitespace.TabCount > 0)
            {
                ICodeElement element = tokenNode.Value.FindParentElement();
                if (element != null)
                {
                    base.AddViolation(element, whitespace.Location, StyleCop.CSharp.Rules.TabsMustNotBeUsed, new object[0]);
                }
            }
            else if ((whitespace.TabCount == 0) && (whitespace.SpaceCount > 1))
            {
                Node<CsToken> next = tokenNode.Next;
                Node<CsToken> previous = tokenNode.Previous;
                if (((((previous != null) && (previous.Value.CsTokenType != CsTokenType.EndOfLine)) && ((previous.Value.CsTokenType != CsTokenType.Comma) && (previous.Value.CsTokenType != CsTokenType.Semicolon))) && (((next != null) && (next.Value.CsTokenType != CsTokenType.OperatorSymbol)) && ((next.Value.CsTokenType != CsTokenType.EndOfLine) && (next.Value.CsTokenType != CsTokenType.SingleLineComment)))) && (next.Value.CsTokenType != CsTokenType.MultiLineComment))
                {
                    CsElement element2 = tokenNode.Value.FindParentElement() ?? previous.Value.FindParentElement();
                    if (element2 != null)
                    {
                        base.AddViolation(element2, whitespace.Location, (System.Enum) StyleCop.CSharp.Rules.CodeMustNotContainMultipleWhitespaceInARow, new object[0]);
                    }
                }
            }
        }

        private void CheckXmlHeaderComment(XmlHeader header)
        {
            for (Node<CsToken> node = header.ChildTokens.First; node != null; node = node.Next)
            {
                CsToken part = node.Value;
                if ((part.CsTokenType == CsTokenType.XmlHeaderLine) && (part.Text.Length > 3))
                {
                    if ((((part.Text[3] != ' ') && (part.Text[3] != '\t')) && ((part.Text[3] != '/') && (part.Text[2] != '\n'))) && (part.Text[2] != '\r'))
                    {
                        base.AddViolation(node.Value.FindParentElement(), part.Location, (System.Enum) StyleCop.CSharp.Rules.DocumentationLinesMustBeginWithSingleSpace, new object[0]);
                        continue;
                    }
                    if ((part.Text.Length > 4) && ((part.Text[4] == ' ') || (part.Text[4] == '\t')))
                    {
                        bool flag = true;
                        for (Node<CsToken> node2 = node.Previous; node2 != null; node2 = node2.Previous)
                        {
                            if (node2.Value.CsTokenType == CsTokenType.XmlHeaderLine)
                            {
                                for (Node<CsToken> node3 = node.Next; node3 != null; node3 = node3.Next)
                                {
                                    if (node3.Value.CsTokenType == CsTokenType.XmlHeaderLine)
                                    {
                                        flag = false;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        if (flag)
                        {
                            base.AddViolation(part.FindParentElement(), part.Location, (System.Enum) StyleCop.CSharp.Rules.DocumentationLinesMustBeginWithSingleSpace, new object[0]);
                        }
                    }
                }
            }
        }

        public override bool DoAnalysis(CodeDocument document)
        {
            Param.RequireNotNull(document, "document");
            CsDocument document2 = (CsDocument) document;
            if (document2.FileHeader != null)
            {
                return !document2.FileHeader.UnStyled;
            }
            return true;
        }

        private static bool IsAllowedAfterClosingParenthesis(CsToken token)
        {
            CsTokenType csTokenType = token.CsTokenType;
            switch (csTokenType)
            {
                case CsTokenType.CloseParenthesis:
                case CsTokenType.OpenParenthesis:
                case CsTokenType.CloseSquareBracket:
                case CsTokenType.OpenSquareBracket:
                case CsTokenType.CloseAttributeBracket:
                case CsTokenType.Semicolon:
                case CsTokenType.Comma:
                    return true;
            }
            if (csTokenType == CsTokenType.OperatorSymbol)
            {
                OperatorSymbol symbol = (OperatorSymbol) token;
                if (((symbol.SymbolType == OperatorType.Decrement) || (symbol.SymbolType == OperatorType.Increment)) || ((symbol.SymbolType == OperatorType.MemberAccess) || (symbol.SymbolType == OperatorType.Pointer)))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsTokenADot(CsToken token)
        {
            if (token.CsTokenType == CsTokenType.OperatorSymbol)
            {
                OperatorSymbol symbol = (OperatorSymbol) token;
                if (symbol.SymbolType == OperatorType.MemberAccess)
                {
                    return (symbol.Text == ".");
                }
            }
            return false;
        }

        private bool IsTokenFirstNonWhitespaceTokenOnLine(MasterList<CsToken> tokens, Node<CsToken> node)
        {
            Node<CsToken> previous = node.Previous;
            if (previous == null)
            {
                return true;
            }
            bool flag = true;
            foreach (CsToken token in tokens.ReverseIterator(previous))
            {
                if (token.LineNumber != node.Value.LineNumber)
                {
                    return flag;
                }
                if (token.CsTokenType != CsTokenType.WhiteSpace)
                {
                    return false;
                }
            }
            return flag;
        }
    }
}

