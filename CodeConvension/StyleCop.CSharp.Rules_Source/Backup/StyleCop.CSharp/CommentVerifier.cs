namespace StyleCop.CSharp
{
    using StyleCop.Spelling;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Xml;

    internal static class CommentVerifier
    {
        internal const int MinimumCharacterPercentage = 40;
        internal const int MinimumHeaderCommentLength = 10;

        private static void AddAttributeValue(StringBuilder commentWithAttributesBuilder, XmlNode childNode, string attributeName)
        {
            if (childNode.Attributes != null)
            {
                XmlAttribute attribute = childNode.Attributes[attributeName];
                if (attribute != null)
                {
                    commentWithAttributesBuilder.Append(attribute.Value);
                }
            }
        }

        public static void ExtractTextFromCommentXml(XmlNode commentXml, out string textWithAttributesRemoved, out string textWithAttributesPreserved)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder commentWithAttributesBuilder = new StringBuilder();
            foreach (XmlNode node in commentXml.ChildNodes)
            {
                if (node.NodeType == XmlNodeType.Text)
                {
                    builder.Append(node.Value);
                    commentWithAttributesBuilder.Append(node.Value);
                }
                else
                {
                    switch (node.Name)
                    {
                        case "typeparamref":
                        case "typeparam":
                        case "paramref":
                        case "param":
                            AddAttributeValue(commentWithAttributesBuilder, node, "name");
                            goto Label_019A;

                        case "exception":
                        case "event":
                        case "permission":
                            AddAttributeValue(commentWithAttributesBuilder, node, "cref");
                            goto Label_019A;

                        case "see":
                            if (node.ChildNodes.Count == 0)
                            {
                                AddAttributeValue(commentWithAttributesBuilder, node, "cref");
                                AddAttributeValue(commentWithAttributesBuilder, node, "href");
                                AddAttributeValue(commentWithAttributesBuilder, node, "langword");
                            }
                            goto Label_019A;

                        case "seealso":
                            if (node.ChildNodes.Count == 0)
                            {
                                AddAttributeValue(commentWithAttributesBuilder, node, "cref");
                                AddAttributeValue(commentWithAttributesBuilder, node, "href");
                            }
                            goto Label_019A;
                    }
                }
            Label_019A:
                if (node.HasChildNodes)
                {
                    string str;
                    string str2;
                    ExtractTextFromCommentXml(node, out str, out str2);
                    builder.Append(" ");
                    commentWithAttributesBuilder.Append(" ");
                    if ((node.Name != "c") && (node.Name != "code"))
                    {
                        builder.Append(str);
                    }
                    commentWithAttributesBuilder.Append(str2);
                }
            }
            textWithAttributesRemoved = builder.ToString().Trim();
            textWithAttributesPreserved = commentWithAttributesBuilder.ToString().Trim();
        }

        public static InvalidCommentType IsGarbageComment(XmlNode commentXml, CsElement element, out string spellingError)
        {
            string innerText = commentXml.InnerText;
            string textWithAttributesPreserved = commentXml.InnerText;
            if (commentXml.HasChildNodes && ((commentXml.ChildNodes.Count > 1) || (commentXml.ChildNodes[0].NodeType != XmlNodeType.Text)))
            {
                ExtractTextFromCommentXml(commentXml, out innerText, out textWithAttributesPreserved);
            }
            return IsGarbageComment(innerText, textWithAttributesPreserved, element, out spellingError);
        }

        public static InvalidCommentType IsGarbageComment(string commentWithAttributesRemoved, string commentWithAttributesPreserved, CsElement element, out string spellingError)
        {
            spellingError = null;
            InvalidCommentType valid = InvalidCommentType.Valid;
            string text = commentWithAttributesRemoved.Trim();
            string str2 = commentWithAttributesPreserved.Trim();
            string str3 = str2.TrimEnd(new char[] { '.' }).Trim();
            if (string.IsNullOrEmpty(str2))
            {
                return (valid | InvalidCommentType.Empty);
            }
            if (TextContainsIncorectSpelling(element, text, out spellingError))
            {
                valid |= InvalidCommentType.IncorrectSpelling;
            }
            if (str2.Length < 10)
            {
                valid |= InvalidCommentType.TooShort;
            }
            if (!char.IsUpper(str2[0]) && !char.IsDigit(str2[0]))
            {
                valid |= InvalidCommentType.NoCapitalLetter;
            }
            if (str2[str2.Length - 1] != '.')
            {
                valid |= InvalidCommentType.NoPeriod;
            }
            int num = 0;
            int num2 = 0;
            bool flag = false;
            foreach (char ch in str3)
            {
                if (char.IsLetter(ch))
                {
                    num++;
                }
                else if (char.IsWhiteSpace(ch))
                {
                    flag = true;
                }
                else
                {
                    num2++;
                }
            }
            if ((num == 0) || (((num * 100) / (num + num2)) < 40))
            {
                valid |= InvalidCommentType.TooFewCharacters;
            }
            if (!flag)
            {
                valid |= InvalidCommentType.NoWhitespace;
            }
            return valid;
        }

        private static bool IsSpelledCorrectly(NamingService namingService, string word)
        {
            return (((namingService.GetPreferredAlternateForDeprecatedWord(word) == null) && (namingService.GetCompoundAlternateForDiscreteWord(word) == null)) && (namingService.CheckSpelling(word) != WordSpelling.Unrecognized));
        }

        private static bool TextContainsIncorectSpelling(CsElement element, string text, out string spellingError)
        {
            NamingService namingService = NamingService.GetNamingService(element.Document.SourceCode.Project.Culture);
            spellingError = string.Empty;
            if (namingService.SupportsSpelling)
            {
                WordParser parser = new WordParser(text, WordParserOptions.SplitCompoundWords);
                if (parser.PeekWord() != null)
                {
                    ICollection<string> recognizedWords = element.Document.SourceCode.Project.RecognizedWords;
                    string item = parser.NextWord();
                    do
                    {
                        if ((!item.StartsWith("$") || !item.EndsWith("$")) && (!recognizedWords.Contains(item) && !IsSpelledCorrectly(namingService, item)))
                        {
                            if (!string.IsNullOrEmpty(spellingError))
                            {
                                spellingError = spellingError + ", ";
                            }
                            spellingError = spellingError + item;
                        }
                    }
                    while ((item = parser.NextWord()) != null);
                }
            }
            return !string.IsNullOrEmpty(spellingError);
        }
    }
}

