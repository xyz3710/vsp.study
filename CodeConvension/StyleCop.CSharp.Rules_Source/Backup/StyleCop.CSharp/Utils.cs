namespace StyleCop.CSharp
{
    using StyleCop;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal class Utils
    {
        private static void AddClassMember(Dictionary<string, List<CsElement>> members, CsElement child, string name)
        {
            int index = name.IndexOf('<');
            if (index > -1)
            {
                AddClassMemberAux(members, child, name.Substring(0, index));
            }
            AddClassMemberAux(members, child, name);
        }

        private static void AddClassMemberAux(Dictionary<string, List<CsElement>> members, CsElement child, string name)
        {
            List<CsElement> list = null;
            if (!members.TryGetValue(name, out list))
            {
                list = new List<CsElement>(1);
                members.Add(name, list);
            }
            list.Add(child);
        }

        public static Dictionary<string, List<CsElement>> CollectClassMembers(ClassBase parentClass)
        {
            Dictionary<string, List<CsElement>> members = new Dictionary<string, List<CsElement>>();
            if (parentClass.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Partial }))
            {
                foreach (ClassBase base2 in parentClass.PartialElementList)
                {
                    CollectClassMembersAux(base2, members);
                }
                return members;
            }
            CollectClassMembersAux(parentClass, members);
            return members;
        }

        private static void CollectClassMembersAux(ClassBase @class, Dictionary<string, List<CsElement>> members)
        {
            foreach (CsElement element in @class.ChildElements)
            {
                if (element.ElementType == ElementType.Field)
                {
                    foreach (VariableDeclaratorExpression expression in ((Field) element).VariableDeclarationStatement.Declarators)
                    {
                        AddClassMember(members, element, expression.Identifier.Text);
                    }
                    continue;
                }
                if (element.ElementType == ElementType.Event)
                {
                    foreach (EventDeclaratorExpression expression2 in ((Event) element).Declarators)
                    {
                        AddClassMember(members, element, expression2.Identifier.Text);
                    }
                    continue;
                }
                if (element.ElementType != ElementType.EmptyElement)
                {
                    AddClassMember(members, element, element.Declaration.Name);
                }
            }
        }

        public static bool ContainsPartialMembers(CsElement element)
        {
            if ((((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Struct)) || (element.ElementType == ElementType.Interface)) && element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Partial }))
            {
                return true;
            }
            if (((element.ElementType == ElementType.Root) || (element.ElementType == ElementType.Namespace)) || ((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Struct)))
            {
                foreach (CsElement element2 in element.ChildElements)
                {
                    if (ContainsPartialMembers(element2))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static CsToken ExtractBaseClassMemberName(Expression parentExpression, StyleCop.Node<CsToken><CsToken> baseTokenNode)
        {
            bool flag = false;
            foreach (CsToken token in parentExpression.Tokens.ForwardIterator(baseTokenNode.Next))
            {
                if ((((token.CsTokenType != CsTokenType.WhiteSpace) && (token.CsTokenType != CsTokenType.EndOfLine)) && ((token.CsTokenType != CsTokenType.SingleLineComment) && (token.CsTokenType != CsTokenType.MultiLineComment))) && (token.CsTokenType != CsTokenType.PreprocessorDirective))
                {
                    if (flag)
                    {
                        if (token.CsTokenType == CsTokenType.Other)
                        {
                            return token;
                        }
                        break;
                    }
                    if ((token.CsTokenType != CsTokenType.OperatorSymbol) || (((OperatorSymbol) token).SymbolType != OperatorType.MemberAccess))
                    {
                        break;
                    }
                    flag = true;
                }
            }
            return null;
        }

        public static ICollection<CsElement> FindClassMember(string word, ClassBase parentClass, Dictionary<string, List<CsElement>> members, bool interfaces)
        {
            if (word != parentClass.Declaration.Name)
            {
                ICollection<CsElement> is2 = MatchClassMember(word, members, interfaces);
                if ((is2 != null) && (is2.Count > 0))
                {
                    return is2;
                }
            }
            return null;
        }

        public static ClassBase GetClassBase(CsElement element)
        {
            bool flag = false;
            ICodePart parent = element.Parent;
            while (!flag && (parent != null))
            {
                if (parent is ClassBase)
                {
                    flag = true;
                }
                else
                {
                    parent = parent.Parent;
                }
            }
            return (parent as ClassBase);
        }

        public static bool HasABaseClassSpecified(ClassBase classBase)
        {
            if (!string.IsNullOrEmpty(classBase.BaseClass))
            {
                return true;
            }
            if (classBase.PartialElementList == null)
            {
                return false;
            }
            if (CS$<>9__CachedAnonymousMethodDelegate1 == null)
            {
                CS$<>9__CachedAnonymousMethodDelegate1 = new Func<Class, bool>(null, (IntPtr) <HasABaseClassSpecified>b__0);
            }
            return Enumerable.Any<Class>(classBase.PartialElementList.OfType<Class>(), CS$<>9__CachedAnonymousMethodDelegate1);
        }

        public static bool HasImplementedInterfaces(ClassBase classBase)
        {
            if (classBase.ImplementedInterfaces.Count > 0)
            {
                return true;
            }
            if (classBase.PartialElementList == null)
            {
                return false;
            }
            if (CS$<>9__CachedAnonymousMethodDelegate3 == null)
            {
                CS$<>9__CachedAnonymousMethodDelegate3 = new Func<Class, bool>(null, (IntPtr) <HasImplementedInterfaces>b__2);
            }
            return Enumerable.Any<Class>(classBase.PartialElementList.OfType<Class>(), CS$<>9__CachedAnonymousMethodDelegate3);
        }

        public static bool IsAReSharperComment(CsToken token)
        {
            if (((token.CsTokenType != CsTokenType.MultiLineComment) && (token.CsTokenType != CsTokenType.SingleLineComment)) && ((token.CsTokenType != CsTokenType.XmlHeader) && (token.CsTokenType != CsTokenType.XmlHeaderLine)))
            {
                return false;
            }
            string text = token.Text;
            if (!text.StartsWith("// ReSharper disable "))
            {
                return text.StartsWith("// ReSharper restore ");
            }
            return true;
        }

        public static bool IsExpressionInsideContainer(Expression expresion, params System.Type[] codeUnits)
        {
            for (ICodePart part = expresion.Parent; part != null; part = part.Parent)
            {
                foreach (System.Type type in codeUnits)
                {
                    if (part.GetType() == type)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static ICollection<CsElement> MatchClassMember(string word, Dictionary<string, List<CsElement>> members, bool interfaces)
        {
            List<CsElement> list = null;
            List<CsElement> list2;
            if (members.TryGetValue(word, out list2))
            {
                foreach (CsElement element in list2)
                {
                    if (((element.ElementType == ElementType.Field) || (element.Declaration.Name == word)) || (interfaces && element.Declaration.Name.EndsWith("." + word, StringComparison.Ordinal)))
                    {
                        if (list == null)
                        {
                            list = new List<CsElement>();
                        }
                        list.Add(element);
                    }
                }
            }
            return list;
        }

        public static bool TokenContainNullable(StyleCop.Node<CsToken><CsToken> token)
        {
            if ((!CsTokenList.MatchTokens(StringComparison.Ordinal, token, new string[] { "System", ".", "Nullable" }) && !CsTokenList.MatchTokens(StringComparison.Ordinal, token, new string[] { "global", "::", "System", ".", "Nullable" })) && !token.Value.Text.Equals("Nullable", StringComparison.Ordinal))
            {
                return false;
            }
            return true;
        }
    }
}

