namespace StyleCop.CSharp
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;

    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), CompilerGenerated, DebuggerNonUserCode]
    internal class CodeStrings
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal CodeStrings()
        {
        }

        internal static string Class
        {
            get
            {
                return ResourceManager.GetString("Class", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string ExampleHeaderSummaryForDestructor
        {
            get
            {
                return ResourceManager.GetString("ExampleHeaderSummaryForDestructor", resourceCulture);
            }
        }

        internal static string ExampleHeaderSummaryForInstanceConstructor
        {
            get
            {
                return ResourceManager.GetString("ExampleHeaderSummaryForInstanceConstructor", resourceCulture);
            }
        }

        internal static string ExampleHeaderSummaryForPrivateInstanceConstructor
        {
            get
            {
                return ResourceManager.GetString("ExampleHeaderSummaryForPrivateInstanceConstructor", resourceCulture);
            }
        }

        internal static string ExampleHeaderSummaryForStaticConstructor
        {
            get
            {
                return ResourceManager.GetString("ExampleHeaderSummaryForStaticConstructor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForBooleanGetAccessor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForBooleanGetAccessor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForBooleanGetAndSetAccessor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForBooleanGetAndSetAccessor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForBooleanSetAccessor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForBooleanSetAccessor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForDestructor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForDestructor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForGetAccessor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForGetAccessor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForGetAndSetAccessor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForGetAndSetAccessor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForInstanceConstructor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForInstanceConstructor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForPrivateInstanceConstructor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForPrivateInstanceConstructor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForSetAccessor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForSetAccessor", resourceCulture);
            }
        }

        internal static string HeaderSummaryForStaticConstructor
        {
            get
            {
                return ResourceManager.GetString("HeaderSummaryForStaticConstructor", resourceCulture);
            }
        }

        internal static string ParameterNotUsed
        {
            get
            {
                return ResourceManager.GetString("ParameterNotUsed", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("StyleCop.CSharp.CodeStrings", typeof(CodeStrings).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static string Struct
        {
            get
            {
                return ResourceManager.GetString("Struct", resourceCulture);
            }
        }
    }
}

