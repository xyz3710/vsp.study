namespace StyleCop.CSharp
{
    using StyleCop;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [SourceAnalyzer(typeof(CsParser))]
    public class LayoutRules : SourceAnalyzer
    {
        public override void AnalyzeDocument(CodeDocument document)
        {
            Param.RequireNotNull(document, "document");
            CsDocument document2 = (CsDocument) document;
            if ((document2.RootElement != null) && !document2.RootElement.Generated)
            {
                document2.WalkDocument(new CodeWalkerElementVisitor<object>(this.VisitElement), new CodeWalkerStatementVisitor<object>(this.CheckStatementCurlyBracketPlacement), new CodeWalkerExpressionVisitor<object>(this.CheckExpressionCurlyBracketPlacement));
                this.CheckLineSpacing(document2);
            }
        }

        private static bool BracketSharesLine(StyleCop.Node<CsToken><CsToken> bracketNode, bool allowTrailingCharacters)
        {
            bool flag = false;
            CsToken token = null;
            for (StyleCop.Node<CsToken><CsToken> node = bracketNode.Next; node != null; node = node.Next)
            {
                CsToken token2 = node.Value;
                if (token2.CsTokenType == CsTokenType.EndOfLine)
                {
                    break;
                }
                if (((token2.CsTokenType != CsTokenType.WhiteSpace) && (token2.CsTokenType != CsTokenType.SingleLineComment)) && (token2.CsTokenType != CsTokenType.MultiLineComment))
                {
                    token = token2;
                    break;
                }
            }
            if ((token != null) && (!allowTrailingCharacters || ((((token.CsTokenType != CsTokenType.Semicolon) && (token.CsTokenType != CsTokenType.Comma)) && (!IsTokenADot(token) && (token.CsTokenType != CsTokenType.CloseParenthesis))) && (token.CsTokenType != CsTokenType.CloseSquareBracket))))
            {
                flag = true;
            }
            if (!flag)
            {
                for (StyleCop.Node<CsToken><CsToken> node2 = bracketNode.Previous; node2 != null; node2 = node2.Previous)
                {
                    CsToken token3 = node2.Value;
                    if (token3.CsTokenType == CsTokenType.EndOfLine)
                    {
                        return flag;
                    }
                    if ((token3.CsTokenType != CsTokenType.WhiteSpace) && (token3.CsTokenType != CsTokenType.SingleLineComment))
                    {
                        return true;
                    }
                }
            }
            return flag;
        }

        private void CheckBracketPlacement(CsElement parentElement, Statement parentStatement, CsTokenList tokens, StyleCop.Node<CsToken><CsToken> openBracketNode, bool allowAllOnOneLine)
        {
            Bracket bracket = (Bracket) openBracketNode.Value;
            if (((bracket.MatchingBracket != null) && !bracket.Generated) && !bracket.MatchingBracket.Generated)
            {
                if (bracket.LineNumber == bracket.MatchingBracket.LineNumber)
                {
                    if (allowAllOnOneLine)
                    {
                        if (tokens.First.Value.LineNumber != tokens.Last.Value.LineNumber)
                        {
                            bool flag = false;
                            if (parentStatement is VariableDeclarationStatement)
                            {
                                VariableDeclarationStatement statement = parentStatement as VariableDeclarationStatement;
                                foreach (VariableDeclaratorExpression expression in statement.Declarators)
                                {
                                    if (CS$<>9__CachedAnonymousMethodDelegate1 == null)
                                    {
                                        CS$<>9__CachedAnonymousMethodDelegate1 = new Func<Expression, bool>(null, (IntPtr) <CheckBracketPlacement>b__0);
                                    }
                                    flag = Enumerable.Any<Expression>(expression.Initializer.ChildExpressions, CS$<>9__CachedAnonymousMethodDelegate1);
                                }
                            }
                            if (!(parentStatement is VariableDeclarationStatement) || !flag)
                            {
                                base.AddViolation(parentElement, bracket.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CurlyBracketsForMultiLineStatementsMustNotShareLine, new object[] { GetOpeningOrClosingBracketText(bracket) });
                            }
                        }
                    }
                    else if (parentStatement == null)
                    {
                        base.AddViolation(parentElement, bracket.LineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementMustNotBeOnSingleLine, new object[] { parentElement.FriendlyTypeText });
                    }
                    else if (parentStatement.StatementType != StatementType.ConstructorInitializer)
                    {
                        base.AddViolation(parentElement, bracket.LineNumber, (System.Enum) StyleCop.CSharp.Rules.StatementMustNotBeOnSingleLine, new object[0]);
                    }
                }
                else
                {
                    if (BracketSharesLine(openBracketNode, false))
                    {
                        base.AddViolation(parentElement, bracket.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CurlyBracketsForMultiLineStatementsMustNotShareLine, new object[] { GetOpeningOrClosingBracketText(bracket) });
                    }
                    if (BracketSharesLine(bracket.MatchingBracketNode, true))
                    {
                        base.AddViolation(parentElement, bracket.MatchingBracket.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CurlyBracketsForMultiLineStatementsMustNotShareLine, new object[] { GetOpeningOrClosingBracketText(bracket.MatchingBracket) });
                    }
                }
            }
        }

        private void CheckChildElementSpacing(CsElement element)
        {
            CsElement element2 = null;
            if ((element.ChildElements != null) && (element.ChildElements.Count > 0))
            {
                foreach (CsElement element3 in element.ChildElements)
                {
                    if ((((element2 != null) && !element2.Generated) && !element3.Generated) && ((((element2.ElementType != element3.ElementType) || (element3.Header != null)) || ((element2.Location.LineSpan > 1) && (element3.ElementType != ElementType.AssemblyOrModuleAttribute))) || ((((element3.ElementType != ElementType.UsingDirective) && (element3.ElementType != ElementType.ExternAliasDirective)) && ((element3.ElementType != ElementType.Accessor) && (element3.ElementType != ElementType.EnumItem))) && ((element3.ElementType != ElementType.Field) && (element3.ElementType != ElementType.AssemblyOrModuleAttribute)))))
                    {
                        int lineNumber = element3.LineNumber;
                        if (element3.Header != null)
                        {
                            lineNumber = element3.Header.LineNumber;
                        }
                        if ((lineNumber == element2.Location.EndPoint.LineNumber) || (lineNumber == (element2.Location.EndPoint.LineNumber + 1)))
                        {
                            base.AddViolation(element3, StyleCop.CSharp.Rules.ElementsMustBeSeparatedByBlankLine, new object[0]);
                        }
                    }
                    element2 = element3;
                }
            }
        }

        private void CheckElementBracketPlacement(CsElement element, bool allowAllOnOneLine)
        {
            CsToken token = null;
            if (element.Declaration.Tokens.First != null)
            {
                token = element.Declaration.Tokens.Last.Value;
            }
            bool flag = false;
            for (StyleCop.Node<CsToken><CsToken> node = element.Tokens.First; !element.Tokens.OutOfBounds(node); node = node.Next)
            {
                if ((token == null) || flag)
                {
                    if (node.Value.CsTokenType == CsTokenType.Equals)
                    {
                        allowAllOnOneLine = true;
                    }
                    else if (node.Value.CsTokenType == CsTokenType.OpenCurlyBracket)
                    {
                        this.CheckBracketPlacement(element, null, element.Tokens, node, allowAllOnOneLine);
                        if (allowAllOnOneLine && (element.ElementType == ElementType.Accessor))
                        {
                            this.CheckSiblingAccessors(element, node);
                            return;
                        }
                        break;
                    }
                }
                if (!flag)
                {
                    flag = node.Value == token;
                }
            }
        }

        private void CheckElementCurlyBracketPlacement(CsElement element)
        {
            if (!element.Generated)
            {
                if (element.ElementType == ElementType.Accessor)
                {
                    this.CheckElementBracketPlacement(element, true);
                }
                else if ((((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Constructor)) || ((element.ElementType == ElementType.Destructor) || (element.ElementType == ElementType.Enum))) || ((((element.ElementType == ElementType.Event) || (element.ElementType == ElementType.Indexer)) || ((element.ElementType == ElementType.Interface) || (element.ElementType == ElementType.Method))) || ((element.ElementType == ElementType.Namespace) || (element.ElementType == ElementType.Struct))))
                {
                    bool allowAllOnOneLine = false;
                    if (element.ElementType == ElementType.Indexer)
                    {
                        CsElement element2 = element.FindParentElement();
                        if ((element2 != null) && ((element2.ElementType == ElementType.Interface) || ((element2.ElementType == ElementType.Class) && element2.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Abstract }))))
                        {
                            allowAllOnOneLine = true;
                        }
                    }
                    this.CheckElementBracketPlacement(element, allowAllOnOneLine);
                }
                else if (element.ElementType == ElementType.Property)
                {
                    this.CheckElementBracketPlacement(element, IsAutomaticProperty((Property) element));
                }
            }
        }

        private bool CheckExpressionCurlyBracketPlacement(Expression expression, Expression parentExpression, Statement parentStatement, CsElement parentElement, object context)
        {
            switch (expression.ExpressionType)
            {
                case ExpressionType.CollectionInitializer:
                case ExpressionType.ObjectInitializer:
                case ExpressionType.ArrayInitializer:
                {
                    CsTokenList tokens = expression.Tokens;
                    Expression parent = expression.Parent as Expression;
                    if (parent != null)
                    {
                        tokens = parent.Tokens;
                    }
                    this.CheckBracketPlacement(parentElement, parentStatement, tokens, GetOpenBracket(tokens), true);
                    break;
                }
                case ExpressionType.Lambda:
                case ExpressionType.AnonymousMethod:
                {
                    StyleCop.Node<CsToken><CsToken> openBracket = GetOpenBracket(expression.Tokens);
                    if (openBracket != null)
                    {
                        bool allowAllOnOneLine = expression.Location.StartPoint.LineNumber == expression.Location.EndPoint.LineNumber;
                        this.CheckBracketPlacement(parentElement, parentStatement, expression.Tokens, openBracket, allowAllOnOneLine);
                    }
                    break;
                }
            }
            return true;
        }

        private void CheckLineSpacing(CsDocument document)
        {
            int count = 0;
            StyleCop.Node<CsToken><CsToken> precedingTokenNode = null;
            bool fileHeader = true;
            bool firstTokenOnLine = true;
            bool flag3 = false;
            bool flag4 = true;
            bool flag5 = false;
            for (StyleCop.Node<CsToken><CsToken> node2 = document.Tokens.First; node2 != null; node2 = node2.Next)
            {
                if (base.Cancel)
                {
                    return;
                }
                CsToken part = node2.Value;
                if (((fileHeader && (part.CsTokenType != CsTokenType.EndOfLine)) && ((part.CsTokenType != CsTokenType.WhiteSpace) && (part.CsTokenType != CsTokenType.SingleLineComment))) && (part.CsTokenType != CsTokenType.MultiLineComment))
                {
                    fileHeader = false;
                }
                if (((node2 == document.Tokens.Last) && ((part.CsTokenType == CsTokenType.EndOfLine) || (part.CsTokenType == CsTokenType.WhiteSpace))) && (precedingTokenNode != null))
                {
                    int num2 = (precedingTokenNode.Value is ITokenContainer) ? ((ITokenContainer) precedingTokenNode.Value).Tokens.Last<CsToken>().LineNumber : precedingTokenNode.Value.LineNumber;
                    if (num2 != part.LineNumber)
                    {
                        base.AddViolation(part.FindParentElement(), part.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CodeMustNotContainBlankLinesAtEndOfFile, new object[0]);
                    }
                }
                if (part.Text == "\n")
                {
                    count++;
                    if ((!flag3 && flag4) && !flag5)
                    {
                        base.AddViolation(part.FindParentElement(), part.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CodeMustNotContainBlankLinesAtStartOfFile, new object[0]);
                        flag5 = true;
                    }
                    firstTokenOnLine = true;
                    flag3 = false;
                    this.CheckLineSpacingNewline(precedingTokenNode, node2, count);
                }
                else if (part.CsTokenType != CsTokenType.WhiteSpace)
                {
                    flag3 = true;
                    flag4 = false;
                    this.CheckLineSpacingNonWhitespace(document, precedingTokenNode, part, fileHeader, firstTokenOnLine, count);
                    count = 0;
                    precedingTokenNode = node2;
                    if ((firstTokenOnLine && (part.CsTokenType != CsTokenType.SingleLineComment)) && (part.CsTokenType != CsTokenType.MultiLineComment))
                    {
                        firstTokenOnLine = false;
                    }
                }
            }
        }

        private void CheckLineSpacingNewline(StyleCop.Node<CsToken><CsToken> precedingTokenNode, StyleCop.Node<CsToken><CsToken> node, int count)
        {
            if (((count == 2) && (precedingTokenNode != null)) && !precedingTokenNode.Value.Generated)
            {
                if (precedingTokenNode.Value.CsTokenType == CsTokenType.OpenCurlyBracket)
                {
                    base.AddViolation(precedingTokenNode.Value.FindParentElement(), precedingTokenNode.Value.LineNumber, (System.Enum) StyleCop.CSharp.Rules.OpeningCurlyBracketsMustNotBeFollowedByBlankLine, new object[0]);
                }
                else if (precedingTokenNode.Value.CsTokenType == CsTokenType.XmlHeader)
                {
                    base.AddViolation(precedingTokenNode.Value.FindParentElement(), precedingTokenNode.Value.LineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementDocumentationHeadersMustNotBeFollowedByBlankLine, new object[0]);
                }
            }
            StyleCop.Node<CsToken><CsToken> previous = node.Previous;
            StyleCop.Node<CsToken><CsToken> node3 = null;
            if (previous != null)
            {
                node3 = previous.Previous;
            }
            if ((count > 2) || (((count == 2) && !node.Value.Generated) && ((((previous == null) || (node3 == null)) || ((previous.Value.CsTokenType == CsTokenType.EndOfLine) && (node3.Value.CsTokenType == CsTokenType.EndOfLine))) || (node.Next == null))))
            {
                base.AddViolation(node.Value.FindParentElement(), node.Value.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CodeMustNotContainMultipleBlankLinesInARow, new object[0]);
            }
        }

        private void CheckLineSpacingNonWhitespace(CsDocument document, StyleCop.Node<CsToken><CsToken> precedingTokenNode, CsToken token, bool fileHeader, bool firstTokenOnLine, int count)
        {
            if (token.Generated)
            {
                return;
            }
            if (count <= 1)
            {
                if (count == 1)
                {
                    if (token.CsTokenType == CsTokenType.XmlHeader)
                    {
                        if (((precedingTokenNode != null) && (precedingTokenNode.Value.CsTokenType != CsTokenType.XmlHeader)) && ((precedingTokenNode.Value.CsTokenType != CsTokenType.OpenCurlyBracket) && (precedingTokenNode.Value.CsTokenType != CsTokenType.PreprocessorDirective)))
                        {
                            base.AddViolation(token.FindParentElement(), token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementDocumentationHeaderMustBePrecededByBlankLine, new object[0]);
                            return;
                        }
                    }
                    else if (token.CsTokenType == CsTokenType.SingleLineComment)
                    {
                        if ((((precedingTokenNode != null) && (precedingTokenNode.Value.CsTokenType != CsTokenType.SingleLineComment)) && ((precedingTokenNode.Value.CsTokenType != CsTokenType.OpenCurlyBracket) && (precedingTokenNode.Value.CsTokenType != CsTokenType.LabelColon))) && (((precedingTokenNode.Value.CsTokenType != CsTokenType.PreprocessorDirective) && !token.Text.Trim().StartsWith("////", StringComparison.Ordinal)) && !StyleCop.CSharp.Utils.IsAReSharperComment(token)))
                        {
                            CsElement element = token.FindParentElement();
                            if (element != null)
                            {
                                base.AddViolation(element, token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.SingleLineCommentMustBePrecededByBlankLine, new object[0]);
                                return;
                            }
                        }
                    }
                    else if ((precedingTokenNode != null) && (precedingTokenNode.Value.CsTokenType == CsTokenType.CloseCurlyBracket))
                    {
                        Bracket part = precedingTokenNode.Value as Bracket;
                        if (((((part.MatchingBracket != null) && (part.MatchingBracket.LineNumber != part.LineNumber)) && (firstTokenOnLine && (token.CsTokenType != CsTokenType.CloseCurlyBracket))) && (((token.CsTokenType != CsTokenType.Finally) && (token.CsTokenType != CsTokenType.Catch)) && ((token.CsTokenType != CsTokenType.WhileDo) && (token.CsTokenType != CsTokenType.Else)))) && ((((token.CsTokenType != CsTokenType.PreprocessorDirective) && (token.CsTokenType != CsTokenType.Select)) && ((token.CsTokenType != CsTokenType.From) && (token.CsTokenType != CsTokenType.Let))) && ((token.CsTokenType != CsTokenType.OperatorSymbol) && (token.CsTokenType != CsTokenType.By))))
                        {
                            base.AddViolation(part.FindParentElement(), part.LineNumber, (System.Enum) StyleCop.CSharp.Rules.ClosingCurlyBracketMustBeFollowedByBlankLine, new object[0]);
                        }
                    }
                }
                return;
            }
            if (token.CsTokenType == CsTokenType.CloseCurlyBracket)
            {
                base.AddViolation(token.FindParentElement(), token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.ClosingCurlyBracketsMustNotBePrecededByBlankLine, new object[0]);
                goto Label_0167;
            }
            if (token.CsTokenType != CsTokenType.OpenCurlyBracket)
            {
                if (((token.CsTokenType == CsTokenType.Else) || (token.CsTokenType == CsTokenType.Catch)) || (token.CsTokenType == CsTokenType.Finally))
                {
                    base.AddViolation(token.FindParentElement(), token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.ChainedStatementBlocksMustNotBePrecededByBlankLine, new object[0]);
                }
                else if (token.CsTokenType == CsTokenType.WhileDo)
                {
                    base.AddViolation(token.FindParentElement(), token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.WhileDoFooterMustNotBePrecededByBlankLine, new object[0]);
                }
                goto Label_0167;
            }
            bool flag = false;
            if (precedingTokenNode == null)
            {
                flag = true;
            }
            else
            {
                Statement statement = precedingTokenNode.Value.FindParentStatement();
                if (statement == null)
                {
                    flag = true;
                }
                else
                {
                    StatementType statementType = statement.StatementType;
                    switch (statementType)
                    {
                        case StatementType.If:
                        case StatementType.While:
                        case StatementType.Catch:
                        case StatementType.Try:
                        case StatementType.Finally:
                        case StatementType.DoWhile:
                        case StatementType.Else:
                        case StatementType.Lock:
                        case StatementType.Switch:
                        case StatementType.Unsafe:
                        case StatementType.Using:
                            flag = true;
                            goto Label_00D8;
                    }
                    if ((statementType == StatementType.VariableDeclaration) && (precedingTokenNode.Value.CsTokenType != CsTokenType.Semicolon))
                    {
                        flag = true;
                    }
                    else if ((statementType == StatementType.Expression) && (precedingTokenNode.Value.CsTokenType == CsTokenType.Delegate))
                    {
                        flag = true;
                    }
                }
            }
        Label_00D8:
            if (flag)
            {
                base.AddViolation(token.FindParentElement(), token.Location, (System.Enum) StyleCop.CSharp.Rules.OpeningCurlyBracketsMustNotBePrecededByBlankLine, new object[0]);
            }
        Label_0167:
            if (((fileHeader || (precedingTokenNode == null)) || ((precedingTokenNode.Value.CsTokenType != CsTokenType.SingleLineComment) || (token.CsTokenType == CsTokenType.SingleLineComment))) || ((token.CsTokenType == CsTokenType.MultiLineComment) || (token.CsTokenType == CsTokenType.XmlHeader)))
            {
                return;
            }
            bool flag2 = false;
            if (precedingTokenNode != null)
            {
                foreach (CsToken token2 in document.Tokens.ReverseIterator(precedingTokenNode.Previous))
                {
                    if (token2.CsTokenType == CsTokenType.EndOfLine)
                    {
                        break;
                    }
                    if (token2.CsTokenType != CsTokenType.WhiteSpace)
                    {
                        flag2 = true;
                        break;
                    }
                }
            }
            string str = precedingTokenNode.Value.Text.Trim();
            if ((!flag2 && !str.StartsWith("////", StringComparison.Ordinal)) && !IsCommentInFileHeader(precedingTokenNode))
            {
                base.AddViolation(precedingTokenNode.Value.FindParentElement(), precedingTokenNode.Value.LineNumber, (System.Enum) StyleCop.CSharp.Rules.SingleLineCommentsMustNotBeFollowedByBlankLine, new object[0]);
            }
        }

        private void CheckMissingBlock(CsElement parentElement, Statement statement, Statement embeddedStatement, string statementType, bool allowStacks)
        {
            if (((embeddedStatement != null) && (embeddedStatement.StatementType != StatementType.Block)) && ((!allowStacks || (statement.ChildStatements == null)) || ((statement.ChildStatements.Count == 0) || (GetFirstChildStatement(statement).StatementType != statement.StatementType))))
            {
                base.AddViolation(parentElement, embeddedStatement.LineNumber, (System.Enum) StyleCop.CSharp.Rules.CurlyBracketsMustNotBeOmitted, new object[] { statementType });
            }
        }

        private void CheckSiblingAccessors(CsElement accessor, StyleCop.Node<CsToken><CsToken> openingBracketNode)
        {
            Bracket bracket = (Bracket) openingBracketNode.Value;
            if ((bracket.MatchingBracket != null) && (bracket.LineNumber == bracket.MatchingBracket.LineNumber))
            {
                CsElement element = accessor.FindParentElement();
                if (element != null)
                {
                    foreach (CsElement element2 in element.ChildElements)
                    {
                        if (element2 != accessor)
                        {
                            StyleCop.Node<CsToken><CsToken> openBracket = GetOpenBracket(element2.Tokens);
                            if (openBracket != null)
                            {
                                bracket = (Bracket) openBracket.Value;
                                if (((bracket != null) && (bracket.MatchingBracket != null)) && (bracket.LineNumber != bracket.MatchingBracket.LineNumber))
                                {
                                    base.AddViolation(accessor, accessor.LineNumber, (System.Enum) StyleCop.CSharp.Rules.AllAccessorsMustBeMultiLineOrSingleLine, new object[] { element.FriendlyTypeText });
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool CheckStatementCurlyBracketPlacement(Statement statement, Expression parentExpression, Statement parentStatement, CsElement parentElement, object context)
        {
            switch (statement.StatementType)
            {
                case StatementType.DoWhile:
                    this.CheckMissingBlock(parentElement, statement, ((DoWhileStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;

                case StatementType.Else:
                    this.CheckMissingBlock(parentElement, statement, ((ElseStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;

                case StatementType.Block:
                {
                    bool allowAllOnOneLine = false;
                    LambdaExpression parent = statement.Parent as LambdaExpression;
                    if ((parent != null) && (parent.Location.StartPoint.LineNumber == parent.Location.EndPoint.LineNumber))
                    {
                        allowAllOnOneLine = true;
                    }
                    this.CheckBracketPlacement(parentElement, statement, statement.Tokens, GetOpenBracket(statement.Tokens), allowAllOnOneLine);
                    break;
                }
                case StatementType.Foreach:
                    this.CheckMissingBlock(parentElement, statement, ((ForeachStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;

                case StatementType.For:
                    this.CheckMissingBlock(parentElement, statement, ((ForStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;

                case StatementType.If:
                    this.CheckMissingBlock(parentElement, statement, ((IfStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;

                case StatementType.Lock:
                    this.CheckMissingBlock(parentElement, statement, ((LockStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;

                case StatementType.Switch:
                    this.CheckBracketPlacement(parentElement, statement, statement.Tokens, GetOpenBracket(statement.Tokens), false);
                    break;

                case StatementType.Using:
                    this.CheckMissingBlock(parentElement, statement, ((UsingStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, true);
                    break;

                case StatementType.While:
                    this.CheckMissingBlock(parentElement, statement, ((WhileStatement) statement).EmbeddedStatement, statement.FriendlyTypeText, false);
                    break;
            }
            return true;
        }

        public override bool DoAnalysis(CodeDocument document)
        {
            Param.RequireNotNull(document, "document");
            CsDocument document2 = (CsDocument) document;
            if (document2.FileHeader != null)
            {
                return !document2.FileHeader.UnStyled;
            }
            return true;
        }

        private static bool DoesAccessorHaveBody(Accessor accessor)
        {
            for (StyleCop.Node<CsToken><CsToken> node = accessor.Tokens.First; node != accessor.Tokens.Last; node = node.Next)
            {
                if (node.Value.CsTokenType == CsTokenType.OpenCurlyBracket)
                {
                    return true;
                }
            }
            return false;
        }

        private static Statement GetFirstChildStatement(Statement statement)
        {
            if ((statement.ChildStatements != null) && (statement.ChildStatements.Count > 0))
            {
                using (IEnumerator<Statement> enumerator = statement.ChildStatements.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        return enumerator.Current;
                    }
                }
            }
            return null;
        }

        private static StyleCop.Node<CsToken><CsToken> GetOpenBracket(CsTokenList tokens)
        {
            for (StyleCop.Node<CsToken><CsToken> node = tokens.First; !tokens.OutOfBounds(node); node = node.Next)
            {
                if (node.Value.CsTokenType == CsTokenType.OpenCurlyBracket)
                {
                    return node;
                }
            }
            return null;
        }

        private static string GetOpeningOrClosingBracketText(Bracket bracket)
        {
            switch (bracket.CsTokenType)
            {
                case CsTokenType.OpenParenthesis:
                case CsTokenType.OpenCurlyBracket:
                case CsTokenType.OpenSquareBracket:
                case CsTokenType.OpenGenericBracket:
                case CsTokenType.OpenAttributeBracket:
                    return StyleCop.CSharp.Strings.Opening;

                case CsTokenType.CloseParenthesis:
                case CsTokenType.CloseCurlyBracket:
                case CsTokenType.CloseSquareBracket:
                case CsTokenType.CloseGenericBracket:
                case CsTokenType.CloseAttributeBracket:
                    return StyleCop.CSharp.Strings.Closing;
            }
            return string.Empty;
        }

        private static bool IsAutomaticProperty(Property property)
        {
            if (property.GetAccessor != null)
            {
                return !DoesAccessorHaveBody(property.GetAccessor);
            }
            return ((property.SetAccessor != null) && !DoesAccessorHaveBody(property.SetAccessor));
        }

        private static bool IsCommentInFileHeader(StyleCop.Node<CsToken><CsToken> comment)
        {
            for (StyleCop.Node<CsToken><CsToken> node = comment; node != null; node = node.Previous)
            {
                if (((node.Value.CsTokenType != CsTokenType.SingleLineComment) && (node.Value.CsTokenType != CsTokenType.WhiteSpace)) && (node.Value.CsTokenType != CsTokenType.EndOfLine))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsTokenADot(CsToken token)
        {
            if (token.CsTokenType == CsTokenType.OperatorSymbol)
            {
                OperatorSymbol symbol = (OperatorSymbol) token;
                if (symbol.SymbolType == OperatorType.MemberAccess)
                {
                    return (symbol.Text == ".");
                }
            }
            return false;
        }

        private bool VisitElement(CsElement element, CsElement parentElement, object context)
        {
            this.CheckElementCurlyBracketPlacement(element);
            this.CheckChildElementSpacing(element);
            return true;
        }
    }
}

