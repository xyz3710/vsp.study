namespace StyleCop.CSharp
{
    using StyleCop;
    using StyleCop.Spelling;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.XPath;

    [SourceAnalyzer(typeof(CsParser))]
    public class DocumentationRules : SourceAnalyzer
    {
        internal const string CompanyNameProperty = "CompanyName";
        private static readonly int[] CopyrightCharTable = new int[] { 0xa9, 0xfffd };
        internal const string CopyrightProperty = "Copyright";
        private const string CrefGenericParamsRegex = @"((\s*(<|&lt;)\s*{0}\s*(>|&gt;))|(\s*{{\s*{0}\s*}}))";
        private const string CrefRegex = "(?'see'<see\\s+cref\\s*=\\s*\")?(?(see)({2}|(T:{0}))|({1}))(?(see)(\"\\s*(/>|>[\\w\\s]*</see>)))";
        internal const string IgnoreInternals = "IgnoreInternals";
        internal const bool IgnoreInternalsDefaultValue = false;
        internal const string IgnorePrivates = "IgnorePrivates";
        internal const bool IgnorePrivatesDefaultValue = false;
        private Dictionary<string, CachedXmlDocument> includedDocs;
        internal const bool IncludeFieldsDefaultValue = true;
        internal const string IncludeFieldsProperty = "IncludeFields";

        public override void AnalyzeDocument(CodeDocument document)
        {
            Param.RequireNotNull(document, "document");
            CsDocument document2 = (CsDocument) document;
            if ((document2.RootElement != null) && !document2.RootElement.Generated)
            {
                NamingService namingService = NamingService.GetNamingService(document.SourceCode.Project.Culture);
                namingService.AddDeprecatedWords(document.SourceCode.Project.DeprecatedWords);
                foreach (string str in document.SourceCode.Project.DictionaryFolders)
                {
                    if (str.StartsWith(".", StringComparison.Ordinal))
                    {
                        namingService.AddDictionaryFolder(StyleCop.Utils.MakeAbsolutePath(Path.GetDirectoryName(document.SourceCode.Path), str));
                        string location = document.SourceCode.Settings.Location;
                        if (location != null)
                        {
                            namingService.AddDictionaryFolder(StyleCop.Utils.MakeAbsolutePath(Path.GetDirectoryName(location), str));
                        }
                        namingService.AddDictionaryFolder(StyleCop.Utils.MakeAbsolutePath(Path.GetDirectoryName(document.SourceCode.Project.Location), str));
                        continue;
                    }
                    namingService.AddDictionaryFolder(str);
                }
                namingService.AddDictionaryFolder(Path.GetDirectoryName(document.SourceCode.Path));
                namingService.AddDictionaryFolder(Path.GetDirectoryName(document.SourceCode.Project.Location));
                this.CheckElementDocumentation(document2);
                this.CheckFileHeader(document2);
                this.CheckSingleLineComments(document2.RootElement);
            }
        }

        private static string BuildCrefValidationStringForType(ClassBase type)
        {
            string str;
            string str2;
            StringBuilder builder = new StringBuilder();
            string[] strArray = type.FullyQualifiedName.Split(new char[] { '.' });
            for (int i = 1; i < strArray.Length; i++)
            {
                builder.Append(BuildTypeNameStringWithParamsNumber(strArray[i]));
                if (i < (strArray.Length - 1))
                {
                    builder.Append(@"\.");
                }
            }
            BuildRegExForAllTypeOptions(type, out str, out str2);
            return string.Format(CultureInfo.InvariantCulture, "(?'see'<see\\s+cref\\s*=\\s*\")?(?(see)({2}|(T:{0}))|({1}))(?(see)(\"\\s*(/>|>[\\w\\s]*</see>)))", new object[] { builder, str2, str });
        }

        private static string BuildGenericParametersRegex(string[] genericParams)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < genericParams.Length; i++)
            {
                builder.Append(genericParams[i]);
                if (i < (genericParams.Length - 1))
                {
                    builder.Append(@"\s*,\s*");
                }
            }
            return string.Format(CultureInfo.InvariantCulture, @"((\s*(<|&lt;)\s*{0}\s*(>|&gt;))|(\s*{{\s*{0}\s*}}))", new object[] { builder });
        }

        private static void BuildRegExForAllTypeOptions(ClassBase type, out string regexWithGenerics, out string regexWithoutGenerics)
        {
            string[] strArray = type.FullyQualifiedName.Split(new char[] { '.' });
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            for (int i = 0; i < strArray.Length; i++)
            {
                builder2.Append(BuildTypeNameStringWithGenerics(strArray[i]));
                builder.Append(RemoveGenericsFromTypeName(strArray[i]));
                if (i < (strArray.Length - 1))
                {
                    builder2.Append(".");
                    builder.Append(".");
                }
            }
            regexWithGenerics = BuildRegExStringFromTypeName(builder2.ToString());
            regexWithoutGenerics = BuildRegExStringFromTypeName(builder.ToString());
        }

        private static string BuildRegExStringFromTypeName(string qualifiedTypeName)
        {
            string[] strArray = qualifiedTypeName.Split(new char[] { '.' });
            StringBuilder builder = new StringBuilder();
            for (int i = 1; i < strArray.Length; i++)
            {
                builder.Append("(");
                for (int j = i; j < strArray.Length; j++)
                {
                    builder.Append(strArray[j]);
                    if (j < (strArray.Length - 1))
                    {
                        builder.Append(@"\.");
                    }
                }
                builder.Append(")");
                if (i < (strArray.Length - 1))
                {
                    builder.Append("|");
                }
            }
            return ("(" + builder + ")");
        }

        private static string BuildTypeNameStringWithGenerics(string typeName)
        {
            int index = typeName.IndexOf('<');
            if (index < 0)
            {
                return typeName;
            }
            return (typeName.Substring(0, index) + BuildGenericParametersRegex(ExtractGenericParametersFromType(typeName, index)));
        }

        private static string BuildTypeNameStringWithParamsNumber(string typeName)
        {
            int index = typeName.IndexOf('<');
            if (index < 0)
            {
                return typeName;
            }
            string[] strArray = ExtractGenericParametersFromType(typeName, index);
            return (typeName.Substring(0, index) + "`" + strArray.Length);
        }

        private static bool CharacterIsCopyright(char character)
        {
            for (int i = 0; i < CopyrightCharTable.Length; i++)
            {
                if (character == CopyrightCharTable[i])
                {
                    return true;
                }
            }
            return false;
        }

        private void CheckClassElementHeader(ClassBase classElement, AnalyzerSettings settings)
        {
            AnalyzerSettings settings2 = settings;
            settings2.RequireFields = false;
            this.CheckHeader(classElement, settings2, classElement.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Partial }));
        }

        private void CheckConstructorSummaryText(Constructor constructor, XmlDocument formattedDocs)
        {
            XmlNode node = formattedDocs.SelectSingleNode("root/summary");
            if (node != null)
            {
                string input = node.InnerXml.Trim();
                string type = (constructor.Parent is Struct) ? CachedCodeStrings.StructText : CachedCodeStrings.ClassText;
                string typeRegex = BuildCrefValidationStringForType((ClassBase) constructor.FindParentElement());
                string pattern = GetExpectedSummaryTextForConstructorType(constructor, type, typeRegex);
                if (!Regex.IsMatch(input, pattern, RegexOptions.ExplicitCapture))
                {
                    base.AddViolation(constructor, StyleCop.CSharp.Rules.ConstructorSummaryDocumentationMustBeginWithStandardText, new object[] { GetExampleSummaryTextForConstructorType(constructor, type) });
                }
            }
        }

        private void CheckDestructorSummaryText(Destructor destructor, XmlDocument formattedDocs)
        {
            XmlNode node = formattedDocs.SelectSingleNode("root/summary");
            if (node != null)
            {
                string input = node.InnerXml.Trim();
                string expectedSummaryTextForDestructor = GetExpectedSummaryTextForDestructor(BuildCrefValidationStringForType((ClassBase) destructor.FindParentElement()));
                if (!Regex.IsMatch(input, expectedSummaryTextForDestructor))
                {
                    base.AddViolation(destructor, StyleCop.CSharp.Rules.DestructorSummaryDocumentationMustBeginWithStandardText, new object[] { GetExampleSummaryTextForDestructor(destructor) });
                }
            }
        }

        private bool CheckDocumentationForElement(CsElement element, CsElement parentElement, AnalyzerSettings settings)
        {
            if (base.Cancel)
            {
                return false;
            }
            if (!element.Generated)
            {
                if (((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Interface)) || (element.ElementType == ElementType.Struct))
                {
                    ClassBase classElement = element as ClassBase;
                    this.CheckClassElementHeader(classElement, settings);
                }
                else if ((((element.ElementType == ElementType.Enum) || (element.ElementType == ElementType.Delegate)) || ((element.ElementType == ElementType.Event) || (element.ElementType == ElementType.Property))) || (((element.ElementType == ElementType.Indexer) || (element.ElementType == ElementType.Constructor)) || ((element.ElementType == ElementType.Destructor) || (element.ElementType == ElementType.Field))))
                {
                    this.CheckHeader(element, settings, false);
                }
                else if (element.ElementType == ElementType.Method)
                {
                    this.CheckHeader(element, settings, element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Partial }));
                }
                if (element.ElementType == ElementType.Enum)
                {
                    this.CheckEnumHeaders(element as StyleCop.CSharp.Enum, settings);
                }
                if (((element.ElementType == ElementType.Accessor) || (element.ElementType == ElementType.Constructor)) || ((element.ElementType == ElementType.Destructor) || (element.ElementType == ElementType.Method)))
                {
                    this.CheckElementComments(element);
                }
            }
            return true;
        }

        private void CheckDocumentationValidity(CsElement element, int lineNumber, XmlNode documentationXml, string documentationType)
        {
            string str;
            InvalidCommentType type = CommentVerifier.IsGarbageComment(documentationXml, element, out str);
            if (((type & InvalidCommentType.Empty) != InvalidCommentType.Valid) && !documentationXml.InnerXml.StartsWith("<", StringComparison.Ordinal))
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationTextMustNotBeEmpty, new object[] { documentationType });
            }
            if (((type & InvalidCommentType.NoPeriod) != InvalidCommentType.Valid) && !documentationXml.InnerXml.EndsWith(">", StringComparison.Ordinal))
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationTextMustEndWithAPeriod, new object[] { documentationType });
            }
            if ((((type & InvalidCommentType.NoCapitalLetter) != InvalidCommentType.Valid) && !documentationXml.InnerXml.StartsWith("<", StringComparison.Ordinal)) && (!documentationType.Equals("return", StringComparison.Ordinal) || (!documentationXml.InnerText.StartsWith("true", StringComparison.Ordinal) && !documentationXml.InnerText.StartsWith("false", StringComparison.Ordinal))))
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationTextMustBeginWithACapitalLetter, new object[] { documentationType });
            }
            if (((type & InvalidCommentType.NoWhitespace) != InvalidCommentType.Valid) && !documentationXml.InnerXml.StartsWith("<", StringComparison.Ordinal))
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationTextMustContainWhitespace, new object[] { documentationType });
            }
            if (((type & InvalidCommentType.TooFewCharacters) != InvalidCommentType.Valid) && !documentationXml.InnerXml.StartsWith("<", StringComparison.Ordinal))
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationMustMeetCharacterPercentage, new object[] { documentationType, 40, 60 });
            }
            if (((type & InvalidCommentType.TooShort) != InvalidCommentType.Valid) && !documentationXml.InnerXml.StartsWith("<", StringComparison.Ordinal))
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationTextMustMeetMinimumCharacterLength, new object[] { documentationType, 10 });
            }
            if ((type & InvalidCommentType.IncorrectSpelling) != InvalidCommentType.Valid)
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementDocumentationMustBeSpelledCorrectly, new object[] { documentationType, str });
            }
        }

        private void CheckElementComments(CsElement element)
        {
            foreach (CsToken token in element.Tokens)
            {
                if ((token.CsTokenType == CsTokenType.XmlHeader) || (token.CsTokenType == CsTokenType.XmlHeaderLine))
                {
                    base.AddViolation(element, token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.SingleLineCommentsMustNotUseDocumentationStyleSlashes, new object[0]);
                }
            }
        }

        private void CheckElementDocumentation(CsDocument document)
        {
            AnalyzerSettings context = new AnalyzerSettings();
            context.IgnorePrivates = false;
            context.IgnoreInternals = false;
            context.RequireFields = true;
            if (document.Settings != null)
            {
                BooleanProperty addInSetting = document.Settings.GetAddInSetting(this, "IgnorePrivates") as BooleanProperty;
                if (addInSetting != null)
                {
                    context.IgnorePrivates = addInSetting.Value;
                }
                addInSetting = document.Settings.GetAddInSetting(this, "IgnoreInternals") as BooleanProperty;
                if (addInSetting != null)
                {
                    context.IgnoreInternals = addInSetting.Value;
                }
                addInSetting = document.Settings.GetAddInSetting(this, "IncludeFields") as BooleanProperty;
                if (addInSetting != null)
                {
                    context.RequireFields = addInSetting.Value;
                }
            }
            document.WalkDocument<AnalyzerSettings>(new CodeWalkerElementVisitor<AnalyzerSettings>(this.CheckDocumentationForElement), context);
        }

        private void CheckEnumHeaders(StyleCop.CSharp.Enum element, AnalyzerSettings settings)
        {
            foreach (EnumItem item in element.Items)
            {
                if ((item.Header == null) || (item.Header.Text.Length == 0))
                {
                    if ((!settings.IgnorePrivates || (element.Declaration.AccessModifierType != AccessModifierType.Private)) && (!settings.IgnoreInternals || (element.Declaration.AccessModifierType != AccessModifierType.Internal)))
                    {
                        base.AddViolation(item, StyleCop.CSharp.Rules.EnumerationItemsMustBeDocumented, new object[0]);
                    }
                    continue;
                }
                this.ParseHeader(item, item.Header, item.LineNumber, false);
            }
        }

        private void CheckFileHeader(CsDocument document)
        {
            string companyName = null;
            string str2 = null;
            StringProperty setting = base.GetSetting(document.Settings, "CompanyName") as StringProperty;
            if (setting != null)
            {
                companyName = setting.Value;
            }
            StringProperty property2 = base.GetSetting(document.Settings, "Copyright") as StringProperty;
            if (property2 != null)
            {
                str2 = property2.Value;
                FileInfo file = new FileInfo(document.SourceCode.Path);
                str2 = StyleCop.Utils.ReplaceTokenVariables(str2, file);
            }
            this.CheckFileHeader(document, str2, companyName);
        }

        private void CheckFileHeader(CsDocument document, string copyright, string companyName)
        {
            if (((document.FileHeader == null) || (document.FileHeader.HeaderXml == null)) || (document.FileHeader.HeaderXml.Length == 0))
            {
                base.AddViolation(document.RootElement, 1, (System.Enum) StyleCop.CSharp.Rules.FileMustHaveHeader, new object[0]);
            }
            else
            {
                try
                {
                    XmlDocument document2 = new XmlDocument();
                    document2.LoadXml(document.FileHeader.HeaderXml);
                    XmlNode node = document2.DocumentElement["copyright"];
                    if (node == null)
                    {
                        base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderMustShowCopyright, new object[0]);
                    }
                    else
                    {
                        string str = node.InnerText.Trim();
                        if (str.Length == 0)
                        {
                            base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderMustHaveCopyrightText, new object[0]);
                        }
                        else if (!string.IsNullOrEmpty(copyright) && !MatchCopyrightText(copyright, str))
                        {
                            base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderCopyrightTextMustMatch, new object[] { copyright });
                        }
                        XmlNode node2 = node.Attributes["file"];
                        if (node2 == null)
                        {
                            base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderMustContainFileName, new object[0]);
                        }
                        else
                        {
                            string str2;
                            ElementType type;
                            if (string.Compare(node2.InnerText, document.SourceCode.Name, StringComparison.OrdinalIgnoreCase) != 0)
                            {
                                base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderFileNameDocumentationMustMatchFileName, new object[0]);
                            }
                            string strB = string.Empty;
                            string s = string.Empty;
                            if (!this.GetFirstTypeName(document.RootElement, out str2, out type) && (str2 != null))
                            {
                                string strA = this.RemoveExtensions(node2.InnerText);
                                if (str2.IndexOf('<') > -1)
                                {
                                    s = str2.Replace("out ", string.Empty).Replace("in ", string.Empty).Replace('<', '{').Replace('>', '}');
                                    if (s.IndexOf('%') > -1)
                                    {
                                        s = s.SubstringBefore('%');
                                    }
                                    strB = s.SubstringBefore('{');
                                }
                                else
                                {
                                    strB = str2.SubstringBefore('%');
                                }
                                if (((string.Compare(strA, str2, StringComparison.OrdinalIgnoreCase) != 0) && (string.Compare(strA, strB, StringComparison.OrdinalIgnoreCase) != 0)) && (string.Compare(strA, s, StringComparison.OrdinalIgnoreCase) != 0))
                                {
                                    string str6;
                                    if (type == ElementType.Delegate)
                                    {
                                        str6 = "\"" + this.GetShortestItem(new string[] { str2, strB, s }) + "\"";
                                    }
                                    else
                                    {
                                        str6 = "\"" + str2 + "\"";
                                        if (!string.IsNullOrEmpty(strB) && !strB.Equals(str2, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            str6 = str6 + ", \"" + strB + "\"";
                                        }
                                        if ((!string.IsNullOrEmpty(s) && !s.Equals(str2, StringComparison.InvariantCultureIgnoreCase)) && !s.Equals(strB, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            str6 = str6 + ", \"" + s + "\"";
                                        }
                                    }
                                    if (document.SourceCode.Name.ToLowerInvariant() != "global.asax.cs")
                                    {
                                        base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderFileNameDocumentationMustMatchTypeName, new object[] { str6 });
                                    }
                                }
                            }
                        }
                        node2 = node.Attributes["company"];
                        if (node2 == null)
                        {
                            base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderMustHaveValidCompanyText, new object[0]);
                        }
                        else
                        {
                            str = node2.Value.Trim();
                            if (str.Length == 0)
                            {
                                base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderMustHaveValidCompanyText, new object[0]);
                            }
                            else if (!string.IsNullOrEmpty(companyName) && (string.CompareOrdinal(companyName, str) != 0))
                            {
                                base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderCompanyNameTextMustMatch, new object[] { companyName });
                            }
                        }
                    }
                    node = document2.DocumentElement["summary"];
                    if ((node == null) || (node.InnerText.Length == 0))
                    {
                        base.AddViolation(document.RootElement, document.FileHeader.LineNumber, (System.Enum) StyleCop.CSharp.Rules.FileHeaderMustHaveSummary, new object[0]);
                    }
                }
                catch (XmlException)
                {
                    base.AddViolation(document.RootElement, 1, (System.Enum) StyleCop.CSharp.Rules.FileMustHaveHeader, new object[0]);
                }
                catch (ArgumentException)
                {
                    base.AddViolation(document.RootElement, 1, (System.Enum) StyleCop.CSharp.Rules.FileMustHaveHeader, new object[0]);
                }
            }
        }

        private void CheckForBlankLinesInDocumentationHeader(CsElement element, XmlHeader header)
        {
            if (!element.Generated)
            {
                int num = 0;
                int num2 = 0;
                for (StyleCop.Node<CsToken><CsToken> node = header.ChildTokens.First; (node != null) && (node != header.ChildTokens.Last.Next); node = node.Next)
                {
                    CsToken token = node.Value;
                    if (token.CsTokenType == CsTokenType.EndOfLine)
                    {
                        num++;
                        if (num > 1)
                        {
                            base.AddViolation(element, token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationHeadersMustNotContainBlankLines, new object[0]);
                            return;
                        }
                    }
                    else if (token.CsTokenType == CsTokenType.XmlHeaderLine)
                    {
                        num2 += XmlHeaderLineCodeElementCount(token);
                        if ((node == header.ChildTokens.First) || (node == header.ChildTokens.Last))
                        {
                            if (IsXmlHeaderLineEmpty(token) && (num2 == 0))
                            {
                                base.AddViolation(element, token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationHeadersMustNotContainBlankLines, new object[0]);
                                return;
                            }
                        }
                        else if (!IsXmlHeaderLineEmpty(token) || (num2 > 0))
                        {
                            num = 0;
                        }
                    }
                }
            }
        }

        private void CheckForRepeatingComments(CsElement element, XmlDocument formattedDocs)
        {
            List<string> list = new List<string>();
            XmlNode node = formattedDocs.SelectSingleNode("root/summary");
            if (node != null)
            {
                list.Add(node.InnerXml.Trim());
            }
            node = formattedDocs.SelectSingleNode("root/returns");
            if (node != null)
            {
                list.Add(node.InnerXml.Trim());
            }
            node = formattedDocs.SelectSingleNode("root/remarks");
            if (node != null)
            {
                list.Add(node.InnerXml.Trim());
            }
            node = formattedDocs.SelectSingleNode("root/value");
            if (node != null)
            {
                list.Add(node.InnerXml.Trim());
            }
            XmlNodeList list2 = formattedDocs.SelectNodes("root/param");
            if ((list2 != null) && (list2.Count > 0))
            {
                foreach (XmlNode node2 in list2)
                {
                    list.Add(node2.InnerXml.Trim());
                }
            }
            list2 = formattedDocs.SelectNodes("root/typeparam");
            if ((list2 != null) && (list2.Count > 0))
            {
                foreach (XmlNode node3 in list2)
                {
                    list.Add(node3.InnerXml.Trim());
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Length > 0)
                {
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if ((string.Compare(list[i], list[j], StringComparison.Ordinal) == 0) && (string.Compare(list[i], CachedCodeStrings.ParameterNotUsed, StringComparison.Ordinal) != 0))
                        {
                            base.AddViolation(element, StyleCop.CSharp.Rules.ElementDocumentationMustNotBeCopiedAndPasted, new object[] { CachedCodeStrings.ParameterNotUsed });
                            return;
                        }
                    }
                }
            }
        }

        private void CheckGenericTypeParams(CsElement element, XmlDocument formattedDocs)
        {
            List<string> list = ExtractGenericTypeList(element.Declaration.Name);
            XmlNodeList list2 = formattedDocs.SelectNodes("root/typeparam");
            if ((list2 == null) || (list2.Count == 0))
            {
                if ((list != null) && (list.Count > 0))
                {
                    bool flag = element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Partial });
                    if ((!flag || (formattedDocs.SelectSingleNode("root/summary") != null)) || (formattedDocs.SelectSingleNode("root/content") == null))
                    {
                        if (flag)
                        {
                            base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParametersMustBeDocumentedPartialClass, new object[] { element.FriendlyTypeText });
                        }
                        else
                        {
                            base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParametersMustBeDocumented, new object[] { element.FriendlyTypeText });
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < list2.Count; i++)
                {
                    XmlNode node = list2[i];
                    if ((list == null) || (list.Count <= i))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParameterDocumentationMustMatchTypeParameters, new object[] { element.FriendlyTypeText });
                        break;
                    }
                    XmlNode namedItem = node.Attributes.GetNamedItem("name");
                    if ((namedItem == null) || (namedItem.Value.Length == 0))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParameterDocumentationMustDeclareParameterName, new object[0]);
                    }
                    else if (namedItem.Value != list[i])
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParameterDocumentationMustMatchTypeParameters, new object[] { element.FriendlyTypeText });
                        break;
                    }
                }
                if ((list == null) || (list.Count > list2.Count))
                {
                    base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParameterDocumentationMustMatchTypeParameters, new object[] { element.FriendlyTypeText });
                }
                foreach (XmlNode node3 in list2)
                {
                    if ((node3.InnerText == null) || (node3.InnerText.Length == 0))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.GenericTypeParameterDocumentationMustHaveText, new object[] { node3.OuterXml });
                        continue;
                    }
                    this.CheckDocumentationValidity(element, element.LineNumber, node3, "typeparam");
                }
            }
        }

        private void CheckHeader(CsElement element, AnalyzerSettings settings, bool partialElement)
        {
            if ((element.Header == null) || (element.Header.Text.Length == 0))
            {
                if (((settings.RequireFields || (element.ElementType != ElementType.Field)) && (!settings.IgnorePrivates || (element.ActualAccess != AccessModifierType.Private))) && (!settings.IgnoreInternals || ((element.ActualAccess != AccessModifierType.Internal) && (element.ActualAccess != AccessModifierType.ProtectedAndInternal))))
                {
                    if (partialElement)
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.PartialElementsMustBeDocumented, new object[] { element.FriendlyTypeText });
                    }
                    else if (!IsNonPublicStaticExternDllImport(element))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.ElementsMustBeDocumented, new object[] { element.FriendlyTypeText });
                    }
                }
            }
            else
            {
                this.ParseHeader(element, element.Header, element.LineNumber, partialElement);
            }
        }

        private void CheckHeaderElementsForEmptyText(CsElement element, XmlDocument formattedDocs)
        {
            XmlNode documentationXml = formattedDocs.SelectSingleNode("root/remarks");
            if (documentationXml != null)
            {
                this.CheckDocumentationValidity(element, element.LineNumber, documentationXml, "remarks");
            }
            documentationXml = formattedDocs.SelectSingleNode("root/example");
            if (documentationXml != null)
            {
                this.CheckDocumentationValidity(element, element.LineNumber, documentationXml, "example");
            }
            documentationXml = formattedDocs.SelectSingleNode("root/permission");
            if (documentationXml != null)
            {
                this.CheckDocumentationValidity(element, element.LineNumber, documentationXml, "permission");
            }
            documentationXml = formattedDocs.SelectSingleNode("root/exception");
            if (documentationXml != null)
            {
                this.CheckDocumentationValidity(element, element.LineNumber, documentationXml, "exception");
            }
        }

        private void CheckHeaderParams(CsElement element, ICollection<Parameter> parameters, XmlDocument formattedDocs)
        {
            XmlNodeList list = formattedDocs.SelectNodes("root/param");
            if ((list == null) || ((list.Count == 0) && (parameters.Count > 0)))
            {
                base.AddViolation(element, StyleCop.CSharp.Rules.ElementParametersMustBeDocumented, new object[0]);
            }
            else
            {
                int num = 0;
                foreach (Parameter parameter in parameters)
                {
                    if (list.Count <= num)
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.ElementParametersMustBeDocumented, new object[0]);
                        break;
                    }
                    XmlNode node = list[num];
                    XmlNode namedItem = node.Attributes.GetNamedItem("name");
                    if ((namedItem == null) || (namedItem.Value.Length == 0))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.ElementParameterDocumentationMustDeclareParameterName, new object[0]);
                    }
                    else
                    {
                        string name = parameter.Name;
                        if (name.StartsWith("@", StringComparison.Ordinal) && (name.Length > 1))
                        {
                            name = name.Substring(1, name.Length - 1);
                        }
                        if (namedItem.Value != name)
                        {
                            base.AddViolation(element, StyleCop.CSharp.Rules.ElementParameterDocumentationMustMatchElementParameters, new object[0]);
                        }
                    }
                    num++;
                }
                foreach (XmlNode node3 in list)
                {
                    if ((node3.InnerText == null) || (node3.InnerText.Length == 0))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.ElementParameterDocumentationMustHaveText, new object[] { node3.OuterXml });
                        continue;
                    }
                    this.CheckDocumentationValidity(element, element.LineNumber, node3, "param");
                }
                if (list.Count > num)
                {
                    base.AddViolation(element, StyleCop.CSharp.Rules.ElementParameterDocumentationMustMatchElementParameters, new object[0]);
                }
            }
        }

        private void CheckHeaderReturnValue(CsElement element, TypeToken returnType, XmlDocument formattedDocs)
        {
            if (returnType != null)
            {
                XmlNode documentationXml = formattedDocs.SelectSingleNode("root/returns");
                if (returnType.Text != "void")
                {
                    if (documentationXml == null)
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.ElementReturnValueMustBeDocumented, new object[0]);
                    }
                    else if ((documentationXml.InnerXml == null) || (documentationXml.InnerXml.Length == 0))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.ElementReturnValueDocumentationMustHaveText, new object[0]);
                    }
                    else
                    {
                        this.CheckDocumentationValidity(element, element.LineNumber, documentationXml, "return");
                    }
                }
                else if (documentationXml != null)
                {
                    base.AddViolation(element, StyleCop.CSharp.Rules.VoidReturnValueMustNotBeDocumented, new object[0]);
                }
            }
        }

        private void CheckHeaderSummary(CsElement element, int lineNumber, bool partialElement, XmlDocument formattedDocs)
        {
            XmlNode documentationXml = formattedDocs.SelectSingleNode("root/summary");
            string documentationType = "summary";
            if ((documentationXml == null) && partialElement)
            {
                documentationXml = formattedDocs.SelectSingleNode("root/content");
                documentationType = "content";
            }
            if (documentationXml == null)
            {
                if (partialElement)
                {
                    base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.PartialElementDocumentationMustHaveSummary, new object[0]);
                }
                else
                {
                    base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementDocumentationMustHaveSummary, new object[0]);
                }
            }
            else if (string.IsNullOrEmpty(documentationXml.InnerXml))
            {
                if (partialElement)
                {
                    base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.PartialElementDocumentationMustHaveSummaryText, new object[0]);
                }
                else
                {
                    base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementDocumentationMustHaveSummaryText, new object[0]);
                }
            }
            else
            {
                int startIndex = 0;
                startIndex = 0;
                while (startIndex < documentationXml.InnerText.Length)
                {
                    if (((documentationXml.InnerText[startIndex] != '\r') && (documentationXml.InnerText[startIndex] != '\n')) && ((documentationXml.InnerText[startIndex] != '\t') && (documentationXml.InnerText[startIndex] != ' ')))
                    {
                        break;
                    }
                    startIndex++;
                }
                if (documentationXml.InnerText.Substring(startIndex, documentationXml.InnerText.Length - startIndex).StartsWith("Summary description for", StringComparison.Ordinal))
                {
                    base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.ElementDocumentationMustNotHaveDefaultSummary, new object[0]);
                }
                else
                {
                    this.CheckDocumentationValidity(element, lineNumber, documentationXml, documentationType);
                }
            }
        }

        private void CheckInheritDocRules(CsElement element)
        {
            bool flag = false;
            if (element.ElementType == ElementType.Class)
            {
                Class class2 = (Class) element;
                if (string.IsNullOrEmpty(class2.BaseClass) && (class2.ImplementedInterfaces.Count == 0))
                {
                    flag = true;
                }
            }
            else if ((element.ElementType == ElementType.Interface) || (element.ElementType == ElementType.Struct))
            {
                if (((ClassBase) element).ImplementedInterfaces.Count == 0)
                {
                    flag = true;
                }
            }
            else
            {
                ClassBase parent = element.Parent as ClassBase;
                if (((parent == null) || (((parent.ElementType == ElementType.Class) && !StyleCop.CSharp.Utils.HasABaseClassSpecified(parent)) && !StyleCop.CSharp.Utils.HasImplementedInterfaces(parent))) || (((parent.ElementType == ElementType.Interface) || (parent.ElementType == ElementType.Struct)) && !StyleCop.CSharp.Utils.HasImplementedInterfaces(parent)))
                {
                    if (element.ElementType != ElementType.Method)
                    {
                        flag = true;
                    }
                    else
                    {
                        StyleCop.CSharp.Method method = element as StyleCop.CSharp.Method;
                        if (!element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Override }) && !element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Public }))
                        {
                            flag = true;
                        }
                        else
                        {
                            string name = element.Declaration.Name;
                            if (name == null)
                            {
                                goto Label_01ED;
                            }
                            if (!(name == "Equals"))
                            {
                                if (name == "GetHashCode")
                                {
                                    if ((method.ReturnType.Text != "int") || (method.Parameters.Count > 0))
                                    {
                                        flag = true;
                                    }
                                    goto Label_01EF;
                                }
                                if (name == "ToString")
                                {
                                    if ((method.ReturnType.Text != "string") || (method.Parameters.Count > 0))
                                    {
                                        flag = true;
                                    }
                                    goto Label_01EF;
                                }
                                goto Label_01ED;
                            }
                            if (((method.ReturnType.Text != "bool") || (method.Parameters.Count != 1)) || (method.Parameters[0].Type.Text != "object"))
                            {
                                flag = true;
                            }
                        }
                    }
                }
            }
            goto Label_01EF;
        Label_01ED:
            flag = true;
        Label_01EF:
            if (flag)
            {
                base.AddViolation(element, StyleCop.CSharp.Rules.InheritDocMustBeUsedWithInheritingClass, new object[0]);
            }
        }

        private void CheckPropertySummaryFormatting(Property property, XmlDocument formattedDocs)
        {
            if (!property.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Override }))
            {
                XmlNode node = formattedDocs.SelectSingleNode("root/summary");
                if (node != null)
                {
                    bool flag = ((property.ReturnType.Text == "bool") || (property.ReturnType.Text == "Boolean")) || (property.ReturnType.Text == "System.Boolean");
                    if (property.GetAccessor != null)
                    {
                        if ((property.SetAccessor == null) || !IncludeSetAccessorInDocumentation(property, property.SetAccessor))
                        {
                            string str2 = flag ? CachedCodeStrings.HeaderSummaryForBooleanGetAccessor : CachedCodeStrings.HeaderSummaryForGetAccessor;
                            string str3 = node.InnerText.TrimStart(new char[0]);
                            string headerSummaryForGetAndSetAccessor = CachedCodeStrings.HeaderSummaryForGetAndSetAccessor;
                            if (str3.StartsWith(headerSummaryForGetAndSetAccessor, StringComparison.Ordinal))
                            {
                                base.AddViolation(property, StyleCop.CSharp.Rules.PropertySummaryDocumentationMustOmitSetAccessorWithRestrictedAccess, new object[] { str2 });
                            }
                            else if (!str3.StartsWith(str2, StringComparison.Ordinal))
                            {
                                base.AddViolation(property, StyleCop.CSharp.Rules.PropertySummaryDocumentationMustMatchAccessors, new object[] { str2 });
                            }
                        }
                        else
                        {
                            string str = flag ? CachedCodeStrings.HeaderSummaryForBooleanGetAndSetAccessor : CachedCodeStrings.HeaderSummaryForGetAndSetAccessor;
                            if (!node.InnerText.TrimStart(new char[0]).StartsWith(str, StringComparison.Ordinal))
                            {
                                base.AddViolation(property, StyleCop.CSharp.Rules.PropertySummaryDocumentationMustMatchAccessors, new object[] { str });
                            }
                        }
                    }
                    else if (property.SetAccessor != null)
                    {
                        string str5 = flag ? CachedCodeStrings.HeaderSummaryForBooleanSetAccessor : CachedCodeStrings.HeaderSummaryForSetAccessor;
                        if (!node.InnerText.TrimStart(new char[0]).StartsWith(str5, StringComparison.Ordinal))
                        {
                            base.AddViolation(property, StyleCop.CSharp.Rules.PropertySummaryDocumentationMustMatchAccessors, new object[] { str5 });
                        }
                    }
                }
            }
        }

        private void CheckPropertyValueTag(CsElement element, XmlDocument formattedDocs)
        {
            XmlNode documentationXml = formattedDocs.SelectSingleNode("root/value");
            if (documentationXml == null)
            {
                if (((element.ActualAccess == AccessModifierType.Public) || (element.ActualAccess == AccessModifierType.ProtectedInternal)) || (element.ActualAccess == AccessModifierType.Protected))
                {
                    base.AddViolation(element, StyleCop.CSharp.Rules.PropertyDocumentationMustHaveValue, new object[0]);
                }
            }
            else if (documentationXml.InnerText.Length == 0)
            {
                base.AddViolation(element, StyleCop.CSharp.Rules.PropertyDocumentationMustHaveValueText, new object[0]);
            }
            else
            {
                this.CheckDocumentationValidity(element, element.LineNumber, documentationXml, "value");
            }
        }

        private void CheckSingleLineComments(DocumentRoot root)
        {
            if (root.Tokens != null)
            {
                foreach (CsToken token in root.Tokens)
                {
                    if (((token.CsTokenType == CsTokenType.SingleLineComment) && token.Text.StartsWith("///", StringComparison.Ordinal)) && ((token.Text.Length == 3) || ((token.Text.Length > 3) && (token.Text[3] != '/'))))
                    {
                        base.AddViolation(token.FindParentElement(), token.LineNumber, (System.Enum) StyleCop.CSharp.Rules.SingleLineCommentsMustNotUseDocumentationStyleSlashes, new object[0]);
                    }
                }
            }
        }

        private static int CountOfStringInStringOccurrences(string text, params string[] stringsToFind)
        {
            if ((string.IsNullOrEmpty(text) || (stringsToFind == null)) || (stringsToFind.Length == 0))
            {
                return 0;
            }
            int num = 0;
            foreach (string str in stringsToFind)
            {
                int startIndex = 0;
                while ((startIndex = text.IndexOf(str, startIndex)) != -1)
                {
                    startIndex += str.Length;
                    num++;
                }
            }
            return num;
        }

        public override bool DelayAnalysis(CodeDocument document, int passNumber)
        {
            Param.RequireNotNull(document, "document");
            bool flag = false;
            if (passNumber == 0)
            {
                CsDocument document2 = document as CsDocument;
                if ((document2 != null) && (document2.RootElement != null))
                {
                    flag = StyleCop.CSharp.Utils.ContainsPartialMembers(document2.RootElement);
                }
            }
            return flag;
        }

        public override bool DoAnalysis(CodeDocument document)
        {
            Param.RequireNotNull(document, "document");
            CsDocument document2 = (CsDocument) document;
            CachedCodeStrings.Culture = document.SourceCode.Project.Culture;
            if (document2.FileHeader != null)
            {
                return !document2.FileHeader.UnStyled;
            }
            return true;
        }

        private static XmlNodeList ExtractDocumentationNodeFromIncludedFile(XmlDocument document, string xpath)
        {
            XmlNodeList list = null;
            try
            {
                list = document.SelectNodes(xpath);
                if ((list != null) && (list.Count != 0))
                {
                    return list;
                }
                return null;
            }
            catch (XPathException)
            {
            }
            catch (XmlException)
            {
            }
            catch (ArgumentException)
            {
            }
            return list;
        }

        private static string[] ExtractGenericParametersFromType(string typeName, int index)
        {
            List<string> list = new List<string>();
            for (int i = index + 1; i < typeName.Length; i++)
            {
                StringBuilder builder = new StringBuilder();
                while (i < typeName.Length)
                {
                    if (((typeName[i] == '>') || (typeName[i] == ',')) || char.IsWhiteSpace(typeName[i]))
                    {
                        if (builder.Length > 0)
                        {
                            list.Add(builder.ToString());
                        }
                        break;
                    }
                    builder.Append(typeName[i]);
                    if (i == (typeName.Length - 1))
                    {
                        list.Add(builder.ToString());
                    }
                    i++;
                }
                if (typeName[i] == '>')
                {
                    break;
                }
            }
            return list.ToArray();
        }

        private static List<string> ExtractGenericTypeList(string name)
        {
            List<string> list = null;
            if ((!name.StartsWith("operator", StringComparison.Ordinal) || (name.Length <= 8)) || !char.IsWhiteSpace(name[8]))
            {
                int startIndex = name.LastIndexOf(".", StringComparison.Ordinal);
                if (startIndex == -1)
                {
                    startIndex = 0;
                }
                int num2 = name.IndexOf("<", startIndex, StringComparison.Ordinal);
                if (num2 == -1)
                {
                    return list;
                }
                list = new List<string>();
                while (true)
                {
                    int num3 = name.IndexOf(",", num2 + 1, StringComparison.Ordinal);
                    if (num3 == -1)
                    {
                        break;
                    }
                    list.Add(ExtractGenericTypeParameter(name, num2, num3));
                    num2 = num3;
                }
                int endIndex = name.IndexOf(">", num2 + 1, StringComparison.Ordinal);
                if (endIndex != -1)
                {
                    list.Add(ExtractGenericTypeParameter(name, num2, endIndex));
                }
            }
            return list;
        }

        private static string ExtractGenericTypeParameter(string fullType, int startIndex, int endIndex)
        {
            string str = fullType.Substring(startIndex + 1, (endIndex - startIndex) - 1).Trim();
            if (str.StartsWith("out ", StringComparison.Ordinal) && (str.Length > 4))
            {
                return str.Substring(4);
            }
            if (str.StartsWith("in ", StringComparison.Ordinal) && (str.Length > 3))
            {
                str = str.Substring(3);
            }
            return str;
        }

        private static void ExtractIncludeTagFileAndPath(XmlNode documentationNode, out string file, out string path)
        {
            file = null;
            XmlAttribute attribute = documentationNode.Attributes["file"];
            if (attribute != null)
            {
                file = attribute.Value;
            }
            path = null;
            attribute = documentationNode.Attributes["path"];
            if (attribute != null)
            {
                path = attribute.Value;
            }
        }

        private static XmlDocument FormatXmlDocument(XmlDocument doc)
        {
            XmlDocument document = (XmlDocument) doc.Clone();
            document.Normalize();
            Queue<XmlNode> queue = new Queue<XmlNode>();
            queue.Enqueue(document.DocumentElement);
            while (queue.Count > 0)
            {
                XmlNodeList childNodes = queue.Dequeue().ChildNodes;
                for (int i = 0; i < childNodes.Count; i++)
                {
                    XmlNode item = childNodes[i];
                    if (item.NodeType != XmlNodeType.Text)
                    {
                        queue.Enqueue(item);
                    }
                    else
                    {
                        string str = item.InnerText.TrimEnd(new char[0]).Replace(" \n", "\n").Replace("\n ", "\n").Replace("\n\n\n\n", " ").Replace("\n\n\n", " ").Replace("\n\n", " ").Replace("\n", " ");
                        if (i != (childNodes.Count - 1))
                        {
                            str = str + " ";
                        }
                        item.InnerText = str;
                    }
                }
            }
            return document;
        }

        private static string GetActualQualifiedNamespace(ClassBase type)
        {
            CsElement parent = type;
            while (parent.Parent is ClassBase)
            {
                parent = (CsElement) parent.Parent;
            }
            string fullyQualifiedName = parent.FullyQualifiedName;
            int num = fullyQualifiedName.LastIndexOf('.');
            if (num != -1)
            {
                return fullyQualifiedName.Substring(0, num + 1);
            }
            return string.Empty;
        }

        public override int GetDependantFilesHashCode(CultureInfo culture)
        {
            return NamingService.GetNamingService(culture).GetDependantFilesHashCode();
        }

        private static string GetExampleSummaryTextForConstructorType(Constructor constructor, string type)
        {
            if (constructor.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Static }))
            {
                return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.ExampleHeaderSummaryForStaticConstructor, new object[] { constructor.Declaration.Name, type });
            }
            if ((constructor.AccessModifier == AccessModifierType.Private) && ((constructor.Parameters == null) || (constructor.Parameters.Count == 0)))
            {
                return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.ExampleHeaderSummaryForPrivateInstanceConstructor, new object[] { constructor.Declaration.Name, type });
            }
            string name = ((ClassBase) constructor.Parent).Declaration.Name;
            string str2 = "{";
            if (name.Contains("<"))
            {
                string[] source = name.SubstringAfterLast('<').TrimEnd(new char[] { '>' }).Split(new char[] { ',' });
                for (int i = 0; i < source.Count<string>(); i++)
                {
                    string str4 = source[i];
                    str2 = str2 + str4;
                    if (i < (source.Count<string>() - 1))
                    {
                        str2 = str2 + ",";
                    }
                }
                str2 = str2 + "}";
            }
            else
            {
                str2 = constructor.Declaration.Name;
            }
            return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.ExampleHeaderSummaryForInstanceConstructor, new object[] { str2, type });
        }

        private static string GetExampleSummaryTextForDestructor(Destructor destructor)
        {
            return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.ExampleHeaderSummaryForDestructor, new object[] { destructor.Declaration.Name.Substring(1) });
        }

        private static string GetExpectedSummaryTextForConstructorType(Constructor constructor, string type, string typeRegex)
        {
            if (constructor.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Static }))
            {
                return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.HeaderSummaryForStaticConstructor, new object[] { typeRegex, type });
            }
            if ((constructor.AccessModifier == AccessModifierType.Private) && ((constructor.Parameters == null) || (constructor.Parameters.Count == 0)))
            {
                return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.HeaderSummaryForPrivateInstanceConstructor, new object[] { typeRegex, type });
            }
            return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.HeaderSummaryForInstanceConstructor, new object[] { typeRegex, type });
        }

        private static string GetExpectedSummaryTextForDestructor(string typeRegex)
        {
            return string.Format(CultureInfo.InvariantCulture, CachedCodeStrings.HeaderSummaryForDestructor, new object[] { typeRegex });
        }

        private bool GetFirstTypeName(CsElement parentElement, out string firstTypeName, out ElementType firstTypeElementType)
        {
            bool flag = false;
            firstTypeName = null;
            firstTypeElementType = ElementType.Root;
            foreach (CsElement element in parentElement.ChildElements)
            {
                if (element.ElementType == ElementType.Namespace)
                {
                    flag = this.GetFirstTypeName(element, out firstTypeName, out firstTypeElementType);
                    if (firstTypeName != null)
                    {
                        return flag;
                    }
                }
                else if (((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Interface)) || (((element.ElementType == ElementType.Struct) || (element.ElementType == ElementType.Delegate)) || (element.ElementType == ElementType.Enum)))
                {
                    if (element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Partial }))
                    {
                        flag = true;
                    }
                    firstTypeName = element.FullyQualifiedName.SubstringAfterLast('.');
                    firstTypeElementType = element.ElementType;
                    if ((((CsElement) element.Parent).ChildElements.Count <= 1) || ((element.ElementType != ElementType.Delegate) && (element.ElementType != ElementType.Enum)))
                    {
                        return flag;
                    }
                }
            }
            return flag;
        }

        private string GetShortestItem(params string[] items)
        {
            if (items.Length == 0)
            {
                return null;
            }
            string str = items[0];
            foreach (string str2 in items)
            {
                if ((str2.Length > 0) && (str2.Length <= str.Length))
                {
                    str = str2;
                }
            }
            return str;
        }

        private static bool IncludeSetAccessorInDocumentation(Property property, Accessor setAccessor)
        {
            if ((setAccessor.AccessModifier != property.AccessModifier) && ((setAccessor.AccessModifier != AccessModifierType.Private) || setAccessor.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Private })))
            {
                if ((setAccessor.AccessModifier == AccessModifierType.Internal) && ((property.ActualAccess == AccessModifierType.Internal) || (property.ActualAccess == AccessModifierType.ProtectedAndInternal)))
                {
                    return true;
                }
                if (((property.ActualAccess != AccessModifierType.Private) || setAccessor.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Private })) && ((setAccessor.AccessModifier != AccessModifierType.Protected) && (setAccessor.AccessModifier != AccessModifierType.ProtectedInternal)))
                {
                    return false;
                }
            }
            return true;
        }

        private bool InsertIncludedDocumentation(CsElement element, XmlDocument documentation)
        {
            return this.InsertIncludedDocumentationForNode(element, documentation.DocumentElement);
        }

        private bool InsertIncludedDocumentationForChildNodes(CsElement element, XmlNode documentationNode)
        {
            if (documentationNode.ChildNodes.Count == 1)
            {
                if (!this.InsertIncludedDocumentationForNode(element, documentationNode.FirstChild))
                {
                    return false;
                }
            }
            else if (documentationNode.ChildNodes.Count > 1)
            {
                XmlNode[] nodeArray = new XmlNode[documentationNode.ChildNodes.Count];
                int num = 0;
                for (XmlNode node = documentationNode.FirstChild; node != null; node = node.NextSibling)
                {
                    nodeArray[num++] = node;
                }
                for (int i = 0; i < nodeArray.Length; i++)
                {
                    if (!this.InsertIncludedDocumentationForNode(element, nodeArray[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool InsertIncludedDocumentationForNode(CsElement element, XmlNode documentationNode)
        {
            if ((documentationNode.NodeType == XmlNodeType.Element) && (documentationNode.Name == "include"))
            {
                return this.LoadAndReplaceIncludeTag(element, documentationNode);
            }
            return this.InsertIncludedDocumentationForChildNodes(element, documentationNode);
        }

        private static bool IsNonPublicStaticExternDllImport(CsElement element)
        {
            if (element.ActualAccess != AccessModifierType.Public)
            {
                if (!element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Static }) || !element.Declaration.ContainsModifier(new CsTokenType[] { CsTokenType.Extern }))
                {
                    return false;
                }
                if (element.Attributes != null)
                {
                    foreach (StyleCop.CSharp.Attribute attribute in element.Attributes)
                    {
                        if (attribute.AttributeExpressions != null)
                        {
                            foreach (Expression expression in attribute.AttributeExpressions)
                            {
                                AttributeExpression expression2 = expression as AttributeExpression;
                                if (expression2 != null)
                                {
                                    foreach (Expression expression3 in expression2.ChildExpressions)
                                    {
                                        MethodInvocationExpression expression4 = expression3 as MethodInvocationExpression;
                                        if (((expression4 != null) && (expression4.Name != null)) && ((expression4.Name.Tokens.MatchTokens(new string[] { "DllImport" }) || expression4.Name.Tokens.MatchTokens(new string[] { "DllImportAttribute" })) || (expression4.Name.Tokens.MatchTokens(new string[] { "System", ".", "Runtime", ".", "InteropServices", ".", "DllImport" }) || expression4.Name.Tokens.MatchTokens(new string[] { "System", ".", "Runtime", ".", "InteropServices", ".", "DllImportAttribute" }))))
                                        {
                                            return true;
                                        }
                                    }
                                    continue;
                                }
                            }
                            continue;
                        }
                    }
                }
            }
            return false;
        }

        private static bool IsXmlHeaderLineEmpty(CsToken token)
        {
            int num = 0;
            for (int i = 0; i < token.Text.Length; i++)
            {
                char c = token.Text[i];
                if (num < 3)
                {
                    if (c != '/')
                    {
                        return false;
                    }
                    num++;
                }
                else if (!char.IsWhiteSpace(c))
                {
                    return false;
                }
            }
            return true;
        }

        private bool LoadAndReplaceIncludeTag(CsElement element, XmlNode documentationNode)
        {
            string str;
            string str2;
            ExtractIncludeTagFileAndPath(documentationNode, out str, out str2);
            if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(str2))
            {
                base.AddViolation(element, StyleCop.CSharp.Rules.IncludeNodeDoesNotContainValidFileAndPath, new object[] { documentationNode.OuterXml });
            }
            else
            {
                string directoryName = Path.GetDirectoryName(element.Document.SourceCode.Path);
                CachedXmlDocument includedDocument = this.LoadIncludedDocumentationFile(directoryName, str);
                if (includedDocument == null)
                {
                    base.AddViolation(element, StyleCop.CSharp.Rules.IncludedDocumentationFileDoesNotExist, new object[] { str });
                }
                else
                {
                    XmlNodeList includedDocumentationNodes = ExtractDocumentationNodeFromIncludedFile(includedDocument.Document, str2);
                    if (includedDocumentationNodes == null)
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.IncludedDocumentationXPathDoesNotExist, new object[] { str2, str });
                    }
                    else if (!this.ReplaceIncludeTagWithIncludedDocumentationContents(element, documentationNode, includedDocument, includedDocumentationNodes))
                    {
                        base.AddViolation(element, StyleCop.CSharp.Rules.IncludedDocumentationXPathDoesNotExist, new object[] { str2, str });
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static CachedXmlDocument LoadDocFileFromDisk(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(path);
                    return new CachedXmlDocument(path, document);
                }
            }
            catch (IOException)
            {
            }
            catch (UnauthorizedAccessException)
            {
            }
            catch (SecurityException)
            {
            }
            catch (ArgumentException)
            {
            }
            catch (XmlException)
            {
            }
            return null;
        }

        private void LoadHeaderIntoDocuments(CsElement element, XmlHeader header, int lineNumber, out XmlDocument rawDocs, out XmlDocument formattedDocs)
        {
            rawDocs = new XmlDocument();
            try
            {
                string xml = "<root>" + header.RawText + "</root>";
                rawDocs.LoadXml(xml);
                formattedDocs = FormatXmlDocument(rawDocs);
            }
            catch (XmlException exception)
            {
                base.AddViolation(element, lineNumber, (System.Enum) StyleCop.CSharp.Rules.DocumentationMustContainValidXml, new object[] { exception.Message });
                rawDocs = (XmlDocument) (formattedDocs = null);
            }
        }

        private CachedXmlDocument LoadIncludedDocumentationFile(string basePath, string file)
        {
            CachedXmlDocument document = null;
            string fullPath = file;
            if (!Path.IsPathRooted(file))
            {
                try
                {
                    fullPath = Path.GetFullPath(Path.Combine(basePath, file));
                }
                catch (ArgumentException)
                {
                    fullPath = null;
                }
                catch (SecurityException)
                {
                    fullPath = null;
                }
                catch (NotSupportedException)
                {
                    fullPath = null;
                }
                catch (PathTooLongException)
                {
                    fullPath = null;
                }
            }
            if (fullPath != null)
            {
                string key = fullPath.ToUpper(CultureInfo.InvariantCulture);
                lock (this)
                {
                    if (this.includedDocs == null)
                    {
                        this.includedDocs = new Dictionary<string, CachedXmlDocument>();
                    }
                    if (!this.includedDocs.TryGetValue(key, out document))
                    {
                        document = LoadDocFileFromDisk(key);
                        this.includedDocs.Add(key, document);
                    }
                }
            }
            return document;
        }

        private static bool MatchCopyrightText(string copyright1, string copyright2)
        {
            if ((copyright1 == null) || (copyright2 == null))
            {
                return ((copyright1 == null) && (copyright2 == null));
            }
            copyright1 = copyright1.Replace(Environment.NewLine, "   ");
            copyright2 = copyright2.Replace(Environment.NewLine, "   ");
            if (copyright1.Length != copyright2.Length)
            {
                return false;
            }
            for (int i = 0; i < copyright1.Length; i++)
            {
                char character = copyright1[i];
                char ch2 = copyright2[i];
                if ((character != ch2) && (!CharacterIsCopyright(character) || !CharacterIsCopyright(ch2)))
                {
                    return false;
                }
            }
            return true;
        }

        private void ParseHeader(CsElement element, XmlHeader header, int lineNumber, bool partialElement)
        {
            XmlDocument rawDocs = null;
            XmlDocument formattedDocs = null;
            this.LoadHeaderIntoDocuments(element, header, lineNumber, out rawDocs, out formattedDocs);
            if ((rawDocs != null) && (formattedDocs != null))
            {
                if (rawDocs.SelectSingleNode("root/inheritdoc") != null)
                {
                    this.CheckInheritDocRules(element);
                }
                else if (this.InsertIncludedDocumentation(element, formattedDocs))
                {
                    this.CheckForBlankLinesInDocumentationHeader(element, header);
                    this.CheckHeaderSummary(element, lineNumber, partialElement, formattedDocs);
                    this.CheckHeaderElementsForEmptyText(element, formattedDocs);
                    if (element.ElementType == ElementType.Method)
                    {
                        StyleCop.CSharp.Method method = element as StyleCop.CSharp.Method;
                        this.CheckHeaderParams(element, method.Parameters, formattedDocs);
                        this.CheckHeaderReturnValue(element, method.ReturnType, formattedDocs);
                    }
                    else if (element.ElementType == ElementType.Constructor)
                    {
                        Constructor constructor = element as Constructor;
                        this.CheckHeaderParams(element, constructor.Parameters, formattedDocs);
                        this.CheckConstructorSummaryText(constructor, formattedDocs);
                    }
                    else if (element.ElementType == ElementType.Delegate)
                    {
                        StyleCop.CSharp.Delegate delegate2 = element as StyleCop.CSharp.Delegate;
                        this.CheckHeaderParams(element, delegate2.Parameters, formattedDocs);
                        this.CheckHeaderReturnValue(element, delegate2.ReturnType, formattedDocs);
                    }
                    else if (element.ElementType == ElementType.Indexer)
                    {
                        Indexer indexer = element as Indexer;
                        this.CheckHeaderParams(element, indexer.Parameters, formattedDocs);
                        this.CheckHeaderReturnValue(element, indexer.ReturnType, formattedDocs);
                    }
                    else if (element.ElementType == ElementType.Property)
                    {
                        this.CheckPropertyValueTag(element, formattedDocs);
                        this.CheckPropertySummaryFormatting(element as Property, formattedDocs);
                    }
                    else if (element.ElementType == ElementType.Destructor)
                    {
                        this.CheckDestructorSummaryText((Destructor) element, formattedDocs);
                    }
                    if ((((element.ElementType == ElementType.Method) || (element.ElementType == ElementType.Constructor)) || ((element.ElementType == ElementType.Delegate) || (element.ElementType == ElementType.Indexer))) || ((((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Struct)) || ((element.ElementType == ElementType.Interface) || (element.ElementType == ElementType.Property))) || (((element.ElementType == ElementType.Event) || (element.ElementType == ElementType.Field)) || (element.ElementType == ElementType.Destructor))))
                    {
                        this.CheckForRepeatingComments(element, formattedDocs);
                    }
                    if (((element.ElementType == ElementType.Class) || (element.ElementType == ElementType.Method)) || (((element.ElementType == ElementType.Delegate) || (element.ElementType == ElementType.Interface)) || (element.ElementType == ElementType.Struct)))
                    {
                        this.CheckGenericTypeParams(element, formattedDocs);
                    }
                }
            }
        }

        public override void PostAnalyze()
        {
            base.PostAnalyze();
            this.includedDocs = null;
        }

        public override void PreAnalyze()
        {
            base.PreAnalyze();
            this.includedDocs = null;
        }

        private string RemoveExtensions(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                IList<string> list = new string[] { ".asax", ".ascx", ".ashx", ".asmx", ".aspx", ".axd", ".browser", ".cd", ".compile", ".config", ".master", ".msgx", ".svc", ".cs" };
                string item = Path.GetExtension(path).ToLowerInvariant();
                if (list.Contains(item))
                {
                    return this.RemoveExtensions(Path.GetFileNameWithoutExtension(path));
                }
            }
            return path;
        }

        private static string RemoveGenericsFromTypeName(string typeName)
        {
            int index = typeName.IndexOf('<');
            if (index > 0)
            {
                typeName = typeName.Substring(0, index);
            }
            return typeName;
        }

        private bool ReplaceIncludeTagWithIncludedDocumentationContents(CsElement element, XmlNode documentationNode, CachedXmlDocument includedDocument, XmlNodeList includedDocumentationNodes)
        {
            try
            {
                XmlNode refChild = documentationNode;
                foreach (XmlNode node2 in includedDocumentationNodes)
                {
                    XmlNode node3 = documentationNode.OwnerDocument.ImportNode(node2, true);
                    this.InsertIncludedDocumentationForNode(element, node3);
                    documentationNode.ParentNode.InsertAfter(node3, refChild);
                    refChild = node3;
                }
                documentationNode.ParentNode.RemoveChild(documentationNode);
            }
            catch (XmlException)
            {
                return false;
            }
            return true;
        }

        private static int XmlHeaderLineCodeElementCount(CsToken token)
        {
            string text = token.Text;
            int num = CountOfStringInStringOccurrences(text, new string[] { "<code ", "<code>", "<c>" });
            int num2 = CountOfStringInStringOccurrences(text, new string[] { "</code>", "</c>" });
            return (num - num2);
        }

        public override ICollection<IPropertyControlPage> SettingsPages
        {
            get
            {
                return new IPropertyControlPage[] { new CompanyInformation(this) };
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct AnalyzerSettings
        {
            public bool IgnoreInternals;
            public bool IgnorePrivates;
            public bool RequireFields;
        }

        private class CachedXmlDocument
        {
            private readonly XmlDocument document;
            private readonly string filePath;

            public CachedXmlDocument(string filePath, XmlDocument document)
            {
                this.filePath = filePath;
                this.document = document;
            }

            public XmlDocument Document
            {
                get
                {
                    return this.document;
                }
            }

            public string FilePath
            {
                get
                {
                    return this.filePath;
                }
            }
        }
    }
}

