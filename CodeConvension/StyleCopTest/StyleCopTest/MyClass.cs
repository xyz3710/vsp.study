﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleCopTest
{
	/// <summary>
	/// Class MyClass.
	/// </summary>
	public class MyClass
	{
		private bool _enabled;
		private bool _fieldName;

		#region Constructor
		/// <summary>
		/// MyClass class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public MyClass()
		{
		}
		#endregion

		#region Test region
		/// <summary>
		/// Value를 구하거나 설정합니다.
		/// </summary>
		/// <value>Value</value>
		public int Value
		{
			get;
			set;
		}
		#endregion

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="MyClass"/> is enabled.
		/// </summary>
		/// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
		public bool Enabled
		{
			get
			{
				return this._enabled;
			}
			set
			{
				this._enabled = value;
			}
		}

		/// <summary>
		/// Gets the string public.
		/// </summary>
		/// <param name="param1">The param1.</param>
		/// <param name="param2">The param2.</param>
		/// <param name="param3">The param3.</param>
		/// <param name="param4">The param4.</param>
		/// <returns>System.String.</returns>
		public string GetStringPublic(
			int param1,
			int param2,
			int param3,
			int param4)
		{
			if (param1 == 0)
			{
				return string.Empty;
			}
			param4 = param1 + param2;
			switch (param2)
			{
				case 0:
					
					break;
				case 1:

					break;
			}
			return GetString();
		}

		/// <summary>
		/// sdfsdf
		/// </summary>
		/// <returns>value</returns>
		private static string GetStringStatic()
		{
			int? nullable = null;
			//// Nullable<int> nullable2 = null;
			string empty = string.Empty;

			empty = string.Format("{0}, {1}", "test" + "context", nullable);

			return string.Empty;
		}

		private string GetString()
		{
			return string.Empty;
		}

		private int GetValue()
		{
			if (true)
			{
				this.Value = 2;
			}

			// comments
			// single blank comment
			return this.Value;
		}

		/// <summary>
		/// Ss the a1500.
		/// </summary>
		/// <returns>System.Object.</returns>
		public object SA1500()
		{
			lock (this)
			{
				return this;
			}
		}
	}
}
