﻿// ****************************************************************************************************************** //
//	Domain		:	StyleCopTest.TestEnum
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2014년 2월 18일 화요일 오후 5:33
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="TestEnum.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleCopTest
{
	/// <summary>
	/// Enum TestEnum
	/// </summary>
	internal enum TestEnum
	{
		/// <summary>
		/// The item1
		/// </summary>
		Item1,
		/// <summary>
		/// The item2
		/// </summary>
		Item2,
		/// <summary>
		/// The item3
		/// </summary>
		Item3,
	}
}
