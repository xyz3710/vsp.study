﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleCopTest
{
	/// <summary>
	/// Enum PublicEnum
	/// </summary>
	public enum PublicEnum
	{
		/// <summary>
		/// The item1
		/// </summary>
		Item1,
		/// <summary>
		/// The item2
		/// </summary>
		Item2,
		/// <summary>
		/// The item3
		/// </summary>
		Item3,
	}
}
