﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleCopTest
{
	/// <summary>
	/// Class Program.
	/// </summary>
	public class Program
	{
		/// <summary>
		/// The age
		/// </summary>
		public static readonly string Age;
		private int _age;
		private string _name;

		private static void Main(string[] args)
		{
			const int TEST_CONST = 10;
			int xy = 5 + 6;
			if (xy == 11)
			{
				Console.WriteLine("ok");
			}
			
			Console.WriteLine(xy);
			Console.WriteLine(TEST_CONST);

			List<int> items = new List<int> { 1, 2, 3, 4, 5 };

			var y = from x in items
					select x;
		}
	}
}
