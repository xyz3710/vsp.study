﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StyleCop;
using StyleCop.CSharp;

namespace GS.CI.StyleCop.Rules
{
	/// <summary>
	/// Gachisoft ReadablityRules.
	/// </summary>
	[SourceAnalyzer(typeof(CsParser))]
	public class ReadablityRules : SourceAnalyzer
	{
		/// <summary>
		/// Analyzes the document.
		/// </summary>
		/// <param name="document">The document.</param>
		public override void AnalyzeDocument(CodeDocument document)
		{
			Param.RequireNotNull(document, "document");
			CsDocument doc = (CsDocument)document;

			if ((doc.RootElement != null) && doc.RootElement.Generated == false)
			{
				// check all class entries
				doc.WalkDocument(CheckElement);
			}
		}

		private bool CheckElement(CsElement element, CsElement parentElement, object context)
		{
			if (Cancel == true)
			{
				return false;
			}

			if (element.Generated == true)
			{
				return true;
			}

			var token = element.Tokens.FirstOrDefault();

			if (token == null || token.Generated == true)
			{
				return true;
			}

			if (token.CsTokenType == CsTokenType.CloseCurlyBracket)
			{
				var statement = token.FindParentStatement();

				if (statement == null)
				{
					return true;
				}
				else
				{
					switch (statement.StatementType)
					{
						case StatementType.Break:
						case StatementType.Continue:
						case StatementType.Foreach:
						case StatementType.For:
						case StatementType.Goto:
						case StatementType.If:
						case StatementType.Label:
						case StatementType.Return:
						case StatementType.Switch:
						case StatementType.Try:
						case StatementType.Using:
						case StatementType.While:
							AddViolation(
						element,
						element.Location,
						"FlowControlKeywordsMustSeparatedLine",
						element.Name);

							break;
					}
				}
			}

			/*
			switch (token.CsTokenType)
			{
				case CsTokenType.Break:
				case CsTokenType.Case:
				case CsTokenType.Continue:
				case CsTokenType.Do:
				case CsTokenType.For:
				case CsTokenType.Foreach:
				case CsTokenType.Goto:
				case CsTokenType.If:
				case CsTokenType.Switch:
				case CsTokenType.Try:
				case CsTokenType.UsingDirective:
				case CsTokenType.While:
				case CsTokenType.WhileDo:
					AddViolation(
						element,
						element.Location,
						"FlowControlKeywordsMustSeparatedLine",
						element.Name);

					break;
			}
			*/

			if (element.ElementType == ElementType.Field &&
				element.AccessModifier == AccessModifierType.Private)
			{
				string declaredName = element.Declaration.Name;

				if (declaredName.StartsWith("_", StringComparison.Ordinal) == false ||
					declaredName.Substring(1, 1).ToLower() != declaredName.Substring(1, 1))
				{
					AddViolation(
						element,
						element.Location,
						"FlowControlKeywordsMustSeparatedLine",
						element.Name);
				}
			}

			return true;
		}
	}
}
