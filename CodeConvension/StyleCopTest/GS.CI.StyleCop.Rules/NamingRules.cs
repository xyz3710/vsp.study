﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StyleCop;
using StyleCop.CSharp;

namespace GS.CI.StyleCop.Rules
{
	/// <summary>
	/// Gachisoft NamingRules.
	/// </summary>
	[SourceAnalyzer(typeof(CsParser))]
	public class NamingRules : SourceAnalyzer
	{
		/// <summary>
		/// Analyzes the document.
		/// </summary>
		/// <param name="document">The document.</param>
		public override void AnalyzeDocument(CodeDocument document)
		{
			Param.RequireNotNull(document, "document");
			CsDocument doc = (CsDocument)document;

			if ((doc.RootElement != null) && doc.RootElement.Generated == false)
			{
				// check all class entries
				doc.WalkDocument(this.CheckElement);
				//// or ProcessElement(doc.RootElement);
			}
		}

		private bool CheckElement(CsElement element, CsElement parentElement, object context)
		{
			if (Cancel == true)
			{
				return false;
			}

			if (element.Generated == true)
			{
				return true;
			}

			if (element.ElementType == ElementType.Field &&
				element.AccessModifier == AccessModifierType.Private &&
				element.Tokens.Count(x => x.CsTokenType == CsTokenType.Const || x.CsTokenType == CsTokenType.Static) == 0)
			{
				string declaredName = element.Declaration.Name;

				if (declaredName.StartsWith("_", StringComparison.Ordinal) == false ||
					declaredName.Substring(1, 1).ToLower() != declaredName.Substring(1, 1))
				{
					AddViolation(
						element,
						element.Location,
						"PrivateFieldNameMustStartWithUnderscoreFollowedByLowerCase",
						element.Name);
				}
			}

			foreach (CsElement child in element.ChildElements)
			{
				if (this.ProcessElement(child) == false)
				{
					return false;
				}
			}

			return true;
		}

		private bool ProcessElement(CsElement element)
		{
			if (Cancel)
			{
				return false;
			}

			if (element.Generated == false)
			{
				return true;
			}

			if (element.ElementType == ElementType.Field &&
				element.AccessModifier == AccessModifierType.Private)
			{
				string declaredName = element.Declaration.Name;

				if (declaredName.StartsWith("_", StringComparison.Ordinal) == false ||
					declaredName.Substring(1, 1).ToLower() != declaredName.Substring(1, 1))
				{
					AddViolation(
						element,
						element.Location,
						"PrivateFieldNameMustStartWithUnderscoreFollowedByLowerCase",
						element.Name);
				}
			}

			foreach (CsElement child in element.ChildElements)
			{
				if (ProcessElement(child) == false)
				{
					return false;
				}
			}

			return true;
		}
	}
}
