﻿using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Custom rules for GachiSoft")]
[assembly: AssemblyDescription("Custom rules for GachiSoft")]
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyCompany("GachiSoft")]
[assembly: AssemblyProduct("GS.CI.StleCop.Rules")]
[assembly: AssemblyCopyright("Copyright (C) GachiSoft 2014")]
[assembly: AssemblyTrademark("GachiSoft")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0c29b88a-2f7d-413a-ab6b-bfd48ddb0dc9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2014.0226.18.0004")]
[assembly: AssemblyFileVersion("2014.0226.18.0004")]
//// Added by CI.MSBuild in 2014.02.26 09:04:48
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.EnableEditAndContinue | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
// Added by CI.MSBuild in 2014.02.26 17:47:19
[assembly: NeutralResourcesLanguage("ko-KR")]
