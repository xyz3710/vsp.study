﻿namespace DA01
{
	partial class FrmBasic
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.statusBar = new System.Windows.Forms.ToolStripStatusLabel();
			this.xGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.txtSqlCommand = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtParam1 = new System.Windows.Forms.TextBox();
			this.txtParam2 = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rdOracleProvider = new System.Windows.Forms.RadioButton();
			this.rdSQLProvider = new System.Windows.Forms.RadioButton();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.rdSPCommand = new System.Windows.Forms.RadioButton();
			this.button1 = new System.Windows.Forms.Button();
			this.rdSQLCommand = new System.Windows.Forms.RadioButton();
			this.btnTest = new System.Windows.Forms.Button();
			this.btnViewProduct = new System.Windows.Forms.Button();
			this.btnTransfer = new System.Windows.Forms.Button();
			this.btnUpdateDataSet = new System.Windows.Forms.Button();
			this.btnConnectDB = new System.Windows.Forms.Button();
			this.cboTables = new System.Windows.Forms.ComboBox();
			this.btn2Params = new System.Windows.Forms.Button();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGrid)).BeginInit();
			this.panel1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBar});
			this.statusStrip1.Location = new System.Drawing.Point(0, 651);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1016, 22);
			this.statusStrip1.TabIndex = 4;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// statusBar
			// 
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(0, 17);
			// 
			// xGrid
			// 
			this.xGrid.DisplayLayout.AddNewBox.Hidden = false;
			appearance1.BackColor = System.Drawing.SystemColors.Window;
			appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.xGrid.DisplayLayout.Appearance = appearance1;
			this.xGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance2.BorderColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.GroupByBox.Appearance = appearance2;
			appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
			this.xGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance4.BackColor2 = System.Drawing.SystemColors.Control;
			appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
			this.xGrid.DisplayLayout.MaxColScrollRegions = 1;
			this.xGrid.DisplayLayout.MaxRowScrollRegions = 1;
			appearance5.BackColor = System.Drawing.SystemColors.Window;
			appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xGrid.DisplayLayout.Override.ActiveCellAppearance = appearance5;
			appearance6.BackColor = System.Drawing.SystemColors.Highlight;
			appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xGrid.DisplayLayout.Override.ActiveRowAppearance = appearance6;
			this.xGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.Yes;
			this.xGrid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
			this.xGrid.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
			this.xGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.xGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance7.BackColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.Override.CardAreaAppearance = appearance7;
			appearance8.BorderColor = System.Drawing.Color.Silver;
			appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xGrid.DisplayLayout.Override.CellAppearance = appearance8;
			this.xGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xGrid.DisplayLayout.Override.CellPadding = 0;
			appearance9.BackColor = System.Drawing.SystemColors.Control;
			appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance9.BorderColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.Override.GroupByRowAppearance = appearance9;
			appearance10.TextHAlignAsString = "Left";
			this.xGrid.DisplayLayout.Override.HeaderAppearance = appearance10;
			this.xGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance11.BackColor = System.Drawing.SystemColors.Window;
			appearance11.BorderColor = System.Drawing.Color.Silver;
			this.xGrid.DisplayLayout.Override.RowAppearance = appearance11;
			this.xGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
			appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
			this.xGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xGrid.Location = new System.Drawing.Point(3, 113);
			this.xGrid.Name = "xGrid";
			this.xGrid.Size = new System.Drawing.Size(1010, 535);
			this.xGrid.TabIndex = 0;
			this.xGrid.Text = "ultraGrid1";
			this.xGrid.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.xGrid_AfterRowUpdate);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.tableLayoutPanel1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1016, 651);
			this.panel1.TabIndex = 6;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.xGrid, 0, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 651);
			this.tableLayoutPanel1.TabIndex = 6;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 4;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.txtSqlCommand, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.label3, 2, 1);
			this.tableLayoutPanel2.Controls.Add(this.txtParam1, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.txtParam2, 3, 1);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 53);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1010, 54);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 3);
			this.label1.Margin = new System.Windows.Forms.Padding(3);
			this.label1.Name = "label1";
			this.tableLayoutPanel2.SetRowSpan(this.label1, 2);
			this.label1.Size = new System.Drawing.Size(114, 48);
			this.label1.TabIndex = 0;
			this.label1.Text = "SQL command : ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtSqlCommand
			// 
			this.txtSqlCommand.AcceptsReturn = true;
			this.txtSqlCommand.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtSqlCommand.Location = new System.Drawing.Point(120, 0);
			this.txtSqlCommand.Margin = new System.Windows.Forms.Padding(0);
			this.txtSqlCommand.Multiline = true;
			this.txtSqlCommand.Name = "txtSqlCommand";
			this.tableLayoutPanel2.SetRowSpan(this.txtSqlCommand, 2);
			this.txtSqlCommand.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtSqlCommand.Size = new System.Drawing.Size(770, 54);
			this.txtSqlCommand.TabIndex = 1;
			this.txtSqlCommand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSqlCommand_KeyDown);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(893, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 27);
			this.label2.TabIndex = 2;
			this.label2.Text = "param1";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(893, 27);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 27);
			this.label3.TabIndex = 3;
			this.label3.Text = "param2";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtParam1
			// 
			this.txtParam1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtParam1.Location = new System.Drawing.Point(963, 3);
			this.txtParam1.Name = "txtParam1";
			this.txtParam1.Size = new System.Drawing.Size(44, 21);
			this.txtParam1.TabIndex = 4;
			// 
			// txtParam2
			// 
			this.txtParam2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtParam2.Location = new System.Drawing.Point(963, 30);
			this.txtParam2.Name = "txtParam2";
			this.txtParam2.Size = new System.Drawing.Size(44, 21);
			this.txtParam2.TabIndex = 4;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 6;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.groupBox2, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.btnTest, 4, 0);
			this.tableLayoutPanel3.Controls.Add(this.btnViewProduct, 4, 1);
			this.tableLayoutPanel3.Controls.Add(this.btnTransfer, 5, 0);
			this.tableLayoutPanel3.Controls.Add(this.btnUpdateDataSet, 5, 1);
			this.tableLayoutPanel3.Controls.Add(this.btnConnectDB, 3, 0);
			this.tableLayoutPanel3.Controls.Add(this.cboTables, 2, 0);
			this.tableLayoutPanel3.Controls.Add(this.btn2Params, 3, 1);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(1016, 50);
			this.tableLayoutPanel3.TabIndex = 1;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rdOracleProvider);
			this.groupBox1.Controls.Add(this.rdSQLProvider);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.tableLayoutPanel3.SetRowSpan(this.groupBox1, 2);
			this.groupBox1.Size = new System.Drawing.Size(272, 44);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "DB Provider";
			// 
			// rdOracleProvider
			// 
			this.rdOracleProvider.AutoSize = true;
			this.rdOracleProvider.Location = new System.Drawing.Point(195, 19);
			this.rdOracleProvider.Name = "rdOracleProvider";
			this.rdOracleProvider.Size = new System.Drawing.Size(60, 16);
			this.rdOracleProvider.TabIndex = 1;
			this.rdOracleProvider.Text = "Oracle";
			this.rdOracleProvider.UseVisualStyleBackColor = true;
			this.rdOracleProvider.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
			// 
			// rdSQLProvider
			// 
			this.rdSQLProvider.AutoSize = true;
			this.rdSQLProvider.Location = new System.Drawing.Point(20, 19);
			this.rdSQLProvider.Name = "rdSQLProvider";
			this.rdSQLProvider.Size = new System.Drawing.Size(98, 16);
			this.rdSQLProvider.TabIndex = 2;
			this.rdSQLProvider.Text = "SQL Express";
			this.rdSQLProvider.UseVisualStyleBackColor = true;
			this.rdSQLProvider.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.rdSPCommand);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Controls.Add(this.rdSQLCommand);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(281, 3);
			this.groupBox2.Name = "groupBox2";
			this.tableLayoutPanel3.SetRowSpan(this.groupBox2, 2);
			this.groupBox2.Size = new System.Drawing.Size(272, 44);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Command Type";
			// 
			// rdSPCommand
			// 
			this.rdSPCommand.AutoSize = true;
			this.rdSPCommand.Location = new System.Drawing.Point(136, 19);
			this.rdSPCommand.Name = "rdSPCommand";
			this.rdSPCommand.Size = new System.Drawing.Size(121, 16);
			this.rdSPCommand.TabIndex = 1;
			this.rdSPCommand.Text = "Stored Procedure";
			this.rdSPCommand.UseVisualStyleBackColor = true;
			this.rdSPCommand.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
			// 
			// button1
			// 
			this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.button1.Location = new System.Drawing.Point(563, 13);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(92, 25);
			this.button1.TabIndex = 3;
			this.button1.Text = "DB 연결(&E)";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// rdSQLCommand
			// 
			this.rdSQLCommand.AutoSize = true;
			this.rdSQLCommand.Location = new System.Drawing.Point(11, 19);
			this.rdSQLCommand.Name = "rdSQLCommand";
			this.rdSQLCommand.Size = new System.Drawing.Size(110, 16);
			this.rdSQLCommand.TabIndex = 2;
			this.rdSQLCommand.Text = "SQL Command";
			this.rdSQLCommand.UseVisualStyleBackColor = true;
			this.rdSQLCommand.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
			// 
			// btnTest
			// 
			this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btnTest.Location = new System.Drawing.Point(821, 3);
			this.btnTest.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnTest.Name = "btnTest";
			this.btnTest.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnTest.Size = new System.Drawing.Size(88, 19);
			this.btnTest.TabIndex = 4;
			this.btnTest.Text = "&Test";
			this.btnTest.UseVisualStyleBackColor = true;
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// btnViewProduct
			// 
			this.btnViewProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btnViewProduct.Location = new System.Drawing.Point(821, 28);
			this.btnViewProduct.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnViewProduct.Name = "btnViewProduct";
			this.btnViewProduct.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnViewProduct.Size = new System.Drawing.Size(88, 19);
			this.btnViewProduct.TabIndex = 4;
			this.btnViewProduct.Text = "&1. Products";
			this.btnViewProduct.UseVisualStyleBackColor = true;
			this.btnViewProduct.Click += new System.EventHandler(this.btnViewProduct_Click);
			// 
			// btnTransfer
			// 
			this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btnTransfer.Location = new System.Drawing.Point(921, 3);
			this.btnTransfer.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnTransfer.Name = "btnTransfer";
			this.btnTransfer.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnTransfer.Size = new System.Drawing.Size(89, 19);
			this.btnTransfer.TabIndex = 4;
			this.btnTransfer.Text = "T&ransfer";
			this.btnTransfer.UseVisualStyleBackColor = true;
			this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
			// 
			// btnUpdateDataSet
			// 
			this.btnUpdateDataSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btnUpdateDataSet.Location = new System.Drawing.Point(921, 28);
			this.btnUpdateDataSet.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnUpdateDataSet.Name = "btnUpdateDataSet";
			this.btnUpdateDataSet.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnUpdateDataSet.Size = new System.Drawing.Size(89, 19);
			this.btnUpdateDataSet.TabIndex = 4;
			this.btnUpdateDataSet.Text = "&2. UpdateDS";
			this.btnUpdateDataSet.UseVisualStyleBackColor = true;
			this.btnUpdateDataSet.Click += new System.EventHandler(this.btnUpdateDataSet_Click);
			// 
			// btnConnectDB
			// 
			this.btnConnectDB.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnConnectDB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnConnectDB.Location = new System.Drawing.Point(701, 3);
			this.btnConnectDB.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnConnectDB.Name = "btnConnectDB";
			this.btnConnectDB.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnConnectDB.Size = new System.Drawing.Size(108, 19);
			this.btnConnectDB.TabIndex = 4;
			this.btnConnectDB.Text = "&DB 연결";
			this.btnConnectDB.UseVisualStyleBackColor = true;
			this.btnConnectDB.Click += new System.EventHandler(this.btnConnectDB_Click);
			// 
			// cboTables
			// 
			this.cboTables.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cboTables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboTables.FormattingEnabled = true;
			this.cboTables.Location = new System.Drawing.Point(559, 3);
			this.cboTables.Name = "cboTables";
			this.cboTables.Size = new System.Drawing.Size(133, 20);
			this.cboTables.TabIndex = 5;
			this.cboTables.SelectedIndexChanged += new System.EventHandler(this.cboTables_SelectedIndexChanged);
			// 
			// btn2Params
			// 
			this.btn2Params.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btn2Params.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btn2Params.Location = new System.Drawing.Point(701, 28);
			this.btn2Params.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btn2Params.Name = "btn2Params";
			this.btn2Params.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btn2Params.Size = new System.Drawing.Size(108, 19);
			this.btn2Params.TabIndex = 4;
			this.btn2Params.Text = "2 Out Param&s";
			this.btn2Params.UseVisualStyleBackColor = true;
			this.btn2Params.Click += new System.EventHandler(this.btn2Params_Click);
			// 
			// FrmBasic
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1016, 673);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.statusStrip1);
			this.Name = "FrmBasic";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FrmBasic";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGrid)).EndInit();
			this.panel1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel statusBar;
		private Infragistics.Win.UltraWinGrid.UltraGrid xGrid;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSqlCommand;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Button btnConnectDB;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rdOracleProvider;
		private System.Windows.Forms.RadioButton rdSQLProvider;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rdSPCommand;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.RadioButton rdSQLCommand;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtParam1;
		private System.Windows.Forms.TextBox txtParam2;
		private System.Windows.Forms.Button btnTest;
		private System.Windows.Forms.Button btnViewProduct;
		private System.Windows.Forms.Button btnTransfer;
		private System.Windows.Forms.Button btnUpdateDataSet;
		private System.Windows.Forms.Button btn2Params;
		private System.Windows.Forms.ComboBox cboTables;
	}
}

