﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Common;
using System.Diagnostics;

using iDoIt.Data;
using iDASiT.TrustCore.Common;
using iDASiT.TrustCore.Code;
using iDASiT.TrustCore.Data;

namespace DA02_WrappingClass
{
	public partial class FrmWrapping : Form
	{
		private int _id = 1;

		public FrmWrapping()
		{
			InitializeComponent();
		}

		private void FrmWrapping_Load(object sender, EventArgs e)
		{
			rdOracleProvider.Checked = true;
			rdSqlCreate.Checked = true;
		}

		private void btnCallBasic_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnConnectDB_Click(object sender, EventArgs e)
		{
			if (rdSqlCreate.Checked == true)
			{
				Address address = new Address();

				address.AddressId = txtID.Text;
				address.AddressDesc = string.Format("{0} 번지 Address", txtID.Text);
				address.AddressSeq = 1;

				LotDataItem ldi = new LotDataItem();

				ldi.OperationId = "OP001";
				ldi.LotId = "LOT001";
				ldi.WhenUpdated = DateTime.Now;
				ldi.WorkerId = "WORKER";
				ldi.PlantId = "PLANT01";
				ldi.DataParamKey = new string[] { "----DPkey01----", "----DPkey02----", "----DPkey03----" };
				ldi.LotDataItemSeq = 2;
				ldi.DataItemKey = "DI001";

				ITrustCoreBase trustCore = ldi as ITrustCoreBase;

				int affectedRow = -1;
				
				if (trustCore != null)
					affectedRow = trustCore.Create();

				txtSqlCommand.Text += string.Format("affectedRow : {0}\r\n", affectedRow);
			}
			else
			{
				string addressId = txtID.Text;//(txtID.Text == string.Empty ? "0001" : txtID.Text);

				Address address = new Address();

				address.AddressId = addressId;

				ITrustCoreBase tcAddress = address as ITrustCoreBase;
				DataSet dsResult = null;

				if (tcAddress != null)
					dsResult = tcAddress.GetDataSet();

				bindingData(dsResult.Tables[0]);
			}

		}

		#region Message
		private void clearMessage()
		{
			statusBar.Text = string.Empty;

			bindingData(null);

			Update();
		}

		private void errorMessage(Exception ex)
		{
			statusBar.Text = ex.Message;
		}

		private void successConnection()
		{
			statusBar.Text = "Connection successful";
		}
		#endregion

		#region Binding Data
		private void bindingData(DataTable table)
		{
			xGrid.DataSource = table;
			xGrid.DataBind();

			if (table != null)
				_id = table.Rows.Count;

			txtID.Text = string.Format("{0:0000}", _id);
		}
		#endregion

		private Database getDatabase()
		{
			string databaseName = rdSQLProvider.Checked == true ? "csSQL" : "csOracle";

			return DatabaseFactory.CreateDatabase(databaseName);
		}
	}
}