﻿namespace DA02_WrappingClass
{
	partial class FrmWrapping
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.statusBar = new System.Windows.Forms.ToolStripStatusLabel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.txtSqlCommand = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDesc = new System.Windows.Forms.TextBox();
			this.txtID = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.btnCallBasic = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rdOracleProvider = new System.Windows.Forms.RadioButton();
			this.rdSQLProvider = new System.Windows.Forms.RadioButton();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.rdSqlGet = new System.Windows.Forms.RadioButton();
			this.button1 = new System.Windows.Forms.Button();
			this.rdSqlCreate = new System.Windows.Forms.RadioButton();
			this.btnConnectDB = new System.Windows.Forms.Button();
			this.xGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.statusStrip1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBar});
			this.statusStrip1.Location = new System.Drawing.Point(0, 651);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1016, 22);
			this.statusStrip1.TabIndex = 0;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// statusBar
			// 
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(122, 17);
			this.statusBar.Text = "toolStripStatusLabel1";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.xGrid, 0, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 651);
			this.tableLayoutPanel1.TabIndex = 7;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 4;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.txtSqlCommand, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.label3, 2, 1);
			this.tableLayoutPanel2.Controls.Add(this.txtDesc, 3, 1);
			this.tableLayoutPanel2.Controls.Add(this.txtID, 3, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 53);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1010, 54);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 3);
			this.label1.Margin = new System.Windows.Forms.Padding(3);
			this.label1.Name = "label1";
			this.tableLayoutPanel2.SetRowSpan(this.label1, 2);
			this.label1.Size = new System.Drawing.Size(114, 48);
			this.label1.TabIndex = 0;
			this.label1.Text = "SQL command : ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtSqlCommand
			// 
			this.txtSqlCommand.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtSqlCommand.Location = new System.Drawing.Point(120, 0);
			this.txtSqlCommand.Margin = new System.Windows.Forms.Padding(0);
			this.txtSqlCommand.Multiline = true;
			this.txtSqlCommand.Name = "txtSqlCommand";
			this.tableLayoutPanel2.SetRowSpan(this.txtSqlCommand, 2);
			this.txtSqlCommand.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtSqlCommand.Size = new System.Drawing.Size(620, 54);
			this.txtSqlCommand.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(743, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 27);
			this.label2.TabIndex = 2;
			this.label2.Text = "ID";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(743, 27);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 27);
			this.label3.TabIndex = 3;
			this.label3.Text = "Desc";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtDesc
			// 
			this.txtDesc.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtDesc.Location = new System.Drawing.Point(813, 30);
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Size = new System.Drawing.Size(194, 21);
			this.txtDesc.TabIndex = 4;
			// 
			// txtID
			// 
			this.txtID.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtID.Location = new System.Drawing.Point(813, 3);
			this.txtID.Name = "txtID";
			this.txtID.Size = new System.Drawing.Size(194, 21);
			this.txtID.TabIndex = 5;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 6;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
			this.tableLayoutPanel3.Controls.Add(this.btnCallBasic, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.groupBox2, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.btnConnectDB, 3, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(1016, 50);
			this.tableLayoutPanel3.TabIndex = 1;
			// 
			// btnCallBasic
			// 
			this.btnCallBasic.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnCallBasic.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCallBasic.Location = new System.Drawing.Point(562, 28);
			this.btnCallBasic.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnCallBasic.Name = "btnCallBasic";
			this.btnCallBasic.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnCallBasic.Size = new System.Drawing.Size(127, 19);
			this.btnCallBasic.TabIndex = 5;
			this.btnCallBasic.Text = "Call Basic";
			this.btnCallBasic.UseVisualStyleBackColor = true;
			this.btnCallBasic.Click += new System.EventHandler(this.btnCallBasic_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rdOracleProvider);
			this.groupBox1.Controls.Add(this.rdSQLProvider);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.tableLayoutPanel3.SetRowSpan(this.groupBox1, 2);
			this.groupBox1.Size = new System.Drawing.Size(272, 44);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "DB Provider";
			// 
			// rdOracleProvider
			// 
			this.rdOracleProvider.AutoSize = true;
			this.rdOracleProvider.Location = new System.Drawing.Point(185, 19);
			this.rdOracleProvider.Name = "rdOracleProvider";
			this.rdOracleProvider.Size = new System.Drawing.Size(60, 16);
			this.rdOracleProvider.TabIndex = 1;
			this.rdOracleProvider.Text = "Oracle";
			this.rdOracleProvider.UseVisualStyleBackColor = true;
			// 
			// rdSQLProvider
			// 
			this.rdSQLProvider.AutoSize = true;
			this.rdSQLProvider.Location = new System.Drawing.Point(20, 19);
			this.rdSQLProvider.Name = "rdSQLProvider";
			this.rdSQLProvider.Size = new System.Drawing.Size(98, 16);
			this.rdSQLProvider.TabIndex = 2;
			this.rdSQLProvider.Text = "SQL Express";
			this.rdSQLProvider.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.rdSqlGet);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Controls.Add(this.rdSqlCreate);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(281, 3);
			this.groupBox2.Name = "groupBox2";
			this.tableLayoutPanel3.SetRowSpan(this.groupBox2, 2);
			this.groupBox2.Size = new System.Drawing.Size(272, 44);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Command Type";
			// 
			// rdSqlGet
			// 
			this.rdSqlGet.AutoSize = true;
			this.rdSqlGet.Location = new System.Drawing.Point(176, 19);
			this.rdSqlGet.Name = "rdSqlGet";
			this.rdSqlGet.Size = new System.Drawing.Size(90, 16);
			this.rdSqlGet.TabIndex = 1;
			this.rdSqlGet.Text = "SP Get type";
			this.rdSqlGet.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.button1.Location = new System.Drawing.Point(563, 13);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(92, 25);
			this.button1.TabIndex = 3;
			this.button1.Text = "DB 연결(&E)";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// rdSqlCreate
			// 
			this.rdSqlCreate.AutoSize = true;
			this.rdSqlCreate.Location = new System.Drawing.Point(11, 19);
			this.rdSqlCreate.Name = "rdSqlCreate";
			this.rdSqlCreate.Size = new System.Drawing.Size(108, 16);
			this.rdSqlCreate.TabIndex = 2;
			this.rdSqlCreate.Text = "SP Create type";
			this.rdSqlCreate.UseVisualStyleBackColor = true;
			// 
			// btnConnectDB
			// 
			this.btnConnectDB.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnConnectDB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnConnectDB.Location = new System.Drawing.Point(701, 3);
			this.btnConnectDB.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
			this.btnConnectDB.Name = "btnConnectDB";
			this.btnConnectDB.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.btnConnectDB.Size = new System.Drawing.Size(108, 19);
			this.btnConnectDB.TabIndex = 4;
			this.btnConnectDB.Text = "&DB 연결";
			this.btnConnectDB.UseVisualStyleBackColor = true;
			this.btnConnectDB.Click += new System.EventHandler(this.btnConnectDB_Click);
			// 
			// xGrid
			// 
			appearance13.BackColor = System.Drawing.SystemColors.Window;
			appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.xGrid.DisplayLayout.Appearance = appearance13;
			this.xGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance14.BorderColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.GroupByBox.Appearance = appearance14;
			appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
			this.xGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance16.BackColor2 = System.Drawing.SystemColors.Control;
			appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
			this.xGrid.DisplayLayout.MaxColScrollRegions = 1;
			this.xGrid.DisplayLayout.MaxRowScrollRegions = 1;
			appearance17.BackColor = System.Drawing.SystemColors.Window;
			appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xGrid.DisplayLayout.Override.ActiveCellAppearance = appearance17;
			appearance18.BackColor = System.Drawing.SystemColors.Highlight;
			appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xGrid.DisplayLayout.Override.ActiveRowAppearance = appearance18;
			this.xGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.xGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance19.BackColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.Override.CardAreaAppearance = appearance19;
			appearance20.BorderColor = System.Drawing.Color.Silver;
			appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xGrid.DisplayLayout.Override.CellAppearance = appearance20;
			this.xGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xGrid.DisplayLayout.Override.CellPadding = 0;
			appearance21.BackColor = System.Drawing.SystemColors.Control;
			appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance21.BorderColor = System.Drawing.SystemColors.Window;
			this.xGrid.DisplayLayout.Override.GroupByRowAppearance = appearance21;
			appearance22.TextHAlignAsString = "Left";
			this.xGrid.DisplayLayout.Override.HeaderAppearance = appearance22;
			this.xGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance23.BackColor = System.Drawing.SystemColors.Window;
			appearance23.BorderColor = System.Drawing.Color.Silver;
			this.xGrid.DisplayLayout.Override.RowAppearance = appearance23;
			this.xGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
			this.xGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xGrid.Location = new System.Drawing.Point(3, 113);
			this.xGrid.Name = "xGrid";
			this.xGrid.Size = new System.Drawing.Size(1010, 535);
			this.xGrid.TabIndex = 2;
			this.xGrid.Text = "ultraGrid1";
			// 
			// FrmWrapping
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1016, 673);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.statusStrip1);
			this.Name = "FrmWrapping";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FrmWrapping";
			this.Load += new System.EventHandler(this.FrmWrapping_Load);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel statusBar;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSqlCommand;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtDesc;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rdOracleProvider;
		private System.Windows.Forms.RadioButton rdSQLProvider;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rdSqlGet;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.RadioButton rdSqlCreate;
		private System.Windows.Forms.Button btnConnectDB;
		private Infragistics.Win.UltraWinGrid.UltraGrid xGrid;
		private System.Windows.Forms.TextBox txtID;
		private System.Windows.Forms.Button btnCallBasic;
	}
}

