﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using iDoIt.Data.Configuration;
using iDASiT.TrustCore.Common;

namespace iDASiT.TrustCore.Code
{
	public class Address : TrustCoreBase, IAddress
	{
		public Address()
		{

		}

		#region IAddress 멤버

		[ColumnInfo(1, IsPrimaryKey=true)]
		public string AddressId
		{
			get
			{
				return base.CodeId;
			}
			set
			{
				base.CodeId = value;
			}
		}

		[ColumnInfo(2, Readable=true, Deletable=false)]
		public string AddressDesc
		{
			get
			{
				return base.CodeDesc;
			}
			set
			{
				base.CodeDesc = value;
			}
		}

		[ColumnInfo(3, Readable=true, Deletable=false)]
		public int AddressSeq
		{
			get
			{
				return base.CodeSeq;
			}
			set
			{
				base.CodeSeq = value;
			}
		}

		private int _subAddr;
		//[ColumnInfo(5, Name="SubAddress", Readable=false, Creatable=false, Deletable=false)]
		public int SubAddr
		{
			get
			{
				return _subAddr;
			}
			set
			{
				_subAddr = value;
			}
		}
		#endregion
	}
}
