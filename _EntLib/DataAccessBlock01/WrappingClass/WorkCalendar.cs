using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml.Serialization;

using iDASiT.TrustCore.Common;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Region
{
	public class WorkCalendar : TrustCoreBase
	{
		private string[] _workDayId = new string[] { };
		private DateTime _startDate = DateTime.Now;
		private int _startDayOfWeek;
		private DateTime _endDate;
		private Dictionary<DayOfWeek, double> _workRatios = new Dictionary<DayOfWeek, double>();

		public WorkCalendar()
		{
			_workRatios.Clear();

			_workRatios.Add(DayOfWeek.Sunday, 1.0);
			_workRatios.Add(DayOfWeek.Monday, 1.0);
			_workRatios.Add(DayOfWeek.Tuesday, 1.0);
			_workRatios.Add(DayOfWeek.Wednesday, 1.0);
			_workRatios.Add(DayOfWeek.Thursday, 1.0);
			_workRatios.Add(DayOfWeek.Friday, 1.0);
			_workRatios.Add(DayOfWeek.Saturday, 1.0);
		}

		public double GetWorkRatio(DayOfWeek dayOfWeek)
		{
			return (double)_workRatios[dayOfWeek];
		}

		#region IWorkCalendar ���

		[ColumnInfo(1, IsPrimaryKey=true)]
		public string WorkCalendarId
		{
			get
			{
				return base.CodeId;
			}
			set
			{
				base.CodeId = value;
			}
		}

		[ColumnInfo(2)]
		public string WorkCalendarDesc
		{
			get
			{
				return base.CodeDesc;
			}
			set
			{
				base.CodeDesc = value;
			}
		}


		[ColumnInfo(3)]
		public int WorkCalendarSeq
		{
			get
			{
				return base.CodeSeq;
			}
			set
			{
				base.CodeSeq = value;
			}
		}

		[ColumnInfo(4)]
		public DateTime EndDate
		{
			get
			{
				return _endDate;
			}
			set
			{
				_endDate = value;
			}
		}


		[ColumnInfo(5)]
		public int EndDay
		{
			get
			{
				return _endDate.Day;
			}
		}

		[ColumnInfo(6)]
		public int EndMonth
		{
			get
			{
				return _endDate.Month;
			}
		}

		[ColumnInfo(7)]
		public int EndYear
		{
			get
			{
				return _endDate.Year;
			}
		}

		[ColumnInfo(8)]
		public DateTime StartDate
		{
			get
			{
				return _startDate;
			}
			set
			{
				_startDate = value;
			}
		}

		[ColumnInfo(9)]
		public int StartDay
		{
			get
			{
				return StartDate.Day;
			}
		}

		[ColumnInfo(10)]
		public int StartDayOfWeek
		{
			get
			{
				return _startDayOfWeek;
			}
			set
			{
				_startDayOfWeek = value;
			}
		}

		[ColumnInfo(11)]
		public string StartDayOfWeekDesc
		{
			get
			{
				return ((DayOfWeek)StartDayOfWeek).ToString();
			}
		}

		[ColumnInfo(12)]
		public int StartMonth
		{
			get
			{
				return StartDate.Month;
			}
		}

		[ColumnInfo(13)]
		public int StartYear
		{
			get
			{
				return StartDate.Year;
			}
		}

		[ColumnInfo(14)]
		public double WorkRatioSunday
		{
			get
			{
				return _workRatios[DayOfWeek.Sunday];
			}
			set
			{
				_workRatios[DayOfWeek.Sunday] = value;
			}
		}

		[ColumnInfo(15)]
		public double WorkRatioMonday
		{
			get
			{
				return _workRatios[DayOfWeek.Monday];
			}
			set
			{
				_workRatios[DayOfWeek.Monday] = value;
			}
		}

		[ColumnInfo(16)]
		public double WorkRatioTuesday
		{
			get
			{
				return _workRatios[DayOfWeek.Tuesday];
			}
			set
			{
				_workRatios[DayOfWeek.Tuesday] = value;
			}
		}

		[ColumnInfo(17)]
		public double WorkRatioWednesday
		{
			get
			{
				return _workRatios[DayOfWeek.Wednesday];
			}
			set
			{
				_workRatios[DayOfWeek.Wednesday] = value;
			}
		}

		[ColumnInfo(18)]
		public double WorkRatioThursday
		{
			get
			{
				return _workRatios[DayOfWeek.Thursday];
			}
			set
			{
				_workRatios[DayOfWeek.Thursday] = value;
			}
		}

		[ColumnInfo(19)]
		public double WorkRatioFriday
		{
			get
			{
				return _workRatios[DayOfWeek.Friday];
			}
			set
			{
				_workRatios[DayOfWeek.Friday] = value;
			}
		}

		[ColumnInfo(20)]
		public double WorkRatioSaturday
		{
			get
			{
				return _workRatios[DayOfWeek.Saturday];
			}
			set
			{
				_workRatios[DayOfWeek.Saturday] = value;
			}
		}

		#endregion

		public int WorkDayCount
		{
			get
			{
				return _workDayId.GetLength(0);
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] WorkDayId
		{
			get
			{
				return _workDayId;
			}
			set
			{
				_workDayId = value;
			}
		}
	}
}
