﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace iDASiT.TrustCore.Common
{
	public interface ITrustCore : ICodeElement
	{
		DataSet GetDataSet();

		int Create(ITrustCore tCore);

		int Update();

		int Delete();
	}	
}
