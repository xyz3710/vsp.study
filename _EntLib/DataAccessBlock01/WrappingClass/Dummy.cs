using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using iDoIt.Data.Configuration;
using iDASiT.TrustCore.Common;

namespace iDASiT.TrustCore.Code
{
	public class Test : TrustCoreBase
	{
		public Test()
		{

		}

		#region ITest ���

		public string TestId
		{
			get
			{
				return base.CodeId;
			}
			set
			{
				base.CodeId = value;
			}
		}

		public string TestDesc
		{
			get
			{
				return base.CodeDesc;
			}
			set
			{
				base.CodeDesc = value;
			}
		}

		public int TestSeq
		{
			get
			{
				return base.CodeSeq;
			}
			set
			{
				base.CodeSeq = value;
			}
		}
		#endregion
	}
}
