﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace iDASiT.TrustCore.Common
{
	public interface ITrustCoreBase : ICodeElement
	{
//		DataSet GetDataSet(ITrustCoreBase trustCore);
//
//		int Create(ITrustCoreBase trustCore);
//
//		int Update(ITrustCoreBase trustCore);
//
//		int Delete(ITrustCoreBase trustCore);

		DataSet GetDataSet();
		
		int Create();
		
		int Update();
		
		int Delete();

		T GetThis<T>() where T : ITrustCoreBase;
	}	
}
