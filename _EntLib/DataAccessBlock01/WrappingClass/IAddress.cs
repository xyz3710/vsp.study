using System;
using System.Collections.Generic;
using System.Text;
using iDoIt.Data.Configuration;
using System.Data;

namespace iDASiT.TrustCore.Code
{
	public interface IAddress
	{
		string AddressId
		{
			get;
			set;
		}

		string AddressDesc
		{
			get;
			set;
		}

		int AddressSeq
		{
			get;
			set;
		}

		int SubAddr
		{
			get;
			set;
		}

	}

}
