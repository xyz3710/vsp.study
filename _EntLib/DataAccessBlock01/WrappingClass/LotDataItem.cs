using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Xml.Serialization;

using iDASiT.TrustCore.Common;
using iDASiT.TrustCore.Process;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Data
{
	public class LotDataItem : DataItem
	{
		private string _lotId;
		private string _operatonId;
		private DateTime _whenUpdated;
		private string _workerId;
		private string _plantId;
		private string[] _dataParamKey;
		private int _lotDataItemSeq;

		public LotDataItem()
		{

		}

		#region ILotDataItem ���

		[ColumnInfo(1, IsPrimaryKey=true)]
		public string DataItemKey
		{
			get
			{
				return base.DataItemId;
			}
			set
			{
				base.DataItemId = value;
			}
		}

		[ColumnInfo(2, IsPrimaryKey=true)]
		public int LotDataItemSeq
		{
			get
			{
				return _lotDataItemSeq;
			}
			set
			{
				_lotDataItemSeq = value;
			}
		}

		[ColumnInfo(3, IsPrimaryKey=true)]
		public string LotId
		{
			get
			{
				return _lotId;
			}
			set
			{
				_lotId = value;
			}
		}

		[ColumnInfo(4, IsPrimaryKey=true)]
		public string OperationId
		{
			get
			{
				return _operatonId;
			}
			set
			{
				_operatonId = value;
			}
		}

		[ColumnInfo(5, IsPrimaryKey=true)]
		public string PlantId
		{
			get
			{
				return _plantId;
			}
			set
			{
				_plantId = value;
			}
		}

		[ColumnInfo(6)]
		public DateTime WhenUpdated
		{
			get
			{
				return _whenUpdated;
			}
			set
			{
				_whenUpdated = value;
			}
		}

		[ColumnInfo(7)]
		public string WorkerId
		{
			get
			{
				return _workerId;
			}
			set
			{
				_workerId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		[ColumnInfo(8)]
		public string[] DataParamKey
		{
			get
			{
				return _dataParamKey;
			}
			set
			{
				_dataParamKey = value;
			}
		}

		#endregion
	}
}
