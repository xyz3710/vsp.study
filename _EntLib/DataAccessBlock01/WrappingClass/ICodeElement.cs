﻿using System;

namespace iDASiT.TrustCore.Common
{
	public interface ICodeElement
	{
		string CodeId
		{
			get;
			set;
		}

		string CodeDesc
		{
			get;
			set;
		}

		int CodeSeq
		{
			get;
			set;
		}
	}
}
