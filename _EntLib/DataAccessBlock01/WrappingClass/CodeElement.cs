﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.TrustCore.Common
{
    [Serializable]
    public class CodeElement : ICodeElement
    {
		#region Fields

		private string _codeId;
		private string _codeDesc;
		private int _codeSeq;

		#endregion

		#region Constructor

		public CodeElement(string codeId, string codeDesc, int codeSeq)
		{
			_codeId = codeId.Trim().ToUpper();
			_codeDesc = codeDesc;
			_codeSeq = codeSeq;
		}

		public CodeElement(string codeId, string codeDesc)
			: this(codeId, codeDesc, 0)
		{
		}

		public CodeElement()
			:this(string.Empty, string.Empty, 0)
		{
		}

		#endregion

		#region ICodeElement 멤버
		
		protected string CodeId
		{
			get
			{
				return _codeId;
			}
			set
			{
				_codeId = value.Trim().ToUpper();
			}
		}

		protected string CodeDesc
		{
			get
			{
				return _codeDesc;
			}
			set
			{
				_codeDesc = value;
			}
		}

		protected int CodeSeq
		{
			get
			{
				return _codeSeq;
			}
			set
			{
				_codeSeq = value;
			}
		}

		#endregion

		#region ICodeElement 멤버
		
		string ICodeElement.CodeId
		{
			get
			{
				return _codeId;
			}
			set
			{
				_codeId = value.Trim().ToUpper();
			}
		}

		string ICodeElement.CodeDesc
		{
			get
			{
				return _codeDesc;
			}
			set
			{
				_codeDesc = value;
			}
		}

		int ICodeElement.CodeSeq
		{
			get
			{
				return _codeSeq;
			}
			set
			{
				_codeSeq = value;
			}
		}

		#endregion
	}
}
