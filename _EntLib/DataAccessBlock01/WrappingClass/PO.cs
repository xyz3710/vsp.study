using System;
using System.Collections.Generic;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using iDASiT.TrustCore.Common;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Process
{
	public class PO : TrustCoreBase
	{
		private string[] workFlowId = new string[] { };
		private string[] reworkWorkFlowId = new string[] { };
		private string[] repairWorkFlowIds = new string[] { };
		private string _productSpecId;
		private string _productSpecRev;

		public PO()
		{

		}

		#region IPO ���

		[ColumnInfo(1, IsPrimaryKey=true)]
		public string POId
		{
			get
			{
				return base.CodeId;
			}
			set
			{
				base.CodeId = value;
			}
		}

		[ColumnInfo(2)]
		public string PODesc
		{
			get
			{
				return base.CodeDesc;
			}
			set
			{
				base.CodeDesc = value;
			}
		}

		[ColumnInfo(3)]
		public int POSeq
		{
			get
			{
				return base.CodeSeq;
			}
			set
			{
				base.CodeSeq = value;
			}
		}

		#endregion

		[ColumnInfo(4)]
		public double PORev
		{
			get
			{
				return Convert.ToDouble(base.CodeSeq);
			}
			set
			{
				base.CodeSeq = Convert.ToInt16(value);
			}
		}

		[ColumnInfo(5)]
		public string ProductSpecId
		{
			get
			{
				return _productSpecId;
			}
			set
			{
				_productSpecId = value;
			}
		}

		[ColumnInfo(6)]
		public string ProductSpecRev
		{
			get
			{
				return _productSpecRev;
			}
			set
			{
				_productSpecRev = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] WorkFlowId
		{
			get
			{
				return workFlowId;
			}
			set
			{
				workFlowId = value;
			}
		}

		public int WorkFlowCount
		{
			get
			{
				return workFlowId.GetLength(0);
			}
		}

		public string[] ReworkWorkFlowId
		{
			get
			{
				return reworkWorkFlowId;
			}
			set
			{
				reworkWorkFlowId = value;
			}
		}

		public int ReworkWorkFlowCount
		{
			get
			{
				return reworkWorkFlowId.GetLength(0);
			}
		}

		public int RepairWorkFlowCount
		{
			get
			{
				return repairWorkFlowIds.GetLength(0);
			}
		}

		public string[] RepairWorkFlowId
		{
			get
			{
				return repairWorkFlowIds;
			}
			set
			{
				repairWorkFlowIds = value;
			}
		}

	}
}
