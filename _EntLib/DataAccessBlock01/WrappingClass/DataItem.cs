using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

using iDASiT.TrustCore.Common;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Data
{
	public class DataItem : TrustCoreBase
	{
		private CodeElement _code = new CodeElement();
		private string[] dataParamId = new string[0];

		public DataItem()
		{

		}

		#region IDataItem ���

		[ColumnInfo(103, Inheritable=false)]
		public int DataItemSeq
		{
			get
			{
				return (_code as ICodeElement).CodeSeq;
			}
			set
			{
				(_code as ICodeElement).CodeSeq = value;
			}
		}

		[ColumnInfo(101, IsPrimaryKey=true, Inheritable=false)]
		public string DataItemId
		{
			get
			{
				return (_code as ICodeElement).CodeId;
			}
			set
			{
				(_code as ICodeElement).CodeId = value;
			}
		}

		[ColumnInfo(102, Inheritable=false)]
		public string DataItemDesc
		{
			get
			{
				return (_code as ICodeElement).CodeDesc;
			}
			set
			{
				(_code as ICodeElement).CodeDesc = value;
			}
		}
		#endregion

		public int DataParamCount
		{
			get
			{
				return dataParamId.GetLength(0);
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] DataParamId
		{
			get
			{
				return dataParamId;
			}
			set
			{
				dataParamId = value;
			}
		}

		public void CopyTo(DataItem dataItem)
		{
			if (dataItem == null)
				return;

			dataItem.DataItemId = DataItemId;
			dataItem.DataItemSeq = DataItemSeq;
			dataItem.DataItemDesc = DataItemDesc;

			dataItem.DataParamId = new string[DataParamCount];
			DataParamId.CopyTo(dataItem.DataParamId, 0);
		}
	}
}
