﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Common
{
	public class TrustCore : CodeElement, ITrustCore
	{

		public TrustCore()
		{

		}

		#region ITrustCore 멤버

		public DataSet GetDataSet()
		{
			Console.WriteLine("Select method call");
			return new DataSet();
		}

		public int Create(ITrustCore trustCore)
		{
			Console.WriteLine("\nCreate method with ");
			Type tCore = trustCore.GetType();

			foreach (PropertyInfo property in tCore.GetProperties())
			{
				foreach (Attribute attr in property.GetCustomAttributes(false))
				{
					ColumnInfoAttribute colInfo = attr as ColumnInfoAttribute;

					if (colInfo != null)
					{
						Console.WriteLine("Seq\t\t: {0}\nName\t\t: {1}\nDbType\t\t: {2}",
							colInfo.Seq,
							colInfo.Name == string.Empty ? property.Name : colInfo.Name,
							colInfo.DbType == DbType.AnsiString ? GetDbType(property.PropertyType) : colInfo.DbType);

						Console.WriteLine();
					}
				}
			}

			return 0;
		}

		public int Update()
		{
			Console.WriteLine("Update method");
			return 0;
		}

		public int Delete()
		{
			Console.WriteLine("Delete method");
			return 0;
		}

		#endregion

		public static DbType GetDbType(Type type)
		{
			DbType dbType = DbType.Object;

			if (type == typeof(string))
				dbType = DbType.String;

			if (type == typeof(int))
				dbType = DbType.Int32;

			if (type == typeof(double))
				dbType = DbType.Decimal;

			if (type == typeof(DateTime))
				dbType = DbType.DateTime;

			return dbType;
		}
	}
}
