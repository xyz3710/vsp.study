using System;
using System.Collections.Generic;
using System.Text;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Common
{
	public class Subcontract : BusinessPartner
	{
		public Subcontract()
		{

		}

		#region ISubcontract ���

		[ColumnInfo(2)]
		public string SubcontractDesc
		{
			get
			{
				return base.CodeDesc;
			}
			set
			{
				base.CodeDesc = value;
			}
		}

		[ColumnInfo(1, IsPrimaryKey=true)]
		public string SubcontractId
		{
			get
			{
				return base.CodeId;
			}
			set
			{
				base.CodeId = value;
			}
		}

		[ColumnInfo(3)]
		public int SubcontractSeq
		{
			get
			{
				return base.CodeSeq;
			}
			set
			{
				base.CodeSeq = value;
			}
		}

		#endregion
	}
}
