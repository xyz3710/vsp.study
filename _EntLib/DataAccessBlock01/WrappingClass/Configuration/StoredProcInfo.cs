/**********************************************************************************************************************/
/*	Domain		:	iDASiT.TrustCore.Common
/*	Creator		:	Kim Ki Won(iDASiT Inc.)
/*	Create		:	2007년 7월 4일 수요일 오후 4:09:42a
/*	Purpose		:	Stored Procdure information
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.TrustCore.Common
{
	/// <summary>
	/// 
	/// </summary>
	public class StoredProcInfo
	{
		/// <summary>
		/// 
		/// </summary>
		protected readonly string PREFIX_FORMAT = "UP_{0}_{1}";

		#region Members
		private string _storedProcedureName;
		private string _nameSpace;
		private int _maxArrayLength;
		private ParamInfo _paramInfo;
		#endregion

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the StoredProcInfo class.
		/// </summary>
		/// <param name="nameSpace"></param>
		public StoredProcInfo(string nameSpace)
		{
			ParamInfo = new ParamInfo();
			_maxArrayLength = 1;
			_nameSpace = nameSpace;
		}


		/// <summary>
        /// Initializes a new instance of the StoredProcInfo class.
        /// </summary>
        public StoredProcInfo()
			: this(string.Empty)
        {
		}
        #endregion

		#region Properties

		/// <summary>
		/// Get or set StoredProcedureName.
		/// </summary>
		public String StoredProcedureName
		{
			get
			{
				string[] nameSpace = _nameSpace.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
				String ret = String.Empty;

				if (nameSpace.Length > 0)
				{
					string factory = nameSpace[nameSpace.Length - 1];

					if (factory == "Code")
						ret = string.Format(PREFIX_FORMAT, "CD", _storedProcedureName);
					else
						ret = string.Format(PREFIX_FORMAT, factory.Remove(2).ToUpper(), _storedProcedureName);
				}
				else
					ret = string.Format(PREFIX_FORMAT, string.Empty, _storedProcedureName);

				return ret;
			}
			set
			{
				_storedProcedureName = value;
			}
		}

		/// <summary>
		/// Get or set Max array length in vale.
		/// </summary>
		public int MaxArrayLength
		{
			get
			{
				return _maxArrayLength;
			}
			set
			{
				_maxArrayLength = value;
			}
		}

		/// <summary>
		/// Get or set ParamInfo.
		/// </summary>
		public ParamInfo ParamInfo
		{
			get
			{
				return _paramInfo;
			}
			set
			{
				_paramInfo = value;
			}
		}
		#endregion

//		private string getTableAlias()
//		{
//			string tableName = spi.ClassName;
//			string tableAlias = tableName.Length < 3 ? tableName.Substring(0, 2) + "A" : tableName.Substring(0, 3);
//
//			return tableAlias;
//		}
	}
}
