﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.TrustCore.Common
/*	Creator		:	Kim Ki Won(iDASiT Inc.)
/*	Create		:	2007년 6월 28일 목요일 오후 4:09:42a
/*	Purpose		:	Set table column information
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	Column의 CRUD 사용은 Create = true, Update = true, Read(Select) = false, Delete = false로 되어있다.
 *					지정하지 않으면 Create, Select SP에서는 Primary Key 값만 넘어간다(PK일 경우 CRUD 모두 true)
/**********************************************************************************************************************/
using System;
using System.Data;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

namespace iDASiT.TrustCore.Common
{
	/// <summary>
	/// Attribute for column informations within properties.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple=false, Inherited=false)]
	public class ColumnInfoAttribute : Attribute
	{
		#region Fields
		private short _seq = -1;
		private string _name = string.Empty;
		private DbType _dbType = DbType.AnsiString;
		private bool _isPrimaryKey = false;
		private bool _creatable = true;
		private bool _readable = false;
		private bool _updatable = true;
		private bool _deletable = false;
		private bool _inheritable = true;
		#endregion

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the ColumnInfoAttribute class.
		/// </summary>
		/// <param name="seq">Column sequence(start of 0)</param>
		public ColumnInfoAttribute(short seq)
		{
			_seq = seq;
		}

		/// <summary>
		/// Initializes a new instance of the ColumnInfoAttribute class.
		/// </summary>
		/// <param name="seq">Column sequence</param>
		/// <param name="name">Column name</param>
		/// <param name="dbType">Data type</param>
		/// <param name="isPrimaryKey">Is primary key column</param>
		public ColumnInfoAttribute(short seq, string name, DbType dbType, bool isPrimaryKey)
		{
			_seq = seq;
			_name = name;
			_dbType = dbType;
			_isPrimaryKey = isPrimaryKey;
		}

		/// <summary>
		/// Initializes a new instance of the ColumnInfoAttribute class.
		/// </summary>
		/// <param name="seq">Column sequence</param>
		/// <param name="name">Column name</param>
		/// <param name="dbType">Data type</param>
		/// <param name="isPrimaryKey">is primary key</param>
		/// <param name="creatable">creatable</param>
		/// <param name="readable">getable</param>
		/// <param name="updatable">updatable</param>
		/// <param name="deletable">deletable</param>
		public ColumnInfoAttribute(short seq, string name, DbType dbType, bool isPrimaryKey,
			bool creatable, bool readable, bool updatable, bool deletable)
		{
			_seq = seq;
			_name = name;
			_dbType = dbType;
			_isPrimaryKey = isPrimaryKey;
			_creatable = creatable;
			_readable = readable;
			_updatable = updatable;
			_deletable = deletable;
		}

		#endregion

		#region Properties
		/// <summary>
		/// Get or set Column sequence.
		/// </summary>
		public short Seq
		{
			get
			{
				return _seq;
			}
			set
			{
				_seq = value;
			}
		}

		/// <summary>
		/// Get or set Column name.
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Get or set data type.
		/// </summary>
		public DbType DbType
		{
			get
			{
				return _dbType;
			}
			set
			{
				_dbType = value;
			}
		}

		/// <summary>
		/// Is primary key column(default : false).
		/// </summary>
		public Boolean IsPrimaryKey
		{
			get
			{
				return _isPrimaryKey;
			}
			set
			{
				_isPrimaryKey = value;
			}
		}

		/// <summary>
		/// Creatable column(default : true).
		/// </summary>
		public Boolean Creatable
		{
			get
			{
				_creatable = _isPrimaryKey == true ? true : _creatable;

				return _creatable;
			}
			set
			{
				_creatable = value;
			}
		}

		/// <summary>
		/// Readable column(default : true).
		/// </summary>
		public Boolean Readable
		{
			get
			{
				_readable = _isPrimaryKey == true ? true : _readable;

				return _readable;
			}
			set
			{
				_readable = value;
			}
		}

		/// <summary>
		/// Updatable column(default : true).
		/// </summary>
		public Boolean Updatable
		{
			get
			{
				_updatable = _isPrimaryKey == true ? true : _updatable;

				return _updatable;
			}
			set
			{
				_updatable = value;
			}
		}

		/// <summary>
		/// Deletable column(default : true).
		/// </summary>
		public Boolean Deletable
		{
			get
			{
				_deletable = _isPrimaryKey == true ? true : _deletable;

				return _deletable;
			}
			set
			{
				_deletable = value;
			}
		}

		/// <summary>
		/// Inheritable column(default : true).
		/// </summary>
		public Boolean Inheritable
		{
			get
			{
				return _inheritable;
			}
			set
			{
				_inheritable= value;
			}
		}
		#endregion
	}
}
