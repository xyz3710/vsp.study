﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;

using iDoIt.Data;

namespace iDASiT.TrustCore.Common
{
	public class TrustCoreBase : CodeElement, ITrustCoreBase
	{
		private const string PARAM_PREFIX = "p_";

		#region ITrustCoreBase 멤버

		DataSet ITrustCoreBase.GetDataSet()
		{
			return getDataSet();
		}

		int ITrustCoreBase.Create()
		{
			return executeNonQuery(CRUDType.Create);
		}

		int ITrustCoreBase.Update()
		{
			return executeNonQuery(CRUDType.Update);
		}

		int ITrustCoreBase.Delete()
		{
			return executeNonQuery(CRUDType.Delete);
		}

		T ITrustCoreBase.GetThis<T>()
		{
			// ParamInfo의 column 내용과 return 할 class의 할당할 Property를 비교하기 위해 
			// 모든 Property 정보를 구해와야 하므로 CRUDType.Create으로 설정했다.
			StoredProcInfo spi = getStoredProcInfo(this, CRUDType.Create);

			if (spi == null)
				return default(T);

			T instance = Activator.CreateInstance<T>();
			DataSet ds = getDataSet();		// spi를 Create으로 설정하여 활용할 수 없으므로 외부 method를 실행한다.

			if (ds != null && ds.Tables.Count >= 1)
			{
				ArrayList columnNames = new ArrayList();

				foreach (ParamInfo item in spi.ParamInfo.GetValues())
					columnNames.Add(item.ColumnName);

				foreach (PropertyInfo pi in typeof(T).GetProperties())
				{
					if (columnNames.Contains(pi.Name) == false)
						continue;

					if (pi.CanWrite == true)
					{
						object value = ds.Tables[0].Rows[0][pi.Name];

						// Oracle Number/Integer type을 Decimal로 변환해서 Return 하기 때문에 처리한다
						if (ds.Tables[0].Columns[pi.Name].DataType.Equals(typeof(decimal)) == true)
							value = Convert.ToInt32(value);

						if (Convert.IsDBNull(value) == true)
							value = null;

						pi.SetValue(instance, value, null);
					}
				}
			}

			#region DataReader로 할경우 에러가 발생하여 일단 DataSet으로 처리
			/*
			Database db = null;

			try
			{
				db = DatabaseFactory.CreateDatabase();

				using (DbConnection connection = db.CreateConnection())
				{
					connection.Open();

					DbTransaction transaction = connection.BeginTransaction();
					DbCommand cmdSP = db.GetStoredProcCommand(spi.StoredProcedureName);

					addPKtoParameters(cmdSP, spi, ref db);

					using (IDataReader dataReader = db.ExecuteReader(cmdSP, transaction))
					{
						try
						{
							foreach (PropertyInfo pi in typeof(T).GetProperties())
								if (pi.CanWrite == true && dataReader.IsClosed == false)
									pi.SetValue(instance, dataReader[pi.Name], null);

							transaction.Commit();
						}
						catch (Exception innerEx)
						{
							if (transaction != null)
								transaction.Rollback();

							throw new Exception(innerEx.Message, innerEx);
						}
						finally
						{
							dataReader.Close();
							connection.Close();
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			*/
			#endregion

			return instance;
		}

		#endregion

		#region ITrustCoreBase 멤버 Method overload가 두개일 경우(parameter 있는 경우 없는경우)
		/* Method overload가 두개일 경우(parameter 있는 경우 없는경우)		

		DataSet ITrustCoreBase.GetDataSet(ITrustCoreBase trustCore)
		{
			StoredProcInfo spi = getStoredProcInfo(trustCore, CRUDType.Get);

			if (spi == null)
				return null;

			return getDataSet(spi); 
		}

		int ITrustCoreBase.Create(ITrustCoreBase trustCore)
		{
			StoredProcInfo spi = getStoredProcInfo(trustCore, CRUDType.Create);

			if (spi == null)
				return -1;

			return executeNonQuery(spi); 
		}

		int ITrustCoreBase.Update(ITrustCoreBase trustCore)
		{
			StoredProcInfo spi = getStoredProcInfo(trustCore, CRUDType.Update);

			if (spi == null)
				return -1;

			return executeNonQuery(spi);
		}

		int ITrustCoreBase.Delete(ITrustCoreBase trustCore)
		{
			StoredProcInfo spi = getStoredProcInfo(trustCore, CRUDType.Delete);

			if (spi == null)
				return -1;

			return executeNonQuery(spi);
		}

		DataSet ITrustCoreBase.GetDataSet()
		{
			return (this as ITrustCoreBase).GetDataSet(this);
		}

		int ITrustCoreBase.Create()
		{
			return (this as ITrustCoreBase).Create(this);
		}

		int ITrustCoreBase.Update()
		{
			return (this as ITrustCoreBase).Update(this);
		}

		int ITrustCoreBase.Delete()
		{
			return (this as ITrustCoreBase).Delete(this); 
		}
		*/
		#endregion

		#region GetColumnInfoAttribute from ITrustCoreBase
		/// <summary>
		/// GetColumnInfoAttribute from ITrustCoreBase
		/// </summary>
		/// <param name="trustCore">Wrapping class with implement <seealso cref="ITrustCoreBase"/></param>
		/// <returns></returns>
		private StoredProcInfo getStoredProcInfo(ITrustCoreBase trustCore, CRUDType crud)
		{
			StoredProcInfo spiRet = new StoredProcInfo(trustCore.GetType().Namespace);

			foreach (PropertyInfo property in trustCore.GetType().GetProperties())
			{
				foreach (Attribute attr in property.GetCustomAttributes(false))
				{
					ColumnInfoAttribute ci = attr as ColumnInfoAttribute;

					if (ci != null)
					{
						spiRet.StoredProcedureName = string.Format("{0}{1}", crud, trustCore.GetType().Name);

						if (property.PropertyType.IsArray == true)
							spiRet.MaxArrayLength = ((object[])property.GetValue(trustCore, null)).Length;

						// PK는 CRUD 여부와 상관없이 포함시킨다(FK로 사용될 수 있다)
						if (ci.IsPrimaryKey == false)
						{
							switch (crud)
							{
								case iDASiT.TrustCore.Common.CRUDType.Get:
									if (ci.Readable == false)
										continue;

									break;
								case iDASiT.TrustCore.Common.CRUDType.Create:
									if (ci.Creatable == false)
										continue;

									break;
								case iDASiT.TrustCore.Common.CRUDType.Update:
									if (ci.Updatable == false)
										continue;

									break;
								case iDASiT.TrustCore.Common.CRUDType.Delete:
									if (ci.Deletable == false)
										continue;

									break;
							}
						}

						if (ci.Inheritable == false)
							continue;

						// Primary Key인 컬럼은 앞으로 이동 시킨다.
						string key = string.Format("{0}{1:00000}", ci.IsPrimaryKey == true ? "A" : "Z", ci.Seq);

						ParamInfo pi = new ParamInfo();

						pi.ColumnName = ci.Name == string.Empty ? property.Name : ci.Name;
						pi.DbType = TrustCoreBase.GetDbType(property.PropertyType);
						pi.Value = property.GetValue(trustCore, null);
						pi.IsPrimaryKey = ci.IsPrimaryKey;

						spiRet.ParamInfo[key] = pi;
					}
				}
			}

			if (spiRet.ParamInfo.IndexerCount > 0)
				return spiRet;
			else
				return null;
		}
		#endregion

		#region getDataSet
		private DataSet getDataSet()
		{
			StoredProcInfo spi = getStoredProcInfo(this, CRUDType.Get);

			if (spi == null)
				return null;

			return getDataSetCore(spi);
		}

		private DataSet getDataSetCore(StoredProcInfo spi)
		{
			Database db = null;
			DataSet dsResult = null;

			try
			{
				db = DatabaseFactory.CreateDatabase();

				using (DbConnection connection = db.CreateConnection())
				{
					connection.Open();

					DbTransaction transaction = connection.BeginTransaction();
					DbCommand cmdSP = db.GetStoredProcCommand(spi.StoredProcedureName);

					AddPKtoParameters(cmdSP, spi, ref db);

					try
					{
						dsResult = db.ExecuteDataSet(cmdSP, transaction);

						transaction.Commit();
					}
					catch (Exception innerEx)
					{
						if (transaction != null)
							transaction.Rollback();

						throw new Exception(innerEx.Message, innerEx);
					}
					finally
					{
						connection.Close();
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return dsResult;
		}
		#endregion

		#region executeNonQuery
		private int executeNonQuery(CRUDType crud)
		{
			StoredProcInfo spi = getStoredProcInfo(this, crud);

			if (spi == null)
				return -1;

			return executeNonQueryCore(spi);
		}

		private int executeNonQueryCore(StoredProcInfo spi)
		{
			Database db = null;
			int affectedRow = -1;

			try
			{
				db = DatabaseFactory.CreateDatabase();

				using (DbConnection connection = db.CreateConnection())
				{
					connection.Open();

					DbTransaction transaction = null;
					DbCommand cmdSP = db.GetStoredProcCommand(spi.StoredProcedureName);

					try
					{
						string key = string.Empty;

						for (int i = 0; i < spi.MaxArrayLength; i++)
						{
							transaction = connection.BeginTransaction();

							if (i == 0)
							{
								// Parameter를 추가한다.
								foreach (ParamInfo item in spi.ParamInfo.GetValues())
								{
									// Array type은 single로 변환한다
									if (item.Value != null && item.Value.GetType().IsArray == true)
										db.AddInParameter(cmdSP, PARAM_PREFIX + item.ColumnName,
											TrustCoreBase.GetDbType(Type.GetType(item.Value.GetType().FullName.Replace("[]", string.Empty))),
											((object[])item.Value)[i]);
									else
										db.AddInParameter(cmdSP, PARAM_PREFIX + item.ColumnName, item.DbType, item.Value);
								}
							}
							else
							{
								// 값만 변경한다.
								for (int j = 0; j < spi.ParamInfo.GetValues().Count; j++)
								{
									ParamInfo item = spi.ParamInfo.GetValues()[j];

									if (item.Value.GetType().IsArray == true)
										cmdSP.Parameters[j].Value = ((object[])item.Value)[i];
									else
										cmdSP.Parameters[j].Value = item.Value;
								}
							}

							affectedRow = db.ExecuteNonQuery(cmdSP, transaction);

							transaction.Commit();
						}
					}
					catch (Exception innerEx)
					{
						if (transaction != null)
							transaction.Rollback();

						throw new Exception(innerEx.Message, innerEx);
					}
					finally
					{
						connection.Close();
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return affectedRow;
		}
		#endregion

		#region getDataReader
		private IDataReader getDataReader(StoredProcInfo spi)
		{
			Database db = null;
			IDataReader dataReader = null;

			try
			{
				db = DatabaseFactory.CreateDatabase();

				using (DbConnection connection = db.CreateConnection())
				{
					connection.Open();

					DbTransaction transaction = connection.BeginTransaction();
					DbCommand cmdSP = db.GetStoredProcCommand(spi.StoredProcedureName);

					AddPKtoParameters(cmdSP, spi, ref db);

					try
					{
						dataReader = db.ExecuteReader(cmdSP, transaction);

						transaction.Commit();
					}
					catch (Exception innerEx)
					{
						transaction.Rollback();

						throw new Exception(innerEx.Message, innerEx);
					}
					finally
					{
						connection.Close();
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return dataReader;
		}
		#endregion

		#region GetSelect Command
		private string getSelectCommand(StoredProcInfo spi)
		{
			StringBuilder selectCommand = new StringBuilder();

			selectCommand.Append("SELECT \n");
			selectCommand.Append(getColumnList(spi));
			selectCommand.AppendFormat("	FROM {0} {1} \n", getTableName(), getTableAlias());
			selectCommand.AppendFormat("	WHERE {0} \n", getConditions(spi));

			return selectCommand.ToString();
		}

		private string getTableName()
		{
			string[] tableName = this.ToString().Split(new char[] { '.' });

			return tableName[tableName.Length - 1].ToUpper();
		}

		private string getTableAlias()
		{
			string tableName = getTableName();
			string tableAlias = "T" + tableName.Substring(0, 2);

			return tableAlias.ToUpper();
		}

		private string getColumnList(StoredProcInfo spi)
		{
			StringBuilder sb = new StringBuilder();
			int cnt = 0;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				sb.AppendFormat("	{0}{1}{2} \n",
					cnt++ > 0 ? "," : string.Empty,
					getTableAlias() + ".",
					pi.ColumnName);
			}

			return sb.ToString();
		}

		private string getConditions(StoredProcInfo spi)
		{
			StringBuilder sb = new StringBuilder();
			int pkCnt = 0;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				if (pi.IsPrimaryKey == true)
					sb.AppendFormat("{0}{1}.{2} = '{3}'",
							pkCnt++ > 0 ? "\n		AND " : string.Empty,
							getTableAlias(), pi.ColumnName, pi.Value);
			}

			//sb.Append(";");

			return sb.ToString();
		}
		#endregion

		#region 단일 parameter와 배열 parameter를 구별할 경우를 대비해서(주석처리됨)
		/*
		private int executeNonQuery(StoredProcInfo spi)
		{
			if (spi.MaxArrayLength == 1)
				return execNonArrayParam(spi);
			else
				return execArrayParam(spi);
		}

		private int execNonArrayParam(StoredProcInfo spi)
		{
			Database db = null;
			int affectedRow = -1;

			try
			{
				db = DatabaseFactory.CreateDatabase();

				using (DbConnection connection = db.CreateConnection())
				{
					connection.Open();

					DbTransaction transaction = connection.BeginTransaction();
					DbCommand cmdSP = db.GetStoredProcCommand(spi.StoredProcedureName);

					addParameters(cmdSP, spi, ref db);

					try
					{
						affectedRow = db.ExecuteNonQuery(cmdSP, transaction);

						transaction.Commit();
					}
					catch (Exception ex)
					{
						transaction.Rollback();

						throw new Exception(ex.Message);
					}
					finally
					{
						connection.Close();
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return affectedRow;
		}

		private int execArrayParam(StoredProcInfo spi)
		{
			Database db = null;
			int affectedRow = -1;

			try
			{
				db = DatabaseFactory.CreateDatabase();

				using (DbConnection connection = db.CreateConnection())
				{
					connection.Open();

					DbTransaction transaction = null;
					DbCommand cmdSP = db.GetStoredProcCommand(spi.StoredProcedureName);

					try
					{
						ParamInfo newPi = new ParamInfo();
						string key = string.Empty;

						for (int i = 0; i < spi.MaxArrayLength; i++)
						{
							transaction = connection.BeginTransaction();

							if (i == 0)
							{
								// Parameter를 추가한다.
								foreach (ParamInfo item in spi.ParamInfo.GetValues())
								{
									// Array type은 single로 변환한다
									if (item.Value.GetType().IsArray == true)
										db.AddInParameter(cmdSP, item.ColumnName, 
											TrustCoreBase.GetDbType(Type.GetType(item.Value.GetType().FullName.Replace("[]", string.Empty))), 
											((object[])item.Value)[i]);
									else
										db.AddInParameter(cmdSP, item.ColumnName, item.DbType, item.Value);
								}
							}							
							else
							{
								// 값만 변경한다.
								for (int j = 0; j < spi.ParamInfo.GetValues().Count; j++)
								{
									ParamInfo item = spi.ParamInfo.GetValues()[j];

									if (item.Value.GetType().IsArray == true)
										cmdSP.Parameters[j].Value = ((object[])item.Value)[i];
									else
										cmdSP.Parameters[j].Value = item.Value;
								}
							}

							affectedRow = db.ExecuteNonQuery(cmdSP, transaction);

							transaction.Commit();
						}
					}
					catch (Exception ex)
					{
						if (transaction != null)
							transaction.Rollback();

						throw new Exception(ex.Message);
					}
					finally
					{
						connection.Close();
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return affectedRow;
		}
		*/
		#endregion

		#region AddParameters
		/// <summary>
		/// Select, Delete에서 primary key만을 input parameter로 지정한다.
		/// </summary>
		/// <param name="command"></param>
		/// <param name="spi"></param>
		/// <param name="db"></param>
		private void AddPKtoParameters(DbCommand command, StoredProcInfo spi, ref Database db)
		{
			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
				if (pi.IsPrimaryKey == true)
					db.AddInParameter(command, PARAM_PREFIX + pi.ColumnName, pi.DbType, pi.Value);
		}

		private void AddParameters(DbCommand command, StoredProcInfo spi, ref Database db)
		{
			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
				db.AddInParameter(command, PARAM_PREFIX + pi.ColumnName, pi.DbType, pi.Value);
		}
		#endregion

		#region GetDbType

		/// <summary>
		/// Convert .Net <seealso cref="System.Type"/> to DbType
		/// </summary>
		/// <param name="type">.Net Data type</param>
		/// <returns>DbType</returns>
		public static DbType GetDbType(Type type)
		{
			DbType dbType = DbType.Object;

			if (type == typeof(string))
				dbType = DbType.String;

			if (type == typeof(int))
				dbType = DbType.Int32;

			if (type == typeof(double))
				dbType = DbType.Decimal;

			if (type == typeof(DateTime))
				dbType = DbType.DateTime;

			return dbType;
		}

		#endregion
	}

	#region CRUDType internal Enum
	internal enum CRUDType
	{
		/// <summary>
		/// Read(don't change this enum) <br/>
		/// used by Stored procedure prefix.
		/// </summary>
		Get,
		/// <summary>
		/// Create
		/// </summary>
		Create,
		/// <summary>
		/// Update
		/// </summary>
		Update,
		/// <summary>
		/// Delete
		/// </summary>
		Delete,
	}
	#endregion
}
