using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

using iDASiT.TrustCore.Common;
using iDoIt.Data.Configuration;

namespace iDASiT.TrustCore.Common
{
	public class BusinessPartner : TrustCoreBase
	{
		private CodeElement _code = new CodeElement();
		private string[] addressId = new string[] { };
		private string[] businessLicenseId = new string[] { };
		private string[] emailId = new string[] { };
		private string[] faxId = new string[] { };
		private string[] homepageId = new string[] { };
		private string[] ownerId = new string[] { };
		private string[] telephoneId = new string[] { };

		public BusinessPartner()
		{

		}

		#region IBusinessPartner ���

		[ColumnInfo(101, IsPrimaryKey=true)]
		public string BusinessPartnerId
		{
			get
			{
				return (_code as ICodeElement).CodeId;
			}
			set
			{
				(_code as ICodeElement).CodeId = value;
			}
		}

		[ColumnInfo(102, Creatable=false, Deletable=false, Updatable=false)]
		public string BusinessPartnerDesc
		{
			get
			{
				return (_code as ICodeElement).CodeDesc;
			}
			set
			{
				(_code as ICodeElement).CodeDesc = value;
			}
		}

		[ColumnInfo(103, Creatable=false, Deletable=false, Updatable=false)]
		public int BusinessPartnerSeq
		{
			get
			{
				return (_code as ICodeElement).CodeSeq;
			}
			set
			{
				(_code as ICodeElement).CodeSeq = value;
			}
		}

		#endregion

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] BusinessLicenseId
		{
			get
			{
				return businessLicenseId;
			}
			set
			{
				businessLicenseId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] EmailId
		{
			get
			{
				return emailId;
			}
			set
			{
				emailId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] AddressId
		{
			get
			{
				return addressId;
			}
			set
			{
				addressId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] FaxId
		{
			get
			{
				return faxId;
			}
			set
			{
				faxId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] HomepageId
		{
			get
			{
				return homepageId;
			}
			set
			{
				homepageId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] OwnerId
		{
			get
			{
				return ownerId;
			}
			set
			{
				ownerId = value;
			}
		}

		[XmlArrayItem(typeof(String), IsNullable=true)]
		public string[] TelephoneId
		{
			get
			{
				return telephoneId;
			}
			set
			{
				telephoneId = value;
			}
		}
	}
}
