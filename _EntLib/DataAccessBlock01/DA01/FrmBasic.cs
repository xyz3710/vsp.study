﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDoIt.Data;
using System.Data.Common;
using System.Globalization;
using Infragistics.Win.UltraWinGrid;
using System.Collections;

namespace DA01
{
	public partial class FrmBasic : Form
	{
		private DataSet _dsTables;
		private DataSet _dsProducts;

		public FrmBasic()
		{
			InitializeComponent();

			_dsTables = null;
		}
		
		private void Form1_Load(object sender, EventArgs e)
		{
			rdOracleProvider.Checked = true;
			rdSPCommand.Checked = true;
			cboTables.Items.Clear();
			txtParam1.Text = "7";
			txtParam2.Text = "27";
		}

		private void btnConnectDB_Click(object sender, EventArgs e)
		{
			Database db = null;
			
			clearMessage();

			try
			{
				db = getDatabase();

				successConnection();

				string sqlCommand = getSqlCommand();
				DbCommand cmdSP = null;

				if (rdSQLCommand.Checked == true)
					cmdSP = db.GetSqlStringCommand(sqlCommand);
				else
				{
					cmdSP = db.GetStoredProcCommand(sqlCommand);

					db.AddInParameter(cmdSP, "startNo", DbType.Int32, txtParam1.Text);
					db.AddInParameter(cmdSP, "endNo", DbType.Int32, txtParam2.Text);
				}

				_dsTables = db.ExecuteDataSet(cmdSP);

				enableTableCombo();
			}
			catch (Exception ex)
			{
				errorMessage(ex);
			}
			finally
			{
			
			}			 
		}

		private void enableTableCombo()
		{
			ArrayList tables = new ArrayList();

			cboTables.Items.Clear();

			foreach (DataTable table in _dsTables.Tables)
				tables.Add(table.TableName);

			cboTables.Items.AddRange(tables.ToArray());

			if (cboTables.Items.Count > 0)
				cboTables.SelectedIndex = 0;
		}

		private void btn2Params_Click(object sender, EventArgs e)
		{
			Database db = null;

			try
			{
				db = getDatabase();

				successConnection();

				string sqlCommand = "UP_TwoReturnTest";
				DbCommand cmdSP = db.GetStoredProcCommand(sqlCommand);

				int outParam = 0;

				db.AddInParameter(cmdSP, "inParam", DbType.Int32, 10);
				db.AddOutParameter(cmdSP, "outParam", DbType.Int32, outParam);

				using (DbConnection conneciton = db.CreateConnection())
                {
					conneciton.Open();

					DbTransaction transaction = conneciton.BeginTransaction();

					try
					{
						_dsTables = db.ExecuteDataSet(cmdSP, transaction);

						transaction.Commit();
					}
					catch
					{
						transaction.Rollback();
					}
					finally
					{
						conneciton.Close();
					}
                }

				txtSqlCommand.Text = string.Format("outParam : {0}", db.GetParameterValue(cmdSP, "outParam"));

				enableTableCombo();
			}
			catch (Exception ex)
			{
				errorMessage(ex);
			}
			finally
			{
				
			}
		}

		private void clearMessage()
		{
			statusBar.Text = string.Empty;
			
			_dsTables = null;
			bindingData();

			Update();
		}

		private void errorMessage(Exception ex)
		{
			statusBar.Text = ex.Message;
		}

		private void successConnection()
		{
			statusBar.Text = "Connection successful";
		}

		private Database getDatabase()
		{
			string databaseName = rdSQLProvider.Checked == true ? "csSQL" : "csOracle";

			return DatabaseFactory.CreateDatabase(databaseName);
		}

		private string getSqlCommand()
		{
			string sqlCommand = txtSqlCommand.Text;

			return sqlCommand;
		}

		private void bindingData()
		{
			xGrid.DataSource = _dsTables;
			xGrid.DataBind();
		}

		private void bindingData(DataTable table)
		{
			xGrid.DataSource = table;
			xGrid.DataBind();
		}

		private void radioButton_CheckedChanged(object sender, EventArgs e)
		{
			if (rdSPCommand.Checked == true)
				txtSqlCommand.Text = "UP_GetTest";
			else
				txtSqlCommand.Text = "select * from test";

			txtSqlCommand.SelectAll();
			txtSqlCommand.Focus();
		}

		private void txtSqlCommand_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control == true && e.KeyCode == Keys.Enter)
			{
				e.Handled = true;
				btnConnectDB_Click(sender, new EventArgs());
			}
		}

		private void btnTest_Click(object sender, EventArgs e)
		{
			Database db = DatabaseFactory.CreateDatabase("csNorthWind");

			#region In Out Parameters
			string sqlText = "UP_GetProductDetails";

			DbCommand cmdParams = db.GetStoredProcCommand(sqlText);

			db.AddInParameter(cmdParams, "productId", DbType.Int32, txtParam1.Text);
			db.AddOutParameter(cmdParams, "productName", DbType.String, 50);
			db.AddOutParameter(cmdParams, "unitPrice", DbType.Currency, 8);
			db.AddOutParameter(cmdParams, "qtyPerUnit", DbType.String, 20);

			db.ExecuteNonQuery(cmdParams);

			string result = 
				string.Format(CultureInfo.CurrentCulture, "Params\t: {0}\t{1}\t{2:C}\t{3}",
					db.GetParameterValue(cmdParams, "productId"),
					db.GetParameterValue(cmdParams, "productName"),
					db.GetParameterValue(cmdParams, "unitPrice"),
					db.GetParameterValue(cmdParams, "qtyPerUnit"));

			txtSqlCommand.Text = result;
			#endregion

			#region ExecuteScalar
			sqlText = "UP_GetProductName";

			DbCommand cmdScalar = db.GetStoredProcCommand(sqlText, txtParam2.Text);

			string productName = (string)db.ExecuteScalar(cmdScalar);

			txtSqlCommand.Text += string.Format("\r\nScalar\t: {0}", productName);
			#endregion
		}

		/// <summary>
		/// Multi transaction process
		/// </summary>
		/// <param name="transactionAmount"></param>
		/// <param name="srcAccount"></param>
		/// <param name="destAccount"></param>
		/// <returns></returns>
		private bool transfer(int transactionAmount, int srcAccount, int destAccount)
		{
			bool ret = false;

			Database db = DatabaseFactory.CreateDatabase("csNorthWind");
			
			string sqlCommand = "UP_Credit";
			DbCommand cmdCredit = db.GetStoredProcCommand(sqlCommand);

			db.AddInParameter(cmdCredit, "accountNo", DbType.Int32, srcAccount);
			db.AddInParameter(cmdCredit, "amount", DbType.Int32, transactionAmount);

			sqlCommand = "UP_Debit";

			DbCommand cmdDebit = db.GetStoredProcCommand(sqlCommand);

			db.AddInParameter(cmdDebit, "accountNo", DbType.Int32, destAccount);
			db.AddInParameter(cmdDebit, "amount", DbType.Int32, transactionAmount);

			sqlCommand = "UP_UpdateAccountLog";

			DbCommand cmdLog = db.GetStoredProcCommand(sqlCommand);

			db.AddInParameter(cmdLog, "accountNo", DbType.Int32, destAccount);
			db.AddInParameter(cmdLog, "amount", DbType.Int32, transactionAmount);

			using (DbConnection connection = db.CreateConnection())
            {
				connection.Open();

				DbTransaction transaction = connection.BeginTransaction();

				try
				{
					db.ExecuteNonQuery(cmdCredit, transaction);
					db.ExecuteNonQuery(cmdDebit, transaction);
					db.ExecuteNonQuery(cmdLog, transaction);

					transaction.Commit();

					ret = true;
				}
				catch 
				{
					transaction.Rollback();
				}
				finally
				{
					connection.Close();
				}
            }

			return ret;
		}

		private void btnTransfer_Click(object sender, EventArgs e)
		{
			bool retTrans = transfer((int)DateTime.Now.ToOADate(), int.Parse(txtParam1.Text), int.Parse(txtParam2.Text));

			txtSqlCommand.Text += string.Format("\r\nTransaction\t: {0}", retTrans.ToString());
		}

		/// <summary>
		/// DataSet을 이용한 DB update
		/// </summary>
		private void updateDataSet()
		{
			string tblProducts = _dsProducts.DataSetName;
			Database db = DatabaseFactory.CreateDatabase("csNorthWind");

			/*
			DbCommand cmdDataSet = db.GetSqlStringCommand("UP_GetProducts");

			// retrieve the initial data
			db.LoadDataSet(CommandType.TableDirect, _dsProducts, tblProducts);

			// get the table that will be modifed.
			//DataTable table = _dsProducts.Tables[tblProducts];

			// Add a new product to existing DataSet
			//DataRow addedRow = table.Rows.Add(new object[] { DBNull.Value, "ZZZ. New Product", 6, 25 });

			// modify an existing product
			//table.Rows[0]["productName"] = string.Format("Modifed products {0}", DateTime.Now);
			*/

			// establish the insert, delete and update commands
			DbCommand cmdInsert = db.GetStoredProcCommand("UP_AddProduct");
			
			db.AddInParameter(cmdInsert, "productName", DbType.String, "productName", DataRowVersion.Current);
			db.AddInParameter(cmdInsert, "categoryId", DbType.Int32, "categoryId", DataRowVersion.Current);
			db.AddInParameter(cmdInsert, "unitPrice", DbType.Currency, "unitPrice", DataRowVersion.Current);
			
			DbCommand cmdDelete = db.GetStoredProcCommand("UP_DeleteProduct");

			db.AddInParameter(cmdDelete, "productId", DbType.Int32, "productId", DataRowVersion.Current);

			DbCommand cmdUpdate = db.GetStoredProcCommand("UP_UpdateProduct");

			db.AddInParameter(cmdUpdate, "productId", DbType.Int32, "productId", DataRowVersion.Current);
			db.AddInParameter(cmdUpdate, "productName", DbType.String, "productName", DataRowVersion.Current);
			db.AddInParameter(cmdUpdate, "categoryId", DbType.String, "categoryId", DataRowVersion.Current);
			db.AddInParameter(cmdUpdate, "unitPrice", DbType.String, "unitPrice", DataRowVersion.Current);

			// Submit the DataSet, captureing the number of rows that were affected.
			int rowsAffected = db.UpdateDataSet(_dsProducts, tblProducts, cmdInsert, cmdUpdate, cmdDelete, UpdateBehavior.Standard);

			txtSqlCommand.Text += string.Format("\r\nAffected : {0}", rowsAffected);
		}

		private void btnViewProduct_Click(object sender, EventArgs e)
		{
			getProducts();
		}

		private void getProducts()
		{
			Database db = DatabaseFactory.CreateDatabase("csNorthWind");
			DbCommand cmdProducts = db.GetStoredProcCommand("UP_GetProducts");
			string tblProducts = "products";

			_dsProducts = db.ExecuteDataSet(cmdProducts);
			_dsProducts.DataSetName = tblProducts;

			// 테이블 이름을 지정하지 않으면 Exception 발생한다
			if (_dsProducts.Tables.Count == 1)
				_dsProducts.Tables[0].TableName = tblProducts;

			_dsTables = _dsProducts;

			bindingData();

			for (int i = 0; i < xGrid.DisplayLayout.Bands[0].Columns.Count; i++)
				xGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;

			xGrid.DisplayLayout.Bands[0].Columns["productName"].CellActivation = Activation.AllowEdit;
			xGrid.DisplayLayout.Bands[0].Columns["categoryId"].CellActivation = Activation.AllowEdit;
			xGrid.DisplayLayout.Bands[0].Columns["unitPrice"].CellActivation = Activation.AllowEdit;

			// 안된다 Column을 정의 후 Data를 Binding 시켜야 할 것 같다
			xGrid.DisplayLayout.Bands[0].Columns["categoryId"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
		}

		private void btnUpdateDataSet_Click(object sender, EventArgs e)
		{
			updateDataSet();

			bindingData();

			getProducts();
		}

		private void xGrid_AfterRowUpdate(object sender, RowEventArgs e)
		{
			DialogResult question = MessageBox.Show("변경되었습니다.\r\n저장하시겠습니까?",
				"저장확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

			if (question == DialogResult.Yes)
			{
				xGrid.UpdateData();

				updateDataSet();
			}
		}

		private void cboTables_SelectedIndexChanged(object sender, EventArgs e)
		{
			int selectedIndex = cboTables.SelectedIndex;

			bindingData(_dsTables.Tables[selectedIndex]);
		}

		private void btnCallWrapping_Click(object sender, EventArgs e)
		{
			DA02_WrappingClass.FrmWrapping frmWrapping = new DA02_WrappingClass.FrmWrapping();

			frmWrapping.ShowDialog();
		}
	}
}