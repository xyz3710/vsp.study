﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;

using iDASiT.TrustCore.Common;
using iDoIt.Data.Configuration;
using iDASiT.TrustCore.Code;
using iDASiT.TrustCore.Process;
using iDASiT.TrustCore.Data;
using iDASiT.TrustCore.Region;

namespace AttributeTest
{
	class Program
	{
		static void Main(string[] args)
		{

			Address address = new Address();

			address.AddressId = "A001";
			address.AddressDesc = "This is Address class";
			address.AddressSeq = 1;
			
			ITrustCoreBase tcAddress = address as ITrustCoreBase;
						
			if (tcAddress != null)
			{
				//DataSet dsAddress = tcAddress.GetDataSet();
				Address addr = tcAddress.GetThis<Address>();
			}
			Console.WriteLine("\n\n");

			return;
			/*
Subcontract subcontract = new Subcontract();

subcontract.BusinessPartnerDesc = "Business partner desc";
subcontract.BusinessPartnerId = "BP001";
subcontract.BusinessPartnerSeq = 1;
subcontract.SubcontractId = "SC001";
subcontract.SubcontractDesc = "Subcontract desc";
subcontract.SubcontractSeq = 1;

trustCore = subcontract as ITrustCoreBase;

if (trustCore != null)
{
	trustCore.Create(subcontract);
	trustCore.GetDataSet(subcontract);
	trustCore.Update(subcontract);
	trustCore.Delete(subcontract);
}

Console.WriteLine("\n\n");
*/
			//			LotDataItem ldi = new LotDataItem();
			//			
			//			ldi.OperationId = "OP001";
			//			ldi.LotId = "LOT001";
			//			ldi.WhenUpdated = DateTime.Now;
			//			ldi.WorkerId = "WORKER";
			//			ldi.PlantId = "PLANT01";
			//			ldi.DataParamKey = new string[] { "-----DPkey01-----", "-----DPkey02-----", "-----DPkey03-----" };
			//			ldi.LotDataItemSeq = 2;
			//			ldi.DataItemKey = "DI001";
			//			
			//			ITrustCoreBase tcLotDataItem = ldi as ITrustCoreBase;
			//
			//			if (tcLotDataItem != null)
			//			{
			//				tcLotDataItem.Update();
			//				//trustCore.GetDataSet(ldi);
			//			}


			WorkCalendar calendar = new WorkCalendar();

			calendar.WorkRatioSunday = 1.0;

			ITrustCoreBase tcCalandar = calendar as ITrustCoreBase;

			if (tcCalandar != null)
				tcCalandar.Create();
		}
		
	}
}
