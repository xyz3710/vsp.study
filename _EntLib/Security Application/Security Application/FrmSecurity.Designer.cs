﻿namespace Security_Application
{
	partial class FrmSecurity
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.lstRole = new System.Windows.Forms.ListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lstUser = new System.Windows.Forms.ListBox();
			this.tlpUser = new System.Windows.Forms.TableLayoutPanel();
			this.btnAddUser = new System.Windows.Forms.Button();
			this.btnDelUser = new System.Windows.Forms.Button();
			this.btnAuthenticate = new System.Windows.Forms.Button();
			this.tlpRole = new System.Windows.Forms.TableLayoutPanel();
			this.btnAddRole = new System.Windows.Forms.Button();
			this.btnDelRole = new System.Windows.Forms.Button();
			this.btnAssignRole = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.statusBar = new System.Windows.Forms.ToolStripStatusLabel();
			this.btnCheckRule = new System.Windows.Forms.Button();
			this.tlpEtc = new System.Windows.Forms.TableLayoutPanel();
			this.btnWriteProfile = new System.Windows.Forms.Button();
			this.btnReadProfile = new System.Windows.Forms.Button();
			this.btnObtainCacheToken = new System.Windows.Forms.Button();
			this.btnRetrievIdentity = new System.Windows.Forms.Button();
			this.btnTokenExpire = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.lstRule = new System.Windows.Forms.ListBox();
			this.btnCreateBulkData = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tlpUser.SuspendLayout();
			this.tlpRole.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.tlpEtc.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 4;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tlpUser, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tlpRole, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.statusStrip1, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.btnCheckRule, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.tlpEtc, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.groupBox3, 2, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(804, 500);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.lstRole);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(204, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(195, 369);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Role";
			// 
			// lstRole
			// 
			this.lstRole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstRole.FormattingEnabled = true;
			this.lstRole.ItemHeight = 12;
			this.lstRole.Location = new System.Drawing.Point(3, 17);
			this.lstRole.Name = "lstRole";
			this.lstRole.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.lstRole.Size = new System.Drawing.Size(189, 340);
			this.lstRole.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lstUser);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(195, 369);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "User";
			// 
			// lstUser
			// 
			this.lstUser.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstUser.FormattingEnabled = true;
			this.lstUser.ItemHeight = 12;
			this.lstUser.Location = new System.Drawing.Point(3, 17);
			this.lstUser.Name = "lstUser";
			this.lstUser.Size = new System.Drawing.Size(189, 340);
			this.lstUser.TabIndex = 0;
			this.lstUser.SelectedIndexChanged += new System.EventHandler(this.lstUser_SelectedIndexChanged);
			// 
			// tlpUser
			// 
			this.tlpUser.ColumnCount = 1;
			this.tlpUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpUser.Controls.Add(this.btnAddUser, 0, 0);
			this.tlpUser.Controls.Add(this.btnDelUser, 0, 1);
			this.tlpUser.Controls.Add(this.btnAuthenticate, 0, 2);
			this.tlpUser.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpUser.Location = new System.Drawing.Point(30, 383);
			this.tlpUser.Margin = new System.Windows.Forms.Padding(30, 8, 30, 8);
			this.tlpUser.Name = "tlpUser";
			this.tlpUser.RowCount = 3;
			this.tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpUser.Size = new System.Drawing.Size(141, 84);
			this.tlpUser.TabIndex = 3;
			// 
			// btnAddUser
			// 
			this.btnAddUser.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnAddUser.Location = new System.Drawing.Point(3, 3);
			this.btnAddUser.Name = "btnAddUser";
			this.btnAddUser.Size = new System.Drawing.Size(135, 22);
			this.btnAddUser.TabIndex = 0;
			this.btnAddUser.Text = "사용자 추가";
			this.btnAddUser.UseVisualStyleBackColor = true;
			this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
			// 
			// btnDelUser
			// 
			this.btnDelUser.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnDelUser.Location = new System.Drawing.Point(3, 31);
			this.btnDelUser.Name = "btnDelUser";
			this.btnDelUser.Size = new System.Drawing.Size(135, 22);
			this.btnDelUser.TabIndex = 1;
			this.btnDelUser.Text = "사용자 삭제";
			this.btnDelUser.UseVisualStyleBackColor = true;
			this.btnDelUser.Click += new System.EventHandler(this.btnDelUser_Click);
			// 
			// btnAuthenticate
			// 
			this.btnAuthenticate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnAuthenticate.Location = new System.Drawing.Point(3, 59);
			this.btnAuthenticate.Name = "btnAuthenticate";
			this.btnAuthenticate.Size = new System.Drawing.Size(135, 22);
			this.btnAuthenticate.TabIndex = 2;
			this.btnAuthenticate.Text = "사용자 인증";
			this.btnAuthenticate.UseVisualStyleBackColor = true;
			this.btnAuthenticate.Click += new System.EventHandler(this.btnAuthenticate_Click);
			// 
			// tlpRole
			// 
			this.tlpRole.ColumnCount = 1;
			this.tlpRole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpRole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpRole.Controls.Add(this.btnAddRole, 0, 0);
			this.tlpRole.Controls.Add(this.btnDelRole, 0, 1);
			this.tlpRole.Controls.Add(this.btnAssignRole, 0, 2);
			this.tlpRole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpRole.Location = new System.Drawing.Point(231, 383);
			this.tlpRole.Margin = new System.Windows.Forms.Padding(30, 8, 30, 8);
			this.tlpRole.Name = "tlpRole";
			this.tlpRole.RowCount = 3;
			this.tlpRole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpRole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpRole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpRole.Size = new System.Drawing.Size(141, 84);
			this.tlpRole.TabIndex = 3;
			// 
			// btnAddRole
			// 
			this.btnAddRole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnAddRole.Location = new System.Drawing.Point(3, 3);
			this.btnAddRole.Name = "btnAddRole";
			this.btnAddRole.Size = new System.Drawing.Size(135, 22);
			this.btnAddRole.TabIndex = 0;
			this.btnAddRole.Text = "역할 추가";
			this.btnAddRole.UseVisualStyleBackColor = true;
			this.btnAddRole.Click += new System.EventHandler(this.btnAddRole_Click);
			// 
			// btnDelRole
			// 
			this.btnDelRole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnDelRole.Location = new System.Drawing.Point(3, 31);
			this.btnDelRole.Name = "btnDelRole";
			this.btnDelRole.Size = new System.Drawing.Size(135, 22);
			this.btnDelRole.TabIndex = 1;
			this.btnDelRole.Text = "역할 삭제";
			this.btnDelRole.UseVisualStyleBackColor = true;
			this.btnDelRole.Click += new System.EventHandler(this.btnDelRole_Click);
			// 
			// btnAssignRole
			// 
			this.btnAssignRole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnAssignRole.Location = new System.Drawing.Point(3, 59);
			this.btnAssignRole.Name = "btnAssignRole";
			this.btnAssignRole.Size = new System.Drawing.Size(135, 22);
			this.btnAssignRole.TabIndex = 2;
			this.btnAssignRole.Text = "역할 할당";
			this.btnAssignRole.UseVisualStyleBackColor = true;
			this.btnAssignRole.Click += new System.EventHandler(this.btnAssignRole_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.AllowItemReorder = true;
			this.tableLayoutPanel1.SetColumnSpan(this.statusStrip1, 4);
			this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBar});
			this.statusStrip1.Location = new System.Drawing.Point(0, 475);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(804, 25);
			this.statusStrip1.TabIndex = 4;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// statusBar
			// 
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(0, 20);
			// 
			// btnCheckRule
			// 
			this.btnCheckRule.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCheckRule.Location = new System.Drawing.Point(422, 395);
			this.btnCheckRule.Margin = new System.Windows.Forms.Padding(20);
			this.btnCheckRule.Name = "btnCheckRule";
			this.btnCheckRule.Size = new System.Drawing.Size(161, 60);
			this.btnCheckRule.TabIndex = 3;
			this.btnCheckRule.Text = "인증된 사용자\r\n수행 권한 체크";
			this.btnCheckRule.UseVisualStyleBackColor = true;
			this.btnCheckRule.Click += new System.EventHandler(this.btnCheckRule_Click);
			// 
			// tlpEtc
			// 
			this.tlpEtc.ColumnCount = 1;
			this.tlpEtc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpEtc.Controls.Add(this.btnWriteProfile, 0, 1);
			this.tlpEtc.Controls.Add(this.btnReadProfile, 0, 0);
			this.tlpEtc.Controls.Add(this.btnObtainCacheToken, 0, 2);
			this.tlpEtc.Controls.Add(this.btnRetrievIdentity, 0, 3);
			this.tlpEtc.Controls.Add(this.btnTokenExpire, 0, 4);
			this.tlpEtc.Controls.Add(this.btnClose, 0, 5);
			this.tlpEtc.Controls.Add(this.btnCreateBulkData, 0, 6);
			this.tlpEtc.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpEtc.Location = new System.Drawing.Point(606, 3);
			this.tlpEtc.Name = "tlpEtc";
			this.tlpEtc.Padding = new System.Windows.Forms.Padding(10);
			this.tlpEtc.RowCount = 7;
			this.tableLayoutPanel1.SetRowSpan(this.tlpEtc, 2);
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.04348F));
			this.tlpEtc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.73913F));
			this.tlpEtc.Size = new System.Drawing.Size(195, 469);
			this.tlpEtc.TabIndex = 6;
			// 
			// btnWriteProfile
			// 
			this.btnWriteProfile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnWriteProfile.Location = new System.Drawing.Point(13, 71);
			this.btnWriteProfile.Name = "btnWriteProfile";
			this.btnWriteProfile.Size = new System.Drawing.Size(169, 52);
			this.btnWriteProfile.TabIndex = 1;
			this.btnWriteProfile.Text = "Profile 쓰기";
			this.btnWriteProfile.UseVisualStyleBackColor = true;
			this.btnWriteProfile.Click += new System.EventHandler(this.btnWriteProfile_Click);
			// 
			// btnReadProfile
			// 
			this.btnReadProfile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnReadProfile.Location = new System.Drawing.Point(13, 13);
			this.btnReadProfile.Name = "btnReadProfile";
			this.btnReadProfile.Size = new System.Drawing.Size(169, 52);
			this.btnReadProfile.TabIndex = 0;
			this.btnReadProfile.Text = "Profile 읽기";
			this.btnReadProfile.UseVisualStyleBackColor = true;
			this.btnReadProfile.Click += new System.EventHandler(this.btnReadProfile_Click);
			// 
			// btnObtainCacheToken
			// 
			this.btnObtainCacheToken.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnObtainCacheToken.Location = new System.Drawing.Point(13, 129);
			this.btnObtainCacheToken.Name = "btnObtainCacheToken";
			this.btnObtainCacheToken.Size = new System.Drawing.Size(169, 52);
			this.btnObtainCacheToken.TabIndex = 0;
			this.btnObtainCacheToken.Text = "인증된 사용자 \r\nToken 생성하기\r\n(Cache에 저장)";
			this.btnObtainCacheToken.UseVisualStyleBackColor = true;
			this.btnObtainCacheToken.Click += new System.EventHandler(this.btnObtainCacheToken_Click);
			// 
			// btnRetrievIdentity
			// 
			this.btnRetrievIdentity.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnRetrievIdentity.Location = new System.Drawing.Point(13, 187);
			this.btnRetrievIdentity.Name = "btnRetrievIdentity";
			this.btnRetrievIdentity.Size = new System.Drawing.Size(169, 52);
			this.btnRetrievIdentity.TabIndex = 1;
			this.btnRetrievIdentity.Text = "Token으로 \r\nIdentity 구하기\r\n(Cache에서 로드)";
			this.btnRetrievIdentity.UseVisualStyleBackColor = true;
			this.btnRetrievIdentity.Click += new System.EventHandler(this.btnRetrievIdentity_Click);
			// 
			// btnTokenExpire
			// 
			this.btnTokenExpire.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnTokenExpire.Location = new System.Drawing.Point(13, 245);
			this.btnTokenExpire.Name = "btnTokenExpire";
			this.btnTokenExpire.Size = new System.Drawing.Size(169, 52);
			this.btnTokenExpire.TabIndex = 2;
			this.btnTokenExpire.Text = "Token 만료";
			this.btnTokenExpire.UseVisualStyleBackColor = true;
			this.btnTokenExpire.Click += new System.EventHandler(this.btnTokenExpire_Click);
			// 
			// btnClose
			// 
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnClose.Location = new System.Drawing.Point(13, 303);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(169, 52);
			this.btnClose.TabIndex = 2;
			this.btnClose.Text = "종료(&X)";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.lstRule);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(405, 3);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(195, 369);
			this.groupBox3.TabIndex = 7;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Rule";
			// 
			// lstRule
			// 
			this.lstRule.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstRule.FormattingEnabled = true;
			this.lstRule.ItemHeight = 12;
			this.lstRule.Location = new System.Drawing.Point(3, 17);
			this.lstRule.Name = "lstRule";
			this.lstRule.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.lstRule.Size = new System.Drawing.Size(189, 340);
			this.lstRule.TabIndex = 0;
			// 
			// btnCreateBulkData
			// 
			this.btnCreateBulkData.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCreateBulkData.Location = new System.Drawing.Point(15, 383);
			this.btnCreateBulkData.Margin = new System.Windows.Forms.Padding(5, 25, 5, 25);
			this.btnCreateBulkData.Name = "btnCreateBulkData";
			this.btnCreateBulkData.Size = new System.Drawing.Size(165, 51);
			this.btnCreateBulkData.TabIndex = 2;
			this.btnCreateBulkData.Text = "Bulk Data 생성";
			this.btnCreateBulkData.UseVisualStyleBackColor = true;
			this.btnCreateBulkData.Click += new System.EventHandler(this.btnCreateBulkData_Click);
			// 
			// FrmSecurity
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(804, 500);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "FrmSecurity";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Security Application";
			this.Load += new System.EventHandler(this.FrmSecurity_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.tlpUser.ResumeLayout(false);
			this.tlpRole.ResumeLayout(false);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.tlpEtc.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ListBox lstRole;
		private System.Windows.Forms.ListBox lstUser;
		private System.Windows.Forms.TableLayoutPanel tlpUser;
		private System.Windows.Forms.TableLayoutPanel tlpRole;
		private System.Windows.Forms.Button btnAddUser;
		private System.Windows.Forms.Button btnDelUser;
		private System.Windows.Forms.Button btnAuthenticate;
		private System.Windows.Forms.Button btnAddRole;
		private System.Windows.Forms.Button btnDelRole;
		private System.Windows.Forms.Button btnAssignRole;
		private System.Windows.Forms.Button btnReadProfile;
		private System.Windows.Forms.Button btnWriteProfile;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel statusBar;
		private System.Windows.Forms.TableLayoutPanel tlpEtc;
		private System.Windows.Forms.Button btnObtainCacheToken;
		private System.Windows.Forms.Button btnRetrievIdentity;
		private System.Windows.Forms.Button btnTokenExpire;
		private System.Windows.Forms.Button btnCheckRule;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ListBox lstRule;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnCreateBulkData;
	}
}

