﻿namespace Security_Application
{
	partial class ProfileMannager
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpBody = new System.Windows.Forms.TableLayoutPanel();
			this.tlpButton = new System.Windows.Forms.TableLayoutPanel();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblTitle = new System.Windows.Forms.Label();
			this.txtColor = new System.Windows.Forms.TextBox();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tlpSex = new System.Windows.Forms.TableLayoutPanel();
			this.rdMale = new System.Windows.Forms.RadioButton();
			this.rdFemale = new System.Windows.Forms.RadioButton();
			this.clDialog = new System.Windows.Forms.ColorDialog();
			this.tlpBody.SuspendLayout();
			this.tlpButton.SuspendLayout();
			this.tlpSex.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpBody
			// 
			this.tlpBody.ColumnCount = 2;
			this.tlpBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tlpBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tlpBody.Controls.Add(this.tlpButton, 0, 4);
			this.tlpBody.Controls.Add(this.lblTitle, 0, 0);
			this.tlpBody.Controls.Add(this.txtColor, 1, 3);
			this.tlpBody.Controls.Add(this.txtUsername, 1, 1);
			this.tlpBody.Controls.Add(this.label2, 0, 1);
			this.tlpBody.Controls.Add(this.label3, 0, 2);
			this.tlpBody.Controls.Add(this.label1, 0, 3);
			this.tlpBody.Controls.Add(this.tlpSex, 1, 2);
			this.tlpBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpBody.Location = new System.Drawing.Point(0, 0);
			this.tlpBody.Margin = new System.Windows.Forms.Padding(20);
			this.tlpBody.Name = "tlpBody";
			this.tlpBody.RowCount = 5;
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpBody.Size = new System.Drawing.Size(351, 232);
			this.tlpBody.TabIndex = 1;
			// 
			// tlpButton
			// 
			this.tlpButton.ColumnCount = 2;
			this.tlpBody.SetColumnSpan(this.tlpButton, 2);
			this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.Controls.Add(this.btnOk, 0, 0);
			this.tlpButton.Controls.Add(this.btnCancel, 1, 0);
			this.tlpButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpButton.Location = new System.Drawing.Point(8, 190);
			this.tlpButton.Margin = new System.Windows.Forms.Padding(8);
			this.tlpButton.Name = "tlpButton";
			this.tlpButton.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
			this.tlpButton.RowCount = 1;
			this.tlpButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.Size = new System.Drawing.Size(335, 34);
			this.tlpButton.TabIndex = 0;
			// 
			// btnOk
			// 
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnOk.Location = new System.Drawing.Point(13, 3);
			this.btnOk.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(144, 28);
			this.btnOk.TabIndex = 0;
			this.btnOk.Text = "&Ok";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCancel.Location = new System.Drawing.Point(177, 3);
			this.btnCancel.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(145, 28);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblTitle
			// 
			this.lblTitle.AutoSize = true;
			this.tlpBody.SetColumnSpan(this.lblTitle, 2);
			this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTitle.Location = new System.Drawing.Point(3, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(345, 50);
			this.lblTitle.TabIndex = 1;
			this.lblTitle.Text = "Profile 정보를 입력하시오.";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtColor
			// 
			this.txtColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtColor.Location = new System.Drawing.Point(125, 148);
			this.txtColor.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
			this.txtColor.Name = "txtColor";
			this.txtColor.ReadOnly = true;
			this.txtColor.Size = new System.Drawing.Size(206, 21);
			this.txtColor.TabIndex = 1;
			this.txtColor.Enter += new System.EventHandler(this.txtColor_Enter);
			// 
			// txtUsername
			// 
			this.txtUsername.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtUsername.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
			this.txtUsername.Location = new System.Drawing.Point(125, 60);
			this.txtUsername.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(206, 21);
			this.txtUsername.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(3, 50);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 44);
			this.label2.TabIndex = 2;
			this.label2.Text = "User Name :";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(3, 94);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(99, 44);
			this.label3.TabIndex = 3;
			this.label3.Text = "Sex :";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 138);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 44);
			this.label1.TabIndex = 3;
			this.label1.Text = "Favorite Color :";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tlpSex
			// 
			this.tlpSex.ColumnCount = 2;
			this.tlpSex.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpSex.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpSex.Controls.Add(this.rdMale, 0, 0);
			this.tlpSex.Controls.Add(this.rdFemale, 1, 0);
			this.tlpSex.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpSex.Location = new System.Drawing.Point(108, 97);
			this.tlpSex.Name = "tlpSex";
			this.tlpSex.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
			this.tlpSex.RowCount = 1;
			this.tlpSex.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpSex.Size = new System.Drawing.Size(240, 38);
			this.tlpSex.TabIndex = 4;
			// 
			// rdMale
			// 
			this.rdMale.AutoSize = true;
			this.rdMale.Checked = true;
			this.rdMale.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rdMale.Location = new System.Drawing.Point(30, 3);
			this.rdMale.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
			this.rdMale.Name = "rdMale";
			this.rdMale.Size = new System.Drawing.Size(70, 32);
			this.rdMale.TabIndex = 0;
			this.rdMale.TabStop = true;
			this.rdMale.Text = "남";
			this.rdMale.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.rdMale.UseVisualStyleBackColor = true;
			// 
			// rdFemale
			// 
			this.rdFemale.AutoSize = true;
			this.rdFemale.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rdFemale.Location = new System.Drawing.Point(140, 3);
			this.rdFemale.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
			this.rdFemale.Name = "rdFemale";
			this.rdFemale.Size = new System.Drawing.Size(70, 32);
			this.rdFemale.TabIndex = 1;
			this.rdFemale.TabStop = true;
			this.rdFemale.Text = "여";
			this.rdFemale.UseVisualStyleBackColor = true;
			// 
			// clDialog
			// 
			this.clDialog.AllowFullOpen = false;
			this.clDialog.Color = System.Drawing.Color.White;
			this.clDialog.SolidColorOnly = true;
			// 
			// ProfileMannager
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(351, 232);
			this.Controls.Add(this.tlpBody);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Name = "ProfileMannager";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ProfileMannager";
			this.Load += new System.EventHandler(this.ProfileMannager_Load);
			this.tlpBody.ResumeLayout(false);
			this.tlpBody.PerformLayout();
			this.tlpButton.ResumeLayout(false);
			this.tlpSex.ResumeLayout(false);
			this.tlpSex.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpBody;
		private System.Windows.Forms.TableLayoutPanel tlpButton;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.TextBox txtColor;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tlpSex;
		private System.Windows.Forms.RadioButton rdMale;
		private System.Windows.Forms.RadioButton rdFemale;
		private System.Windows.Forms.ColorDialog clDialog;
	}
}