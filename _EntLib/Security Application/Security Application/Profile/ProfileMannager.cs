using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Security_Application.Profile;

namespace Security_Application
{
	public partial class ProfileMannager : Form
	{
		private Profiles _profile;

		public ProfileMannager()
		{
			InitializeComponent();

			_profile = new Profiles();
		}

		public Profiles Profiles
		{
			get
			{
				return _profile;
			}
			set
			{
				_profile = value;
			}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			_profile.UserName = txtUsername.Text;
			_profile.Sex = rdMale.Checked == true ? true : false;

			DialogResult = DialogResult.OK;

			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;

			Close();
		}

		private void txtColor_Enter(object sender, EventArgs e)
		{
			if (clDialog.ShowDialog() == DialogResult.OK)
			{
				_profile.FavoriteColor = clDialog.Color.ToArgb();

				txtColor.BackColor = Color.FromArgb(_profile.FavoriteColor);
				txtColor.Text = Color.FromArgb(_profile.FavoriteColor).Name;
			}

			btnOk.Select();
		}

		private void ProfileMannager_Load(object sender, EventArgs e)
		{
			Color color = Color.FromArgb(_profile.FavoriteColor);

			clDialog.Color = color;
			txtColor.Text = color.Name;
			txtColor.BackColor = color;

			txtUsername.Text = _profile.UserName;
			txtUsername.Select();
		}        
	}
}