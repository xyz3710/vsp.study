using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Security_Application.Profile
{
	public class Profiles
	{
		private string _userName;
		private bool _sex;
		private int _favoriteColor;

		/// <summary>
		/// Initializes a new instance of the Profiles class.
		/// </summary>
		public Profiles()
		{
			_userName = string.Empty;
			_sex = true;
			_favoriteColor = Color.Silver.ToArgb();
		}
		#region Properties
		/// <summary>
		/// Get or set UserName.
		/// </summary>
		public string UserName
		{
			get
			{
				return _userName;
			}
			set
			{
				_userName = value;
			}
		}
        
        /// <summary>
        /// Get or set Sex.
        /// </summary>
		public bool Sex
		{
			get
			{
				return _sex;
			}
			set
			{
				_sex = value;
			}
		}
        
        /// <summary>
        /// Get or set FavoriteColor.
        /// </summary>
		public int FavoriteColor
		{
			get
			{
				return _favoriteColor;
			}
			set
			{
				_favoriteColor = value;
			}
		}
        
        #endregion
        
	}
}
