/**********************************************************************************************************************/
/*	Domain		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2007년 7월 25일 수요일 오후 1:00:07a
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\aspnet_regsq.exe를 먼저 실행한다.
 * Test Query
USE aspnetDB;
DECLARE @applicationId uniqueidentifier

SELECT @applicationId = applicationId FROM aspnet_Applications WHERE applicationName = 'SecurityTestApplication';
 
SELECT * FROM aspnet_Users WHERE applicationId = @applicationId;
SELECT * FROM aspnet_Membership WHERE applicationId = @applicationId;
SELECT * FROM aspnet_Profile WHERE userId IN (SELECT userId FROM aspnet_Users WHERE applicationId = @applicationId);
SELECT * FROM aspnet_Roles WHERE applicationId = @applicationId;
SELECT * FROM aspnet_UsersINRoles WHERE userId IN (SELECT userId FROM aspnet_Users WHERE applicationId = @applicationId);;
SELECT * FROM aspnet_Applications WHERE applicationId = @applicationId;

USE aspnetDB;
DECLARE @applicationId uniqueidentifier

SELECT @applicationId = applicationId FROM aspnet_Applications WHERE applicationName = 'SecurityTestApplication';
 
DELETE FROM aspnet_Users WHERE applicationId = @applicationId;
DELETE FROM aspnet_Membership WHERE applicationId = @applicationId;
DELETE FROM aspnet_Profile WHERE userId IN (SELECT userId FROM aspnet_Users WHERE applicationId = @applicationId);
DELETE FROM aspnet_Roles WHERE applicationId = @applicationId;
DELETE FROM aspnet_UsersINRoles WHERE userId IN (SELECT userId FROM aspnet_Users WHERE applicationId = @applicationId);;
DELETE FROM aspnet_Applications WHERE applicationId = @applicationId;
 * profile provider 설정에 properties를 설정해야 한다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Security_Application.Profile;

using System.Web.Security;
using System.Security.Principal;
using System.Web.Profile;
using iDoIt.Security;
using System.Collections;

namespace Security_Application
{
	public partial class FrmSecurity : Form
	{
		private IIdentity _identity;
		private ISecurityCacheProvider _cache;
		private IAuthorizationProvider _ruleProvider;
		private IToken _token;

		public FrmSecurity()
		{
			InitializeComponent();
		}

		#region User
		private void btnAddUser_Click(object sender, EventArgs e)
		{
			UserCredential userCredential = new UserCredential();
			DialogResult result = userCredential.ShowDialog();
			
			if (result == DialogResult.OK)
			{
				Cursor saveCursor = Cursor.Current;
				string userId = userCredential.UserId;

				try
				{
					Cursor.Current = Cursors.WaitCursor;

					try
					{
						Membership.CreateUser(userId, userCredential.Password);

						displayMessage(userId, Properties.Resources.AddUserSuccess);
						resetUsers();
					}
					catch (MembershipCreateUserException ex)
					{
						displayMessage(userId, Properties.Resources.AddUserFailure);
						MessageBox.Show(ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				finally
				{
					Cursor.Current = saveCursor;
				}
			}
		}
		
        private void btnDelUser_Click(object sender, EventArgs e)
		{
			string userId = lstUser.SelectedItem.ToString();
			DialogResult result = MessageBox.Show(string.Format("{0} 를 삭제하시겠습니까?", userId), "사용자 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

			if (result == DialogResult.Yes)
			{
				Cursor saveCursor = Cursor.Current;
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					if (Membership.DeleteUser(userId) == true)
						displayMessage(userId, Properties.Resources.DeleteUserSuccess);
					else
						displayMessage(userId, Properties.Resources.DeleteUserFailure);
				}
				finally
				{
					Cursor.Current = saveCursor;
				}

				resetUsers();
			}            	
		}

		private void btnAuthenticate_Click(object sender, EventArgs e)
		{
			if (lstUser.SelectedIndex == -1)
			{
				MessageBox.Show("사용자를 선택해 주십시오.", "사용자 인증", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return;
			}

			UserCredential userCredential = new UserCredential();
			string userId = lstUser.SelectedItem.ToString();

			userCredential.UserId = userId;
			userCredential.Password = "1111";

			DialogResult result = userCredential.ShowDialog();

			if (result == DialogResult.OK)
			{
				if (Membership.ValidateUser(userId, userCredential.Password) == true)
                {
					_identity = new GenericIdentity(userId);
					readNSetProfile();
					checkRule(_identity.Name);
					displayMessage(userId, Properties.Resources.AuthenticateSuccess);
                }
				else
					displayMessage(userId, Properties.Resources.AuthenticateFailure);
			}
		}
		#endregion

		#region Role
		private void btnAddRole_Click(object sender, EventArgs e)
		{
			MessageBox.Show("역할 추가는 Rule과 관련되어 app.config에 저장되어 있습니다.\r\n현재는 추가할 수 없습니다.");
		}

		private void btnDelRole_Click(object sender, EventArgs e)
		{
			MessageBox.Show("역할 삭제는 Rule과 관련되어 app.config에 저장되어 있습니다.\r\n현재는 삭제할 수 없습니다.");
		}

		private void btnAssignRole_Click(object sender, EventArgs e)
		{
			if (validateAuthentication() == false)
				return;

			if (lstRole.SelectedIndex != -1 )
			{
				if (_identity.Name != lstUser.SelectedItem.ToString())
				{
					MessageBox.Show("인증된 사용자를 선택해 주십시오.");

					return;
				}   	

				Cursor saveCursor = Cursor.Current;
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					string[] selectedRole = new string[lstRole.SelectedItems.Count];

					for (int i = 0; i < lstRole.SelectedItems.Count; i++)
						selectedRole[i] = lstRole.SelectedItems[i].ToString();

					// 사용자 Role 삭제
					roleClear();

					Roles.AddUserToRoles(_identity.Name, selectedRole);
					displayMessage(_identity.Name, Properties.Resources.AssignRoleSuccess);
					changeRole(_identity.Name);
				}
				catch
				{
					displayMessage(_identity.Name, Properties.Resources.AssignRoleFailure);
				}
				finally
				{
					Cursor.Current = saveCursor;
				}
			}
			else
				MessageBox.Show("Role을 선택해 주십시오.");
		}

		private void roleClear()
		{
			if (validateAuthentication() == false)
				return;

			if (Roles.GetRolesForUser(_identity.Name).Length > 0)
            	Roles.RemoveUserFromRoles(_identity.Name, Roles.GetRolesForUser(_identity.Name));
		}
		#endregion

		#region Profile
		private void btnReadProfile_Click(object sender, EventArgs e)
		{
			if (validateAuthentication() == false)
				return;

			readNSetProfile();
		}

		private void readNSetProfile()
		{
			Profiles profile = readProfile();

			if (profile != null)
				setProfile(profile);
		}

		private Profiles readProfile()
		{
			Profiles profile = new Profiles();
			ProfileBase userProfile = ProfileBase.Create(_identity.Name);

			if (userProfile == null)
				return null;

			profile.UserName = (string)userProfile["Name"];
			profile.Sex = (bool)userProfile["Sex"];
			int color = (int)userProfile["Color"];

			if (color != 0)
				profile.FavoriteColor = color;
			
			displayMessage(_identity.Name, Properties.Resources.ReadProfileSuccess);

			return profile;
		}

		private void setProfile(Profiles profiles)
		{
			if (validateAuthentication() == false)
				return;

			Color color = Color.FromArgb(profiles.FavoriteColor);

			Text = string.Format("{0} 인증된 사용자 ID : {1}, 이름 : {2}, 성별 : {3}, 선호색 : {4}",
				Properties.Resources.AssemblyTitle,
				_identity.Name, 
				profiles.UserName,
				profiles.Sex == true ? "남" : "여",
				color.Name);

			tlpEtc.BackColor = color;
		}

		private void btnWriteProfile_Click(object sender, EventArgs e)
		{
			if (validateAuthentication() == false)
				return;

			ProfileMannager profileMannager = new ProfileMannager();
			Profiles profile = readProfile();
			
			profileMannager.Profiles = profile;

			DialogResult result = profileMannager.ShowDialog();

			if (result == DialogResult.OK)
			{
				writeProfile(profileMannager.Profiles);
				setProfile(profileMannager.Profiles);
			}
		}

		private void writeProfile(Profiles profiles)
		{
			Cursor saveCursor = Cursor.Current;
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				try
				{
					// Write the profile to the configured ASP.NET Profile provider
					ProfileBase userProfile = ProfileBase.Create(_identity.Name);

					userProfile["Name"] = profiles.UserName;
					userProfile["Sex"] = profiles.Sex;
					userProfile["Color"] = profiles.FavoriteColor;

					userProfile.Save();

					displayMessage(string.Empty, Properties.Resources.WriteProfileSuccess);
				}
				catch (Exception ex)
				{
					displayMessage(ex.Message, Properties.Resources.WriteProfileFailure);
				}
			}
			finally
			{
				Cursor.Current = saveCursor;
			}
		}
		#endregion

        #region Rule
		private void btnCheckRule_Click(object sender, EventArgs e)
		{
			checkRule(_identity.Name);
		}

		private void checkRule(string userId)
		{
			if (validateAuthentication() == false)
				return;

			string[] roles = Roles.GetRolesForUser(userId);
			IPrincipal principal = new GenericPrincipal(_identity, roles);

			if (principal != null)
			{
				string[] rules = new string[lstRule.Items.Count];

				resetRule();
				lstRule.Items.CopyTo(rules, 0);

				for (int i = 0; i < rules.Length; i++)
					if (_ruleProvider.Authorize(principal, rules[i]) == true)
						lstRule.SelectedIndex = i;

				if (lstRule.SelectedItems.Count > 0)
					displayMessage(_identity.Name, Properties.Resources.ReadRuleSucess);
				else
					displayMessage(_identity.Name, Properties.Resources.ReadRuleFailure);
			}
		}
		#endregion

		#region Cache Token
		private void btnObtainCacheToken_Click(object sender, EventArgs e)
		{
			if (validateAuthentication() == false)
				return;

			Cursor saveCursor = Cursor.Current;
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				try
				{
					_token = _cache.SaveIdentity(_identity);

					displayMessage(_token.Value, Properties.Resources.ObtainIdentityTokenSuccess);
				}
				catch
				{
					displayMessage(string.Empty, Properties.Resources.ObtainIdentityTokenFailure);
				}
			}
			finally
			{
				Cursor.Current = saveCursor;
			}
		}

		private void btnRetrievIdentity_Click(object sender, EventArgs e)
		{
			if (validateAuthentication() == false)
				return;

			if (_token != null)
			{
				IIdentity savedIdentity = _cache.GetIdentity(_token);

				if (savedIdentity != null)
					displayMessage(savedIdentity.Name, Properties.Resources.GetTokenSuccess);
				else
					displayMessage(string.Empty, "Identity 토큰이 제거 되었습니다.");
			}
			else
				displayMessage(string.Empty, Properties.Resources.GetTokenFailure);
		}

		private void btnTokenExpire_Click(object sender, EventArgs e)
		{
			if (validateAuthentication() == false)
				return;

			if (_token != null)
			{
				_cache.ExpireIdentity(_token);
				
				displayMessage(string.Empty, Properties.Resources.ExpireTokenSuccess);
			}
			else
				displayMessage(string.Empty, Properties.Resources.ExpireTokenFailure);
		}

		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FrmSecurity_Load(object sender, EventArgs e)
		{
			initClass();
			resetUsers();
			resetRole();
			initRule();
		}

		private void initClass()
		{
			_cache = SecurityCacheFactory.GetSecurityCacheProvider("Caching Store Provider");
			_ruleProvider = AuthorizationFactory.GetAuthorizationProvider("RuleProvider");
		}

        private void resetUsers()
		{
			lstUser.Items.Clear();

			foreach (MembershipUser membershipUser in Membership.GetAllUsers())
				lstUser.Items.Add(membershipUser.UserName);

			lstUser.SelectedIndex = -1;			
		}

		private void resetRole()
		{
			lstRole.Items.Clear();

			if (Roles.GetAllRoles().Length == 0)
				creteRoles();

			lstRole.Sorted = false;
			lstRole.SelectionMode = SelectionMode.MultiExtended;

			lstRole.Items.AddRange(Roles.GetAllRoles());
			lstRole.SelectedIndex = -1;
		}

		private static void creteRoles()
		{
			string[] roles = new string[] { "Admin", "Engineer", "User", "Ceo" };

			foreach (string role in roles)
				Roles.CreateRole(role);
		}

		private void initRule()
		{
			lstRule.Items.Clear();

			string[] rules = new string[] { "Read", "Write", "Delete", "Update" };

			lstRule.Items.AddRange(rules);

			resetRule();
		}

		private void resetRule()
		{
			lstRule.SelectedIndex = -1;
		}

        private void lstUser_SelectedIndexChanged(object sender, EventArgs e)
		{
			string userId = lstUser.SelectedItem.ToString();

			changeRole(userId);

			if (_identity != null && _identity.Name == userId)
            	checkRule(userId);
			else
				resetRule();
		}

		private void changeRole(string userId)
		{
			if (lstRole != null && lstRole.Items.Count > 0)
			{
				string[] roles = Roles.GetRolesForUser(userId);

				lstRole.SelectedIndex = -1;

				foreach (string role in roles)
					lstRole.SelectedItem = role;	
			}
		}

        #region displayMessage
		/// <summary>
		/// 
		/// </summary>
		/// <param name="target"></param>
		/// <param name="msg">msg must Properties.Resources</param>
		private void displayMessage(string target, string msg)
		{
			statusBar.Text = string.Format(msg, target);
		}
		#endregion

		private bool validateAuthentication()
		{
			bool result = false;

			if (_identity != null)
			{
				string userId = _identity.Name;

				result = _identity.IsAuthenticated;
				
				if (result == true)
                	displayMessage(userId, Properties.Resources.AuthenticateSuccess);
                else
					displayMessage(userId, Properties.Resources.AuthenticateFailure);                	                	
			}
			else
				displayMessage(string.Empty, "사용자 인증을 먼저 해주세요.");

            return result;            	
		}

		private void btnCreateBulkData_Click(object sender, EventArgs e)
		{
			/*
			 * 사용자 권한
			Role	Admin	Engineer	User	Ceo	
			Rulle					
			Read	V	V	V	V	
			Write	V	V	V		
			Delete	V				
			Update	V	V		V	
			 * 
 			 */
			// 1. 사용자 추가
			Dictionary<string, string> users = new Dictionary<string, string>();

			users.Add("xyz37", "1111");
			users.Add("xyz38", "1111");
			users.Add("xyz39", "1111");
			users.Add("gain26", "1111");

			foreach (string userId in users.Keys)
				Membership.CreateUser(userId, users[userId]);

			// 2. Role 생성
			// Formload시 생성(createRoles)

			// 3. Role 할당
			Dictionary<string, string[]> roles = new Dictionary<string, string[]>();

			roles.Add("xyz37", new string[] { "Ceo", "Engineer" });
			roles.Add("xyz38", new string[] { "User" });
			roles.Add("xyz39", new string[] { "Admin" });
			roles.Add("gain26", new string[] { "Ceo" });

			foreach (string userId in roles.Keys)
				Roles.AddUserToRoles(userId, roles[userId]);

			// 4. Profile 생성
			Dictionary<string, object[]> profiles = new Dictionary<string, object[]>();

			profiles.Add("xyz37", new object[] {"김기원", true, -16711808});
			profiles.Add("xyz38", new object[] {"김기원 2", true, -65536});
			profiles.Add("gain26", new object[] {"황태영", true, -16776961});

			ProfileBase userProfile = null;
			
			foreach (string userId in profiles.Keys)
			{
				userProfile = ProfileBase.Create(userId);

				userProfile["Name"] = profiles[userId][0];
				userProfile["Sex"] = profiles[userId][1];
				userProfile["Color"] = profiles[userId][2];

				userProfile.Save();	
			}

			resetUsers();

			MessageBox.Show("샘플 자료가 생성되었습니다.");
		}
	}
}