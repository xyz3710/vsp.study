using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Security_Application
{
	public partial class UserCredential : Form
	{
		private string _userId;
		private string _password;

		public UserCredential()
		{
			InitializeComponent();

			_userId = string.Empty;
			_password = string.Empty;
		}

		#region Properties
		/// <summary>
		/// Get or set UserId.
		/// </summary>
		public string UserId
		{
			get
			{
				return _userId;
			}
			set
			{
				_userId = value;
			}
		}
        
        /// <summary>
        /// Get or set Password.
        /// </summary>
		public string Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}        
        #endregion        

		private void btnOk_Click(object sender, EventArgs e)
		{
			if (txtUserId.Text == string.Empty || txtPassword.Text == string.Empty)
				MessageBox.Show("Please enter a user name and a password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			else
			{
				_userId = txtUserId.Text;
				_password = txtPassword.Text;

				DialogResult = DialogResult.OK;

				Close();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;

			Close();
		}

		private void UserCredential_LocationChanged(object sender, EventArgs e)
		{
			txtUserId.Select();
		}

		private void UserCredential_Activated(object sender, EventArgs e)
		{
			txtUserId.Text = _userId;
			txtPassword.Text = _password;

			if (txtUserId.Text == string.Empty)
				txtUserId.Select();
			else
				txtPassword.Select();
		}
	}
}