﻿namespace Security_Application
{
	partial class UserCredential
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpBody = new System.Windows.Forms.TableLayoutPanel();
			this.tlpButton = new System.Windows.Forms.TableLayoutPanel();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblTitle = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtUserId = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.tlpBody.SuspendLayout();
			this.tlpButton.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpBody
			// 
			this.tlpBody.ColumnCount = 2;
			this.tlpBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tlpBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tlpBody.Controls.Add(this.tlpButton, 0, 3);
			this.tlpBody.Controls.Add(this.lblTitle, 0, 0);
			this.tlpBody.Controls.Add(this.label2, 0, 1);
			this.tlpBody.Controls.Add(this.label3, 0, 2);
			this.tlpBody.Controls.Add(this.txtUserId, 1, 1);
			this.tlpBody.Controls.Add(this.txtPassword, 1, 2);
			this.tlpBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpBody.Location = new System.Drawing.Point(0, 0);
			this.tlpBody.Margin = new System.Windows.Forms.Padding(20);
			this.tlpBody.Name = "tlpBody";
			this.tlpBody.RowCount = 4;
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpBody.Size = new System.Drawing.Size(337, 180);
			this.tlpBody.TabIndex = 0;
			// 
			// tlpButton
			// 
			this.tlpButton.ColumnCount = 2;
			this.tlpBody.SetColumnSpan(this.tlpButton, 2);
			this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.Controls.Add(this.btnOk, 0, 0);
			this.tlpButton.Controls.Add(this.btnCancel, 1, 0);
			this.tlpButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpButton.Location = new System.Drawing.Point(8, 138);
			this.tlpButton.Margin = new System.Windows.Forms.Padding(8);
			this.tlpButton.Name = "tlpButton";
			this.tlpButton.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
			this.tlpButton.RowCount = 1;
			this.tlpButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.Size = new System.Drawing.Size(321, 34);
			this.tlpButton.TabIndex = 0;
			// 
			// btnOk
			// 
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnOk.Location = new System.Drawing.Point(13, 3);
			this.btnOk.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(137, 28);
			this.btnOk.TabIndex = 0;
			this.btnOk.Text = "&Ok";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCancel.Location = new System.Drawing.Point(170, 3);
			this.btnCancel.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(138, 28);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblTitle
			// 
			this.lblTitle.AutoSize = true;
			this.tlpBody.SetColumnSpan(this.lblTitle, 2);
			this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblTitle.Location = new System.Drawing.Point(3, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(331, 50);
			this.lblTitle.TabIndex = 1;
			this.lblTitle.Text = "인증할 사용자의 이름과 비밀번호를 입력하시오.";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(3, 50);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(95, 40);
			this.label2.TabIndex = 2;
			this.label2.Text = "User ID :";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(3, 90);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(95, 40);
			this.label3.TabIndex = 3;
			this.label3.Text = "Password :";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// txtUserId
			// 
			this.txtUserId.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtUserId.Location = new System.Drawing.Point(121, 60);
			this.txtUserId.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
			this.txtUserId.Name = "txtUserId";
			this.txtUserId.Size = new System.Drawing.Size(196, 21);
			this.txtUserId.TabIndex = 0;
			// 
			// txtPassword
			// 
			this.txtPassword.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtPassword.Location = new System.Drawing.Point(121, 100);
			this.txtPassword.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(196, 21);
			this.txtPassword.TabIndex = 1;
			// 
			// UserCredential
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(337, 180);
			this.Controls.Add(this.tlpBody);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "UserCredential";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UserCredential";
			this.Activated += new System.EventHandler(this.UserCredential_Activated);
			this.LocationChanged += new System.EventHandler(this.UserCredential_LocationChanged);
			this.tlpBody.ResumeLayout(false);
			this.tlpBody.PerformLayout();
			this.tlpButton.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpBody;
		private System.Windows.Forms.TableLayoutPanel tlpButton;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtUserId;
		private System.Windows.Forms.TextBox txtPassword;
	}
}