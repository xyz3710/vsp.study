using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using iDoIt.ExceptionHandling;

namespace ExceptionHandling.Manager
{
	public class GlobalException
	{
		public static void ProcessUnhandledException(Exception ex)
		{
			ProcessUnhandledException(ex, ExceptionPolicyType.Global);
		}

		public static void ProcessUnhandledException(Exception ex, ExceptionPolicyType policy)
		{
			try
			{
				// exception 처리를 Global Policy 정책을 사용하겠다고 정의
				bool rethrow = ExceptionPolicy.HandleException(ex, GetExceptionPolicyName(policy));

				// 또 다른 exception이 발생할 경우 치명적인 어플리케이션 오류로 간주하고 프로그램 종료
				if (rethrow == true)
					Application.Exit();
			}
			catch
			{
				StringBuilder errorMsg = new StringBuilder();
                
				errorMsg.Append("An unexpected exception occured while calling HandleException with policy 'Global Policy'. ");
				errorMsg.AppendFormat("Please check the event log for details about the exception.{0}", Environment.NewLine);
				errorMsg.AppendFormat("May by custom UI assemblies were not referrenced.{0}{0}", Environment.NewLine);

				MessageBox.Show(errorMsg.ToString(), "Application Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);

				Application.Exit();
			}
		}

		public static string GetExceptionPolicyName(ExceptionPolicyType policy)
		{
			string policyString = String.Empty;

			switch (policy)
			{
				case ExceptionPolicyType.Global:
				case ExceptionPolicyType.UserFriendly:
				case ExceptionPolicyType.Propagate:
				case ExceptionPolicyType.Replace:
				case ExceptionPolicyType.Wrap:
				case ExceptionPolicyType.Notify:
					policyString = policy.ToString();

					break;
				case ExceptionPolicyType.Suppress:
					policyString = "Handle and Resume";

					break;
				case ExceptionPolicyType.LogOnlyPolicy:
					policyString = "Log Only";

					break;
			}

			return string.Format("{0} Policy", policyString);
		}
	}
}
