using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;

namespace ExceptionHandling.Manager
{
	public enum ExceptionPolicyType
	{
		None=0,
		/// <summary>
		/// All used
		/// </summary>
		Global,
		/// <summary>
		/// All used
		/// </summary>
		UserFriendly,
		/// <summary>
		/// Basic Quick Start
		/// </summary>
		Propagate,
		/// <summary>
		/// Basic Quick Start
		/// </summary>
		Replace,
		/// <summary>
		/// Basic Quick Start
		/// </summary>
		Wrap,
		/// <summary>
		/// Basic Quick Start
		/// </summary>
		Suppress,
		/// <summary>
		/// with Logging
		/// </summary>
		Notify,
		/// <summary>
		/// with Logging
		/// </summary>
		LogOnlyPolicy,
	}
}
