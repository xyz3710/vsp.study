using System;
using System.Windows.Forms;
using System.Collections.Specialized;
using System.Text;

using iDoIt.Common.Configuration;
using iDoIt.ExceptionHandling;
using iDoIt.ExceptionHandling.Configuration;
using iDoIt.Common.Configuration.ObjectBuilder;

namespace ExceptionHandling.Handler.UserFriendly
{
	[ConfigurationElementType(typeof(CustomHandlerData))]
	public class UserFriendlyExceptionHandler : IExceptionHandler
	{
		
		public UserFriendlyExceptionHandler(NameValueCollection ignore)
		{
			
		}

		public Exception HandleException(Exception exception, Guid handlingInstanceId)
		{
			DialogResult result = showThreadExceptionDialog(exception);

			return exception;
		}

		/// <summary>
		/// Creates the error message and displays it.
		/// </summary>
		/// <param name="ex"></param>
		/// <returns></returns>
		private DialogResult showThreadExceptionDialog(Exception ex)
		{
			StringBuilder errorMsg = new StringBuilder();

			errorMsg.AppendFormat("예외가 시스템으로부터 발생했습니다.(An exceptinos is caught by System){0}{0}", Environment.NewLine);
			errorMsg.Append(ex.Message);

			return MessageBox.Show(errorMsg.ToString(), "UserFriendly Exception Handler", MessageBoxButtons.OK, MessageBoxIcon.Stop);
		}
	}
}
