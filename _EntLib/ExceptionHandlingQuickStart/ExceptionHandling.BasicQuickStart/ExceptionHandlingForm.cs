/**********************************************************************************************************************/
/*	Domain		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2007년 7월 12일 목요일 오전 11:42:51a
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	 ExceptionPolicy.HandleException(ex, "UI Policy")에서 사용할 Policy assembly를 참조에 넣어줘야 한다
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDoIt.ExceptionHandling;
using System.Reflection;
using ExceptionHandling.Manager;

namespace ExceptionHandling.BasicQuickStart
{
	public partial class ExceptionHandlingForm : Form
	{
		public ExceptionHandlingForm()
		{
			InitializeComponent();
		}

		private void btnUserFriendly_Click(object sender, EventArgs e)
		{
			try
			{
				throw new Exception("시스템 관리자에게 문의하세요(UserFriendly Policy)");
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.UserFriendly);
			}
		}

		private void btnCustomUI_Click(object sender, EventArgs e)
		{
			try
			{
				throw new Exception("시스템 관리자에게 문의하세요(Global Policy)");
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.Global);
			}
		}

		private void btnPropagate_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessWithPropagte();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.UserFriendly);
			}
		}

		private void btnWrap_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessWithWrap();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.UserFriendly);
			}
		}

		private void btnReplace_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessWithReplace();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.UserFriendly);
			}
		}

		private void btnNotify_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessAndNotify();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.UserFriendly);
			}
		}
		
		private void btnSuppress_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessAndResume();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.UserFriendly);
			}
		}

		private void btnPropagateGlobal_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessWithPropagte();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.Global);
			}
		}

		private void btnWrapGlobal_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessWithWrap();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.Global);
			}
		}

		private void btnReplaceGlobal_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessWithReplace();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.Global);
			}
		}

		private void btnNotifyGlobal_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessAndNotify();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.Global);
			}
		}

		private void btnSuppressGlobal_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessAndResume();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex, ExceptionPolicyType.Global);
			}
		}


	}
}