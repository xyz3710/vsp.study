using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ExceptionHandling.BusinessLayer;
using System.Diagnostics;
using iDoIt.ExceptionHandling;
using ExceptionHandling.Manager;

namespace ExceptionHandling.BasicQuickStart
{
	public class ThrowExceptions
	{
		/// <summary>
		/// Initializes a new instance of the ThrowExceptions class.
		/// </summary>
		public ThrowExceptions()
		{
		}

		private void processA()
		{
			throw new Exception("Original Exception : 치명적인 Exception 발생");
		}

		private void processB()
		{
			throw new DBConcurrencyException("Original Exception : DB Concurreny Exception 발생");
		}

		private void processC()
		{
			StackTrace st = new StackTrace();

			throw new System.Security.SecurityException(st.ToString());
		}

		private void processD()
		{
			string message = string.Format("Original Exception : Business Layer Exception 발생 : {0}{1}",
				@"h:\trace.log의 내용 확인", Environment.NewLine);

			throw new BusinessLayerException(message);
		}

		public bool ProcessWithPropagte()
		{
			try
			{
				processA();
			}
			catch (Exception ex)
			{
				bool rethrow = ExceptionPolicy.HandleException(ex, GlobalException.GetExceptionPolicyName(ExceptionPolicyType.Propagate));

				if (rethrow == true)
					throw;
			}

			return true;
		}

		public bool ProcessWithWrap()
		{
			try
			{
				processB();
			}
			catch (Exception ex)
			{
				bool rethrow = ExceptionPolicy.HandleException(ex, GlobalException.GetExceptionPolicyName(ExceptionPolicyType.Wrap));

				if (rethrow == true)
					throw;
			}

			return true;
		}

		public void ProcessWithReplace()
		{
			try
			{
				processC();
			}
			catch (Exception ex)
			{
				bool rethrow = ExceptionPolicy.HandleException(ex, GlobalException.GetExceptionPolicyName(ExceptionPolicyType.Replace));

				if (rethrow == true)
					throw;
			}
		}

		public void ProcessAndResume()
		{
			try
			{
				processD();
			}
			catch (Exception ex)
			{
				bool rethrow = ExceptionPolicy.HandleException(ex, GlobalException.GetExceptionPolicyName(ExceptionPolicyType.Suppress));

				if (rethrow == true)
					throw;
			}
		}

		/// <summary>
		/// used With Logging
		/// </summary>
		public void ProcessAndNotify()
		{
			try
			{
				processD();
			}
			catch (Exception ex)
			{
				bool rethrow = ExceptionPolicy.HandleException(ex, GlobalException.GetExceptionPolicyName(ExceptionPolicyType.Notify));

				if (rethrow == true)
					throw;
			}
		}
	}
}
