﻿namespace ExceptionHandling.BasicQuickStart
{
	partial class ExceptionHandlingForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnUserFriendly = new System.Windows.Forms.Button();
			this.btnCustomUI = new System.Windows.Forms.Button();
			this.btnPropagate = new System.Windows.Forms.Button();
			this.btnWrap = new System.Windows.Forms.Button();
			this.btnReplace = new System.Windows.Forms.Button();
			this.btnSuppress = new System.Windows.Forms.Button();
			this.btnNotify = new System.Windows.Forms.Button();
			this.btnPropagateGlobal = new System.Windows.Forms.Button();
			this.btnWrapGlobal = new System.Windows.Forms.Button();
			this.btnReplaceGlobal = new System.Windows.Forms.Button();
			this.btnNotifyGlobal = new System.Windows.Forms.Button();
			this.btnSupressGlobal = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnUserFriendly
			// 
			this.btnUserFriendly.Location = new System.Drawing.Point(18, 21);
			this.btnUserFriendly.Name = "btnUserFriendly";
			this.btnUserFriendly.Size = new System.Drawing.Size(212, 23);
			this.btnUserFriendly.TabIndex = 0;
			this.btnUserFriendly.Text = "UserFriendly Exception";
			this.btnUserFriendly.UseVisualStyleBackColor = true;
			this.btnUserFriendly.Click += new System.EventHandler(this.btnUserFriendly_Click);
			// 
			// btnCustomUI
			// 
			this.btnCustomUI.Location = new System.Drawing.Point(248, 21);
			this.btnCustomUI.Name = "btnCustomUI";
			this.btnCustomUI.Size = new System.Drawing.Size(212, 23);
			this.btnCustomUI.TabIndex = 6;
			this.btnCustomUI.Text = "CustomUI(GlobalPolicy)";
			this.btnCustomUI.UseVisualStyleBackColor = true;
			this.btnCustomUI.Click += new System.EventHandler(this.btnCustomUI_Click);
			// 
			// btnPropagate
			// 
			this.btnPropagate.Location = new System.Drawing.Point(18, 63);
			this.btnPropagate.Name = "btnPropagate";
			this.btnPropagate.Size = new System.Drawing.Size(212, 23);
			this.btnPropagate.TabIndex = 1;
			this.btnPropagate.Text = "Propagate";
			this.btnPropagate.UseVisualStyleBackColor = true;
			this.btnPropagate.Click += new System.EventHandler(this.btnPropagate_Click);
			// 
			// btnWrap
			// 
			this.btnWrap.Location = new System.Drawing.Point(18, 105);
			this.btnWrap.Name = "btnWrap";
			this.btnWrap.Size = new System.Drawing.Size(212, 23);
			this.btnWrap.TabIndex = 2;
			this.btnWrap.Text = "Wrap one Ex with another";
			this.btnWrap.UseVisualStyleBackColor = true;
			this.btnWrap.Click += new System.EventHandler(this.btnWrap_Click);
			// 
			// btnReplace
			// 
			this.btnReplace.Location = new System.Drawing.Point(18, 147);
			this.btnReplace.Name = "btnReplace";
			this.btnReplace.Size = new System.Drawing.Size(212, 23);
			this.btnReplace.TabIndex = 3;
			this.btnReplace.Text = "Replace one Ex with another";
			this.btnReplace.UseVisualStyleBackColor = true;
			this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
			// 
			// btnSuppress
			// 
			this.btnSuppress.Location = new System.Drawing.Point(18, 231);
			this.btnSuppress.Name = "btnSuppress";
			this.btnSuppress.Size = new System.Drawing.Size(212, 23);
			this.btnSuppress.TabIndex = 5;
			this.btnSuppress.Text = "Process and suppress an Ex";
			this.btnSuppress.UseVisualStyleBackColor = true;
			this.btnSuppress.Click += new System.EventHandler(this.btnSuppress_Click);
			// 
			// btnNotify
			// 
			this.btnNotify.Location = new System.Drawing.Point(18, 189);
			this.btnNotify.Name = "btnNotify";
			this.btnNotify.Size = new System.Drawing.Size(212, 23);
			this.btnNotify.TabIndex = 4;
			this.btnNotify.Text = "Notify Business Layer Exception";
			this.btnNotify.UseVisualStyleBackColor = true;
			this.btnNotify.Click += new System.EventHandler(this.btnNotify_Click);
			// 
			// btnPropagateGlobal
			// 
			this.btnPropagateGlobal.Location = new System.Drawing.Point(248, 63);
			this.btnPropagateGlobal.Name = "btnPropagateGlobal";
			this.btnPropagateGlobal.Size = new System.Drawing.Size(212, 23);
			this.btnPropagateGlobal.TabIndex = 7;
			this.btnPropagateGlobal.Text = "Propagate";
			this.btnPropagateGlobal.UseVisualStyleBackColor = true;
			this.btnPropagateGlobal.Click += new System.EventHandler(this.btnPropagateGlobal_Click);
			// 
			// btnWrapGlobal
			// 
			this.btnWrapGlobal.Location = new System.Drawing.Point(248, 105);
			this.btnWrapGlobal.Name = "btnWrapGlobal";
			this.btnWrapGlobal.Size = new System.Drawing.Size(212, 23);
			this.btnWrapGlobal.TabIndex = 8;
			this.btnWrapGlobal.Text = "Wrap one Ex with another";
			this.btnWrapGlobal.UseVisualStyleBackColor = true;
			this.btnWrapGlobal.Click += new System.EventHandler(this.btnWrapGlobal_Click);
			// 
			// btnReplaceGlobal
			// 
			this.btnReplaceGlobal.Location = new System.Drawing.Point(248, 147);
			this.btnReplaceGlobal.Name = "btnReplaceGlobal";
			this.btnReplaceGlobal.Size = new System.Drawing.Size(212, 23);
			this.btnReplaceGlobal.TabIndex = 9;
			this.btnReplaceGlobal.Text = "Replace one Ex with another";
			this.btnReplaceGlobal.UseVisualStyleBackColor = true;
			this.btnReplaceGlobal.Click += new System.EventHandler(this.btnReplaceGlobal_Click);
			// 
			// btnNotifyGlobal
			// 
			this.btnNotifyGlobal.Location = new System.Drawing.Point(248, 189);
			this.btnNotifyGlobal.Name = "btnNotifyGlobal";
			this.btnNotifyGlobal.Size = new System.Drawing.Size(212, 23);
			this.btnNotifyGlobal.TabIndex = 10;
			this.btnNotifyGlobal.Text = "Notify Business Layer Exception";
			this.btnNotifyGlobal.UseVisualStyleBackColor = true;
			this.btnNotifyGlobal.Click += new System.EventHandler(this.btnNotifyGlobal_Click);
			// 
			// btnSupressGlobal
			// 
			this.btnSupressGlobal.Location = new System.Drawing.Point(248, 231);
			this.btnSupressGlobal.Name = "btnSupressGlobal";
			this.btnSupressGlobal.Size = new System.Drawing.Size(212, 23);
			this.btnSupressGlobal.TabIndex = 11;
			this.btnSupressGlobal.Text = "Process and suppress an Ex";
			this.btnSupressGlobal.UseVisualStyleBackColor = true;
			this.btnSupressGlobal.Click += new System.EventHandler(this.btnSuppressGlobal_Click);
			// 
			// ExceptionHandlingForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(479, 274);
			this.Controls.Add(this.btnCustomUI);
			this.Controls.Add(this.btnSupressGlobal);
			this.Controls.Add(this.btnSuppress);
			this.Controls.Add(this.btnNotifyGlobal);
			this.Controls.Add(this.btnNotify);
			this.Controls.Add(this.btnReplaceGlobal);
			this.Controls.Add(this.btnReplace);
			this.Controls.Add(this.btnWrapGlobal);
			this.Controls.Add(this.btnWrap);
			this.Controls.Add(this.btnPropagateGlobal);
			this.Controls.Add(this.btnPropagate);
			this.Controls.Add(this.btnUserFriendly);
			this.Name = "ExceptionHandlingForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Exception Handling";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnUserFriendly;
		private System.Windows.Forms.Button btnCustomUI;
		private System.Windows.Forms.Button btnPropagate;
		private System.Windows.Forms.Button btnWrap;
		private System.Windows.Forms.Button btnReplace;
		private System.Windows.Forms.Button btnSuppress;
		private System.Windows.Forms.Button btnNotify;
		private System.Windows.Forms.Button btnPropagateGlobal;
		private System.Windows.Forms.Button btnWrapGlobal;
		private System.Windows.Forms.Button btnReplaceGlobal;
		private System.Windows.Forms.Button btnNotifyGlobal;
		private System.Windows.Forms.Button btnSupressGlobal;
	}
}

