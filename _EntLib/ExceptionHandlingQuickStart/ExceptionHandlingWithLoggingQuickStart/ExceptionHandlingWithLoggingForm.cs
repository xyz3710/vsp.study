using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDoIt.Logging;
using iDoIt.ExceptionHandling;
using ExceptionHandling.Manager;
using ExceptionHandling.BasicQuickStart;


namespace ExceptionHandlingWithLoggingQuickStart
{
	public partial class ExceptionHandlingWithLoggingForm : Form
	{
		public ExceptionHandlingWithLoggingForm()
		{
			InitializeComponent();
		}

		private void process()
		{
			throw new Exception("Quick Start Generated Exception");
		}

		private void btnLogOnlyPolicy_Click(object sender, EventArgs e)
		{
			try
			{
				try
				{
					process();
				}
				catch (Exception ex)
				{
					bool rethrow = ExceptionPolicy.HandleException(ex, GlobalException.GetExceptionPolicyName(ExceptionPolicyType.LogOnlyPolicy));

					if (rethrow == true)
						throw;
				}
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex);
			}
		}

		private void btnNotify_Click(object sender, EventArgs e)
		{
			try
			{
				ThrowExceptions exp = new ThrowExceptions();

				exp.ProcessAndNotify();
			}
			catch (Exception ex)
			{
				GlobalException.ProcessUnhandledException(ex);
			}
		}
	}
}