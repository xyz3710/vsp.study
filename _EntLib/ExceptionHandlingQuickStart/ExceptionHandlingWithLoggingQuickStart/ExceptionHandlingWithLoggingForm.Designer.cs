﻿namespace ExceptionHandlingWithLoggingQuickStart
{
	partial class ExceptionHandlingWithLoggingForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnLogOnlyPolicy = new System.Windows.Forms.Button();
			this.btnNotify = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnLogOnlyPolicy
			// 
			this.btnLogOnlyPolicy.Location = new System.Drawing.Point(12, 12);
			this.btnLogOnlyPolicy.Name = "btnLogOnlyPolicy";
			this.btnLogOnlyPolicy.Size = new System.Drawing.Size(168, 23);
			this.btnLogOnlyPolicy.TabIndex = 0;
			this.btnLogOnlyPolicy.Text = "Log Only Policy";
			this.btnLogOnlyPolicy.UseVisualStyleBackColor = true;
			this.btnLogOnlyPolicy.Click += new System.EventHandler(this.btnLogOnlyPolicy_Click);
			// 
			// btnNotify
			// 
			this.btnNotify.Location = new System.Drawing.Point(12, 86);
			this.btnNotify.Name = "btnNotify";
			this.btnNotify.Size = new System.Drawing.Size(168, 23);
			this.btnNotify.TabIndex = 0;
			this.btnNotify.Text = "Event Log Logging";
			this.btnNotify.UseVisualStyleBackColor = true;
			this.btnNotify.Click += new System.EventHandler(this.btnNotify_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 38);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(516, 36);
			this.label1.TabIndex = 1;
			this.label1.Text = "app.config에서 Exception->Log Only Policy 정책에서 Logging Handler.LogCategory에 의해\r\nEve" +
    "ntLog의 ExceptionHandlingWithLoggingQuickStart 아래나\r\nH:\\Trace.log로 로그가 남는다";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 112);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(455, 24);
			this.label2.TabIndex = 1;
			this.label2.Text = "Notify Policy는 H:\\Trace.log에 Log를 남기는 Logging Handler와\r\nReplace Handler에 의해 처리되는 " +
    "2개의 BusinessLayerException handler가 있다";
			// 
			// ExceptionHandlingWithLoggingForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(549, 156);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnNotify);
			this.Controls.Add(this.btnLogOnlyPolicy);
			this.Name = "ExceptionHandlingWithLoggingForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ExceptionHandlingWithLogging";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnLogOnlyPolicy;
		private System.Windows.Forms.Button btnNotify;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
	}
}

