using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ExceptionHandling.DataLayer
{
	public class RecoverableUpdateException : ApplicationException
	{
		/// <summary>
		/// Initializes a new instance of the RecoverableUpdateException class.
		/// </summary>
		public RecoverableUpdateException()
			: base()
		{
		}

		/// <summary>
		/// Initializes a new instance of the RecoverableUpdateException class.
		/// </summary>
		public RecoverableUpdateException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of the RecoverableUpdateException class.
		/// </summary>
		public RecoverableUpdateException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		/// <summary>
		/// Initializes a new instance of the RecoverableUpdateException class.
		/// </summary>
		public RecoverableUpdateException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
