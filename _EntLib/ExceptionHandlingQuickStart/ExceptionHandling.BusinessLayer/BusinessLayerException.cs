using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace ExceptionHandling.BusinessLayer
{
	public class BusinessLayerException : ApplicationException
	{
		/// <summary>
		/// Initializes a new instance of the BusinessLayerException class.
		/// </summary>
		public BusinessLayerException()
			: base()
		{
		}

		/// <summary>
		/// Initializes a new instance of the BusinessLayerException class.
		/// </summary>
		public BusinessLayerException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Initializes a new instance of the BusinessLayerException class.
		/// </summary>
		public BusinessLayerException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		/// <summary>
		/// Initializes a new instance of the BusinessLayerException class.
		/// </summary>
		public BusinessLayerException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
