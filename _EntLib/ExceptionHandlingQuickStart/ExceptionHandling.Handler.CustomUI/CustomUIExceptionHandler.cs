using System;
using System.Collections.Generic;
using System.Text;

using iDoIt.ExceptionHandling;
using iDoIt.Common.Configuration;
using iDoIt.ExceptionHandling.Configuration;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace ExceptionHandling.Handler.CustomUI
{
	[ConfigurationElementType(typeof(CustomHandlerData))]
	public class CustomUIExceptionHandler : IExceptionHandler
	{
		/// <summary>
		/// Initializes a new instance of the AppUIExceptionHandler class.
		/// </summary>
		public CustomUIExceptionHandler(NameValueCollection ignore)
		{
		}

		#region IExceptionHandler ���

		public Exception HandleException(Exception exception, Guid handlingInstanceId)
		{
			CustomUIExceptionHandlerForm uiForm = new CustomUIExceptionHandlerForm();

			uiForm.Exception = exception;
			uiForm.Text = "CustomUI Exception Handler";

			uiForm.ShowDialog();

			return exception;
		}

		#endregion
	}
}
