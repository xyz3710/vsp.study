using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace ExceptionHandling.Handler.CustomUI
{
	public partial class CustomUIExceptionHandlerForm : Form
	{
		private Exception _exception;

		public Exception Exception
		{
			get
			{
				return _exception;
			}
			set
			{
				_exception = value;
			}
		}

		public CustomUIExceptionHandlerForm()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void AppUIExceptionHandler_Load(object sender, EventArgs e)
		{
			StringBuilder errMsg = new StringBuilder();

			errMsg.Append(_exception.Message);
			errMsg.AppendFormat("{0}{0}{1}", Environment.NewLine, _exception.StackTrace);
			errMsg.AppendFormat("{0}{0}{1}", Environment.NewLine, _exception.InnerException);

			txtResult.Text =  errMsg.ToString();
		}
	}
}