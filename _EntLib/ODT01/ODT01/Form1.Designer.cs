﻿namespace ODT01
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			Oracle.DataAccess.Client.OracleParameter oracleParameter1 = new Oracle.DataAccess.Client.OracleParameter();
			Oracle.DataAccess.Client.OracleParameter oracleParameter2 = new Oracle.DataAccess.Client.OracleParameter();
			Oracle.DataAccess.Client.OracleParameter oracleParameter3 = new Oracle.DataAccess.Client.OracleParameter();
			Oracle.DataAccess.Client.OracleParameter oracleParameter4 = new Oracle.DataAccess.Client.OracleParameter();
			Oracle.DataAccess.Client.OracleParameter oracleParameter5 = new Oracle.DataAccess.Client.OracleParameter();
			Oracle.DataAccess.Client.OracleParameter oracleParameter6 = new Oracle.DataAccess.Client.OracleParameter();
			this.dependentsOracleDataAdapter1 = new Oracle.DataAccess.Client.OracleDataAdapter();
			this.dependentsInsertOracleCommand1 = new Oracle.DataAccess.Client.OracleCommand();
			this.dependentsOracleConnection1 = new Oracle.DataAccess.Client.OracleConnection();
			this.dependentsSelectOracleCommand1 = new Oracle.DataAccess.Client.OracleCommand();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dependentsOracleDataAdapter1
			// 
			this.dependentsOracleDataAdapter1.InsertCommand = this.dependentsInsertOracleCommand1;
			this.dependentsOracleDataAdapter1.SelectCommand = this.dependentsSelectOracleCommand1;
			// 
			// dependentsInsertOracleCommand1
			// 
			this.dependentsInsertOracleCommand1.CommandText = resources.GetString("dependentsInsertOracleCommand1.CommandText");
			this.dependentsInsertOracleCommand1.Connection = this.dependentsOracleConnection1;
			oracleParameter1.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Varchar2;
			oracleParameter1.ParameterName = ":current_FIRSTNAME_param0";
			oracleParameter1.SourceColumn = "FIRSTNAME";
			oracleParameter2.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Varchar2;
			oracleParameter2.ParameterName = ":current_LASTNAME_param1";
			oracleParameter2.SourceColumn = "LASTNAME";
			oracleParameter3.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Date;
			oracleParameter3.ParameterName = ":current_BIRTHDATE_param2";
			oracleParameter3.SourceColumn = "BIRTHDATE";
			oracleParameter4.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Varchar2;
			oracleParameter4.ParameterName = ":current_RELATIONSHIP_param3";
			oracleParameter4.SourceColumn = "RELATIONSHIP";
			oracleParameter5.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Int32;
			oracleParameter5.ParameterName = ":current_EMPLOYEEID_param4";
			oracleParameter5.SourceColumn = "EMPLOYEEID";
			oracleParameter6.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Int16;
			oracleParameter6.ParameterName = ":current_DEPENDENTID_param5";
			oracleParameter6.SourceColumn = "DEPENDENTID";
			this.dependentsInsertOracleCommand1.Parameters.Add(oracleParameter1);
			this.dependentsInsertOracleCommand1.Parameters.Add(oracleParameter2);
			this.dependentsInsertOracleCommand1.Parameters.Add(oracleParameter3);
			this.dependentsInsertOracleCommand1.Parameters.Add(oracleParameter4);
			this.dependentsInsertOracleCommand1.Parameters.Add(oracleParameter5);
			this.dependentsInsertOracleCommand1.Parameters.Add(oracleParameter6);
			// 
			// dependentsOracleConnection1
			// 
			this.dependentsOracleConnection1.ConnectionString = "USER ID=xyz37;DATA SOURCE=MES;PASSWORD=iset;PERSIST SECURITY INFO=true;";
			// 
			// dependentsSelectOracleCommand1
			// 
			this.dependentsSelectOracleCommand1.CommandText = "SELECT * FROM DEPENDENTS";
			this.dependentsSelectOracleCommand1.Connection = this.dependentsOracleConnection1;
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.dataGridView1.Location = new System.Drawing.Point(0, 313);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowTemplate.Height = 23;
			this.dataGridView1.Size = new System.Drawing.Size(871, 200);
			this.dataGridView1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(871, 513);
			this.Controls.Add(this.dataGridView1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Oracle.DataAccess.Client.OracleDataAdapter dependentsOracleDataAdapter1;
		private Oracle.DataAccess.Client.OracleCommand dependentsInsertOracleCommand1;
		private Oracle.DataAccess.Client.OracleConnection dependentsOracleConnection1;
		private Oracle.DataAccess.Client.OracleCommand dependentsSelectOracleCommand1;
		private System.Windows.Forms.DataGridView dataGridView1;

	}
}

