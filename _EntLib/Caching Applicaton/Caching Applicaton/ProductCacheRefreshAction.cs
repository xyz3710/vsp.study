using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDoIt.Caching;
using iDoIt.Caching.Expirations;

namespace Caching_Applicaton
{
	public class CacheRefreshAction : ICacheItemRefreshAction
	{
		public delegate void RefreshHandler(object sender, CacheRefreshEventArgs e);

		public event RefreshHandler Expired;

		protected void OnExpired()
		{
			if (Expired != null)
				Expired(this, null);
		}

		#region ICacheItemRefreshAction 

		public void Refresh(string removedKey, object expiredValue, CacheItemRemovedReason removalReason)
		{
			CacheRefreshEventArgs e = new CacheRefreshEventArgs();

			e.RemovedKey = removedKey;
			e.ExpiredValue = expiredValue;
			e.Reason = removalReason;

			Expired(this, e);

			Product product = e.ExpiredValue as Product;

			if (product != null)
				product.PrintString("Expired");
		}

		#endregion
	}

	public class CacheRefreshEventArgs: EventArgs
	{
		private string _removedKey;
		private object _expiredValue;
		private CacheItemRemovedReason _reason;

		/// <summary>
		/// Initializes a new instance of the CacheRefreshEventArgs class.
		/// </summary>
		public CacheRefreshEventArgs()
			: base()
		{
		}

		#region Properties
		/// <summary>
		/// Get or set RemovedKey.
		/// </summary>
		public string RemovedKey
		{
			get
			{
				return _removedKey;
			}
			set
			{
				_removedKey = value;
			}
		}
        
        /// <summary>
        /// Get or set ExpiredValue.
        /// </summary>
		public object ExpiredValue
		{
			get
			{
				return _expiredValue;
			}
			set
			{
				_expiredValue = value;
			}
		}
        
        /// <summary>
        /// Get or set Reason.
        /// </summary>
		public CacheItemRemovedReason Reason
		{
			get
			{
				return _reason;
			}
			set
			{
				_reason = value;
			}
		}
        
        #endregion
        
	}
}
