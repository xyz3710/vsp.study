﻿namespace Caching_Applicaton
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.btnRead = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.txtCurrent = new System.Windows.Forms.TextBox();
			this.mtxtId = new System.Windows.Forms.MaskedTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.ticker = new System.Windows.Forms.Timer(this.components);
			this.btnAuto = new System.Windows.Forms.Button();
			this.btnStop = new System.Windows.Forms.Button();
			this.lblExplain = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
			this.SuspendLayout();
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(5, 10);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(79, 23);
			this.btnAdd.TabIndex = 0;
			this.btnAdd.Text = "&Add";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Location = new System.Drawing.Point(105, 10);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(79, 23);
			this.btnDelete.TabIndex = 1;
			this.btnDelete.Text = "&Delete";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnRefresh
			// 
			this.btnRefresh.Location = new System.Drawing.Point(405, 10);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(79, 23);
			this.btnRefresh.TabIndex = 4;
			this.btnRefresh.Text = "&Refresh";
			this.btnRefresh.UseVisualStyleBackColor = true;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// btnRead
			// 
			this.btnRead.Location = new System.Drawing.Point(305, 10);
			this.btnRead.Name = "btnRead";
			this.btnRead.Size = new System.Drawing.Size(79, 23);
			this.btnRead.TabIndex = 3;
			this.btnRead.Text = "R&ead";
			this.btnRead.UseVisualStyleBackColor = true;
			this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
			// 
			// btnClear
			// 
			this.btnClear.Location = new System.Drawing.Point(505, 10);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(79, 23);
			this.btnClear.TabIndex = 5;
			this.btnClear.Text = "&Clear";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// txtResult
			// 
			this.txtResult.AcceptsReturn = true;
			this.txtResult.AcceptsTab = true;
			this.txtResult.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtResult.Location = new System.Drawing.Point(6, 41);
			this.txtResult.Multiline = true;
			this.txtResult.Name = "txtResult";
			this.txtResult.ReadOnly = true;
			this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtResult.Size = new System.Drawing.Size(582, 320);
			this.txtResult.TabIndex = 8;
			// 
			// txtCurrent
			// 
			this.txtCurrent.AcceptsReturn = true;
			this.txtCurrent.AcceptsTab = true;
			this.txtCurrent.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.txtCurrent.Location = new System.Drawing.Point(6, 397);
			this.txtCurrent.Multiline = true;
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.ReadOnly = true;
			this.txtCurrent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtCurrent.Size = new System.Drawing.Size(582, 320);
			this.txtCurrent.TabIndex = 9;
			// 
			// mtxtId
			// 
			this.mtxtId.HidePromptOnLeave = true;
			this.mtxtId.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert;
			this.mtxtId.Location = new System.Drawing.Point(205, 10);
			this.mtxtId.Mask = "9999999999";
			this.mtxtId.Name = "mtxtId";
			this.mtxtId.PromptChar = ' ';
			this.mtxtId.Size = new System.Drawing.Size(79, 21);
			this.mtxtId.TabIndex = 2;
			this.mtxtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.mtxtId.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			this.mtxtId.ValidatingType = typeof(int);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label2.Location = new System.Drawing.Point(511, 375);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(71, 19);
			this.label2.TabIndex = 8;
			this.label2.Text = "Current";
			// 
			// ticker
			// 
			this.ticker.Tick += new System.EventHandler(this.ticker_Tick);
			// 
			// btnAuto
			// 
			this.btnAuto.Location = new System.Drawing.Point(73, 367);
			this.btnAuto.Name = "btnAuto";
			this.btnAuto.Size = new System.Drawing.Size(79, 23);
			this.btnAuto.TabIndex = 6;
			this.btnAuto.Text = "Auto";
			this.btnAuto.UseVisualStyleBackColor = true;
			this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
			// 
			// btnStop
			// 
			this.btnStop.Location = new System.Drawing.Point(430, 368);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(79, 23);
			this.btnStop.TabIndex = 7;
			this.btnStop.Text = "Stop";
			this.btnStop.UseVisualStyleBackColor = true;
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// lblExplain
			// 
			this.lblExplain.AutoSize = true;
			this.lblExplain.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblExplain.Location = new System.Drawing.Point(154, 371);
			this.lblExplain.Name = "lblExplain";
			this.lblExplain.Size = new System.Drawing.Size(274, 14);
			this.lblExplain.TabIndex = 8;
			this.lblExplain.Text = "25개의 Data를 2초 단위로 Add,  Expire Time 3초";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label1.Location = new System.Drawing.Point(10, 364);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 19);
			this.label1.TabIndex = 8;
			this.label1.Text = "Result";
			// 
			// vScrollBar1
			// 
			this.vScrollBar1.Location = new System.Drawing.Point(572, 41);
			this.vScrollBar1.Name = "vScrollBar1";
			this.vScrollBar1.Size = new System.Drawing.Size(16, 320);
			this.vScrollBar1.TabIndex = 10;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(593, 723);
			this.Controls.Add(this.vScrollBar1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lblExplain);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.mtxtId);
			this.Controls.Add(this.txtCurrent);
			this.Controls.Add(this.txtResult);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btnRead);
			this.Controls.Add(this.btnRefresh);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnStop);
			this.Controls.Add(this.btnAuto);
			this.Controls.Add(this.btnAdd);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Button btnRead;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.TextBox txtCurrent;
		private System.Windows.Forms.MaskedTextBox mtxtId;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Timer ticker;
		private System.Windows.Forms.Button btnAuto;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Label lblExplain;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.VScrollBar vScrollBar1;
	}
}

