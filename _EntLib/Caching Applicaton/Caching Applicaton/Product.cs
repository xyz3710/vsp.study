using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDoIt.Caching;
using iDoIt.Caching.Expirations;

namespace Caching_Applicaton
{
	public class Product
	{
		private string _id;
		private string _name;
		private int _price;

		/// <summary>
		/// Initializes a new instance of the Product class.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="price"></param>
		public Product(string id, string name, int price)
		{
			_id = id;
			_name = name;
			_price = price;
		}

		#region Properties
		/// <summary>
		/// Get or set Id.
		/// </summary>
		public string Id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		/// <summary>
		/// Get or set Name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Get or set Price.
		/// </summary>
		public int Price
		{
			get
			{
				return _price;
			}
			set
			{
				_price = value;
			}
		}

		#endregion

		public string PrintString(string state)
		{
			string indicate = state != string.Empty ? string.Format(" <== [{0, -15} {1,10}] ", state, DateTime.Now.TimeOfDay.ToString()) 
				: string.Empty;

			return string.Format("{0, -15}, {1, -30}, {2, 10}{3}{4}",
				Id, Name, Price, indicate, Environment.NewLine);
		}
	}
}
