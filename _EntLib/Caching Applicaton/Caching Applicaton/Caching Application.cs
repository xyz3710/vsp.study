using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iDoIt.Caching;
using iDoIt.Caching.Expirations;
using System.Collections;

namespace Caching_Applicaton
{
	public partial class Form1 : Form
	{
		private readonly double AutoIntervalSec = 2;
		private readonly double ExpiredTimeSec = 3.5;
		private readonly int MaxCount = 25;

		private CacheManager _cacheManager;
		private ArrayList _keys;
		private Random _random;
		private CacheRefreshAction _cacheRefreshAction;
		private static int _count;

		public Form1()
		{
			InitializeComponent();

			if (_cacheManager == null)
            	_cacheManager = CacheFactory.GetCacheManager();

			if (_keys == null)
				_keys = new ArrayList();

			if (_random == null)
            	_random = new Random(1000);

			if (_cacheRefreshAction == null)
			{
				_cacheRefreshAction = new CacheRefreshAction();

				_cacheRefreshAction.Expired += new CacheRefreshAction.RefreshHandler(_cacheRefreshAction_Expired);
			}
		}
		
		private void btnAdd_Click(object sender, EventArgs e)
		{
			addProduct();
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (mtxtId.Text == string.Empty)
				return;

			int key = int.Parse(mtxtId.Text.Substring(3));

			deleteProduct(getId(key));			
		}

		private void btnRead_Click(object sender, EventArgs e)
		{
			if (mtxtId.Text == string.Empty)
				return;

			int key = int.Parse(mtxtId.Text.Substring(3));

			readProduct(getId(key));
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			refreshProduct();
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			clearProduct();
		}

		private void addProduct()
		{
			string id = getId(getKey());
			string name = "Product " + id;
			int price = _random.Next(10000);

			Product product = new Product(id, name, price);

			_cacheManager.Add(product.Id, product, CacheItemPriority.Normal, _cacheRefreshAction,
				new SlidingTime(TimeSpan.FromMinutes(ExpiredTimeSec * 0.01)));
			_keys.Add(id);

			message(product, "Add");
		}

		private int getKey()
		{
			if (_keys.Count == 0)
				return 1;

			_keys.Sort();
			string key = (string)_keys[_keys.Count - 1];

			return int.Parse(key.Substring(3)) + 1;
		}

		private string getId(int key)
		{
			return string.Format("PID{0:0000000}", key);
		}

		private void deleteProduct(string key)
		{
			Product product = readProduct(key, false);

			_cacheManager.Remove(key);
			_keys.Remove(key);

			message(product, "Delete");
		}

		private Product readProduct(string id, bool showMessage)
		{
			Product product = _cacheManager.GetData(id) as Product;

			if (showMessage == true)
            	message(product, "Read");

			return product;
		}

		private Product readProduct(string id)
		{
			return readProduct(id, true);
		}
		
		void _cacheRefreshAction_Expired(object sender, CacheRefreshEventArgs e)
		{
			Refresh();

			if (_keys.Contains(e.RemovedKey) == true)
				_keys.Remove(e.RemovedKey);

			Product product = e.ExpiredValue as Product;

			if (product != null)
				txtResult.Text += product.PrintString("Expired");

			refreshProduct();
		}

        private void refreshProduct()
		{
			txtCurrent.Clear();
			_keys.Sort();
			
			for (int i = 0; i < _keys.Count; i++)
			{
				Product product = readProduct((string)_keys[i], false);

				if (product != null)
                	txtCurrent.Text += product.PrintString(string.Empty);
			}
		}

		private void clearProduct()
		{
			_cacheManager.Flush();
			_keys.Clear();
			txtResult.Clear();
			txtCurrent.Clear();
		}
		
		private void message(Product product, string action)
		{
			txtResult.Text += product.PrintString(action);

			refreshProduct();
		}

		private void btnAuto_Click(object sender, EventArgs e)
		{
			tickerStart();
		}

		private void btnStop_Click(object sender, EventArgs e)
		{
			tickerStop();
		}

		private void tickerStart()
		{
			if (ticker.Enabled == false)
			{
				ticker.Enabled = true;
				ticker.Interval = (int)(AutoIntervalSec * 1000);
				ticker.Start();
				_count = 1;
			}
		}

		private void tickerStop()
		{
			if (ticker.Enabled == true)
			{
				ticker.Stop();
				ticker.Enabled = false;
			}
		}

		private void ticker_Tick(object sender, EventArgs e)
		{
			addProduct();

			if (_count++ >= MaxCount)
				tickerStop();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			lblExplain.Text = string.Format("{0}개의 Data를 {1}초 단위로 Add/Expire Time : {2}초",
				MaxCount, AutoIntervalSec, ExpiredTimeSec);
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			ticker.Stop();
		}
	}
}