/**********************************************************************************************************************/
/*	Domain		:	iDoIt.Data.Configuration
/*	Creator		:	Kim Ki Won(iDASiT Inc.)
/*	Create		:	2007년 7월 4일 수요일 오후 4:09:42a
/*	Purpose		:	Use table column information to DataParameter
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace iDoIt.Data.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public class ParamInfo
	{
		/// <summary>
		/// 
		/// </summary>
		protected readonly string PREFIX_FORMAT = "UP_{0}_{1}";

		#region Members
		private string _columnName;
		private DbType _dbType;
		private object _value;
		private bool _isPrimaryKey;
		#endregion

		#region Indexer with SortedList

		private SortedList<string, ParamInfo> _slIndexer = new SortedList<string, ParamInfo>();

		/// <summary>
		/// ParamInfo Indexer
		/// </summary>
		/// <param name="key">key</param>
		/// <returns>ParamInfo</returns>
		public ParamInfo this[string key]
		{
			get
			{
				return _slIndexer[key] as ParamInfo;
			}
			set
			{
				_slIndexer.Add(key, value);
			}
		}

		/// <summary>
		/// Get ParamInfo values that was sorted with Seq
		/// </summary>
		/// <returns></returns>
		public IList<ParamInfo> GetValues()
		{
			return _slIndexer.Values;
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _slIndexer.Count;
			}
		}

		#endregion

		#region Properties
		/// <summary>
		/// Get or set ColumnName.
		/// </summary>
		public String ColumnName
		{
			get
			{
				return _columnName;
			}
			set
			{
				_columnName = value;
			}
		}

		/// <summary>
		/// Get or set DbType.
		/// </summary>
		public DbType DbType
		{
			get
			{
				return _dbType;
			}
			set
			{
				_dbType = value;
			}
		}

		/// <summary>
		/// Get or set Value.
		/// </summary>
		public Object Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		/// <summary>
		/// Get or set IsPrimaryKey.
		/// </summary>
		public Boolean IsPrimaryKey
		{
			get
			{
				return _isPrimaryKey;
			}
			set
			{
				_isPrimaryKey = value;
			}
		}        
        #endregion
	}
}
