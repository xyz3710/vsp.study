/**********************************************************************************************************************/
/*	Domain		:	
/*	Creator		:	Kim Ki Won
/*	Create		:	2007년 7월 13일 금요일 오후 7:48:27a
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
 * Host에서 생성한 Key를 Guest에 배포 하는 방법(Host, Guest 양쪽에서 Enc/Dec 할 수 있다.)
1. Host에서 EL Configuration Console을 이용하여 C:\Symmetric.key에 파일을 생성한다.
   (Host와 Guest의 대칭암호화 공유 키는 같은 곳을 참조해야 한다.
    configuration file에서 상대 경로를 지정할 수 없다.
	상대경로 지정시 "Padding is invalid and cannot be removed." exception 발생)
2. 1.에서 생성한 key를 ExportedSymmetricKey.txt로 export한다.
	(실행 파일과 실행 파일.config와 동일한 경로에 저장한다. 복호화용 password를 입력한다.)
3.  프로그램 시작 지점에서 Exported된 key 파일로 C:\Symmetric.key 파일을 생성한다.
	1) ExportedSymmetricKey.txt를 stream으로 읽는다
	2) 2.에서 지정한 password를 가지고 ProtectedKey를 생성한다.
	3) KeyManager를 이용해서 해당 키를 Write 시킨다.

※ Exported된 SymmetricKey로 Host/Guest에서 사용할 수 있는 키를 동시에 생성한다. 
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iDoIt.Security.Cryptography;
using System.IO;

namespace Cryptography_Application
{
	public partial class CryptographyApplicationForm : Form
	{
		private const string ExpotedSymmetricKeyFile = @".\ExportedSymmetricKey.txt";
		private const string ClonedSymmetricKeyFile = @"C:\Symmetric.key";
		private readonly string Password = "password";
		private readonly string ProviderSymmetric = "RijndaelManaged";
		private readonly string ProviderHash = "SHA256Managed";

		public CryptographyApplicationForm()
		{
			InitializeComponent();
		}

		private void CryptographyApplicationForm_Load(object sender, EventArgs e)
		{
			txtPassword.Text = Password;
			txtResult.Text = "Symmetric 버튼으로 생성된 String Valued의 값을 복사해서 Encrypted string value에 붙여넣고 Symmetric Dec 버튼을 누르면 decryption된 결과가 나온다";

			generateSymmetricKey();
		}

		private void generateSymmetricKey()
		{
			// 원본 키파일 소유자에서 export 했던 파일의 stream을 연다.
			using (FileStream fsExportedKey = File.Open(ExpotedSymmetricKeyFile, FileMode.Open))
			{
				// 새로이 생성할 대칭 암호화 키 파일의 stream을 연다.
				using (FileStream fsSymmetricKey = File.Open(ClonedSymmetricKeyFile, FileMode.Create))
				{
					// export 된 key 파일의 내용을 가지고 암호화 키를 복원한다.(password는 key export할 때 암호)
					ProtectedKey symmKey = KeyManager.RestoreKey((Stream)fsExportedKey, Password, System.Security.Cryptography.DataProtectionScope.LocalMachine);
					// 복원한 암호화 키 stream을 암호화 키 파일로 생성한다
					KeyManager.Write(fsSymmetricKey, symmKey);

					fsSymmetricKey.Close();
				}

				fsExportedKey.Close();
			}
		}

        private string byte2Str(byte[] bytes)
		{
			return UnicodeEncoding.Unicode.GetString(bytes);
		}
		
		private byte[] str2Bytes(string str)
		{
			return UnicodeEncoding.Unicode.GetBytes(str);
		}

		private void writeData(string msg, string text)
		{
			txtResult.Text += string.Format("{0,-30}\t: {1}{2}", msg, text, Environment.NewLine);
		}

		private void writeData(string msg, byte[] bytes)
		{
			txtResult.Text += string.Format("{0,-30}\t: ", msg);

			foreach (byte item in bytes)
				txtResult.Text += string.Format("{0} ", item);

			txtResult.Text += string.Format("{0}", Environment.NewLine);
		}
		
		private void btnSymmetric_Click(object sender, EventArgs e)
		{
			byte[] valueToEncrypt = str2Bytes(txtPassword.Text);
			byte[] encDataBytes = Cryptographer.EncryptSymmetric(ProviderSymmetric, valueToEncrypt);
			string encString = Cryptographer.EncryptSymmetric(ProviderSymmetric, txtPassword.Text);

			writeData(" Symmtric".PadLeft(120, '-'), string.Empty);
			writeData("String Value", encString);
			writeData("Byte values", valueToEncrypt);
			writeData("Enc bytes", encDataBytes);
			writeData("Enc string", byte2Str(encDataBytes));

			Array.Clear(valueToEncrypt, 0, valueToEncrypt.Length);

			// 대칭형 암호화를 이용해 데이터 복호화
			// 암호화된 byte 배열에 대한 복호화
			byte[] decDataBytes = Cryptographer.DecryptSymmetric(ProviderSymmetric, encDataBytes);
			string plainText = byte2Str(decDataBytes);

			writeData("Dec bytes", decDataBytes);
			writeData("Result", plainText);
		}

		private void btnHash_Click(object sender, EventArgs e)
		{
			byte[] valueToHash =  str2Bytes(txtPassword.Text);
			byte[] generateHash = Cryptographer.CreateHash(ProviderHash, valueToHash);

			writeData(" Hash".PadLeft(120, '-'), string.Empty);
			writeData("Byte value", valueToHash);
			writeData("Gen. hash", generateHash);

			Array.Clear(valueToHash, 0, valueToHash.Length);

			// 해시 값을 특정 데이터와 일치하는지 비교
			byte[] stringToCompare = str2Bytes("password");
			bool comparisionSuccessed = Cryptographer.CompareHash(ProviderHash, stringToCompare, generateHash);

			writeData("Hash Comparision", comparisionSuccessed.ToString());
		}

		private void btnSymmetricDec_Click(object sender, EventArgs e)
		{
			try
			{
				byte[] decDataBytes = null;

//				try
//				{
//					byte[] valueToEncrypt = str2Bytes(txtSymmetricDec.Text);
//					decDataBytes = Cryptographer.DecryptSymmetric(SYMMETRIC, valueToEncrypt);
//				}
//				catch
//				{
//					// byte로 오류가 날 경우 text로 검색한다.
//				}

				string decDataString = Cryptographer.DecryptSymmetric(ProviderSymmetric, txtSymmetricDec.Text);

				writeData(" Symmtric Decryption".PadLeft(120, '-'), string.Empty);
				writeData("Dec string", decDataString);
				
				if (decDataBytes != null)
					writeData("Dec bytes dec text", byte2Str(decDataBytes));
			}
			catch (Exception ex)
			{
				ExceptionHandling.Manager.GlobalException.ProcessUnhandledException(ex);
			}
		}		
	}
}