﻿namespace Cryptography_Application
{
	partial class CryptographyApplicationForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSymmetric = new System.Windows.Forms.Button();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.btnHash = new System.Windows.Forms.Button();
			this.txtSymmetricDec = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnSymmetricDec = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnSymmetric
			// 
			this.btnSymmetric.Location = new System.Drawing.Point(264, 9);
			this.btnSymmetric.Name = "btnSymmetric";
			this.btnSymmetric.Size = new System.Drawing.Size(97, 23);
			this.btnSymmetric.TabIndex = 0;
			this.btnSymmetric.Text = "Symmetric";
			this.btnSymmetric.UseVisualStyleBackColor = true;
			this.btnSymmetric.Click += new System.EventHandler(this.btnSymmetric_Click);
			// 
			// txtResult
			// 
			this.txtResult.Location = new System.Drawing.Point(13, 38);
			this.txtResult.Multiline = true;
			this.txtResult.Name = "txtResult";
			this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtResult.Size = new System.Drawing.Size(970, 314);
			this.txtResult.TabIndex = 4;
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(116, 10);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new System.Drawing.Size(139, 21);
			this.txtPassword.TabIndex = 5;
			// 
			// btnHash
			// 
			this.btnHash.Location = new System.Drawing.Point(367, 9);
			this.btnHash.Name = "btnHash";
			this.btnHash.Size = new System.Drawing.Size(97, 23);
			this.btnHash.TabIndex = 1;
			this.btnHash.Text = "Hash";
			this.btnHash.UseVisualStyleBackColor = true;
			this.btnHash.Click += new System.EventHandler(this.btnHash_Click);
			// 
			// txtSymmetricDec
			// 
			this.txtSymmetricDec.Location = new System.Drawing.Point(622, 10);
			this.txtSymmetricDec.Name = "txtSymmetricDec";
			this.txtSymmetricDec.Size = new System.Drawing.Size(252, 21);
			this.txtSymmetricDec.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(11, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 12);
			this.label1.TabIndex = 3;
			this.label1.Text = "To Encrypt text :";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(475, 14);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(141, 12);
			this.label2.TabIndex = 3;
			this.label2.Text = "Encrypted string Value :";
			// 
			// btnSymmetricDec
			// 
			this.btnSymmetricDec.Location = new System.Drawing.Point(883, 9);
			this.btnSymmetricDec.Name = "btnSymmetricDec";
			this.btnSymmetricDec.Size = new System.Drawing.Size(100, 23);
			this.btnSymmetricDec.TabIndex = 3;
			this.btnSymmetricDec.Text = "Symmetirc Dec";
			this.btnSymmetricDec.UseVisualStyleBackColor = true;
			this.btnSymmetricDec.Click += new System.EventHandler(this.btnSymmetricDec_Click);
			// 
			// CryptographyApplicationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(995, 364);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtSymmetricDec);
			this.Controls.Add(this.txtPassword);
			this.Controls.Add(this.txtResult);
			this.Controls.Add(this.btnSymmetricDec);
			this.Controls.Add(this.btnHash);
			this.Controls.Add(this.btnSymmetric);
			this.Name = "CryptographyApplicationForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Cryptography Application Form";
			this.Load += new System.EventHandler(this.CryptographyApplicationForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnSymmetric;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Button btnHash;
		private System.Windows.Forms.TextBox txtSymmetricDec;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnSymmetricDec;

	}
}

