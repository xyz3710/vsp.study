using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Command01
{
	public class RemoteControlWithUndo
	{
		private ICommand[] _onCommands;
		private ICommand[] _offCommands;
		private ICommand _undoCommand;

		public RemoteControlWithUndo()
		{
			// 리￢￠c모￢ⓒ￡콘U에?￠®서u¡ⓒ 일I곱Æo개Æⓒø의C on,off 명￢i령¤E을¡i 처ⓒø리￢￠c할O 수uo 있O습oA니￥I다￥U. 
			_onCommands = new ICommand[8];
			_offCommands = new ICommand[8];

			ICommand noCommand = new NoCommand();

			for (int i = 0; i < 7; i++)
			{
				_onCommands[i] = noCommand;
				_offCommands[i] = noCommand;
			}

			_undoCommand = noCommand;
		}

		public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
		{
			_onCommands[slot] = onCommand;
			_offCommands[slot] = offCommand;
		}

		public void OnButtonWasPushed(int slot)
		{
			if (_onCommands[slot] != null)
			{
				_onCommands[slot].Execute();
			}

			_undoCommand = _onCommands[slot];
		}

		public void OffButtonWasPushed(int slot)
		{
			_offCommands[slot].Execute();

			_undoCommand = _offCommands[slot];
		}

		public void UndoButtonsWasPushed()
		{
			_undoCommand.Undo();
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder.Append("\n----------- Remote Control -------------\n");

			for (int i = 0; i < _onCommands.Length; i++)
			{
				stringBuilder.Append("[slot " + i + "]" + _onCommands[i].GetType().Name + "  " + _offCommands[i].GetType().Name + "\n");
			}
			return stringBuilder.ToString();
		}
	}
}

