using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class LightOffCommand : ICommand
	{
		private Light _light;

		public LightOffCommand(Light light)
		{
			_light = light;
		}

		public void Execute()
		{
			_light.Off();
		}

		public void Undo()
		{
			_light.On();
		}
	}
}

