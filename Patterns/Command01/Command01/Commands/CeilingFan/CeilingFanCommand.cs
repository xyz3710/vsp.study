using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class CeilingFanCommand : ICommand
	{
		protected CeilingFan _ceilingFan;
		protected int _prevSpeed;

		public CeilingFanCommand(CeilingFan ceilingFan)
		{
			_ceilingFan = ceilingFan;
		}

		public virtual void Execute()
		{
			_prevSpeed = _ceilingFan.GetSpeed();
			_ceilingFan.Off();
		}

		public void Undo()
		{
			if (_prevSpeed == CeilingFan.HIGH)
			{
				_ceilingFan.High();
			}
			else if (_prevSpeed == CeilingFan.MEDIUM)
			{
				_ceilingFan.Medium();
			}
			else if (_prevSpeed == CeilingFan.LOW)
			{
				_ceilingFan.Low();
			}
			else if (_prevSpeed == CeilingFan.OFF)
			{
				_ceilingFan.Off();
			}
		}
	}
}

