using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class CeilingFanOffCommand : CeilingFanCommand
	{
		public CeilingFanOffCommand(CeilingFan ceilingFan)
			: base(ceilingFan)
		{
		}

		public override void Execute()
		{
			_prevSpeed = _ceilingFan.GetSpeed();
			_ceilingFan.Off();
		}
	}
}

