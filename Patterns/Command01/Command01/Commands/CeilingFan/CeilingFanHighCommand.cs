using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class CeilingFanHighCommand : CeilingFanCommand
	{
		public CeilingFanHighCommand(CeilingFan ceilingFan)
			: base(ceilingFan)
		{
		}
		public override void Execute()
		{
			_prevSpeed = _ceilingFan.GetSpeed();
			_ceilingFan.High();
		}
	}
}

