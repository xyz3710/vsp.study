using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class GarageDoorUpCommand : ICommand
	{
		private GarageDoor _garageDoor;

		public GarageDoorUpCommand(GarageDoor garageDoor)
		{
			_garageDoor = garageDoor;
		}

		public void Execute()
		{
			_garageDoor.Up();
		}

		public void Undo()
		{
			_garageDoor.Down();
		}
	}
}

