using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class GarageDoorDownCommand : ICommand
	{
		private GarageDoor _garageDoor;

		public GarageDoorDownCommand(GarageDoor garageDoor)
		{
			_garageDoor = garageDoor;
		}

		public void Execute()
		{
			_garageDoor.Down();
		}

		public void Undo()
		{
			_garageDoor.Up();
		}
	}
}

