using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Command01
{
	public class GarageDoor
	{
		internal void Up()
		{
			MessageBox.Show("GarageDoor Open");
		}

		internal void Down()
		{
			MessageBox.Show("GarageDoor Down");
		}
	}
}

