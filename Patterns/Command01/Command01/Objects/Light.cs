using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Command01
{
	public class Light
	{
		internal void On()
		{
			MessageBox.Show("Light On");
		}

		internal void Off()
		{
			MessageBox.Show("Light Off");
		}

	}
}

