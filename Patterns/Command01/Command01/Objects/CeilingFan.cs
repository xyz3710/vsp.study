using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Command01
{
	public class CeilingFan
	{
		public static int HIGH = 3;
		public static int MEDIUM = 2;
		public static int LOW = 1;
		public static int OFF = 0;

		private int _speed;

		public string Location
		{
			get; set;
		}

		public CeilingFan(string location)
		{
			this.Location = location;
			_speed = OFF;
		}

		public void High()
		{
			_speed = HIGH;
			MessageBox.Show("High");
		}

		public void Medium()
		{
			_speed = MEDIUM;
			MessageBox.Show("Medium");
		}

		public void Low()
		{
			_speed = LOW;
			MessageBox.Show("Low");
		}

		public void Off()
		{
			_speed = OFF;
			MessageBox.Show("Off");
		}

		public int GetSpeed()
		{
			return _speed;
		}
	}
}

