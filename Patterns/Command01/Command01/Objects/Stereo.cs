using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Command01
{
	public class Stereo
	{
		internal void On()
		{
			MessageBox.Show("Stereo On");
		}
		internal void SetCD()
		{
			MessageBox.Show("Set CD");
		}
		internal void SetVolume(int p)
		{
			MessageBox.Show("Set Volume : " + p.ToString());
		}
		internal void Off()
		{
			MessageBox.Show("Stereo Off");
		}
	}
}

