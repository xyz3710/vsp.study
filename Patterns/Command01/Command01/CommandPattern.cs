﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Command01
{
	public partial class CommandPattern : Form
	{
		private RemoteControlWithUndo _remoteControl;

		private Light _livingRoomLight;
		private Light _kitchenLight;
		private GarageDoor _garageDoor;
		private Stereo _stereo;
		private CeilingFan _ceilingFan;

		private LightOnCommand _livingRoomLightOn;
		private LightOffCommand _livingRoomLightOff;
		private LightOnCommand _kitchenLightOn;
		private LightOffCommand _kitchenLightOff;

		// Garaage Command 
		private GarageDoorUpCommand _garageDoorUp;
		private GarageDoorDownCommand _garageDoorDown;

		// Stereo Command
		private StereoOnWithCDCommand _stereoOnWithCD;
		private StereoOffCommand _stereoOff;

		// Ceiling Fan
		private CeilingFanCommand _ceilingFanHigh;
		private CeilingFanCommand _ceilingFanMedium;
		private CeilingFanCommand _ceilingFanLow;
		private CeilingFanOffCommand _ceilingFanOff;

		// party on / off
		private ICommand[] _partyOn;
		private ICommand[] _partyOff;

		private MacroCommand _partyOnMacro;
		private MacroCommand _partyOffMacro;

		public CommandPattern()
		{
			InitializeComponent();
			this.InitData();

			RegisterClickEventHandler(btnLivingRoomLightOn, btnLivingRoomLightOn_Click);
			RegisterClickEventHandler(btnLivingRoomLightOff, btnLivingRoomLightOff_Click);

			RegisterClickEventHandler(btnKitchenLightOn, btnKitchenLightOn_Click);
			RegisterClickEventHandler(btnKitchenLightOff, btnKitchenLightOff_Click);

			RegisterClickEventHandler(btnGarageDoorUp, btnGarageDoorUp_Click);
			RegisterClickEventHandler(btnGarageDoorDown, btnGarageDoorDown_Click);

			RegisterClickEventHandler(btnStereoOnWidthCd, btnStereoOnWidthCd_Click);
			RegisterClickEventHandler(btnStereoOff, btnStereoOff_Click);

			RegisterClickEventHandler(btnCeilingFanHigh, btnCeilingFanHigh_Click);
			RegisterClickEventHandler(btnCeilingFanMedium, btnCeilingFanMedium_Click);
			RegisterClickEventHandler(btnCeilingFanLow, btnCeilingFanLow_Click);
			RegisterClickEventHandler(btnCeilingFanOff, btnCeilingFanOff_Click);

			RegisterClickEventHandler(btnMacroCommand, btnMacroCommand_Click);
			RegisterClickEventHandler(btnMacroCommandOff, btnMacroCommandOff_Click);

			RegisterClickEventHandler(btnUndo, btnUndo_Click);
		}

		private void RegisterClickEventHandler(Button button, EventHandler eventHandler)
		{
			button.Click += eventHandler;
		}

		private void InitData()
		{
			_remoteControl = new RemoteControlWithUndo();

			_livingRoomLight = new Light();
			_kitchenLight = new Light();
			_garageDoor = new GarageDoor();
			_stereo = new Stereo();
			_ceilingFan = new CeilingFan("Living room");

			_livingRoomLightOn = new LightOnCommand(_livingRoomLight);
			_livingRoomLightOff = new LightOffCommand(_livingRoomLight);
			_kitchenLightOn = new LightOnCommand(_kitchenLight);
			_kitchenLightOff = new LightOffCommand(_kitchenLight);

			_garageDoorUp = new GarageDoorUpCommand(_garageDoor);
			_garageDoorDown = new GarageDoorDownCommand(_garageDoor);

			_stereoOnWithCD = new StereoOnWithCDCommand(_stereo);
			_stereoOff = new StereoOffCommand(_stereo);

			_ceilingFanHigh = new CeilingFanHighCommand(_ceilingFan);
			_ceilingFanMedium = new CeilingFanMediumCommand(_ceilingFan);
			_ceilingFanLow = new CeilingFanLowCommand(_ceilingFan);
			_ceilingFanOff = new CeilingFanOffCommand(_ceilingFan);

			_remoteControl.SetCommand(0, _livingRoomLightOn, _livingRoomLightOff);
			_remoteControl.SetCommand(1, _kitchenLightOn, _kitchenLightOff);
			_remoteControl.SetCommand(2, _garageDoorUp, _garageDoorDown);
			_remoteControl.SetCommand(3, _stereoOnWithCD, _stereoOff);

			_remoteControl.SetCommand(4, _ceilingFanHigh, _ceilingFanOff);
			_remoteControl.SetCommand(5, _ceilingFanMedium, _ceilingFanOff);
			_remoteControl.SetCommand(6, _ceilingFanLow, _ceilingFanOff);

			_partyOn = new ICommand[] { _livingRoomLightOn, _kitchenLightOn, _garageDoorUp, _stereoOnWithCD };
			_partyOff = new ICommand[] { _livingRoomLightOff, _kitchenLightOff, _garageDoorDown, _stereoOff };

			_partyOnMacro = new MacroCommand(_partyOn);
			_partyOffMacro = new MacroCommand(_partyOff);

			_remoteControl.SetCommand(7, _partyOnMacro, _partyOffMacro);
			MessageBox.Show(_remoteControl.ToString());
		}

		private void btnLivingRoomLightOn_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(0);
		}

		private void btnKitchenLightOn_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(1);
		}

		private void btnGarageDoorUp_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(2);
		}

		private void btnStereoOnWidthCd_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(3);
		}

		private void btnLivingRoomLightOff_Click(object sender, EventArgs e)
		{
			_remoteControl.OffButtonWasPushed(0);
		}

		private void btnKitchenLightOff_Click(object sender, EventArgs e)
		{
			_remoteControl.OffButtonWasPushed(1);
		}

		private void btnGarageDoorDown_Click(object sender, EventArgs e)
		{
			_remoteControl.OffButtonWasPushed(2);
		}

		private void btnStereoOff_Click(object sender, EventArgs e)
		{
			_remoteControl.OffButtonWasPushed(3);
		}

		private void btnUndo_Click(object sender, EventArgs e)
		{
			_remoteControl.UndoButtonsWasPushed();
		}

		private void btnCeilingFanHigh_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(4);
		}

		private void btnCeilingFanOff_Click(object sender, EventArgs e)
		{
			_remoteControl.OffButtonWasPushed(4);
			_remoteControl.OffButtonWasPushed(5);
			_remoteControl.OffButtonWasPushed(6);
		}

		private void btnCeilingFanMedium_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(5);
		}

		private void btnCeilingFanLow_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(6);
		}

		private void btnMacroCommand_Click(object sender, EventArgs e)
		{
			_remoteControl.OnButtonWasPushed(7);
		}

		private void btnMacroCommandOff_Click(object sender, EventArgs e)
		{
			_remoteControl.OffButtonWasPushed(7);
		}
	}
}
