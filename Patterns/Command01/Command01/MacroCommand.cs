using System;
using System.Collections.Generic;
using System.Linq;

namespace Command01
{
	public class MacroCommand : ICommand
	{
		private ICommand[] _commands;

		public MacroCommand(ICommand[] commands)
		{
			_commands = commands;
		}

		public void Execute()
		{
			for (int i = 0; i < _commands.Length; i++)
			{
				_commands[i].Execute();
			}
		}

		public void Undo()
		{
			throw new NotImplementedException();
		}
	}
}

