﻿namespace Command01
{
	partial class CommandPattern
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnLivingRoomLightOn = new System.Windows.Forms.Button();
			this.btnLivingRoomLightOff = new System.Windows.Forms.Button();
			this.btnKitchenLightOn = new System.Windows.Forms.Button();
			this.btnKitchenLightOff = new System.Windows.Forms.Button();
			this.btnGarageDoorUp = new System.Windows.Forms.Button();
			this.btnGarageDoorDown = new System.Windows.Forms.Button();
			this.btnStereoOnWidthCd = new System.Windows.Forms.Button();
			this.btnStereoOff = new System.Windows.Forms.Button();
			this.btnCeilingFanHigh = new System.Windows.Forms.Button();
			this.btnCeilingFanOff = new System.Windows.Forms.Button();
			this.btnCeilingFanMedium = new System.Windows.Forms.Button();
			this.btnCeilingFanLow = new System.Windows.Forms.Button();
			this.btnMacroCommand = new System.Windows.Forms.Button();
			this.btnMacroCommandOff = new System.Windows.Forms.Button();
			this.btnUndo = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnLivingRoomLightOn
			// 
			this.btnLivingRoomLightOn.Location = new System.Drawing.Point(15, 12);
			this.btnLivingRoomLightOn.Name = "btnLivingRoomLightOn";
			this.btnLivingRoomLightOn.Size = new System.Drawing.Size(216, 34);
			this.btnLivingRoomLightOn.TabIndex = 0;
			this.btnLivingRoomLightOn.Text = "Living Room Light On";
			this.btnLivingRoomLightOn.UseVisualStyleBackColor = true;
			// 
			// btnLivingRoomLightOff
			// 
			this.btnLivingRoomLightOff.Location = new System.Drawing.Point(265, 12);
			this.btnLivingRoomLightOff.Name = "btnLivingRoomLightOff";
			this.btnLivingRoomLightOff.Size = new System.Drawing.Size(216, 34);
			this.btnLivingRoomLightOff.TabIndex = 0;
			this.btnLivingRoomLightOff.Text = "Living Room Light Off";
			this.btnLivingRoomLightOff.UseVisualStyleBackColor = true;
			// 
			// btnKitchenLightOn
			// 
			this.btnKitchenLightOn.Location = new System.Drawing.Point(15, 52);
			this.btnKitchenLightOn.Name = "btnKitchenLightOn";
			this.btnKitchenLightOn.Size = new System.Drawing.Size(216, 34);
			this.btnKitchenLightOn.TabIndex = 0;
			this.btnKitchenLightOn.Text = "Kitchen Light On";
			this.btnKitchenLightOn.UseVisualStyleBackColor = true;
			// 
			// btnKitchenLightOff
			// 
			this.btnKitchenLightOff.Location = new System.Drawing.Point(265, 52);
			this.btnKitchenLightOff.Name = "btnKitchenLightOff";
			this.btnKitchenLightOff.Size = new System.Drawing.Size(216, 34);
			this.btnKitchenLightOff.TabIndex = 0;
			this.btnKitchenLightOff.Text = "Kitchen Light Off";
			this.btnKitchenLightOff.UseVisualStyleBackColor = true;
			// 
			// btnGarageDoorUp
			// 
			this.btnGarageDoorUp.Location = new System.Drawing.Point(15, 92);
			this.btnGarageDoorUp.Name = "btnGarageDoorUp";
			this.btnGarageDoorUp.Size = new System.Drawing.Size(216, 34);
			this.btnGarageDoorUp.TabIndex = 0;
			this.btnGarageDoorUp.Text = "Garage Door Up";
			this.btnGarageDoorUp.UseVisualStyleBackColor = true;
			// 
			// btnGarageDoorDown
			// 
			this.btnGarageDoorDown.Location = new System.Drawing.Point(265, 92);
			this.btnGarageDoorDown.Name = "btnGarageDoorDown";
			this.btnGarageDoorDown.Size = new System.Drawing.Size(216, 34);
			this.btnGarageDoorDown.TabIndex = 0;
			this.btnGarageDoorDown.Text = "Garage Door Down";
			this.btnGarageDoorDown.UseVisualStyleBackColor = true;
			// 
			// btnStereoOnWidthCd
			// 
			this.btnStereoOnWidthCd.Location = new System.Drawing.Point(15, 132);
			this.btnStereoOnWidthCd.Name = "btnStereoOnWidthCd";
			this.btnStereoOnWidthCd.Size = new System.Drawing.Size(216, 34);
			this.btnStereoOnWidthCd.TabIndex = 0;
			this.btnStereoOnWidthCd.Text = "Stereo On Width CD";
			this.btnStereoOnWidthCd.UseVisualStyleBackColor = true;
			// 
			// btnStereoOff
			// 
			this.btnStereoOff.Location = new System.Drawing.Point(265, 132);
			this.btnStereoOff.Name = "btnStereoOff";
			this.btnStereoOff.Size = new System.Drawing.Size(216, 34);
			this.btnStereoOff.TabIndex = 0;
			this.btnStereoOff.Text = "Stereo Off";
			this.btnStereoOff.UseVisualStyleBackColor = true;
			// 
			// btnCeilingFanHigh
			// 
			this.btnCeilingFanHigh.Location = new System.Drawing.Point(15, 190);
			this.btnCeilingFanHigh.Name = "btnCeilingFanHigh";
			this.btnCeilingFanHigh.Size = new System.Drawing.Size(216, 34);
			this.btnCeilingFanHigh.TabIndex = 0;
			this.btnCeilingFanHigh.Text = "Ceiling Fan High";
			this.btnCeilingFanHigh.UseVisualStyleBackColor = true;
			// 
			// btnCeilingFanOff
			// 
			this.btnCeilingFanOff.Location = new System.Drawing.Point(265, 190);
			this.btnCeilingFanOff.Name = "btnCeilingFanOff";
			this.btnCeilingFanOff.Size = new System.Drawing.Size(216, 34);
			this.btnCeilingFanOff.TabIndex = 0;
			this.btnCeilingFanOff.Text = "Ceiling Fan Off";
			this.btnCeilingFanOff.UseVisualStyleBackColor = true;
			// 
			// btnCeilingFanMedium
			// 
			this.btnCeilingFanMedium.Location = new System.Drawing.Point(15, 230);
			this.btnCeilingFanMedium.Name = "btnCeilingFanMedium";
			this.btnCeilingFanMedium.Size = new System.Drawing.Size(216, 34);
			this.btnCeilingFanMedium.TabIndex = 0;
			this.btnCeilingFanMedium.Text = "Ceiling Fan Medium";
			this.btnCeilingFanMedium.UseVisualStyleBackColor = true;
			// 
			// btnCeilingFanLow
			// 
			this.btnCeilingFanLow.Location = new System.Drawing.Point(15, 270);
			this.btnCeilingFanLow.Name = "btnCeilingFanLow";
			this.btnCeilingFanLow.Size = new System.Drawing.Size(216, 34);
			this.btnCeilingFanLow.TabIndex = 0;
			this.btnCeilingFanLow.Text = "Ceiling Fan Low";
			this.btnCeilingFanLow.UseVisualStyleBackColor = true;
			// 
			// btnMacroCommand
			// 
			this.btnMacroCommand.Location = new System.Drawing.Point(265, 329);
			this.btnMacroCommand.Name = "btnMacroCommand";
			this.btnMacroCommand.Size = new System.Drawing.Size(216, 34);
			this.btnMacroCommand.TabIndex = 0;
			this.btnMacroCommand.Text = "Macro Command";
			this.btnMacroCommand.UseVisualStyleBackColor = true;
			// 
			// btnMacroCommandOff
			// 
			this.btnMacroCommandOff.Location = new System.Drawing.Point(265, 369);
			this.btnMacroCommandOff.Name = "btnMacroCommandOff";
			this.btnMacroCommandOff.Size = new System.Drawing.Size(216, 34);
			this.btnMacroCommandOff.TabIndex = 0;
			this.btnMacroCommandOff.Text = "Macro Command Off";
			this.btnMacroCommandOff.UseVisualStyleBackColor = true;
			// 
			// btnUndo
			// 
			this.btnUndo.Location = new System.Drawing.Point(15, 329);
			this.btnUndo.Name = "btnUndo";
			this.btnUndo.Size = new System.Drawing.Size(216, 74);
			this.btnUndo.TabIndex = 0;
			this.btnUndo.Text = "Undo";
			this.btnUndo.UseVisualStyleBackColor = true;
			// 
			// CommandPattern
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(497, 419);
			this.Controls.Add(this.btnCeilingFanOff);
			this.Controls.Add(this.btnStereoOff);
			this.Controls.Add(this.btnGarageDoorDown);
			this.Controls.Add(this.btnKitchenLightOff);
			this.Controls.Add(this.btnUndo);
			this.Controls.Add(this.btnMacroCommandOff);
			this.Controls.Add(this.btnMacroCommand);
			this.Controls.Add(this.btnCeilingFanLow);
			this.Controls.Add(this.btnCeilingFanMedium);
			this.Controls.Add(this.btnCeilingFanHigh);
			this.Controls.Add(this.btnStereoOnWidthCd);
			this.Controls.Add(this.btnGarageDoorUp);
			this.Controls.Add(this.btnLivingRoomLightOff);
			this.Controls.Add(this.btnKitchenLightOn);
			this.Controls.Add(this.btnLivingRoomLightOn);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Name = "CommandPattern";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnLivingRoomLightOn;
		private System.Windows.Forms.Button btnLivingRoomLightOff;
		private System.Windows.Forms.Button btnKitchenLightOn;
		private System.Windows.Forms.Button btnKitchenLightOff;
		private System.Windows.Forms.Button btnGarageDoorUp;
		private System.Windows.Forms.Button btnGarageDoorDown;
		private System.Windows.Forms.Button btnStereoOnWidthCd;
		private System.Windows.Forms.Button btnStereoOff;
		private System.Windows.Forms.Button btnCeilingFanHigh;
		private System.Windows.Forms.Button btnCeilingFanOff;
		private System.Windows.Forms.Button btnCeilingFanMedium;
		private System.Windows.Forms.Button btnCeilingFanLow;
		private System.Windows.Forms.Button btnMacroCommand;
		private System.Windows.Forms.Button btnMacroCommandOff;
		private System.Windows.Forms.Button btnUndo;
	}
}

