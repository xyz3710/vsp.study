﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Observer01
{
	public partial class Screen2 : UserControl, IObserver, IScreen
	{
		private int _currentNum;
		private Label label1;
		private IObserverCenter _observerCenter;

		public Screen2()
		{
			InitializeComponent();
		}

		public Screen2(IObserverCenter observerCenter)
			: this()
		{
			_observerCenter = observerCenter;
			_observerCenter.AddObserver(this);
		}

		public void NotifyAction(int parameter)
		{
			_currentNum = parameter;
			UpdateScreen();
		}

		public void UpdateScreen()
		{
			label1.Text = $"Screen 2 : {_currentNum}";
		}

		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(22, 52);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(89, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Screen 2 : 0";
			// 
			// Screen2
			// 
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("굴림", 11F);
			this.Name = "Screen2";
			this.Size = new System.Drawing.Size(199, 286);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
