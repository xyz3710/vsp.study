﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer01
{
	public interface IScreen
	{
		void UpdateScreen();
	}
}
