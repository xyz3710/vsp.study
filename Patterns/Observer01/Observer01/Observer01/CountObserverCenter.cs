﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer01
{
	public class CountObserverCenter : IObserverCenter
	{
		private int _count;
		private List<IObserver> _observers;

		public CountObserverCenter()
		{
			_observers = new List<IObserver>();
		}

		public void AddObserver(IObserver observer)
		{
			_observers.Add(observer);
		}

		public void NotifyObservers(int parameter)
		{
			_count = parameter;

			foreach (var observer in _observers)
			{
				observer.NotifyAction(_count);
			}
		}

		public void RemoveObserver(IObserver observer)
		{
			int index = _observers.IndexOf(observer);

			if (index >= 0)
			{
				_observers.RemoveAt(index);
			}
		}
	}
}
