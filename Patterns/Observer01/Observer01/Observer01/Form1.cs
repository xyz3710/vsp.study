﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Observer01
{
	public partial class Form1 : Form
	{
		private CountObserverCenter _countObserverCenter;
		private int _current;

		public Form1()
		{
			InitializeComponent();

			_countObserverCenter = new CountObserverCenter();
			_current = 0;

			Screen1 screen1 = new Screen1(_countObserverCenter) { Dock = DockStyle.Fill };

			tlpMain.Controls.Add(screen1, 0, 1);

			Screen2 screen2 = new Screen2(_countObserverCenter) { Dock = DockStyle.Fill };

			tlpMain.Controls.Add(screen2, 1, 1);

			btnRefresh.Click += (sender, e) =>
			{
				_current++;
				_countObserverCenter.NotifyObservers(_current);
			};
		}
	}
}
