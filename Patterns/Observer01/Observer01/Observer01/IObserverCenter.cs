﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer01
{
	public interface IObserverCenter
	{
		/// <summary>
		/// Adds the observer.
		/// </summary>
		/// <param name="observer">The observer.</param>
		void AddObserver(IObserver observer);

		/// <summary>
		/// Removes the observer.
		/// </summary>
		/// <param name="observer">The observer.</param>
		void RemoveObserver(IObserver observer);

		/// <summary>
		/// Notifies the observers.
		/// </summary>
		/// <param name="parameter">The parameter.</param>
		void NotifyObservers(int parameter);
	}
}
