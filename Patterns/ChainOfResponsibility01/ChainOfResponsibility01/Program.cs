﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ChainOfResponsibility01
{
	class Program
	{
		static void Main(string[] args)
		{
			// setup Chain of Responsibility
			Handler handler1 = new ConcreteHandler1();
			Handler handler2 = new ConcreteHandler2();
			Handler handler3 = new ConcreteHandler3();

			handler1.SetSuccessor(handler2);
			handler2.SetSuccessor(handler3);

			var requests = new int[] { 2, 5, 14, 22, 18, 3, 27, 20 };

			foreach (int request in requests)
			{
				handler1.HandleRequest(request);
			}
		}
	}
}
