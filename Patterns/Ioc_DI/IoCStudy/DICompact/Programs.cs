﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICompact
{
	class Programs
	{
		static void Main()
		{
			DemoContainer container = new DemoContainer();

			// registering dependecies
			container.Register<IRepository>(delegate
			{
				return new NHibernateRepository();
			});
		}
	}
}
