﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IocSample
{
	class Samsung
	{
		private string MobileName
		{
			get;
			set;
		}
		private IMobile Mobile
		{
			get;
			set;
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the Samsung class.
		/// </summary>
		/// <param name="mobileName"></param>
		public Samsung(string mobileName, IMobile mobile)
		{
			MobileName = mobileName;
			Mobile = mobile;
		}
		#endregion

		#region ToString
		/// <summary>
		/// Samsung를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return MobileName;
		}
		#endregion
	}
}
