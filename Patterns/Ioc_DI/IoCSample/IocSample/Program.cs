﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IocSample
{
	class Program
	{
		static void Main(string[] args)
		{
			//Samsung samsung = new Samsung("Galaxy Grand", "Jelly Bean", 16);

			//Console.WriteLine(samsung);

			IoC ioC = new IoC();

			ioC.Assembling();

			Console.WriteLine(ioC);
		}
	}
}
