﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IocSample
{
	class IoC
	{
		private IMobile Mobile
		{
			get;
			set;
		}

		private Samsung Samsung
		{
			get;
			set;
		}

		public void Assembling()
		{
			Mobile = new Android("Jelly Bean", 16);
			Samsung = new Samsung("Galaxy Grand", Mobile);
		}

		#region ToString
		/// <summary>
		/// IoC를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("Mobile name: {0} and {1}",
					Samsung, Mobile);
		}
		#endregion
	}
}
