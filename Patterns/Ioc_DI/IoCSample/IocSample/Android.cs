﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IocSample
{
	class Android : IMobile
	{
		public string Version
		{
			get;
			set;
		}

		public int InternalMemory
		{
			get;
			set;
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the Android class.
		/// </summary>
		/// <param name="version"></param>
		/// <param name="internalMemory"></param>
		public Android(string version, int internalMemory)
		{
			Version = version;
			InternalMemory = internalMemory;
		}
		#endregion

		#region ToString
		/// <summary>
		/// Android를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("Version:{0} is having internal memory:{1} MB",
					Version, InternalMemory);
		}
		#endregion
	}
}
