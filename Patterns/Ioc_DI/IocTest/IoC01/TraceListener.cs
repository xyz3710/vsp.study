﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioc01
{
	public class Tracer
	{
		MessageQueue _mq = new MessageQueue();

		public void Trace(string message)
		{
			_mq.Send(message);
		}
	}

	// better API using Ioc(Inversion Of Control)
	public abstract class TraceListener
	{
		public abstract void Trace(string message);
	}

	public class Tracer2
	{
		TraceListener _listener;

		public Tracer2(TraceListener listener)
		{
			_listener = listener;
		}

		public void Trace(string message)
		{
			_listener.Trace(message);
		}
	}

	public class FileListener : TraceListener
	{
		public override void Trace(string message)
		{
			// TODO
		}
	}
}
