﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioc01
{
	class Program
	{
		static void Main(string[] args)
		{
			// Dependency Injection
			Tracer2 tracer = new Tracer2(new FileListener());

			tracer.Trace("trace message");
		}
	}
}
