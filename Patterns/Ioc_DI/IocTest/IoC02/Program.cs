﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC02
{
	class Program
	{
		static void Main(string[] args)
		{
			IVehicle vehicleCar = new Car();
			IVehicle vehicleTruck = new Truck();

			VehicleController vehicleController = new VehicleController(vehicleCar);

			vehicleController.Accelerate();
			vehicleController.Brake();
		}
	}
}
