﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC02
{
	public interface IVehicle
	{
		void Accelerate();
		void Brake();
	}
}
