﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC02
{
	public class VehicleController
	{
		private IVehicle _vehicle;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the VehicleController class.
		/// </summary>
		public VehicleController(IVehicle vehicle)
		{
			_vehicle = vehicle;
		}
		#endregion

		public void Accelerate()
		{
			_vehicle.Accelerate();
		}

		public void Brake()
		{
			_vehicle.Brake();
		}
	}
}
