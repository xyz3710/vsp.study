﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC02
{
	public class Car : IVehicle
	{
		#region IVehicle 멤버

		public void Accelerate()
		{
			Console.WriteLine("Car accelerates.");
		}

		public void Brake()
		{
			Console.WriteLine("Car stopped.");
		}

		#endregion
	}
}
