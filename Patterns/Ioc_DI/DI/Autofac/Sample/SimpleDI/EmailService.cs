﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDI
{
	class EmailService : IMailService
	{
		#region IMailService 멤버

		public void Execute()
		{
			Console.WriteLine("Email servic Executing.");
		}

		#endregion
	}
}
