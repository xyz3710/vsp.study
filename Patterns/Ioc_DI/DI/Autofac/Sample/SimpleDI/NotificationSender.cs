﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDI
{
	class NotificationSender
	{
		public IMobileService MobileService
		{
			get;
			set;
		}

		public IMailService MailService
		{
			private get;
			set;
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the NotificationSender class.
		/// </summary>
		/// <param name="mobileService"></param>
		public NotificationSender(IMobileService mobileService)
		{
			MobileService = mobileService;
			MailService = null;
		}
		#endregion

		public void SendNotification()
		{
			MobileService.Execute();
			MailService.Execute();
		}
	}
}
