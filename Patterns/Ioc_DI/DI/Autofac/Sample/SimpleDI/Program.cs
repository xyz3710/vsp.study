﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace SimpleDI
{
	class Program
	{
		static void Main(string[] args)
		{
			var builder = new ContainerBuilder();

			builder.RegisterType<SMSService>().As<IMobileService>();
			builder.RegisterType<EmailService>().As<IMailService>();

			var container = builder.Build();

			container.Resolve<IMobileService>().Execute();
			container.Resolve<IMailService>().Execute();

		}
	}
}
