﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDI
{
	class SMSService : IMobileService
	{
		#region IMobileService 멤버

		public void Execute()
		{
			Console.WriteLine("SMS service executeing.");
		}

		#endregion
	}
}
