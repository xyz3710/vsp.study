﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorld
{
	public class MessageChecker
	{
		#region Constructors
		/// <summary>
		/// MessageChecker class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public MessageChecker()
		{
		}

		/// <summary>
		/// MessageChecker class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="messages"></param>
		/// <param name="notifier"></param>
		public MessageChecker(IQueryable<Message> messages, IMessageNotifier notifier)
		{
			Messages = messages;
			Notifier = notifier;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Messages를 구하거나 설정합니다.
		/// </summary>
		public IQueryable<Message> Messages
		{
			get;
			set;
		}

		/// <summary>
		/// Notifier를 구하거나 설정합니다.
		/// </summary>
		public IMessageNotifier Notifier
		{
			get;
			set;
		}
		#endregion

		public void ShowMessage()
		{
			foreach (Message message in Messages)
				Notifier.CheckMessage(message);
		}
	}
}
