﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorld
{
	public interface IMessageNotifier
	{
		void CheckMessage(Message message);
	}
}
