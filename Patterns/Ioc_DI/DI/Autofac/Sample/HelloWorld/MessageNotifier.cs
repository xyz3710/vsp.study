﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HelloWorld
{
	public class MessageNotifier : IMessageNotifier
	{
		private TextWriter _textWriter;

		/// <summary>
		/// MessageNotifier class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="textWriter"></param>
		public MessageNotifier(TextWriter textWriter)
		{
			_textWriter = textWriter;
		}

		#region IMessageNotifier 멤버

		public void CheckMessage(Message message)
		{
			_textWriter.WriteLine(message.Title);
		}

		#endregion
	}
}
