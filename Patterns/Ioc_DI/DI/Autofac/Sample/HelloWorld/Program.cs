﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using System.IO;

namespace HelloWorld
{
	class Program
	{
		static void Main(string[] args)
		{
			var messages = new List<Message>
			{
				new Message { Title = "Welcome " },
				new Message { Title = "Hello" },
				new Message { Title = "World" },				
			}.AsQueryable();

			// step 1: IContainer 생성
			var builder = new ContainerBuilder();

			// step 2: Registering a Component created with an Expression(Constructor type injection)
			builder.Register(c => new MessageChecker(c.Resolve<IQueryable<Message>>(),
														c.Resolve<IMessageNotifier>()));
			// step 2: Registering a Component created with an Expression(Property type injection)
			//builder.Register(c => new MessageChecker
			//{
			//	Messages = c.Resolve<IQueryable<Message>>(),
			//	Notifier = c.Resolve<IMessageNotifier>(),
			//});

			// step 3: Registering a Component instance
			builder.RegisterType<MessageNotifier>().As<IMessageNotifier>();

			// step 4: Registering a Component with it's implementation type
			builder.RegisterInstance(messages).As<IQueryable<Message>>();
			builder.RegisterInstance(Console.Out).As<TextWriter>().ExternallyOwned();

			// step 5: 빌드
			using (var container = builder.Build())
				// step 6: Resove to get instance
				container.Resolve<MessageChecker>().ShowMessage();
		}
	}
}
