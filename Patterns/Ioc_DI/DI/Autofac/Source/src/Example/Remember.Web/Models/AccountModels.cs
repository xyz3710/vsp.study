﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remember.Web.Models
{
    
    public class LoginForm
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}