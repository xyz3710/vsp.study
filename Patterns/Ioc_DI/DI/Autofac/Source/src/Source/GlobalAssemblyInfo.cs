using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyCompany("Autofac Project - http://autofac.org")]
[assembly: AssemblyProduct("Autofac")]
[assembly: AssemblyCopyright("Copyright � 2011 Autofac Contributors")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]