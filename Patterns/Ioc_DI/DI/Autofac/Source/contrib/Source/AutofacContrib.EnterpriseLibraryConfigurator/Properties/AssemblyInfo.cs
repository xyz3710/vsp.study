﻿using System;
using System.Reflection;
using System.Resources;

[assembly: AssemblyTitle("AutofacContrib.EnterpriseLibraryConfigurator")]
[assembly: AssemblyDescription("Autofac support for Enterprise Library container configuration.")]
[assembly: CLSCompliant(true)]
[assembly: NeutralResourcesLanguage("en")]
