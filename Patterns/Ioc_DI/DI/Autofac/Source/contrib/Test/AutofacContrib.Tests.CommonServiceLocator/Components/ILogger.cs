namespace AutofacContrib.Tests.CommonServiceLocator.Components
{
	public interface ILogger
	{
		void Log(string msg);
	}
}