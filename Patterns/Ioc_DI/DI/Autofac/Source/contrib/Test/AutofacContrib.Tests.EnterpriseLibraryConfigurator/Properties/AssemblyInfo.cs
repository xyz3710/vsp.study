﻿using System.Reflection;

[assembly: AssemblyTitle("AutofacContrib.Tests.EnterpriseLibraryConfigurator")]
[assembly: AssemblyDescription("Tests for the Autofac container configurator for Enterprise Library.")]
