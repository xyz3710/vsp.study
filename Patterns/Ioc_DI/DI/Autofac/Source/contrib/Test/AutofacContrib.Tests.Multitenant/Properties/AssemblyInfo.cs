﻿using System.Reflection;

[assembly: AssemblyTitle("AutofacContrib.Tests.Multitenant")]
[assembly: AssemblyDescription("Tests for the Autofac multitenancy feature.")]
