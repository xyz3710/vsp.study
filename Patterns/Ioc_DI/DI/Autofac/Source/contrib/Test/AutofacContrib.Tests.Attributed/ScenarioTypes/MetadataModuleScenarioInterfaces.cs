﻿
namespace AutofacContrib.Tests.Attributed.ScenarioTypes
{
    public interface IMetadataModuleScenario { }
    public interface IMetadataModuleScenarioMetadata { string Name { get; } }

}
