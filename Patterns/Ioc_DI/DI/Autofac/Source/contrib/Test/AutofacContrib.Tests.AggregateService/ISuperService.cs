namespace AutofacContrib.Tests.AggregateService
{
    public interface ISuperService
    {
        ISomeDependency SomeDependency { get; }
    }
}
