﻿using System;
using System.Collections.Generic;
using System.Text;
//
using happydong.MVP.ViewController;
using happydong.MVP.Models;

namespace happydong.MVP.ViewController
{
    public class MainViewController : IMainViewController
    {
        #region IMainViewController

        /// <summary>
        /// 현재 컨트롤러와 연결된 폼(Form)을 지정/반환합니다.
        /// </summary>
        public IMainView CurrentForm { get; set; }

        /// <summary>
        /// 저장관련 내용을 정의합니다.
        /// </summary>
        /// <param name="saveObject">저장할 데이터모델</param>
        public void Save(IBaseObject saveObject)
        {
            if (saveObject.Save())
                CurrentForm.SaveComplete(saveObject);
        }

        /// <summary>
        /// 초기로드 내용을 정의합니다.
        /// </summary>
        public void Load()
        {
            DataModel dModel = new DataModel()
            {
                Title = "new UserInfo",
                UserInfoList = new List<UserInfo>()
                {
                    new UserInfo(){ FirstName = "Dongwook01", LastName = "Kim", BirthDate = DateTime.Now, Memo = "new Application" },
                    new UserInfo(){ FirstName = "Dongwook02", LastName = "Kim", BirthDate = DateTime.Now, Memo = "new Application" },
                    new UserInfo(){ FirstName = "Dongwook03", LastName = "Kim", BirthDate = DateTime.Now, Memo = "new Application" },
                    new UserInfo(){ FirstName = "Dongwook04", LastName = "Kim", BirthDate = DateTime.Now, Memo = "new Application" }
                }
            };

            dModel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(dModel_PropertyChanged);

            CurrentForm.LoadComplete(dModel);
        }

        void dModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Title"))
            {
                ((ICompanyMainView)CurrentForm).ShowMessage("Title이 변경되었습니다.");
            }
        }

        #endregion IMainViewController

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="currentForm">Form</param>
        public MainViewController(IMainView currentForm)
        {
            CurrentForm = currentForm;
        }

        /// <summary>
        /// 데이터를 새로고침합니다.
        /// </summary>
        public void Refresh()
        {
            DataModel dModel = new DataModel()
            {
                Title = "Refresh UserInfo",
                UserInfoList = new List<UserInfo>()
                {
                    new UserInfo(){ FirstName = "Dongwook01", LastName = "Kim", BirthDate = DateTime.Now, Memo = "Refresh" },
                    new UserInfo(){ FirstName = "Dongwook02", LastName = "Kim", BirthDate = DateTime.Now, Memo = "Refresh" },
                    new UserInfo(){ FirstName = "Dongwook03", LastName = "Kim", BirthDate = DateTime.Now, Memo = "Refresh" },
                    new UserInfo(){ FirstName = "Dongwook04", LastName = "Kim", BirthDate = DateTime.Now, Memo = "Refresh" }
                }
            };

            ((ICompanyMainView)CurrentForm).RefreshComplete(dModel);
        }
    }
}
