﻿using System;
using System.Collections.Generic;

//
using happydong.MVP.Models;

namespace happydong.MVP.ViewController
{
    public interface IMainView
    {
        /// <summary>
        /// 현재 사용 데이터모델을 지정/반환합니다.
        /// </summary>
        IBaseObject CurrentData { get; set; }

        /// <summary>
        /// 컨트롤러를 지정/반환합니다.
        /// </summary>
        IMainViewController MainController { get; set; }

        /// <summary>
        /// 내용을 저장하고 완료내용을 정의합니다.
        /// </summary>
        /// <param name="saveItem">저장할 데이터모델</param>
        void SaveComplete(IBaseObject saveItem);

        /// <summary>
        /// 로드후 완료내용을 정의합니다.
        /// </summary>
        /// <param name="loadItem">바딩인할 데이터모델</param>
        void LoadComplete(IBaseObject loadItem);
    }
}
