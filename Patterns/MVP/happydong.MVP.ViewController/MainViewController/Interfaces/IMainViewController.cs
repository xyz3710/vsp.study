﻿using System;
using System.Collections.Generic;
//
using happydong.MVP.Models;

namespace happydong.MVP.ViewController
{
    public interface IMainViewController
    {
        /// <summary>
        /// 현재 컨트롤러와 연결된 폼(Form)을 지정/반환합니다.
        /// </summary>
        IMainView CurrentForm { get; set; }

        /// <summary>
        /// 저장관련 내용을 정의합니다.
        /// </summary>
        /// <param name="saveObject">저장할 데이터모델</param>
        void Save(IBaseObject saveObject);

        /// <summary>
        /// 초기로드 내용을 정의합니다.
        /// </summary>
        void Load();
    }
}
