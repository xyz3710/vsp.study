﻿using System;
using System.Collections.Generic;
using happydong.MVP.Models;

namespace happydong.MVP.ViewController
{
    public interface ICompanyMainView : IMainView
    {
        /// <summary>
        /// 새로고침후 완료내용을 정의합니다.
        /// </summary>
        /// <param name="refreshItem">새로고침 데이터모델</param>
        void RefreshComplete(IBaseObject refreshItem);

        void ShowMessage(string msg);
    }
}
