﻿using System;
using System.Collections.Generic;

//
using System.ComponentModel;

namespace happydong.MVP.Models
{
    public class DataModel : IBaseObject, INotifyPropertyChanged
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        private IList<UserInfo> _userInfoList;
        public IList<UserInfo> UserInfoList
        {
            get { return _userInfoList; }
            set
            {
                _userInfoList = value;
                OnPropertyChanged("UserInfoList");
            }

        }

        public bool Save()
        {
            return true;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
