﻿using System;
using System.Collections.Generic;

namespace happydong.MVP.Models
{
    public class UserInfo : IBaseObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Memo { get; set; }

        public bool Save()
        {
            return true;
        }
    }
}
