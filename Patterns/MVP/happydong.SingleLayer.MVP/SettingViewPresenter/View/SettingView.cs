﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace happydong.SingleLayer.MVP
{
    public partial class SettingView : Form
    {
        public SettingView()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CommandCenter.StateChanged.Execute("세팅뷰에서 내용 전달");
        }
    }
}
