﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace happydong.SingleLayer.MVP
{
    public partial class SubView : UserControl, IBaseSubView
    {

        public event OnDetailInfo RequestDetailInfoEvent;

        public event EventHandler RefreshEvent;

        public SubView()
        {
            InitializeComponent();
        }

        #region IBaseSubView

        public int CurrentIdx { get; private set; }

        public void DataBinding(IBaseModel datalist)
        {
            bindingSource1.DataSource = datalist;
        }

        public void DetailDataBinding(IBaseModel detailData)
        {
            bindingSource2.DataSource = detailData;
        }

        #endregion IBaseSubView

        #region Control Events


        private void button1_Click(object sender, EventArgs e)
        {
            if (RefreshEvent != null)
                RefreshEvent(this, EventArgs.Empty);
        }


        private void dgvMainList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            if (dgvRelatedView.Columns.Count > 0)
                dgvRelatedView.Columns.Clear();

            foreach (DataGridViewColumn column in dgvContentView.Columns)
            {
                dgvRelatedView.Columns.Add(column.Clone() as DataGridViewColumn);
            }

            if (dgvRelatedView.Rows.Count > 0)
                dgvRelatedView.Rows.Clear();

            DataGridViewRow row = dgvContentView.Rows[e.RowIndex].Clone() as DataGridViewRow;
            int indexNum = 0;
            int tempVal = 0;
            foreach (DataGridViewCell cell in dgvContentView.Rows[e.RowIndex].Cells)
            {
                if (indexNum == 0)
                    tempVal = (int)cell.Value;

                row.Cells[indexNum].Value = cell.Value;
                indexNum++;
            }

            DataGridViewCell siteCell = dgvContentView.Rows[e.RowIndex].Cells[dgvContentView.Rows[e.RowIndex].Cells.Count - 1];

            dgvRelatedView.Rows.Add(row);

            if(!webBrowser1.Url.Equals(siteCell.Value.ToString()))
                webBrowser1.Url = new Uri(siteCell.Value.ToString());

            OnDetailInfo(tempVal);

        }

        private void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView listView = (DataGridView)sender;

            if (listView.Rows.Count > 0)
            {
                for (int i = 0; i < listView.Columns.Count; i++)
                {
                    listView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        #endregion Control Events

        private void OnDetailInfo(int idx)
        {
            if (idx == this.CurrentIdx)
                return;

            this.CurrentIdx = idx;
            if (RequestDetailInfoEvent != null)
                RequestDetailInfoEvent(this,this.CurrentIdx);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SettingView s = new SettingView();
            s.Show();
        }
    }
}
