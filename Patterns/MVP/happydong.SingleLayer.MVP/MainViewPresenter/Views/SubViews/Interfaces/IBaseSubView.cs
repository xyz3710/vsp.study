﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace happydong.SingleLayer.MVP
{
    public interface IBaseSubView
    {

        int CurrentIdx { get; }

        void DataBinding(IBaseModel datalist);

        void DetailDataBinding(IBaseModel detailData);
    }
}
