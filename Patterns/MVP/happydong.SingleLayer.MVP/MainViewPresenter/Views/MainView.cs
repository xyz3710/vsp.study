﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace happydong.SingleLayer.MVP
{
    public partial class MainView : Form , IMainView
    {

        SubView subview;

        public MainView()
        {
            InitializeComponent();

            InitializeSubViews();

            MainPresenter = new MainViewPresenter(this);
        }

        #region IMainView

        public IBaseModel CurrentData { get; set; }
        public IMainViewPresenter MainPresenter { get; set; }

        event OnDetailInfo IMainView.DetailInfoEvent
        {
            add { this.subview.RequestDetailInfoEvent += value; }
            remove { this.subview.RequestDetailInfoEvent -= value; }
        }


        public void DetailInfoComplete(IBaseModel detailItem)
        {
            subview.DetailDataBinding(detailItem);
        }

        public void ShowMessage(string msg)
        {
            MessageBox.Show(msg);
        }

        event EventHandler IMainView.LoadEvent
        {
            add { this.Load += value; }
            remove{ this.Load -= value;}
        }

        public void LoadComplete(IBaseModel loadItem)
        {
            if (loadItem == null)
                return;

            CurrentData = loadItem;
            subview.DataBinding(this.CurrentData);
        }

        event EventHandler IMainView.RefreshEvent
        {
            add { this.subview.RefreshEvent += value; }
            remove { this.subview.RefreshEvent -= value; }
        }

        public void RefreshComplete(IBaseModel refreshItem)
        {
            if (refreshItem == null)
                return;

            CurrentData = refreshItem;
            subview.DataBinding(this.CurrentData);
        }

        public void InitializeSubViews()
        {
            subview = new SubView();
            subview.Dock = DockStyle.Fill;
            this.Controls.Add(subview);
        }

        #endregion IMainView

    }
}