﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace happydong.SingleLayer.MVP
{
    public class UserInfoModel : IBaseModel
    {

        /// <summary>
        /// 사용자 공유번호를 지정/반환합니다.
        /// </summary>
        public int UserIdx { get; set; }

        /// <summary>
        /// 사용자 이름을 지정/반환합니다.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 사용자 나이를 지정/반환합니다.
        /// </summary>
        public int UserAge { get; set; }

        /// <summary>
        /// 사용자 주소를 지정/반환합니다.
        /// </summary>
        public string UserAddress { get; set; }

        /// <summary>
        /// 사용자 메일을 지정/반환합니다.
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// 사용자 웹사이트를 지정/반환합니다.
        /// </summary>
        public string UserSite { get; set; }

        /// <summary>
        /// 생성자
        /// </summary>
        public UserInfoModel()
        {
        }

        #region IBaseModel

        public bool Save()
        {
            return true;
        }

        #endregion IBaseModel

    }
}
