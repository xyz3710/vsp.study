﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace happydong.SingleLayer.MVP
{
    public delegate void OnDetailInfo(object sender,int idx);

    public interface IMainView
    {
        /// <summary>
        /// 현재 사용 데이터모델을 지정/반환합니다.
        /// </summary>
        IBaseModel CurrentData { get; set; }

        /// <summary>
        /// 컨트롤러를 지정/반환합니다.
        /// </summary>
        IMainViewPresenter MainPresenter { get; set; }

        void InitializeSubViews();

        void ShowMessage(string msg);

        event EventHandler LoadEvent;
        void LoadComplete(IBaseModel loadItem);

        event OnDetailInfo DetailInfoEvent;
        void DetailInfoComplete(IBaseModel detailItem);

        event EventHandler RefreshEvent;
        void RefreshComplete(IBaseModel refreshItem);
    }
}
