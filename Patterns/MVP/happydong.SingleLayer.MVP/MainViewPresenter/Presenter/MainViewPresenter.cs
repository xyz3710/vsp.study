﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace happydong.SingleLayer.MVP
{
    public class MainViewPresenter : IMainViewPresenter
    {

        public IMainView CurrentForm { get; set; }

        /// <summary>
        /// 생성자
        /// </summary>
        public MainViewPresenter(IMainView view)
        {
            CurrentForm = view;
            CurrentForm.LoadEvent += new EventHandler(CurrentForm_Onload);
            CurrentForm.RefreshEvent += new EventHandler(CurrentForm_RefreshEvent);
            CurrentForm.DetailInfoEvent += new OnDetailInfo(CurrentForm_OnDetailInfoEvent);

            CommandCenter.StateChanged.Executed += new EventHandler<ExecutedEventArgs>(StateChanged_Executed);
        }

        void StateChanged_Executed(object sender, ExecutedEventArgs e)
        {
            CurrentForm.ShowMessage(e.Parameter.ToString());
        }

        #region Events

        void CurrentForm_RefreshEvent(object sender, EventArgs e)
        {
            ResultModel result = new ResultModel()
            {
                UserList = new List<UserInfoModel>()
                {
                    new UserInfoModel(){ UserIdx = 100,  UserName = "[Refresh] 김동욱01", UserAge = 30, UserAddress = "[Refresh] 서울시 광진구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.happydong.kr"},
                    new UserInfoModel(){ UserIdx = 101,  UserName = "[Refresh] 김동욱02", UserAge = 30, UserAddress = "[Refresh] 서울시 동대문구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.google.co.kr"},
                    new UserInfoModel(){ UserIdx = 102,  UserName = "[Refresh] 김동욱03", UserAge = 30, UserAddress = "[Refresh] 서울시 강남구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.naver.com"},
                    new UserInfoModel(){ UserIdx = 103,  UserName = "[Refresh] 김동욱04", UserAge = 30, UserAddress = "[Refresh] 서울시 강북구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.devpia.com"},
                    new UserInfoModel(){ UserIdx = 104,  UserName = "[Refresh] 김동욱05", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.nate.com"},
                    new UserInfoModel(){ UserIdx = 105,  UserName = "[Refresh] 김동욱05", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.me2day.net"},
                    new UserInfoModel(){ UserIdx = 106,  UserName = "[Refresh] 김동욱06", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.gmail.com"},
                    new UserInfoModel(){ UserIdx = 107,  UserName = "[Refresh] 김동욱07", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.live.com"},
                    new UserInfoModel(){ UserIdx = 108,  UserName = "[Refresh] 김동욱08", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.happydong.kr"},
                    new UserInfoModel(){ UserIdx = 109,  UserName = "[Refresh] 김동욱09", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.hoons.kr"},
                    new UserInfoModel(){ UserIdx = 110,  UserName = "[Refresh] 김동욱10", UserAge = 30, UserAddress = "[Refresh] 서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.show.co.kr"}
                },
            };

            CurrentForm.RefreshComplete(result);
        }

        void CurrentForm_OnDetailInfoEvent(object sender, int idx)
        {
            MemoModel memoData = new MemoModel();

            for (int i = 0; i < 20; i++)
            {
                if (i < 10)
                {
                    memoData.Title = string.Format("{0}_{1}", memoData.Title, idx.ToString());
                }

                memoData.Description = string.Format("{0}__{1}", memoData.Description, idx);
            }

            CurrentForm.DetailInfoComplete(memoData);
        }

        void CurrentForm_Onload(object sender, EventArgs e)
        {

            ResultModel result = new ResultModel()
            {
                UserList = new List<UserInfoModel>()
                {
                    new UserInfoModel(){ UserIdx = 100,  UserName = "김동욱01", UserAge = 30, UserAddress = "서울시 광진구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.happydong.kr"},
                    new UserInfoModel(){ UserIdx = 101,  UserName = "김동욱02", UserAge = 30, UserAddress = "서울시 동대문구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.google.co.kr"},
                    new UserInfoModel(){ UserIdx = 102,  UserName = "김동욱03", UserAge = 30, UserAddress = "서울시 강남구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.naver.com"},
                    new UserInfoModel(){ UserIdx = 103,  UserName = "김동욱04", UserAge = 30, UserAddress = "서울시 강북구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.devpia.com"},
                    new UserInfoModel(){ UserIdx = 104,  UserName = "김동욱05", UserAge = 30, UserAddress = "서울시 서초구 ....", UserEmail = "happydong@camdensoft.com", UserSite = "http://www.nate.com"}
                },
            };

            CurrentForm.LoadComplete(result);

        }

        #endregion Events

    }
}
