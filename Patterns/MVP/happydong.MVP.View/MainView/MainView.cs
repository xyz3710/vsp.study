﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using happydong.MVP.ViewController;
using happydong.MVP.Models;

namespace happydong.MVP.View
{
    public partial class MainView : Form , ICompanyMainView
    {
        #region ICompanyMainView

        /// <summary>
        /// 현재 사용 데이터모델을 지정/반환합니다.
        /// </summary>
        public IBaseObject CurrentData { get; set; }

        /// <summary>
        /// 컨트롤러를 지정/반환합니다.
        /// </summary>
        public IMainViewController MainController { get; set; }

        /// <summary>
        /// 새로고침후 완료내용을 정의합니다.
        /// </summary>
        /// <param name="refreshItem">새로고침 데이터모델</param>
        public void RefreshComplete(IBaseObject refreshItem)
        {
            CurrentData = refreshItem;
            bindingSource1.DataSource = CurrentData;
        }

        /// <summary>
        /// 내용을 저장하고 완료내용을 정의합니다.
        /// </summary>
        /// <param name="saveItem">저장할 데이터모델</param>
        public void SaveComplete(IBaseObject saveItem)
        {
            MessageBox.Show("Save success");
        }

        /// <summary>
        /// 로드후 완료내용을 정의합니다.
        /// </summary>
        /// <param name="loadItem">바딩인할 데이터모델</param>
        public void LoadComplete(IBaseObject loadItem)
        {
            CurrentData = loadItem;
            bindingSource1.DataSource = CurrentData;
        }

        public void ShowMessage(string msg)
        {
            MessageBox.Show(msg);
        }

        #endregion ICompanyMainView

        #region 생성자

        public MainView()
        {
            CurrentData = new DataModel();
            InitializeComponent();
        }

        public MainView(DataModel dataModel)
            : this()
        {
            CurrentData = dataModel;
        }

        #endregion 

        #region Form LoadEvent

        /// <summary>
        /// 폼로드 이벤트를 정의합니다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainView_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        /// <summary>
        /// 초기세팅을 정의합니다.
        /// </summary>
        private void InitForm()
        {
            bindingSource1.DataSource = CurrentData;
            MainController = new MainViewController(this);
            MainController.Load();
        }

        #endregion Form LoadEvent

        #region Control Events

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("종료 하시겠습니까?", "MVC Appliction", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MainController.Save(CurrentData);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ((MainViewController)MainController).Refresh();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgvControl = (DataGridView)sender;
            dgvControl.Columns[dgvControl.ColumnCount -1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        #endregion Control Events

    }
}
