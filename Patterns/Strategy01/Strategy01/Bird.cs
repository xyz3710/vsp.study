﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Strategy01
{
	class Bird
	{
		private string _name;
		private string _fly;
		private string _sound;

		public Bird(string name)
		{
			_name = name;
		}

		public string GetName()
		{
			return _name;
		}

		public void SetFly(IFlyable fly)
		{
			_fly = fly.Fly();
		}

		public void SetSound(ISoundable sound)
		{
			_sound = sound.Sound();
		}

		public override string ToString()
		{
			return $"{_name}: {_fly} {_sound}";
		}
	}
}
