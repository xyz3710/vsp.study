﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy01
{
	interface ISoundable
	{
		string Sound();
	}

	public class Kokio : ISoundable
	{
		public string Sound()
		{
			return "꼬끼오";
		}
	}

	public class Quak : ISoundable
	{
		public string Sound()
		{
			return "꽥꽥";
		}
	}

	public class Silent : ISoundable
	{
		public string Sound()
		{
			return "조용";
		}
	}
}
