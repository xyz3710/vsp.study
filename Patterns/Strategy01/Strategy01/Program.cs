﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strategy01
{
	static class Program
	{
		static void Main()
		{
			var bird = new Bird("펭귄");

			bird.SetFly(new DontFly());
			bird.SetSound(new Silent());

			Console.WriteLine(bird);
		}
	}
}
