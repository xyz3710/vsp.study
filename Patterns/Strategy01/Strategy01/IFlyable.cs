﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy01
{
	interface IFlyable
	{
		string Fly();
	}

	public class DontFly : IFlyable
	{
		public string Fly()
		{
			return "날 수 없어요";
		}
	}

	public class FastFly : IFlyable
	{
		public string Fly()
		{
			return "매우 빠르게 날아갑니다.";
		}
	}

	public class SlowFly : IFlyable
	{
		public string Fly()
		{
			return "천천히 날아갑니다.";
		}
	}
}
