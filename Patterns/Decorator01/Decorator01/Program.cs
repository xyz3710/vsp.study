﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decorator01
{
	static class Program
	{
		static void Main()
		{
			var darkRoast = new DarkRoast();
			var coffee1 = new Whip(new Mocha(new Mocha(new Mocha(darkRoast))));

			Console.WriteLine($"{coffee1.GetDescription()} {coffee1.Cost}");

			var espresso = new Espresso();
			var coffee2 = new Whip(new Mocha(new Soy(espresso)));

			Console.WriteLine($"{coffee2.GetDescription()} {coffee2.Cost}");
		}
	}
}
