﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator01
{
	public class DarkRoast : Beverage
	{
		public DarkRoast()
		{
			_description = "다크로스트";
		}

		public override double Cost
		{
			get
			{
				return 0.9d;
			}
		}
	}

	public class Decaf : Beverage
	{
		public Decaf()
		{
			_description = "디카페인";
		}

		public override double Cost
		{
			get
			{
				return 1.05d;
			}
		}
	}

	public class Espresso : Beverage
	{
		public Espresso()
		{
			_description = "에스프레소";
		}

		public override double Cost
		{
			get
			{
				return 0.89d;
			}
		}
	}
}
