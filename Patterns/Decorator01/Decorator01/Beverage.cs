﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator01
{
	public abstract class Beverage
	{
		protected string _description = "No Name";

		public virtual string GetDescription()
		{
			return _description;
		}

		public abstract double Cost
		{
			get;
		}
	}
}
