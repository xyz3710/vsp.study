﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator01
{
	public class Mocha : CondimentDecorator
	{
		private Beverage _beverage;

		public Mocha(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string GetDescription()
		{
			return $"{_beverage.GetDescription()} 모카";
		}

		public override double Cost
		{
			get
			{
				return _beverage.Cost + 0.2d;
			}
		}
	}

	public class Soy : CondimentDecorator
	{
		private Beverage _beverage;

		public Soy(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string GetDescription()
		{
			return $"{_beverage.GetDescription()} 두유";
		}

		public override double Cost
		{
			get
			{
				return _beverage.Cost + 0.15d;
			}
		}
	}

	public class SteamMilk : CondimentDecorator
	{
		private Beverage _beverage;

		public SteamMilk(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string GetDescription()
		{
			return $"{_beverage.GetDescription()} 스팀밀크";
		}

		public override double Cost
		{
			get
			{
				return _beverage.Cost + 0.1d;
			}
		}
	}

	public class Whip : CondimentDecorator
	{
		private Beverage _beverage;
		
		public Whip(Beverage beverage)
		{
			_beverage = beverage;
		}

		public override string GetDescription()
		{
			return $"{_beverage.GetDescription()} 휘핑크림";
		}

		public override double Cost
		{
			get
			{
				return _beverage.Cost + 0.1d;
			}
		}
	}
}
