using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VistorPattern01
{
	public class Body : ICarElement
	{
		#region ICarElement ���

		public void Accept(ICarElementVisitor visitor)
		{
			visitor.Visit(this);
		}

		#endregion
	}
}
