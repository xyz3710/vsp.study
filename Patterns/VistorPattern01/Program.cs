﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VistorPattern01
{
	class Program
	{
		static void Main(string[] args)
		{
			Car car = new Car();

			car.Accept(new CarElementPrintVisitor());
			car.Accept(new CarElementDoVisitor ());
		}
	}
}
