using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VistorPattern01
{
	public class Wheel : ICarElement
	{
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		/// <value>Name</value>
		public string Name
		{
			get;
			set;
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the Wheel class.
		/// </summary>
		public Wheel(string name)
		{
			Name = name;
		}
		#endregion

		#region ICarElement 멤버

		public void Accept(ICarElementVisitor visitor)
		{
			visitor.Visit(this);
		}

		#endregion
	}
}
