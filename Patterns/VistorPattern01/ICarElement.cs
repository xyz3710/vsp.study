﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VistorPattern01
{
	public interface ICarElement
	{
		void Accept(ICarElementVisitor visitor);
	}
}
