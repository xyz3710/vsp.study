using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VistorPattern01
{
	public interface ICarElementVisitor
	{
		void Visit(Wheel wheel);
		void Visit(Engine engine);
		void Visit(Body body);
		void Visit(Car car);
	}
}
