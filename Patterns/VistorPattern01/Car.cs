using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VistorPattern01
{
	public class Car : ICarElement
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the Car class.
		/// </summary>
		public Car()
		{
			CarElements = new ICarElement[] { 
				new Wheel("font left"), new Wheel("font right"),
				new Wheel("back left"), new Wheel("back right"),
				new Body(), new Engine(),
			};
		}
		#endregion

		/// <summary>
		/// ICarElement를 구하거나 설정합니다.
		/// </summary>
		public ICarElement[] CarElements
		{
			get;
			set;
		}

		#region ICarElement 멤버

		public void Accept(ICarElementVisitor visitor)
		{
			foreach (ICarElement carElement in CarElements)
			{
				carElement.Accept(visitor);
			}

			visitor.Visit(this);
			
		}

		#endregion
	}
}
