﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VistorPattern01
{
	public class CarElementPrintVisitor : ICarElementVisitor
	{
		#region ICarElementVisitor 멤버

		public void Visit(Wheel wheel)
		{
			Console.WriteLine("Visiting : " + wheel.Name);
		}

		public void Visit(Engine engine)
		{
			Console.WriteLine("Visiting Engine");
		}

		public void Visit(Body body)
		{
			Console.WriteLine("Visiting Body");
		}

		public void Visit(Car car)
		{
			Console.WriteLine("Visiting Car");
		}

		#endregion
	}
}
