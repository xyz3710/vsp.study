﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VistorPattern01
{
	public class CarElementDoVisitor : ICarElementVisitor
	{
		#region ICarElementVisitor 멤버

		public void Visit(Wheel wheel)
		{
			Console.WriteLine("Kicking my {0} wheel", wheel.Name);
		}

		public void Visit(Engine engine)
		{
			Console.WriteLine("Starting my Engine");
		}

		public void Visit(Body body)
		{
			Console.WriteLine("Moving my Body");
		}

		public void Visit(Car car)
		{
			Console.WriteLine("Staring my Car");
		}

		#endregion
	}
}
