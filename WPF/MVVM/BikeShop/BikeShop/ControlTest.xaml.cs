﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BikeShop
{
	/// <summary>
	/// ControlTest.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class ControlTest : Page, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return System.Convert.ToInt32(value) * 2;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public ControlTest()
		{
			InitializeComponent();

			var elements = (Content as StackPanel).Children;
			var uniformGrid = elements.OfType<System.Windows.Controls.Primitives.UniformGrid>().FirstOrDefault();

			if (uniformGrid != null)
			{
				var label = new Label { Content = "추가 컨트롤", };

				uniformGrid.Children.Add(label);
			}

			foreach (var element in elements.OfType<Panel>())
			{
				element.Margin = new Thickness(10);
				element.Background = Brushes.White;
				element.ToolTip = element.Name;
			}

			//var line = elements.OfType<Line>().FirstOrDefault();
			Loaded += (sender, e) =>
			{
				var r = new Random(1);
				var g = new Random(1);
				var b = new Random(1);
				var colors = new List<Color>();

				for (int i = 0; i < 10; i++)
				{
					colors.Add(Color.FromRgb((byte)r.Next(1, 255), (byte)g.Next(1, 255), (byte)b.Next(1, 255)));
				}

				DataContext = Enumerable.Range(0, 9).Select(x => new Car { Speed = x * 2d, Color = colors[x] });
			};
		}

		private void OnButton_Click(object sender, RoutedEventArgs e)
		{
			MessageBox.Show(System.Convert.ToString((sender as Button).Content));
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
