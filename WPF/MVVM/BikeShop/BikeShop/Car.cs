﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace BikeShop
{
	public class Car
	{
		public double Speed { get; set; }

		public Color Color { get; set; }

		public Human Human { get; set; }

		/// <summary>
		/// Car를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return $"Speed:{Speed}, Color:{Color}, Human:{Human}";
		}
	}

	public class Human
	{
		public string Name { get; set; }

		public int Age { get; set; }


	}
}
