﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Ch01_SayHello
{
	/// <summary>
	/// App.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class App : Application
	{
		public App()
		{
			System.Diagnostics.Debug.WriteLine("App.ctor");
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			System.Diagnostics.Debug.WriteLine("App.OnActivated");
		}

		protected override void OnDeactivated(EventArgs e)
		{
			base.OnDeactivated(e);
			System.Diagnostics.Debug.WriteLine("App.OnDeactivated");
		}

		protected override void OnExit(ExitEventArgs e)
		{
			base.OnExit(e);
			System.Diagnostics.Debug.WriteLine("App.OnExit");
		}

		protected override void OnFragmentNavigation(System.Windows.Navigation.FragmentNavigationEventArgs e)
		{
			base.OnFragmentNavigation(e);
			System.Diagnostics.Debug.WriteLine("App.OnFragmentNavigation");
		}

		protected override void OnLoadCompleted(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnLoadCompleted(e);
			System.Diagnostics.Debug.WriteLine("App.OnLoadCompleted");
		}

		protected override void OnNavigated(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigated(e);
			System.Diagnostics.Debug.WriteLine("App.OnNavigated");
		}

		protected override void OnNavigating(System.Windows.Navigation.NavigatingCancelEventArgs e)
		{
			base.OnNavigating(e);
			System.Diagnostics.Debug.WriteLine("App.OnNavigating");
		}

		protected override void OnNavigationFailed(System.Windows.Navigation.NavigationFailedEventArgs e)
		{
			base.OnNavigationFailed(e);
			System.Diagnostics.Debug.WriteLine("App.OnNavigationFailed");
		}

		protected override void OnNavigationProgress(System.Windows.Navigation.NavigationProgressEventArgs e)
		{
			base.OnNavigationProgress(e);
			System.Diagnostics.Debug.WriteLine("App.OnNavigationProgress");
		}

		protected override void OnNavigationStopped(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigationStopped(e);
			System.Diagnostics.Debug.WriteLine("App.OnNavigationStopped");
		}

		protected override void OnSessionEnding(SessionEndingCancelEventArgs e)
		{
			base.OnSessionEnding(e);
			System.Diagnostics.Debug.WriteLine("App.OnSessionEnding");
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			ShutdownMode = System.Windows.ShutdownMode.OnMainWindowClose;
			System.Diagnostics.Debug.WriteLine("App.OnStartup");

			for (int i = 0; i < 2; i++)
			{
				var win = new Window
				{
					Title = "Extra window no. " + i.ToString(),
					ShowInTaskbar = false,
				};

				win.Show();
			}

		}
	}
}
