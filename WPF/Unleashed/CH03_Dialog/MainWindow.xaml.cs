﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CH3_Dialog
{
	/// <summary>
	/// MainWindow.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			PrintLogicalTree(0, this);
			//AddHandler(Window.MouseDownEvent, new MouseButtonEventHandler(OnAboutDialogMouseRightButtonDown), true);
			AddHandler(ListBox.SelectionChangedEvent, new SelectionChangedEventHandler(GenericHandler));
			AddHandler(Button.ClickEvent, new RoutedEventHandler(GenericHandler));
			InputBindings.Add(new InputBinding(ApplicationCommands.NotACommand, new KeyGesture( Key.F1)));
			InputBindings.Add(new InputBinding(ApplicationCommands.Help, new KeyGesture(Key.F2)));
		}

		protected override void OnContentRendered(EventArgs e)
		{
			base.OnContentRendered(e);
			PrintVisualTree(0, this);
		}

		protected override void OnPreviewKeyDown(KeyEventArgs e)
		{
			base.OnPreviewKeyDown(e);

			switch (e.Key)
			{
				case Key.Q:
					if (Keyboard.IsKeyDown(Key.LeftCtrl))
					{
						Close();
					}

					break;
			}
		}

		private void PrintLogicalTree(int depth, object obj)
		{
			// 계층구조의 깊이를 표현하기 위해 빈 공간을 가진 객체를 찍는다.
			Debug.WriteLine(new string('\t', depth) + obj);

			// 가끔 마지막 엘리먼트가 문자열처럼 DependencyObjects가 아니라면
			if (obj is DependencyObject == false)
			{
				return;
			}

			// 로지컬 트리의 자식 엘리먼트를 재귀적으로 호출
			foreach (var child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
			{
				PrintLogicalTree(depth + 1, child);
			}
		}

		private void PrintVisualTree(int depth, DependencyObject obj)
		{
			// 계층구조의 깊이를 표현하기 위해 빈 공간을 가진 객체를 찍는다.
			Debug.WriteLine(new string('\t', depth) + obj);

			// 비주얼 트리의 자식 엘리먼트를 재귀적으로 호출하낟.
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				PrintVisualTree(depth + 1, VisualTreeHelper.GetChild(obj, i));
			}
		}

		private void OnAboutDialogMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			Title = "Source = " + e.Source.GetType().Name + ", OriginalSource = " + e.OriginalSource.GetType().Name + " @ " + e.Timestamp;

			// Control 클래스에 상속받은 모든 가능한 소스를 표시한다.
			var control = e.Source as Control;

			// Source 컨트롤상 테두리의 보였다 안보였다를 전환할 수 있도록 한다.
			if (control.BorderThickness != new Thickness(5))
			{
				control.BorderThickness = new Thickness(5);
				control.BorderBrush = Brushes.Black;
			}
			else
			{
				control.BorderThickness = new Thickness(0);
			}
		}

		private void OnButtonClick(object sender, RoutedEventArgs e)
		{
			MessageBox.Show("You just clicked " + e.Source);
		}

		private void OnListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				MessageBox.Show("You just selected " + e.AddedItems[0]);
			}
		}

		private void GenericHandler(object sender, RoutedEventArgs e)
		{
			if (e.RoutedEvent == Button.ClickEvent)
			{
				MessageBox.Show("[GENERIC]You just clicked " + e.Source);

			}
			else if (e.RoutedEvent == ListBox.SelectionChangedEvent)
			{
				var se = e as SelectionChangedEventArgs;

				MessageBox.Show("[GENERIC]You just selected " + se.AddedItems[0]);
			}
		}

		private void OnHelpCanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void OnHelpExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			Process.Start("http://xyz37.blog.me");
		}
	}
}
