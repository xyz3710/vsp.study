﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CH04_NewControls
{
	/// <summary>
	/// MainWindow.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		protected override void OnPreviewKeyDown(KeyEventArgs e)
		{
			base.OnPreviewKeyDown(e);

			switch (e.Key)
			{
				case Key.Q:
					if (Keyboard.IsKeyDown(Key.LeftCtrl))
					{
						Close();
					}

					break;
			}
		}

		private void OnButtonClick(object sender, RoutedEventArgs e)
		{
			var message = $"{sender.GetType()} button clicked.";

			lblStatus.Content = message;
			System.Diagnostics.Debug.WriteLine(message);
		}

		private void ToggleButton_Checked(object sender, RoutedEventArgs e)
		{
			expander.IsExpanded = (sender as ToggleButton).IsChecked ?? false;
		}

		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				System.Diagnostics.Debug.WriteLine(GetText(e.AddedItems[0] as StackPanel), "current: ");
			}

			if (e.RemovedItems.Count > 0)
			{
				System.Diagnostics.Debug.WriteLine(GetText(e.RemovedItems[0] as StackPanel), "previous: ");
			}
		}

		private string GetText(StackPanel panel)
		{
			if (panel == null)
			{
				return string.Empty;
			}

			var last = panel.Children.OfType<StackPanel>().SelectMany(x => x.Children.OfType<TextBlock>()).FirstOrDefault();

			return last?.Text;
		}

		private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				System.Diagnostics.Debug.WriteLine(GetText((e.AddedItems[0] as ListBoxItem).Content as StackPanel), "LB current: ");
			}

			if (e.RemovedItems.Count > 0)
			{
				System.Diagnostics.Debug.WriteLine(GetText((e.RemovedItems[0] as ListBoxItem).Content as StackPanel), "LB previous: ");
			}
		}

		private void OnChangeListBoxOrientationButtonClick(object sender, RoutedEventArgs e)
		{
			if (lbTest.ItemsPanel != null)
			{
				var panelTemplate = lbTest.ItemsPanel as ItemsPanelTemplate;

				if (panelTemplate.VisualTree == null)
				{
					return;
				}

				/*
					var pi = panelTemplate.VisualTree.Type.GetProperty(nameof(VirtualizingStackPanel.Orientation));

					if (pi != null)
					{
						//var orient1 = (Orientation)pi.GetValue();
					}

					var vsp = Activator.CreateInstance(panelTemplate.VisualTree.Type) as VirtualizingStackPanel;

					if (vsp != null)
					{
						System.Diagnostics.Debug.WriteLine(vsp.Orientation, "MainWindow.OnChangeListBoxOrientationButtonClick");
						var orient = !Convert.ToBoolean((int)vsp.Orientation);

						vsp.Orientation = (Orientation)Convert.ToInt32(orient);

						return;
					}
				*/
			}

			var panelFactory = new FrameworkElementFactory(typeof(VirtualizingStackPanel));

			panelFactory.SetValue(VirtualizingStackPanel.OrientationProperty, Orientation.Horizontal);
			//panelFactory.SetValue(VirtualizingStackPanel.OrientationProperty, Orientation.Vertical);

			lbTest.ItemsPanel = new ItemsPanelTemplate(panelFactory);

			//var prevOrient = lbTest.ItemsPanel as ItemsPanelTemplate;
			var listBox = FindByName(nameof(lbTest), this) as ListBox;

			listBox.Items.SortDescriptions.Clear();
			//listBox.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Content", System.ComponentModel.ListSortDirection.Ascending));

		}

		private FrameworkElement FindByName(string name, FrameworkElement root)
		{
			Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
			tree.Push(root);

			while (tree.Count > 0)
			{
				FrameworkElement current = tree.Pop();
				if (current.Name == name)
					return current;

				int count = VisualTreeHelper.GetChildrenCount(current);
				for (int i = 0; i < count; ++i)
				{
					DependencyObject child = VisualTreeHelper.GetChild(current, i);
					if (child is FrameworkElement)
						tree.Push((FrameworkElement)child);
				}
			}

			return null;
		}

		private void OnOpenClick(object sender, RoutedEventArgs e)
		{
			MessageBox.Show("Open");
		}
	}
}
