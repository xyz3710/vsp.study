﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch04_Join
{
	class Program
	{
		static void Main(string[] args)
		{
			// Cross Join
			var binary = new int[] { 0, 1 };
			var q = from b4 in binary
					from b3 in binary
					from b2 in binary
					from b1 in binary
					select new
					{
						Value = b4 << 3 | b3 << 2 | b2 << 1 | b1 << 0,
						Digit = string.Format("{0}{1}{2}{3}", b4, b3, b2, b1),
					};

			foreach (var item in q)
				Console.WriteLine("{0}: {1}", item.Digit, item.Value);
		}
	}
}
