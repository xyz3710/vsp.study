﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch09_ParallelLinq
{
	class Program
	{
		static void Main(string[] args)
		{
			TestAsParallel();
			TestSimpleSequentialVariance();
		}

		private static void TestSimpleSequentialVariance()
		{

		}

		private static void TestAsParallel()
		{
			int[] source1 = { 1, 2, 3, 4, 5 };
			int[] source2 = { 6, 7, 8, 9, 10 };
			var q = source1.AsParallel().Concat(source2.AsParallel());
			var q2 = source1.AsParallel().AsOrdered().Concat(source2.AsParallel());
		}

	}

	public static class VarianceExtension
	{
		// Sequential variance calculation
		public static double Variance1(this IEnumerable<int> source)
		{
			double mean = source.Average();		// 고전적인 방법

			return source.Aggregate(
				(double)0d,
				(subTotal, item) =>
					subTotal += Math.Pow((item - mean), 2),
				(totalSum) => totalSum / (source.Count() - 1));
		}

		public static double Variance2(this IEnumerable<int> source)
		{
			double mean = source.Average();		// 고전적인 방법

			return source.Aggregate(
				(double)0d,
				(subTotal, item) =>
					subTotal + ((double)(item - mean)) * ((double)(item - mean)),
				(totalSum) => totalSum / (source.Count() - 1));
		}

		public static double Variance(this IEnumerable<int> source)
		{
			// optimization 2 - removing count and mean calcs
			return source.Aggregate(
				new double[] { 0d, 0d, 0d },	// seed - array of three doubles
				(subTotal, item) =>			// item aggregation function, run for each element
				{
					subTotal[0]++;		// count
					subTotal[1] += item;		// sum
					subTotal[2] += (double)item * (double)item;	// sum of squares

					return subTotal;
				},
				// result selector function
				// (finesses the final sum into the variance)
				// mean = sum / count
				// variance = (sum_sqr - sum * mean) / (n - 1)
				result => (result[2] - (result[1] * (result[1] / result[0]))) / (result[0] - 1)
			);
		}

		public static double StandardDeviation(this IEnumerable<int> source)
		{
			return Math.Sqrt(source.Variance());
		}

		public static double StandardDeviation<T>(this IEnumerable<T> source, Func<T, int> selector)
		{
			return StandardDeviation(Enumerable.Select(source, selector));
		}

		public static double Variance(this ParallelQuery<int> source)
		{
			/* based upone the blog posting by Igor Ostrovsky at -
			 * http://blogs.msdn.com/pfxteam/archive/2008/06/05/8576194.aspx
			 * which demonstrates how to use the factory functions in an
			 * Aggregate function for efficiency
			 */

			// check for invaalid source condition
			if (source == null)
				throw new ArgumentNullException("source");
				
			return source.Aggregate(
				// seed - arry of three double constructed
				// using factory function, initialized to 0
				new double[] { 0d, 0d, 0d },
				// item aggregation function, run for each element
				(subTotal, item) =>
				{
					subTotal[0]++;		// count
					subTotal[1] += item;		// sum
					// sum of squares
					subTotal[2] += (double)item * (double)item;

					return subTotal;
				},
				// combine function,
				// run on completion of each *thread*
				(total, thisThread) =>
				{
					total[0] += thisThread[0];
					total[1] += thisThread[1];
					total[2] += thisThread[2];

					return total;
				},
				// result selector functino
				// finesses the final sum into the variance
				// mean = sum / count
				// variance = (sum_sqr - sum * mean) / (n - 1)
				(result) =>
					result[0] > 1 ?
						(result[2] - result[1] * result[0]) / (result[0] - 1)
						: 0d
			);
		}

		public static double StandardDeviation(this ParallelQuery<int> source)
		{
			return Math.Sqrt(source.Variance());
		}

		private static double StandardDeviation<T>(this ParallelQuery<T> source, Func<T, int> selector)
		{
			return StandardDeviation(ParallelEnumerable.Select(source, selector));
		}
	}
}
