﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch03_SelectSelectMany
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] sentence = { "The quick brown", "fox jumps over", "the lazy dog." };

			PrintTitle(1);
			IEnumerable<string[]> words1 = sentence.Select(w => w.Split(' '));
			foreach (string[] segment in words1)
				foreach (string word in segment)
					Console.WriteLine(word);

			PrintTitle(2);
			IEnumerable<string> words2 = sentence.SelectMany(segment => segment.Split(' '));
			foreach (string word in words2)
				Console.WriteLine(word);

			PrintTitle(3);
			IEnumerable<string> words3 = from segment in sentence
										 from word in segment.Split(' ')
										 select word;
			foreach (string word in words3)
				Console.WriteLine(word);

			var words4 = words3.Select((w, idx) => new
			{
				Index = idx,
				Word = w,
			});

			foreach (var item in words4)
				Console.WriteLine("{0}: {1}", item.Index, item.Word);

			Console.WriteLine();
			SelectManyEx1();
		}

		private static void PrintTitle(int no)
		{
			Console.WriteLine("\r\nOption {0}:\r\n============", no);
		}

		class PetOwner
		{
			public string Name
			{
				get;
				set;
			}
			public List<String> Pets
			{
				get;
				set;
			}
		}

		public static void SelectManyEx1()
		{
			PetOwner[] petOwners = 
					{ new PetOwner { Name="Higa, Sidney", 
						  Pets = new List<string>{ "Scruffy", "Sam" } },
					  new PetOwner { Name="Ashkenazi, Ronen", 
						  Pets = new List<string>{ "Walker", "Sugar" } },
					  new PetOwner { Name="Price, Vernette", 
						  Pets = new List<string>{ "Scratches", "Diesel" } } };

			// Query using SelectMany().
			IEnumerable<string> query1 = petOwners.SelectMany(petOwner => petOwner.Pets);

			Console.WriteLine("Using SelectMany():");

			// Only one foreach loop is required to iterate 
			// through the results since it is a
			// one-dimensional collection.
			foreach (string pet in query1)
			{
				Console.WriteLine(pet);
			}

			// This code shows how to use Select() 
			// instead of SelectMany().
			IEnumerable<List<String>> query2 =
				petOwners.Select(petOwner => petOwner.Pets);

			Console.WriteLine("\nUsing Select():");

			// Notice that two foreach loops are required to 
			// iterate through the results
			// because the query returns a collection of arrays.
			foreach (List<String> petList in query2)
			{
				foreach (string pet in petList)
				{
					Console.WriteLine(pet);
				}
				Console.WriteLine();
			}
		}
	}
}
