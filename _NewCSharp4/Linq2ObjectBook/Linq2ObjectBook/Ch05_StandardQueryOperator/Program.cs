﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch_05_StandardQueryOperator
{
	class Program
	{
		static void Main(string[] args)
		{
			TestAggregate();
			Console.WriteLine();

			TestOfType();
			Console.WriteLine();

			TestDistinct();
			Console.WriteLine();

			TestRange();
			Console.WriteLine();

			TestSkipWhile();
			Console.WriteLine();

			TestPage();
			Console.WriteLine();
		}

		private static void TestPage()
		{
			int[] nums = Enumerable.Range(1, 50).ToArray();
			int page = 3;
			int pageSize = 10;

			var q = nums.Page(page, pageSize);

			Console.WriteLine("Page: {0}, PageSize: {1}, from:{2}, to:{3}",  page, pageSize, 1, 50);
			foreach (var item in q)
				Console.WriteLine(item + " ");
		}
		
		private static void TestSkipWhile()
		{
			string sampleString =
				@"# comment line 1
#comment line2
Data line 1
Data line 2

This line is ignored
";

			var q = sampleString.Split('\n')
				.SkipWhile(line => line.StartsWith("#"))
				.TakeWhile(line => !string.IsNullOrEmpty(line.Trim()));

			foreach (var s in q)
				Console.WriteLine(s);
		}

		private static void TestRange()
		{
			var q = from X in Enumerable.Range(0, 5)
					from Y in Enumerable.Range(0, 5)
					let Pixel = X + Y
					select new
					{
						X,
						Y,
						Pixel
					};
			foreach (var item in q)
				Console.WriteLine("{0}, {1} = {2}", item.X, item.Y, item.Pixel);
		}

		private static void TestDistinct()
		{
			int[] source = { 1, 2, 3, 1, 2, 3, 4, 5 };
			var q = source.Distinct();

			Console.WriteLine("Distinct example: {0}", source.Aggregate<int, string>(
																string.Empty,
																(acc, next) =>
																{
																	string nextValue = next.ToString();
																	
																	if (acc == string.Empty)
																		return nextValue;
																
																	return string.Format("{0}{1}{2}", acc, ", ", nextValue);
																}) );
			foreach (var item in q)
				Console.Write(item + " ");

			string[] names = { "one", "ONE", "One", "Two", "Two" };
			var q1 = names.Distinct(StringComparer.CurrentCultureIgnoreCase);

			Console.WriteLine("Distinct example: {0}", names.Aggregate((acc, next) => string.Format("{0}, {1}", acc, next)));

			foreach (var item in q1)
				Console.WriteLine(item + " ");

			var q2 = names.Distinct(EqualityComparer<string>.Default);

			foreach (var item in q2)
				Console.WriteLine(item + " ");
		}

		private static void TestOfType()
		{
			List<Shape> shapes = new List<Shape>
			{
				new Rectangle(),
				new Circle(),
				new Rectangle(),
				new Circle(),
				new Rectangle(),
			};
			var q1 = from rect in shapes.OfType<Rectangle>()
					select rect;
			var q2 = shapes.OfType<Circle>();
			Console.WriteLine("Number of Rectangles = {0}", q1.Count());
			Console.WriteLine("Number of Circles = {0}", q2.Count());
		}

		private static void TestAggregate()
		{
			var nums = new int[] { 1, 2, 3 };
			var sum = nums.Aggregate(0,		// seed
				(acc, i) => acc + i, // 누적 함수
				acc => acc + 100);	// 결과 선택기
			Console.WriteLine("Sum plus 100 = {0}", sum);
		}
	}
	
	public static class PagingExtensions
	{
		public static IEnumerable<T> Page<T>(this IEnumerable<T> source, int page, int pageSize)
		{
			const int maxPageSize = 100;

			pageSize = Math.Min(pageSize, maxPageSize);

			return source.Skip((page - 1) * pageSize)
				.Take(pageSize);
		}
	}
}
