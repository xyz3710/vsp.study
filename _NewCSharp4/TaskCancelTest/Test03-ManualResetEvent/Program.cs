﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// 대기 핸들이 있는 취소 요청 수신 대기: http://msdn.microsoft.com/ko-kr/library/ee191552(v=vs.110).aspx
namespace Test03_ManualResetEvent
{
	class Program
	{
		// Old-style ManualResetEvent that doesn't support unified cancellation.
		static ManualResetEvent _mre = new ManualResetEvent(false);

		static void Main(string[] args)
		{
			var cts = new CancellationTokenSource();

			// Pass the same token source to the delegate and to the task instance.
			Task.Run(() => DoWork(cts.Token), cts.Token);
			Console.WriteLine("Press 's' to start/restart, 'p' to pause, or 'c' to cancel.");
			Console.WriteLine("Or any other key to exit.");

			// Old-style UI thread
			bool goAgain = true;

			while (goAgain == true)
			{
				switch (Console.ReadKey().KeyChar)
				{
					case 'c':
						cts.Cancel();

						break;
					case 'p':
						_mre.Reset();

						break;
					case 's':
						_mre.Set();

						break;
					default:
						goAgain = false;

						break;
				}

				Thread.Sleep(100);
			}
		}

		private static void DoWork(CancellationToken token)
		{
			while (true)
			{
				// Wait on the event if it is not signaled.
				int eventThatSignaledIndex = WaitHandle.WaitAny(new WaitHandle[] { _mre, token.WaitHandle }, new TimeSpan(0, 0, 20));

				if (eventThatSignaledIndex == 1)
				{
					// Where we canceled while waiting?
					Console.WriteLine("The wait operation was canceled.");
					throw new OperationCanceledException(token);
				}
				else if (token.IsCancellationRequested == true)
				{
					// Were we canceled while running?
					Console.WriteLine("I was canceled while running.");
					token.ThrowIfCancellationRequested();
				}
				else if (eventThatSignaledIndex == WaitHandle.WaitTimeout)
				{
					Console.WriteLine("I timed out.");

					break;
				}
				else
				{
					Console.Write("Working...");
					// Simulating work.
					Thread.SpinWait(500000);
				}
			}
		}
	}
}
