﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskCancelTest
{
	class Program
	{
		static void Main(string[] args)
		{
			TaskCancel taskCancel = new TaskCancel();

			taskCancel.InitTaks();
			taskCancel.Start();
			Thread.Sleep(1000);
			taskCancel.Cancel();
			Thread.Sleep(500);

			taskCancel.InitTaks();
			taskCancel.Start();
			taskCancel.Cancel();
		}
	}

	public class TaskCancel
	{
		public TaskCancel()
		{			
		}

		public Task DetectionTask
		{
			get;
			set;
		}


		private CancellationTokenSource CancelToken
		{
			get;
			set;
		}

		public void InitTaks()
		{
			DisposeDetectionTask();

			CancelToken = new CancellationTokenSource();
			DetectionTask = new Task(DetectAction, CancelToken.Token);
		}

		public void Start()
		{
			DetectionTask.Start();
		}

		public void DisposeDetectionTask()
		{
			if (DetectionTask != null)
			{
				if (DetectionTask.IsCanceled == false)
				{
					Cancel();
				}

				while (!(DetectionTask.Status == TaskStatus.RanToCompletion ||
										DetectionTask.Status == TaskStatus.Faulted ||
										DetectionTask.Status == TaskStatus.Canceled))
				{
					Thread.Sleep(1);
				}

				DetectionTask.Dispose();
				Console.WriteLine("dispose 됨");
			}
		}

		public void Cancel()
		{
			Console.WriteLine("Before cancel.");
			CancelToken.Cancel(true);			
		}

		private void DetectAction()
		{
			while (CancelToken.IsCancellationRequested == false)
			{
				Console.WriteLine("In detect action");
				Thread.Sleep(5000);
			}

			Console.WriteLine("Canceled.");
		}
	}
}
