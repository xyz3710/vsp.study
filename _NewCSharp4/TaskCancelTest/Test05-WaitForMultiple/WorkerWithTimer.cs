﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Test05_WaitForMultiple
{
	class WorkerWithTimer
	{
		private CancellationTokenSource internalTokenSource;
		private CancellationToken internalToken;
		private CancellationToken externalToken;
		private Timer timer;

		/// <summary>
		/// WorkerWithTimer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public WorkerWithTimer()
		{
			internalTokenSource = new CancellationTokenSource();
			internalToken = internalTokenSource.Token;

			// A toy cancellation trigger that times out after 3 seconds.
			// if the user does not press 'c'.
			timer = new Timer(new TimerCallback(CancelAfterTimeout), null, 3000, 3000);
		}

		public void DoWork(CancellationToken token)
		{
			internalToken = internalTokenSource.Token;
			externalToken = token;

			using (CancellationTokenSource linkedCts = CancellationTokenSource.CreateLinkedTokenSource(internalToken, externalToken))
			{
				try
				{
					DoWorkInternal(linkedCts.Token);
				}
				catch (OperationCanceledException)
				{
					if (internalToken.IsCancellationRequested == true)
					{
						Console.WriteLine("Operation timed out.");
					}
					else if (externalToken.IsCancellationRequested == true)
					{
						Console.WriteLine("Cancelling per user request.");
						externalToken.ThrowIfCancellationRequested();
					}
				}
			}
		}

		private void DoWorkInternal(CancellationToken token)
		{
			for (int i = 0; i < 1000; i++)
			{
				if (token.IsCancellationRequested == true)
				{
					// We need to dispose the timer if cancellation was requested by the external token.
					timer.Dispose();
					// Throw the exception.
					token.ThrowIfCancellationRequested();
				}

				// Simulating work.
				Thread.SpinWait(7500000);
				Console.WriteLine("Working...");
			}
		}

		public void CancelAfterTimeout(object state)
		{
			Console.WriteLine("\r\nTimer fired.");
			internalTokenSource.Cancel();
			timer.Dispose();
		}
	}
}
