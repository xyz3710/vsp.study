﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// 여러 개의 취소 요청 수신 대기: http://msdn.microsoft.com/ko-kr/library/ee191553(v=vs.110).aspx
namespace Test05_WaitForMultiple
{
	class Program
	{
		static void Main(string[] args)
		{
			WorkerWithTimer worker = new WorkerWithTimer();
			CancellationTokenSource cts = new CancellationTokenSource();
			// Task for UI thread, so we can call Task.Wait wait on the main thread.
			Task.Run(() =>
			{
				Console.WriteLine("Press 'c' to cancel with 3 seconds after work begins.");
				Console.WriteLine("or let the task time out by doing nothing.");

				if (Console.ReadKey().KeyChar == 'c')
				{
					cts.Cancel();
				}
			});

			Thread.Sleep(1000);		// Let the user read the UI message.
			// Start the worker task.
			Task task = Task.Run(() => worker.DoWork(cts.Token), cts.Token);

			try
			{
				task.Wait(cts.Token);
			}
			catch (OperationCanceledException e)
			{
				if (e.CancellationToken == cts.Token)
				{
					Console.WriteLine("Canceled from UI thread throwing OCE.");
				}
			}
			catch (AggregateException ae)
			{
				Console.WriteLine("AggregateException caught: " + ae.InnerException);

				foreach (Exception innerException in ae.InnerExceptions)
				{
					Console.WriteLine(innerException.Message + innerException.Source);
				}
			}

			Console.WriteLine("Press any key to exit.");
			Console.ReadKey();
		}
	}
}
