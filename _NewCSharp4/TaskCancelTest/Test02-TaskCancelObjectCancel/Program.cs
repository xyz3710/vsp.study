﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.Net;

// 취소 요청에 대한 콜백 등록: http://msdn.microsoft.com/ko-kr/library/dd997364.aspx
namespace Test02_TaskCancelObjectCancel
{
	class Program
	{

		static void Main(string[] args)
		{
			MultiCancel();		// 작업 취소 및 개체 취소
			CancelWithPolling();		// 취소 요청 수신 및 응답: 폴링을 통해 취소 요청 수신 대기: http://msdn.microsoft.com/ko-kr/library/ee191559(v=vs.110).aspx
			CancelWithCallback();		// 취소 요청에 대한 콜백 등록

			Console.ReadLine();
		}

		private static void CancelWithCallback()
		{
			CancellationTokenSource cts = new CancellationTokenSource();
			var task = Task.Run(() => DoWork(cts.Token));

			Console.WriteLine("Press 'c' to cancel.");

			if (Console.ReadKey().KeyChar == 'c')
			{
				cts.Cancel();
			}

			Console.WriteLine("Press any key to continue other test.");
			Console.ReadKey();
		}

		private static void DoWork(CancellationToken token)
		{
			WebClient wc = new WebClient();

			wc.DownloadStringCompleted += (senderObject, ea) =>
			{
				if (ea.Cancelled == false)
				{
					Console.WriteLine(ea.Result + "\r\nPress any key.");
				}
				else
				{
					Console.WriteLine("Download was canceled.");
				}
			};

			if (token.IsCancellationRequested == false)
			{
				using (CancellationTokenRegistration ctr = token.Register(() => wc.CancelAsync()))
				{
					Console.WriteLine("Starting request.");

					wc.DownloadStringAsync(new Uri("http://contoso.com"));
				}
			}
		}

		private static void CancelWithPolling()
		{
			CancellationTokenSource cts = new CancellationTokenSource();

			//Task.Factory.StartNew(() =>
			//{
			//	NestedLoops(cts.Token);
			//});
			Task.Run(() => NestedLoops(cts.Token));
			Console.WriteLine("Press 'c' to cancel");

			if (Console.ReadKey().KeyChar == 'c')
			{
				cts.Cancel();
				Console.WriteLine("Press any key to other test.");
			}

			Console.ReadKey();
		}

		private static void NestedLoops(CancellationToken token)
		{
			int columns = 1000;
			int rows = 500;

			for (int x = 0; x < columns && token.IsCancellationRequested == false; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					// Simulating work.
					Thread.SpinWait(5000);
					Console.WriteLine("{0}, {1}", x, y);
				}

				if (token.IsCancellationRequested == true)
				{
					// Cleanup or undo here if necessary...
					Console.WriteLine("\r\nCancelling after row {0}", x);

					break;
				}
			}
		}

		private static void MultiCancel()
		{
			CancellationTokenSource cts = new CancellationTokenSource();
			CancellationToken token = cts.Token;

			var obj1 = new MyCancelableObject();
			var obj2 = new MyCancelableObject();
			var obj3 = new MyCancelableObject();

			// Register the object's cancel method with the token's cancellation request.
			token.Register(() => obj1.Cancel());
			token.Register(() => obj2.Cancel());
			token.Register(() => obj3.Cancel());

			// Request cancellation on the token.
			cts.Cancel();
		}

	}
}
