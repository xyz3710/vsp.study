﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test02_TaskCancelObjectCancel
{
	class MyCancelableObject
	{
		internal void Cancel()
		{
			Console.WriteLine("On Canceled.");
		}
	}
}
