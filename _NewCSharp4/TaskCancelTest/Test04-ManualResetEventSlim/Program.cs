﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// 대기 핸들이 있는 취소 요청 수신 대기: http://msdn.microsoft.com/ko-kr/library/ee191552(v=vs.110).aspx
namespace Test04_ManualResetEventSlim
{
	class Program
	{
		static ManualResetEventSlim _mres = new ManualResetEventSlim(false);

		static void Main(string[] args)
		{
			var cts = new CancellationTokenSource();

			// Pass the same token  source to the delegate and to the task instance.
			Task.Run(() => DoWork(cts.Token), cts.Token);
			Console.WriteLine("Press 'c' to cancel, 'p' to pause, or 's' to start/restart,");
			Console.WriteLine("or any other key to exit.");

			// New-style UI thread
			bool goAgain = true;

			while (goAgain == true)
			{
				switch (Console.ReadKey().KeyChar)
				{
					case 'c':
						cts.Cancel();		// Token can only be canceled once.

						break;
					case 'p':
						_mres.Reset();

						break;
					case 's':
						_mres.Set();

						break;
					default:
						goAgain = false;

						break;
				}

				Thread.Sleep(100);
			}
		}

		private static Task DoWork(CancellationToken token)
		{
			while (true)
			{
				if (token.IsCancellationRequested == true)
				{
					Console.WriteLine("Canceled while running.");
					token.ThrowIfCancellationRequested();
				}

				// Wait on the event to be signaled or the token to be canceled, whichever comes first. 
				// The token will throw an exception if it is canceled while the thread is waiting on the event.
				try
				{
					_mres.Wait(token);
				}
				catch (OperationCanceledException)
				{
					// Throw immediately to be responsive. The alternative is to do one more item of work,
					// and throw on next iteration, because IsCancellationRequested will be true.
					Console.WriteLine("The wait operation was canceled.");
					throw;
				}

				Console.WriteLine("Working...");
				// Simulating work.
				Thread.SpinWait(500000);
			}
		}
	}
}
