﻿/**********************************************************************************************************************/
/*	Domain		:	_02_CSharp4Style.Animal
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 3월 21일 월요일 오후 4:06
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	Covariance, Contravariance C# 3.0 style
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02_CSharp4Style
{
	class Animal
	{
		public bool Hungry
		{
			get;
			set;
		}

		public override string ToString()
		{
			return "Animal";
		}
	}

	class Mammal : Animal
	{
		public override string ToString()
		{
			return "Mammal";
		}
	}

	class Giraffe : Mammal
	{
		public override string ToString()
		{
			return "Giraffe";
		}
	}

	class Tiger : Mammal
	{
		public override string ToString()
		{
			return "Tiger";
		}
	}

	class Program
	{
		void FeedAnimals(IEnumerable<Animal> animals)
		{
			foreach (Animal animal in animals)
			{
				if (animal.Hungry)
				{
					Feed(animal);
				}
			}
		}

		private void Feed(Animal animal)
		{
			Console.WriteLine(animal.ToString());
		}

		static void Main(string[] args)
		{
			List<Giraffe> giraffes = new List<Giraffe>{
                 new Giraffe{ Hungry = true},
                 new Giraffe{Hungry = true},
                 new Giraffe{Hungry = false}
             };

			Program program = new Program();
			program.FeedAnimals(giraffes);
		}
	}
}
