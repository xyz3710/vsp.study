﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dynamic01_Basic
{
	class Program
	{
		static void Main(string[] args)
		{
			dynamic d = 7;
			int i = d;

			Console.WriteLine("i : {0}", i);

			var people = new
			{
				Name = "김기원",
				Age = 38
			};
			
			PeopleInfo(people);
		}

		private static void PeopleInfo(dynamic people)
		{
			Console.WriteLine("이름 : {0}, 나이 : {1}", people.Name, people.Age);
		}
	}
}
