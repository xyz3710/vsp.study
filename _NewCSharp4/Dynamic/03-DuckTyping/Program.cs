﻿/**********************************************************************************************************************/
/*	Domain		:	Dynamic03_DuckTyping.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 3월 21일 월요일 오후 1:12
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://cafe.naver.com/jobstartgogo.cafe?iframe_url=/ArticleRead.nhn%3Farticleid=7413& 중
 *					http://vsts2010.net/20
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dynamic03_DuckTyping
{
	public class Duck
	{
		public string Quack()
		{
			return "Quack!";
		}

		public string Swim()
		{
			return "Paddle paddle paddle...";
		}
	}

	public class Goose
	{
		public string Honk()
		{
			return "Honk!";
		}

		public string Swim()
		{
			return "Splash Splash Splash";
		}
	}

	public class DuckRecording
	{
		public string Quack()
		{
			return Play();
		}

		public string Play()
		{
			return "Quack!!";
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine(MakeItQuack(new Duck()));
			PrintBar();
			Console.WriteLine(MakeItQuack(new DuckRecording()));
			PrintBar();
			Console.WriteLine(MakeItSwim(new Duck()));
			PrintBar();
			Console.WriteLine(MakeItSwim(new Goose()));
		}

		private static void PrintBar()
		{
			Console.WriteLine(string.Empty.PadRight(60, '*'));
		}

		private static string MakeItQuack(dynamic duck)
		{
			return duck.Quack();
		}

		private static string MakeItSwim(dynamic duck)
		{
			return duck.Swim();
		}
	}
}
