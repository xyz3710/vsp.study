﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dynamic02_OptionalAndNamedParameter
{
	class Program
	{
		static void Main(string[] args)
		{
			Method(1, 2, 3);
			Method(1, 2);
			Method(1);
			Method(1, z: 3);
			Method(x: 1, z: 3);
			Method(z: 3, x: 1);
		}

		/// <summary>
		/// 테스트 method
		/// </summary>
		/// <param name="x">첫번째 파라미터</param>
		/// <param name="y">두번째 파라미터</param>
		/// <param name="z">세번째 파라미터</param>
		private static void Method(int x, int y = 5, int z = 7)
		{
			Console.WriteLine("x : {0}, y : {1}, z : {2}", x, y, z);
		}
	}
}
