﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _04_DynamicReturnMethod
{
	class Program
	{
		static void Main(string[] args)
		{
			int result = SettingNames.Stats_Dashboard_TopSalesCountByProduct.GetValue();

			Console.WriteLine(result);
		}
	}
}
