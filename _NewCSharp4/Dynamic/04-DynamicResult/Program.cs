﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using System.ComponentModel;

namespace _04_DynamicResult
{
	class Program
	{
		static void Main(string[] args)
		{
			int defaultValue1 = SettingNames.Account_MaxLoginFailCount.GetValue();
			string defaultValue2 = SettingNames.Membership_Password.GetValue();
			string defaultValue3 = SettingNames.Package_ImageRoot.GetValue();
			int? defaultValue4 = SettingNames.Package_ImageRoot.GetValue();
		}
	}

	/// <summary>
	/// SettingName에 관련된 확장 기능을 제공합니다.
	/// </summary>
	public static class SettingNamesExtension
	{
		/// <summary>
		/// SettingNames에서 값을 구합니다.
		/// </summary>
		/// <param name="settingNames"></param>
		/// <returns></returns>
		public static dynamic GetValue(this SettingNames settingNames)
		{
			Type type = typeof(SettingNames);
			var ca = type.GetField(settingNames.ToString())
				.GetCustomAttributes(typeof(DefaultValueAttribute), false)
				.Cast<DefaultValueAttribute>()
				.SingleOrDefault();
			//TypeConverter typeConverter = new TypeConverter();
			//TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(settingNames.ToString());
			
			if (ca != null)
				return ca.Value;

			return default(dynamic);
		}
	}

	public class DynamicValue : DynamicObject
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="binder"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public override bool TryConvert(ConvertBinder binder, out object result)
		{
			return base.TryConvert(binder, out result);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="binder"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			return base.TryGetMember(binder, out result);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="binder"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override bool TrySetMember(SetMemberBinder binder, object value)
		{
			return base.TrySetMember(binder, value);
		}
	}
}
