using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace _04_DynamicResult
{
	/// <summary>
	/// Setting 테이블의 Key를 제공합니다.
	/// </summary>
	public enum SettingNames
	{
		/// <summary>
		/// Account : 최대 로그인 실패 회수
		/// </summary>
		[DefaultValue(7)]
		Account_MaxLoginFailCount,
		/// <summary>
		/// Account : 암호 초기화 토큰이 유요한 시간: 단위 분
		/// </summary>
		[DefaultValue(10)]
		Account_PasswordTokenExpirationDuring,
		/// <summary>
		/// Membership_UserId
		/// </summary>
		[DefaultValue("user")]
		Membership_UserId,
		/// <summary>
		/// Membership_Password
		/// </summary>
		[DefaultValue("theProgrammer100")]
		Membership_Password,
		/// <summary>
		/// Membership_AdminUserId
		/// </summary>
		[DefaultValue("admin")]
		Membership_AdminUserId,
		/// <summary>
		/// Membership_AdminPassword
		/// </summary>
		[DefaultValue("theProgrammer100")]
		Membership_AdminPassword,
		/// <summary>
		/// Product : 최근 수정된 패키지 보여주기에서 보여줄 항목 개수: 3의 배수로 입력해야 보기 좋음
		/// </summary>
		Product_LastModifedPackageCount,
		/// <summary>
		/// Product : 더보기 버튼을 눌렀을 때 가져올 목록 개수
		/// </summary>
		Product_MoreListCount,
		/// <summary>
		/// Package_ImageRoot
		/// </summary>
		Package_ImageRoot,
		/// <summary>
		/// Package : Search Engine Optimizer를 위해 패키지 Url에 추가될 접두어
		/// </summary>
		Package_UrlSeoPrefix,
		/// <summary>
		/// RedirectView : View를 Redirect 시키기 위한 솔루션 구성으로 Debug, Release.Test 값만 지정한다.
		/// </summary>
		RedirectView_PostfixInvokeConfiguration,
		/// <summary>
		/// RedirectView : RedirectViewPostfixInvokeConfiguration 시에 사용되는 View의 postfix (.Mobile, .Hybrid)
		/// </summary>
		RedirectView_Postfix,
		/// <summary>
		/// Email_Smtp_Server
		/// </summary>
		Email_Smtp_Server,
		/// <summary>
		/// Email_Smtp_Port
		/// </summary>
		Email_Smtp_Port,
		/// <summary>
		/// Email_Smtp_EnableSsl
		/// </summary>
		Email_Smtp_EnableSsl,
		/// <summary>
		/// Email_Smtp_UserName
		/// </summary>
		Email_Smtp_UserName,
		/// <summary>
		/// Email_Smtp_Password
		/// </summary>
		Email_Smtp_Password,
		/// <summary>
		/// Email_Smtp_UseDefaultCredentials
		/// </summary>
		Email_Smtp_UseDefaultCredentials,
		/// <summary>
		/// Email_AdminEmail
		/// </summary>
		Email_AdminEmail,
		/// <summary>
		/// Email_QuestionEmail
		/// </summary>
		Email_QuestionEmail,
		/// <summary>
		/// System : 에러 발생시 adminEmail에 등록된 이메일로 알림을 전송할지 여부
		/// </summary>
		System_ErrorNotificationEnabled,
		/// <summary>
		/// System : 인터페이스 송/수신 패킷을 로깅할지 여부
		/// </summary>
		System_HttpPackageLogEnabled,
		/// <summary>
		/// ReCaptcha_Enabled
		/// </summary>
		ReCaptcha_Enabled,
		/// <summary>
		/// ReCaptcha_Theme
		/// </summary>
		ReCaptcha_Theme,
		/// <summary>
		/// ReCaptcha_PublicKey
		/// </summary>
		ReCaptcha_PublicKey,
		/// <summary>
		/// ReCaptcha_PrivateKey
		/// </summary>
		ReCaptcha_PrivateKey,
		/// <summary>
		/// Layout_Theme_Site
		/// </summary>
		Layout_Theme_Site,
		/// <summary>
		/// Layout_Theme_Administration
		/// </summary>
		Layout_Theme_Administration,
		/// <summary>
		/// Layout_Theme_Statistics
		/// </summary>
		Layout_Theme_Statistics,
		/// <summary>
		/// Stats : Dashboard에 제한할 벤더별 판매개수
		/// </summary>
		[DefaultValue(5)]
		Stats_Dashboard_TopSalesCountByVendor,
		/// <summary>
		/// Stats : Dashboard에 제한할 제품별 판매개수
		/// </summary>
		[DefaultValue(3)]
		Stats_Dashboard_TopSalesCountByProduct,
	}
}

