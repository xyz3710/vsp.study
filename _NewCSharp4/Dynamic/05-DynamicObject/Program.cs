﻿/**********************************************************************************************************************/
/*	Domain		:	_05_DynamicObject.DynamicNumber
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 28일 월요일 오후 2:13
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://igoro.com/archive/use-c-dynamic-typing-to-conveniently-access-internals-of-an-object/
 *					http://blogs.msdn.com/b/csharpfaq/archive/2009/10/19/dynamic-in-c-4-0-creating-wrappers-with-dynamicobject.aspx
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;

namespace _05_DynamicObject
{
	// The class derived from DynamicObject.
	public class DynamicNumber : DynamicObject
	{
		// The inner dictionary.
		Dictionary<string, object> _dictionary = new Dictionary<string, object>();

		// Getting a property.
		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			return _dictionary.TryGetValue(binder.Name, out result);
		}

		// Setting a property.
		public override bool TrySetMember(SetMemberBinder binder, object value)
		{
			_dictionary[binder.Name] = value;

			return true;
		}

		// Converting an object to a specified type.
		public override bool TryConvert(ConvertBinder binder, out object result)
		{
			// Converting to string. 
			if (binder.Type == typeof(String))
			{
				result = _dictionary["Textual"];
				return true;
			}

			// Converting to integer.
			if (binder.Type == typeof(int))
			{
				result = _dictionary["Numeric"];
				return true;
			}

			if (binder.Type == typeof(double))
			{
				result = _dictionary["Double"];

				return true;
			}

			// In case of any other type, the binder 
			// attempts to perform the conversion itself.
			// In most cases, a run-time exception is thrown.
			return base.TryConvert(binder, out result);
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			// Creating the first dynamic number.
			dynamic number = new DynamicNumber();

			// Creating properties and setting their values
			// for the dynamic number.
			// The TrySetMember method is called.
			number.Textual = "One";
			number.Numeric = 1;
			number.Double = 2.0d;

			// Implicit conversion to integer.
			int testImplicit = number;

			// Explicit conversion to string.
			string testExplicit = (String)number;

			Console.WriteLine(testImplicit);
			Console.WriteLine(testExplicit);

			// The following statement produces a run-time exception
			// because the conversion to double is not implemented.
			double test = (double)number;

			Console.WriteLine(test);
		}
	}
}
