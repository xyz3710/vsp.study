﻿/**********************************************************************************************************************/
/*	Domain		:	Exam01_Factory.Program
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 5일 수요일 오후 1:11
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://msdn.microsoft.com/ko-kr/library/dd537609(v=vs.100).aspx
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Exam01_Factory
{
	class Program
	{
		static void Main(string[] args)
		{
			#region 명시적 작업 만들기 - 작업 만들기와 예약을 분리할 필요가 있을 때
			// Create a task and supply a user delegate by using a lambda expression.
			var taskA = new Task(() => Console.WriteLine("Hello from taskA."));

			// Start the task.
			taskA.Start();

			// Output a message from the joining thread.
			Console.WriteLine("Hello from the calling thread in TaskA.");

			taskA.Wait();		// 생략할 경우 taskA가 실행이 안되는 경우도 있다.
			#endregion

			#region 명시적 작업 만들기 - 작업 만들기와 예약을 분리할 필요가 없을 때
			// Create and start the task in one operation.
			var taskB = Task.Factory.StartNew(() => Console.WriteLine("Hello from taskB."));

			// Output a message from the joining thread.
			Console.WriteLine("Hello from the calling thread in TaskB.");

			taskB.Wait();
			#endregion

			#region Task에서 값 반환 1
			Task<double>[] taskArray = new Task<double>[]
			{
				Task<double>.Factory.StartNew(() => DoComputation1()),
				// May be written more conveniently like this:
				Task.Factory.StartNew(() => DoComputation2()),
				Task.Factory.StartNew(() => DoComputation3())                
			};

			#region 작업 연속 만들기
			taskArray[0].ContinueWith(x =>
			{
				double cw = x.Result * -1d;

				Console.WriteLine("작업 연속 만들기: {0}", cw);
			});
			#endregion

			double[] results = new double[taskArray.Length];

			for (int i = 0; i < taskArray.Length; i++)
				results[i] = taskArray[i].Result;
			
			foreach (var item in results)
				Console.WriteLine(item);

			//foreach (var item in taskArray)
			//	item.Wait();
			#endregion

			#region PLINQ 및 TPL의 람다 식
			// http://msdn.microsoft.com/ko-kr/library/dd460699(v=vs.100).aspx
			Console.WriteLine("\r\nPLINQ 및 TPL의 람다 식");
			int[] input = { 4, 1, 6, 2, 9, 5, 10, 3 };
			int sum = 0;

			try
			{
				Parallel.ForEach(
						input,					        // source collection
						() => 0,					        // thread local initializer
						(n, loopState, localSum) =>		// body
						{
							localSum += n;
							Console.WriteLine("Thread={0}, n={1}, localSum={2}", Thread.CurrentThread.ManagedThreadId, n, localSum);
							return localSum;
						},
						(localSum) => Interlocked.Add(ref sum, localSum)					// thread local aggregator
					);

				Console.WriteLine("\nSum={0}", sum);
			}
			// No exception is expected in this example, but if one is still thrown from a task,
			// it will be wrapped in AggregateException and propagated to the main thread.
			catch (AggregateException e)
			{
				Console.WriteLine("Parallel.ForEach has thrown an exception. THIS WAS NOT EXPECTED.\n{0}", e);
			}
			#endregion

			#region Task에서 값 반환
			// http://msdn.microsoft.com/ko-kr/library/dd537613(v=vs.100).aspx
			Console.WriteLine("\r\nTask에서 값 반환");
			// Return a value type with a lambda expression
			Task<int> task1 = Task<int>.Factory.StartNew(() => 1);
			int r = task1.Result;

			// Return a named reference type with a multi-line statement lambda.
			Task<Test> task2 = Task<Test>.Factory.StartNew(() =>
			{
				string s = ".NET";
				double d = 4.0;
				return new Test
				{
					Name = s,
					Number = d
				};
			});
			Test test = task2.Result;

			// Return an array produced by a PLINQ query
			Task<string[]> task3 = Task<string[]>.Factory.StartNew(() =>
			{
				string path = @"D:\Personal\_Library\Picture\";
				string[] files = System.IO.Directory.GetFiles(path);

				var result = (from file in files.AsParallel()
							  let info = new System.IO.FileInfo(file)
							  where info.Extension == ".jpg"
							  select file).ToArray();

				return result;
			});

			foreach (var name in task3.Result)
				Console.WriteLine(name);
			#endregion

			#region 생성자를 통한 상태객체 반환
			Console.WriteLine("\r\n생성자를 통한 상태객체 반환");
			// Create the task object by using an Action(Of Object) to pass in custom data
			// in the Task constructor. This is useful when you need to capture outer variables
			// from within a loop. As an experiement, try modifying this code to 
			// capture i directly in the lambda, and compare results.
			Task[] taskArray2 = new Task[10];
			
			for (int i = 0; i < taskArray2.Length; i++)
			{
				taskArray2[i] = new Task((obj) =>
				{
					MyCustomData mydata = (MyCustomData)obj;
					mydata.ThreadNum = Thread.CurrentThread.ManagedThreadId;
					Console.WriteLine("Hello from Task #{0} created at {1} running on thread #{2}.",
										mydata.Name, mydata.CreationTime, mydata.ThreadNum);
				},
				new MyCustomData()
				{
					Name = i,
					CreationTime = DateTime.Now.Ticks
				}
				);

				taskArray2[i].Start();
			}
			#endregion

			#region 분리된 상태의 중첩된 작업 만들기
			var outer1 = Task.Factory.StartNew(() =>
			{
				Console.WriteLine("Outer1 task beginning.");

				var child = Task.Factory.StartNew(() =>
				{
					Thread.SpinWait(5000000);
					Console.WriteLine("Detached task completed.");
				});

				// 외부 작업은 중첩된 작업이 완료될 때까지 대기하지 않는다.
				//child.Wait();
			});

			outer1.Wait();
			Console.WriteLine("Outer1 task completed.");
			#endregion

			#region 자식 작업 만들기
			var parent = Task.Factory.StartNew(() =>
			{
				Console.WriteLine("Parent task beginning.");

				var child = Task.Factory.StartNew(() =>
				{
					Thread.SpinWait(5000000);
					Console.WriteLine("Detached child task completed.");
				}, TaskCreationOptions.AttachedToParent);
				// TaskCreationOptions.AttachedToParent 사용시 암시적으로 모든 자식 작업이 완료될 때까지 대기한다.
			});

			parent.Wait();
			Console.WriteLine("Parent task completed.");

			#endregion

			Console.WriteLine("Enter Key 대기중");
			Console.ReadLine();
		}

		private static double DoComputation1()
		{
			double result = 0d;

			for (int i = 0; i < 100; i++)
				result += i * i;

			return result;
		}

		private static double DoComputation2()
		{
			double result = 0d;

			for (int i = 100; i < 200; i++)
				result += i * i;

			return result;
		}

		private static double DoComputation3()
		{
			double result = 0d;

			for (int i = 200; i < 300; i++)
				result += i * i;

			return result;
		}
	}

}
