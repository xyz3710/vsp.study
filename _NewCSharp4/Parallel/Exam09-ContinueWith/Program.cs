﻿using System;
using System.Threading.Tasks;

namespace Exam09_ContinueWith
{
	class Program
	{
		static string Calc(object from)
		{
			long sum = 0;
			long start = (long)from;
			Console.WriteLine("현재 이 메서드를 실행중인 스레드 ID : {0}",
				Task.CurrentId);

			for (long i = start; i < 100000000; i++)
			{
				sum += i;
				if (i == 100000 || i == 200000)
				{
					//100000에 이르면, 그냥 실행을 종료시킨다.
					throw new ApplicationException("그냥 에러가 났음");
				}
			}
			Console.WriteLine("계산 끝");
			return sum.ToString();
		}

		static void Main(string[] args)
		{
			Task<string> task = new Task<string>(
				Calc, 1L);

			task.Start();

			Task<string> justDoIt = task.ContinueWith<string>(
				 (antecedentTask) =>
				 {
					 Console.WriteLine("이전 작업 상태 : {0}", antecedentTask.Status);
					 return Calc(100001L);
				 },
				 TaskContinuationOptions.None);

			Task<string> justDoIt2 = justDoIt.ContinueWith<string>(
				 (antecedentTask) =>
				 {
					 Console.WriteLine("이전 작업 상태 : {0}", antecedentTask.Status);
					 return Calc(200001L);
				 },
				 TaskContinuationOptions.None);

			try
			{
				Console.WriteLine(task.Result);
			}
			catch (AggregateException ex)
			{
				foreach (var item in ex.InnerExceptions)
				{
					Console.WriteLine("에러 : {0}", item.Message);
				}
			}
			finally
			{
				try
				{
					Console.WriteLine(justDoIt.Result);
				}
				catch (AggregateException ex)
				{
					foreach (var item in ex.InnerExceptions)
					{
						Console.WriteLine("에러 : {0}", item.Message);
					}
				}
				finally
				{
					Console.WriteLine(justDoIt2.Result);
					Console.WriteLine("끝");
				}
			}
		}
	}
}
