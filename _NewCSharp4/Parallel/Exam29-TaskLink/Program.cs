﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exam29_TaskLink
{
	class Program
	{
		static void Main(string[] args)
		{
			TaskCancellationTest();
			TaskLinkTest1();
			TaskLinkTest2();
		}

		private static void TaskLinkTest2()
		{
			Task<int[]> parent = new Task<int[]>(() =>
			{
				var results = new int[3];
				new Task(() => results[0] = Sum(10), TaskCreationOptions.AttachedToParent).Start();
				new Task(() => results[1] = Sum(20), TaskCreationOptions.AttachedToParent).Start();
				new Task(() => results[2] = Sum(30), TaskCreationOptions.AttachedToParent).Start();

				return results;
			});

			var cwt = parent.ContinueWith(
				parentTask => Array.ForEach(parentTask.Result, Console.WriteLine));

			parent.Start();
			Wait();
		}

		private static int Sum(int n)
		{
			var sum = 0;

			for (int i = 1; i <= n; i++)
			{
				sum += i;
			}

			return sum;
		}

		private static void Wait()
		{
			Console.WriteLine("아무키나 누르면 계속 합니다.");
			Console.ReadLine();
		}

		private static void TaskLinkTest1()
		{
			// TaskContinuationOptions
			// OnlyOnCanceled, OnlyOnFaulted, OnlyOnRantoCompletion,
			CancellationTokenSource cts = new CancellationTokenSource();
			//cts.Cancel();

			Task<int> t = Task.Run(() => Sum(cts.Token, 100), cts.Token);

			t.ContinueWith(task =>
				Console.WriteLine("The sum is: {0}", task.Result),
				TaskContinuationOptions.OnlyOnRanToCompletion);

			t.ContinueWith(taks =>
				Console.WriteLine(" Sum threw: \r\n\t{0}", taks.Exception.InnerException),
				TaskContinuationOptions.OnlyOnFaulted);

			t.ContinueWith(taks =>
				Console.WriteLine(" Sum was canceled."),
				TaskContinuationOptions.OnlyOnCanceled);

			Wait();
		}

		private static void TaskCancellationTest()
		{
			CancellationTokenSource cts = new CancellationTokenSource();
			Task<int> t = Task.Run(() => Sum(cts.Token, 1000000), cts.Token);

			cts.Cancel();

			try
			{
				Console.WriteLine("The sum is: " + t.Result);
			}
			catch (AggregateException ex)
			{
				ex.Handle(e => e is OperationCanceledException);
				Console.WriteLine(" Sum was canceled.");
			}

			Wait();
		}

		private static int Sum(CancellationToken ct, int n)
		{
			int sum = 0;

			for (; n > 0; n--)
			{
				//if (n == 5)
				//{
				//	throw new InvalidOperationException("에러네");
				//}

				ct.ThrowIfCancellationRequested();

				checked
				{
					sum += n;
				}
			}

			return sum;
		}
	}
}
