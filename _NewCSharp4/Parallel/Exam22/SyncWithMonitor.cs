﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Exam22
{
	/// <summary>
	/// Monitor를 이용한 thread 동기화
	/// http://vsts2010.net/662
	/// </summary>
	class SyncWithMonitor
	{
		readonly static object sync = new object();
		static readonly int count = 10000000;
		static int sum = 0;

		static void IncreaseByOne()
		{
			for (int i = 0; i < count; i++)
			{
				bool locked = false;
				Monitor.Enter(sync, ref locked);

				try
				{
					sum += 1;
				}
				finally
				{
					if (locked)
					{
						Monitor.Exit(sync);
					}
				}
			}
		}

		static void Main23(string[] args)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			Task task = Task.Factory.StartNew(IncreaseByOne);

			for (int i = 0; i < count; i++)
			{
				bool locked = false;
				Monitor.Enter(sync, ref locked);

				try
				{
					sum -= 1;
				}
				finally
				{
					if (locked)
					{
						Monitor.Exit(sync);
					}
				}
			}
			task.Wait();

			stopwatch.Stop();
			Console.WriteLine("Result = {0}, {1}", sum, stopwatch.ElapsedMilliseconds);
		}
	}
}