﻿using System;
using System.Threading.Tasks;

namespace Exam22
{
	class Program22
	{
		static readonly int count = 10000000;
		static int sum = 0;

		static void IncreaseByOne()
		{
			for (int i = 0; i < count; i++)
			{
				sum += 1;
			}
		}

		static void Main22(string[] args)
		{
			Task task = Task.Factory.StartNew(IncreaseByOne);

			for (int i = 0; i < count; i++)
			{
				sum -= 1;
			}

			task.Wait();
			Console.WriteLine("Result = {0}", sum);
		}
	}
}