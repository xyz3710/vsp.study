﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cancel
{
	class Program
	{
		static void Main(string[] args)
		{
			//CancelBasicExample();
			//CancelWithChindren();			

			CancellationTokenSource cts = new CancellationTokenSource();
			// 하위 호환도 됨
			ThreadPool.QueueUserWorkItem(new WaitCallback(DoLongRunWork), cts.Token);

			Thread.Sleep(1000);
			cts.Cancel();
		}

		private static void CancelWithChindren()
		{
			// 작업 및 해당 자식 취소
			// http://msdn.microsoft.com/ko-kr/library/dd537607(v=vs.100).aspx
			Console.WriteLine("Press any key to start. Press 'c' to cancel.");
			Console.ReadKey();

			var tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;

			// Store references to the tasks so that we can wait on them and
			// observe their status after cancellation.
			Task[] tasks = new Task[10];

			// Request cancellation of a single task when the token source is canceled.
			// Pass the token to the user delegate, and also to the task so it can 
			// handle the exception correctly.
			tasks[0] = Task.Factory.StartNew(() => DoSomeWork(1, token), token);

			// Request cancellation of a task and its children. Note the token is passed
			// to (1) the user delegate and (2) as the second argument to StartNew, so 
			// that the task instance can correctly handle the OperationCanceledException.
			tasks[1] = Task.Factory.StartNew(() =>
			{
				// Create some cancelable child tasks.
				for (int i = 2; i < 10; i++)
				{
					// For each child task, pass the same token
					// to each user delegate and to StartNew.
					tasks[i] = Task.Factory.StartNew(iteration =>
								DoSomeWork((int)iteration, token), i, token);
				}
				// Passing the same token again to do work on the parent task. 
				// All will be signaled by the call to tokenSource.Cancel below.
				DoSomeWork(2, token);
			}, token);

			// Give the tasks a second to start.
			Thread.Sleep(1000);

			// Request cancellation from the UI thread.
			if (Console.ReadKey().KeyChar == 'c')
			{
				tokenSource.Cancel();
				Console.WriteLine("\nTask cancellation requested.");

				// Optional: Observe the change in the Status property on the task.
				// It is not necessary to wait on tasks that have canceled. However,
				// if you do wait, you must enclose the call in a try-catch block to
				// catch the OperationCanceledExceptions that are thrown. If you do 
				// not wait, no OCE is thrown if the token that was passed to the 
				// StartNew method is the same token that requested the cancellation.

				#region Optional_WaitOnTasksToComplete
				try
				{
					Task.WaitAll(tasks);
				}
				catch (AggregateException e)
				{
					// For demonstration purposes, show the OCE message.
					foreach (var v in e.InnerExceptions)
						Console.WriteLine("msg: " + v.Message);
				}

				// Prove that the tasks are now all in a canceled state.
				for (int i = 0; i < tasks.Length; i++)
					Console.WriteLine("task[{0}] status is now {1}", i, tasks[i].Status);
				#endregion
			}

			// Keep the console window open while the
			// task completes its output.
			Console.ReadLine();
		}

		private static void DoLongRunWork(object obj)
		{
			CancellationToken token = (CancellationToken)obj;

			for (int i = 0; i < 10000000; i++)
			{
				Thread.SpinWait(5000000);
				Console.WriteLine(i);

				if (token.IsCancellationRequested)
					// 작업 종료
					break;
			}
		}

		private static void CancelBasicExample()
		{
			// 작업 취소
			// http://msdn.microsoft.com/ko-kr/library/dd997396(v=vs.100).aspx
			var tokenSource2 = new CancellationTokenSource();
			CancellationToken ct = tokenSource2.Token;

			var task = Task.Factory.StartNew(() =>
			{

				// Were we already canceled?
				ct.ThrowIfCancellationRequested();

				bool moreToDo = true;
				while (moreToDo)
				{
					Console.WriteLine(DateTime.Now.ToString("mm:ss:fff"));
					// Poll on this property if you have to do
					// other cleanup before throwing.
					if (ct.IsCancellationRequested)
					{
						// Clean up here, then...
						ct.ThrowIfCancellationRequested();
					}

				}
			}, tokenSource2.Token); // Pass same token to StartNew.

			Thread.Sleep(3000);
			tokenSource2.Cancel();

			// Just continue on this thread, or Wait/WaitAll with try-catch:
			try
			{
				task.Wait();
			}
			catch (AggregateException e)
			{
				foreach (var v in e.InnerExceptions)
					Console.WriteLine(e.Message + " " + v.Message);
			}

			Console.ReadKey();
		}

		static void DoSomeWork(int taskNum, CancellationToken ct)
		{
			// Was cancellation already requested?
			if (ct.IsCancellationRequested)
			{
				Console.WriteLine("We were cancelled before we got started.");
				Console.WriteLine("Press Enter to quit.");
				ct.ThrowIfCancellationRequested();
			}
			int maxIterations = 1000;

			// NOTE!!! A benign "OperationCanceledException was unhandled
			// by user code" error might be raised here. Press F5 to continue. Or,
			//  to avoid the error, uncheck the "Enable Just My Code"
			// option under Tools > Options > Debugging.
			for (int i = 0; i < maxIterations; i++)
			{
				// Do a bit of work. Not too much.
				var sw = new SpinWait();
				for (int j = 0; j < 3000; j++)
					sw.SpinOnce();
				Console.WriteLine("...{0} ", taskNum);
				if (ct.IsCancellationRequested)
				{
					Console.WriteLine("bye from {0}.", taskNum);
					Console.WriteLine("\nPress Enter to quit.");

					ct.ThrowIfCancellationRequested();
				}
			}
		}
	}
}
