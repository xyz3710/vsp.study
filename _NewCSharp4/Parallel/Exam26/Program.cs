﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam26
{
	class Program
	{
		static IEnumerable<long> GetPrimeNumber(long num)
		{
			List<long> primeList = new List<long>();

			Parallel.For(0, num + 1, (i) =>
			{
				bool isPrime = true;
				Parallel.For(2, i, (j, loopState) =>
				{
					if (i % j == 0)
					{
						isPrime = false;
						loopState.Break();
					}
				});
				if (isPrime)
				{
					primeList.Add(i);
				}
			});
			return primeList;
		}

		static IEnumerable<long> GetPrimeNumber2(long num)
		{
			ConcurrentBag<long> primeList = new ConcurrentBag<long>();

			Parallel.For(0, num + 1, (i) =>
			{
				bool isPrime = true;

				Parallel.For(2, i, (j, loopState) =>
				{
					if (i % j == 0)
					{
						isPrime = false;

						loopState.Break();
					}
				});

				if (isPrime)
				{
					primeList.Add(i);
				}
			});

			return primeList;
		}

		static readonly object _sync = new object();

		static IEnumerable<long> GetPrimeNumber3(long num)
		{
			List<long> primeList = new List<long>();

			Parallel.For<List<long>>(0, num + 1,
				() => new List<long>(), //스레드 로컬 변수 초기화
				(i, outerLoopState, subList) =>
				{
					bool isPrime = true;
					Parallel.For(2, i, (j, loopState) =>
					{
						if (i % j == 0)
						{
							isPrime = false;
							loopState.Break();
						}
					});

					if (isPrime)
					{
						subList.Add(i); //스레드 로컬 리스트에 추가
					}

					return subList;
				},
			(subList) => //각 스레드 종료 후에 취합
			{
				lock (_sync)
				{
					primeList.AddRange(subList);
				}
			});

			return primeList;
		}


		static void Main(string[] args)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();

			IEnumerable<long> primeList1 = GetPrimeNumber(99999);
			sw.Stop();
			Console.WriteLine("List Elapsed : {0}, Found prime counts : {1}", sw.Elapsed.ToString(), primeList1.Count());		//	뭔가 한다.		

			sw.Restart();
			IEnumerable<long> primeList2 = GetPrimeNumber2(99999);
			sw.Stop();
			Console.WriteLine("ConcurrentBag Elapsed : {0}, Found prime counts : {1}", sw.Elapsed.ToString(), primeList2.Count());		//	뭔가 한다.		

			sw.Restart();
			IEnumerable<long> primeList3 = GetPrimeNumber3(99999);
			sw.Stop();
			Console.WriteLine("SubList Elapsed : {0}, Found prime counts : {1}", sw.Elapsed.ToString(), primeList3.Count());		//	뭔가 한다.		
		}
	}
}