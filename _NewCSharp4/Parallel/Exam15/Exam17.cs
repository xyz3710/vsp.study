﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Exam15
{
	class Program
	{
		public static int[] SimpleParallelTask(int[] source,
			CancellationToken token)
		{
			Func<int, int> square = (num) =>
			{
				Console.WriteLine(Task.CurrentId);
				Thread.Sleep(10);
				return num * num;
			};

			return source.AsParallel()
				 .WithCancellation(token)
				 .WithDegreeOfParallelism(3)
				 .Select(square)
				 .ToArray();
		}

		static void Main(string[] args)
		{
			CancellationTokenSource cts =
				new CancellationTokenSource();

			Console.WriteLine("끝내려면 아무키나 누르세요");

			int[] nums = Enumerable.Range(1, 10000).ToArray();

			Task task = Task.Factory.StartNew(() =>
			{
				nums = SimpleParallelTask(nums, cts.Token);
			}, cts.Token);

			Console.Read();

			cts.Cancel();
			Console.WriteLine("-------------------------------------");

			try
			{
				task.Wait();
			}
			catch (AggregateException)
			{
				Console.WriteLine("쿼리가 중간에 취소되었습니다.");
			}
		}
	}
}
