﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Exam15
{
	class Program15
	{
		static void Main15(string[] args)
		{
			int[] nums = Enumerable.Range(1, 20).ToArray();

			Func<int, int> square = (num) =>
			{
				Console.WriteLine(Task.CurrentId);
				Thread.Sleep(10);

				return num * num;
			};

			nums = nums.AsParallel()
				.WithDegreeOfParallelism(4)
				.WithExecutionMode(ParallelExecutionMode.ForceParallelism)
				.Select(square)
				.ToArray();
		}
	}
}
