﻿/**********************************************************************************************************************/
/*	Domain		:	Exam05.Program
/*	Creator		:	KIM-KIWON\xyz37(김기원)
/*	Create		:	2012년 9월 7일 금요일 오전 9:53
/*	Purpose		:	http://vsts2010.net/322
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Exam05
{
	class Program
	{
		static string Calc(object from)
		{
			long sum = 0;
			long start = (long)from;
			Console.WriteLine("현재 이 메서드를 실행중인 스레드 ID : {0}", Task.CurrentId);

			for (long i = start; i < 100000000; i++)
			{
				sum += i;
				if (i == 100000 || i == 200000)
				{
					//100000에 이르면, 그냥 실행을 종료시킨다.
					throw new ApplicationException("그냥 에러가 났음");
				}
			}
			Console.WriteLine("계산 끝");

			return sum.ToString();
		}

		static void Main(string[] args)
		{
			Task<string> task = new Task<string>(Calc, 1L);

			task.Start();

			Task<string> justDoIt = task.ContinueWith<string>((antecedentTask) =>
			{
				Console.WriteLine("이전 작업 상태2 : {0}", antecedentTask.Status);
				Console.WriteLine("Task State: 무사 완료!!");

				return Calc(100001L);
			},
			TaskContinuationOptions.OnlyOnRanToCompletion);

			Task<string> justDoIt2 = justDoIt.ContinueWith<string>((antecedentTask) =>
			{
				Console.WriteLine("이전 작업 상태2 : {0}", antecedentTask.Status);
				Console.WriteLine("Task State: 처리되지 않은 예외 발견");

				return Calc(200001L);
			}, 
			TaskContinuationOptions.NotOnFaulted);
			
			try
			{
				Console.WriteLine("작업 결과 : {0}", task.Result);
			}
			catch (AggregateException ex)
			{
				foreach (var item in ex.InnerExceptions)
				{
					Console.WriteLine("에러 : {0}", item.Message);
				}
			}
			finally
			{
				try
				{
					Console.WriteLine(justDoIt);
				}
				catch (AggregateException ex)
				{
					foreach (var item in ex.InnerExceptions)
					{
						Console.WriteLine("JustDoIt1 에러 : {0}", item.Message);
					}
				}
				finally
				{
					Console.WriteLine(justDoIt2.Result);
					Console.WriteLine("끝");
				}
			}

			Console.WriteLine("\r\nComplete and Fault\r\n");

			Task completedTask = task.ContinueWith(
				 (antecedentTask) =>
				 {
					 Console.WriteLine("Task State: 무사 완료!!");
					 //작업 완료 후에 이어서 뭔가를 처리
				 },
				 TaskContinuationOptions.OnlyOnRanToCompletion);

			Task faultedTask = task.ContinueWith(
				 (antecedentTask) =>
				 {
					 Console.WriteLine("Task State: 처리되지 않은 예외 발견!!");
				 },
				 TaskContinuationOptions.OnlyOnFaulted);

			try
			{
				completedTask.Wait();
				faultedTask.Wait();
			}
			catch (AggregateException ex)
			{
				foreach (var item in ex.InnerExceptions)
				{
					Console.WriteLine("에러 : {0}", item.Message);
				}
			}
		}
	}
}
