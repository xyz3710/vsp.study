﻿using System;
using System.Threading.Tasks;
using System.Threading;

namespace Exam11
{
	class Program
	{
		static void PrintDash(CancellationToken cancellationToken)
		{
			cancellationToken.Register(Canceled);

			while (!cancellationToken.IsCancellationRequested)
			{
				Console.Write("-");
			}

			//if (cancellationToken.IsCancellationRequested)
			//{
			//	cancellationToken.ThrowIfCancellationRequested();
			//}
		}

		static void Canceled()
		{
			Console.WriteLine("작업이 취소되었군요!!");
		}

		static void Main(string[] args)
		{
			string stars = "*".PadRight(Console.WindowWidth - 1, '*');

			CancellationTokenSource cancellationTokenSource =
				 new CancellationTokenSource();

			Task task = Task.Factory.StartNew(
				 () => PrintDash(cancellationTokenSource.Token));

			Task canceledTask = task.ContinueWith(
				 (antecedentTask) => Console.WriteLine("작업이 취소되었군요!!!"),
				 TaskContinuationOptions.OnlyOnCanceled);

			Console.ReadLine();

			cancellationTokenSource.Cancel();
			Console.WriteLine(stars);
			task.Wait();
			Console.WriteLine("작업의 완료상태 : {0}", task.Status);
			Console.WriteLine();
		}
	}
}
