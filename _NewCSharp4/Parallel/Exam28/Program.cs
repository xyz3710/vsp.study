﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Exam20
{
	class Program
	{
		static void RangeParallelForEach(int[] nums)
		{
			List<int> threadIds = new List<int>();
			var part = Partitioner.Create(0, nums.Length);
			Parallel.ForEach(part, (num, state) =>
			{
				if (!threadIds.Contains(Task.CurrentId.Value))
				{
					threadIds.Add(Task.CurrentId.Value);
				}
				for (int i = num.Item1; i < num.Item2; i++)
				{
					nums[i] = nums[i] * nums[i];
				}
			});
			Console.WriteLine("Thread ID list of RangeForEach");
			foreach (var id in threadIds)
			{
				Console.WriteLine("{0}", id.ToString());
			}
		}

		static void ParallelFor(int[] nums)
		{
			List<int> threadIds = new List<int>();
			Parallel.For(0, nums.Length, (i) =>
			{
				if (!threadIds.Contains(Task.CurrentId.Value))
				{
					threadIds.Add(Task.CurrentId.Value);
				}
				nums[i] = nums[i] * nums[i];
			});
			Console.WriteLine("Thread ID list of ParallelFor");
			foreach (var id in threadIds)
			{
				Console.WriteLine("{0}", id.ToString());
			}
		}

		static void Main28(string[] args)
		{
			int max = 50000000;
			int[] nums1 = Enumerable.Range(1, max).ToArray<int>();
			int[] nums2 = Enumerable.Range(1, max).ToArray<int>();
			ParallelFor(nums1);
			RangeParallelForEach(nums2);
		}
	}
}