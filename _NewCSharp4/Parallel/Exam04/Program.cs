﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Exam04
{
	class Program
	{
		static string Calc(object from)
		{
			long sum = 0;
			long start = (long)from;
			Console.WriteLine("현재 이 메서드를 실행중인 스레드 ID : {0}",
				Task.CurrentId);

			for (long i = start; i < 100000000; i++)
			{
				sum += i;
			}
			return sum.ToString();
		}

		static void Main(string[] args)
		{
			Task<string> task = new Task<string>(Calc, 4L);

			task.Start();
			Debugger.Break();

			Console.WriteLine("추가 스레드의 ID {0}",
				task.Id);

			Console.WriteLine("추가 스레드에 제공된 상태 값 : {0}",
				 task.AsyncState.ToString());

			while (!task.IsCompleted)
			{
				if (task.Status == TaskStatus.Running)
				{
					Debugger.Break();
					Console.Write(".");
				}
			}

			Console.WriteLine("");
			//여기서 추가 스레드가 끝날 때 까지 기다린다.
			Console.WriteLine(task.Result);
			System.Diagnostics.Trace.Assert(
				task.IsCompleted);
		}
	}
}
