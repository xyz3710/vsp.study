﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Exam13
{
	class Program13
	{
		static void Main13(string[] args)
		{
			int[] nums = Enumerable.Range(1, 1000).ToArray<int>();
			Parallel.For(0, 1000, (i) =>
			{
				nums[i] = nums[i] * nums[i];
			});

			for (int i = 0; i < 10; i++)
			{
				Console.WriteLine(nums[i].ToString());
			}
		}
	}
}
