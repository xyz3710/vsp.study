﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace Exam13
{
	class Program
	{
		static void Main(string[] args)
		{
			CancellationTokenSource cts = new CancellationTokenSource();
			ParallelOptions parallelOptions = new ParallelOptions
			{
				CancellationToken = cts.Token
			};
			cts.Token.Register(() => Console.WriteLine("Cancelling...."));

			Console.WriteLine("끝내려면 엔터키를 누르세용.");

			IEnumerable<string> files = Directory.GetFiles(@"E:\_0_경제 포커스2", "*", SearchOption.AllDirectories);
			List<string> fileList = new List<string>();

			Console.WriteLine("파일 개수 : {0}", files.Count());

			Task task = Task.Factory.StartNew(() =>
			{
				try
				{
					Parallel.ForEach(files, parallelOptions,
						(file) =>
						{
							FileInfo fileInfo = new FileInfo(file);

							if (fileInfo.Exists)
							{
								if (fileInfo.Length >= 10000000)
								{
									fileList.Add(fileInfo.Name);
								}
							}
						});
				}
				catch (OperationCanceledException)
				{
				}
			});

			Console.Read();

			cts.Cancel();
			task.Wait();

			foreach (var file in fileList)
			{
				Console.WriteLine(file);
			}
			Console.WriteLine("총 파일 개수 : {0}", fileList.Count());
		}
	}
}
