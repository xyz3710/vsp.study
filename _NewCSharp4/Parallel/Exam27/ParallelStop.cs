﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam27
{
	class ParallelStop
	{
		static void Main(string[] args)
		{
			Parallel.For(0, 30, (i, loopState) =>
			{
				if (i == 10)
				{
					loopState.Stop();

					Console.WriteLine("Stop at {0}", i.ToString());
				}
				else
				{
					Console.WriteLine(i.ToString());
				}
			});
		}
	}
}
