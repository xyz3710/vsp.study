﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam27
{
	class Program
	{
		static void Main1(string[] args)
		{
			Parallel.For(0, 30, (i, loopState) =>
			{
				if (i == 10)
				{
					loopState.Break();

					Console.WriteLine("Break at {0}", i.ToString());
				}
				else
				{
					Console.WriteLine(i.ToString());
				}
			});
		}
	}
}
