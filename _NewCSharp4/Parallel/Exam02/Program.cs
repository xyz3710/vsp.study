﻿using System;
using System.Threading.Tasks;

namespace Exam02
{
	class Program
	{
		static void Main(string[] args)
		{
			const int max = 10000;

			//현재 작업중인 스레드외에 추가로 스레드를 생성
			Task task = new Task(() =>
			{
				for (int count = 0; count < max; count++)
				{
					Console.Write("|");
				}
			});

			//추가 스레드 시작
			task.Start();

			//현재 작업중인 스레드에서도 반복문 시작
			for (int count = 0; count < max; count++)
			{
				Console.Write("-");
			}

			//혹시 현재 스레드가 빨리 끝나더라도,
			//추가 스레드가 끝날 때 까지 기다리기.            
			task.Wait();
		}
	}
}
