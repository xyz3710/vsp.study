﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02_NewPapers
{
	public class Person : IObserver<News>
	{
		#region Constructors
		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person(string name = "", int age = 30)
		{
			Name = name;
			Age = age;
		}
		#endregion

		private IDisposable _unsubscriber;

		public virtual void Subscribe(IObservable<News> provider)
		{
			if (provider != null)
			{
				_unsubscriber = provider.Subscribe(this);
			}
		}

		public virtual void Unsubscribe()
		{
			_unsubscriber.Dispose();
		}
		 
		#region IObserver<News> 멤버

		public void OnCompleted()
		{
			Console.WriteLine("{0}님 신문 구독이 끝났습니다.", Name);
		}

		public void OnError(Exception error)
		{
			Console.WriteLine("{0}님 {1} 구독을 해지하셨습니다.", Name, (error as NewspaperException).NewspaperBrandName);
		}

		public void OnNext(News value)
		{
			Console.WriteLine("{0}님이 구독 : {1}", Name, value);
		}

		#endregion

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}
	}
}
