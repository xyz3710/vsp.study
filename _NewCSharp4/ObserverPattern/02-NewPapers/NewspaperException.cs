﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02_NewPapers
{
	class NewspaperException : Exception
	{
		#region Constructors
		/// <summary>
		/// NewspaperException class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public NewspaperException(string newspaperBrandName)
		{
			NewspaperBrandName = newspaperBrandName;
		}
		#endregion

		/// <summary>
		/// NewspaperBrandName를 구하거나 설정합니다.
		/// </summary>
		public string NewspaperBrandName
		{
			get;
			set;
		}
	}
}
