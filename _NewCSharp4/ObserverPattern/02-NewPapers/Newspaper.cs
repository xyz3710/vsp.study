﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02_NewPapers
{
	/// <summary>
	/// IObservable&lt;News&gt;를 구현하는 push 기반 알림을 지원하는 Newspaper 클래스입니다.
	/// </summary>
	public class Newspaper : IObservable<News>
	{
		private string _brandName;

		#region Constructors
		/// <summary>
		/// Newspaper 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Newspaper(string brandName)
		{
			_brandName = brandName;
			Observers = new List<IObserver<News>>();
		}
		#endregion

		/// <summary>
		/// Observers를 구하거나 설정합니다.
		/// </summary>
		public List<IObserver<News>> Observers
		{
			get;
			set;
		}

		#region IObservable<News> 멤버

		/// <summary>
		/// 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		/// <param name="observer">알림을 받을 개체입니다.</param>
		/// <returns>리소스가 삭제되도록 설정할 수 있는 옵서버 인터페이스입니다.</returns>
		public IDisposable Subscribe(IObserver<News> observer)
		{
			if (Observers.Contains(observer) == false)
				Observers.Add(observer);

			return new Unsubscriber(Observers, observer);
		}

		#endregion

		#region Unsubscriber
		private class Unsubscriber : IDisposable
		{
			private List<IObserver<News>> _observers;
			private IObserver<News> _observer;

			#region Constructors
			/// <summary>
			/// Unsubscriber class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="observers"></param>
			/// <param name="observer"></param>
			public Unsubscriber(List<IObserver<News>> observers, IObserver<News> observer)
			{
				_observers = observers;
				_observer = observer;
			}
			#endregion

			#region IDisposable 멤버

			/// <summary>
			/// 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
			/// </summary>
			public void Dispose()
			{
				if (_observers != null && _observers.Contains(_observer) == true)
					_observers.Remove(_observer);
			}

			#endregion
		}
		#endregion

		/// <summary>
		/// Observer들에게 알림을 전달합니다.
		/// </summary>
		public void PushNotification(News token)
		{
			foreach (IObserver<News> observer in Observers)
			{
				if (token == null)
					observer.OnError(new NewspaperException(_brandName));
				else
					observer.OnNext(token);
			}
		}

		/// <summary>
		/// Observer들에게 알림을 종료합니다.
		/// </summary>
		public void EndNotification()
		{
			foreach (IObserver<News> observer in Observers)
				if (Observers.Contains(observer) == true)
					observer.OnCompleted();

			Observers.Clear();
		}
	}
}
