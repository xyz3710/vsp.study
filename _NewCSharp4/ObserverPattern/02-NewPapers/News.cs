﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02_NewPapers
{
	public class News
	{
		#region Constructors
		/// <summary>
		/// News class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="title"></param>
		/// <param name="article"></param>
		public News(string title, string article)
		{
			Title = title;
			Article = article;
		}

		#endregion

		/// <summary>
		/// Title를 구하거나 설정합니다.
		/// </summary>
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Article를 구하거나 설정합니다.
		/// </summary>
		public string Article
		{
			get;
			set;
		}

		#region ToString
		/// <summary>
		/// News를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("Title:{0}, Article:{1}",
					Title, Article);
		}
		#endregion
	}
}
