﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02_NewPapers
{
	class Program
	{
		static void Main(string[] args)
		{
			Newspaper hanKyure = new Newspaper("한겨레");
			Newspaper kyungHeyang = new Newspaper("경향신문");

			Person person1 = new Person("박근혜");
			Person person2 = new Person("정동영");
			Person person3 = new Person("전두환");
			Person person4 = new Person("김영삼");
			Person person5 = new Person("노태후");

			person1.Subscribe(hanKyure);
			person1.Subscribe(kyungHeyang);
			person2.Subscribe(hanKyure);
			person3.Subscribe(hanKyure);
			person4.Subscribe(hanKyure);
			person5.Subscribe(kyungHeyang);

			hanKyure.PushNotification(new News("속보", "이명박이 피격 당했다."));
			hanKyure.PushNotification(new News("속보", "국민들이 환호한다. 잘 죽었다."));
			kyungHeyang.PushNotification(new News("속보", "이명박이 계란을 맞고 중태에 빠졌다."));
			kyungHeyang.PushNotification(new News("만평", "얼마나 죄를 졌으면 계란을 맞고 죽냐. ㅋㅋㅋ"));
			kyungHeyang.PushNotification(null);
			
			hanKyure.EndNotification();
			kyungHeyang.EndNotification();
		}
	}
}
