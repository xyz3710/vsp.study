using System;

namespace _01_ObserverPattern
{
	public struct Location
	{
		double lat, lon;

		public Location(double latitude, double longitude)
		{
			this.lat = latitude;
			this.lon = longitude;
		}

		/// <summary>
		/// 위도
		/// </summary>
		public double Latitude
		{
			get
			{
				return this.lat;
			}
		}

		/// <summary>
		/// 경도
		/// </summary>
		public double Longitude
		{
			get
			{
				return this.lon;
			}
		}
	}
}
