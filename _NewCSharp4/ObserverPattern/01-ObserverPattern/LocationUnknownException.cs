using System;

namespace _01_ObserverPattern
{
	public class LocationUnknownException : Exception
	{
		internal LocationUnknownException()
		{
		}
	}
}
