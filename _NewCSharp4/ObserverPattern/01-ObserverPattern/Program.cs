﻿/**********************************************************************************************************************/
/*	Domain		:	_01_ObserverPattern.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 3월 22일 화요일 오전 8:57
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://msdn.microsoft.com/ko-kr/library/dd990377(v=VS.100).aspx
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _01_ObserverPattern
{
	class Program
	{
		static void Main(string[] args)
		{
			// Define a provider and two observers.
			LocationTracker provider = new LocationTracker();
			LocationReporter reporter1 = new LocationReporter("FixedGPS");
			reporter1.Subscribe(provider);
			LocationReporter reporter2 = new LocationReporter("MobileGPS");
			reporter2.Subscribe(provider);

			provider.TrackLocation(new Location(47.6456, -122.1312));
			reporter1.Unsubscribe();
			provider.TrackLocation(new Location(47.6677, -122.1199));
			provider.TrackLocation(null);
			provider.EndTransmission();
		}
	}
	// The example displays output similar to the following:
	//      FixedGPS: The current location is 47.6456, -122.1312
	//      MobileGPS: The current location is 47.6456, -122.1312
	//      MobileGPS: The current location is 47.6677, -122.1199
	//      MobileGPS: The location cannot be determined.
	//      The Location Tracker has completed transmitting data to MobileGPS.

}