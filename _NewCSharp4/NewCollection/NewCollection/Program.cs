﻿/**********************************************************************************************************************/
/*	Domain		:	NewCollection.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 11월 1일 화요일 오후 8:44
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://geekswithblogs.net/BlackRabbitCoder/archive/2011/06/16/c.net-fundamentals-choosing-the-right-collection-class.aspx
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;

namespace NewCollection
{
	class Program
	{
		static void Main(string[] args)
		{
			ConcurrentQueue<int> queue = new ConcurrentQueue<int>();

			for (int i = 0; i < 100; i++)
				queue.Enqueue(i);

			for (int outer = 0; outer < 8; outer++)
			{
				for (int i = 1; i <= 10; i++)
				{
					if (i == 10)
					{
						Console.Write(0);
						continue;
					}

					Console.Write(i);
				}
			}

			Dictionary<int, int> pos = new Dictionary<int, int>();
			List<int> ids = new List<int>();
			Parallel.ForEach<int>(
				queue,
				new ParallelOptions
				{
					MaxDegreeOfParallelism = 2,
				},
				x =>
				{
					int id = Thread.CurrentThread.ManagedThreadId;

					if (ids.Contains(id) == false)
						ids.Add(id);

					int row = 1;
					int col = ids.Count;

					if (pos.ContainsKey(col) == true)
						row = pos[col];

					Console.SetCursorPosition(col * 5, row);
					Console.Write(x);

					pos[col] = ++row;
				});

			Console.WriteLine();

			var scores = new[] { 73, 77, 89, 90, 92, 77 };

			foreach (var score in scores.DefaultIfEmpty(100))
				Console.WriteLine("The score is: " + score);
		}
	}
}
