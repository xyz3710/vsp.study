﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _01_Zip
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] parms = { "custom_action_id", "id", "resource_type", "uuid" };
			string[] values = { "1", "330", "Lot", "b53c37ed8c1f8a8075961400b1b4606eb12fd81b" };

			Console.WriteLine(ZipExtentionMethodTest(parms, values));


		}

		private static string ZipExtentionMethodTest(string[] parameters, string[] values)
		{
			// Dictionary로 parsing
			string result= string.Empty;
			var paramStr = parameters.Zip(values, (param, value) => new
			{
				inplace = (values[values.Length - 1] != value ?
					param + "=" + value + "&" : param + "=" + value
				)
			});

			foreach (var item in paramStr)
				result += item.inplace;

			return result;
		}
	}
}
