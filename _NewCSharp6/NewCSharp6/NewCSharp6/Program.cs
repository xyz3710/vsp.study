﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Console;

namespace NewCSharp6
{
	class Program
	{
		private int PropertyName { get; set; } = 199;
		private string NewProperty { get; } = "Readonly property";
		private Dictionary<string, string> Dic = new Dictionary<string, string> { ["Name"] = "Sam", ["Relation"] = "Son" };

		static void Main(string[] args)
		{
			var n = -1;

			TestNameof();

			TestNullConditionalOperator();

			//WriteLine(GetFibonacci(n));

			TestAsyncInCatch();

			TestStringInterpolation();

			var pro = new Program();

			WriteLine($"PropertyName:{pro.PropertyName}, NewProperty:{pro.NewProperty}");

			WriteLine(Add(10, 20));

			TestExceptionFilters(null);

			TestIndexInitializers();
		}

		private static void TestIndexInitializers()
		{
			var dic = new Dictionary<string, string>
			{
				["Name"] = "Robbin",
				["Age"] = "43",
				["Hobby"] = "Maker",
			};

			WriteLine(dic["Hobby"]);
		}

		private static void TestExceptionFilters(bool? ignore)
		{
			try
			{
				throw new ArgumentException("ignore");
			}
			catch (ArgumentException) when (ignore == null)
			{
				WriteLine("exception filtered: ignore is null");
			}
		}

		private static int Add(int a, int b) => a + b;

		private static void TestStringInterpolation()
		{
			var price = 100.0506;
			var formattedPrice = $"{price:#,##0.0#}";

			WriteLine(formattedPrice);
		}

		private static async void TestAsyncInCatch()
		{
			var program = new Program();

			await program.HandleAsync(new AsyncResource(), false);
		}

		/// <summary>
		/// handle as an asynchronous operation. by CSharp 6
		/// </summary>
		/// <param name="resource">The resource.</param>
		/// <param name="throwException">if set to <c>true</c> [throw exception].</param>
		/// <returns>Task.</returns>
		public async Task HandleAsync(AsyncResource resource, bool throwException)
		{
			try
			{
				await resource.OpenAsync(throwException);
			}
			catch (Exception exception)
			{
				await resource.LogAsync(exception);
			}
			finally
			{
				await resource.CloseAsync();
			}
		}

		public async Task HandleAsync5(AsyncResource resource, bool throwException)
		{
			Exception exceptionToLog = null;
			try
			{
				await resource.OpenAsync(throwException);
			}
			catch (Exception exception)
			{
				exceptionToLog = exception;
			}
			if (exceptionToLog != null)
			{
				await resource.LogAsync(exceptionToLog);
			}
		}

		private static void TestNullConditionalOperator()
		{
			var @string = "Hi";

			GetStringLength(@string);
			GetStringLength2(null);
		}

		private static int GetStringLength(string arg)
		{
			return arg != null ? arg.Length : 0;
		}

		private static int GetStringLength2(string arg)
		{
			return arg?.Length ?? 0;
		}

		private static void TestNameof()
		{
			WriteLine(nameof(PropertyName));
		}

		private static int GetFibonacci(int n)
		{
			if (n < 0)
			{
				throw new ArgumentException("Negative value not allowed", nameof(n));
			}

			return n;
		}
	}

	public class AsyncResource
	{
		internal Task CloseAsync()
		{
			return Task.Factory.StartNew(() =>
			{
				Console.WriteLine(nameof(CloseAsync));
			});
		}

		internal Task LogAsync(Exception exception)
		{
			return Task.Factory.StartNew(() =>
			{
				Console.WriteLine(nameof(LogAsync));
			});
		}

		internal Task OpenAsync(bool throwException)
		{
			return Task.Factory.StartNew(() =>
			{
				Console.WriteLine(nameof(OpenAsync));
			});
		}
	}
}
